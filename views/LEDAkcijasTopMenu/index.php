<ul class="navbar-nav">
    <?php foreach($menu as $item){ ?>
    <li class="nav-item <?php echo ($item['active']) ? 'active' : ''?>">
        <a class="nav-link" href="<?php echo $item['link']?>"><?php echo $item['label']?></a>
    </li>
    <?php } ?>
</ul>