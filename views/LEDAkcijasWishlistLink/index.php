<?php

use Constructor\WebApp;
use Constructor\Url;

?>
<a href="<?php echo Url::get('wishlist')?>" class="action-toggle wish-toggle"><span><?php echo WebApp::$app->l('Vēlmju saraksts')?></span></a>
<?php if(!$user){ ?>
<div class="action-menu wish-menu">
    <div class="header">
        <span class="not-registered"><?php echo WebApp::$app->l('Pieejams tikai reģistrētiem lietotājiem')?></span>
    </div>
    <?php echo $this->render('/LEDAkcijasLogin/login-form') ?>
</div>
<?php } ?>
