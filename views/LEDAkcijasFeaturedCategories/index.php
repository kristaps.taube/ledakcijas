<?php

use Constructor\WebApp;
use Constructor\Url;

$lang = WebApp::$app->getLanguage();
$links_col = \LedAkcijasLinkCollection::getInstance();
$catcol = \shopcatcollection::getInstance();
$prod_col = \shopprodcollection::getInstance();

?>

<?php foreach($categories as $category){ ?>
<div class="product-block highlighted">
    <div class="row align-items-start">
        <aside>
            <div class="side-bn">
                <a href="<?php echo URL::get('category', ['category' => $category]) ?>">
                    <div class="header">
                        <span><?php echo $category['title_'.$lang]?></span>
                    </div>
                    <img src="<?php echo $category['featured_image']?>" alt="">
                </a>
            </div>
        </aside>
        <div class="product-grid d-flex">
            <div class="product menu">
                <div class="catalog-menu">
                    <?php if($links = $links_col->getForFeaturedCategory($category['item_id'])){ ?>
                    <ul>
                        <?php foreach($links as $link){ ?>
                        <?php
                            $link_cat = $link['category_id'] ? $catcol->getFromCache($link['category_id']) : $category;
                        ?>
                        <li class="nav-item">
                            <a href="<?php echo URL::get('filter_link', ['category' => $link_cat, 'link' => $link]) ?>" class="nav-link"><?php echo $link['name_'.$lang]?></a>
                        </li>
                        <?php } ?>
                        <li class="nav-item">
                            <a href="<?php echo URL::get('category', ['category' => $category]) ?>" class="nav-link orange"><?php echo WebApp::l('Skatīt visus')?> &raquo;</a>
                        </li>
                    </ul>
                    <?php } ?>
                </div>
            </div>
            <?php foreach($prod_col->getFeaturedFromCategory($category['item_id'], 5) as $product){ ?>

            <?php echo $this->render('/LEDAkcijasProductList/_item', ['product' => $product])?>

            <?php } ?>
            <div class="product placeholder"></div>
            <div class="product placeholder"></div>
            <div class="product placeholder"></div>
            <div class="product placeholder"></div>
        </div>
    </div>
</div>
<?php } ?>