<?php

use Constructor\WebApp;
use Constructor\Url;

$lang = WebApp::$app->getLanguage();

?>
<div class="langmenu align-items-center ml-auto">
    <div class="select-wrap" tabindex="1">
        <div class="select-value">
            <?php echo strtoupper($lang)?>
        </div>
        <div class="select-options">
            <?php foreach($languages as $short => $language){ ?>
            <div class="option <?php echo $lang == $short ? 'selected' : ''?>" data-value="<?php echo $short; ?>">
                <a href="<?php echo Url::get('/', ['lang' => $short])?>"><?php echo strtoupper($short); ?></a>
            </div>
            <?php } ?>
        </div>
    </div>
</div>