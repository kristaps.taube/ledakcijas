<?php

use Constructor\WebApp;
use Constructor\Url;

?>
<form id='SearchForm' action='<?php echo Url::get('search')?>' method='get' data-autocomplete-src='<?php echo Url::get('/search/autocomplete', ['l' => Constructor\WebApp::$app->getLanguage()])?>' class="form-inline col-12">
    <div class="input-group">
        <input type="search" placeholder="<?php echo Webapp::$app->l('Meklēt')?>.." id="search-needle" name="q" aria-label="<?php echo Webapp::$app->l('Meklēt')?>" autocomplete="off" value="<?php echo htmlspecialchars($query)?>">
        <button type="submit"></button>
        <div id="autocomplete-suggestions">
            <!-- placeholder -->
        </div>
    </div>
</form>