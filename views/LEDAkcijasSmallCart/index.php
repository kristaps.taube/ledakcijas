<?php

use Constructor\Url;
use Constructor\WebApp;
use Constructor\App;

$sum = 0;

?>
<a href="<?php echo Url::get('cart')?>" class="action-toggle cart-toggle <?php echo $items ? 'has-items' : ''?> <?php echo $open ? 'visible' : ''?>">
    <span>
    <?php echo App::l("Grozs")?>
    <?php if($items){ ?>
    (<span id="cart-count"><?php echo count($items)?></span>)
    <?php } ?>
    </span>
</a>
<?php if($items){ ?>
<div class="action-menu cart-menu <?php echo $open ? 'visible' : ''?>">
    <div class="header">
        <span><?php echo WebApp::$app->l("Mans grozs")?></span>
    </div>
    <div id="small-cart" class="small-cart">
        <?php foreach($items as $item){ ?>
        <?php
            $picture = $item['product']['picture'] ? $item['product']['picture'] : "/images/html/blank_product.png";
        ?>
        <div class="row no-gutters prod">
            <div class="col image">
                <a href="<?php echo Url::get("product", ['product' => $item['product']])?>">
                    <img src="<?php echo getThumbUrl($picture, 55, 55, 6) ?>" alt="">
                </a>
            </div>
            <div class="col">
                <a href="<?php echo Url::get("product", ['product' => $item['product']])?>">
                    <span class="name"><?php echo $item['name'] ?></span>
                    <div class="price-wrap">
                    <?php if($item['price'] != $item['base_price']){ ?>
                    <span class="new-price">&euro;
                        <span class="value" data-raw='<?php echo $item['price'] ?>'><?php echo number_format($item['price'], 2)?></span>
                    </span>
                    <?php } ?>
                    <span class="price">&euro;
                        <span class="value" data-raw='<?php echo $item['base_price'] ?>'><?php echo number_format($item['base_price'], 2)?></span>
                    </span>
                    </div>
                </a>
            </div>
            <div class="col delete">
                <a href="<?php echo Url::get("/cart/remove", ['key' => $item['key'], 'a' => 1])?>" class="delete"></a>
                <span class="count">
                    <span class="value"><?php echo $item['count']?></span> <?php echo  WebApp::$app->l('gab.') ?>
                </span>
            </div>
        </div>
        <?php
            $sum += ($item['count'] * $item['price']);
        ?>
        <?php } ?>
    </div>
    <div class="footer">
        <div class="sum-wrap text-right">
            <?php echo WebApp::$app->l("Preču kopsumma")?>:
            <span class="price">&euro;
                <span class="value"><?php echo number_format($sum, 2)?></span>
            </span>
        </div>
        <a href="<?php echo Url::get('cart')?>" class="btn orange d-block my-2"><?php echo WebApp::$app->l("Pirkt")?></a>
    </div>
</div>
<?php } ?>