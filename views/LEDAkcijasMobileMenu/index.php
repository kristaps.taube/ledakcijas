<?php

use Constructor\WebApp;
use Constructor\Url;

$lang = WebApp::$app->getLanguage();

?>
<div class="langmenu">
    <?php foreach($languages as $short => $language){ ?>
    <a href="<?php echo Url::get('/', ['lang' => $short])?>"><?php echo strtoupper($short)?></a>
    <?php } ?>
</div>
<div id="mobile-nav">
    <div class="catalog-menu">
        <?php foreach($categories as $category){ ?>
        <div class="nav-row">
            <div class="column show">
                <div class="nav-item">
                    <a href="<?php echo Url::get('category', ['category' => $category]) ?>" class="nav-link">
                        <i style="background-image:url('<?php echo $category['mob_icon']?>')"></i><?php echo $category['title_'.$lang]?>
                    </a>
                    <div class="submenu-toggle"></div>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>
    <nav>
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="<?php echo Url::get("catalog")?>"><?php echo WebApp::$app->l("Katalogi")?></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo Url::get("calculator")?>"><?php echo WebApp::$app->l("LED kalkulators")?></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo Url::get("blog")?>"><?php echo WebApp::$app->l("Blogs")?></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo Url::get("buj")?>"><?php echo WebApp::$app->l("BUJ")?></a>
            </li>
        </ul>
    </nav>
</div>
