<?php

use Constructor\WebApp;
use Constructor\Url;

$catcol = \shopcatcollection::getInstance();
$lang = WebApp::$app->getLanguage();

$link_groups = LedAkcijasLinkGroupsCollection::getInstance();
$links = LedAkcijasLinkCollection::getInstance();

?>
<ul>
    <?php foreach($catcol->getForMenu() as $category){ ?>
    <li class="nav-item">
        <a href="<?php echo Url::get('category', ['category' => $category]) ?>" class="nav-link">
            <i style="background-image:url('<?php echo $category['icon']?>')"></i><?php echo $category['title_'.$lang]?>
        </a>
        <?php if($filter_groups = $link_groups->getByCategory($category['item_id'])){ ?>
        <div class="submenu-wrap">
            <div class="container row">
                <div class="col offset-md-4 offset-lg-3 col-md-8 col-lg-9 submenu">
                    <div class="row">
                        <?php foreach($filter_groups as $filter_group){ ?>
                        <div class="col-12 col-md-6 col-lg-4">
                            <div class="menu">
                                <div class="nav-item heading">
                                    <span class='nav-link'><?php echo $filter_group['group_name_'.$lang]?></span>
                                </div>
                                <?php foreach($links->getForMenu($filter_group['item_id']) as $link){ ?>

                                <?php
                                    $link_cat = $link['category_id'] ? $catcol->getFromCache($link['category_id']) : $category;
                                ?>

                                <div class="nav-item">
                                    <a href="<?php echo Url::get('filter_link', ['category' => $link_cat, 'link' => $link])?>" class="nav-link"><?php echo $link['name_'.$lang]?></a>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
    </li>
    <?php } ?>
    <li class="nav-item">
        <a href="<?php echo Url::get('catalog')?>" class="nav-link orange">
            <i></i><?php echo WebApp::l("Visas kategorijas")?> &raquo;
        </a>
    </li>
</ul>