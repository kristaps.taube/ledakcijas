<?php

use Constructor\WebApp;

?>
<div class="col-12 col-md-4 col-lg-3">
    <div class="action-wrap catalog-menu-wrap">
        <div class="action-toggle catalog-toggler">
            <?php echo WebApp::l('Kategorijas')?>
        </div>
        <div class="action-menu catalog-menu">
            <?php echo $this->render('index') ?>
        </div>
    </div>
</div>