<?php

use Constructor\Url;

?>
<ol class="carousel-indicators">
    <?php foreach($banners as $i => $banner){ ?>
    <li data-target="#main-bns" data-slide-to="<?php echo $i ?>" class="<?php echo (!$i) ? 'active' : '' ?>"></li>
    <?php } ?>
</ol>
<div class="carousel-inner">
    <?php foreach($banners as $i => $banner){ ?>
    <div class="carousel-item <?php echo (!$i) ? 'active' : '' ?>">
        <?php if($banner['link']){ ?><a href='<?php echo Url::get('/banners/click', ['b' => $banner['item_id'], 'lang' => $lang])?>'><?php } ?>
        <img class="d-block w-100" src="<?php echo getThumbUrl($banner['image'], 915, 468, 7)?>" alt="Slide">
        <?php if($banner['link']){ ?></a><?php } ?>
    </div>
    <?php } ?>
</div>