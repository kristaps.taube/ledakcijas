<?php

use Constructor\WebApp;
use Constructor\Url;

$lang = WebApp::$app->getLanguage();
$filter_col = EVeikalsFilters::getInstance();

$group_ids = [];
foreach($filter_groups as $filter_group){
    $group_ids[] = $filter_group['item_id'];
}

$grouped_filters = $filter_col->getByGroupIds($group_ids, $lang);

if($filter_link){
    $params = ['l' => $filter_link['item_id']];
}else{
    $params = ['c' => $category['item_id']];
}

if($filter_groups){ ?>
<div id="filters">
    <form action="<?php echo Url::get("/category/process-filters", $params)?>" method="POST">
        <div class="heading d-flex justify-content-between">
            <span><?php echo WebApp::l("Filtrēšana")?></span>
            <a href='<?php echo Url::get("/category/clear-filters", $params)?>' class='btn'><?php echo WebApp::l("Notīrīt")?></a>
        </div>
        <?php foreach($filter_groups as $filter_group){ ?>

        <div class="filter">
            <div class="heading">
                <?php echo $filter_group['title_'.$lang]?>
            </div>
            <div class="content">
                <?php if(isset($grouped_filters[$filter_group['item_id']])){ ?>
                    <?php foreach($grouped_filters[$filter_group['item_id']] as $filter){ ?>
                    <label class="cb">
                        <input type="checkbox" name="filters[]" value="<?php echo $filter['item_id']?>" <?php echo (in_array($filter['item_id'], $active_filters)? 'checked' : '')?>>
                        <span><?php echo $filter['param_title_'.$lang]?></span>
                    </label>
                    <?php } ?>
                <?php } ?>

            </div>
        </div>
        <?php } ?>
    </form>
</div>
<?php } ?>