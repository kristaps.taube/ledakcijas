<?php

use Constructor\Url;
use Constructor\WebApp;

$lang = WebApp::$app->getLanguage();

?>
<section class="pros">
    <div class="container">
        <div class="row">
            <?php foreach($pros as $pro){ ?>
            <div class="col-6 col-md-4 col-lg-2 pro">
                <?php if($pro['link_'.$lang]){ ?>
                <a href="<?php echo Url::get($pro['link_'.$lang])?>">
                <?php } ?>
                    <div class="image d-flex align-items-center justify-content-center">
                        <img src="<?php echo $pro['image']?>" alt="">
                    </div>
                    <div class="text"><?php echo $pro['text_'.$lang]?></div>
                <?php if($pro['link_'.$lang]){ ?>
                </a>
                <?php } ?>
            </div>
            <?php } ?>
        </div>
    </div>
</section>