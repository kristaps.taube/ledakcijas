<?php

use Constructor\WebApp;
use Constructor\Url;

$lang = isset($lang) ? $lang : WebApp::$app->getLanguage();

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <!-- utf-8 works for most cases -->
    <meta name="viewport" content="width=device-width">
    <!-- Forcing initial-scale shouldn't be necessary -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html charset=UTF-8" />
    <!-- Use the latest (edge) version of IE rendering engine -->
    <title><?php echo WebApp::l("LEDakcijas.lv")?></title>
    <!-- the <title> tag shows on email notifications on Android 4.4. -->
    <style type="text/css">
        /* ensure that clients don't add any padding or spaces around the email design and allow us to style emails for the entire width of the preview pane */

        body,
        #bodyTable {
            height: 100% !important;
            width: 100% !important;
            margin: 0;
            padding: 0;
            font-family: 'Arial';
            color: #323232;
            font-size: 16px;
        }

        /* Ensures Webkit- and Windows-based clients don't automatically resize the email text. */

        body,
        table,
        td,
        p,
        a,
        li,
        blockquote {
            margin: 0;
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }

        /* Forces Yahoo! to display emails at full width */

        .thread-item.expanded .thread-body .body,
        .msg-body {
            width: 100% !important;
            display: block !important;
        }

        /* Forces Hotmail to display emails at full width */

        .ReadMsgBody,
        .ExternalClass {
            width: 100%;
            background-color: #f4f4f4;
        }

        /* Forces Hotmail to display normal line spacing. */

        .ExternalClass,
        .ExternalClass p,
        .ExternalClass span,
        .ExternalClass font,
        .ExternalClass td,
        .ExternalClass div {
            line-height: 100%;
        }

        /* Resolves webkit padding issue. */

        table {
            border-spacing: 0;
        }

        /* Resolves the Outlook 2007, 2010, and Gmail td padding issue, and removes spacing around tables that Outlook adds. */

        table,
        td {
            border-collapse: collapse;
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }

        /* Corrects the way Internet Explorer renders resized images in emails. */

        img {
            -ms-interpolation-mode: bicubic;
        }

        /* Ensures images don't have borders or text-decorations applied to them by default. */

        img,
        a img {
            border: 0;
            outline: none;
            text-decoration: none;
        }

        /* Styles Yahoo's auto-sensing link color and border */

        .yshortcuts a {
            border-bottom: none !important;
        }

        p {
            line-height: 20px;
            margin-bottom: 10px;
        }

        /* Apple Mail doesn't support max-width, so we use media queries to constrain the email container width. */

        @media only screen and (min-width: 601px) {
            .email-container {
                width: 600px !important;
            }
        }
    </style>
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" bgcolor="#f4f4f4" style="margin:0; padding:0; -webkit-text-size-adjust:none; -ms-text-size-adjust:none;">
    <table cellpadding="0" cellspacing="0" border="0" height="100%" width="100%" bgcolor="#f4f4f4" id="bodyTable" style="border-collapse: collapse;table-layout: fixed;margin:0 auto;">
        <tr>
            <td>
                <!-- Outlook and Lotus Notes don't support max-width but are always on desktop, so we can enforce a wide, fixed width view. -->
                <!--[if (gte mso 9)|(IE)]>
                <table width="600" align="center" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                    <td>
                <![endif]-->
                <!-- Beginning of Outlook-specific wrapper : END -->

                <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="max-width: 600px;margin: auto;" class="email-container">
                    <tr>
                        <td>
                            <table border="0" width="100%" cellpadding="0" cellspacing="0">
                                <?php if(isset($view_link) && $view_link){ ?>
                                <tr>
                                    <td colspan="2" class="hat" style="text-align: center; font-size: 14px; background-color: #ffffff; padding: 10px;">
                                        <p style="margin:0"><?php echo WebApp::l("Nesaprotams e-pasts?", ['lang' => $lang])?>
                                            <a href="<?php echo $view_link?>" style="font-weight: 700; color: #1d3c72; text-decoration: none;"><?php echo WebApp::l("Spied šeit!", ['lang' => $lang])?></a>
                                        </p>
                                    </td>
                                </tr>
                                <?php } ?>
                                <tr>
                                    <td valign="middle" style="padding-left: 30px;text-align: left; background-color: #123C76; padding: 15px;">
                                        <a href="<?php echo Url::get('/')?>">
                                            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASEAAAA/CAYAAACvtn5EAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAJv1JREFUeNrsXQecVcX1Pq9sZYFlgaVJE7EgiImKFUE00RgVS2xJMEbTTLH8LYmxxJaCnagUeyGxRTH2EhELiqLRSDRCUKTLssrusn3fe/d/T9433LOzc9u+LaB3fr/5wXt73713Zs75znfOzJyJWZZFIUuRXU+z61S79rNrU4DfxOyab9cP7fqgXZ+g9pcf2fVEu/YN+OyOKnl2zdj1SbveZtf1dj3brkfatY9dm+1aadd6u8bt2h99lcbvLdyjEPfhfwvwf1m4TSm7fmHXFXb9wK6fo+8WUVSi8iUrsZAgVGbXZ+w6IcfnTrfrb9rxu7/b9aitoN9W2bXCrnt28XPfBRjdYtc3IvGNylcRhO6167QOevYPcL8wwHVBNGRbynN2vcmuT0VdEZWvCghtb9eldk120LNftuvkENevtOuwaMjalDvseibcwKhEZZsrYQCll3Y9xy0upGxspNDnt1V2/b5djxbfhQ1GNWufZ9l1gV17dkE/cdymxa5X2nWE4b0utetqysaA2gC9+H8D7sNls+Fajh9xnKiYsjGvHey6B2Vjb2Nc3u10u+5L2Rjd8kiko/JlBqGU4fOfDeDgVoZpIJQK+a46aD1i1xe7uL9+6wIcM11ApSPLRDDHn9p1iPY3BqhX7HoQ2KoEQCsS86hszSWeiytn1/KQTEpX3lxKWRf31XYG5Vf90L8Lnv8qmNjOlJ2V08F/kF0fB4NqL9uMSlS2ahCKbWXv3tUK1s8ApN3Rb7V2nQE37SOtL3akKFAdlS8xCFmGz2GAoFT73JyjIjfk2Paw4NDL4z7xbgDLf8NF+0Bry952/WEk2lH5qjChMAo2VPu8KiQA6EHfxhwBJiw4FLh8n+4A17K9hRdH8mLJKu37a7vBXY1KVDodhHJlHdtp3y0L8XueLdJn4DZ3MftodLl/ow8gdnbhVdWnad8xAP0yEu+obAsl2UXP4WDpSO27MNPJPTUQYuZRF+L3u9q1BPfg2E7KB5jTYBlpXPsxtZ6al0yrKYRrOZqyq82Z1altIEm4luxWLTWwmiBlnl3n23WK+I63tvy+G1laVKLS4SBk5cCiSqj1rA2XNSF+30NzxxopXEzodsrGSsLGgVTcq4bc10LVU/CFgifZ9QqPZ1XbdSFlV0I/F/Jdf6+BEE/bH0DZRaFRicqXwh3LN7hDQRlAzAAALSFBKCE+N1C4FcJF1L5ZKhV0LvUBocYQ9/P6Gz/n23Z91q6XhXzX+QYX98RIxP/HONsr41HZypiQviaoKqTyxXyYlVcpNQBgdYjfd6ZLUheiLYUh7vs7u26g7MrwoOUeMCJV9utgWelDThYAL1e2FuyxK4q+IJPd3cPtOgAyy+/LWQh4IoQXt74kruUQAW+I3hHX9sB787WvUXbd1bas2/2E2+82Xs2Q4dptAYQGaJ83hmAjJhDKhHh2XwMIBXXH2Lr16qD+qoZAb99OgNPbwTvxl1A2kDzWYLWvpmzqky8C3v8/2ufBuPcXObab3cgzKLso03IZu5iQKTZOT+P9qzpZhhUA7ULZFe1T0M8ZyGcaTJjl4MdgjHfZ9WuUDegPxt9YCTn+V4DrOWXMIjDSt7YxAPoOZScmBqJNlg/T5/H6G8IAm7dmENJXBVeGAJJMwO+83DFZwqwxGkHZPVgdURYiVjNDfBdm+4m+z42zCJwPQWCrPA0MSBWOpXG2gRsC3v9DCFShGLMJcO/aW34J4fxCsD635RqKlfB7875CTnXyrU5iopIBcbzvMYAPxxpX4O9J/LsJ1zLYHGLXQ/E9A886ciYIEjA0aXw+0K4vAJAe3kYA6Bd2vRlt3uwTCrCEp8EMmuOI39+aQWi4wYp3xnM6utQANEo9lEENUC1iVQwKa+06zq6naPS1WPttKoSy9DGwSQXIH8PqskU/QVxzbAgQWor77Cq+G5pD3+0CMNkAEFKK6mVZM+hnXs39DbCP2e10rYIwoIFwWdlQLcf3fQGENRifcjDnSjDZJN4zhfEcINh1H9yD2/wpXJo5dv2vXd/bygGIdfRiAHEN2pn0CH/EMFb1MDDHwhDe147x6BIQGqx9XpdDPKmWwq2tiefAoj6j7F6r9pQDNRDKGPosHVBZeEB7G/pBL/M0EBoAxQ/KJvR75uUgHwcDvNcIgcyQ+4SGpQk4W+PJIUCoPQL/M7uOp2xQPg5ZWwIGswz9sRvasjfkNoX+LAcwXQeQ2QDWfJhd9xegxcp9iV2P28pBSLn0NdR6IseNvWbE9yn8/yABQh0NQEZQCwNCZQaGEbToGz/XQ0C7AoRyKXocrMUABkHBoZDaBtg3Gq77XPtcTE7ANEhp8RnjcjCFUrgnpQIwGvCcjXiPBjzfghHifWmXkvssnwV3508Y4xa4oD0MLkEcf3eL7Q3Ee6pAeB1Aopqc2U5+//3A0mK4/nnKpjeR68ieBtBMh1HZiGt5bdZPqO2sIrszV8G1+Qxxrd3Brmohz2WINebh/414txZcv4b8Y6YDcC92mwdhrJiBrwajbUBbk5q7W+tBFNLkzLQuADPyAn3e4nMOZfN1KfkucDFecbxTDNdYLtc0kTn1spUrE4r5fA4DQhs0IfGjfYl2Kn6upYfhu3wfpXcredR260djAMAlym1lexoCeRZld+CPgoKXGMA8BrdkPZjEKCgh4d2ryH+R6UjRTga0YYjXmEAoA2YyF8/j3x4P5rITFLOInDVUq3B9XwFCgwGYhXi/88i8kLUFruUkAHFvuOluK/cvBsCNBUDzs24DsHwdAFIkGIQs1VBqZmRv2vUhzW2fjHbuB91ICvBI4BkfAFzLhPsYR1/w94vteqMWH01pcloNhudV1pKzBk+lLH5MgJDUzTg5OdS9QCgFIOXZ2lc6M1YThqpt52Pt/e7VXUxokAE8qZ3vkjAAmAmEdJeticKtDk9q9+eZre9Sdtd9MwSzEdY0bhiHPMSRLoDyyRhKkHU0vUSfpAAOu7rITxwuL7s+7yAGNQq/24R210AR+L1GA6CayFlIWmtwA91KSrggVgBwTwmwbICbFhdB32qX5yXRFk42dyplc0BxGpa3wLBOgmFQsTbpLlmQlXHo72bRPnVdT8RvDgKY1Rr0KOh49RBGXTHXQS5ybQlWlib3YDdfMwXuK7PSR7YGEBqSQzypO4sey1ppGBwrBDjkGwBGLyWGa8Is7EwIa7wGfV+AIKuykPmC5sdFrCeNf5vE9UmhGE0B5cLSgqJNLkDRAiUsBjtoBNux8M76uqoGtCtfA0aV+XIg2MFp1HYdGYP7RYjvKObEM5OfUPYAAf3ai8EKKshZYvKZcEWK0Hd5WrxMAV0twCoJQLkL8jMWba7E34rEmFjknLZSI9oX18BFtY1B8XK7nuvCNIPMIitQLhRjVG/wTtTnzYKFWi7eUR3aVw4X+FXymMjqqlkrfQf8ppC/L/VhUp1VBmsWqMoATDUhwCEICE3WPofZY8cuwo4Ak9VCENWsT28IQx2+U26xmvUrx1iVwhquw+/jwrVi6/YeYhZ+zDVlcKWJWq81kjGENN6xTMR/ZOkPANiI90oKRcjg+0MRA3oGrkgcDIoD0xPIyd7wBb7nE1weBuhWg7UfBgBaqwVv+2MMVb/Vi9iXAq9+6MN+uHYtrinC89bjnqVoywZcsxLP2QnjMBD33SRYW0ZT9s/RpoSBmTTAAPEkx4s+OhMzjJXlEcRWbn7c45o4gGgg3NdnOwKEkh3oEoXdQjHQ4Md2ZWBaInyZIcgeNCZU6ANCrAD6VounQ7zveCjCKghBHILF9+WFjDxLxSuBP4QCNYiYRhLvx+7U3lDmw/F5IyqvI+FFbT/zACF1H7b8L5F5lbiF53JQdCrunQTIcYyG9/q9jLhIizBkkxBE5QWH8wCcabTvUMQg0ogZFQmL/gncPR67P+Kev8OzSjHODQA2BoOr4ab8TsTyuF/fwLM54LsCzK1eKJ8a435wR9SapFoAXDOuHQBQYBbzOICmSbhHDELHUDZNy18ou9whD78fg9iWhXYUoF0bYXjy0QfVMEhzYdhedxmvvpCHn/q4b2n8fTre/TJDjFNhAq8tO4CcJRI7dIQ7lkdtFyuuzwF0mtoJBu159gXwm5u0Tk54UEo1YzPOwHr0mM3GEH2Q9GF0s7V3TFO4hYbDxHvmoY394aJcS+6rly0IeDN+uwZ+PAvvTWBFG4S7kfJpZwyBU7+z0SYDSJJ4xt1QfFOfsqLej/cqoNYL8cqgnPMDxPhuwPu96GPMyvAuat0Qy9GtHu63JRhdNUCaA9nT0P9qO0s55PcYF5ZbB4C7HlUvzGQv0WJbSr+WgH30gWxlArhkxXAzFwaUsSaMxese1xwHI1AjwDJnEBpiCC5/aoieBw0sFxtiH1KIE1qMREfSFSEUU1HrjijNBtewIgcwVtsLeFHf92A5ZblP9HOQchCsc1wo3S3kPk1bAAFxY7ULwFaexJg14/ogcbAgQdGkMDI3QLmCjIGuWH3QlqQPQJairRn8xg+ElJvKMaYHPEINXluI7kNfXCsmC37k4WYfAgbKs2oPurTB0oLV6v+fgSnOASNrEezLK4YXdOOuiiH64YZ+srDVESC0HbVeKayCnkEDs7rynQGli2lApgKaBRoL06fK14RQer8OtkK4h1cbGGFQEOphAOMFAFwTILPV/H0IAOL77w8LmABjY2v8Gw+GeDgs2wNgIaaxfBts7FgIuV9aX0so8VAowgaDrKgNo0VwSS5xGT9LAOp6DyPXTMFWr6taGcCI9kFQ9QGXCQSe6doLMnAdXF1TmQt3Zyxc1Dc92jGDnOOdhkPmdPYs22FR61nWJ2G4RlGwfXvxEJ6JCrr7Xd9MIZaVBAWhwQYXJJe4TE/K7bywpoAsrJD8TwSJhQCjMS5gEaSUG+7f2+P6E0IGpY+HO7aUnOX4DbDiSkGVS8Bu0Pnitxy/eJfctyX8C/GJWEBBrQeAsqKfSs5aI1WuROyrGe9zowegDQKbGAVA+LmhzxvQl3vAQG3wAck04l7vknea4TSuV/upFGvcBDZzIq5JIPi6hwA3XV4XALA+8HheBn1zOt5vOmJUa0TgfmcBtpbBzU+Q99YavTQCbHnpw/se13FscFdyTpfZGwZKX7O3O65tDkoEgoJQvgHpwgSmc9k64CYcQVhYnNxzQ/uxtbCA6Ff6BLxuGQJ7L4R8j7PI2bulZrPKAAb9IJgbEaRdBveAFVetzRniAUL1FHyjbhqsuQ5uoC7YvwU7q4ZcLAa4uI0fr74+GMAyDfc9w8CMd4NS/JHM58NJkGyAqzMXYOJWNiEueD0UTy2uXAq2oXJ58zWj0Y+VLvfaoAVvdQOoygOofM99KLvCey/BwNeD4aixZmN+B8BEAVLPEMbxM4AnG5qTPdzOJFjZaBj36Ybwwa/t+gfKzkpKxlbRESCUaxmS4+91lhJ0xXSCcj8TLIMgaE8Xihl0D1w/j7+tglVmOj+Twh/pfBQsEAkAUPGRAtz7TsquhK3UAoXsDm1P3ssmEgEA3BLtzMB9092T3wAUP4YhGwpG7QZwPCt6ABS+CX09BYLeKNpYCEC1AhpHNSXdFOC6DFwvnvLn2bfnAKCW5gqPBWvxM3L7B4yVLEG9Df3AgWw+PHQC5EPFe7gPJooxYt34gswrud3GNg3Q+JVdHyVzMJvvyVlB/4bfXKmNG8eqfoD7qFXTPdDGxR0BQumQQqmXkyAoxeS/CzslAn1KoO4HAktrFpRq/gIAotY1qLUXcp3DJuHHqilXS7gW7IY94gJQQQHjMVgbPSC8mZz9Se0tMwVDbRHKMwRAcDK5r2dq8HERghoJVbgd33WJj6yDhVYrgdM+7e6v3bsOMnS2GCOVSG0D7h2UoacCMOkYAOAW8t4EXecR55H3aoJLczVickELy8cs1GvA3iqo9cJUaawTBtblVz4HI+LY1U0u1zA7XwQX+SXtbz+Eq6hOAO6LEARvo1nRESDU09DpYcqSHIW8WKPIQafFWdDv6gAmt4MLC4qFAMRKD6qeS5krmKZKNZuPMatATMgNgNiv/xos1b/If+pdBnfdrnmJ3JOo3Yt+nEXOloh8n/FLCAWLC6Xrg79vIid5WRDGFgsYTyS4Wq96AJDKT8RAtRBuSMaHVbMM8DqrnWA8XqFw+dLPh6s2QYuN6s+pI/etFaY+iWHczgbbWe8hb3p2zVK4ymonRAnY65lknuFrFwiN1T7/m7qulFPr2aNlPoHHzig7e/j53ZYWE7GP72lKK12Exzz6agRiNlOgyCd7xGaKoJAt+H+BByM6CtfMcnHZ7ob7p2bDhnu0bw2UqRz/DoO78CwUcQjuNRwApDIOeLke6rSVTADl7AWFdCvjEbMaQc4ZcB94GFK11aMR/XQEZRciPgnmuJjaBvFN5Rmw88vJWfGus7wfg9mU+sRki4UxqMR9OSZ5kcv18wwyfyp0ZDU523VOC8qwg4BQHBZTlje6UNH0dQnLqeuPgHbLDf0JdX76UrfCC+ku074rEUKcwvu5leEA9DFQZK9ZxAUAvJ0RX1rkM143gRF95OKyXQqKnoBrwbvJTYvf2FW9EXGR/oidXYmY0gPCCvdFPCOFd/Vya84C+/thALlr9GCRKsHbQjChkT5xv6cA3uMR06oHODAI8Fq2x8FGecx40eqLHrHPCvTNHR7POxkgPoO8Z7LnA1QORlxxLZjafdr4qaIDEMfifkJORsoRMDSBXfxkQAXUzwxb2oXKZmk0u6YbFN7NZVjRDYDIg8yzMscZrN9igIsK2o7yuA+P4Y5Qggd9Yhq85eF9xAtudmE4MoDJ48WB8G+DZZlcH+UmHw1W9C2XZ98OCzsSiqlvF6kCQyqGMniVCrhAPyDvWTHl0hSS+1IStcn0APQ398t/PO73NgBmBrVdncxbVPbHvcbDVZvgEfbYF4yvN7kf+NAPHsvlPu18FZWfeSCAnn97IfrJr5wMOVuOMeDxvyGMQAcBIZMAWV0MAAlNOLq6FHoE87qqFIFiX0ht99IRrNdqUHuVEGsymEKVCyvgGZcBFGzNV19qm9rWjUGwld8NLtlJPvJSCWH/PzJvU+DynIdMMkjw/qqVHtfoMtwjoPGrhdvkduIJ77naB2CwLmAf5hnGxAIb4jEcBBew3kMOvkn+M3sZCr48RemZWt6xFq7iRA8XXTHvnwpDMxAs6KOwrk6QwTBZlK4qvTWfdlM3gJBb4LCzN9LyoPIaketAb2e4ANBloOZLoAy90E/MEP7scf9UiDaoaWE/+VDrVdgVOIG8M/up67l/efr3qhAAoVzKRxCzXOXxXvo7NgcwwhnE0w6h1ocP6KWOgqemUQH0Wpf78DifB3fXDbivBWup9IlHZkIabDlbrBL9X07ege1TYWw2ARwbDUy5Q5hQD0NgaxqsQF4nKyELyxTtO16Z+l0Kd4ZXLqWR3M/v2le8i0qw3gDhCLOMwQIFVhkPi6BgY3ziDDz4PAMxF595RoMDt7+ApWXlOBLxBqbIbwmKn8DYToS1f8xF+BWLGIjrSz1YMn/uQ05eopVgbivB0Ez3LkJ/bQYbmoJ3eQbAUqP9jtkYry+aCpeqHC5AETmbTf2YfYkGRm7MQKXtYGAYDFdumTBKeRjzyWCVsxDXcUsqPwC/2Y6ctUb8/Z5gFDfi9/qxO8xoOMh8NkBxHXR3O0P/qGclyDkrzvLxctSqZjmbtg5yfyrGQx/nXtR6NTz3w1+pHcs9Ypbl61mpDHDR6ZRbV2GlPpfapicdiGDjILActWBTJSr7iJwEYEPxr3InmnyUN+ZjXdU1Ks9Phpy1YVUehjAmrHcZfrMByvgxwDUFWRyNdx4ARVZJ3dV2DL99bXEoLxsXPp/Lbb/X/gDCdQJA1CGKanJkCIxFPwD0anLWL3kZ/pQBMIYCTHnM/klOnqISxId2Rb9UaEzWDfBU0rcgC3vV+WMJzYVVINXs0o4iGDW1JIRdyvc6A4TU2o9Jkd5vFeUV0Hav00F5NnMelHaViA8UijhBipxVt834PuFBv1Uaj3xNqWMGF69B3CsD65+nXWsJNtICoc4T7lwhOelW1G/TAMpmar1GRrkPheS9B1ClADkHiu7lih4OBrBcKGSBqGoxa7OoRQYGLJVapdYo1P6u0scWkpP1skC0TZ1O06iBRqH4rI9Hg+hHExjHDIy/BX2ttn2od81zcW0b8H689edO8g/2BwYhU0CPrSpH9Ed+xQGgGXTza134zHpYP4598PqYf7gYCl3IWDBmw2XcjBiCYgIya0EhBEmtGLeEkGao7eJES4sn6rKiUpL2JecwwVqDosjPPaF0m8AuGsA6UuJ9pcKqLAEl+E7lo44b3lOeUqFmb3Z3cUnk7+bCslfieYVQ1LTh/QsFqHu1U/9evltSgHjMAFIxsMQ+aMMmjZ3GtDGQRsByiQPr79QH7/EuYj0tGL84td6XSOIZo+BuHwLWqrfZMvRJK5lhEIoF8BkVzb8E/mu+Rn1jGt2VQk7ai8c0+m0JZDd1jkqBuQSUdCCsoOk5ZKCSZOgQ+UzLhYJmNLqsklSxq8O7nI8GODeLAa8SlsztviaXwQLYVMK9SIOOf4Q4zkYRJ1DrptJCIZLCaqp+Ue/F62d4qnycYEFyWf8afD+I2iYwTwvBkwcGxgQ7yQiwUMspuC1PUHYFMbs8O3u4ISrAz5Z0ARR/P8TDSoSwK9dC9fVHYIP8XJ6BG6EpbUootXIF1YkcU8h7ZvM8BNQr8fteuL43OVP2qp9TcNlKyVnFbYm+SFDrHN6kAYLK0aOyK5aJ38a13zB7WwwQnSD+nhH3SWsucx45izPlmjuLWqfPiYE18+THPYg/nQY3VAJZnvh9DYzyhXi3uJCvmJBRIvNm3S1MKObhV8Y0q0jiIWmDJdaVO2MQeh2ULO0aHVjytFiFEuaEAUQk6Mm/ZQQAxsX/E5r1UfdMacroFgtJCgWMC8GPae2IG8CtXcxVa4O8v24ESHufcYiFDEY7a8hJE5FHzsZTy8UKx7XPcjuFnFmJgTGoVfX9YC1N/aPuuxpVKs5osKnh5ExtV4EVroebpGZKByOmEjMoPD/7JnLOK9OZkJp8GY5Zp1MESDWjz1YhEJ4HhlmGtlQCgDYAqHriN9LQxg3jLuUjIRhjC+6vJiTiYHg8Tu+jT+sBpuOEHqZFTCetGfxkABBScr+cWm+JGoJqGRiWhXf7UGuniQR45qCS7liMgp0z3l2lu59PBgodlY7vXwrRt0HGYTCstdrw2YzQQloEzcuh+L3xvdqPVo778yzQU1tJH8Wpc9fKxTXC0Om6q0AoZnCtKFK0qGzjgGYBWJ4Cy9kI0BmsuQumhPXMgHi6/Ay4xFHpHDCKJV2sfCbqr6hs40UJO7s5PMt1DdydWnIWF8olBYX4dwCA6SnE01ZFXdnhY9LKVVMxIfII8kYlKtu69eV4DW9Y5aRqHDzmyQO14FClwVVxQ95H94+tyP36spYtMVsdhCIGFJUvc2GXjGff+pKzbIAnPXjjKc9+qmOvo9L5hmELGwqyWDEqUYlKVDoDiP4X/olHfRGVqESlG4uVpGB7gqISlahEpaPdsf+VJAVba8E5Uz4lZ8cs53/hJE3tSZDO+3Z46pNXHu8J//y5HBrEbI4XdvG0qjo1oGUbGAjeuc6bTz/q5Ofwamme4VnSTe3kceFd/WrWiRf48ZqdByLD51u4z3jFOW+X4riV2lDbHYXXTPH+Ud6TyIsfj4FMeZ2NxyvxOVEar/KuMgGQUuAghYVoFwEiXsnT/QrnflGHKfLKU5kf57CA9+CVrzLFBy+z5y0lV1C4RE7dWbgPenbRc3p3c1t59om3nfDK4u9TsOOev+qFU8RwJkle8c2rvHlv1l5d+HzOvHCw+KxSqCgA4ZXUJT734L9zKtthJjdMMqEg1qgGVpt3Fh+PfxnZjqTWyblVrpMPYYG/hZe+lZzjbqrJmR5VaRq48F4sTsD1d8pmylsE4OOpVZ5W5eNGHqLsYjM+eI0zv81B5QPvvgl2NRYNVOlKeUZkBNjWoXh/nhnhvUqmEyeZnZ2MDlR5bbhwHuZx5CRY5w2OCwUoctbDQbD0vBVCnTJwHACzBe+qUoAuJuc4694A0iH4XmXy402EO+P3/8Q9z4WFnIPvpNBOwfMvF328mJwZHxbifPHeEwEMyyDs3LY9YBxYeHijLp8fxov1ZgtLxqeg8t4l3k7AWyK8Tj9R7SYxzj8Tcjce92uCEakUxorTz34D1vYe9DF/xwncXhOG7HRyjpvh43lUzqSTyNmEOovapoYltPtsyBXf9x18fyr6503IL4Fxj4E8TYInILNBfgM6kY97qfO29oDhZqP/N4CLV+G2XEbZHEMveTDcwzAenAJXpdAYB8NzBNpyN76bBj1ieX4U1+4AljUG4/AEmA4b8psx/rMwfqu1d1kkdLcYeroHPKabyTlxpYncE8xtCUzHyf9YkEpYrzOBjopa8YNVVn52q3iDZy9BJV+AgM+Bourle+Qcp6JSQHxCTsa4vnAlHqbshsKJ5OQ3qQfgqd3Lx0IZyql1drc/oXMmAqzuhFC7HYrYF0DxNGUP6lMnjRwDUPoUwsVnoalkZ9djIF+AsE0WIM/P4Y2WPP17Azl7rq7AexEEtgzgWyQY6kwow3wI0UL051r8RmUh5D7khGDPQ6hmi/bw5sKp+D/nYD5L/O1cwT6PgwD+E/24CO14Bvf+Dq67CsrG/dhIwTIhqvI83Pi3hYG4GgZiBdqvWNscPHcB3vFVtPtttK8M1/UAoN0PED4H358OBn87gNbEBneEMVkHoC0Qvz2SnJ30v8b3bNFfxrjxO/Nq6l+J+w3Ee74F5U3iu9lo++vknaSORLhjoQcAKfCdT85pukrvfgs5XSaM7CDo1UMwUJMEaXgeesfjzAs6D0J/tsB4rcfn7SGzRUJ+D8T/1ZmCf4FOXCQAaAW13bjbKvyTFG6ZVzKmwYLlqFMPCEqqklpPBfKqkxj+CstbQk4eGj1NarVAzAUAhznk5Nd9Df8vQWeU4z0XQNnuw3VDQfdLoPAsHHvDUvUBc9oV4LYTeaegfA5WvhfAVwl7BixNZTE8CIr7OoTyCgjFa8KaphD72Anv3yD6eCM5G3PXQVhXaFayGiD6KpTtRPH8w2DhFgGgLoRAPYR3OghCXEnODvyN1PrE2LXkJEpPA+znoR4LVrECfbkPrPhGcvIR/yEEAE0DEFwovpuKsavGu3wdLOBNGLrZsM7MbHih4b2CTe0DQ7Ea71sOZqaUsQpt6g/lMpWT0Pd/NIDAEnLyTB8PGSpAf58lGIB0WR6E7KjTXYvxbyVY/dUBPY9e5GzojUHPdsOzr8L3Kq1tEZ5RBI+lBWTgJg3898I1awQg50FeFNObABBhuXkF/a5ONVHHiGeELCk9/QLXDccY9BQ6s4TMyfi3rJjW88d4BRfZBeNt/s+Ss53/HVj4/aGID4lANqPxAQC6zQE6vhTCXSao71wwgf7kHOxHABaZaCktEJnQgYeCsi7H39+HhZuCTt7H8A5DoYhTMVB6XuVPxf/Xi+ddA5fiAgjEC8LSzEU/xKjt6QmqDWfinWeDVRWSk1qhWgzoW1qb88X/KzTwMp3nldRASCYxU+lYlUX/r3DjJDu7EVb+bIz30ABj2w8g8mNNCctx37EA1ItEoH6NaFMPbRIkJSzyxej/HTBW9UJJeTw4z/WTMEJ66SlcYhknLcS99sYYXCr64WOt/2qE+zgPLCMP3/eGfpwIQ/46gM/P81iP/lDKejtk6gjhwj6MZyUBlJYAhHe1vp8LI9APMtgs9GWZ1q8Nmj6SkA83IDkP2LAjZLFO/H2j0CHTwZkxuf3eC4hSGLCHEO+ZK/72d1jMQnQMgaLWwMIwfR4tGlEoGFi+UORmjapOgZU5EwI1TATCYtq1SU1R/gJ/9pciNlMCxZpm8OVVORGW+CI8c4QAihYtcD6RnMx8TWjvGvxWKTPTeE6/cB3iFduL/pbvq3xwjmudAqulkmXliWuKRfvl7z8H4KmAIb+36Wy4amp9DNARgo3lC8VOoL8S4tl5QnGfAAtkNqqy6e3iEoDk390Fhvm+JmOfwlWaAXf/VgG6xVrbi7QgaZ2QtQsgkwkx6VEC0D4ZimlKVr+UWp+pp9LSboZyTgfIPS36pYc2+6NcuJ8L7+AVyHwz+rUGcnwpdKIAzxpP5hNM2LXcF0aT4ErJ3NUXo9+uwTjvoOmXzKY4CSThTMSCRoo25GnPLxJ9nhSEQOpYTIv3KSN6C1x0EpNNaYC4njZY7tJok1sk7uKSVYvOngaX4Wiwnfvhh8pDCW/CS83EwL0n/rZJoO1mcnLCVMDNugfC+DL84ruBphXknCb6HKzqXRiIlVBExYhWQfD6CVYyEsG+zyGoJhB6GMA3CxZ1pbB0NeiD63HfDSLguhvuPxoA+DKumw4ffCYG+3UMcjPeo07MPo7H53kieF0lBrpOm5GsEBbtXPQ5C+PucJFNZ8PdByCdjfvWiEDwZhHrU7lyTGM2FfGSCvSzAvnbYIF/ZWBB+0IQ70U/rEG851ZMBDwBQE/h+2aMX7NouzxlRQbCb4esLcW4/kOw8Z+LeNANLv2xJ5SzEu7m83C37sK4FgFU7gCLlEdc14k+m4lrZqId/0b/xiHTjWB+M9AXw2DAj9GYi5qwOQ5jOgm/HSkmd6ZjEmcmFPl9MVZV1Pq4oFfxuzvx/XLB/jZrHkWV8FoeRRzwNgB4JWQ2Y2DyM2FEjoEMLhQTLnNg3N8hl6PD3PIJ6RcXQWhTAm17CbpcBIGRAagR8DGfR+OSgkarYHIBBknGio4kJ4kT/+YoxCXepdbZ+fpjgBagg9SRI5aw7HHhfiTwPjuLAC+5UPTDAZxL0dEtUK6BYIMjAcCKEV2BYG0KcY27MTO4FgxhLAJ/tSL+Vow+S5FzYoOKO2UM/aq3pwT/Twn6fDjcGTlrdgv88tki3qCOH1bMRB+LOJ5dj/4sElPt6oSOMrg5NWL2sIHanmGegPvch5xUsi2a0ToEExmLhLsj+ycfY18v3LMWAVIH495PaSxlX7iLLxncLlm+iXvOF0ysBIC7CfJSjXfIF++hzsRT8rsdYirzIZNKXsdALj4WDDUfcrRSc5FlUWfdl2MM3xM6NhIAOh/goOS0iJx0vbIvjoAn8D45yc9UHutGIW8ZwVyGow8fR5uLRdvnAqgeFXpQBvmVKWtHwVg1kEuaIBMIRcVcrsSgnKJ9Pw7W4h6A8iQIw0UewtUVpR/clKmYxXw9GsKodEBhoD0fRvcEcjJo+pWE5nVZ+uyYHxCFzXj3ZSwPkXlx1hLQzRPBMt7ATEF391UGDPScCICi0sFyVQFXNygAxQzgswVr/l+AAQCVImxLROCW8wAAAABJRU5ErkJggg=="
                                                alt="alt text" height="65" width="292" border="0" style="display: block;">
                                        </a>
                                    </td>
                                    <td valign="middle" style="padding-right: 40px;text-align: right;  background-color: #123C76; padding: 15px;">
                                        <table class="contacts">
                                            <tr>
                                                <td class="image" style="padding: 0 10px 0 0;">
                                                    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAh1JREFUeNrElz9Lw0AYxtOrOIh0sPgBitDBP5Mu6uKgFhGR6uZHEBRHP4CjFMEqCI52clNw0IJYBBF0l4IgUqUoteoo2PicPJEY7y6pEfPCj8LlyT255u5938Rs27YCRByMgjEwALpAgtdewTW4AEegCN79Joz5GMvJ58ESeAP74AyUQY2aJEiDQTAFWsEa2ORDqUMaa8iCO1AC40AYtA6C2hLvzeq0qsE4yIE6mAtgpmOOc+Q4p9FYCgrgCqRCmDqkOFfBa+4V5ijs/ANTh07OmdMZZ/nXuFfaA07BA9gGbSFWXne/c+dCgpvB/U77wKP9PbZCvvM7en0ZL3MnuoWX9s94Vm2UJijRy3I2VJXHwC2q2eoYCmE8Tq+4YEZ6Y8Zxx5Pm6Pdbv48ivUYF06DMSA2P6EZzcyWEcYNeY4K590whulSM7ZEwIb0GBBN+WSE4VIxtBCkAPiG9ugQLQU0hOAFVz9i8FT6kV0IYBHJl656xaTDxB+aWYOlKaq7nFaveAb0hPKXXq2ART2tE8qEWPGMdoAQmPY3CDFgEw6DFYJz+9MRhXgUbPgc/r0kmeyDDX3e8ML2qcrv0WrV4461PoZfZbdduPrYVjYL0yphSpso836RxRZcyTUVCxyyrTJA4NhUJXVk00c4JKgZTWVK7/cqirhEI0tyNgBVwAMrgHpyD3iCNQKStT6TNXmTt7b819JF9wsSi+mj7EGAAsp8fS8m28BoAAAAASUVORK5CYII="
                                                        alt="" height="30" width="30">
                                                </td>
                                                <td style="padding: 0;">
                                                    <table class="links">
                                                        <tr>
                                                            <td style="padding:0;">
                                                                <a href="tel:<?php echo WebApp::l("+371 200 22 881", ['lang' => $lang])?>" style=" color: #ffffff; text-decoration: none; font-size: 18px; font-weight: 700; white-space: nowrap;"><?php echo WebApp::l("+371 200 22 881", ['lang' => $lang])?></a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding:0;">
                                                                <a href="mailto:<?php echo WebApp::l("info@ledakcijas.lv", ['lang' => $lang])?>" style=" color: #ffffff; text-decoration: none; white-space: nowrap;"><?php echo WebApp::l("info@ledakcijas.lv", ['lang' => $lang])?></a>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>

                            <!-- Content : BEGIN -->
                            <table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
                                <tr>
                                    <td style="padding: 40px;">
                                        <?php echo $content ?>
                                    </td>
                                </tr>
                            </table>
                            <!-- Content : END -->
                        </td>
                    </tr>

                    <!-- Footer : BEGIN -->
                    <tr>
                        <td style="text-align:center;line-height: 30px; padding:15px;">
                            <b style="display: inline-block; vertical-align:middle;"><?php echo WebApp::l("Seko mums", ['lang' => $lang])?>:</b>
                            <a href="https://www.facebook.com/ledakcijas/" style="display: inline-block; vertical-align:middle;margin-left:5px;height:30px;">
                                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB8AAAAfCAYAAAAfrhY5AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAhhJREFUeNrEl7FLAlEcx8/rCsohuEyIIBqahEBpEUoIBxEKBIm2oqkhaHEo/Aukxf4EB6tRcBMHhxxy08mhrUAEMYlEgijq+4Pfwes49Z2e+oUPB/fu/b7vns/3vufS964USekgAkLAB7zADXqgBeqgDIpv5ZuOTEGXhHkAJEEMLEjU/AJ5kMIgqqOae0AanCijKwsSGETbqlHt0ykIamMaK9y/thK6DsqaR0EJrCvOiOqUMIDoMHMaYQ4sKs6K6uXMMyD+5h6eatk3boI78Ay+hfuZAX0awG+sAU1oSNswfgSHoItC/xrwdpkhPwH5nIrTHrCxuH64c3fURYgBBkTzpI3OT+BlzDWQNKZd5w1EVq/mG3iTS1x2bNSIoY+u8ZYps3P9snHTom0b7AvPuYbUIr+Ixnu1jD7AplUDFt05z4DK2+ucRL2QyoeEU1qVNCb5VD6dnNKajWe9Kh+LszB3a3wey2gZvIMHcGFa7fe4HIB5G+Y9lYOAYmMAS1ZvMaCtn1oqJ5BZqK5y9JmFymRe5P/mNEV+RTLvcOaapvIUMo2DJTVl85R4qlU57E1DWSPVimEiAcISgWILnAn/cbpsSBo32Ecxm1O0OeLwOCjD7TJ29Un1xRhtDpAVEOcHnRTVi8O4Miw6F3j6Gw4ZU50wjAuyHw00Qr8DizDLabVi1agN6NjmoHg7qW81TaIYFTiexFfqnwADAC3ojolo4DoqAAAAAElFTkSuQmCC"
                                    alt="" width="30" height="30">
                            </a>
                            <a href="https://twitter.com/ledakcijas" style="display: inline-block; vertical-align:middle;margin-left:5px;height:30px;">
                                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAppJREFUeNq0l09oE0EUhzfLShVFxXhQvBRRREUa0V5KSlGQXqRgqQdFerGINyG2DcWztJuWgOBBinqoB3sQRaVgDx5CAiJFaRFTFcSevMWbIATUb+StLMvs7GyaPPjYZHZmfvPnzZu3mT35ScfScjAIvdANWSlvwAaswHKj6q/adJZJEN4Co1CAY5YDrEMZFhhEM66Sa+ggD2twP4WoI3VVm7VsfzGfVlgtQwWOOq2baltBfNJWeBb8hNWwNdWHj/hskrAa3bjTfhuPztyN7Om00zmbDu+5G/Le+RaX9w8swW1YhKZh2ecRV1qOJ4WjLTrSLzjPsXkdFNCx8uo78ApmQhqBwymtB8EMC5pOr8HPBGE/LPovmlT9OpxTwQS+atoUgunnYs7pBzgNbw3CL3SFzHobj+dwRHfOeZ9zJQzqbAo+Qx9cgjeaOl0xbVX5QcOABz2JvTobgsdwXZxmUWL0WTgBB2BHTNtswhb1etKZzr7ASXgJN+C9XAYPLZzuUML7bs8wundwucUzO5DwPms6t2pfH8U4SHyP/UXV50WbWNowvL8Cn+C7DCJnoT1ksdQNT/btlKHSPXgKVQkYptlu5VGyGNyGK5mDyc7ATtgLu2G7oe5dOGwhvOJJhJkxVFJ7/ER+f4NhWI3MNCMXzFVLV1gOUp+PFlnGMxiDH5ozq7ZjxDY1IqQeD7y6bLgEliS6DUdE98EtiW4jKRy/HL6dFuCm5oZSS7gLLsjZVP/3Q494eCbl+V4Xrf/CTbmNKpE7uUsShHwbEoHfSiPIPMMiNbkYOmVTiNbici51Buc6IDqHaCkpy5yAoixNO5a3iOiEbV5dEmda34SoajsQnanNl0RNvHdMPktsrS5tesJ7mvbbqWMfbX8FGAAspaE2dQD1WgAAAABJRU5ErkJggg=="
                                    alt="" width="30" height="30">
                            </a>
                            <a href="https://www.youtube.com/channel/UCbpaCBf56Vo3EEXdIXyYyCw" style="display: inline-block; vertical-align:middle;margin-left:5px;height:30px;">
                                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAoJJREFUeNq0lz9oVEEQxt89DlQwiTGHYrD0kEA4SGcRq+M0ItrEP1wlqSwFC7ugkCYBQ5IqZUh1mEQsLE4NVjZ2QggceCIixwkhEr1rBCHJt/K9sFl39+2+bAZ+xfJ2Z+bN7szs5s6OPokySAEUQQ/HXdAE276K8o7zYlAGVVABFw3zWmAd1MB7sOuiOE3GwSZ4ByYsxiN+m+DcTa7N7MA5UAdrYCjDNg1xbZ26vBwogU9gLDq6jFFXydUBMfEDGIzCySB1ltIcSMLeG4WXXt12qA4sB/5zXSSWTWk4rtlzkVIfmd+/wA5z3iaiNpwB/awXV5i68pkQtl7KDohITCmK7oOVQH9+F7wAOY6FrVeiTiRbUFZS7XVA40JWqVNO0bJ8BqrKgjcGRefBhYxOvFXGVdmBivKxaSkuX8EM99dHVJ2VxIGCprzamspJIDrYF/AMnHZ04KembBdidjVV9hwU9oGn4Bt4DE6lzNc1pmIstVRZfnuEdgDMMiIPLR1Wp7MnDnjSTzAqJgf2TPeBriG8rtIBc2CexcpHunnDiXeJzB+wAJ473oR0P9XMc3FLyYQBi6K/YBFMgx8ef9unuT1tx1LNP3Q6DUo+g0vgkadxIZc1feYg1DXl43WDkjb4nvGQ3lTGNdkBcYFsSB9vsYGEknvgtjRu0OZByogiMck7XMSutaK04x2ecpd23M+WrGvHEW39K0w55V1QD3QPtIlodDdM6faA+3xc0qYNY75v0bvOMRjvUPdWWsHZAFcDR6JNnRuuFU9MHLFcTHz3fERnPK3kJttxR0lRV2lw7X9h96354vY6DK6BJZbQyPI4XeLc4eTmG+J1vMuasB76eb4vwAD3WoQOItxwFgAAAABJRU5ErkJggg=="
                                    alt="" width="30" height="30">
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td style="background-color:#2D2D2D">
                            <table width="100%">
                                <tr>
                                    <td style="padding:25px 40px; width:50%;">
                                        <span style="color:#FFFFFF; font-size:18px; font-weight:700;"><?php echo WebApp::l("Kontakti", ['lang' => $lang])?></span>
                                        <table style="margin-top:10px;">
                                            <tr>
                                                <td style="padding:5px;">
                                                    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAMxJREFUeNpi+P//PwMQJwPxcyC+D8RBUDGiMIhI+Y8KfgKxOCkGvP+PCeKJNYCJgYGBkQETODMQCUAGvMAiLkSKAVewiM8mxYADaGJbgHgjsQaAAkISiP8gBeAXIBYhJRZAeDlaLMwn1QBVaPwjgzJSDADhcizpYR6SdzyAeD1ULBYmjmwAIxCvwmIIKEzWAvFfNPHvQByI7iQ2HIbgAvex+QvkkgosYYINvMAXQKCAXYEWxegghZiokgLiPCBeB8S3gPgDFKeC5AECDAB6O5DmUgABcQAAAABJRU5ErkJggg=="
                                                        alt="">
                                                </td>
                                                <td style="padding:5px;">
                                                    <a href="tel:<?php echo WebApp::l("+371 200 22 881", ['lang' => $lang])?>" style="color: #b2b2b2; text-decoration: none;"><?php echo WebApp::l("+371 200 22 881", ['lang' => $lang])?></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding:5px;">
                                                    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAN9JREFUeNpi/P//PwMlgImBQgAyIBqI3wPxfxLxe7BeoBc+/IeAG/+JBzeh9AcGKOMnENsC8WIiNC+Bqv0F4jAgSXwDYn8gbsOjuR2I/aBqwYDxP2o0/APiXCg9FSmQcYljxAITVIECEAcA8XcoDgZiCSCejq6HBUvMgGx5CKXdoBokgfgGEP8GYlYU1Wh+BAVMOBA3QPlHgPgAlD0R3f/YAtEDiCfjCcQFQOwCxB/RDfgDxHZAvIyIaFwHxNbQqP+PnJBukZCQQInuHywhRQPx+/+kA5CeaMYBz40AAQYAK7MXeS0SJJYAAAAASUVORK5CYII="
                                                        alt="">
                                                </td>
                                                <td style="padding:5px;">
                                                    <a href="mailto:<?php echo WebApp::l("info@ledakcijas.lv", ['lang' => $lang])?>" style="color: #b2b2b2; text-decoration: none;"><?php echo WebApp::l("info@ledakcijas.lv", ['lang' => $lang])?></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding:5px;">
                                                    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAANRJREFUeNpi+P//PwMaVgPiRUD85j8EvIHy1bCoZUAX8Abib/+xg29QeZwGyAPxl//4wReoOrg+JgYEKANibiT+BiBOBuKFSGIg+VIGZIBk2j0km7agOXUOktw9XC5QQGLvZkAFe3CoQzHgPxLbBM0AYxzqULxwFS3AeoHYCYgbgPgvkvhVXLHQ+J840IDLAD4gfkpA81OoOrwJCR/wIpQSQXg2Ds2ziUnKMK88QNN8H93p+AwAYUc0AxxxqMNpAAhPgmqeiEcNXgO4gHgblMapDiDAAMFpxVvRWMy0AAAAAElFTkSuQmCC"
                                                        alt="">
                                                </td>
                                                <td style="padding:5px;">
                                                    <span style="color: #b2b2b2"><?php echo WebApp::l("Katlakalna iela 10, Rīga, LV-1073", ['lang' => $lang])?></span>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="padding:25px 40px; width:50%;">
                                        <span style="color:#FFFFFF; font-size:18px; font-weight:700;"><?php echo WebApp::l("Darba laiks", ['lang' => $lang])?></span>
                                        <table style="margin-top:10px;">
                                            <tr>
                                                <td style="padding:5px;">
                                                    <span style="color: #b2b2b2"><?php echo WebApp::l("P.,O.,T.,P.: 9:00-18:00", ['lang' => $lang])?></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding:5px;">
                                                    <span style="color: #b2b2b2"><?php echo WebApp::l("Cet.: 9:00-19:00", ['lang' => $lang])?></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding:5px;">
                                                    <span style="color: #b2b2b2"><?php echo WebApp::l("Se.: 9:00-15:00", ['lang' => $lang])?></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding:5px;">
                                                    <span style="color: #b2b2b2"><?php echo WebApp::l("Sv.: Nestrādājam", ['lang' => $lang])?></span>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <!-- Footer : END -->
                    <?php if(isset($unsubscribeurl)){ ?>
                    <tr>
                        <td colspan="2" class="hat" style="text-align: center; font-size: 14px; background-color: #ffffff; padding: 10px;">
                            <p style="margin:0"><?php echo WebApp::l("Nevēlies vairāk saņemt e-pastus?", ['lang' => $lang])?>
                                <a href="<?php echo $unsubscribeurl?>" style="font-weight: 700; color: #1d3c72; text-decoration: none;"><?php echo WebApp::l("Spied šeit!", ['lang' => $lang])?></a>
                            </p>
                        </td>
                    </tr>
                    <?php } ?>
                </table>

                <!-- End of Outlook-specific wrapper : BEGIN -->
                <!--[if (gte mso 9)|(IE)]>
                    </td>
                    </tr>
                </table>
                <![endif]-->
                <!-- End of Outlook-specific wrapper : END -->

            </td>
        </tr>

    </table>
</body>

</html>