<?php

use Constructor\Url;

?>
<ul>
    <?php foreach($pages as $page){ ?>
    <li>
        <a href="<?php echo Url::get("page", ['page' => $page])?>">&raquo; <?php echo $page['title']?></a>
    </li>
    <?php } ?>
</ul>