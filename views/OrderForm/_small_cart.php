<?php

use Constructor\WebApp;
use Constructor\Url;

$lang = WebApp::$app->getLanguage();

$sum = 0;

?>
<aside class="col d-flex justify-content-end align-items-start">
    <div id="form-small-cart">
        <div class="row">
            <div class="col">
                <div class="header">
                    <span><?php echo WebApp::l("Mans grozs")?></span>
                </div>
            </div>
        </div>
        <div class="small-cart">
            <?php foreach($items as $item){ ?>
            <?php
              $picture = $item['product']['picture'] ? $item['product']['picture'] : "/images/html/blank_product.png";
              $sum += $item['price'] * $item['count'];
            ?>
            <div class="row no-gutters prod" data-key="<?php echo $item['key']?>">
                <div class="col image">
                    <a href="<?php echo Url::get("product", ['product' => $item['product']])?>">
                        <img src="<?php echo getThumbUrl($picture, 55, 51, 6)?>" alt="">
                    </a>
                </div>
                <div class="col">
                    <a href="<?php echo Url::get("product", ['product' => $item['product']])?>">
                        <span class="name"><?php echo $item['name']?></span>
                        <div class="price-wrap">
                            <?php if($item['price'] != $item['base_price']){ ?>
                            <span class="new-price">&euro;
                                <span class="value"><?php echo number_format($item['price'], 2)?></span>
                            </span>
                            <?php } ?>
                            <span class="price">&euro;
                                <span class="value"><?php echo number_format($item['base_price'], 2)?></span>
                            </span>
                        </div>
                    </a>
                </div>
                <div class="col delete">
                    <a href="<?php echo Url::get("/cart/remove", ['key' => $item['key']])?>" class="delete"></a>
                    <span class="count">
                        <span class="value"><?php echo number_format($item['count'])?></span> <?php echo  WebApp::$app->l('gab.') ?>
                    </span>
                </div>
            </div>
            <?php } ?>
        </div>
        <div class="row summary">
            <div class="col-12">
                <div class="row top">
                    <div class="col-12 text-right">
                        <div class="sum">
                            <label><?php echo WebApp::l("Preču kopsumma")?>: </label>
                            <div class="price-wrap">
                                <span class="price">&euro;
                                    <span class="value"><?php echo number_format($sum, 2)?></span>
                                </span>
                            </div>
                        </div>
                        <div class="sum_discount" style="display:<?php echo $sum_discount_data['sum'] ? 'block' : 'none'?>">
                            <label><?php echo WebApp::l("Apjoma atlaide")?>: </label>
                            <div class="price-wrap">
                                <span class="price">&euro;
                                    <span class="value"><?php echo number_format($sum_discount_data['sum'], 2)?></span>
                                </span>
                            </div>
                        </div>
                        <div class="delivery">
                            <label><?php echo WebApp::l("Piegāde")?>: </label>
                            <div class="price-wrap">
                                <span class="price">&euro;
                                    <span class="value"><?php echo number_format($delivery_price, 2)?></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <hr>
                    </div>
                </div>
                <div class="row bottom">
                    <div class="col-12 text-right">
                        <div class="total">
                            <label><?php echo WebApp::l("Summa apmaksai")?>:</label>
                            <div class="price-wrap">
                                <span class="price">&euro;
                                    <span class="value"><?php echo number_format($sum + $delivery_price - $sum_discount_data['sum'], 2)?></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</aside>