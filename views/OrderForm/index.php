<?php

use Constructor\WebApp;
use Constructor\Url;

$omniva_cities = array_keys($omniva_data);
ksort($omniva_cities, SORT_LOCALE_STRING);

$omniva_addreses = $fields['omniva_city']['value'] ? $omniva_data[$fields['omniva_city']['value']] : [];
asort($omniva_addreses, SORT_LOCALE_STRING);

$pastastacija_cities = array_keys($pastastacija_data);
asort($pastastacija_cities, SORT_LOCALE_STRING);

$pastastacija_addreses = $fields['pastastacija_city']['value'] ? $pastastacija_data[$fields['pastastacija_city']['value']] : [];
asort($pastastacija_addreses, SORT_LOCALE_STRING);

$dpd_cities = array_keys($dpd_data);
asort($dpd_cities, SORT_LOCALE_STRING);

$dpd_addreses = $fields['dpd_city']['value'] ? $dpd_data[$fields['dpd_city']['value']] : [];
asort($dpd_addreses, SORT_LOCALE_STRING);

$sum = 0;
foreach($items as $item){
    $sum += $item['price'] * $item['count'];
}

$active_marker = false;
if($fields['delivery_method']['value'] == 'pakomats' && $fields['pakomats_type']['value']){

    $type = $fields['pakomats_type']['value'];

    if($fields[$type.'_address']['value']){

        foreach($map_markers as $marker){

            if($marker['customProperties']['providerID'] == $type && $marker['customProperties']['ppointID'] == $fields[$type.'_address']['value']){
                $active_marker = $marker;
                break;
            }

        }

    }

}

?>
<form action="" method="POST" id="OrderForm">
    <?php if($errors){ ?>
    <ul class='errors'>
        <?php foreach($errors as $field => $field_errors){ ?>
        <li>
            <?php echo implode("</li><li>", $field_errors)?>
        </li>
        <?php } ?>
    </ul>
    <?php } ?>
    <div class="row">
        <div class="col-12 col-md-7 col-lg-7">
            <section class="customer-info order-form-section">
                <div class="d-flex justify-content-between align-items-center">
                    <h3>2. <?php echo WebApp::l("Pasūtītāja informācija")?></h3>
                    <?php if(!WebApp::$app->user->get() && false){ ?>
                    <a href="#" class="btn blue outline"><?php echo WebApp::l("Ieiet sistēmā")?> ????</a>
                    <?php } ?>
                </div>
                <div class="row">
                    <div class="col-12">
                        <?php foreach($fields['payer']['values'] as $key => $payer){ ?>
                        <label for="f-payer-<?php echo $key?>" class="cb">
                            <input type="radio" name="payer" id="f-payer-<?php echo $key?>" value="<?php echo $key?>" <?php echo ($key == $fields['payer']['value']) ? 'checked' : '' ?>>
                            <span><?php echo $payer?></span>
                        </label>
                        <?php } ?>
                    </div>
                    <div class="col-12 related-fields" data-selector="payer" data-parent="legal">
                        <label for="f-company"><?php echo $fields['company_name']['label']?></label>
                        <input type="text" name="company_name" id="f-company" autocomplete="work" value="<?php echo htmlspecialchars($fields['company_name']['value'])?>">
                    </div>
                    <div class="col-12 related-fields" data-selector="payer" data-parent="legal">
                        <label for="f-registration_nr"><?php echo $fields['registration_nr']['label']?></label>
                        <input type="text" name="registration_nr" id="f-registration_nr" autocomplete="work" value="<?php echo htmlspecialchars($fields['registration_nr']['value'])?>">
                    </div>
                    <div class="col-12 related-fields" data-selector="payer" data-parent="legal">
                        <label for="f-vat_number"><?php echo $fields['vat_number']['label']?></label>
                        <input type="text" name="vat_number" id="f-vat_number" autocomplete="work" value="<?php echo htmlspecialchars($fields['vat_number']['value'])?>">
                    </div>
                    <div class="col-12 related-fields" data-selector="payer" data-parent="private">
                        <label for="f-name"><?php echo $fields['name']['label']?></label>
                        <input type="text" name="name" id="f-name" autocomplete="name" value="<?php echo htmlspecialchars($fields['name']['value'])?>">
                    </div>
                    <div class="col-12">
                        <label for="f-email"><?php echo $fields['email']['label']?></label>
                        <input type="text" name="email" id="f-email" autocomplete="email" value="<?php echo htmlspecialchars($fields['email']['value'])?>">
                    </div>
                    <div class="col-12">
                        <label for="f-phone"><?php echo $fields['phone']['label']?></label>
                        <input type="text" name="phone" id="f-phone" autocomplete="phone" value="<?php echo htmlspecialchars($fields['phone']['value'])?>">
                    </div>
                    <div class="col-12">
                        <br>
                    </div>
                    <?php if(!WebApp::$app->user->get() && false){ ?>
                    <div class="col-12">
                        <label for="f-register-me" class="cb">
                            <input type="checkbox" name="register-me" id="f-register-me" values="1">
                            <span><?php echo WebApp::l("Reģistrēt mani kā pastāvīgo klientu sistēmā")?></span>
                        </label>
                    </div>
                    <?php } ?>
                </div>
            </section>
            <section class="delivery-info order-form-section">
                <h3>3. <?php echo WebApp::l("Pasūtījuma saņemšana")?></h3>
                <div class="row">
                    <?php foreach($fields['delivery_method']['values'] as $key => $method){ ?>
                    <?php
                        if(!is_numeric($key)){
                            continue;
                        }
                    ?>
                    <div class="col-12">
                        <label class="cb">
                            <input type="radio" name="delivery_method" value="<?php echo $key?>" <?php echo $key == $fields['delivery_method']['value'] ? 'checked' : '' ?>>
                            <span><?php echo $method?></span>
                        </label>
                    </div>
                    <?php } ?>

                    <div class="col-12">
                        <label for="f-delivery-courier" class="cb">
                            <input type="radio" name="delivery_method" id="f-delivery-courier" value="courier" <?php echo $fields['delivery_method']['value'] == 'courier' ? 'checked' : ''?>>
                            <span><?php echo $fields['delivery_method']['values']['courier']?></span>
                        </label>
                        <div class="related-fields" data-selector="delivery_method" data-parent="courier">
                            <div class="row no-gutters">
                                <?php foreach(['city', 'zip', 'street', 'house_nr', 'flat_nr'] as $i => $field){ ?>
                                <div class="col-12">
                                    <label for="delivery-<?php echo $field?>"><?php echo $fields[$field]['label']?></label>
                                    <input type="text" name="<?php echo $field?>" id="delivery-<?php echo $field?>" autocomplete="delivery address-line<?php echo $i?>" value="<?php echo htmlspecialchars($fields[$field]['value'])?>">
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>

                    <?php # Pickup start ?>

                    <div class="col-12">
                        <label for="f-delivery-pakomats" class="cb">
                            <input type="radio" name="delivery_method" id="f-delivery-pakomats" value="pakomats" <?php echo $fields['delivery_method']['value'] == 'pakomats' ? 'checked' : ''?>>
                            <span>
                                <?php echo $fields['delivery_method']['values']['pakomats']?>
                            </span>
                        </label>
                        <div class="related-fields" data-selector="delivery_method" data-parent="pakomats">
                            <div id="package-delivery-wrap">
                                <nav>
                                    <div class="nav nav-tabs" role="tablist">
                                        <a class="nav-item nav-link active" id="tab-map-view" data-toggle="tab" href="#map-view" role="tab" aria-controls="map-view" aria-selected="false"><?php echo WebApp::l("Kartes skats")?></a>
                                        <a class="nav-item nav-link" id="tab-classic-view" data-toggle="tab" href="#classic-view" role="tab" aria-controls="classic-view" aria-selected="true"><?php echo WebApp::l("Klasiskais skats")?></a>
                                    </div>
                                </nav>
                                <div class="tab-content">
                                    <div class="tab-pane fade" id="classic-view" role="tabpanel" aria-labelledby="tab-classic-view">
                                        <div class="row no-gutters">

                                        <?php # Ominva ?>
                                        <div class="col-12">
                                            <label for="omniva" class="cb">
                                                <input type="radio" name="pakomats_type" id="omniva" value="omniva" <?php echo $fields['pakomats_type']['value'] == 'omniva' ? 'checked' : ''?>>
                                                <span><?php echo $fields['pakomats_type']['values']['omniva']?>
                                                </span>
                                            </label>
                                            <div class="related-fields" data-selector="pakomats_type" data-parent="omniva">
                                                <div class="row no-gutters">
                                                    <div class="col-12">
                                                        <select name="omniva_city" id="omniva_city" class="styled">
                                                            <option value="0" data-default="true"><?php echo WebApp::l("Izvēlieties pilsētu")?></option>
                                                            <?php foreach($omniva_cities as $city){ ?>
                                                            <option value='<?php echo htmlspecialchars($city)?>' <?php echo ($fields['omniva_city']['value'] && $city == $fields['omniva_city']['value'])? 'selected' : '' ?>><?php echo $city ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-12">
                                                        <select name="omniva_address" id="omniva_address" class="styled">
                                                            <option value="0" data-default="true"><?php echo WebApp::l("Izvēlieties pakomātu")?></option>
                                                            <?php foreach($omniva_addreses as $id => $addrese){ ?>
                                                            <option value='<?php echo $id?>' <?php echo ($fields['omniva_address']['value'] && $id == $fields['omniva_address']['value']) ? 'selected' : ''?>><?php echo $addrese?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php #Pastastacija ?>
                                        <div class="col-12">
                                            <label for="pastastacija" class="cb">
                                                <input type="radio" name="pakomats_type" id="pastastacija" value="pastastacija"  <?php echo $fields['pakomats_type']['value'] == 'pastastacija' ? 'checked' : ''?>>
                                                <span><?php echo $fields['pakomats_type']['values']['pastastacija']?>
                                                </span>
                                            </label>
                                            <div class="related-fields" data-selector="pakomats_type" data-parent="pastastacija">
                                                <div class="row no-gutters">
                                                    <div class="col-12">
                                                        <select name="pastastacija_city" id="pastastacija_city" class="styled">
                                                            <option value="0" data-default="true"><?php echo WebApp::l("Izvēlieties pilsētu")?></option>
                                                            <?php foreach($pastastacija_cities as $city){ ?>
                                                            <option value='<?php echo htmlspecialchars($city)?>' <?php echo ($fields['pastastacija_city']['value'] && $city == $fields['pastastacija_city']['value']) ? 'selected' : '' ?>><?php echo $city ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-12">
                                                        <select name="pastastacija_address" id="pastastacija_address" class="styled">
                                                            <option value="0" data-default="true"><?php echo WebApp::l("Izvēlieties pakomātu")?></option>
                                                            <?php foreach($pastastacija_addreses as $id => $addrese){ ?>
                                                            <option value='<?php echo $id?>' <?php echo ($fields['pastastacija_address']['value'] && $id == $fields['pastastacija_address']['value']) ? 'selected' : ''?>><?php echo $addrese?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php #DPD ?>
                                        <div class="col-12">
                                            <label for="dpd" class="cb">
                                                <input type="radio" name="pakomats_type" id="dpd" value="dpd" <?php echo $fields['pakomats_type']['value'] == 'dpd' ? 'checked' : ''?>>
                                                <span><?php echo $fields['pakomats_type']['values']['dpd']?>
                                                </span>
                                            </label>
                                            <div class="related-fields" data-selector="pakomats_type" data-parent="dpd">
                                                <div class="row no-gutters">
                                                    <div class="col-12">
                                                        <select name="dpd_city" id="dpd_city" class="styled">
                                                            <option value="0" data-default="true"><?php echo WebApp::l("Izvēlieties pilsētu")?></option>
                                                            <?php foreach($dpd_cities as $city){ ?>
                                                            <option value='<?php echo htmlspecialchars($city)?>' <?php echo ($fields['dpd_city']['value'] && $city == $fields['dpd_city']['value']) ? 'selected' : '' ?>><?php echo $city ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-12">
                                                        <select name="dpd_address" id="dpd_address" class="styled">
                                                            <option value="0" data-default="true"><?php echo WebApp::l("Izvēlieties pakomātu")?></option>
                                                            <?php foreach($dpd_addreses as $id => $addrese){ ?>
                                                            <option value='<?php echo $id?>' <?php echo ($fields['dpd_address']['value'] && $id == $fields['dpd_address']['value']) ? 'selected' : ''?>><?php echo $addrese?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php #CircleK ?>
                                        <div class="col-12">
                                            <label for="circlek" class="cb">
                                                <input type="radio" name="pakomats_type" id="circlek" value="circlek" <?php echo $fields['pakomats_type']['value'] == 'circlek' ? 'checked' : ''?>>
                                                <span><?php echo $fields['pakomats_type']['values']['circlek']?>
                                                </span>
                                            </label>
                                            <div class="related-fields" data-selector="pakomats_type" data-parent="circlek">
                                                <div class="row no-gutters">
                                                    <div class="col-12">
                                                        <select name="circlek_address" id="circlek_address" class="styled">
                                                            <option value="0" data-default="true"><?php echo WebApp::l("Izvēlieties adresi")?></option>
                                                            <?php foreach($fields['circlek_address']['values'] as $key => $value){ ?>
                                                            <option value='<?php echo htmlspecialchars($key)?>' <?php echo ($fields['circlek_address']['value'] && $key == $fields['circlek_address']['value']) ? 'selected' : '' ?>><?php echo $value ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php #Pasts ?>
                                        <div class="col-12">
                                            <label for="pasts" class="cb">
                                                <input type="radio" name="pakomats_type" id="pasts" value="pasts" <?php echo $fields['pakomats_type']['value'] == 'pasts' ? 'checked' : ''?>>
                                                <span><?php echo $fields['pakomats_type']['values']['pasts']?>
                                                </span>
                                            </label>
                                            <div class="related-fields" data-selector="pakomats_type" data-parent="pasts">
                                                <div class="row no-gutters">
                                                    <div class="col-12">
                                                        <select name="pasts_address" id="pasts_address" class="styled">
                                                            <option value="0" data-default="true"><?php echo WebApp::l("Izvēlieties pasta nodaļu")?></option>
                                                            <?php foreach($fields['pasts_address']['values'] as $key => $value){ ?>
                                                            <option value='<?php echo htmlspecialchars($key)?>' <?php echo ($fields['pasts_address']['value'] && $key == $fields['pasts_address']['value']) ? 'selected' : '' ?>><?php echo $value ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        </div>
                                    </div>
                                    <div class="tab-pane fade show active" id="map-view" role="tabpanel" aria-labelledby="tab-map-view">
                                        <div id="map-wrap">
                                            <div class="row autocomplete-wrap">
                                                <div class="col">
                                                    <div class="wrap">
                                                        <input id="autocomplete-location" placeholder="<?php echo WebApp::l("Ievadiet atrašanās vietas adresi")?>" type="text" class="" name="problem-address">
                                                        <a href="#" id="submit-autocomplete-location"></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col">
                                                    <div id="package-point-map"></div>
                                                </div>
                                            </div>
                                            <script>
                                                function initPackageMap(){
                                                    var map = document.getElementById('package-point-map');
                                                    var markers = <?php echo json_encode($map_markers)?>;

                                                    var mapZoom = 14;
                                                    window.onload = function () {
                                                        initLocationAutocomplete();

                                                        <?php if($active_marker){ ?>
                                                            var currentpos = new google.maps.LatLng(<?php echo json_encode($active_marker['coords']['lat'])?>, <?php echo json_encode($active_marker['coords']['lng'])?>);
                                                            initPackageGoogleMap(map, markers, mapZoom, currentpos);

                                                            for(var i in allMapMarkers){

                                                                var marker = allMapMarkers[i];
                                                                var properties = marker.customProperties;

                                                                if(properties.providerID == <?php echo json_encode($active_marker['customProperties']['providerID'])?> && properties.ppointID == <?php echo json_encode($active_marker['customProperties']['ppointID'])?>){

                                                                    google.maps.event.trigger(marker, 'click');
                                                                    break;

                                                                }

                                                            }

                                                        <?php }else{ ?>
                                                            navigator.geolocation.getCurrentPosition(currentSuccess, currentFail);
                                                            var currentpos = new google.maps.LatLng('56.967027', '24.142261');
                                                        <?php } ?>

                                                        function currentSuccess(position) {
                                                            currentpos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                                                            initPackageGoogleMap(map, markers, mapZoom, currentpos);
                                                        }

                                                        function currentFail(position) {
                                                            initPackageGoogleMap(map, markers, mapZoom, currentpos);
                                                        }
                                                    };
                                                }

                                            </script>
                                            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBXwqlcoMGDzAShiQ17Bv_8TdTHJ7zOl48&libraries=places,geometry, geocode&callback=initPackageMap" async defer></script>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php # Pickup end ?>
                </div>

            </section>
            <section class="payment-info order-form-section">
                <h3>4. <?php echo WebApp::l("Apmaksa")?></h3>
                <div class="row">
                    <?php foreach($fields['payment_method']['values'] as $key => $value){ ?>
                    <div class="col-12">
                        <label for="payment-<?php echo $key?>" class="cb">
                            <input type="radio" name="payment_method" id="payment-<?php echo $key?>" value="<?php echo $key?>" <?php echo ($key == $fields['payment_method']['value'] ? 'checked' : '')?> <?php echo ($key == 'in_shop' && in_array($fields['delivery_method']['value'], ['courier', 'pakomats']) ? 'disabled' : '')?>>
                            <span><?php echo $value ?></span>
                        </label>
                    </div>
                    <?php } ?>
                    <div class="col-12">
                        <div class="paymethod-img">
                            <img src="/images/html/cards-color.svg" alt="">
                            <img src="/images/html/swedbank-color.png" alt="">
                        </div>
                    </div>
                </div>
            </section>
            <?php if(!$backend){ ?>
            <section class="order-form-section">
                <div class="row">
                    <div class="col-12">
                        <label for="subscribe" class="cb">
                            <input type="checkbox" name="subscribe" id="subscribe" <?php echo isset($post['subscribe']) ? 'checked' : ''?>>
                            <span><?php echo WebApp::l("Vēlos reģistrēties jaunumu saņemšanai savā e-pastā")?></span>
                        </label>
                        <label for="agree-tos" class="cb">
                            <input type="checkbox" name="agree-tos" id="agree-tos" required>
                            <span>
                                <?php echo WebApp::l('Piekrītu internetveikala "LEDakcijas" [link]', ['replace' => ['[link]' => '<a href='.Url::get('tos').'>'.WebApp::l("lietošanas noteikumiem").'</a>']])?>
                            </span>
                        </label>
                    </div>
                </div>
            </section>
            <?php } ?>
        </div>
        <?php
            if(!$backend){
                echo $this->render('_small_cart', [
                    'cart' => $cart,
                    'sum_discount_data' => $sum_discount_data,
                    'delivery_price' => $delivery_price,
                    'items' => $items
                ]);
            }
        ?>
    </div>
    <?php if(!$backend){ ?>
    <div class="row summary">
        <div class="col-12">
            <div class="row top">
                <div class="col-12 col-lg-7 text-right">
                    <div class="sum">
                        <label><?php echo WebApp::l("Preču kopsumma")?>: </label>
                        <div class="price-wrap">
                            <span class="price">&euro;
                                <span class="value"><?php echo number_format($sum, 2)?></span>
                            </span>
                        </div>
                    </div>
                    <div class="sum_discount" style="display:<?php echo $sum_discount_data['sum'] ? 'block' : 'none'?>">
                        <label><?php echo WebApp::l("Apjoma atlaide")?>: </label>
                        <div class="price-wrap">
                            <span class="price">&euro;
                                <span class="value"><?php echo number_format($sum_discount_data['sum'], 2)?></span>
                            </span>
                        </div>
                    </div>
                    <div class="delivery">
                        <label><?php echo WebApp::l("Piegāde")?>: </label>
                        <div class="price-wrap">
                            <span class="price">&euro;
                                <span class="value"><?php echo number_format($delivery_price, 2)?></span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-lg-9">
                    <hr>
                </div>
            </div>

            <div class="row bottom">
                <div class="col-12 col-lg-7 text-right">
                    <div class="total">
                        <label><?php echo WebApp::l("Summa apmaksai")?>:</label>
                        <div class="price-wrap">
                            <span class="price">&euro;
                                <span class="value"><?php echo number_format($sum + $delivery_price - $sum_discount_data['sum'], 2)?></span>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-7 text-right">
                    <a href="<?php echo Url::get("/") ?>" class="btn blue outline"><?php echo WebApp::l("Turpināt iepirkties")?></a>
                    <input type="submit" class="btn orange" value="<?php echo WebApp::l("Pasūtīt")?>" name='checkout'>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
</form>
<script>
    var omniva_data = <?php echo json_encode($omniva_data)?>;
    var pastastacija_data = <?php echo json_encode($pastastacija_data)?>;
    var dpd_data = <?php echo json_encode($dpd_data)?>;

    var multi_level_pack_data = <?php echo json_encode(['omniva' => $omniva_data, 'pastastacija' => $pastastacija_data, 'dpd' => $dpd_data])?>;

    var delivery_prices = <?php echo json_encode($delivery_prices)?>;
    var delivery_discounts = <?php echo json_encode($delivery_discounts)?>;
    var product_sum = <?php echo json_encode((float)$cart->getSum())?>;
    var delivery_price = <?php echo json_encode((float)$delivery_price)?>;
    var sum_discounts = <?php echo json_encode($sum_discounts)?>;
</script>