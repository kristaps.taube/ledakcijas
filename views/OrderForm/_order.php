<?php

use Constructor\WebApp;

$months = explode(',', WebApp::l("janvāris,februāris,marts,aprīlis,maijs,jūnijs,jūlijs,augusts,septembris,oktobris,novembris,decembris", ['lang' => $lang]));

if($order){

    $time = strtotime($order['datums']);
    $date = date('Y', $time).'. '.WebApp::l("gada", ['lang' => $lang]).' '.date('d', $time).'. '.$months[date('m', $time)-1];

    $order_status_row = \EVeikalsOrderStatuses::getInstance()->GetById($order['status_id']);

}else{
    $date = date('Y').'. '.WebApp::l("gada", ['lang' => $lang]).' '.date('d').'. '.$months[date('m')-1];
    $order_status_row = \EVeikalsOrderStatuses::getInstance()->getNewStatusRow();
}

$payer = $fields['payer'];

# SUMS
$sum_wo_discount = $discount_sum = $vat_sum = $total_sum = 0;

if($delivery_method && $delivery_method['price']){
    $vat_sum += $delivery_method['price'] - round(($delivery_method['price'] / (1 + $this->vat)), 2);
    $total_sum += $delivery_method['price'];
}

?>

<table id="ordertable<?php echo $id; ?>" style="page-break-inside:avoid" width="100%"  border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td colspan="1" rowspan="2"><?=(isset($logo) ? '<img src="'.$logo.'" alt="" style="max-height: 100px">' : '')?></td>
        <td colspan="4" align="right"><b><?php echo WebApp::l("Pasūtījuma nr.", ['lang' => $this->lang]).': '.$num?></b></td>
    </tr>
    <tr>
        <td colspan="4" align="right"><b><?php echo $date?></b></td>
    </tr>
    <tr>
        <td colspan="5"><hr /></td>
    </tr>
    <tr>
        <td><strong><span class="mazsTd"><?php echo WebApp::l("Preču izsniedzējs", ['lang' => $lang])?>:</span></strong></td>
        <td><strong><span class="mazsTd"><?php echo WebApp::l('"Firma SIA"', ['lang' => $lang])?></span></strong></td>
    </tr>
    <tr>
        <td><span class="mazsTd"><?php echo WebApp::l("Reģistrācijas numurs", ['lang' => $lang])?>:</span></td>
        <td><span class="mazsTd"><?php echo WebApp::l("400000000123", ['lang' => $lang])?></span></td>
    </tr>
    <tr>
        <td><span class="mazsTd"><?php echo WebApp::l("Juridiskā adrese", ['lang' => $lang])?>:</span></td>
        <td><span class="mazsTd"><?php echo WebApp::l("Kr. Barona iela 129, Rīga, LV-1012", ['lang' => $lang])?></span></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td><span class="mazsTd"><?php echo WebApp::l("Bankas nosaukums", ['lang' => $lang])?>:</span></td>
        <td><span class="mazsTd"><?php echo WebApp::l("Hansabanka", ['lang' => $lang])?></span></td>
    </tr>
    <tr>
        <td><span class="mazsTd"><?php echo WebApp::l("Bankas kods", ['lang' => $lang])?>:</span></td>
        <td><span class="mazsTd"><?php echo WebApp::l("HABALV22", ['lang' => $lang])?></span></td>
    </tr>
    <tr>
        <td><span class="mazsTd"><?php echo WebApp::l("Bankas konts", ['lang' => $lang])?>:</span></td>
        <td><span class="mazsTd"><?php echo WebApp::l("LV52HABA0551005111951", ['lang' => $lang])?></span></td>
    </tr>
    <tr>
        <td><span class="mazsTd"><?php echo WebApp::l("Tālrunis", ['lang' => $lang])?>:</span></td>
        <td><span class="mazsTd"><?php echo WebApp::l("6777 0077", ['lang' => $lang])?></span></td>
    </tr>
    <tr>
        <td colspan="5"><hr /></td>
    </tr>
    <?php if($payer == 'private'){ ?>
    <tr>
        <td><strong><span class="mazsTd"><?php echo WebApp::l("Vārds, uzvārds", ['lang' => $lang])?>:</span></strong></td>
        <td><strong><span class="mazsTd"><?php echo $fields['name']?></span></strong></td>
    </tr>
    <?php }else{ ?>
    <tr>
        <td><strong><span class="mazsTd"><?php echo WebApp::l("Preču saņēmējs", ['lang' => $lang])?>:</span></strong></td>
        <td><strong><span class="mazsTd"><?php echo $fields['company_name']?></span></strong></td>
    </tr>
    <tr>
        <td><strong><span class="mazsTd"><?php echo WebApp::l("Reģistrācijas numurs", ['lang' => $lang])?>:</span></strong></td>
        <td><strong><span class="mazsTd"><?php echo $fields['registration_nr']?></span></strong></td>
    </tr>
    <tr>
        <td><strong><span class="mazsTd"><?php echo WebApp::l("PVN reģistrācijas numurs", ['lang' => $lang])?>:</span></strong></td>
        <td><strong><span class="mazsTd"><?php echo $fields['vat_number']?></span></strong></td>
    </tr>
    <?php } ?>
    <tr>
        <td><span class="mazsTd"><?php echo WebApp::l("Klienta tālrunis", ['lang' => $lang])?>:</span></td>
        <td><span class="mazsTd"><?php echo $fields['phone']?></span></td>
    </tr>
    <tr>
        <td><span class="mazsTd"><?php echo WebApp::l("Klienta e-pasts", ['lang' => $lang])?>:</span></td>
        <td><span class="mazsTd"><?php echo $fields['email']?></span></td>
    </tr>
    <tr>
        <td><span class="mazsTd"><?php echo WebApp::l("Rēķina status", ['lang' => $lang])?>:</span></td>
        <td><span class="mazsTd" style="font-weight:bold;color: #FFA500"><?php echo ($order_status_row ? $order_status_row['status_'.$lang] : "")?></span></td>
    </tr>
    <tr>
        <td colspan="5"><hr /></td>
    </tr>
    <tr>
        <td><span class="mazsTd"><?php echo WebApp::l("Piegādes veids", ['lang' => $lang])?>:</span></td>
        <td><span class="mazsTd"><?php echo $delivery_method['name']?></span></td>
    </tr>
    <?php if($delivery_method['sub']){ ?>
    <tr>
        <td><span class="mazsTd"><?php echo $delivery_method['sub']['label']?>:</span></td>
        <td><span class="mazsTd"><?php echo $delivery_method['sub']['value']?></span></td>
    </tr>
    <?php } ?>
    <tr>
        <td><span class="mazsTd"><?php echo WebApp::l("Apmaksas veids", ['lang' => $lang])?>:</span></td>
        <td><span class="mazsTd"><?php echo $payment_method?></span></td>
    </tr>
    <tr>
        <td colspan="5"><hr /></td>
    </tr>
    <tr>
        <td colspan="5">
            <table id="ProdTable">
                <tr>
                    <td><?php echo WebApp::l("Nr.", ['lang' => $lang])?></td>
                    <td><?php echo WebApp::l("Preces nosaukums", ['lang' => $lang])?></td>
                    <td><?php echo WebApp::l("Daudzums", ['lang' => $lang])?></td>
                    <td><?php echo WebApp::l("Cena", ['lang' => $lang])?></td>
                    <td><?php echo WebApp::l("Atlaide", ['lang' => $lang])?></td>
                    <td><?php echo WebApp::l("Cena ar atlaidi", ['lang' => $lang])?></td>
                    <td><?php echo WebApp::l("PVN likme", ['lang' => $lang])?></td>
                    <td><?php echo WebApp::l("PVN summa", ['lang' => $lang])?></td>
                    <td><?php echo WebApp::l("Summa(ar PVN)", ['lang' => $lang])?></td>
                </tr>
                <?php foreach($items as $i => $item){ ?>
                <?php

                    $vat = (float)$item['product']['pvn'] ? (float)$item['product']['pvn'] : ($this->vat * 100);

                    $wo_vat_base_price = round($item['base_price'] / (1 + $vat / 100), 2);
                    $wo_vat_price = round($item['price'] / (1 + $vat / 100), 2);

                    $discount_size = round($wo_vat_base_price - $wo_vat_price, 2);
                    $discount_size_p = number_format((1 - $wo_vat_price / $wo_vat_base_price) * 100, 2);

                    $prod_vat_sum = $item['count'] * $item['price'] - $item['count'] * $wo_vat_price;

                    $prod_sum = $item['price'] * $item['count'];

                    # sums
                    $sum_wo_discount += ($item['count'] * $wo_vat_price);
                    $discount_sum += ($item['count'] * $discount_size);
                    $vat_sum += $prod_vat_sum;
                    $total_sum += $prod_sum;
                ?>
                <tr>
                    <td><?php echo number_format($i+1)?>.</td>
                    <td><?php echo $item['name']?></td>
                    <td><?php echo $item['count']?></td>
                    <td><?php echo number_format($wo_vat_base_price, 2)?></td>
                    <td>
                        <?php echo number_format($discount_size, 2); ?>
                        (<?php echo $discount_size_p."%"; ?>)
                    </td>
                    <td><?php echo number_format($wo_vat_price, 2); ?></td>
                    <td><?php echo $vat; ?>%</td>
                    <td><?php echo number_format($prod_vat_sum, 2); ?></td>
                    <td><?php echo number_format($prod_sum, 2); ?></td>
                </tr>
                <?php } ?>
                <tr>
                    <td colspan="5" align="right"><?php echo WebApp::l("Summa bez atlaidēm", ['lang' => $lang]) ?>:</td>
                    <td colspan="14" align="left"><?php echo number_format($sum_wo_discount, 2, ',', ' ')." EUR"?></td>
                </tr>
                <?php if(isset($sum_discount_data) && $sum_discount_data['sum']){ ?>
                <tr>
                    <td colspan="5" align="right"><?php echo WebApp::l("Summas atlaide", ['lang' => $lang])."(".round($sum_discount_data['percentage'] * 100, 2)."%)" ?>:</td>
                    <td colspan="14" align="left"><?php echo number_format($sum_discount_data['sum'], 2, ',', ' ')." EUR"?></td>
                </tr>
                <?php $discount_sum += $sum_discount_data['sum']; ?>
                <?php $total_sum -= $sum_discount_data['sum']; ?>
                <?php } ?>
                <tr>
                    <td colspan="5" align="right"><?php echo WebApp::l("Atlaides summa", ['lang' => $lang]) ?>:</td>
                    <td colspan="14" align="left"><?php echo number_format($discount_sum, 2, ',', ' ')." EUR"?></td>
                </tr>
                <tr>
                    <td colspan='19'>&nbsp;</td>
                </tr>
                <?php if($delivery_method && $delivery_method['price']){ ?>
                <tr>
                    <td colspan="5" align="right"><?php echo WebApp::l("Piegādes izmaksas", ['lang' => $lang]) ?>:</td>
                    <td colspan="14" align="left"><?php echo number_format($delivery_method['price'] / (1 + $this->vat), 2, ',', ' ')." EUR"?></td>
                </tr>
                <?php } ?>
                <tr>
                    <td colspan="5" align="right"><?php echo WebApp::l("Pievienotās vērtības nodoklis", ['lang' => $lang]) ?>:</td>
                    <td colspan="14" align="left"><?php echo number_format($vat_sum, 2, ',', ' ')." EUR"?></td>
                </tr>
                <tr>
                    <td colspan='19'>&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="5" align="right"><strong><?php echo WebApp::l("Summa apmaksai", ['lang' => $lang]) ?>:</strong></td>
                    <td colspan="14" align="left"><strong><?php echo number_format($total_sum, 2, ',', ' ')." EUR"?></strong></td>
                </tr>
                <tr>
                    <td colspan="5" align="right"><?php echo WebApp::l("Summa vārdiem", ['lang' => $lang]) ?>:</td>
                    <td colspan="14" align="left">
                        <?php
                        echo ucfirst(Numbers::sayNumber(
                                $total_sum,
                                $lang,
                                WebApp::l("eiro", ['lang' => $lang]),
                                WebApp::l("eiro", ['lang' => $lang]),
                                WebApp::l("centi", ['lang' => $lang]),
                                WebApp::l("centi", ['lang' => $lang]),
                                WebApp::l("centi", ['lang' => $lang])
                            ));
                        ?>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align='center' colspan='5'>
            <?php echo WebApp::l("Lūdzam rēķinu apmaksāt 3 darba dienu laikā!", ['lang' => $lang])?><br />
            <?php echo WebApp::l("Ar cieņu, Jūsu LEDAkcijas", ['lang' => $lang])?>
        </td>
    </tr>
</table>
<style type="text/css">
    #ProdTable{
        width: 100%;
        border: 1px solid black;
        border-collapse: collapse;
    }

    #ProdTable td{
        border: 1px solid black;
    }

</style>
