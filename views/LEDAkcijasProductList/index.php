<?php

use Constructor\WebApp;

?>
<div class="product-block">
    <div class="header">
        <h4><?php echo $title ?></h4>
        <a href="<?php echo $link ?>" class="more"><?php echo WebApp::l("Skatīt visas")?> &raquo;</a>
    </div>
    <div class="product-grid d-flex">
        <?php foreach($products as $product){ ?>
        <?php echo $this->render("_item", ['product' => $product])?>
        <?php } ?>
        <div class="product placeholder"></div>
        <div class="product placeholder"></div>
        <div class="product placeholder"></div>
        <div class="product placeholder"></div>
    </div>
</div>