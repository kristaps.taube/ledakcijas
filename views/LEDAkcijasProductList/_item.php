<?php

use Constructor\Url;
use Constructor\WebApp;
use LEDAkcijas\CategoryAmountStepCollection;
use LEDAkcijas\ProductAmountPriceCollection;

$manager = \shopmanager::getInstance();
$lang = WebApp::$app->getLanguage();

$prices = $manager->getProductPrice($product, WebApp::$app->user->get());

$price = $prices['price'];
$base_price = $prices['base_price'];
$featured = ($price != $base_price);

if(!$featured){
    $product_steps = ProductAmountPriceCollection::getInstance()->getByProduct($product['item_id']); // will this be a bottle neck?
    $js_steps = [];
    foreach($product_steps as $key => $value){
     if(!($value['price'] > 0)){
         unset($product_steps[$key]);
         continue;
     }
     $js_steps[] = ['from' => $value['count_from'], 'to' => $value['count_to'], 'price' => $value['price']];
    }
}

$picture = $product['picture'] ? $product['picture'] : "/images/html/blank_product.png";

$unit = WebApp::l("gab.");
$unque_id = uniqid('prod-'.$product['item_id'].'-');

?>
<div class="product" data-id="<?php echo $product['item_id']?>" data-featured="<?php echo $featured ? 1 : 0?>">
    <div class="image">
        <div class='tags'>
            <?php if($featured){ ?>
            <div class="tag discount"></div>
            <?php } ?>
            <?php if(isset($tags) && is_array($tags)){ ?>
                <?php foreach($tags as $tag){ ?>
                    <?php echo $tag;?>
                <?php } ?>
            <?php } ?>
        </div>
        <a href="<?php echo Url::get('product', ['product' => $product])?>">
            <img src="<?php echo Url::get(getThumbUrl($picture, 240, 225, 6))?>" alt="">
        </a>
    </div>
    <div class="info">
        <a href="<?php echo Url::get('product', ['product' => $product])?>">
            <span class="name"><?php echo $product['name_'.$lang] ?></span>
            <span class="code"><?php echo WebApp::l('Preces kods')?>: <?php echo $product['code']?></span>
            <div class="price-wrap">
                <?php if($price != $base_price){ ?>
                <span class="new-price">
                    &euro;
                    <span class="value">
                        <?php echo number_format($price, 2)?>
                    </span>
                </span>
                <?php } ?>
                <span class="price">
                    &euro;
                    <span class="value">
                        <?php echo number_format($base_price, 2)?>
                    </span>
                </span>
            </div>
        </a>
    </div>
    <div class="actions">
        <?php if(!$featured && $product_steps){ ?>
        <div class="action-wrap steps-wrap">
            <a href="#" class="action-toggle steps-toggle"></a>
            <div class="action-menu steps-menu">
                <div class="header">
                    <span><?php echo WebApp::l("Pērc vairāk, maksā mazāk!")?></span>
                </div>
                <div class="row">
                    <?php foreach($product_steps as $step){ ?>
                    <div class="col">
                        <label for=""><?php echo WebApp::l("Skaits")?></label>
                        <span class="count">
                            <span class="value"><?php echo $step['count_from'].($step['count_to'] ? "-".$step['count_to'] : '')?></span>
                            <?php if(!$step['count_to']){ ?>
                                <?php echo WebApp::l("un vairāk"); ?>
                            <?php } ?>
                        </span>
                        <div class="price-wrap">
                            <span class="price">
                                &euro;
                                <span class="value">
                                    <?php echo number_format($step['price'], 2)?>
                                </span>
                                /<?php echo $unit?>
                            </span>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <?php } ?>
        <div class="action-wrap compare-wrap">
            <a href="<?php echo Url::get('/compare/add', ['id' => $product['item_id'], 'lang' => WebApp::$app->getLanguage()])?>" class="action-toggle compare-toggle"></a>
        </div>
        <div class="action-wrap wish-wrap">
            <a href="<?php echo Url::get('/wishlist/toggle', ['id' => $product['item_id'], 'lang' => WebApp::$app->getLanguage()])?>" class="action-toggle wish-toggle"></a>
        </div>
        <div class="action-wrap add-wrap">
            <a href="#" class="action-toggle add-toggle"></a>
            <div class="action-menu add-menu">
                <form method='post' action=''>
                    <div class="row">
                        <div class="col-12">
                            <label for="<?php echo $unque_id?>"><?php echo WebApp::l("Daudzums")?></label>
                        </div>
                        <div class="col-12">
                            <div class="numwrap">
                                <input type="number" name="count" id="<?php echo $unque_id?>" min="1" value="1">
                                <a href="#" class="num-control minus">
                                    <span>&ndash;</span>
                                </a>
                                <a href="#" class="num-control plus">
                                    <span>+</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <label for=""><?php echo WebApp::l('Cena')?>/<?php echo $unit?>:</label>
                            <div class="price-wrap" data-unit="<?php echo $unit?>">
                                <?php if($price != $base_price){ ?>
                                <span class="new-price" data-default='<?php echo $price?>'>
                                    &euro;
                                    <span class="value">
                                        <?php echo number_format($price, 2)?>
                                    </span>
                                </span>
                                <?php } ?>
                                <span class="price">
                                    &euro;
                                    <span class="value" data-default='<?php echo $base_price?>'>
                                        <?php echo number_format($base_price, 2)?>
                                    </span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <?php if(!$featured && $product_steps){ ?>
                    <div class="row">
                        <div class="col-12">
                            <?php foreach($product_steps as $step){ ?>
                            <p>
                                <?php
                                    echo WebApp::l("Pērc [counts] - maksā [price]", [
                                        'replace' => [
                                            '[counts]' => '<b>'.$step['count_from'].$unit.($step['count_to'] ? '-'.$step['count_to'].$unit : ' '.WebApp::l("un vairāk")).'</b>',
                                            '[price]' => '<b>'.number_format($step['price'], 2).'/'.$unit.'</b>'
                                        ]
                                    ]);
                                ?>
                            </p>
                            <?php } ?>
                        </div>
                    </div>
                    <?php } ?>
                    <div class="row">
                        <div class="col-12">
                            <a href="<?php echo Url::get('/cart/add', ['p' => $product['item_id']])?>" class="btn orange float-right add"><?php echo WebApp::l("Likt grozā")?></a>
                        </div>
                    </div>
                    <?php if(!$featured && $js_steps){ ?>
                    <script>
                        if(typeof product_prices == 'undefined') var product_prices = [];
                        product_prices[<?php echo $product['item_id']?>] = <?php echo json_encode($js_steps)?>;
                    </script>
                    <?php } ?>
                </form>
            </div>
        </div>
    </div>
</div>