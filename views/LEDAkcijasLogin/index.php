<?php

use Constructor\WebApp;
use Constructor\Url;

?>

<a href="#" class="action-toggle login-toggle"><span><?php echo WebApp::$app->l("Ieiet sistēmā")?></span></a>
<div class="action-menu login-menu">
    <div class="header">
        <span><?php echo WebApp::$app->l("Ieiet sistēmā")?></span>
    </div>
    <?php echo $this->render('login-form')?>
</div>