<?php

use Constructor\Url;
use Constructor\WebApp;

?>
<a href="#" class="action-toggle login-toggle"><span><?php echo WebApp::$app->l('Mans profils')?></span></a>
<div class="action-menu login-menu">
    <div class="profile-nav">
        <div class="nav-item">
            <a href="<?php echo Url::get('profile/order-history')?>" class="nav-link"><?php echo WebApp::$app->l('Mani pasūtījumi')?></a>
        </div>
        <div class="nav-item">
            <a href="<?php echo Url::get("compare")?>" class="nav-link"><?php echo WebApp::$app->l('Manas salīdzināšanas')?></a>
        </div>
        <hr>
        <div class="nav-item">
            <a href="<?php echo Url::get('/user/logout')?>" class="nav-link font-weight-bold"><?php echo WebApp::$app->l('Iziet')?></a>
        </div>
    </div>
</div>