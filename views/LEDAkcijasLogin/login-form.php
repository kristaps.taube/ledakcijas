<?php

use Constructor\Url;
use Constructor\WebApp;

?>
<div class="soc-buttons">
    <a href="<?php echo Url::get('/registration/facebook')?>" class="btn soc-login facebook"><?php echo WebApp::$app->l("Ieiet sistēmā ar Facebook")?></a>
    <a href="<?php echo Url::get('/registration/google')?>" class="btn soc-login google"><?php echo WebApp::$app->l("Ieiet sistēmā ar Google+")?></a>
</div>
<div class="text-center my-2">
    <span><?php echo WebApp::$app->l("vai")?></span>
</div>
<form action="<?php echo Url::get('/user/process-login')?>" method="POST">
    <div class="row">
        <div class="col-12">
            <input type="email" name="email" placeholder="<?php echo WebApp::$app->l("E-pasta adrese..")?>" autocomplete="email">
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <input type="password" name="password" placeholder="<?php echo WebApp::$app->l("Parole")?>" autocomplete="current-password">
        </div>
    </div>
    <div class="row">
        <div class="col-6 d-flex align-items-center">
            <a href="<?php echo Url::get('password_recovery')?>" class="forgot-pass"><?php echo WebApp::$app->l("Aizmirsi paroli?")?></a>
        </div>
        <div class="col-6 d-flex align-items-center">
            <input type="submit" value="<?php echo WebApp::$app->l("Ieiet")?>" class="btn orange solid">
        </div>
    </div>
</form>
<div class="footer">
    <span>
        <?php echo WebApp::$app->l("Neesi reģistrējies?")?>
        <a href="<?php echo Url::get('registration')?>"><?php echo WebApp::$app->l("Reģistrēties")?> &raquo;</a>
    </span>
</div>