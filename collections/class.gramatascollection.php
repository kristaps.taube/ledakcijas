<?

include_once('class.dbcollection.php');

class gramatascollection extends dbcollection
{
  //Class initialization
  function gramatascollection($name,$id)
  {
    $this->dbcollection($name,$id);
    $this->type = "gramatascollection";

    //Properties array definition
    $this->properties = Array(

      "it0"     => Array(
        "label"     => "Title:",
        "type"      => "string",
        "column_name" => "title"
      ),

      "it1"        => Array(
        "label"     => "Text",
        "type"      => "html",
        "wrap"      => "none",
        "width"     => "200",
        "rows"      => "6",
        "cols"      => "30",
        "custom"    => "",
        "column_name" => "text"
      ),

      "it2"    => Array(
        "label"     => "Picture:",
        "type"      => "file",
        "upload_dir"  => "images/gramatu_bildes/",
        "resize_mode" => 1,
        "resize_width" => 100,
        "resize_height" => 100,
        "column_name" => "picture"
      ),

      "it3" => Array(
        "label"     => "Long text:",
        "type"      => "wysiwyg",
        "rows"      => "7",
        "cols"      => "50",
        "dialog"    => "?module=wysiwyg&site_id=".$GLOBALS['site_id'],
        "dialogw"   => "720",
        "dialogh"   => "450",
        "dlg_res"   => true,
        "column_name" => "longtext"
      ),

      "it4"    => Array(
        "label"     => "Date:",
        "type"      => "date",
        "time"      => 1,
        "column_name" => "date"
      )


    );

    //Collection display table definition
    $this->columns = Array(

      "title"        => Array(
        "width"     => "30%",
        "title"     => "Title",
      ),

      "text"    => Array(
        "width"     => "40%",
        "title"     => "Text"
      ),

      "picture"    => Array(
        "width"     => "30%",
        "title"     => "Picture",
        "format"    => "[if {%_self%}]<img src=\"{%_self%}\" />[/if]"
      )



    );

    $this->postInit();

  }

  function IsEditableOutside()
  {
    $this->description = 'Labot gramatas';
    $this->longname = $this->name;
    return true;
  }



}

?>