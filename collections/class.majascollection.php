<?
//Serveris.LV Constructor collection module
//Created by Aleksandrs Selickis, 2004-2005
//Project: Deinland

include_once('class.catsubcollection.php');

class MajasCollection extends catsubcollection
{

    var $texts = array("lv"=> array("platiba" => "Plat�ba: ",
                                "istabu_skaits" => "Istabu skaits:",
                                "stavs" => "St�vs: ",
                                "stavu_skaits" => "St�vu skaits: ",
                                "zemes_platiba" => "Zemes plat�ba",
                                "serija" => "S�rija: ",
                                "istabas" => "Istabas: ",
                                "majas_tips" => "M�jas tips: ",
                                "ipasuma_tips" => "�pa�uma tips: ",
                                "ertibas" => "�rt�bas: ",
                                "sanmezgls" => "Sanmezgls: ",
                                "datums" => "Sludin�jums ievietots: ",
                                "komplektacija" => "Komplekt�cija: ",
                                "kontakti" => "Kontakti: ",
                                "telefons" => "T�lrunis: ",

                                "form_pilseta" => "Pils�ta: *",
                                "form_rajons" => "Rajons: *",
                                "form_iela" => "Iela: ",
                                "form_platiba" => "Plat�ba: ",
                                "form_zemes_paltiba" => "Zemes plat�ba: ",
                                "form_istabas" => "Istabas: ",
                                "form_stavs" => "St�vs: ",
                                "form_stavu_skaits" => "St�vu skaits: ",
                                
                                "form_serija" => "S�rija: ",
                                "form_serija_103" => "103. s�rija",
                                "form_serija_104" => "104. s�rija",
                                "form_serija_119" => "119. s�rija",
                                "form_serija_467" => "467. s�rija",
                                "form_serija_602" => "602. s�rija",
                                "form_serija_hrusch" => "Hru��ova",
                                "form_serija_lietuv" => "Lietuvie�u pr.",
                                "form_serija_mazgim" => "Maz�ime�u",
                                "form_serija_privma" => "Priv�tm�ja",
                                "form_serija_stalin" => "Sta�ina laika",
                                "form_serija_chehu" => "�ehu pr.",
                                "form_serija_pirmsk" => "Pirmskara m�ja",
                                "form_serija_specpr" => "Specprojekts",
                                
                                "form_istabas" => "Istabas: ",
                                "form_istabas_1" => "Caurst.-izol�tas istabas",
                                "form_istabas_2" => "Caurstaig�jamas istabas",
                                "form_istabas_3" => "Izol�tas istabas",
                                
                                "form_majas_tips" => "M�jas_tips: ",
                                "form_majas_tips_1" => "Koka",
                                "form_majas_tips_2" => "Pane�u",
                                "form_majas_tips_3" => "�ie�e�u",
                                "form_majas_tips_4" => "Akmens",
                                "form_majas_tips_5" => "Pane�u-�ie�e�u",
                                "form_majas_tips_6" => "�ie�e�u-pane�u",
                                    
                                "form_ipasuma_tips" => "�pa�uma tips: ",
                                "form_ipasuma_tips_1" => "Kooperat�va",
                                "form_ipasuma_tips_2" => "Saimnieka m�ja",
                                "form_ipasuma_tips_3" => "�res ties�bas",
                                "form_ipasuma_tips_4" => "Privatiz�ta",

                                "form_ertibas" => "�rt�bas: ",
                                "form_ertibas_1" => "Visas �rt�bas",
                                "form_ertibas_2" => "Da��jas �rt�bas",
                                "form_ertibas_3" => "Bez �rt�b�m",

                                "form_sanmezgls" => "Sanmezgls: ",
                                "form_sanmezgls_1" => "Atsevi��s",
                                "form_sanmezgls_2" => "Kop�js",

                                "form_komplektacija" => "Komplekt�cija: ",
                                "form_cena" => "Cena: ",
                                "form_cena_2" => "&#128/m�n.",

                                "form_sludinajuma_teksts" => "Sludni�juma teksts:&nbsp;*&nbsp;",
                                "form_telefons" => "Telefons: *",
                                "form_bilde" => "Bilde",
                                "form_submit" => "Pievienot",

                                "error_pilseta" => "Nor�diet pils�tu",
                                "error_rajons" => "Nor�diet rajonu",
                                "error_teksts" => "Uzrakstiet sludin�juma tekstu",
                                "error_info" => "Nor�diet J�su kontaktinform�ciju",

                                "upload_error" => "Neizdev�s pievienot failu sludin�jumam.",
                                "upload_error_fails" => "Fails ",
                                "upload_error_1" => " ir liel�ks par 500 Kb",
                                "upload_error_2" => " nav GIF, JPG vai PNG format�",

                                "paldies" => "Paldies! J�su sludin�jums tika saglab�ts.<br/><br/>Sludin�jums b�s publiski pieejams tikl�dz m�su darbinieki to apstiprin�s."
                                ),  

                   "ru"=> array("platiba" => "�������: ",
                                "istabu_skaits" => "���-�� ������:",
                                "stavs" => "����: ",
                                "stavu_skaits" => "���-�� ������: ",
                                "zemes_platiba" => "������� �����: ",
                                "serija" => "�����: ",
                                "istabas" => "�������: ",
                                "majas_tips" => "��� ����: ",
                                "ipasuma_tips" => "��� �������������: ",
                                "ertibas" => "��������: ",
                                "sanmezgls" => "���.����: ",
                                "datums" => "���������� ���������: ",
                                "komplektacija" => "������������: ",
                                "kontakti" => "��������: ",
                                "telefons" => "�������: ",

                                "form_pilseta" => "�����: *",
                                "form_rajons" => "�����: *",
                                "form_iela" => "�����: ",
                                "form_platiba" => "�������: ",
                                "form_zemes_platiba" => "������� �����: ",
                                "form_istabas" => "������: ",
                                "form_stavs" => "����: ",
                                "form_stavu_skaits" => "���-�� ������: ",
                                
                                "form_serija" => "�����: ",
                                "form_serija_103" => "103. �����",
                                "form_serija_104" => "104. �����",
                                "form_serija_119" => "119. �����",
                                "form_serija_467" => "467. �����",
                                "form_serija_602" => "602. �����",
                                "form_serija_hrusch" => "��������",
                                "form_serija_lietuv" => "��������� ������",
                                "form_serija_mazgim" => "�����������",
                                "form_serija_privma" => "������� ���",
                                "form_serija_stalin" => "��������",
                                "form_serija_chehu" => "������� ������",
                                "form_serija_pirmsk" => "��������� ���",
                                "form_serija_specpr" => "����������",
                                
                                "form_istabas" => "�������: ",
                                "form_istabas_1" => "������-����. �������",
                                "form_istabas_2" => "������� �������",
                                "form_istabas_3" => "�������������",
                                
                                "form_majas_tips" => "��� ����: ",
                                "form_majas_tips_1" => "����������",
                                "form_majas_tips_2" => "���������",
                                "form_majas_tips_3" => "���������",
                                "form_majas_tips_4" => "��������",
                                "form_majas_tips_5" => "��������-���������",
                                "form_majas_tips_6" => "��������-���������",
                                    
                                "form_ipasuma_tips" => "��� �������������: ",
                                "form_ipasuma_tips_1" => "�������������",
                                "form_ipasuma_tips_2" => "� ���� � ����������",
                                "form_ipasuma_tips_3" => "� ������ ������",
                                "form_ipasuma_tips_4" => "�����������������",

                                "form_ertibas" => "��������: ",
                                "form_ertibas_1" => "��� ��������",
                                "form_ertibas_2" => "��������� ��������",
                                "form_ertibas_3" => "��� �������",

                                "form_sanmezgls" => "�������: ",
                                "form_sanmezgls_1" => "���������",
                                "form_sanmezgls_2" => "�����",

                                "form_komplektacija" => "������������: ",
                                "form_cena" => "����: ",
                                "form_cena_2" => "&#128/���.",

                                "form_sludinajuma_teksts" => "����� ����������:&nbsp;*&nbsp;",
                                "form_telefons" => "�������: *",
                                "form_bilde" => "��������",
                                "form_submit" => "��������",

                                "error_pilseta" => "������� �����",
                                "error_rajons" => "������� �����",
                                "error_teksts" => "�������� ����� ����������",
                                "error_info" => "������� ���� ���������� ����������",

                                "upload_error" => "�� ������� ������������ ���� � ����������.",
                                "upload_error_fails" => "���� ",
                                "upload_error_1" => " ������ 500 Kb",
                                "upload_error_2" => " �� ������������� GIF, JPG ��� PNG �������",

                                "paldies" => "�������! ���� ���������� ���������.<br/><br/>���������� ����� �������� �������� ��� ������ ���� ���������� ��� ��������."
                                ),

                   "en"=> array("platiba" => "Floor space: ",
                                "istabu_skaits" => "Room count:",
                                "stavs" => "Floor: ",
                                "stavu_skaits" => "Floor count: ",
                                "zemes_platiba" => "������� �����: ",
                                "serija" => "Series: ",
                                "istabas" => "Rooms: ",
                                "majas_tips" => "House type: ",
                                "ipasuma_tips" => "Property type: ",
                                "ertibas" => "Comfort: ",
                                "sanmezgls" => "toilet facilities: ",
                                "datums" => "Date added: ",
                                "komplektacija" => "Complectaion: ",
                                "kontakti" => "Contacts: ",
                                "telefons" => "Phone: ",

                                "form_pilseta" => "Pils�ta: *",
                                "form_rajons" => "Rajons: *",
                                "form_iela" => "Iela: ",
                                "form_platiba" => "Plat�ba: ",
                                "form_zemes_paltiba" => "������� �����: ",
                                "form_istabas" => "Istabu skaits: ",
                                "form_stavs" => "St�vs: ",
                                "form_stavu_skaits" => "St�vu skaits: ",
                                
                                "form_serija" => "S�rija: ",
                                "form_serija_103" => "103. s�rija",
                                "form_serija_104" => "104. s�rija",
                                "form_serija_119" => "119. s�rija",
                                "form_serija_467" => "467. s�rija",
                                "form_serija_602" => "602. s�rija",
                                "form_serija_hrusch" => "Hru��ova",
                                "form_serija_lietuv" => "Lietuvie�u pr.",
                                "form_serija_mazgim" => "Maz�ime�u",
                                "form_serija_privma" => "Priv�tm�ja",
                                "form_serija_stalin" => "Sta�ina laika",
                                "form_serija_chehu" => "�ehu pr.",
                                "form_serija_pirmsk" => "Pirmskara m�ja",
                                "form_serija_specpr" => "Specprojekts",
                                
                                "form_istabas" => "Istabas: ",
                                "form_istabas_1" => "Caurst.-izol�tas istabas",
                                "form_istabas_2" => "Caurstaig�jamas istabas",
                                "form_istabas_3" => "Izol�tas istabas",
                                
                                "form_majas_tips" => "M�jas_tips: ",
                                "form_majas_tips_1" => "Koka",
                                "form_majas_tips_2" => "Pane�u",
                                "form_majas_tips_3" => "�ie�e�u",
                                "form_majas_tips_4" => "Akmens",
                                "form_majas_tips_5" => "Pane�u-�ie�e�u",
                                "form_majas_tips_6" => "�ie�e�u-pane�u",
                                    
                                "form_ipasuma_tips" => "�pa�uma tips: ",
                                "form_ipasuma_tips_1" => "Kooperat�va",
                                "form_ipasuma_tips_2" => "Saimnieka m�ja",
                                "form_ipasuma_tips_3" => "�res ties�bas",
                                "form_ipasuma_tips_4" => "Privatiz�ta",

                                "form_ertibas" => "�rt�bas: ",
                                "form_ertibas_1" => "Visas �rt�bas",
                                "form_ertibas_2" => "Da��jas �rt�bas",
                                "form_ertibas_3" => "Bez �rt�b�m",

                                "form_sanmezgls" => "Sanmezgls: ",
                                "form_sanmezgls_1" => "Atsevi��s",
                                "form_sanmezgls_2" => "Kop�js",

                                "form_komplektacija" => "Komplekt�cija: ",
                                "form_cena" => "Cena: ",
                                "form_cena_2" => "&#128/m�n.",

                                "form_sludinajuma_teksts" => "Sludni�juma teksts:&nbsp;*&nbsp;",
                                "form_telefons" => "Telefons: *",
                                "form_bilde" => "Bilde",
                                "form_submit" => "Pievienot",

                                "error_pilseta" => "Nor�diet pils�tu",
                                "error_rajons" => "Nor�diet rajonu",
                                "error_teksts" => "Uzrakstiet sludin�juma tekstu",
                                "error_info" => "Nor�diet J�su kontaktinform�ciju",

                                "upload_error" => "Neizdev�s pievienot failu sludin�jumam.",
                                "upload_error_fails" => "Fails ",
                                "upload_error_1" => " ir liel�ks par 500 Kb",
                                "upload_error_2" => " nav GIF, JPG vai PNG format�",

                                "paldies" => "Paldies! J�su sludin�jums tika saglab�ts.<br/><br/>
            Sludin�jums b�s publiski pieejams tikl�dz m�su darbinieki to apstiprin�s."
                                ),                                      
    );


function DisplayItemText($id)
{
    $row = $this->GetItem($id);
    echo "<b>".$row['1'].'</b><br />';
	echo $row['2']."<br />";
}


function DisplayItemInfo($id)
{
    $row = $this->GetItem($id);
	echo "<i>".$row['3'].'</i><br />';
}


function GetItemDate($id)
{
    $row = $this->GetItem($id);
	echo "<i>".$row['0'].'</i><br />';
}


function DisplayForm($type, $cat_id, $kompl_table_name, $form = "", $lang = 0)
{
    switch ($lang)
    {
    case "1": 
        $lang_dir = "_ru";
        $lang_val = "ru";
        break;
    case "2":
        $lang_dir = "_en";
        $lang_val = "en";
        break;
    case "3":
        $lang_dir = "_de";
        $lang_val = "de";
        break;
    default:
        $lang_dir = "";
        $lang_val = "lv";
    }

    //ja forma tiek aizpildiita atkaartoti, tad njem sho veertiibu no post masiiva
    if ($form['table']) $kompl_table_name = $form['table'];

    echo '<table class="rightAdTableTxt" border=0 cellspacing=0 cellpadding=0>'
        .'<form enctype="multipart/form-data" action="?add_adv=1&cat_id='.$cat_id.'&type='.$type.'" method="POST">'

        .'<tr><td>'.$this->texts[$lang_val]['form_pilseta'].'</td><td><input class="AdFormInput" name="pilseta" type="text" size=26 value="'.$form['pilseta'].'"></td></tr>'
        .'<tr><td>'.$this->texts[$lang_val]['form_rajons'].'</td><td><input class="AdFormInput" name="rajons" type="text" size=26 value="'.$form['rajons'].'"></td></tr>'
        .'<tr><td>'.$this->texts[$lang_val]['form_iela'].'</td><td><input class="AdFormInput" name="iela" type="text" size=26 value="'.$form['iela'].'"></td></tr>'

        .'<tr><td>'.$this->texts[$lang_val]['form_platiba'].'</td><td><input class="AdFormInput" name="maja_platiba" type="text" size=26 value="'.$form['maja_platiba'].'"></td></tr>'
        .'<tr><td>'.$this->texts[$lang_val]['form_zemes_platiba'].'</td><td><input class="AdFormInput" name="maja_zemes_platiba" type="text" size=26 value="'.$form['maja_zemes_platiba'].'"></td></tr>'
        .'<tr><td>'.$this->texts[$lang_val]['form_stavu_skaits'].'</td><td><input class="AdFormInput" name="maja_stavu_skaits" type="text" size=26 value="'.$form['maja_stavu_skaits'].'"></td></tr>'
        .'<tr><td>'.$this->texts[$lang_val]['form_istabas'].'</td><td><input class="AdFormInput" name="maja_istabas" type="text" size=26 value="'.$form['maja_istabas'].'"></td></tr>';

    $data = sqlQueryDataAssoc("SELECT * FROM ".$kompl_table_name);
    $i=0;

    echo '<tr><td colspan="2"><table class="slud">';
    echo '<tr><td>'.$this->texts[$lang_val]['ertibas'].'</td></tr>';
    foreach ($data as $key=>$row)
    {
        if ($i%2 == 0) echo '<tr>';
        echo '<td>&nbsp;&nbsp;&nbsp;'.$row['ertiba_nosaukums'].':</td><td><input type="checkbox" name="ertiba_'.$row['ertiba_id'].'"';if ($form['ertiba_'.$row['ertiba_id']]) echo ' CHECKED'; echo '></td>';
        if ($i%2 ==1) echo '</tr>';
        $i++;
    }
    echo '</table></td></tr>';

    echo '<tr><td>'.$this->texts[$lang_val]['form_cena'].'</td><td>
                <input class="AdFormInput" type="text" name="maja_cena_val" value="'.$form['maja_cena_val'].'">
                <select class="AdFormInput" name="maja_cena_type">
                    <OPTION value=""'; if (!$form['maja_cena_type']) echo' selected'; echo'></OPTION> 
                    <option value="&#128"'; if ($form['maja_cena_type']=="&#128") echo' selected'; echo'>&#128</option>
                    <option value="&#128/m�n."'; if ($form['maja_cena_type']=="&#128/m�n.") echo' selected'; echo'>'.$this->texts[$lang_val]['form_cena_2'].'</option>
                    <option value="&#128/m2"'; if ($form['maja_cena_type']=="&#128/m2") echo' selected'; echo'>&#128/m2</option>
                </select>
                <input type="hidden" name="table" value="'.$kompl_table_name.'">
              </td></tr>';

    echo '<tr><td valign=top>'.$this->texts[$lang_val]['form_sludinajuma_teksts'].'</td><td><textarea  name="text" rows=5 cols=32>'.$form['text'].'</textarea></td></tr>'
        .'<tr><td>'.$this->texts[$lang_val]['form_telefons'].'</td><td><input class="AdFormInput" name="phone" type="text" size=30 value="'.$form['phone'].'"></td></tr>'
        .'<tr><td>E-pasts: *</td><td><input class="AdFormInput" name="email" type="text" size=30 value="'.$form['email'].'"></td></tr>'
        .'<tr><td>'.$this->texts[$lang_val]['form_bilde'].' 1: </td><td><input class="AdFormInput" name="pic1" type="file" size=30></td></tr>'
        .'<tr><td>'.$this->texts[$lang_val]['form_bilde'].' 2: </td><td><input class="AdFormInput" name="pic2" type="file" size=30></td></tr>'
        .'<tr><td>'.$this->texts[$lang_val]['form_bilde'].' 3: </td><td><input class="AdFormInput" name="pic3" type="file" size=30></td></tr>'
        .'<tr><td>'.$this->texts[$lang_val]['form_bilde'].' 4: </td><td><input class="AdFormInput" name="pic4" type="file" size=30></td></tr>'
        .'<tr><td>'.$this->texts[$lang_val]['form_bilde'].' 5: </td><td><input class="AdFormInput" name="pic5" type="file" size=30></td></tr>'
        .'<tr><td>'.$this->texts[$lang_val]['form_bilde'].' 6: </td><td><input class="AdFormInput" name="pic6" type="file" size=30></td></tr>'
        .'<tr><td></td><td align="right"><br><input class="AdFormInput" type="submit" value="'.$this->texts[$lang_val]['form_submit'].'"></td></tr>'
        .'</form></table>';

}









function SaveAdv($type, $cat_id, $tbl_base, $email_to, $path, $lang)
{
    switch ($lang)
    {
        case "1": 
            $lang_dir = "_ru";
            $lang_val = "ru";
            break;
        case "2":
            $lang_dir = "_en";
            $lang_val = "en";
            break;
        case "3":
            $lang_dir = "_de";
            $lang_val = "de";
            break;
        default:
            $lang_dir = "";
            $lang_val = "lv";
    }

    $tbl_adv = $tbl_base."_"."adv";
    $tbl_kompl = $tbl_base."_"."komplektacija";
    $tbl_adv_kompl = $tbl_base."_"."adv_kompl";

    $directory = sqlQueryValue("select dirroot from sites where site_id=".$this->site_id);
    //$directory .= 'images\\slud_bildes\\';

    $uploaddir = $directory .'images/slud_bildes'.$lang_dir.'/';
    //$uploaddir = $directory .'images\\slud_bildes'.$lang_dir.'\\';

    $rajons = addslashes(strip_tags($_POST['rajons']));
    $pilseta = addslashes(strip_tags($_POST['pilseta']));
    $iela = addslashes(strip_tags($_POST['iela']));
    $text = addslashes(strip_tags($_POST['text']));
    $phone = addslashes(strip_tags($_POST['phone']));
    $email = addslashes(strip_tags($_POST['email']));

    $error = 0;

    //echo '<td align="center" bgcolor="#F7FFF4" width="440" style="padding-right:10px; padding-left:10px;padding-top:15px;" colspan="2">';

    echo '<div align="center" style="margin:10px; font: 11px Tahoma; font-weight:bold;">';

    if (!$pilseta) { echo $this->texts[$lang_val]['error_pilseta'].'<br>'; $error = 1; }
    if (!$rajons) { echo $this->texts[$lang_val]['error_rajons'].'<br>'; $error = 1; }
    if (!$text) { echo $this->texts[$lang_val]['error_teksts'].'<br>'; $error = 1; }
    if (!$phone || !$email) { echo $this->texts[$lang_val]['error_info'].'<br>'; $error = 1; }
    
    echo '<br/>';

//sludinaajuma ID
    $next_id = 1 + sqlQueryValue("SELECT MAX(adv_id) FROM ".$tbl_adv);

    if ($_FILES['pic1']['name'] != "")
    {
        $file1 = $next_id . "_1.JPG";
        if ($this->upload($uploaddir, $i=1, $next_id, $directory, $lang_val)) $error = 1;
        $pic1 = $_POST['pic1'];
        $pic1 = basename($pic1);
    }

    if ($_FILES['pic2']['name'] != "")
    {
        $file2 = $next_id . "_2.JPG";
        if ($this->upload($uploaddir, $i=2, $next_id, $directory, $lang_val)) $error = 1;
        $pic2 = $_POST['pic2'];
        $pic2 = basename($pic2);
    }

    if ($_FILES['pic3']['name'] != "")
    {
        $file3 = $next_id . "_3.JPG";
        if ($this->upload($uploaddir, $i=3, $next_id, $directory, $lang_val)) $error = 1;
        $pic3 = $_POST['pic3'];
        $pic3 = basename($pic3);
    }

    if ($_FILES['pic4']['name'] != "")
    {
        $file4 = $next_id . "_4.JPG";
        if ($this->upload($uploaddir, $i=4, $next_id, $directory, $lang_val)) $error = 1;
        $pic4 = $_POST['pic4'];
        $pic4 = basename($pic4);
    }

    if ($_FILES['pic5']['name'] != "")
    {
        $file5 = $next_id . "_5.JPG";
        if ($this->upload($uploaddir, $i=5, $next_id, $directory, $lang_val)) $error = 1;
        $pic5 = $_POST['pic5'];
        $pic5 = basename($pic5);
    }

    if ($_FILES['pic6']['name'] != "")
    {
        $file6 = $next_id . "_6.JPG";
        if ($this->upload($uploaddir, $i=6, $next_id, $directory, $lang_val)) $error = 1;
        $pic6 = $_POST['pic6'];
        $pic6 = basename($pic6);
    }

    $adv_date_start = time();
    $adv_date_end = mktime(0, 0, 0, date("m")  , date("d")+14, date("Y"));


    //tikai majas
    $maja_platiba = $_POST['maja_platiba'];
    $maja_zemes_platiba = $_POST['maja_zemes_platiba'];
    $maja_istabas = $_POST['maja_istabas'];
    $maja_stavu_skaits = $_POST['maja_stavu_skaits'];
    
    /*
    switch ($_POST['maja_istabas'])
    {
        case 1: $maja_istabas = "Caurst.-izol�tas istabas";
                    break;
        case 2: $maja_istabas = "Caurstaig�jamas istabas";
                    break;        
        case 3: $maja_istabas = "Izol�tas istabas";
                    break;
    } 
    */

    $maja_cena = $_POST['maja_cena_val']." ".$_POST['maja_cena_type'];

    if (!$error)
    {
        echo '
        <div align="center" style="padding-top:10px;FONT-WEIGHT: bold; TEXT-DECORATION: none">
        '.$this->texts[$lang_val]['paldies'].'   
        </div>';
        //echo '</td>';

        $q1 = "
            INSERT INTO ".$tbl_adv." (
                    adv_id,
                    cat_id,
                    adv_date_start,
                    adv_date_end,
                    rajons,
                    pilseta,
                    iela,
                    text,
                    phone,
                    email,
                    pic1,
                    pic2,
                    pic3,
                    pic4,
                    pic5,
                    pic6,
                    maja_platiba,
                    maja_zemes_platiba,
                    maja_istabas,
                    maja_stavu_skaits,
                    maja_cena
                    )
                values (".$next_id.",".$cat_id.",'".$adv_date_start."','".$adv_date_end."','".$rajons."','".$pilseta."',
                    '".$iela."','".$text."','".$phone."','".$email."','".$file1."',
                    '".$file2."','".$file3."',
                    '".$file4."','".$file5."',
                    '".$file6."','".$maja_platiba."','".$maja_zemes_platiba."',
                    '".$maja_istabas."','".$maja_stavu_stavs."','".$majas_cena."'
            )
                ";

        sqlQuery($q1);
        //$new_adv_id = sqlLastID();

        //arii vajag ierakstiit komplektaciju  
        $data = sqlQueryDataAssoc("SELECT * FROM ".$tbl_kompl);
        
        $c = 0;
        foreach ($data as $key=>$row)
        {
            if (isset($_POST['ertiba_'.$row['ertiba_id']])) 
            {
               sqlQueryDataAssoc("INSERT INTO ".$tbl_adv_kompl."(adv_id,ertiba_id) values ($next_id, ".$row['ertiba_id'].");");
               if ($c) $komplektacija .= ", ";
               $komplektacija .= $row['ertiba_nosaukums'];
               $c = 1;
            }
        }

        $c = 0;
        foreach ($path as $key=>$val)
        {
            if ($c) $cat_path .= " -> ";
            $cat_path .= $val ;
            $c = 1;
        }

        $mailtext .= "Kategorij�: ".$cat_path."\n\n";

        $mailtext .= "Pils�ta: ".$pilseta."\n";
        $mailtext .= "Rajons: ".$rajons."\n";
        $mailtext .= "Iela: ".$iela."\n\n";

        $mailtext .= "Plat�ba: ".$maja_platiba."\n";
        $mailtext .= "Zemes plat�ba: ".$maja_zemes_platiba."\n";
        $mailtext .= "St�vu skaits: ".$maja_stavu_skaits."\n";
        $mailtext .= "Istabas: ".$maja_istabas."\n";

        $mailtext .= "Komplekt�cija: ". $komplektacija. "\n\n";

        $mailtext .= "Cena: ".$maja_cena."\n";

        $mailtext .= "Teksts: ".$text."\n";
        $mailtext .= "T�lrunis: ".$phone."\n";
        $mailtext .= "E-pasts: ".$email."\n";

        send_mail("Notification", "", "", $email_to, $encoding, "Tika pievienots jauns sludin�jums", $mailtext);
     
    }
    else
    {
        //echo '</td></tr><tr>';
        //echo '<td align="center" bgcolor="#F7FFF4" width="440" style="padding-right:10px; padding-left:10px;padding-top:15px;" colspan="2">';
        echo '<div align="center">';
        $this->DisplayForm($_GET['type'],$_GET['cat_id'],$_POST['table'],$_POST,$lang);
        echo '</div><br/>';
        //echo '</td>';
    }  
}




function upload($uploaddir, $i, $next_id, $directory, $lang_val)
{
    global $_FILES;

    //nokluseeta v�rt�ba ir 1; ja fails tiks veiksm�gi pievienots, tad buus 0
    $error = 1;

    switch ($i)
    {
        case 1: $userfile = 'pic1'; break;
        case 2: $userfile = 'pic2'; break;
        case 3: $userfile = 'pic3'; break;
        case 4: $userfile = 'pic4'; break;
        case 5: $userfile = 'pic5'; break;
        case 6: $userfile = 'pic6'; break;
    }

    $uploadfile = $uploaddir . $next_id . "_".$i.".JPG";

        if ($_FILES[$userfile]['size']>500000)
            echo $this->texts[$lang_val]['upload_error'].'<br>'
                     .$this->texts[$lang_val]['upload_error_fails'] .$_FILES[$userfile]['name'].$this->texts[$lang_val]['upload_error_1'];
        else
            if ("image/pjpeg" != $_FILES[$userfile]["type"] && "image/jpeg" != $_FILES[$userfile]["type"] && "image/gif" != $_FILES[$userfile]["type"] && "image/x-png" != $_FILES[$userfile]["type"])
                echo $this->texts[$lang_val]['upload_error'].'<br>'
                         .$this->texts[$lang_val]['upload_error_fails']
                         .$_FILES[$userfile]['name'].$this->texts[$lang_val]['upload_error_2'];
            else
                if (!move_uploaded_file($_FILES[$userfile]['tmp_name'], $uploadfile)) {
                   print $this->texts[$lang_val]['upload_error'];
                }
                else
                {
                    $error = 0;
                    //error_reporting (E_ALL);

                    $overlay_img = imagecreatefromGIF($directory."images/logo.gif");//LOGO PIC
                    //$overlay_img = imagecreatefromGIF($directory."images\logo.gif");//LOGO PIC
                    
                    if ($_FILES[$userfile]["type"] == "image/gif")
                        $abc = imagecreatefromGIF($uploadfile); //UPLOADED PIC FILE
                    else if ($_FILES[$userfile]["type"] == "image/jpeg" || $_FILES[$userfile]["type"] == "image/pjpeg")
                        $abc = imagecreatefromjpeg($uploadfile); //UPLOADED PIC FILE
                    else if ($_FILES[$userfile]["type"] == "image/x-png")
                    {
                        $abc = imagecreatefrompng($uploadfile); //UPLOADED PIC FILE
                    }
                    else 
                    {
                        print $this->texts[$lang_val]['upload_error'];
                        $error = 1;
                    }
                    
                    if (!$error)
                    {
                        $new_h = round(ImageSY($abc) * (190/ImageSX($abc)));

                        $def = @imagecreatetruecolor(190, $new_h) or die("Cannot Initialize new GD image stream");

                        //resize to fixed size
                        //imagecopyresized($def, $abc, 0, 0, 0, 0, 190, 150, ImageSX($abc), ImageSY($abc)); 
                        //resize proportional
                        
                        imagecopyresized($def, $abc, 0, 0, 0, 0, 190, $new_h, ImageSX($abc), ImageSY($abc)); 

                        imagecopymerge($def, $overlay_img , 185-ImageSX($overlay_img),$new_h-ImageSY($overlay_img)-5,0,0, ImageSX($overlay_img), ImageSY($overlay_img),100);

                        imagejpeg($def, $uploadfile, 100);  //new file from $def

                        ImageDestroy($overlay_img); 
                        ImageDestroy($abc); 
                        ImageDestroy($def); 
                    }
                    }
    return $error;
}







//dzivoklicollection COPY!!!
function DisplayAdv($adv_id, $tbl_base, $adv_type,$email,$phone,$lang)
{

    switch ($lang)
    {
        case "1": 
            $lang_dir = "_ru";
            $lang_val = "ru";
            break;
        case "2":
            $lang_dir = "_en";
            $lang_val = "en";
            break;
        case "3":
            $lang_dir = "_de";
            $lang_val = "de";
            break;
        default:
            $lang_dir = "";
            $lang_val = "lv";
    }

$tbl_kompl = $tbl_base."_"."komplektacija";
$tbl_adv_kompl = $tbl_base."_"."adv_kompl";

$data = sqlQueryRow("SELECT * FROM ".$tbl_base."_adv where adv_id='".$adv_id."'");
//print_r($data);
$query = '
        SELECT ertiba_nosaukums
        FROM `'.$tbl_kompl.'` as k, `'.$tbl_adv_kompl.'` as ak
        WHERE k.ertiba_id=ak.ertiba_id and ak.adv_id='.$adv_id.'
        ';
$data_kompekt = sqlQueryDataAssoc($query);

//onmouseover="img_big.src=\'/images/slud_bildes/'.$data['pic1'].'\';"
//onmouseout="img_big.src=\'/images/slud_bildes/'.$data['pic1'].'\';" 

//sakaarto bildes peec kaartas jaunajaa masiivaa
//piem. ir defineeti $data['pic4'], $data['pic6'] => buus $pic[1], pic[1]
$j = 1;
for ($i=1; $i<7; $i++)
    {
        $ar_ind = "pic".$i;
        if ($data[$ar_ind] != "") 
        {
            $pic[$j] = $data[$ar_ind];
            $j++;
        }
    }

echo '<table style="font: 11px Tahoma;"><tr>';

if (!empty($pic))
{

echo '<td bgcolor="#F7FFF4" width="200" style="padding-left: 10px;padding-top:15px;" >';

echo '
				<!-- inner picture table -->
				<table border="0" cellspacing="5" cellpadding="0">
					<tr>
						<td colspan="3" bgcolor="#CFECC4" width="190" height="150">';
                        
                        if (isset($pic['1'])) echo '<img name="img_big" src="/images/slud_bildes'.$lang_dir.'/'.$pic[1].'" width="190">'; 
echo '
                        </td>
					</tr>
					<tr>
                        ';
                           if (isset($pic[1])) 
                        {
                            echo '<td bgcolor="#CFECC4" width="60" height="50">';
                            echo '<img 
                                onmouseover="img_big.src=\'/images/slud_bildes'.$lang_dir.'/'.$pic[1].'\';"
                                onmouseout="img_big.src=\'/images/slud_bildes'.$lang_dir.'/'.$pic[1].'\';" 
                                border="0" style="margin:0px;padding:0px;" width="60" src="/images/slud_bildes'.$lang_dir.'/'.$data['pic1'].'">';
                        }
                        else
                            echo '<td bgcolor="#9CF576" width="60" height="50">';
                        echo '
                        </td>
                        ';
                        if (isset($pic[2])) 
                        {
                            echo '<td bgcolor="#CFECC4" width="60" height="50">';
                            echo '<img
                                 onmouseover="img_big.src=\'/images/slud_bildes'.$lang_dir.'/'.$pic[2].'\';"
                                 border="0" style="margin:0px;padding:0px;" width="60" src="/images/slud_bildes'.$lang_dir.'/'.$pic[2].'">';
                        }
                        else
                            echo '<td bgcolor="#9CF576" width="60" height="50">';
                        echo '
                        </td>
                            ';
                        if (isset($pic[3])) 
                        {
                            echo '<td bgcolor="#CFECC4" width="60" height="50">';
                            echo '<img
                                onmouseover="img_big.src=\'/images/slud_bildes'.$lang_dir.'/'.$pic[3].'\';"
                                border="0" style="margin:0px;padding:0px;" width="60" src="/images/slud_bildes'.$lang_dir.'/'.$pic[3].'">';
                        }
                        else
                            echo '<td bgcolor="#9CF576" width="60" height="50">';
                        echo '
                        </td>
					</tr>
					<tr>
                        ';
                        if (isset($pic[4])) 
                        {
                            echo '<td bgcolor="#CFECC4" width="60" height="50">';
                            echo '<img
                            onmouseover="img_big.src=\'/images/slud_bildes'.$lang_dir.'/'.$pic[4].'\';"
                            border="0" style="margin:0px;padding:0px;" width="60" src="/images/slud_bildes'.$lang_dir.'/'.$pic[4].'">';
                        }
                        else
                            echo '<td bgcolor="#9CF576" width="60" height="50">';
                        echo '
                        </td>
                        ';
                        if (isset($pic[5])) 
                        {
                            echo '<td bgcolor="#CFECC4" width="60" height="50">';
                            echo '<img
                            onmouseover="img_big.src=\'/images/slud_bildes'.$lang_dir.'/'.$pic[5].'\';"
                            border="0" style="margin:0px;padding:0px;" width="60" src="/images/slud_bildes'.$lang_dir.'/'.$pic[5].'">';
                        }
                        else
                            echo '<td bgcolor="#9CF576" width="60" height="50">';
                        echo '
                        </td>
                        ';
                        if (isset($pic[6])) 
                        {
                            echo '<td bgcolor="#CFECC4" width="60" height="50">';
                            echo '<img
                            onmouseover="img_big.src=\'/images/slud_bildes'.$lang_dir.'/'.$pic[6].'\';"
                            border="0" style="margin:0px;padding:0px;" width="60" src="/images/slud_bildes'.$lang_dir.'/'.$pic[6].'">';
                        }
                        else
                            echo '<td bgcolor="#9CF576" width="60" height="50">';
                        echo '
                        </td>
					</tr>
				</table>
				<!-- end of inner picture table -->
    </td>';
}

echo '

		<td bgcolor="#F7FFF4" valign="top" style="padding:15px;" colspan="2">
                ';

            //$desc = $this->GetDescriptionByID($row['cat_id']);
            //echo $desc;

            echo '
            <div class="slud" style="text-decoration:none; FONT-WEIGHT: bold">';
                $put_comma = 0;
                if ($adv_type) 
                {
                    echo $adv_type;
                    $put_comma = 1;
                }
                
                if ($data['pilseta'])
                {
                    if ($put_comma) echo ', ';
                    echo $data['pilseta'];
                    $put_comma = 1;
                }
                if ($data['rajons'])
                {
                    if ($put_comma) echo ', ';
                    echo $data['rajons'];
                }
            echo '
            </div>
             ';

            echo '<div class=slud>'.$data['text'].'</div>';

if ($data['maja_platiba']) echo '<div class=slud>&nbsp;&nbsp;'.$this->texts[$lang_val]['platiba'].' '.$data['maja_platiba'].'</div>';
if ($data['maja_istabas']) echo '<div class=slud>&nbsp;&nbsp;'.$this->texts[$lang_val]['istabu_skaits'].' '.$data['maja_istabas'].'</div>';
if ($data['maja_stavu_skaits']) echo '<div class=slud>&nbsp;&nbsp;'.$this->texts[$lang_val]['stavu_skaits'].' '.$data['maja_stavu_skaits'].'</div>';
if ($data['maja_zemes_platiba']) echo '<div class=slud>&nbsp;&nbsp;'.$this->texts[$lang_val]['zemes_platiba'].' '.$data['maja_zemes_platiba'].'</div>';

echo '<div class="slud">'.$this->texts[$lang_val]['datums'].' '.date("Y-m-d",$data['adv_date_start']).'</div>';

            echo '<br><div class=slud style="text-decoration:none; FONT-WEIGHT: bold">'.$this->texts[$lang_val]['komplektacija'].'</div>';   
            echo '<div class="slud" style="FONT-WEIGHT: normal">';
            $comma_flag = false;
            foreach ($data_kompekt as $key=>$val)
            {
                if ($comma_flag) echo ', ';
                echo $val['ertiba_nosaukums'];
                $comma_flag = true;
            }
                
            echo '</div><br>';

            echo '<div class="slud" style="text-decoration:none; FONT-WEIGHT: bold">'.$this->texts[$lang_val]['kontakti'].'</div>';
            echo '<div class="slud">'.$this->texts[$lang_val]['telefons'].' '.$phone.'</div>';
            echo '<div class="slud">E-mail: <a href="mialto:"'.$email.'">'.$email.'</a></div>';

            echo '<br><div class=slud style="COLOR: #ee6219;FONT-WEIGHT: bold">'.$data['maja_cena'].'</div>';

            echo '
        </td>

		</tr>
	</table>
            ';
}



}

?>

