<?
//Serveris.LV Constructor component
//Created by Aleksandrs Selickis, 2004, alex@datateks.lv

include_once('class.dbcollection.php');

class galleriescollection extends dbcollection
{
  //Class initialization
  function galleriescollection($name,$id)
  {
    $this->dbcollection($name,$id);
    $this->type = "galleriescollection";

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "Gallery thumbnail path",
        "size"      => "35",
        "type"      => "str"
      ),

       "it1"    => Array(
        "label"     => "Gallery Title",
        "size"      => "35",
        "type"      => "str"
      ),

      "it2"    => Array(
        "label"     => "Gallery Date",
        "size"      => "35",
        "type"      => "str",
        timestamp => true
      ),

      "it3" => Array(
        "label"     => "Description:",
        "type"      => "wysiwyg",
        "rows"      => "7",
        "cols"      => "50",
        "dialog"    => "?module=wysiwyg&site_id=".$GLOBALS['site_id'],
        "dialogw"   => "720",
        "dialogh"   => "450",
        "dlg_res"   => true,
        "value"     => "<P>&nbsp;</P>"
      )

    );

    //Collection display table definition
    $this->columns = Array(

      "col0"        => Array(
        "width"     => "33%",
        "title"     => "Thumbnail path"
      ),

      "col1"        => Array(
        "width"     => "33%",
        "title"     => "Title"
      ),

      "col2"        => Array(
        "width"     => "33%",
        "title"     => "Date"
      )

    );




  }

  function IsEditableOutside()
  {
    $this->description = 'Galerijas';
    $this->longname = $this->name;
    return false;
  }

  function getGalleries ()
  {
    if(!$this->name)
    {
      $this->error = true;
      return false;
    }
    $data = sqlQueryData("SELECT * FROM `".$this->table."` ORDER BY ind");
    return $data;
  }


}

?>
