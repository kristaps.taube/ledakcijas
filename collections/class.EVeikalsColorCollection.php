<?
require_once('class.dbcollection.php');

class EVeikalsColorCollection extends dbcollection{
  
  function __construct($name,$id){

    $this->dbCollection($name,$id);
    $this->type = get_class();

    //Properties array definition
    $this->properties_assoc = Array(

      "group" => Array(
        "label"     => "Grupa:",
        "type"      => "relation",
        "related_collection" => "EVeikalsColorGroups",
        "relation_format" => "{%title%}",
      ),

      "title"    => Array(
        "label"     => "Nosaukums:",
        "type"      => "text",
        "multilang" => true
      ),

      "color"    => Array(
        "label"     => "Krāsa:",
        "type"      => "color"
      ),

			"image"    => Array(
        "label"     => "Bilde:",
        "type"      => "file",
        "upload_dir"    => "/files/colors/"
      ),

    );

    //Collection display table definition
    $this->columns = Array(

      "group"        => Array("title"     => "Grupa"),
      "title_lv"        => Array("title"     => "Nosaukums"),
      "color"        => Array("title"     => "Krāsa"),
			"image" => Array(
        "title"  => "Bilde",
        "format"    => "[if {%image%}]<img src='{%image%}' style='max-width: 100px;max-height: 100px' />[/if]"
      ),

    );

     $this->PostInit();
  }

  function IsEditableOutside(){
    $this->description = 'Krāsas';
    $this->longname = $this->name;
    return true;
  }

  function getIdsByGroup($group_id){

  	static $cache;

		if($cache[$group_id]) return $cache[$group_id];

		$cache[$group_id] = DB::GetColumn("SELECT item_id FROM `".$this->table."` WHERE `group` = :group", array(":group" => $group_id));

		return $cache[$group_id];

	}

  function GetEditData($page, &$count, &$recordposition, $order = ''){
    $query_params = $this->GetDefaultQueryParams($page, $order);
    $count = $this->ItemCount($query_params['countwhere']);
    if(isset($recordposition) && $recordposition){
      $recordposition = $this->GetDBRecordPosition($query_params, $recordposition);
    }
    $data = $this->GetDBData($query_params);

    foreach($data as &$entry){

      $entry['color'] = $entry['color'] ? ("<div style='float: left;margin-right: 10px;border: 1px solid black;width: 50px;height:50px;background:".$entry['color']."'></div> ".$entry['color']) : "";

    }

    return $data;

  }

}