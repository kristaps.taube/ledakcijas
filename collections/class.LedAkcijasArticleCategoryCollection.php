<?php

class LedAkcijasArticleCategoryCollection extends pdocollection
{

    public function __construct($name = '', $id = '')
    {

        parent::__construct($name, $id);

        $langs = [];
        foreach(getLanguages() as $short => $lang){
            $langs[$short] = $lang['fullname'];
        }

        //Properties array definition
        $this->properties_assoc = [

            "category_title" => [
                "label" => "Nosaukums:",
                "type" => "text",
                "multilang" => true
            ],
            "url" => [
                "label" => "URL:",
                "type" => "text",
                "multilang" => true
            ],
        ];

        //Collection display table definition
        $this->columns = [
            'category_title_lv' => ['title' => 'Nosaukums'],
            'raksti' => [
                "title"     => "Raksti",
                "subrelated_collection" => "LedAkcijasArticleCollection",
                "subrelated_field" => "category_id",
                "format"    => '[if {%_self%}==1]<b>{%_self%}</b> raksts[else]<b>{%_self%}</b> raksti[/if]',
            ],
        ];

        $this->PostInit();

    }

    function processItemBeforeSave($item_id, &$item, $create, &$jsmessage)
    {

        foreach(getLanguages() as $short => $lang){

            if(!$item['url_'.$short] && $item['category_title_'.$short]) $item['url_'.$short] = transliterateURL($item['category_title_'.$short]);

        }

        return parent::processItemBeforeSave($item_id, $item, $create, $jsmessage);
    }

    function getByUrl($url)
    {

        $cond = [];
        foreach(getLanguages() as $short => $lang){
            $cond[] = 'url_'.$short.' = :url';
        }

        return DB::GetRow('SELECT * FROM `'.$this->table.'` WHERE '.implode(' OR ', $cond), [':url' => $url]);
    }

    public function IsEditableOutside()
    {
        $this->description = 'Collection: '.get_class();
        $this->longname = $this->name;
        return true;
    }

}