<?

include_once('class.collection.php');

class miboipasiepiedcollection extends collection
{
  //Class initialization
  function miboipasiepiedcollection($name,$id)
  {
    $this->Collection($name,$id);
    $this->type = "miboipasiepiedcollection";

    //Properties array definition
    $this->properties = Array(

      "it0"     => Array(
        "label"     => "Title:",
        "type"      => "string"
      ),

      "it1"     => Array(
        "label"     => "Description:",
        "type"      => "string",
        "type"      => "html",
        "rows"      => "6",
        "cols"      => "30"
      ),

      "it2"     => Array(
        "label"     => "Image:",
        "type"      => "string",
        "type"      => "dialog",
        "dialog"    => "?module=dialogs&action=filex&site_id=".$GLOBALS['site_id']."&view=thumbs",
        "dialogw"   => "600"
      ),

      "it3"     => Array(
        "label"     => "Link:",
        "type"      => "dialog",
        "dialog"    => "?module=dialogs&action=links&site_id=".$GLOBALS['site_id']
      ),

      "it4"  => Array(
        "label"   => "Price:",
        "type"      => "string"
      ),


    );

    //Collection display table definition
    $this->columns = Array(

      "col0"        => Array(
        "width"     => "25%",
        "title"     => "Title"
      ),

      "col1"        => Array(
        "width"     => "25%",
        "title"     => "Description"
      ),

      "col3"        => Array(
        "width"     => "25%",
        "title"     => "Link"
      ),

      "col4"        => Array(
        "width"     => "25%",
        "title"     => "Price"
      ),



    );


  }

  function IsEditableOutside()
  {
    return false;
  }



}

?>
