<?php

class LedAkcijasSumDiscountCollection extends pdocollection
{

    public function __construct($name = '', $id = '')
    {

        parent::__construct($name, $id);

        //Properties array definition
        $this->properties_assoc = [

            "sum" => [
                "label" => "Summa no:",
                "type" => "text",
                "column_type" => "decimal(10,2)",
            ],
            "discount" => [
                "label" => "Atlaides apjoms(%):",
                "type" => "text"
            ],

        ];

        //Collection display table definition
        $this->columns = [
            'sum' => ['title' => 'Summa no'],
            'discount' => ['title' => 'Atlaides apjoms(%)'],
        ];

        $this->PostInit();

    }

    function processItemBeforeSave($item_id, &$item, $create, &$jsmessage)
    {

        #if(!$item['url']) $item['url'] = transliterateURL($item['title']);
        #if(!$item['date']) $item['date'] = date("Y-m-d H:i:s");
        return parent::processItemBeforeSave($item_id, $item, $create, $jsmessage);

    }

    public function getSumDiscount($sum)
    {

        $discount = 0;

        foreach($this->getSumDiscounts() as $dsum => $ddiscount){

            if($sum >= $dsum){
                $discount = $ddiscount;
            }

        }

        return $discount;

    }

    public function getSumDiscounts()
    {

        static $discounts;


        if(!$discounts){

            $discounts = [];
            foreach($this->getDBData(['order' => 'sum ASC']) as $entry){
                $discounts[$entry['sum']] = $entry['discount'] / 100;
            }

        }

        return $discounts;

    }

    public function IsEditableOutside()
    {
        $this->description = 'Collection: '.get_class();
        $this->longname = $this->name;
        return true;
    }

}