<?
include_once('class.dbcollection.php');

class EVeikalsSizeGroups extends dbcollection{

  function __construct($name,$id){
    $this->dbCollection($name,$id);
    $this->type = get_class();

    //Properties array definition
    $this->properties_assoc = Array(

      "group_title"    => Array(
        "label"     => "Nosaukums:",
        "type"      => "str",
        "multilang" => true
      ),

    );

    //Collection display table definition
    $this->columns = Array(

      "group_title_lv"        => Array(
        "width"     => "60%",
        "title"     => "Nosakums:"
      ),

      "params"    => Array(
        "title"     => "Izmēri",
        "subrelated_collection" => "EVeikalsSizeCollection",
        "subrelated_field" => "group",
        "format"    => '[if {%_self%}==1]<b>{%_self%}</b> izmērs[else]<b>{%_self%}</b> izmēri[/if]',
      ),

    );

     $this->PostInit();
  }

  function IsEditableOutside(){
    $this->description = 'Izmēru grupas';
    $this->longname = $this->name;
    return true;
  }

  function processFormBeforeDisplay(&$form_data, $create, $item){

  }

  function getByIds($ids){
    return sqlQueryData("SELECT * FROM `".$this->table."` WHERE item_id IN (".implode(",", $ids).") ORDER BY ind");
  }

}
