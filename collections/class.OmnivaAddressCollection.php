<?php

class OmnivaAddressCollection extends pdocollection
{

    public function __construct($name = '',$id = '')
    {

        parent::__construct($name, $id);

        //Properties array definition
        $this->properties_assoc = [

            "city" => [
                "label" => "Pilsēta:",
                "type" => "text",
                "multilang" => true
            ],

            "address" => [
                "label" => "Adrese:",
                "type" => "text",
                "multilang" => true
            ],

            "coords" => [
                "label" => "Koordinātas:",
                "type" => "text",
            ],

            "zip" => [
                "label" => "ZIP:",
                "type" => "text",
            ],

        ];

        //Collection display table definition
        $this->columns = [
            "city_lv" => ["title" => "Pilsēta"],
            "address_lv" => ["title" => "Adrese"],
            "zip" => ["title" => "zip"],
        ];

        $this->PostInit();

    }

    public function getByZip($zip)
    {
        return $this->getRow(['where' => 'zip = :zip', 'params' => [':zip' => $zip]]);
    }

    public function IsEditableOutside()
    {
        $this->description = 'Collection: '.get_class();
        $this->longname = $this->name;
        return true;
    }

}