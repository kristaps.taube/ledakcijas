<?
//Serveris.LV Constructor component
//Created by Aleksandrs Selickis, 2004, alex@datateks.lv

include_once('class.dbcollection.php');

class mailinglistcollection extends dbcollection
{
  //Class initialization
  function mailinglistcollection($name,$id)
  {
    $this->dbCollection($name,$id);
    $this->type = "mailinglistcollection";

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "Name",
        "size"      => "35",
        "type"      => "str"
      ),

      "it1"    => Array(
        "label"     => "Surname",
        "size"      => "35",
        "type"      => "str"
      ),

      "it2"    => Array(
        "label"     => "E-mail",
        "size"      => "35",
        "type"      => "str"
      ),

      "it3"    => Array(
        "label"     => "Status",
        "size"      => "35",
        "type"      => "str"
      ),

      "it4"    => Array(
        "label"     => "IP",
        "size"      => "35",
        "type"      => "str"
      ),

      "it5"    => Array(
        "label"     => "time",
        "size"      => "35",
        "type"      => "str"
      ),

      "it6"    => Array(
        "label"     => "hash",
        "size"      => "35",
        "type"      => "str"
      ),

    );

    //Collection display table definition
    $this->columns = Array(

      "col0"        => Array(
        "width"     => "20%",
        "title"     => "Name"
      ),

      "col1"        => Array(
        "width"     => "20%",
        "title"     => "Surname"
      ),

      "col2"        => Array(
        "width"     => "20%",
        "title"     => "E-mail"
      ),

      "col3"        => Array(
        "width"     => "10%",
        "title"     => "Status"
      ),

      "col4"        => Array(
        "width"     => "10%",
        "title"     => "IP"
      ),

      "col5"        => Array(
        "width"     => "10%",
        "title"     => "Time"
      ),

      "col6"        => Array(
        "width"     => "10%",
        "title"     => "Hash"
      ),

    );




  }

  function getRowByHash($hash)
  {
        $row = sqlQueryRow("select * from ".$this->site_id."_coltable_".$this->name." where col6='".$hash."'");
        return $row;
  }

  function getMaxInd()
  {
        $ind = sqlQueryValue("select max(ind) from ".$this->site_id."_coltable_".$this->name);
        return $ind;
  }

  function IsEditableOutside()
  {
    $this->description = $this->type;
    $this->longname = $this->name;
    return true;
  }







}

?>