<?
include_once('class.dbcollection.php');

class EVeikalsColorGroups extends dbcollection{

  function __construct($name,$id){
    $this->dbCollection($name,$id);
    $this->type = get_class();

    //Properties array definition
    $this->properties_assoc = Array(

      "title"    => Array(
        "label"     => "Nosaukums:",
        "type"      => "str"
      )

    );

    //Collection display table definition
    $this->columns = Array(

      "title"        => Array(
        "width"     => "60%",
        "title"     => "Nosakums:"
      ),

      "params"    => Array(
        "title"     => "Krāsas",
        "subrelated_collection" => "EVeikalsColorCollection",
        "subrelated_field" => "group",
        "format"    => '[if {%_self%}==1]<b>{%_self%}</b> krāsa[else]<b>{%_self%}</b> krāsas[/if]',
      ),


    );

     $this->PostInit();
  }

  function IsEditableOutside()
  {
    $this->description = 'Krāsu grupas';
    $this->longname = $this->name;
    return true;
  }

  function processFormBeforeDisplay(&$form_data, $create, $item){

  }

  function getByIds($ids){
    return sqlQueryData("SELECT * FROM `".$this->table."` WHERE item_id IN (".implode(",", $ids).") ORDER BY ind");
  }

}

?>
