<?php

use Constructor\App;

class Collection
{
  var $name;
  var $longname;
  var $site_id;
  var $type;
  var $collection_id;
  var $properties;
  var $columns;
  var $description;
  var $colproperties;

  //Class initialization
  function Collection($name, $id)
  {

    $this->name = $name;
    $this->site_id = isset(App::$app) && App::$app->getSiteId() ? App::$app->getSiteId() : $GLOBALS['site_id'];

    $this->type = "collection";
    $this->collection_id = intval($id);
    if((!$name)and($id))
      $this->name = $this->GetName();
    $this->properties = Array();
    $this->columns = Array();
    $this->colproperties = Array();
  }

  function IsEditableOutside()
  {
    $this->description = $this->type;
    $this->longname = $this->name;
    return false;
  }

  function GetDefaultOrder()
  {
    return 'ind';
  }

  function GetEditPageSize()
  {
    return 20;
  }

  function CanChangeOrder()
  {
    return true;
  }

  function CanBeRelated()
  {
    return false;
  }

  function IsAddToBottomMode()
  {
    return true;
  }

  //all parameters except offset and count are ignored
  function GetDBData($query_params = Array())
  {
    $data = sqlQueryData("SELECT collection_id, property_num, property_val, ind FROM ".$this->site_id."_colitems WHERE collection_id=".$this->collection_id." ORDER BY ind, property_num");

    if($data == null)
      return($data);

    $rdata = Array();
    $rrow = Array();
    $lastind = -1;
    $i = 0;
    foreach($data as $row)
    {
      if(($row['ind'] != $lastind)and($lastind != -1))
      {
        $rrow['ind'] = $lastind;
        $rrow['item_id'] = $lastind;
        $rdata[] = $rrow;
        $rrow = Array();
        $i = 0;
      }
      $lastind = $row['ind'];

      $rrow['col'.$i] = $row['property_val'];
      $i++;
    }
    $rrow['ind'] = $row['ind'];
    $rrow['item_id'] = $lastind;
    $rdata[] = $rrow;

    //slice to offset and count
    if(isset($query_params['offset']))
      $offset = $query_params['offset'];
    else
      $offset = '0';
    if(isset($query_params['count']))
      $count = $query_params['count'];
    else
      $count = '20000000';
    $rdata = array_slice($rdata, $offset, $count);

    return $rdata;
  }

  function GetDBRecordPosition($query_params, $recpos)
  {
    return $this->GetItemInd($recpos) - 1;
  }

    public function FindCollection($name)
    {
        static $cache;

        if($cache){
            foreach($this->getAllCollections() as $entry){
                $cache[$entry['type']][$entry['name']] = $entry['collection_id'];
            }
        }

        $id = isset($cache[$this->type][$name]) ? $cache[$this->type][$name] : null;

        if($id){
            $this->collection_id = $id;
        }

        return $id;

    }

    protected function getAllCollections()
    {

        static $cache;

        if(!$cache){
            $cache = DB::GetTable("SELECT * FROM ".$this->site_id."_collections");
        }

        return $cache;

    }

  function GetName(){

    if(empty($GLOBALS['CollectionNames'])){

        $data = DB::GetTable("SELECT collection_id, name FROM ".$this->site_id."_collections");
        foreach($data as $row){
            $GLOBALS['CollectionNames'][$row['collection_id']] = $row['name'];
        }

    }

    if($GLOBALS['CollectionNames'][$this->collection_id]){
      $name = $GLOBALS['CollectionNames'][$this->collection_id];
    }

    return $name;
  }

  public static function getCollectionById($site_id, $id)
    {
        static $cache;

        if(!isset($cache[$id])){
            $cache[$id] = DB::GetRow("SELECT * FROM ".$site_id."_collections WHERE collection_id = :id", [':id' => $id]);
        }

        return $cache[$id];

    }

  public static function getCollectionIds($site_id)
  {

    return DB::GetColumn("SELECT collection_id FROM ".$site_id."_collections");

  }

    function IsValidCollection(){

        if(empty($GLOBALS['CollectionIds'])){
            $GLOBALS['CollectionIds'] = self::getCollectionIds($this->site_id);
        }

        return (in_array($this->collection_id, $GLOBALS['CollectionIds'])) ? $this->collection_id : NULL;

    }

  function AddNewCollection($name)
  {
    $this->name = $name;
    sqlQuery("INSERT INTO ".$this->site_id."_collections (name, type) VALUES ('$name', '".$this->type."')");
    $lid = sqlQueryValue('SELECT LAST_INSERT_ID()');

    $this->collection_id = $lid;
    return $lid;
  }
  
  function DelCollection()
  {
    sqlQuery("DELETE FROM ".$this->site_id."_colitems WHERE collection_id=".$this->collection_id);
    sqlQuery("DELETE FROM ".$this->site_id."_collections WHERE collection_id=".$this->collection_id);
  }

  function Clear()
  {
    sqlQuery("DELETE FROM ".$this->site_id."_colitems WHERE collection_id=".$this->collection_id);
  }

  function AddNewItem()
  {
    $newind = 1 + sqlQueryValue("SELECT MAX(ind) FROM ".$this->site_id."_colitems WHERE collection_id=".$this->collection_id);
    $f = 0;
    foreach($this->properties as $key => $prop)
    {
      sqlQuery("INSERT INTO ".$this->site_id."_colitems (collection_id, property_num, property_val, ind) VALUES ('".$this->collection_id."', '".$f."', '', '".$newind."')");
      $f++;
    }
    return $newind;
  }
  
  function ChangeItemProperty($ind, $property_num, $property_val)
  {

    sqlQuery("UPDATE ".$this->site_id."_colitems SET property_val='".$property_val."' WHERE property_num='".$property_num."' AND collection_id='".$this->collection_id."' AND ind='".$ind."'");
  }
  
  //properties = array(value1, value2, ... )
  function ChangeItem($ind, $properties)
  {
    $f = 0;
    foreach($properties as $property)
    {

      $this->ChangeItemProperty($ind, $f, $property);
      $f++;
    }
    return $ind;
  }
  
  function DelItem($ind)
  {
    sqlQuery("DELETE FROM ".$this->site_id."_colitems WHERE collection_id='".$this->collection_id."' AND ind='$ind'");
    sqlQuery("UPDATE ".$this->site_id."_colitems SET ind=ind-1 WHERE collection_id='".$this->collection_id."' AND ind>'$ind'");
  }
  
  function GetItem($ind)
  {
    $data = sqlQueryData("SELECT property_num, property_val FROM ".$this->site_id."_colitems WHERE collection_id='".$this->collection_id."' AND ind='$ind'");
    $a = Array();
    foreach($data as $row)
    {
      $a[$row["property_num"]] = $row["property_val"];
    }
    return $a;
  }
  
  function ExistsItem($ind)
  {
    $i = sqlQueryValue("SELECT ind FROM ".$this->site_id."_colitems WHERE collection_id='".$this->collection_id."' AND ind='$ind'");
    if($i)
      return true;
    else
      return false;
  }

  function FirstItem()
  {
    $i = sqlQueryValue("SELECT Min(ind) FROM ".$this->site_id."_colitems WHERE collection_id='".$this->collection_id."' LIMIT 1");
    return $i;
  }

  function LastItem()
  {
    $i = sqlQueryValue("SELECT Max(ind) FROM ".$this->site_id."_colitems WHERE collection_id='".$this->collection_id."' LIMIT 1");
    return $i;
  }

  function ItemCount($where = '')
  {
    $i = sqlQueryValue("SELECT Count(DISTINCT ind) FROM ".$this->site_id."_colitems WHERE collection_id='".$this->collection_id."'");
    return $i;
  }

  function SwapItems($ind1, $ind2)
  {
    sqlQuery("DELETE FROM ".$this->site_id."_colitems WHERE collection_id='".$this->collection_id."' AND ind='-1'");
    sqlQuery("UPDATE ".$this->site_id."_colitems SET ind='-1' WHERE collection_id='".$this->collection_id."' AND ind='$ind1'");
    sqlQuery("UPDATE ".$this->site_id."_colitems SET ind='$ind1' WHERE collection_id='".$this->collection_id."' AND ind='$ind2'");
    sqlQuery("UPDATE ".$this->site_id."_colitems SET ind='$ind2' WHERE collection_id='".$this->collection_id."' AND ind='-1'");
  }
  
  function MoveItemUp($ind, $where = '')
  {
    if($ind > 1)
    {
      $this->SwapItems($ind, $ind-1);
      return($ind-1);
    }
    return($ind);
  }

  function MoveItemDown($ind, $where = '')
  {
    if($this->ExistsItem($ind+1))
    {
      $this->SwapItems($ind,$ind+1);
      return($ind+1);
    }
    return($ind);
  }

  function Edit()
  {
    EditCollection($this);
  }

  function EditToolbar()
  {
    $site_id = $this->site_id;
    $category_id = $this->collection_id;
    $collectiontype = $this->type;

    #$Toolbar->backendroot = $GLOBALS['cfgWebRoot'];

    /*$Toolbar = new Toolbar();
    $Toolbar->AddButton("add", "Add New", "add_component.gif", "openDialog('?module=collections&action=newcolitem&site_id=$site_id&page_id=$page_id&category_id=$category_id&coltype=".$collectiontype."&session_id=".session_id()."&session_name=".session_name()."', 400, 260, 1, 1);", "Add new collection item");
    $Toolbar->AddButton("edit", "Edit", "edit_properties.gif", "openDialog('?module=collections&action=editcolitem&site_id=$site_id&page_id=$page_id&category_id=$category_id&coltype=".$collectiontype."&session_id=".session_id()."&session_name=".session_name()."&id='+SelectedRowID, 400, 260, 1, 1);", "Edit collection item");
    $Toolbar->AddLink("up", "Up", "move_up.gif", "javascript:if(SelectedRowID)window.location='?module=collections&site_id=$site_id&page_id=$page_id&action=moveup&category_id=$category_id&coltype=".$_GET['coltype']."&justedit=".$_GET['justedit']."&session_id=".session_id()."&session_name=".session_name()."&main=1&selrow='+SelectedRowID+'&id='+SelectedRowID;", "");
    $Toolbar->AddLink("down", "Down", "move_down.gif", "javascript:if(SelectedRowID)window.location='?module=collections&site_id=$site_id&page_id=$page_id&action=movedown&category_id=$category_id&coltype=".$_GET['coltype']."&justedit=".$_GET['justedit']."&session_id=".session_id()."&session_name=".session_name()."&main=1&selrow='+SelectedRowID+'&id='+SelectedRowID;", "");
    $Toolbar->AddLink("del", "Delete", "delete_component.gif", "if(SelectedRowID)window.location='?module=collections&action=delcolitem&site_id=$site_id&page_id=$page_id&category_id=$category_id&coltype=".$collectiontype."&justedit=".$_GET['justedit']."&session_id=".session_id()."&session_name=".session_name()."&main=1&id='+SelectedRowID;", "Deletes selected collection item", "Are you sure you want to delete selected collection item?");
    if(!$_GET['justedit'])
    {
      $Toolbar->AddSeperator("210");
      $Toolbar->AddLink("delc", "Delete Collection", "delete_component.gif", "window.location='?module=collections&action=delcollection&site_id=$site_id&page_id=$page_id&category_id=$category_id&coltype=".$collectiontype."&main=1&session_id=".session_id()."&session_name=".session_name()."'", "Deletes collection and all items", "Are you sure you want to delete the collection and all items? You won\'t be able to undo this operation!");
    }
    return  $Toolbar->output();*/

    require($GLOBALS['cfgDirRoot']."library/"."class.toolbar2.php");
    $Toolbar = new Toolbar2();

    $Toolbar->AddSpacer();

    $Toolbar->AddButtonImageText("add", 'collection_new', "{%langstr:add_new%}", "", "openDialog('?module=collections&action=newcolitem&site_id=$site_id&page_id=$page_id&category_id=$category_id&coltype=".$collectiontype."&session_id=".session_id()."&session_name=".session_name()."', 400, 460, 1, 1);", 85, "{%langstr:hint_new_collection_item%}");
    $Toolbar->AddButtonImage("edit", 'collection_edit', "{%langstr:col_edit%}", "", "openDialog('?module=collections&action=editcolitem&site_id=$site_id&page_id=$page_id&category_id=$category_id&coltype=".$collectiontype."&session_id=".session_id()."&session_name=".session_name()."&id='+SelectedRowID, 400, 460, 1, 1);", 31, "{%langstr:hint_edit_collection_item%}");
    if($this->CanChangeOrder())
    {
      $Toolbar->AddButtonImage("up", 'up', "{%langstr:up%}", "javascript:if(SelectedRowID)window.location='?module=collections&site_id=$site_id&page_id=$page_id&action=moveup&category_id=$category_id&coltype=".$_GET['coltype']."&justedit=".$_GET['justedit']."&session_id=".session_id()."&session_name=".session_name()."&main=1&selrow='+SelectedRowID+'&id='+SelectedRowID;", '', 31, "");
      $Toolbar->AddButtonImage("down", 'down', "{%langstr:down%}", "javascript:if(SelectedRowID)window.location='?module=collections&site_id=$site_id&page_id=$page_id&action=movedown&category_id=$category_id&coltype=".$_GET['coltype']."&justedit=".$_GET['justedit']."&session_id=".session_id()."&session_name=".session_name()."&main=1&selrow='+SelectedRowID+'&id='+SelectedRowID;", '', 31, "");
    }
    $Toolbar->AddButtonImage("del", 'delete', "{%langstr:col_del%}", "javascript: if(SelectedRowID && SelectedRowID!=-1) window.location='?module=collections&action=delcolitem&site_id=$site_id&page_id=$page_id&category_id=$category_id&coltype=".$collectiontype."&justedit=".$_GET['justedit']."&session_id=".session_id()."&session_name=".session_name()."&main=1&id='+SelectedRowID; else if(!SelectedRowIDsEmpty) window.location='?module=collections&action=delcolitem&site_id=$site_id&page_id=$page_id&category_id=$category_id&coltype=".$collectiontype."&justedit=".$_GET['justedit']."&session_id=".session_id()."&session_name=".session_name()."&main=1&id='+SelectedRowIDs;", '', 31, "{%langstr:hint_delete_collection_item%}", "{%langstr:ask_delete_collection_item%}");
    $Toolbar->AddSeperator();
    if(!is_array($_SESSION['collections_viewfilter_' . $this->name]) || !count($_SESSION['collections_viewfilter_' . $this->name]))
      $filterimg = 'collection_find';
    else
      $filterimg = 'collection_find_checked';
    $Toolbar->AddButtonImage("filter", $filterimg, "{%langstr:col_filter%}", "", "openDialog('?module=collections&action=filtercollection&site_id=$site_id&page_id=$page_id&category_id=$category_id&coltype=".$collectiontype."&session_id=".session_id()."&session_name=".session_name()."', 400, 460, 1, 1);", 31, "{%langstr:hint_filter_collection%}");
    if($filterimg == 'collection_find_checked')
    {
      $Toolbar->AddButtonImageText("remfilter", 'collection_find_clear', "{%langstr:col_showall%}", "javascript:window.location='?module=collections&action=filtercollectionclear&site_id=$site_id&page_id=$page_id&category_id=$category_id&coltype=".$collectiontype."&justedit=".$_GET['justedit']."&session_id=".session_id()."&session_name=".session_name()."&main=1'", '', 66, "{%langstr:hint_collection_find_clear%}");
    }
    if(!$_GET['justedit'])
    {
      $Toolbar->AddSeperator();
      $Toolbar->AddButtonImage("properties", 'sitemap_properties', "{%langstr:toolbar_properties%}", "", "openDialog('?module=collections&action=colproperties&site_id=$site_id&page_id=$page_id&category_id=$category_id&coltype=".$collectiontype."&session_id=".session_id()."&session_name=".session_name()."', 400, 460, 1, 1);", 31, "{%langstr:hint_collection_properties%}");
      $Toolbar->AddButtonImage("copycol", 'sitemap_copy', "{%langstr:toolbar_copy_collection%}", "", "openDialog('?module=collections&action=copycollection&site_id=$site_id&page_id=$page_id&category_id=$category_id&coltype=".$collectiontype."&session_id=".session_id()."&session_name=".session_name()."', 400, 460, 1, 1);", 31, "{%langstr:hint_copy_collection%}");

      $Toolbar->AddSeperator();
      $Toolbar->AddSeperator();
      $Toolbar->AddButtonImage("delc", 'collection_delete', "{%langstr:toolbar_delete_collection%}", "javascript: window.location='?module=collections&action=delcollection&site_id=$site_id&page_id=$page_id&category_id=$category_id&coltype=".$collectiontype."&main=1&session_id=".session_id()."&session_name=".session_name()."'", '', 31, "{%langstr:hint_delete_collection%}", "{%langstr:ask_delete_collection%}");


    }

    $Toolbar->AddSeperator();

    $this->GetExtraToolbarButtons($Toolbar, $extraitembuttons, $extrabuttons);
    if($extraitembuttons)
    {
      foreach($extraitembuttons as $key => $item)
      {
        $extraitembuttons[$key] = '"' . $item . '"';
      }
      $extraitembuttons = ',' . implode(', ', $extraitembuttons);
    }else
    {
      $extraitembuttons = '';
    }
    if($extrabuttons)
    {
      foreach($extrabuttons as $key => $item)
      {
        $extrabuttons[$key] = '"' . $item . '"';
      }
      $extrabuttons = ',' . implode(', ', $extrabuttons);
    }else
    {
      $extrabuttons = '';
    }

    $return = $Toolbar->output();


    echo '<script language="JavaScript" type="text/javascript">
/*<![CDATA[*/
che = new Array("edit", "up", "down", "del"'.$extraitembuttons.');
che2 = new Array("add","edit", "up", "down", "del", "properties", "delc", "filter", "remfilter"'.$extrabuttons.');

DisableToolbarButtons(che);
/*]]>*/
</script>';

if($_GET['selrow'])
{
  echo '<script language="JavaScript">
        <!--
          var tablerow' . $_GET['selrow'] . ' = document.getElementById("tablerow' . $_GET['selrow'] . '");
          if(typeof(tablerow' . $_GET['selrow'] . ')!= "undefined" && tablerow' . $_GET['selrow'] . ')
          {
            preEl = tablerow' . $_GET['selrow'] . ';
            ChangeTextColor(tablerow' . $_GET['selrow'] . ',\'tableRowSel\');
            SelectedRowID = ' . $_GET['selrow'] . ';
            if(SelectedRowID){ EnableToolbarButtons(che); }else { DisableToolbarButtons(che); }
          }
        <!---->
        </script>';
}


    require($GLOBALS['cfgDirRoot']."library/"."class.rightmenu.php");
    $rightmenu = new rightmenu;

    $rightmenu->addItem('{%langstr:add_new%}','openDialog(\'?module=collections&action=newcolitem&site_id='.$site_id.'&page_id='.$page_id.'&category_id='.$category_id.'&coltype='.$collectiontype.'&session_id='.session_id().'&session_name='.session_name().'\', 400, 460, 1, 1);',false,false,'collection_new');
    $rightmenu->addItem('{%langstr:col_edit%}',"openDialog('?module=collections&action=editcolitem&site_id=$site_id&page_id=$page_id&category_id=$category_id&coltype=".$collectiontype."&session_id=".session_id()."&session_name=".session_name()."&id='+SelectedRowID, 400, 460, 1, 1);",false,false,'collection_edit');
    $rightmenu->addItem('{%langstr:up%}',"javascript:if(SelectedRowID)window.location='?module=collections&site_id=$site_id&page_id=$page_id&action=moveup&category_id=$category_id&coltype=".$_GET['coltype']."&justedit=".$_GET['justedit']."&session_id=".session_id()."&session_name=".session_name()."&main=1&selrow='+SelectedRowID+'&id='+SelectedRowID",false,false,'up');
    $rightmenu->addItem('{%langstr:down%}',"javascript:if(SelectedRowID)window.location='?module=collections&site_id=$site_id&page_id=$page_id&action=movedown&category_id=$category_id&coltype=".$_GET['coltype']."&justedit=".$_GET['justedit']."&session_id=".session_id()."&session_name=".session_name()."&main=1&selrow='+SelectedRowID+'&id='+SelectedRowID;",false,false,'down');
    $rightmenu->addItem('{%langstr:col_del%}',"javascript: var agree = confirm(unescape('Are you sure to delete?')); if (agree) { if(SelectedRowID)window.location='?module=collections&action=delcolitem&site_id=$site_id&page_id=$page_id&category_id=$category_id&coltype=".$collectiontype."&justedit=".$_GET['justedit']."&session_id=".session_id()."&session_name=".session_name()."&main=1&id='+SelectedRowID; }",false,false,'delete');

/*    if(!$_GET['justedit'])
    {
      $rightmenu->addSpacer();
      $rightmenu->addSpacer();
      $rightmenu->addSpacer();
      $rightmenu->addItem('{%langstr:toolbar_delete_collection%}',"javascript: var agree = confirm(unescape('Are you sure to delete?')); if (agree) { window.location='?module=collections&action=delcollection&site_id=$site_id&page_id=$page_id&category_id=$category_id&coltype=".$collectiontype."&main=1&session_id=".session_id()."&session_name=".session_name()."' }",false,false,'collection_delete');
    }*/ 

    return  $return.$rightmenu->output();
  }

  function GetExtraToolbarButtons($Toolbar, &$extraitembuttons, &$extrabuttons)
  {
    $extrabuttons = Array();
    $extraitembuttons = Array();
  }

  function GetDefaultQueryParams($page, $order, $mustfilter = true)
  {
    $query_params = Array('what' => '`' . $this->table . '`.*', 'table' => '`' . $this->table . '`', 'where' => '1', 'countwhere' => '1');
    if($page != -1)
    {
      if(!$order)
        $query_params['order'] = $this->GetDefaultOrder();
      else
        $query_params['order'] = $order;
      $query_params['offset'] = $page * $this->GetEditPageSize();
      $query_params['count'] = $this->GetEditPageSize();
    }

    $properties = $this->GetDBProperties();
    //do we have any related tables in columns?
    foreach($this->columns as $colname => $colparams)
    {
      if($properties[$colname]['related_collection'])
      {
        $relcol = $this->InitializeRelatedCollection($colname, $properties[$colname]['related_collection']);
        if($relcol)
        {
          $query_params['table'] = $query_params['table'] . ' LEFT JOIN `' . $relcol->table . '`';
          $query_params['table'] .= ' ON `' . $this->table . '`.`' . $colname . '` = `' . $relcol->table . '`.item_id';
          if($colparams['format'])
            $format = $colparams['format'];
          else
          {
            $format = $properties[$colname]['relation_format'];
            $this->columns[$colname]['format'] = $format;
          }
          $fields = $relcol->ExtractFieldsFromFormat($format);
          foreach($fields as $field)
          {
            $query_params['what']  .= ', `' . $relcol->table . '`.`' . $field . '`';
          }
        }
      //do we have any subrelate tables
      }else if($colparams['subrelated_collection'])
      {
        $relcol = $this->InitializeRelatedCollection($colname, $colparams['subrelated_collection']);
        if($relcol)
        {
          $query_params['what'] .= ', (SELECT COUNT(*) FROM `' . $relcol->table . '` WHERE `' . $relcol->table . '`.`' . $colparams['subrelated_field'] . '` = `' . $this->table . '`.item_id) AS `' . $colname . '`';
        }
      }
    }

    //add filters
    if($mustfilter)
    {
      $filterfields = $_SESSION['collections_viewfilter_' . $this->name];
      $filterfields = $this->CleanAssocArray($filterfields);
      if($filterfields)
      {
        foreach($filterfields as $fieldname => $filterval)
        {
          if($filterval)
          {
            $query_params['where'] .= ' AND `' . $this->table . '`.`' . $fieldname . '` LIKE "' . $filterval . '"';
            $query_params['countwhere'] .= ' AND `' . $this->table . '`.`' . $fieldname . '` LIKE "' . $filterval . '"';
          }
        }
      }
    }

    return $query_params;
  }

  //collection default editor events
  function GetEditData($page, &$count, &$recordposition, $order = '')
  {
    $query_params = $this->GetDefaultQueryParams($page, $order);
    $count = $this->ItemCount($query_params['countwhere']);
    if(isset($recordposition) && $recordposition)
    {
      $recordposition = $this->GetDBRecordPosition($query_params, $recordposition);
    }
    return $this->GetDBData($query_params);
  }

  function processFormBeforeDisplay(&$form_data, $create, $item)
  {
  }

  function processItemBeforeSave($item_id, &$item, $create, &$jsmessage)
  {
    $jsmessage = '';
    return true;
  }

  function processItemAfterSave($item_id, $item, $create)
  {
  }

  function processDBProperties(&$form_data)
  {
  }


  //returns property for collection with specified ID or default property for all collections of this type
  //page unspecified for both cases
  function getProperty($propertyname)
  {
    $val = null;
    if($this->collection_id)
      $val = sqlQueryValue('SELECT propertyvalue FROM '. $this->site_id . '_colproperties WHERE collection_id=' . $this->collection_id . ' AND propertyname="' . $propertyname . '" AND page_id=-1');
    if($val == null)
      $val = sqlQueryValue('SELECT propertyvalue FROM '. $this->site_id . '_colproperties WHERE collection_id=-1 AND propertyname="' . $propertyname . '" AND coltype="'.$this->type.'" AND page_id=-1');
    return $val;
  }


  function setProperty($propertyname, $propertyvalue)
  {
    if($this->collection_id)
    {
      sqlQuery('DELETE FROM '. $this->site_id . '_colproperties WHERE collection_id=' . $this->collection_id . ' AND propertyname="' . $propertyname . '" AND page_id=-1');
      sqlQuery('INSERT INTO '. $this->site_id . '_colproperties (coltype, page_id, collection_id, propertyname, propertyvalue)
                 VALUES ("'.$this->type.'", -1, ' . $this->collection_id . ', "'.$propertyname.'", "'.$propertyvalue.'")');
    }
  }

  //returns property for collection with specified ID and page
  function getPropertyForPage($page_id, $propertyname)
  {
    $val = null;
    if($this->collection_id)
      $val = sqlQueryValue('SELECT propertyvalue FROM '. $this->site_id . '_colproperties WHERE page_id='.$page_id.' AND collection_id=' . $this->collection_id . ' AND propertyname="' . $propertyname . '"');
    return $val;
  }


  function setPropertyForPage($page_id, $propertyname, $propertyvalue)
  {
    if($this->collection_id)
    {
      sqlQuery('DELETE FROM '. $this->site_id . '_colproperties WHERE collection_id=' . $this->collection_id . ' AND propertyname="' . $propertyname . '" AND page_id=' . $page_id);
      sqlQuery('INSERT INTO '. $this->site_id . '_colproperties (coltype, page_id, collection_id, propertyname, propertyvalue)
                 VALUES ("'.$this->type.'", '.$page_id.', ' . $this->collection_id . ', "'.$propertyname.'", "'.$propertyvalue.'")');
    }
  }

  function postInit()
  {
    //nothing
  }

  //compatibility functions for in-cms editing
  function GetDBProperties()
  {
    $properties = $this->properties;
    $this->processDBProperties($properties);
    return $properties;
  }

  function GetItemAssocFromPost()
  {
    $properties = $this->getDBProperties();
    $newproperties = Array();
    foreach($properties as $key => $property)
    {
      $newproperties[str_replace('it', 'col', $key)] = $_POST[$key];
    }
    return $newproperties;
  }

  //properties = array('col0' => 'value', 'col1' => 'value', ...)
  function AddItemAssoc($properties)
  {
    if(!$this->name)
    {
      $this->error = true;
      return false;
    }
    $ind = $this->AddNewItem();
    $this->ChangeItemByIdAssoc($ind, $properties);

    return $ind;
  }

  //tread ind as item_id for base collection
  function GetItemByIDAssoc($item_id)
  {
    $item = $this->GetItem($item_id);
    $itemr = Array();
    foreach($item as $key => $val)
    {
      $itemr['col' . $key] = $val;
    }
    return $itemr;
  }


  function ChangeItemByIdAssoc($ind, $properties)
  {
    foreach($properties as $key => $val)
    {
      if(substr($key, 0, 3) == 'col')
      {
        $it = substr($key, 3);
        if(is_numeric($it))
        {
          $this->ChangeItemProperty($ind, $it, $val);
        }
      }
    }
    return $id;
  }

  function DelDBItem($ind)
  {
    $this->DelItem($ind);
  }

  function GetItemInd($id)
  {
    return $id;
  }

  function GetIdSelectionLookup($format, $value)
  {
    return Array($value . ':&lt;' . $value . '&gt;');
  }

  function GetDisplayColumns()
  {
    return $this->columns;
  }

  function CleanAssocArray($properties)
  {
    return $properties;
  }


  //default save handlers for item manipulation
  // edit/add form is displayed by component, not collection
  function saveAddForComponents()
  {
    $this->execAddItemSave('?module=collections&action=newcolitem');
  }

  function saveEditForComponents()
  {
    $this->execModItemSave(intval($_GET['id']), '?module=collections&action=editcolitem');
  }

  function moveItemUpForComponents($ids)
  {
    foreach($ids as $id)
    {
      $ind = $this->GetItemInd($id);
      $this->MoveItemUp($ind);
    }
  }

  function moveItemDownForComponents($ids)
  {
    foreach($ids as $id)
    {
      $ind = $this->GetItemInd($id);
      $this->MoveItemDown($ind);
    }
  }

  function deleteItemForComponents($ids)
  {
    foreach($ids as $id)
    {
      $this->DelDBItem($id);
    }
  }


  function displayAddEditForm($create, $id, $action, $title = 'Item Properties', $buttons = true)
  {
    $form_data = $this->GetDBProperties();

    if(!$create)
    {
      $item = $this->getItemByIdAssoc($id);
      foreach($form_data as $key => $form_property)
      {
        if($form_property['column_name'])
          $form_data[$key]['value'] = $item[$key];
        else
          $form_data[$key]['value'] = $item[str_replace('it', 'col', $key)];
      }
    }
    else
    {
      $item = false;
    }

    $this->processFormBeforeDisplay($form_data, $create, $item);


    //display fields if redirected from save handler with error
    foreach(array_keys($_GET) as $key){
      $s = explode('_', $key, 2);
      if($s[0] == 'formdata' && isset($form_data[$s[1]]))
        $form_data[$s[1]]['value'] = stripslashes($_GET[$key]);
    }

    if($_GET['jsmessage'])
    {
      echo '<script language="JavaScript">
            <!--
            alert("' . stripslashes($_GET['jsmessage']) . '");
            <!---->
            </script>';
    }

    require($GLOBALS['cfgDirRoot']."library/"."class.formgen.php");
    $FormGen = new FormGen();
    $FormGen->action = $action;
    $FormGen->title = $title;
    $FormGen->cancel =  'close';
    $FormGen->properties = $form_data;
    $FormGen->buttons = $buttons;
    echo $FormGen->output();
  }


  function execAddItemSave($errorlink = '')
  {
    $properties = $this->GetItemAssocFromPost();

    $properties = $this->processPropertyDataTypes($properties);

    if($this->processItemBeforeSave(0, $properties, true, $jsmessage))
    {
      if($properties !== false)
      {
        $ind = $this->AddItemAssoc($properties, $this->IsAddToBottomMode());
        $id = $this->GetItemId($ind);
        $this->processItemAfterSave($id, $properties, true);
      }
    }
    else
    {
      $formdata = '';
      foreach($_POST as $key => $val)
      {
        $formdata .= '&formdata_' . $key . '=' . urlencode($val);
      }
      if($errorlink)
        Redirect($errorlink . '&jsmessage='.$jsmessage.'&site_id='.$this->site_id.'&category_id='.$this->collection_id . $formdata);
    }
  }


  function execModItemSave($id, $errorlink = '')
  {
    $properties = $this->GetItemAssocFromPost();
		$properties = $this->processPropertyDataTypes($properties);

    if($this->processItemBeforeSave($id, $properties, false, $jsmessage))
    {
      if($properties !== false)
      {
        $this->ChangeItemByIdAssoc($id, $properties);
        $this->processItemAfterSave($id, $properties, false);
      }
    }
    else
    {
      $formdata = '';
      foreach($properties as $key => $val)
      {
        $formdata .= '&formdata_' . $key . '=' . urlencode(stripslashes($val));
      }
      Redirect($errorlink . '&id='.$id.'&jsmessage='.$jsmessage.'&site_id='.$this->site_id.'&category_id='.$this->collection_id . $formdata);
    }
  }

}

?>
