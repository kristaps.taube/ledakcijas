<?
//Serveris.LV Constructor component
//Created by Aleksandrs Selickis, 2004, alex@datateks.lv

include_once('class.collection.php');

class gallerycollection extends collection
{
  //Class initialization
  function gallerycollection($name,$id)
  {
    $this->Collection($name,$id);
    $this->type = "gallerycollection";

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "Thumbnail path",
        "size"      => "35",
        "type"      => "str"
      ),

      "it1"    => Array(
        "label"     => "Image path",
        "size"      => "35",
        "type"      => "str"
      ),

      "it2"    => Array(
        "label"     => "Photographer",
        "size"      => "35",
        "type"      => "str"
      ),

      "it3"    => Array(
        "label"     => "Phone",
        "size"      => "35",
        "type"      => "str"
      ),

      "it4" => Array(
        "label"     => "Text:",
        "type"      => "customtext",
        "rows"      => "7",
        "cols"      => "50",
        "dialog"    => "?module=wysiwyg&site_id=".$this->site_id,
        "dialogw"   => "720",
        "dialogh"   => "450",
        "dlg_res"   => true
      ),

      "it5"    => Array(
        "label"     => "Is link",
        "type"      => "hidden"
      ),

    );

    //Collection display table definition
    $this->columns = Array(

      "col0"        => Array(
        "width"     => "20%",
        "title"     => "Thumbnail path"
      ),

      "col1"        => Array(
        "width"     => "20%",
        "title"     => "Image path"
      ),

      "col2"        => Array(
        "width"     => "15%",
        "title"     => "Photographer"
      ),

      "col3"        => Array(
        "width"     => "10%",
        "title"     => "Phone"
      ),

      "col4"        => Array(
        "width"     => "30%",
        "title"     => "Text"
      ),

      "col5"        => Array(
        "width"     => "5%",
        "title"     => "Is link"
      )

    );




  }




}

?>
