<?
//Serveris.LV Constructor component
//Created by Aleksandrs Selickis, 2004, alex@datateks.lv

include_once('class.dbcollection.php');

class hotlinkscollection extends dbcollection
{
  //Class initialization
  function hotlinkscollection($name,$id)
  {
    $this->dbCollection($name,$id);
    $this->type = "hotlinkscollection";

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "userid",
        "size"      => "35",
        "type"      => "str"
      ),

      "it1"    => Array(
        "label"     => "userind",
        "size"      => "35",
        "type"      => "str"
      ),

      "it2"    => Array(
        "label"     => "title",
        "size"      => "35",
        "type"      => "str"
      ),

      "it3"    => Array(
        "label"     => "link",
        "size"      => "35",
        "type"      => "str"
      )

    );

    //Collection display table definition
    $this->columns = Array(

      "col0"        => Array(
        "width"     => "40%",
        "title"     => "User ID"
      ),

      "col1"        => Array(
        "width"     => "40%",
        "title"     => "User index"
      ),

      "col2"        => Array(
        "width"     => "20%",
        "title"     => "Title"
      ),

      "col3"        => Array(
        "width"     => "20%",
        "title"     => "Link"
      )

    );

  }


  function GetUserLinks($uid)
  {
     $data = sqlQueryData("select ind,col2,col3 from ".$this->site_id."_coltable_".$this->name." where col0=".$uid." order by col1 desc");
     return $data;
  }

  function DelDBItem($ind, $uid)
  {
     $cur = sqlQueryValue("select col1 from ".$this->site_id."_coltable_".$this->name." where col0=".$uid." and ind='".$ind."'");
     echo $cur;
     if ($cur)
     {
        sqlQuery("update ".$this->site_id."_coltable_".$this->name." set col1=col1-1 
                where col1>'".$cur."' and col0='".$uid."'");
        $this->DelItem($ind);
     }
  }

  function AddDBItem($url,$title,$uid)
  {
     if (sqlQueryValue("select count(*) from ".$this->site_id."_coltable_".$this->name." where col3='".$url."' and col0='".$uid."'")>0)
      {
        //index tekoshai lapai
        $cur = sqlQueryValue("select col1 from ".$this->site_id."_coltable_".$this->name." where col0=".$uid." and col3='".$url."'");

        //paarbiida uz augshu
        $max = sqlQueryValue("select max(col1)from ".$this->site_id."_coltable_".$this->name." where col0=".$uid);
        //echo $max;
        $min = sqlQueryValue("select min(col1)from ".$this->site_id."_coltable_".$this->name." where col0=".$uid."");
        //echo $min;

        /*
        if ($max>$cur && $max>$min)
        {
            sqlQuery("update ".$this->site_id."_coltable_".$this->name." set col1='-1' 
                where col1='".$max."' and col0='".$uid."'");
            sqlQuery("update ".$this->site_id."_coltable_".$this->name." set col1='".$max."' 
                where col1='".$min."' and col0='".$uid."'");
            sqlQuery("update ".$this->site_id."_coltable_".$this->name." set col1='".$min."' 
                where col1='-1' and col0='".$uid."'");
        }*/
        if ($max>$cur && $max>$min)
        {
            sqlQuery("update ".$this->site_id."_coltable_".$this->name." set col1='-1' 
                where col1='".$cur."' and col0='".$uid."'");
            sqlQuery("update ".$this->site_id."_coltable_".$this->name." set col1=col1-1 
                where col1>'".$cur."' and col0='".$uid."'");
            sqlQuery("update ".$this->site_id."_coltable_".$this->name." set col1='".$max."' 
                where col1='-1' and col0='".$uid."'");
        }
        /*
        (2) -> 4
        1 
        2  
        3 2
        4 3
        */
      }
     else
      {
        //veido jaunu
        $max = sqlQueryValue("select max(col1)from ".$this->site_id."_coltable_".$this->name." where col0=".$uid);
        if (!$max) $max = 1;
        else $max++;
		$lastindex=$this->AddNewItem($lastindex);
		$properties=array($uid,$max,$title,$url);
		$this->ChangeItem($lastindex,$properties);
      }
     
  }

}

?>