<?
//Serveris.LV Constructor collection module
//Created by Aleksandrs Selickis, 2004

include_once('class.collection.php');

class FlashCollection extends collection
{
  //Class initialization
  function FlashCollection($name,$id)
  {
    $this->Collection($name,$id);
    $this->type = "flashcollection";

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "Param name",
        "size"      => "35",
        "type"      => "str"
      ),

      "it1"    => Array(
        "label"     => "Param value",
        "size"      => "35",
        "type"      => "str"
      )

    );

    //Collection display table definition
    $this->columns = Array(

      "col0"        => Array(
        "width"     => "50%",
        "title"     => "Param name"
      ),

      "col1"    => Array(
        "width"     => "50%",
        "title"     => "Param value"
      )

    );




  }




}

?>
