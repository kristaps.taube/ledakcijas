<?php

use Constructor\WebApp;

class EVeikalsFilters extends pdocollection{

  function __construct($name = '', $id = ''){

    parent::__construct($name,$id);

    //Properties array definition
    $this->properties_assoc = Array(

      "param_title"    => Array(
        "label"     => "Nosaukums:",
        "type"      => "str",
        "multilang" => true
      ),

      "group" => Array(
        "label"     => "Grupa:",
        "type"      => "relation",
        "related_collection" => "EVeikalsFilterGroups",
        "relation_format" => "{%title_lv%}",
      ),



    );

    //Collection display table definition
    $this->columns = Array(

      "group"        => Array(
        "width"     => "40%",
        "title"     => "Grupa:"
      ),

      "param_title_lv"        => Array(
        "width"     => "40%",
        "title"     => "Nosakums:"
      ),

    );

     $this->PostInit();
  }

  function IsEditableOutside(){
    $this->description = 'Filtri';
    $this->longname = $this->name;
    return true;
  }

  function processFormBeforeDisplay(&$form_data, $create, $item){


  }

    public function addIfNotExist($filter, $group_id)
    {

        static $added;

        $check = DB::GetRow("SELECT * FROM `".$this->table."` WHERE param_title_lv = :title AND `group` = :group", [':title' => $filter, ':group' => $group_id]);

        if(!$check){

            $data= ['group' => $group_id];
            foreach(getLanguages() as $short => $lang){
                $data['param_title_'.$short] = $filter;
            }

            $id = $this->Insert($data);

        }

        return $check ? $check['item_id'] : $id;

    }

  function getGroupedByIds($ids, $prodfilterrel = false){

    $return = Array();

    if(!empty($ids)){

      $query = "SELECT f.* ";
      $query .= "FROM `".$this->table."` AS f WHERE item_id IN(".implode(",", $ids).")";

      $filters = sqlQueryData($query);

      foreach($filters as $filter){
        $return[$filter['group']][] = $filter['item_id'];
      }

    }

    return $return;

  }

  function getByGroupAndProd($group, $prod_id, $ProdFilterRel){

    $prod_filters = $ProdFilterRel->getFilterIds($prod_id);
    if(empty($prod_filters)) return Array();
    return sqlQueryData("SELECT * FROM `".$this->table."` WHERE `group` = '".$group."' AND item_id IN (".implode(",", $prod_filters).") ORDER BY ind");

  }

  public function getByGroupIds($group_ids, $lang)
  {

    if(!$group_ids) return [];

    $data = [];

    foreach($this->getDBData(['order' => 'param_title_'.$lang,'where' => '`group` in ('.implode(',', $group_ids).')']) as $entry){

        if(!isset($data[$entry['group']])) $data[$entry['group']] = [];

        $data[$entry['group']][] = $entry;

    }

    foreach($data as $group => $filters){
        #usort($filters, array($this, 'sortGroupedFilters'));
        #$data[$group] = $filters;
    }

    return $data;

  }

  public static function sortGroupedFilters($a, $b)
  {

    $sort_field = 'param_title_'.WebApp::$app->getLanguage();

    if(is_numeric($a[$sort_field])){
        $a = (float)$a[$sort_field];
    }
    if(is_numeric($b[$sort_field])){
        $b = (float)$b[$sort_field];
    }

    if($a[$sort_field] > $b[$sort_field]){
        return -1;
    }elseif($a[$sort_field] < $b[$sort_field]){
        return 1;
    }else{
        return 0;
    }

  }

  function getByGroups($groups, $prodfilterrel = false, $prods = false, $cat_ids = false){

  	static $cache;

    $return = array();

    foreach($groups as $group_id){

    	if($cache[$group_id]){
    		$return[$group_id] = $cache[$group_id];
    	}else{
    		$must_query[] = $group_id;
    	}

    }

    if(count($return) == count($groups)) return $return;


    $query = "SELECT f.*";

    if($prodfilterrel && $prods && $cat_ids !== false){
      $query .= ", (
        SELECT COUNT(*)
        FROM `".$prodfilterrel->table."` as p
        LEFT JOIN `".$prods->table."` as pr ON pr.item_id = p.product_id
        WHERE
          p.filter_id = f.item_id AND
          pr.disabled = 0 AND
          pr.category_id IN(".implode(",", $cat_ids).")
      ) as count ";
    }

    $query .= "FROM `".$this->table."` as f WHERE `group` IN(".implode(",", $must_query).") ORDER BY ind";

    foreach(DB::GetTable($query) as $row){
        $return[$row['group']][] = $row;
        $cache[$row['group']][] = $row;
    }

    return $return;

  }

  function getByGroup($group, $prodfilterrel = false, $prods = false, $cat_ids = false){

    static $cache;

    if($cache[$group]) return $cache[$group];

    $query = "SELECT f.*";

    if($prodfilterrel && $prods && $cat_ids !== false){
      $query .= ", (
        SELECT COUNT(*)
        FROM `".$prodfilterrel->table."` as p
        LEFT JOIN `".$prods->table."` as pr ON pr.item_id = p.product_id
        WHERE
          p.filter_id = f.item_id AND
          pr.disabled = 0 AND
          pr.category_id IN(".implode(",", $cat_ids).")
      ) as count ";
    }

    $query .= "FROM `".$this->table."` as f WHERE `group` = '".$group."' ORDER BY ind";

    $cache[$group] = sqlQueryData($query);
    return $cache[$group];

  }

  function getByIds($ids){

    return sqlQueryData("SELECT * FROM `".$this->table."` WHERE item_id IN (".implode(",", $ids).")");

  }


}

