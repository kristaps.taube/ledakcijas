<?
require_once('class.pdocollection.php');

class EVeikalsCustomFieldValuesCollection extends pdocollection{

  function __construct($name,$id){

    parent::__construct($name,$id);
    $this->type = get_class();

    //Properties array definition
    $this->properties_assoc = Array(

      "field_id" => Array(
        "label"     => "Field:",
        "type"      => "relation",
				"column_type" => "int(32)",
				"column_index" => 1,
        "related_collection" => "EVeikalsCustomFieldCollection",
        "relation_format" => "{%fieldname_lv%}",
      ),

			"value"    => Array(
        "label"     => "Vērtība:",
        "type"      => "str",
        "multilang" => true
      ),

    );

    //Collection display table definition
    $this->columns = Array(

      "field_id"        => Array(
        "title"     => "Lauks:"
      ),

			"value_lv"        => Array(
        "title"     => "Vērtība:"
      ),

    );

    $this->PostInit();

	}

  function getByField($field_id){
  	return DB::GetTable("SELECT * FROM `".$this->table."` WHERE field_id = :fid ORDER BY ind", array(":fid" => $field_id));
  }

  function IsEditableOutside(){
    $this->description = 'Collection: '.get_class();
    $this->longname = $this->name;
    return true;
  }

  function processFormBeforeDisplay(&$form_data, $create, $item){


  }


}