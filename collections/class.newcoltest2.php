<?
//Serveris.LV Constructor component
//Created by Aivars Irmejs, 2006, aivars@datateks.lv

include_once('class.dbcollection.php');

class newcoltest2 extends dbcollection
{
  //Class initialization
  function newcoltest2($name,$id)
  {
    $this->DBCollection($name,$id);
    $this->type = "newcoltest2";

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "Vards:",
        "size"      => "35",
        "type"      => "str",
      ),

      "it1"    => Array(
        "label"     => "E-pasts:",
        "size"      => "35",
        "type"      => "str",
      ),

      "it2"    => Array(
        "label"     => "Bilde:",
        "type"      => "file",
      )

    );

    //Collection display table definition
    $this->columns = Array(

      "col0"        => Array(
        "width"     => "",
        "title"     => "Name",
      ),

      "col1"        => Array(
        "width"     => "",
        "title"     => "E-pasts",
      ),

      "col2"        => Array(
        "width"     => "",
        "title"     => "Bilde",
      )
    );

    $this->postInit();
  }

  function processItemBeforeSave($ind, &$item, $create, &$jsmessage)
  {
    if($item['col0'])
    {
      $jsmessage = '';
      return true;
    }else
    {
      $jsmessage = 'Ludzu noradiet vardu!';
      return false;
    }
  }


  function IsEditableOutside()
  {
    $this->description = 'Testejam ne-asociativo kolekciju redigesanu';
    $this->longname = $this->name;
    return true;
  }



}

?>
