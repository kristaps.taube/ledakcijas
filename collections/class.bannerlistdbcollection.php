<?php

class bannerlistdbcollection extends pdocollection{

  //Class initialization
  function bannerlistdbcollection($name = '', $id = '')
  {

    parent::__construct($name, $id);

    //Properties array definition
    $this->properties_assoc = Array(

      "position"    => Array(
        "label"     => "Position:",
        "size"      => "35",
        "type"      => "list",
        "lookup"    => Array("top:Top", "left:Left"),
      ),

      "image"    => Array(
        "label"     => "Image path",
        "type"      => "dialog",
        "dialog"    => "?module=dialogs&action=filex&site_id=".$GLOBALS['site_id']."&view=thumbs",
        "dialogw"   => "600",
      ),

      "description"    => Array(
        "label"     => "Description",
        "size"      => "35",
        "type"      => "str",
      ),

	    "link"    => Array(
        "label"     => "Link:",
        "type"      => "dialog",
        "dialog"    => "?module=dialogs&action=links&site_id=".$GLOBALS['site_id'],
      ),

      "width"    => Array(
        "label"     => "Width:",
        "size"      => "35",
        "type"      => "str",
      ),

      "height"    => Array(
        "label"     => "Height:",
        "size"      => "35",
        "type"      => "str",
      ),

      "target"    => Array(
        "label"     => "Target:",
        "size"      => "35",
        "type"      => "list",
        "lookup"    => Array(":Same page", "_blank:New Page"),
      ),

      "script"    => Array(
        "label"     => "Banner script:",
        "size"      => "35",
        "type"      => "html",
        "cols" => "40",
        "rows" => "6"
      ),

      "views"    => Array(
        "label"     => "Views:",
        "size"      => "35",
        "type"      => "str",
        "column_type" => "int(32)"
      ),

      "view_limit"    => Array(
        "label"     => "View limit:",
        "size"      => "35",
        "type"      => "str",
        "column_type" => "int(32)"
      ),

      "hits"    => Array(
        "label"     => "Hits:",
        "size"      => "35",
        "type"      => "str",
        "column_type" => "int(32)"
      ),

      "hit_limit"    => Array(
        "label"     => "Hit limit:",
        "size"      => "35",
        "type"      => "str",
        "column_type" => "int(32)"
      ),

      "date_start"    => Array(
        "label"     => "Date start:",
        "size"      => "35",
        "type"      => "date",
        "column_type" => "date"
      ),

      "date_end"    => Array(
        "label"     => "Date end:",
        "size"      => "35",
        "type"      => "date",
        "column_type" => "date"
      ),

    );

    //Collection display table definition
    $this->columns = Array(

      "position"        => Array(
        "title"     => "Position"
      ),

      "image"        => Array(
        "title"     => "Image"
      ),

      "views"        => Array(
        "title"     => "Views"
      ),

      "view_limit"        => Array(
        "title"     => "View limit"
      ),

      "hits"        => Array(
        "title"     => "Hits"
      ),

      "hit_limit"        => Array(
        "title"     => "Hit limit"
      ),

      "date_start"        => Array(
        "title"     => "Start"
      ),

      "date_end"        => Array(
        "title"     => "End"
      ),

    );

    $this->PostInit();

  }

  function IsEditableOutside(){
    $this->description = 'Banneri';
    $this->longname = $this->name;
    return true;
  }

  function IsAddToBottomMode(){
 		return false;
 	}

  function updateViews($ids){
    if(empty($ids)) return;
    sqlQuery("UPDATE `".$this->table."` set views = views + 1 WHERE item_id IN(".implode(",", $ids).")");
  }

  function processItemBeforeSave($item_id, &$item, $create, &$jsmessage){

    if($item['date_start']){
      $parts = explode(" ", $item['date_start']);
      $item['date_start'] = $parts[0];
    }

    if($item['date_end']){
      $parts = explode(" ", $item['date_end']);
      $item['date_end'] = $parts[0];
    }

    return true;

  }

  function GetEditData($page, &$count, &$recordposition, $order = ''){
    $data = parent::GetEditData($page, $count, $recordposition, $order);

    foreach ($data as $key => $row){

      $data[$key]['image'] = "<img src='".getThumbUrl($data[$key]['image'], 100 , 50, 6)."' />";
      $data[$key]['position'] = ucfirst($data[$key]['position']);
      $data[$key]['hits'] = $data[$key]['hits']? number_format($data[$key]['hits']) : "0";
      $data[$key]['views'] = $data[$key]['views']? number_format($data[$key]['views']) : "0";
      $data[$key]['view_limit'] = $data[$key]['view_limit']? number_format($data[$key]['view_limit'])." (".(round($data[$key]['views'] / $data[$key]['view_limit'] * 100))."%)" : "No limit";
      $data[$key]['hit_limit'] = $data[$key]['hit_limit']? number_format($data[$key]['hit_limit'])." (".(round($data[$key]['hits'] / $data[$key]['hit_limit'] * 100))."%)" : "No limit";
      $data[$key]['hits'] = $data[$key]['hits']." (CTR ".($data[$key]['views']? round($data[$key]['hits']/$data[$key]['views'],6) : "0")."%)";

      $data[$key]['date_end'] = $data[$key]['date_end']? convertDateFromMySQL($data[$key]['date_end']) : "Not set";
      $data[$key]['date_start'] = $data[$key]['date_start']? convertDateFromMySQL($data[$key]['date_start']) : "Not set";

    }
    return $data;
  }


}
