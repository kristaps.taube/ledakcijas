<?
//Serveris.LV Constructor component
//Created by Aivars Irmejs, 2006, aivars@datateks.lv

class shopcustomimagescollection extends pdocollection
{
  //Class initialization
  function shopcustomimagescollection($name = '', $id ='')
  {
    parent::__construct($name, $id);

    //Properties array definition
    $this->properties_assoc = Array(

      "product_id"    => Array(
        "label"     => "Product id",
        "size"      => "35",
        "type"      => "hidden",
      ),

      "image"    => Array(
        "label"     => "Image",
        "size"      => "35",
        "type"      => "file",
      ),

      "title"    => Array(
        "label"     => "Title",
        "type"      => "str",
        "multilang" => true
      ),

    "is_video"    => Array(
        "label"     => "Video:",
        "type"      => "check",
        "column_type" => "tinyint(1)",
        "default_value" => 0
    ),

      "video_url"    => Array(
        "label"     => "Video URL:",
        "type"      => "str"
      ),

    );

    //Collection display table definition
    $this->columns = Array(
      "thumb"     => Array(
        "title"     => "Image:",
        "format"    => "[if {%image%}]<a href=\"{%image%}\" target=\"_blank\"><img src=\"{%imagethumb%}\" /></a>[/if]"
      ),

      "title"        => Array(
        "width"     => "",
        "title"     => "Code",
        "multilang" => true
      ),

			"video" => array("title" => "Video")
    );

    $this->postInit();
  }

  function getImagesForProduct($product_id)
  {
    return sqlQueryDataAssoc('SELECT * FROM '.$this->table.' WHERE product_id = ' . intval($product_id) . ' ORDER BY ind');
  }

  function getFirstImage($product_id)
  {
    return sqlQueryRow('SELECT * FROM '.$this->table.' WHERE product_id = ' . intval($product_id) . ' ORDER BY ind LIMIT 1');
  }

  function getUpInd($item_id, $product_id)
  {
    $myind = $this->GetItemInd($item_id);
    return sqlQueryValue('SELECT ind FROM ' . $this->table . ' WHERE ind < ' . $myind . ' AND product_id = ' . $product_id.' ORDER BY ind DESC');
  }

  function getDownInd($item_id, $product_id)
  {
    $myind = $this->GetItemInd($item_id);
    return sqlQueryValue('SELECT ind FROM ' . $this->table . ' WHERE ind > ' . $myind . ' AND product_id = ' . $product_id);
  }

  function moveItemUpForProduct($item_id, $product_id)
  {
    $myind = $this->GetItemInd($item_id);
    $upind = sqlQueryValue('SELECT ind FROM ' . $this->table . ' WHERE ind < ' . $myind . ' AND product_id = ' . $product_id.' ORDER BY ind DESC LIMIT 1');
    if($myind && $upind)
    {
      $this->SwapItems($myind, $upind);
    }
  }

  function moveItemDownForProduct($item_id, $product_id){
    $myind = $this->GetItemInd($item_id);
    $upind = sqlQueryValue('SELECT ind FROM ' . $this->table . ' WHERE ind > ' . $myind . ' AND product_id = ' . $product_id.' ORDER BY ind ASC LIMIT 1');
    if($myind && $upind){
      $this->SwapItems($myind, $upind);
    }
  }

  function replaceProductId($from, $to)
  {
    sqlQuery('UPDATE '.$this->table.' SET product_id='.$to.' WHERE product_id='.$from);
  }

  function IsEditableOutside()
  {
    $this->description = $this->type;
    $this->longname = $this->name;
    return true;
  }

  function GetEditData($page, &$count, &$recordposition, $order = '')
  {
    $data = parent::GetEditData($page, $count, $recordposition, $order);
    foreach ($data as $key => $row)
    {
      if (!$row['image']) continue;
      $data[$key]['imagethumb'] = getThumbURL($row['image'], 120, 90, 6);
    }
    return $data;
  }

  function dropDeadSessionItems()
  {
    sqlQuery('DELETE p FROM `'.$this->table.'` p LEFT JOIN sessions s ON -p.product_id=s.id WHERE p.product_id < 0 AND s.id IS NULL');
  }

}

?>
