<?
include_once('class.pdocollection.php');

class EVeikalsPiedavajumiCol extends pdocollection{
  
  function __construct($name,$id)
  {
    $this->dbCollection($name,$id);
    $this->type = get_class();

    //Properties array definition
    $this->properties_assoc = Array(

      "name"    => Array(
        "label"     => "Nosaukums:",
        "type"      => "text"
      ),
			"datums"    => Array(
        "label"     => "Datums:",
        "type"      => "date",				
      ),
      "desc"    => Array(
        "label"     => "Apraksts:",
        "type"      => "text"
      ),
			
			"user"					=> Array(
				"label"     => "Lietotājs:",
				"type"      => "relation",
				"related_collection" => "shopusercollection",
				"relation_format" => "{%email%}",
			),
			
    );

		
    //Collection display table definition
    $this->columns = Array(

      "name"        => Array("title"     => "Nosaukums"),
      "datums"        => Array("title"     => "Datums"),
			"desc"        => Array("title"     => "Apraksts"),			
			"user"        => Array("title"     => "Lietotājs"),
			"params"    => Array(
        "title"     => "Produkti",
        "subrelated_collection" => "EVeikalsProdArticlesCol",
        "subrelated_field" => "piedavajums",
        "format"    => '[if {%_self%}==1]<b>{%_self%}</b> produkts[else]<b>{%_self%}</b> produkti[/if]',
      ),
    );

     $this->PostInit();
  }

  function IsEditableOutside()
  {
    $this->description = 'Collection: '.get_class();
    $this->longname = $this->name;
    return true;
  }

}