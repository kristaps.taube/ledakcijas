<?php

class CircleKCollection extends pdocollection
{

    public function __construct($name = '',$id = '')
    {

        parent::__construct($name, $id);

        //Properties array definition
        $this->properties_assoc = [

            "name" => [
                "label" => "Nosaukums:",
                "type" => "text",
                "multilang" => true
            ],

            "address" => [
                "label" => "Adrese:",
                "type" => "text",
                "multilang" => true
            ],

            "coords" => [
                "label" => "Koordinātas:",
                "type" => "text",
            ],

            "nr" => Array(
                "label"     => "NR:",
                "type"      => "hidden",
                "column_type"      => "int(32)",
            ),

        ];

        //Collection display table definition
        $this->columns = [
            "name_lv" => ["title" => "Nosaukums"],
            "address_lv" => ["title" => "Adrese"],
        ];

        $this->PostInit();

    }

    public function getByNr($nr)
    {
        return $this->getRow(['where' => "nr = :nr", 'params' => [':nr' => $nr]]);
    }

    public function IsEditableOutside()
    {
        $this->description = 'Collection: '.get_class();
        $this->longname = $this->name;
        return true;
    }

}