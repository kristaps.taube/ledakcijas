<?
//Serveris.LV Constructor component
//Created by Aleksandrs Selickis, 2004, alex@datateks.lv

include_once('class.dbcollection.php');

class mailingcategoriesemailscollectionex extends dbcollection
{
  //Class initialization
  function mailingcategoriesemailscollectionex($name,$id)
  {
    $this->dbCollection($name,$id);
    $this->type = "mailingcategoriesemailscollectionex";

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "E-mail",
        "type"      => "relation",
        "related_collection" => "mailinglistcollectionex",
        "relation_format" => "{%maillist_email%}",
        "column_name" => "mailcatemail_email"
      ),

      "it1"    => Array(
        "label"     => "Category",
        "type"      => "relation",
        "related_collection" => "mailingcategoriescollectionex",
        "relation_format" => "{%mailcat_title%}",
        "column_name" => "mailcatemail_cat"
      )

    );

    //Collection display table definition
    $this->columns = Array(

      "mailcatemail_email"        => Array(
        "title"     => "E-mail"
      ),

      "mailcatemail_cat"        => Array(
        "title"     => "Category"
      )

    );

    $this->PostInit();
  }

  function GetSubscribersForCategory($cat_id, $just_subscriber_ids = false)
  {
    $params = Array(
                'where' => ' mailcatemail_cat = ' . $cat_id,
                'assoc' => true
              );
    if($just_subscriber_ids)
      $params['what'] = 'mailcatemail_email';

    return $this->GetDBData($params);
  }

  function GetSubscribersForCategory2($cat_id, $emailscol)
  {
    return sqlQueryDataAssoc('
      SELECT
        *,
        e.maillist_name,
        e.maillist_email
      FROM '.$this->table.'
        LEFT JOIN '.$emailscol->table.' e ON mailcatemail_email=e.item_id
      WHERE
        mailcatemail_cat='.$cat_id.' AND
        e.maillist_status = 1
    ');
  }

  function GetCategoriesForSubscriber($subscriber_id)
  {
    return $this->GetDBData(
          Array(
            'where' => ' mailcatemail_email = ' . $subscriber_id
          )
    );
  }

  function HasRelation($cat_id, $email_id)
  {
    return sqlQueryValue('SELECT COUNT(*) FROM `' . $this->table . '` WHERE mailcatemail_cat = ' . $cat_id . ' AND mailcatemail_email = ' . $email_id);
  }

  function AddRelation($cat_id, $email_id)
  {
    if(!$this->HasRelation($cat_id, $email_id))
    {
      return $this->AddItemAssoc(Array('mailcatemail_cat' => $cat_id, 'mailcatemail_email' => $email_id));
    }else
    {
      return false;
    }
  }

  function DeleteRelation($cat_id, $email_id)
  {
    sqlQuery('DELETE FROM `' . $this->table . '` WHERE mailcatemail_cat = ' . $cat_id . ' AND mailcatemail_email = ' . $email_id);
  }

  function DeleteEmailRelations($email_id)
  {
    sqlQuery('DELETE FROM `' . $this->table . '` WHERE mailcatemail_email = ' . $email_id);
  }

  function DeleteCategoryRelations($cat_id)
  {
    sqlQuery('DELETE FROM `' . $this->table . '` WHERE mailcatemail_cat = ' . $cat_id);
  }

  function IsEditableOutside()
  {
    $this->description = 'Mailing list categories - e-mails relation';
    $this->longname = $this->name;
    return false;
  }







}

?>