<?php

class EVeikalsProdFilterRel extends pdocollection{

  function __construct($name = '', $id = ''){

    parent::__construct($name, $id);

    //Properties array definition
    $this->properties_assoc = Array(

      "product_id"    => Array(
        "label"     => "Product(id):",
        "type"      => "str",
        "column_type" => "int(32)",
        "column_index" => 1,
      ),

      "filter_id"    => Array(
        "label"     => "Filter(id):",
        "type"      => "str",
        "column_type" => "int(32)",
        "column_index" => 1,
      ),

    );

    //Collection display table definition
    $this->columns = Array(

      "product_id"        => Array(
        "width"     => "50%",
        "title"     => "Product(id):"
      ),

      "filter_id"        => Array(
        "width"     => "50%",
        "title"     => "Filter(id):"
      ),

    );

     $this->PostInit();
  }

  function getWithFilterForProduct($product_id)
  {

    $filter_col = EVeikalsFilters::getInstance();

    return DB::GetTable('
        SELECT f.*, g.*
        FROM `'.$this->table.'` AS pf
        JOIN `'.$filter_col->table.'` AS f ON f.item_id = pf.filter_id
        JOIN `'.$filter_col->group->table.'` AS g ON g.item_id = f.group
        WHERE pf.product_id = :pid
    ', [':pid' => $product_id]);


  }

  function IsEditableOutside()
  {
    $this->description = 'Filtru produktu attiecības';
    $this->longname = $this->name;
    return false;
  }

  function processFormBeforeDisplay(&$form_data, $create, $item){

  }

  function getByFilter($filter){
    return sqlQueryData("SELECT * FROM `".$this->table."` WHERE filter_id=".$filter);
  }

  function getByProduct($prod){
    return sqlQueryData("SELECT * FROM `".$this->table."` WHERE product_id=".$prod);
  }

  function getFilterIdsByProdIds($prod_ids){
  	if(empty($prod_ids)) return array();

  	$table = DB::GetTable("SELECT product_id, filter_id FROM `".$this->table."` WHERE product_id IN(".implode(",", $prod_ids).")");

    $data = array();
    foreach($table as $row){
        $data[$row['product_id']][] = $row['filter_id'];
    }

		return $data;

	}

  function getFilterIds($prod){
    $data = $this->getByProduct($prod);

    $return = Array();
    foreach($data as $row){
      $return[] = $row['filter_id'];
    }

    return $return;

  }

  function DeleteByProdAndFilter($prod, $filter){
    sqlQuery("DELETE FROM `".$this->table."` WHERE product_id = '".$prod."' AND filter_id = '".$filter."'");
  }

  function DeleteByProd($prod){
    sqlQuery("DELETE FROM `".$this->table."` WHERE product_id = '".$prod."'");
  }

}

