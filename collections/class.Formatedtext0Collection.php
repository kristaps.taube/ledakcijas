<?php

class Formatedtext0Collection extends pdocollection
{
  
    function __construct($name = '', $id = '')
    {

        parent::__construct($name, $id);

        //Properties array definition
        $this->properties_assoc = [

            "page_id" => [
                "label" => "Page id:",
                "type" => "text",
                "column_type" => "int(32)",
                "column_index" => true
            ],

            "name" => [
                "label" => "Name:",
                "type" => "text",
            ],

            "content" => [
                "label" => "Content:",
                "type" => "text",
            ],

        ];

        //Collection display table definition
        $this->columns = [];

        $this->PostInit();

    }

    function IsEditableOutside()
    {
        $this->description = 'Collection: '.get_class();
        $this->longname = $this->name;
        return false;
    }

    public function getContent($page_id, $name, $savemode)
    {

        $content= '';
        switch($savemode){

            case 0: $content = DB::getValue('SELECT content FROM `'.$this->table.'` WHERE page_id = :page_id AND name = :name', [':page_id' => $page_id, ':name' => $name]); break; // edited page
            case 1:
                $page = GetPageByID($page_id); // this is cached
                $language_id = $page['language'] * (-1); // we store language default values as page_id equal to negative language id
                $content = DB::getValue('SELECT content FROM `'.$this->table.'` WHERE page_id = :page_id AND name = :name', [':page_id' => $language_id, ':name' => $name]);
                break; // language value
            case 2:  // default value. We store it with page_id = 0
                $content = DB::getValue('SELECT content FROM `'.$this->table.'` WHERE page_id = :page_id AND name = :name', [':page_id' => 0, ':name' => $name]); break;
                break;
            default: die('Unknown savemode');

        }

        return $content;

    }

    public function setContent($page_id, $name, $content, $savemode)
    {

        switch($savemode){
            case 0: $page_id = $page_id; break;
            case 1:
                $page = GetPageByID($page_id); // this is cached
                $page_id = $page['language'] * (-1); // we store language default values as page_id equal to negative language id
            case 2: $page_id = 0; break;
            default: die('Unknown savemode');
        }

        $entry = DB::getRow("SELECT item_id FROM `".$this->table."` WHERE page_id = :page_id AND name = :name", [':page_id' => $page_id, ':name' => $name]);

        if($entry){
            $this->Update(['content' => $content], $entry['item_id']);
        }else{
            $this->Insert(['content' => $content, 'page_id' => $page_id, 'name' => $name]);
        }

    }

}