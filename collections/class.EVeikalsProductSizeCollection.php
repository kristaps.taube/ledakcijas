<?
include_once('class.dbcollection.php');

class EVeikalsProductSizeCollection extends dbcollection{

  function __construct($name,$id){

    $this->dbCollection($name,$id);
    $this->type = get_class();

    //Properties array definition
    $this->properties_assoc = Array(

      "prod_id"    => Array(
        "label"     => "Produkta id:",
        "type"      => "text"
      ),

      "size_id"    => Array(
        "label"     => "Izmēra id:",
        "type"      => "text"
      ),

    );

    //Collection display table definition
    $this->columns = Array(
      "prod_id"        => Array("title"     => "Produkta id"),
      "size_id"        => Array("title"     => "Izmēra id"),
    );

     $this->PostInit();
  }

  function productSizeCount($prod){
		return DB::GetValue("SELECT COUNT(*) FROM `".$this->table."` WHERE prod_id = :id" , array(":id" => $prod));
	}

  function hasSizes($prod){
    return $this->productSizeCount($prod) ? true : false;
  }

  function getSizeIdsForProduct($prod){
    $sizes = array();
    $data = sqlQueryData("SELECT * FROM `".$this->table."` WHERE prod_id = '".$prod."'");

    foreach($data as $entry){
      $sizes[] = $entry['size_id'];
    }

    return $sizes;
  }

  function DeleteByProdAndSize($prod, $size){
    sqlQuery("DELETE FROM `".$this->table."` WHERE prod_id = '".$prod."' AND size_id = '".$size."'");
  }

  function IsEditableOutside(){
    $this->description = 'Produktu izmēri';
    $this->longname = $this->name;
    return true;
  }

}