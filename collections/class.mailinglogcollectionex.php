<?
//Serveris.LV Constructor component
//Created by Aleksandrs Selickis, 2004, alex@datateks.lv

class mailinglogcollectionex extends pdocollection
{
  //Class initialization
  function mailinglogcollectionex($name,$id)
  {
    $this->dbCollection($name,$id);
    $this->type = "mailinglogcollectionex";

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "Date",
        "type"      => "date",
        "time"      => true,
        "column_name" => "maillog_time",
        "column_type" => "datetime",
        "column_index" => 1
      ),

      "it1"    => Array(
        "label"     => "E-mail",
        "type"      => "str",
        "column_name" => "maillog_email",
        "column_type" => "VARCHAR(250)",
      ),

      "it2"    => Array(
        "label"     => "Subject",
        "type"      => "str",
        "column_name" => "maillog_subject",
        "column_type" => "VARCHAR(250)",
      ),

      "it3"    => Array(
        "label"     => "Status",
        "type"      => "str",
        "column_name" => "maillog_status"
      ),
      
      "it4"    => Array(
        "label"     => "Message id",
        "type"      => "str",
        "column_name" => "message_id",
        "column_type" => "int(11)",
      )

    );

    //Collection display table definition
    $this->columns = Array(

      "maillog_time"        => Array(
        "title"     => "Time"
      ),

      "maillog_email"        => Array(
        "title"     => "E-mail"
      ),

      "maillog_subject"        => Array(
        "title"     => "Subject"
      ),

      "maillog_status"        => Array(
        "title"     => "Additional"
      ),

    );

    $this->PostInit();
  }


  function AddLogItem($email, $subject, $status, $message_id = false)
  {
    $date = date("Y-m-d H:i:s");
    $item = Array(
      'maillog_time'   => $date,
      'maillog_email'  => $email,
      'maillog_subject'=> $subject,
      'maillog_status' => $status,
      'message_id'     => $message_id,
    );
    #$item = $this->AddItemSlashes($item);
    $this->Insert($item);
  }



  function IsEditableOutside()
  {
    $this->description = 'Mailing list log';
    $this->longname = $this->name;
    return true;
  }







}

?>