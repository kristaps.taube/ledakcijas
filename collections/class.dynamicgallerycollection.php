<?

include_once('class.dbcollection.php');

class dynamicgallerycollection extends dbcollection
{
  function dynamicgallerycollection($name, $id)
  {
    $this->DBcollection($name, $id);
    $this->type = "dynamicgallerycollection";

    $this->properties_assoc = Array(
      "component_id" => Array(
        "label"     => "Component:",
        "type"      => "hidden",
        "column_type" => "int",
        "column_index" => 1
      ),

      "title" => Array(
        "label"     => "Title:",
        "type"      => "str",
      ),

      "image"    => Array(
        "label"     => "Image:",
        "type"      => "file",
      )

    );

    $this->columns = Array(
      "title"     => Array(
        "title"     => "Title:"
      ),

      "image"     => Array(
        "title"     => "Image:"
      )

    );

    $this->postInit();
  }

  function getImagesByComponent($component_id)
  {
    return sqlQueryDataAssoc('SELECT * FROM '.$this->table.' WHERE component_id = ' .$component_id . ' ORDER BY ind');
  }

  function copyItems($src_component_id, $dest_component_id)
  {
    $data = $this->getImagesByComponent($src_component_id);
    foreach ($data as $row)
    {
      unset($row['id']);
      unset($row['ind']);
      $row['component_id'] = $dest_component_id;

      foreach ($row as $key => $val)
      {
        $row[$key] = db_escape($val);
      }

      $this->addItemAssoc($row);
    }

  }

  function deleteItems($component_id)
  {
    sqlQuery('DELETE FROM '.$this->table.' WHERE component_id='.(int)$component_id);
  }


}

?>