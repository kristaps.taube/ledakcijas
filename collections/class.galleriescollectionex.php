<?
//Serveris.LV Constructor component
//Created by Aleksandrs Selickis, 2004, alex@datateks.lv

include_once('class.dbcollection.php');

class galleriescollectionex extends dbcollection
{
  //Class initialization
  function galleriescollectionex($name,$id)
  {
    $this->dbcollection($name,$id);
    $this->type = "galleriescollectionex";

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "Gallery thumbnail path",
        "size"      => "35",
        "type"      => "str",
        "column_name" => "thumb"
      ),

       "it1"    => Array(
        "label"     => "Gallery Title",
        "size"      => "35",
        "type"      => "str",
        "column_name" => "title"
      ),

      "it2"    => Array(
        "label"     => "Gallery Date",
        "size"      => "35",
        "type"      => "date",
        "column_name" => "date"
      ),

      "it3" => Array(
        "label"     => "Description:",
        "type"      => "wysiwyg",
        "rows"      => "7",
        "cols"      => "50",
        "dialog"    => "?module=wysiwyg&site_id=".$GLOBALS['site_id'],
        "dialogw"   => "720",
        "dialogh"   => "450",
        "dlg_res"   => true,
        "value"     => "<P>&nbsp;</P>",
        "column_name" => "description"
      ),

      "it4"    => Array(
        "label"     => "Disabled",
        "type"      => "check",
        "column_name" => "disabled"
      )

    );

    //Collection display table definition
    $this->columns = Array(

      "thumb"        => Array(
        "width"     => "",
        "title"     => "Thumbnail",
        "format"    => "[if {%thumb%}]<img src=\"{%thumb%}\" />[/if]",
      ),

      "title"        => Array(
        "width"     => "",
        "title"     => "Title"
      ),

      "date"        => Array(
        "width"     => "",
        "title"     => "Date"
      ),

      "pictures"    => Array(
        "width"     => "",
        "title"     => "Pictures",
        "subrelated_collection" => "picturescollectionex",
        "subrelated_field" => "gal_id",
        "format"    => '[if {%pictures%}==1]<b>{%pictures%}</b> item[else]<b>{%pictures%}</b> items[/if]',
        "sortdesc"  => true,
      )

    );

    $this->postInit();




  }

  function IsEditableOutside()
  {
    $this->description = 'Galerijas';
    $this->longname = $this->name;
    return true;
  }

  function getGalleries ()
  {
    if(!$this->name)
    {
      $this->error = true;
      return false;
    }
    $data = sqlQueryData("SELECT * FROM `".$this->table."` ORDER BY ind");
    return $data;
  }

  function getEnabledGalleries ()
  {
    if(!$this->name)
    {
      $this->error = true;
      return false;
    }
    $data = sqlQueryData("SELECT * FROM `".$this->table."` WHERE `disabled` = '' ORDER BY ind");
    return $data;
  }

  function getFirstGallery ()
  {
    if(!$this->name)
    {
      $this->error = true;
      return false;
    }
    $data = sqlQueryRow("SELECT * FROM `".$this->table."` WHERE `disabled` = '' ORDER BY ind LIMIT 1");
    return $data;
  }

  function countTimesPictureUsed($pic)
  {
    return sqlQueryValue('SELECT COUNT(*) FROM `'.$this->table.'` WHERE thumb="'.$pic.'"');
  }

}

?>
