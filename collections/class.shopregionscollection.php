<?
//Serveris.LV Constructor component
//Created by Kristaps Grinbergs, 2006, kristaps@datateks.lv

include_once('class.dbcollection.php');

class shopregionscollection extends dbcollection
{
  //Class initialization
  function shopregionscollection ($name,$id)
  {
    $this->dbcollection($name,$id);
    $this->type = "shopregionscollection";

    //Properties array definition
    $this->properties = Array(


      "it0"    => Array(
          "label"     => "Rajona nosaukums:",
          "type"      => "str",
          "validate" => "required",
          "multilang" => true,
          "column_name" => "name"
      ),

      "it1"    => Array(
          "label"     => "Piegādes cena:",
          "type"      => "str",
          "validate" => "required",
          "column_name" => "price"
      )

    );

    //Collection display table definition
    $this->columns = Array(

      "name"        => Array(
        "title"     => "Rajona nosaukums",
        "multilang" => true
      ),

      "price"        => Array(
        "title"     => "Piegādes cena"
      )

    );

    $this->postInit();
  }


  function IsEditableOutside()
  {
    $this->description = 'Reģioni';
    $this->longname = $this->name;
    return true;
  }

}

?>
