<?
include_once('class.dbcollection.php');

class audiocollection extends dbcollection
{
  //Class initialization
  function audiocollection($name,$id)
  {
    $this->DBCollection($name,$id);
    $this->type = "audiocollection";

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "Nosaukums",
        "type"      => "str",
        "column_name" => "nosaukums"
      ),

      "it1" => Array(
        "label"     => "Apraksts:",
        "type"      => "wysiwyg",
        "rows"      => "7",
        "cols"      => "50",
        "dialog"    => "?module=wysiwyg&site_id=".$GLOBALS['site_id'],
        "dialogw"   => "720",
        "dialogh"   => "450",
        "dlg_res"   => true,
        "column_name" => "apraksts"
      ),

      "it2"    => Array(
        "label"     => "Fails",
        "type"      => "dialog",
        "dialog"    => "?module=dialogs&action=filex&site_id=".$GLOBALS['site_id'],
        "column_name" => "file"
      ),

      "it3"    => Array(
        "label"     => "Datums:",
        "type"      => "date",
        "dialog"    => "?module=dialogs&action=date&site_id=".$GLOBALS['site_id']."&time=1",
        "dialogw"   => "430",
        "dialogh"   => "250",
        "column_name" => "datums"
      )

    );

    //Collection display table definition
    $this->columns = Array(

      "nosaukums"        => Array(
        "width"     => "15%",
        "title"     => "Nosaukums"
      ),

      "apraksts"        => Array(
        "width"     => "25%",
        "title"     => "Apraksts"
      ),
    );
    $this->postInit();
  }

  function GetAnswerByQuestId($id){
    if(!$this->name)
    {
      $this->error = true;
      return false;
    }
    $data = sqlQueryRow("SELECT `atb_var` FROM `".$this->table."` WHERE `jaut_teksts`='".$id."'");
    return $data;
  }

  function MoveItemsUp($id){
    if(!$this->name)
    {
      $this->error = true;
      return false;
    }
    $data = sqlQueryRow("UPDATE `".$this->table."` SET `jaut_teksts` = '".($id-1)."' WHERE `jaut_teksts`='".$id."'");
    return $data;
  }

  function IsEditableOutside()
  {
    $this->description = 'Anketa';
    $this->longname = $this->name;
    return true;
  }

  function GetData($array){
    if(!$this->name)
    {
      $this->error = true;
      return false;
    }
    $massiv = Array();
    foreach($array as $ar => $val){
      $massiv [] = sqlQueryData("SELECT * FROM `".$this->table."` WHERE `jaut_teksts` = '".$val[0]."'");
    }
    print("<pre>");
    print_r($massiv);
    return $data;
  }

  function GetDataByInd($ind)
  {
    if(!$this->name)
    {
      $this->error = true;
      return false;
    }
    $data = sqlQueryData("SELECT * FROM `".$this->table."` WHERE `jaut_teksts` = '".$ind."'");
    return $data;
  }

}
?>
