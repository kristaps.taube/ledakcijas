<?php

class LedAkcijasBUJCollection extends pdocollection
{

    public function __construct($name = '', $id = '')
    {

        parent::__construct($name, $id);

        $langs = [];
        foreach(getLanguages() as $short => $lang){
            $langs[$short] = $lang['fullname'];
        }

        //Properties array definition
        $this->properties_assoc = [

            "lang" => [
                "label" => "Valoda:",
                "type" => "list",
                "lookupassoc" => $langs
            ],
            "category_id" => Array(
                "label"     => "Kategorija:",
                "type"      => "relation",
                "related_collection" => "LedAkcijasBUJCategoryCollection",
                "relation_format" => "{%category_title_lv%}",
            ),
            "question" => [
                "label" => "Jautājums:",
                "type" => "text"
            ],
            "answere" => [
                "label" => "Atbilde:",
                'type'          => 'wysiwyg',
                'cols'          => 50,
                'rows'          => 7,
                'dialog'        => '?module=wysiwyg&site_id='.$this->site_id,
                'dialogw'       => 800,
                'dialogh'       => 600,
                'dlg_res'       => true
            ],

        ];

        //Collection display table definition
        $this->columns = [
            'category_id' => ['title' => 'Kategorija'],
            'question' => ['title' => 'Jautājums'],
        ];

        $this->PostInit();

    }

    function processItemBeforeSave($item_id, &$item, $create, &$jsmessage)
    {

        #if(!$item['url']) $item['url'] = transliterateURL($item['title']);
        #if(!$item['date']) $item['date'] = date("Y-m-d H:i:s");

        return parent::processItemBeforeSave($item_id, $item, $create, $jsmessage);
    }

    public function IsEditableOutside()
    {
        $this->description = 'Collection: '.get_class();
        $this->longname = $this->name;
        return true;
    }

}