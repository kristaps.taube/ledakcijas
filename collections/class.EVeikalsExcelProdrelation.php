<?
include_once('class.dbcollection.php');

class EVeikalsExcelProdrelation extends dbcollection{

  function __construct($name,$id){

    $this->dbCollection($name,$id);
    $this->type = get_class();

    //Properties array definition
    $this->properties_assoc = Array(

      "prod_prop"    => Array(
        "label"     => "Product property:",
        "type"      => "list",
        "lookupassoc"    => Array(),
      ),

      "excel_column"    => Array(
        "label"     => "Excel column:",
        "type"      => "text"
      ),

    );

    //Collection display table definition
    $this->columns = Array(

      "prod_prop"        => Array("title"     => "Produkta lauks"),
      "excel_column"        => Array("title"     => "Excel kolonna"),

    );

     $this->PostInit();
  }

  function IsEditableOutside(){
    $this->description = 'Excel column and Prod. prop. relation';
    $this->longname = $this->name;
    return true;
  }

  function processItemBeforeSave($item_id, &$item, $create, &$jsmessage){

    $check = sqlQueryRow("SELECT * FROM `".$this->table."` WHERE prod_prop = '".$item['prod_prop']."'");

    if($check){
      $jsmessage = "Šim laukam jau ir piesaistīta kolonna";
      return false;
    }

    $check = sqlQueryRow("SELECT * FROM `".$this->table."` WHERE excel_column = '".$item['excel_column']."'");

    if($check){
      $jsmessage = "Šai kolonnai jau ir piesaistīts lauks";
      return false;
    }

    return true;

  }

  function processFormBeforeDisplay(&$form_data, $create, $item){

    $prod_table = getOptionByPath("shop\\product_table");

    $columns = sqlQueryData("SHOW columns FROM ".$prod_table['value']);

    $ignore_columns = array("item_id", "ind", "cat_ind");

    foreach($columns as $column){

      if(in_array($column['Field'], $ignore_columns)) continue;
      $form_data['prod_prop']['lookupassoc'][$column['Field']] = $column['Field'];

    }

  }

}