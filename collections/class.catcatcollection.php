<?
//Serveris.LV Constructor collection module
//Created by Aleksandrs Selickis, 2004

include_once('class.collection.php');

class CatCatCollection extends collection
{
  //Class initialization
  function CatCatCollection($name,$id)
  {
    $this->Collection($name,$id);
    $this->type = "catcatcollection";

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "Category name",
        "size"      => "35",
        "type"      => "str"
      ),
	
	  "it1"    => Array(
        "label"     => "Description",
        "size"      => "35",
        "type"      => "str"
      ),

      "it2"    => Array(
        "label"     => "Level",
        "size"      => "35",
        "type"      => "str"
      ),

      "it3"    => Array(
        "label"     => "Sub collecton type",
        "size"      => "35",
        "type"      => "str"
      ),

      "it4"    => Array(
        "label"     => "Sub collecton ID",
        "size"      => "35",
        "type"      => "str"
      ),

    );

    //Collection display table definition
    $this->columns = Array(

      "col0"        => Array(
        "width"     => "20%",
        "title"     => "Category name"
      ),

      "col1"        => Array(
        "width"     => "30%",
        "title"     => "Description"
      ),

      "col2"        => Array(
        "width"     => "15%",
        "title"     => "Level"
      ),

      "col3"        => Array(
        "width"     => "20%",
        "title"     => "Sub collection type"
      ),

      "col4"        => Array(
        "width"     => "15%",
        "title"     => "Sub collection ID"
      )

    );




  }

//-------------------     OVERRIDDEN METHODS     --------------------//
//--------------------         BEGIN              -------------------//

function DelItem($ind)
{
	
	$data = $this->GetDBData();

	$read=0;
	$level=0;
	foreach ($data as $key=>$row)
	  {
		if ($read && $row['col2']<$level) $read=0;

		//atrod ierakstus kuri ir peec dotaa
		if ($read==1 && $row['col2']>=$level) 
		  {
			$SubCat[] = $row['ind'];
			//ja ieraksts satur kolekciju, tad izdzesh sho kolekciju
			if ($row['col4']!=0)
			  {
				$coltype=$row['col3'];
				include_once($GLOBALS['cfgDirRoot'] . "collections/class." . $coltype . ".php");

				$collection = new $type("",$row['col4']);
				if ($collection->IsValidCollection($row['col4'])) $collection->DelCollection($row['col4']);
			  }
		  }

		if ($row['ind'] == $ind) 
		  {
			$read=1;
			$level=$row['col2'] + 1;
		  }
	  }
		
	  //dzesh visus ierakstus peec dotaa
	  foreach ($SubCat as $key=>$val) 
		parent::DelItem($ind+1);

	  //dzesh doto ierakstu
	  parent::DelItem($ind);
}

function MoveItemUp($ind)
{
	$cur_row = $this->GetItem($ind);

	$it_num=0;
	for ($i=$ind-1; $i>=1; $i--)
	{
		$row=$this->GetItem($i);

		//ja level sakrit tad izkaitljo sparpibu cik daudz jaapaarvieto
		if ($row[2]==$cur_row[2])
		{
			$it_num=$ind-$i;
			break;
		}
		//ja liimenis augstaaks tad paartrauc mekleeshanu
		if ($row[2]<$cur_row[2]) 
		{
			break;
		}
	}

	//apakshkategoriju skaits paarbidiishanai kopaa ar doto kategoriju
	$sub_row_num=0;

	$total_row_num = count($this->GetDBData());
	for ($k=$ind+1; $k<=$total_row_num; $k++)
	{
		$sub_row = $this->GetItem($k);
		if ($sub_row[2]>$cur_row[2]) $sub_row_num++;
		else break;
	}
	

	//paarbidam uz augshu in_num reizes kategoriju ar apakshkategorijaam (ja ir)
	$mov_ind = $ind;
	if ($it_num>0) 
		for ($j=$it_num; $j>0; $j--,$mov_ind--)
		{
			parent::MoveItemUp($mov_ind);
			if ($sub_row_num!=0)
				for ($l=1; $l<=$sub_row_num; $l++)
					parent::MoveItemUp($mov_ind+$l);

		}
		
}

function MoveItemDown($ind)
{
	$cur_row = $this->GetItem($ind);

	$total_row_num = count($this->GetDBData());
	$it_num=0;
	for ($k=$ind+1; $k<=$total_row_num; $k++)
	{
		$row=$this->GetItem($k);

		//ja level sakrit tad izkatlo sparpibu cik daudz jaapaarvieto
		if ($row[2]==$cur_row[2])
		{
			$it_num=$k-$ind;
			break;
		}
		//ja liimenis augstaaks(veertiiba ir mazaaka) tad paartrauc mekleeshanu
		if ($row[2]<$cur_row[2]) 
		{
			break;
		}
	}

	if ($it_num) $this->MoveItemUp($ind+$it_num);

}

//-------------------            END             --------------------//
//-------------------     OVERRIDDEN METHODS     --------------------//


}

?>

