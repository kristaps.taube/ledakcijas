<?php

require_once('class.pdocollection.php');

class EVeikalsOrderCollection extends pdocollection
{

    function __construct($name = '', $id ='')
    {

        parent::__construct($name, $id);

        //Properties array definition
        $this->properties_assoc = Array(

            "status_id" => Array(
                "label"     => "Status:",
                "type"      => "relation",
                "value" => 1,
                "column_type" => "int(10)",
                "column_index" => 1,
                "related_collection" => "EVeikalsOrderStatuses",
                "relation_format" => "{%status%}",
            ),

            "payment_method_id" => Array(
                "label"     => "Payment method:",
                "type"      => "relation",
                "value" => 1,
                "column_type" => "int(10)",
                "column_index" => 1,
                "related_collection" => "EVeikalsPaymentMethods",
                "relation_format" => "{%name_lv%}",
            ),

            "user_id" => Array(
                "label"     => "Lietotājs:",
                "type"      => "relation",
                "column_type" => "int(32)",
                "column_index" => 1,
                "related_collection" => "shopusercollection",
                "relation_format" => "{%email%}",
            ),

            "datums" => Array(
                "label"     => "Datums:",
                "type"      => "date",
                'timestamp' => true,
                "column_type" => "datetime",
                "column_index" => 1
            ),

            "rekins" => Array(
                "label"     => "Rekins:",
                "type"      => "wysiwyg",
                "rows"      => "7",
                "cols"      => "50",
                "dialog"    => "?module=wysiwyg&site_id=".$GLOBALS['site_id'],
                "dialogw"   => "720",
                "dialogh"   => "450",
                "dlg_res"   => true,
            ),

            "pavadzime" => Array(
                "label"     => "Pavadzime:",
                "type"      => "wysiwyg",
                "rows"      => "7",
                "cols"      => "50",
                "dialog"    => "?module=wysiwyg&site_id=".$GLOBALS['site_id'],
                "dialogw"   => "720",
                "dialogh"   => "450",
                "dlg_res"   => true,
            ),

            "summa" => Array(
                "label"     => "Summa:",
                "type"      => "str",
                'column_type' => 'decimal(8,2)'
            ),

            "tracking_num" => Array(
                "label"     => "Tracking Nummurs:",
                "type"      => "str",
                'column_type' => 'varchar(255)'
            ),

            "num" => Array(
                "label"     => "Nummurs:",
                "type"      => "str",
                'column_type' => 'varchar(255)'
            ),

            "pasutitajs" => Array(
                "label"     => "Pasūtītājs:",
                "type"      => "str",
                "column_type" => "varchar(255)"
            ),

            "piegadats" => Array(
                "label"     => "Piegādāts:",
                "type"      => "check",
                "column_type" => "tinyint(1)",
                "column_index" => 1
            ),

            "apmaksats" => Array(
                "label"     => "Apmaksāts:",
                "type"      => "check",
                "column_type" => "tinyint(1)",
                "column_index" => 1
            ),

            "apmaksas_veids" => Array(
                "label"     => "Apmaksas veids:",
                "type"      => "str",
                "column_type" => "varchar(255)"
            ),

            "kopeja_summa" => Array(
                "label"     => "Kopējā summa:",
                "type"      => "str",
                'column_type' => 'decimal(8,2)'
            ),

            "order_data" => Array(
                "label"     => "Order data:",
                "type"      => "hidden",
            ),

            "card_data" => Array(
                "label"     => "Card data:",
                "type"      => "hidden",
            ),

            "sent" => Array(
                "label"     => "Nosūtīts:",
                "type"      => "hidden",
                "column_type" => "tinyint(1)"
            ),

            "telefons" => Array(
                "label"     => "Telefons:",
                "type"      => "str",
                "column_type" => "varchar(100)",
                "column_index" => 1
            ),

            "valoda" => Array(
                "label"     => "Valoda:",
                "type"      => "str",
                "column_type" => "varchar(2)",
                "column_index" => 1
            ),

            "epasts" => Array(
                "label"     => "E-pasts:",
                "type"      => "str",
                "column_type" => "varchar(100)",
                "column_index" => 1
            ),

            "adrese" => Array(
                "label"     => "Adrese:",
                "type"      => "str",
                "column_type" => "varchar(255)",
                "column_index" => 1
            ),

            "apmaksas_datums" => Array(
                "label"     => "Apmaksas datums:",
                "type"      => "date",
                'timestamp' => true,
                "column_type" => "datetime",
                "column_index" => 1
            ),

            "piegades_datums" => Array(
                "label"     => "Piegādes datums:",
                "type"      => "date",
                'timestamp' => true,
                "column_type" => "datetime",
                "column_index" => 1
            ),

            "reminder_sent" => Array(
                "label"     => "Reminder sent:",
                "type"      => "str",
                "default_value" => 0,
                "column_type" => "tinyint(1)",
                "column_index" => 1
            ),

            "review_sent" => Array(
                "label"     => "Review sent:",
                "type"      => "str",
                "default_value" => 0,
                "column_type" => "tinyint(1)",
                "column_index" => 1
            ),

            "review_stars" => Array(
                "label"     => "Review stars:",
                "type"      => "str",
                "default_value" => 0,
                "column_type" => "int(1)",
                "column_index" => 1
            ),

            "review_hash" => Array(
                "label"     => "Review hash:",
                "type"      => "str",
                "default_value" => 0,
                "column_type" => "varchar(32)",
                "column_index" => 1
            ),


            );

        //Collection display table definition
        $this->columns = Array(

        "item_id"        => Array(
        "title"     => "Id:"
        ),

        "datums"    => Array(
        "title"     => "Datums"
        ),

        "summa"    => Array(
        "title"     => "Summa"
        ),

        "review_sent"    => Array(
        "title"     => "Novērtējumam sūtīts"
        ),

        "review_stars"    => Array(
        "title"     => "Novērtējums"
        ),

        "review_hash"    => Array(
        "title"     => "Novērtējuma hash",
        ),

        );

        $this->PostInit();
    }

  public function IsEditableOutside()
	{
    $this->description = 'Collection: '.get_class();
    $this->longname = $this->name;
    return true;
  }

	public function getDataForUser($id)
  {
    return DB::GetTable("SELECT * FROM `".$this->table."` WHERE user_id = :id ORDER BY datums", array(":id" => $id));
  }

	public function getNextId(){
    $row = DB::GetRow("SHOW TABLE STATUS LIKE '".$this->table."'");
    return $row['Auto_increment'];
  }

	public function GetOrdersWithUsers($usercol, $sortby, $page, $pagesize, $asc, $params=null)
  {
    if(!$sortby)
    {
      $sortby = 'O.datums';
      $asc = 0;
    }

    $cond = array();

    if (!empty($params['phone']))
      $cond[] = 'O.telefons LIKE "%'.$params['phone'].'%"';

    if (!empty($params['email']))
      $cond[] = 'O.epasts LIKE "%'.$params['email'].'%"';

    if (!empty($params['name']))
      $cond[] = 'O.pasutitajs LIKE "%'.$params['name'].'%"';

    if (!empty($params['address']))
      $cond[] = 'O.adrese LIKE "%'.$params['address'].'%"';

    if(!empty($params['date_from'])){
      $cond[] = "O.datums > '".convertDateToMySQL($params['date_from'])." 00:00:00'";
    }

    if(!empty($params['date_to'])){
      $cond[] = "O.datums < '".convertDateToMySQL($params['date_to'])." 23:59:59'";
    }

   $sql = '';

    if ($cond)
      $sql .= ' WHERE '.implode(' AND ', $cond);


    return array(
      sqlQueryData("SELECT SQL_CALC_FOUND_ROWS O.*, U.email
                         FROM `".$this->table."` O
                         LEFT JOIN `".$usercol->table."` U
                           ON O.user_id = U.item_id
                         ".$sql."
                         ORDER BY " . $sortby . ($asc ? ' ASC' : ' DESC') . "
                         LIMIT " . ($page * $pagesize) . ',' . $pagesize),
      sqlQueryValue('SELECT FOUND_ROWS()')
    );
  }

  public function getOrdersByDateInterval($from, $to)
  {
    if (!$from && !$to) return null;

    $where = Array();
    if ($from) $where[]= "datums >= '".date('Y-m-d H:i', $from)."'";
    if ($to) $where[] = "datums <= '".date('Y-m-d H:i', $to)."'";

    return sqlQueryData("SELECT * FROM `".$this->table."` WHERE ".implode(' AND ', $where)." ORDER BY datums DESC");
  }

  public function getData($params)
  {
    $sql = '';

    $cond = array();
    if (!empty($params['datums_from']))
      $cond[] = 'datums >= "'.date('Y-m-d H:i:s', strtotime($params['datums_from'])).'"';

    if (!empty($params['datums_to']))
    {
      if (strpos($params['datums_to'], ':') !== false)
        $to = strtotime($params['datums_to']);
      else
        $to = strtotime($params['datums_to']) + 86400;

      $cond[] = 'datums < "'.date('Y-m-d H:i:s', $to).'"';
    }

    if ($cond)
      $sql .= ' WHERE '.implode(' AND ', $cond);

//  echo 'SELECT * FROM '.$this->table.$sql.' ORDER BY item_id DESC';

    return sqlQueryDataAssoc('SELECT * FROM '.$this->table.$sql.' ORDER BY item_id DESC');
  }

  public function getProductStats($orderprodcol, $prodscol, $from, $to, $params){

    $cond = array(
      'datums >= "'.date('Y-m-d', strtotime($from)).' 00:00:00"',
      'datums <= "'.date('Y-m-d', strtotime($to)).' 23:59:59"'
    );

    if ($params['min_times']){
      $cond[] = '
        (
          SELECT
            SUM(op2.count)
          FROM
            '.$this->table.' o2
              JOIN '.$orderprodcol->table.' op2 ON op2.order_id=o2.item_id
              JOIN '.$prodscol->table.' p2 ON op2.prod_id=p2.item_id
          WHERE
            datums >= "'.date('Y-m-d H:i:s', strtotime($from)).'" AND
            datums <= "'.date('Y-m-d H:i:s', strtotime($to)).'" AND
            op2.prod_id=op.prod_id
        ) > '.(int)$params['min_times'].'
      ';
    }

    $orderby = 'o.datums';
    if ($params['type'] == 'most_popular')
    {
      $orderby = '
        (
          SELECT
            SUM(op2.count)
          FROM
            '.$this->table.' o2
              JOIN '.$orderprodcol->table.' op2 ON op2.order_id=o2.item_id
              JOIN '.$prodscol->table.' p2 ON op2.prod_id=p2.item_id
          WHERE
            datums >= "'.date('Y-m-d', strtotime($from)).' 00:00:00" AND
            datums <= "'.date('Y-m-d', strtotime($to)).' 23:59:59" AND
            op2.prod_id=op.prod_id
        ) DESC
      ';
    }

    if ($params['prod_limit'])
    {
      $data = $this->findMostPopularProducts($orderprodcol, $prodscol, $from, $to, $params['prod_limit']);
      $ids = array();
      foreach ($data as $row)
      {
        $ids[] = $row['prod_id'];
      }

      if (!$ids) return array();

      $cond[] = 'op.prod_id IN ('.implode(',', $ids).')';
      $orderby = 'count DESC';
    }

    $sql = '
      SELECT
        op.prod_id,
        SUM(op.count) AS count,
        DATE(o.datums) AS date,
        p.name_lv
      FROM
        '.$this->table.' o
          JOIN '.$orderprodcol->table.' op ON op.order_id=o.item_id
          JOIN '.$prodscol->table.' p ON op.prod_id=p.item_id
      WHERE
       '.implode(' AND ', $cond).'
      GROUP BY DATE(o.datums), op.prod_id
      ORDER BY '.$orderby.'
    ';

    return sqlQueryData($sql);

  }

  public function findMostPopularProducts($orderprodcol, $prodscol, $from, $to, $limit)
  {
    $cond = array(
      'datums >= "'.date('Y-m-d H:i:s', strtotime($from)).'"',
      'datums < "'.date('Y-m-d H:i:s', strtotime($to)).'"'
    );

    $sql = '
      SELECT prod_id, count FROM (
        SELECT
          op.prod_id,
          SUM(op.count) AS count
        FROM
          '.$this->table.' o
            JOIN '.$orderprodcol->table.' op ON op.order_id=o.item_id
            JOIN '.$prodscol->table.' p ON op.prod_id=p.item_id
        WHERE
         '.implode(' AND ', $cond).'
        GROUP BY op.prod_id
      ) t ORDER BY count DESC LIMIT '.(int)$limit.'
    ';

    return sqlQueryData($sql);

  }

	public function GetEditData($page, &$count, &$recordposition, $order = ''){
    $query_params = $this->GetDefaultQueryParams($page, $order);
    $count = $this->ItemCount($query_params['countwhere']);
    if(isset($recordposition) && $recordposition)
    {
      $recordposition = $this->GetDBRecordPosition($query_params, $recordposition);
    }
    $data = $this->GetDBData($query_params);

    foreach($data as &$entry){

      $entry['review_sent'] = $entry['review_sent'] ? "Jā" : "Nē";
      $entry['review_stars'] = $entry['review_stars'] ? $entry['review_stars'] : "Nav vērtēts";

    }

    return $data;

  }

}