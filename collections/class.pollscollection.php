<?
/*
 * Datateks.lv Constructor component
 * Created by Raivo Fismeisters, 2010, raivo@datateks.lv
 * Project:
*/

include_once('class.dbcollection.php');

class pollscollection extends dbcollection
{
  function pollscollection($name, $id)
  {
    $this->DBcollection($name, $id);
    $this->type = "pollscollection";

    $this->properties_assoc = Array(

      "question"        => Array(
        "label"     => "Question:",
        "type"      => "customtext",
        "rows"      => "6",
        "cols"      => "30",
        "dialog"    => "?module=wysiwyg&canpreview='+((window.parent.dialogArguments)?1:0)+'&site_id=".$GLOBALS['site_id']."&page_id=".$this->page_id,
        "dialogw"   => "720",
        "dialogh"   => "450",
        "dlg_res"   => true,
        "multilang" => true
      ),

      'is_active' => array(
        'label' => 'Is active ?',
        'type' => 'check',
        'column_type' => 'tinyint(1)',
        'column_index' => 1,
        'value' => 1
      ),

      'is_archived' => array(
        'label' => 'Is archived ?',
        'type' => 'check',
        'column_type' => 'tinyint(1)',
        'column_index' => 1,
      )

    );

    $this->columns = Array(
      "question_lv" => array(
        'title' => 'Question'
      ),

      "choices"      => Array(
        "title"     => "Choices:",
        "subrelated_collection" => "pollchoicescollection",
        "subrelated_field" => "question_id",
        "format"        => "[if {%_self%}==1]<b>{%_self%}</b> choice[else]<b>{%_self%}</b> choices[/if]"
      ),

      'is_active' => array('title' => 'Is active ?')

    );

    $this->postInit();
  }

  function IsEditableOutside()
  {
    $this->description = $this->type;
    $this->longname = $this->name;
    return true;
  }

  function getRandom()
  {
    $count = $this->itemCount('is_active=1 AND is_archived=0');
    if (!$count) return null;

    $i = mt_rand(0, $count-1);
    return sqlQueryRow('SELECT * FROM '.$this->table.' WHERE is_active=1 AND is_archived=0 LIMIT '.$i.', 1');
  }

  function getArchived()
  {
    return sqlQueryData('SELECT * FROM '.$this->table.' WHERE is_active=1 AND is_archived=1 ORDER BY item_id DESC');
  }

  function processItemBeforeSave($ind, &$item, $create, &$jsmessage)
  {
    $item['is_active'] = (int)(boolean)$item['is_active'];
    $item['is_archived'] = (int)(boolean)$item['is_archived'];

    $jsmessage = '';
    return true;
  }

}

?>