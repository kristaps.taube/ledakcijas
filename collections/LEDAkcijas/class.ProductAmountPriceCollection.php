<?php

namespace LEDAkcijas;

use \DB;
use LEDAkcijas\CategoryAmountStepCollection;

class ProductAmountPriceCollection extends \pdocollection
{

    function __construct($name = '', $id = '')
    {

        parent::__construct($name,$id);

        //Properties array definition
        $this->properties_assoc = [

            "product_id" => Array(
                "label" => "Produkts:",
                "type" => "relation",
                "column_type" => "int(32)",
                "column_index" => 1,
                "related_collection" => "shopprodcollection",
                "relation_format" => "{%name_lv%}",
            ),

            "category_step_id" => Array(
                "label" => "Solis:",
                "type" => "relation",
                "column_type" => "int(32)",
                "column_index" => 1,
                "related_collection" => "LEDAkcijas\CategoryAmountStepCollection",
                "relation_format" => "{%count_from%} - {%count_to%}",
            ),

            "price" => [
                "label" => "No:",
                "type" => "text",
                "column_type" => "double(10,2)",
            ],


        ];

        //Collection display table definition
        $this->columns = [
            "category_step_id" => ["title" => "Solis"],
            "price" => ["title" => "Cena"],
        ];

        $this->PostInit();

    }


    public function getByProduct($id)
    {

        $categorysteps = CategoryAmountStepCollection::getInstance();

        $data = DB::GetTable('
            SELECT ap.*, s.count_from, s.count_to
            FROM `'.$this->table.'` AS ap
            JOIN `'.$categorysteps->table.'` AS s ON s.item_id = ap.category_step_id
            WHERE
                product_id = :pid
        ', [':pid' => $id]);
        $result = [];
        foreach($data as $entry){
            $result[$entry['category_step_id']] = $entry;
        }
        return $result;
    }

    function IsEditableOutside()
    {
        $this->description = 'Collection: '.get_class();
        $this->longname = $this->name;
        return true;
    }

}