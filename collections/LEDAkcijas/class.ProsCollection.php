<?php

namespace LEDAkcijas;

use Constructor\App;

class ProsCollection extends \pdocollection
{

    function __construct($name = '',$id = '')
    {

        parent::__construct($name, $id);

        //Properties array definition
        $this->properties_assoc = [

            "text" => [
                "label" => "Text:",
                "type" => "text",
                "multilang" => true,
            ],

            "link"    => [
                "label"     => "Link:",
                "type"      => "dialog",
                "dialog"    => "?module=dialogs&action=links&site_id=".App::$app->getSiteId(),
                "multilang" => true ,
            ],

            "image"    => [
                "label"     => "Image path",
                "type"      => "dialog",
                "dialog"    => "?module=dialogs&action=filex&site_id=".App::$app->getSiteId()."&view=thumbs",
                "dialogw"   => "600",
            ],

        ];

        //Collection display table definition
        $this->columns = [
            "text_lv" => ["title" => "Teksts"],
        ];

        $this->PostInit();

    }

    function IsEditableOutside()
    {
        $this->description = 'Collection: '.get_class();
        $this->longname = $this->name;
        return true;
    }

}