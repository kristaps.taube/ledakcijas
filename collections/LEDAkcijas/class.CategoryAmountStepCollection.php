<?php

namespace LEDAkcijas;

use \DB;

class CategoryAmountStepCollection extends \pdocollection
{
  
    function __construct($name = '', $id = '')
    {

        parent::__construct($name,$id);

        //Properties array definition
        $this->properties_assoc = [

            "category_id" => Array(
                "label" => "Kategorija:",
                "type" => "relation",
                "column_type" => "int(32)",
                "column_index" => 1,
                "related_collection" => "shopcatcollection",
                "relation_format" => "{%title_lv%}",
            ),

            "count_from" => [
                "label" => "No:",
                "type" => "text",
                "column_type" => "int(32)",
            ],

            "count_to" => [
                "label" => "Līdz:",
                "type" => "text",
                "column_type" => "int(32)",
            ],

        ];

        //Collection display table definition
        $this->columns = [
            "category_id" => ["title" => "Kategorija"],
            "count_from" => ["title" => "No"],
            "count_to" => ["title" => "Līdz"],
        ];

        $this->PostInit();

    }

    function IsEditableOutside()
    {
        $this->description = 'Collection: '.get_class();
        $this->longname = $this->name;
        return true;
    }

    public function getByCategory($cat_id)
    {
        return DB::GetTable("SELECT * FROM `".$this->table."` WHERE category_id = :cid", [':cid' => $cat_id]);
    }

    public function deleteMissing($steps, $category_id)
    {

        $prod_amount_price_col = \LEDAkcijas\ProductAmountPriceCollection::getInstance();

        $current = $this->getByCategory($category_id);

        foreach($current as $current_step){

            $found = false;

            foreach($steps as $step){
                if($step[0] == $current_step['count_from'] && $step[1] == $current_step['count_to']){
                    $found = true;
                    break;
                }
            }

            if(!$found){
                $this->Delete($current_step['item_id']);
                $prod_amount_price_col->Delete(['category_step_id' => $current_step['item_id']]);
            }

        }

    }

    public function getStepId($from, $to, $category)
    {

        $result = DB::GetRow("SELECT * FROM `".$this->table."` WHERE category_id = :cid AND count_from = :from AND count_to = :to", [':cid' => $category, ':from' => $from, ':to' => $to]);

        $id = $result ? $result['item_id'] : $this->Insert(['category_id' => $category, 'count_from' => $from, 'count_to' => $to]);

        return $id;

    }


}