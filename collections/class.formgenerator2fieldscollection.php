<?

include_once('class.dbcollection.php');

class formgenerator2fieldscollection extends dbcollection
{
  function formgenerator2fieldscollection($name, $id)
  {
    $this->DBcollection($name, $id);
    $this->type = "formgenerator2fieldscollection";

    $this->properties_assoc = Array(

      "page_id" => Array(
        "label"     => "Page ID:",
        "type"      => "hidden",
        "column_type" => "int(10)",
        "column_index" => 1
      ),

      "type" => Array(
        "label"     => "Type:",
        "type"      => "list",
        'lookupassoc' => array(
          '' => '(neviens)',
          'text' => 'Text',
          'textarea' => 'Multiline text',
          'submit' => 'Submit button',
          'title' => 'Title',
          'list' => 'Drop-down list',
          'radiolist' => 'Radio buttons',
          'checklist' => 'Checkboxes',
          'line' => 'Separator - line',
          'space' => 'Separator - empty line'
        )
      ),

      "label" => Array(
        "label"     => "Name:",
        "type"      => "str",
        "multilang" => 1
      ),

      "items" => Array(
        "label"     => "Items:",
        "type"      => "customtext",
        "rows"      => 7,
        "multilang" => 1
      ),

      "first_item_selected" => Array(
        "label"     => "First item selected:",
        "type"      => "check",
        "column_type" => "tinyint(1)"
      ),

      "validate_type" => Array(
        "label"     => "Validation type:",
        "type"      => "list",
        "lookupassoc" => array('' => 'without validation', 'email' => 'e-mail address')
      ),

      "req" => Array(
        "label"     => "Required to fill:",
        "type"      => "check",
        "column_type" => "tinyint(1)"
      ),

    );

    $this->columns = Array(
      "label_lv"     => Array(
        "title"     => "Name:",
      ),

    );

    $this->postInit();
  }

  function getData($params)
  {
    $sql = '';

    $cond = array();
    if ($params['page_id'])
      $cond[] = 'page_id='.(int)$params['page_id'];

    if ($cond)
      $sql .= ' WHERE '.implode(' AND ', $cond);

    return sqlQueryDataAssoc('SELECT * FROM '.$this->table.$sql.' ORDER BY ind');
  }

}

?>