<?
//Serveris.LV Constructor collection module
//Created by Aleksandrs Selickis, 2004

include_once('class.collection.php');

class ForumPostCollection extends collection
{
  //Class initialization
  function ForumPostCollection($name,$id)
  {
    $this->Collection($name,$id);
    $this->type = "forumpostcollection";

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "Category index",
        "size"      => "35",
        "type"      => "str"
      ),

      "it1"    => Array(
        "label"     => "User name",
        "size"      => "35",
        "type"      => "str"
      ),

      "it2"    => Array(
        "label"     => "Text",
        "size"      => "35",
        "type"      => "str"
      ),

      "it3"    => Array(
        "label"     => "Time",
        "size"      => "35",
        "type"      => "str"
      ),

      "it4"    => Array(
        "label"     => "IP",
        "size"      => "35",
        "type"      => "str"
      )

    );

    //Collection display table definition
    $this->columns = Array(

      "col0"        => Array(
        "width"     => "10%",
        "title"     => "Category index"
      ),

      "col1"        => Array(
        "width"     => "20%",
        "title"     => "User name"
      ),

      "col2"        => Array(
        "width"     => "45%",
        "title"     => "Text"
      ),

      "col3"        => Array(
        "width"     => "10%",
        "title"     => "Time"
      ),

      "col4"        => Array(
        "width"     => "15%",
        "title"     => "IP"
      )

    );




  }




}

?>
