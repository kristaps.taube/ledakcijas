<?
//Serveris.LV Constructor component
//Created by Aleksandrs Selickis, 2004, alex@datateks.lv

include_once('class.dbcollection.php');

class linklistdbcollection extends dbcollection
{
  //Class initialization
  function linklistdbcollection($name,$id)
  {
    $this->dbcollection($name,$id);
    $this->type = "linklistdbcollection";

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "Description",
        "size"      => "35",
        "type"      => "str",
        "column_name" => "description"
      ),

	  "it1"    => Array(
        "label"     => "Link:",
        "type"      => "dialog",
        "dialog"    => "?module=dialogs&action=links&site_id=".$GLOBALS['site_id'],
        "column_name" => "link"
      )

    );

    //Collection display table definition
    $this->columns = Array(

      "description"        => Array(
        "width"     => "50%",
        "title"     => "Description"
      ),

      "link"        => Array(
        "width"     => "50%",
        "title"     => "Link"
      )

    );




  }




}

?>
