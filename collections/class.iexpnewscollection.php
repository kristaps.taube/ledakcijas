<?
//Serveris.LV Constructor collection module
//Created by Aivars Irmejs, 2003, aivars@serveris.lv

include_once('class.newscollection.php');

class iexpnewscollection extends newscollection
{
  //Class initialization
  function iexpnewscollection($name,$id)
  {
    $this->Collection($name,$id);
    $this->type = "iexpnewscollection";

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "Autors:",
        "size"      => "35",
        "type"      => "str"
      ),

      "it1"    => Array(
        "label"     => "Virsraksts:",
        "size"      => "35",
        "type"      => "str"
      ),

      "it2" => Array(
        "label"     => "Teksts:",
        "type"      => "wysiwyg",
        "rows"      => "7",
        "cols"      => "50",
        "dialog"    => "?module=wysiwyg&site_id=".$GLOBALS['site_id'],
        "dialogw"   => "720",
        "dialogh"   => "450",
        "dlg_res"   => true
      ),

      "it3" => Array(
        "label"     => "Pilnais teksts:",
        "type"      => "hidden",
        "rows"      => "7",
        "cols"      => "50",
        "dialog"    => "?module=wysiwyg&site_id=".$GLOBALS['site_id'],
        "dialogw"   => "720",
        "dialogh"   => "450",
        "dlg_res"   => true
      ),

      "it4"    => Array(
        "label"     => "Datums:",
        "type"      => "str",
        "size"      => "30"
      )

    );

    //Collection display table definition
    $this->columns = Array(

      "col1"        => Array(
        "width"     => "20%",
        "title"     => "Title"
      ),

      "col2"    => Array(
        "width"     => "30%",
        "title"     => "Short Text"
      ),

      "col3"    => Array(
        "width"     => "30%",
        "title"     => "Text"
      ),

      "col4"     => Array(
        "width"     => "20%",
        "title"     => "Date"
      )


    );

    //Set default value of date to current time
    $this->properties['it4']['value'] = date("Y-m-d H:i:s", time());


  }


  //Override GetDBData for postprocessing the retrieved data
  function GetDBData($changeDate = true)
  {
    //Call original GetDBData()
    $r = parent::GetDBData();
    //Change date to short date format
    if($changeDate)
    {
      foreach($r as $key => $row)
      {
        $r[$key]["col4"] = date('d.m.Y',strtotime($r[$key]["col4"]));
      }
    }
    return $r;
  }

  //Override ChangeItem to maintain order of news items sorted by date
  function ChangeItem($ind, $properties)
  {
    //Call the original ChangeItem()
    parent::ChangeItem($ind, $properties);
    //Sort list by date
    $a = $this->getItem($ind);
    $date = $a[4];
    do
    {
      $lastind = $ind;
      $a = $this->getItem($ind-1);
      if($a[4]<$date)
      {
        $ind = parent::moveItemUp($ind);
      }else
      {
        $a = $this->getItem($ind+1);
        if($a[3]>$date)
        {
          $ind = parent::moveItemDown($ind);
        }
      }
    }while($ind!=$lastind);  //Stop when row hasn't been moved
    return $ind;
  }


}

?>
