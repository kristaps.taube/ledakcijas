<?
//Serveris.LV Constructor component
//Created by Aleksandrs Selickis, 2004, alex@datateks.lv

include_once('class.dbcollection.php');

class lmtaraksticollection extends dbcollection
{
  //Class initialization
  function lmtaraksticollection($name,$id)
  {
    $this->dbcollection($name,$id);
    $this->type = "lmtaraksticollection";
    

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "Raksta fails:",
        "type"      => "dialog",
        "dialog"    => "?module=dialogs&action=filex&site_id=".$GLOBALS['site_id'],
        "dialogw"   => "600"
      ),

	  "it1"    => Array(
        "label"     => "Title:",
        "type"      => "str"
      ),

      "it2"    => Array(
        "label"     => "Description",
        "type"      => "wysiwyg"
      )

    );

    //Collection display table definition
    $this->columns = Array(

      "col1"        => Array(
        "width"     => "40%",
        "title"     => "Description"
      ),

      "col2"        => Array(
        "width"     => "60%",
        "title"     => "Link"
      )

    );




  }




}

?>
