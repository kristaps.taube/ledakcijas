<?
//Serveris.LV Constructor component
//Created by Aleksandrs Selickis, 2005, alex@datateks.lv
//Project: SmartCar

include_once('class.collection.php');

class smartcaritemlistcollection extends collection
{
  //Class initialization
  function smartcaritemlistcollection($name,$id)
  {
    $this->Collection($name,$id);
    $this->type = "smartcaritemlistcollection";

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "Image path",
        "type"      => "dialog",
        "dialog"    => "?module=dialogs&action=filex&site_id=".$GLOBALS['site_id']."&view=thumbs",
        "dialogw"   => "600"
      ),

	  "it1"    => Array(
        "label"     => "Link:",
        "type"      => "dialog",
        "dialog"    => "?module=dialogs&action=links&site_id=".$GLOBALS['site_id'],
      ),

      "it2"    => Array(
        "label"     => "Title",
        "size"      => "35",
        "type"      => "str"
      ),

      "it3"    => Array(
        "label"     => "Description",
        "type"      => "wysiwyg",
        "rows"      => "7",
        "cols"      => "50",
        "dialog"    => "?module=wysiwyg&site_id=".$GLOBALS['site_id'],
        "dialogw"   => "720",
        "dialogh"   => "450",
        "dlg_res"   => true
      )

    );

    //Collection display table definition
    $this->columns = Array(

      "col0"        => Array(
        "width"     => "20%",
        "title"     => "Image path"
      ),

      "col1"        => Array(
        "width"     => "30%",
        "title"     => "Link"
      ),

      "col2"        => Array(
        "width"     => "20%",
        "title"     => "Title"
      ),

      "col3"        => Array(
        "width"     => "30%",
        "title"     => "Description"
      )

    );




  }




}

?>