<?php

class PastastacijaCollection extends pdocollection
{

    public function __construct($name = '',$id = '')
    {

        parent::__construct($name, $id);

        //Properties array definition
        $this->properties_assoc = [

            "city" => [
                "label" => "Pilsēta:",
                "type" => "text",
                "multilang" => true
            ],

            "address" => [
                "label" => "Adrese:",
                "type" => "text",
                "multilang" => true
            ],

            "coords" => [
                "label" => "Koordinātas:",
                "type" => "text",
            ],

            "import_id"    => Array(
                "label"     => "ID:",
                "type"      => "hidden",
                "column_type"      => "int(32)",
            ),

        ];

        //Collection display table definition
        $this->columns = [
            "city_lv" => ["title" => "Pilsēta"],
            "address_lv" => ["title" => "Adrese"],
        ];

        $this->PostInit();

    }

    public function getByImportID($id){
        return $this->getRow(['where' => "import_id = :id", 'params' => [':id' => $id]]);
    }

    public function IsEditableOutside()
    {
        $this->description = 'Collection: '.get_class();
        $this->longname = $this->name;
        return true;
    }

}