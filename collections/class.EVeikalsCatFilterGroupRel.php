<?php

class EVeikalsCatFilterGroupRel extends pdocollection
{

  function __construct($name='',$id=''){

    parent::__construct($name,$id);

    //Properties array definition
    $this->properties_assoc = Array(

        "category_id" => Array(
            "label"     => "Category id:",
            "type"      => "relation",
            "column_type" => "int(32)",
            "column_index" => 1,
            "related_collection" => "shopcatcollection",
            "relation_format" => "{%title_lv%}",
        ),

        "filter_group_id" => Array(
            "label"     => "Filter group id:",
            "type"      => "relation",
            "column_type" => "int(32)",
            "column_index" => 1,
            "related_collection" => "EVeikalsFilterGroups",
            "relation_format" => "{%title_lv%}",
        ),

    );

    //Collection display table definition
    $this->columns = Array(

      "category_id"        => Array(
        "width"     => "50%",
        "title"     => "Category(id):"
      ),

      "filter_group_id"        => Array(
        "width"     => "50%",
        "title"     => "Filter group(id):"
      ),

    );

     $this->PostInit();
  }

  public function getImportedFilterGroup($cat_id)
  {

    return DB::GetValue("
        SELECT r.filter_group_id
        FROM `".$this->table."` AS r
        LEFT JOIN `".$this->filter_group_id->table."` AS fg ON fg.item_id = r.filter_group_id
        WHERE
            r.category_id = :cid AND
            fg.imported = 1
    ", [':cid' => $cat_id]);

  }

  function IsEditableOutside()
  {
    $this->description = 'Filtru grupu un kategoriju attiecības';
    $this->longname = $this->name;
    return false;
  }

  function processFormBeforeDisplay(&$form_data, $create, $item){

  }

  function getByFilter($filter){
    return sqlQueryData("SELECT * FROM `".$this->table."` WHERE filter_group_id=".$filter);
  }

  function getByCategory($cat){
    return sqlQueryData("SELECT * FROM `".$this->table."` WHERE category_id=".$cat);
  }

  function getGroupIds($cat){
    $data = $this->getByCategory($cat);

    $return = Array();
    foreach($data as $row){
      $return[] = $row['filter_group_id'];
    }

    return $return;

  }

  function DeleteByCatAndFilter($cat, $filter){
    sqlQuery("DELETE FROM `".$this->table."` WHERE category_id = '".$cat."' AND filter_group_id = '".$filter."'");
  }

  function getByCatIds($ids){
    return sqlQueryData("SELECT * FROM `".$this->table."` WHERE category_id IN (".implode(",", $ids).")");
  }


}
