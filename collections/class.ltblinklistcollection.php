<?
//Serveris.LV Constructor component
//Created by Aleksandrs Selickis, 2004, alex@datateks.lv

include_once('class.collection.php');

class ltblinklistcollection extends collection
{
  //Class initialization
  function ltblinklistcollection($name,$id)
  {
    $this->Collection($name,$id);
    $this->type = "ltblinklistcollection";

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "Path",
        "type"      => "dialog",
        "dialog"    => "?module=dialogs&action=filex&site_id=".$GLOBALS['site_id'],
        "dialogw"   => "600"
      ),

      "it1"    => Array(
        "label"     => "Description",
        "size"      => "35",
        "type"      => "str"
      ),

	  "it2"    => Array(
        "label"     => "Size:",
        "type"      => "str"
      ),

      "it3"    => Array(
        "label"     => "Title",
        "size"      => "35",
        "type"      => "str"
      )


    );

    //Collection display table definition
    $this->columns = Array(

      "col0"        => Array(
        "width"     => "30%",
        "title"     => "Path"
      ),

      "col1"        => Array(
        "width"     => "20%",
        "title"     => "Description"
      ),

      "col2"        => Array(
        "width"     => "20%",
        "title"     => "Size"
      ),

      "col3"        => Array(
        "width"     => "20%",
        "title"     => "Title"
      )

    );




  }




}

?>
