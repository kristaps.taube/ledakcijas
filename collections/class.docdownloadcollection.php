<?
//Serveris.LV Constructor component
//Created by Aivars Irmejs, 2007

include_once('class.dbcollection.php');

class docdownloadcollection extends dbcollection
{
  //Class initialization
  function docdownloadcollection($name,$id)
  {
    $this->dbcollection($name,$id);
    $this->type = "docdownloadcollection";

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "Datums, Laiks:",
        "type"      => "date",
        "dialog"    => "?module=dialogs&action=date&site_id=".$GLOBALS['site_id']."&time=1",
        "column_name" => "datums"
      ),

      'it1'   => array (
        'label'   => 'Faila tips:',
        'type'    => 'list',
        'lookup'  => Array( 'file:File', 'word:Word', 'excel:Excel','pdf:PDF'),
        "column_name" => "tips"
      ),

      "it2"    => Array(
      "label"     => "Fails:",
      "type"      => "dialog",
      "dialog"    => "?module=dialogs&action=filex&site_id=".$GLOBALS['site_id']."&view=thumbs",
      "dialogw"   => "600",
      "column_name" => "fails"
    ),

      "it3"    => Array(
        "label"     => "Nosaukums:",
        "size"      => "35",
        "type"      => "str",
        "column_name" => "virsraksts"
      )

    );

    //Collection display table definition
    $this->columns = Array(

      "datums"        => Array(
        "width"     => "25%",
        "title"     => "Datums, laiks"
      ),

      "tips"        => Array(
        "width"     => "25%",
        "title"     => "Faila tips"
      ),

      "fails"        => Array(
        "width"     => "25%",
        "title"     => "Fails"
      ),

      "virsraksts"        => Array(
        "width"     => "25%",
        "title"     => "Nosaukums"
      )

    );

    $this->postInit();
  }

  function IsEditableOutside()
  {
    $this->description = 'Dokumenti';
    $this->longname = $this->name;
    return false;
  }

}

?>
