<?
require_once('class.dbcollection.php');

class EVeikalsSizeCollection extends dbcollection{

  function __construct($name,$id){

    $this->dbCollection($name,$id);
    $this->type = get_class();

    //Properties array definition
    $this->properties_assoc = Array(

      "group" => Array(
        "label"     => "Grupa:",
        "type"      => "relation",
        "related_collection" => "EVeikalsSizeGroups",
        "relation_format" => "{%group_title_lv%}",
      ),

      "title"    => Array(
        "label"     => "Nosaukums:",
        "type"      => "text",
        "multilang" => true
      ),

    );

    //Collection display table definition
    $this->columns = Array(

      "group"        => Array("title"     => "Grupa"),
      "title_lv"        => Array("title"     => "Nosaukums"),

    );

     $this->PostInit();
  }

  function IsEditableOutside(){
    $this->description = 'Izmēri';
    $this->longname = $this->name;
    return true;
  }

	function getIdsByGroup($group_id){

  	static $cache;

		if($cache[$group_id]) return $cache[$group_id];

		$cache[$group_id] = DB::GetColumn("SELECT item_id FROM `".$this->table."` WHERE `group` = :group", array(":group" => $group_id));

		return $cache[$group_id];

	}


}