<?
include_once('class.dbcollection.php');

class EVeikalsProductColorCollection extends dbcollection{
  
  function __construct($name,$id){

    $this->dbCollection($name,$id);
    $this->type = get_class();

    //Properties array definition
    $this->properties_assoc = Array(

      "prod_id"    => Array(
        "label"     => "Produkta id:",
        "type"      => "text"
      ),

      "color_id"    => Array(
        "label"     => "Krāsas id:",
        "type"      => "text"
      ),

    );

    //Collection display table definition
    $this->columns = Array(
      "prod_id"        => Array("title"     => "Produkta id"),
      "color_id"        => Array("title"     => "Krāsas id"),
    );

     $this->PostInit();
  }

  function productColorCount($prod){
		return DB::GetValue("SELECT COUNT(*) FROM `".$this->table."` WHERE prod_id = :id" , array(":id" => $prod));
	}

  function hasColors($prod){
    return $this->productColorCount($prod) ? true : false;
  }

  function getColorIdsForProduct($prod){
    $colors = array();
    $data = sqlQueryData("SELECT * FROM `".$this->table."` WHERE prod_id = '".$prod."'");

    foreach($data as $entry){
      $colors[] = $entry['color_id'];
    }

    return $colors;
  }

  function DeleteByProdAndColor($prod, $color){
    sqlQuery("DELETE FROM `".$this->table."` WHERE prod_id = '".$prod."' AND color_id = '".$color."'");
  }
  function IsEditableOutside(){
    $this->description = 'Produktu krāsas';
    $this->longname = $this->name;
    return true;
  }

}