<?php

class EVeikalsSavedCarts extends pdocollection{
  
  function __construct($name = '', $id = ''){

    parent::__construct($name, $id);

    //Properties array definition
    $this->properties_assoc = Array(

      "user_id" => Array(
        "label"     => "Lietotājs:",
        "type"      => "relation",
        "related_collection" => "shopusercollection",
        "relation_format" => "{%email%}",
        "column_type" => "int",
        "column_index" => true
      ),

      "data"    => Array(
        "label"     => "data:",
        "type"      => "customtext"
      ),

    );

    //Collection display table definition
    $this->columns = Array(

      "user_id"        => Array("title"     => "Lietotājs"),

    );

     $this->PostInit();
  }

  function IsEditableOutside(){
    $this->description = 'Saved carts';
    $this->longname = $this->name;
    return false;
  }

  function deleteSavedCart($user){
    sqlQuery("DELETE FROM `".$this->table."` WHERE user_id=".$user);
  }

  function getByUser($user){
    return sqlQueryRow("SELECT * FROM `".$this->table."` WHERE user_id=".$user);
  }

  function updateData($user, $data, $id = false){

    if($id){ // we know which row to update
      $this->Update(array("data" => serialize($data)), array("item_id" => $id));
      return $id;
    }

    $row = $this->getByUser($user);

    if($row){ // updating
      $this->Update(array("data" => serialize($data)), array("item_id" => $row['item_id']));
      return $row['item_id'];
    }else{ // adding
      $d = array("user_id" => $user, "data" => serialize($data));
      return $this->Insert($d);
    }

  }

}