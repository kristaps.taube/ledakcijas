<?
//Serveris.LV Constructor collection module
//Created by Aleksandrs Selickis, 2005

include_once('class.dbcollection.php');

class jljnnewscollectionex extends dbcollection
{
  //Class initialization
  function jljnNewsCollectionEx($name,$id)
  {
    $this->dbCollection($name,$id);
    $this->type = "jljnnewscollectionex";

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "Title:",
        "size"      => "35",
        "type"      => "str"
      ),

      "it1" => Array(
        "label"     => "Short text:",
        "type"      => "wysiwyg",
        "rows"      => "7",
        "cols"      => "50",
        "dialog"    => "?module=wysiwyg&site_id=".$GLOBALS['site_id'],
        "dialogw"   => "720",
        "dialogh"   => "450",
        "dlg_res"   => true
      ),

      "it2" => Array(
        "label"     => "Long text:",
        "type"      => "wysiwyg",
        "rows"      => "7",
        "cols"      => "50",
        "dialog"    => "?module=wysiwyg&site_id=".$GLOBALS['site_id'],
        "dialogw"   => "720",
        "dialogh"   => "450",
        "dlg_res"   => true
      ),

      "it3"    => Array(
        "label"     => "Date:",
        "type"      => "str",
        "size"      => "30"
      ),

      "it4"    => Array(
        "label"     => "Author:",
        "type"      => "str",
        "size"      => "30"
      ),

      "it5"    => Array(
        "label"     => "Authorized:",
        "type"      => "hidden",
        "size"      => "30",
      ),

      "it6"    => Array(
        "label"     => "Archived:",
        "type"      => "hidden",
        "size"      => "30"
      ),

      "it7"    => Array(
        "label"     => "Start Date:",
        "type"      => "str",
        "size"      => "30"
      ),

      "it8"    => Array(
        "label"     => "End Date:",
        "type"      => "str",
        "size"      => "30"
      ),

      "it9"    => Array(
        "label"     => "Allow comments:",
        "type"      => "hidden",
        "size"      => "30"
      ),

      "it10"    => Array(
        "label"     => "Region:",
        "type"      => "str",
        "size"      => "30"
      )


    );

    //Collection display table definition
    $this->columns = Array(

      "col0"        => Array(
        "width"     => "10%",
        "title"     => "Title"
      ),

      "col1"    => Array(
        "width"     => "20%",
        "title"     => "Short Text"
      ),

      "col2"    => Array(
        "width"     => "30%",
        "title"     => "Text"
      ),

      "col3"     => Array(
        "width"     => "10%",
        "title"     => "Date"
      ),

      "col4"     => Array(
        "width"     => "10%",
        "title"     => "Author"
      ),

      "col5"     => Array(
        "width"     => "10%",
        "title"     => "Authorized"
      ),

      "col6"     => Array(
        "width"     => "10%",
        "title"     => "Archived"
      ),

      "col7"     => Array(
        "width"     => "10%",
        "title"     => "Start Date"
      ),

      "col8"     => Array(
        "width"     => "10%",
        "title"     => "End Date"
      ),

      "col10"     => Array(
        "width"     => "10%",
        "title"     => "Region"
      ),

    );

    //Set default value of date to current time
    //$this->properties['it3']['value'] = date("Y-m-d H:i:s", time());
      

    if ($_SESSION['userdata']['fname']!="" || $_SESSION['userdata']['lname']!="")
      $name = $_SESSION['userdata']['username']." (".$_SESSION['userdata']['fname']." ".$_SESSION['userdata']['lname'].")";
    else
      $name = $_SESSION['userdata']['username'];
    
    $this->properties['it4']['value'] = $name;

  }

  function IsEditableOutside()
  {
    $this->description = 'Edit site news';
    $this->longname = $this->name;
    return true;
  }



  //Override GetDBData for postprocessing the retrieved data
  function GetDBData($listtype = 0, $isdesign = 0)
  {
   
    //move expired to archive and set expiration date
    $today = getdate();
    $time = mktime(0, 0, 0, $today['mon'], $today['mday'], $today['year']+10);
    sqlQuery("UPDATE `".$this->site_id."_coltable_".$this->name."` SET col6=1, col8=".$time." WHERE col8<".time());
    
    if ($listtype==2)
    {
        //get all authorized items
        //$r = sqlQueryData("SELECT * FROM `".$this->site_id."_coltable_".$this->name."` WHERE col5='1' ORDER BY ind");
        $r = sqlQueryData("SELECT * FROM `".$this->site_id."_coltable_".$this->name."` ".(($isdesign==0)?"WHERE col5='1' AND col7<".time():"WHERE col7<".time())." AND col8>".time()." ORDER BY ind");
    }
    else if ($listtype==1)
    {
        //get archived and authorized items
        //$r = sqlQueryData("SELECT * FROM `".$this->site_id."_coltable_".$this->name."` WHERE col5='1' AND col6='1' ORDER BY ind");
        $r = sqlQueryData("SELECT * FROM `".$this->site_id."_coltable_".$this->name."` WHERE ".($isdesign==0?" col5='1' AND ":"")." col6='1' AND col7<".time()." AND col8>".time()." ORDER BY ind");
    }
    else
    {
        //get not archived and authorized items
        //$r = sqlQueryData("SELECT * FROM `".$this->site_id."_coltable_".$this->name."` WHERE col5='1' AND col6='0' ORDER BY ind");
        $r = sqlQueryData("SELECT * FROM `".$this->site_id."_coltable_".$this->name."` WHERE ".($isdesign==0?" col5='1' AND ":"")." col6='0' AND col7<".time()." AND col8>".time()." ORDER BY ind");
    }

    return $r;
  }

  function FirstValidItem($listtype=0)
  {
    if(!$this->name)
    {
      $this->error = true;
      return false;
    }

    if ($listtype==2)
    {
        //all
        $i = sqlQueryValue("SELECT Min(ind) FROM `".$this->site_id."_coltable_".$this->name."` WHERE col5='1' AND col7<".time()." AND col8>".time()." LIMIT 1");
        return $i;
    }
    else if ($listtype==1)
    {
        //archived
        $i = sqlQueryValue("SELECT Min(ind) FROM `".$this->site_id."_coltable_".$this->name."` WHERE col5='1' AND col6='1' AND col7<".time()." AND col8>".time()." LIMIT 1");
        return $i;
    }
    else
    {
        //not archived
        $i = sqlQueryValue("SELECT Min(ind) FROM `".$this->site_id."_coltable_".$this->name."` WHERE col5='1' AND col6='0' AND col7<".time()." AND col8>".time()." LIMIT 1");
        return $i;
    }
  }

  function Archive($itemcount)
  {
    sqlQuery("UPDATE `".$this->site_id."_coltable_".$this->name."` SET col6='1' WHERE ind>$itemcount");
  }


  //Override ChangeItem to maintain order of news items sorted by date
  function ChangeItem($ind, $properties)
  {
    //Call the original ChangeItem()
    parent::ChangeItem($ind, $properties);
    //Sort list by date
    $a = $this->getItem($ind);
    $date = $a[3];
    do
    {
      $lastind = $ind;
      $a = $this->getItem($ind-1);
      if($a[3]<$date)
      {
        $ind = parent::moveItemUp($ind);
      }else
      {
        $a = $this->getItem($ind+1);
        if($a[3]>$date)
        {
          $ind = parent::moveItemDown($ind);
        }
      }
    }while($ind!=$lastind);  //Stop when row hasn't been moved
    return $ind;
  }

  function ExistsItemEx($ind,$direction,$listtype)
  {
    if ($listtype==2) $cond = " AND col5='1'";
    else if ($listtype==1) $cond = " AND col5='1' AND col6='1'";
    else $cond = " AND col5='1' AND col6='0'";

    if(!$this->name)
    {
      $this->error = true;
      return false;
    }
    //$i = sqlQueryValue("SELECT ind FROM `".$this->site_id."_coltable_".$this->name."` WHERE ind='$ind'");
    if ($direction=="+")
        $i = sqlQueryValue("SELECT min(ind) FROM `".$this->site_id."_coltable_".$this->name."` WHERE ind>'$ind'".$cond);
    if ($direction=="-")
        $i = sqlQueryValue("SELECT max(ind) FROM `".$this->site_id."_coltable_".$this->name."` WHERE ind<'$ind'".$cond);

    if($i)
      return $i;
    else
      return 0;
  }

  //find next item to change of same type
  function MoveItemUp($ind, $listtype)
  {
    //find item with lower index
    $newind = $this->ExistsItemEx($ind,"-",$listtype);
    if($newind > 0)
    {
      $this->SwapItems($ind, $newind);
      return($newind);
    }
    return($ind);
  }
  
  //find next item to change of same type
  function MoveItemDown($ind, $listtype)
  {
    //find item with greater index
    $newind = $this->ExistsItemEx($ind,"+",$listtype);
    if($newind > 0)
    {
      $this->SwapItems($ind,$newind);
      return($newind);
    }
    return($ind);
  }


}

?>
