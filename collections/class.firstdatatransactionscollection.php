<?
//Datateks.lv Constructor component
//Created by Raivo Fismeisters, 2008, raivo@datateks.lv

include_once('class.dbcollection.php');

class firstdatatransactionscollection extends dbcollection
{
  function firstdatatransactionscollection($name, $id)
  {
    $this->DBcollection($name, $id);
    $this->type = "firstdatatransactionscollection";

    $this->currencies = Array(428 => 'LVL', 978 => 'EUR', 840 => 'USD', 941 => 'RSD', 703 => 'SKK', 440 => 'LTL', 233 => 'EEK', 643 => 'RUB', 891 => 'YUM');

    $this->properties = Array(
      "it0" => Array(
        "label"     => "Transakcijas ID:",
        "type"      => "str",
        "column_name" => "trans_id",
        "column_type" => "varchar(50)",
        "column_index" => 1
      ),

      "it1" => Array(
        "label"     => "Summa:",
        "type"      => "str",
        "column_name" => "amount",
        "column_type" => "int(10)"
      ),

      "it2"    => Array(
        "label"     => "Valūta:",
        "type"      => "str",
        "column_name" => "currency",
        "column_type" => "int(10)"
      ),

      "it3"    => Array(
        "label"     => "Klienta IP:",
        "type"      => "str",
        "column_name" => "client_ip",
        "column_type" => "varchar(50)"
      ),


      "it4"    => Array(
        "label"     => "Klienta host:",
        "type"      => "str",
        "column_name" => "client_host"
      ),

      "it5"    => Array(
        "label"     => "Apraksts:",
        "type"      => "str",
        "column_name" => "description"
      ),

      "it6"    => Array(
        "label"     => "Valoda",
        "type"      => "str",
        "column_name" => "lang",
        "column_type" => "varchar(10)"
      ),

      "it7"    => Array(
        "label"     => "DMS OK:",
        "type"      => "str",
        "column_name" => "dms_ok",
        "column_type" => "tinyint(1)"
      ),

      "it8"    => Array(
        "label"     => "Atgriezts:",
        "type"      => "str",
        "column_name" => "reversed",
        "column_type" => "tinyint(1)"
      ),

      "it9"    => Array(
        "label"     => "Rezultāts:",
        "type"      => "str",
        "column_name" => "result",
        "column_type" => "varchar(255)"
      ),

      "it10"    => Array(
        "label"     => "Rezultāta kods:",
        "type"      => "str",
        "column_name" => "result_code",
        "column_type" => "varchar(255)"
      ),

      "it11"    => Array(
        "label"     => "Laiks:",
        "type"      => "str",
        "column_name" => "t_time",
        "column_type" => "int(10)"
      ),

      "it12" => Array(
        "label"     => "Rēķina ID:",
        "type"      => "str",
        "column_name" => "order_id",
        "column_type" => "int(10)",
        "column_index" => 1
      ),

      "it13" => Array(
        "label"     => "Response:",
        "type"      => "str",
        "column_name" => "response"
      ),

    );

    $this->columns = Array(
    );

    $this->postInit();
  }

  function getIdByTransId($trans_id, $vars)
  {
    return sqlQueryValue("SELECT item_id FROM `".$this->table."` WHERE trans_id='".mysql_real_escape_string($trans_id)."'");
  }

  function getIdByOrderId($order_id)
  {
    return sqlQueryValue("SELECT item_id FROM `".$this->table."` WHERE order_id='".$order_id."'");
  }

  function getTransactionByOrderId($order_id, $result, $result_code)
  {
    return sqlQueryRow("SELECT * FROM `".$this->table."` WHERE order_id='".$order_id."' AND result='".$result."' AND result_code='".$result_code."'");
  }
  
  function updateByTransId($trans_id, $vars)
  {
    $a = Array();
    foreach ($vars as $key => $val)
    {
      $a[] = "`".$key."`='".mysql_real_escape_string($val)."'";
    }
    sqlQuery("UPDATE `".$this->table."` SET ".implode(',', $a)." WHERE trans_id='".mysql_real_escape_string($trans_id)."'");
  }

  function getTransaction($id)
  {
    $row = sqlQueryRow("SELECT * FROM `".$this->table."` WHERE item_id='".$id."'");
    if (!$row) return false;

    $row['currency_code'] = $this->currencies[$row['currency']];

    return $row;
  }
}

?>