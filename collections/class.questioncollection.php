<?
include_once('class.dbcollection.php');

class questioncollection extends dbcollection
{
  //Class initialization
  function questioncollection($name,$id)
  {
    $this->DBCollection($name,$id);
    $this->type = "questioncollection";

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "Jautajums:",
        "size"      => "300",
        "type"      => "string",
        "column_name" => "jautajums"
      ),

      "it1"    => Array(
        "label"     => "Tips:",
        "size"      => "20",
        "type"      => "string",
        "column_name" => "tips"
      ),

      "it2"    => Array(
        "label"     => "Svarigums:",
        "size"      => "20",
        "type"      => "string",
        "column_name" => "svarigums"
      ),

    );

    //Collection display table definition
    $this->columns = Array(

      "jautajums"        => Array(
        "width"     => "25%",
        "title"     => "jautajums"
      ),

      "tips"        => Array(
        "width"     => "25%",
        "title"     => "tips"
      ),

      "svarigums"        => Array(
        "width"     => "25%",
        "title"     => "svarigums"
      ),

    );
    $this->postInit();
  }

  function GetQuestionById($id)
  {
    $data =  sqlQueryRow("SELECT jautajums FROM `".$this->table."` WHERE `item_id` = '".$id."' LIMIT 1");
    return $data;
  }

  function GetSequence()
  {
    $data =  sqlQueryData("SELECT `item_id` FROM `".$this->table."` ORDER BY `ind` ASC");
    return $data;
  }
}

?>
