<?php

require_once('class.pdocollection.php');

class EVeikalsOrderStatuses extends pdocollection{
  
  function __construct($name = '', $id = ''){

    parent::__construct($name,$id);

    /*
        $payment_methods['in_shop'] = WebApp::l("Apmaksa preces saņemšanas brīdī veikalā", ['lang' => $lang]);
        $payment_methods['order'] = WebApp::l("Priekšapmaksa pēc rēķina saņemšanas e-pastā", ['lang' => $lang]);
        $payment_methods['online']
    */

    $delivery_types = [
        'shop' => ['in_shop', 'order', 'online'],
        'pakomats' => ['order', 'online'],
        'courier' => ['order', 'online']
    ];

    //Properties array definition
    $this->properties_assoc = Array(

        "status"    => Array(
            "label"     => "Nosaukums:",
            "type"      => "text",
            "multilang" => true
        ),

        "color"    => Array(
            "label"     => "Krāsa:",
            "type"      => "color"
        ),

        "new"    => Array(
            "label"     => "Jauns:",
            "type"      => "check",
            "column_type" => "int(10)",
        ),

        "paid"    => Array(
            "label"     => "Apmaksāts:",
            "type"      => "check",
            "column_type" => "int(10)",
        ),

        "canceled"    => Array(
            "label"     => "Anulēšanas statuss:",
            "type"      => "check"
        ),

        "email_notification_subject"    => Array(
            "label"     => "E-pasta notifikācijas tēma:",
            "type"      => "text",
            "multilang" => true
        ),

    );

    foreach($delivery_types as $delivery_type => $payment_methods){

        foreach($payment_methods as $payment_method){

            $this->properties_assoc['email_notification_'.$delivery_type.'_'.$payment_method] = [
                'label'         => 'E-pasta notifikācija(DM: '.ucfirst($delivery_type).' PM: '.ucfirst($payment_method).')',
                'type'          => 'wysiwyg',
                'cols'          => 50,
                'rows'          => 7,
                'dialog'        => '?module=wysiwyg&site_id='.$this->site_id,
                'dialogw'       => 800,
                'dialogh'       => 600,
                'dlg_res'       => true,
                "multilang" => true
            ];

            $this->properties_assoc['sms_notification_'.$delivery_type.'_'.$payment_method] = [
                'label'         => 'SMS notifikācija(DM: '.ucfirst($delivery_type).' PM: '.ucfirst($payment_method).')',
                'type'          => 'wysiwyg',
                'cols'          => 50,
                'rows'          => 7,
                'dialog'        => '?module=wysiwyg&site_id='.$this->site_id,
                'dialogw'       => 800,
                'dialogh'       => 600,
                'dlg_res'       => true,
                "multilang" => true
            ];

        }

    }


    //Collection display table definition
    $this->columns = [

        "status_lv"        => Array("title"     => "Nosaukums"),
        "color"        => Array("title"     => "Krāsa"),
        "new"        => Array("title"     => "Jauns"),
        "paid"        => Array("title"     => "Apmaksāts"),
        "canceled"        => Array("title"     => "Anulēšanas statuss"),

    ];

     $this->PostInit();
  }

    function processItemBeforeSave($item_id, &$item, $create, &$jsmessage){

        if($item['new']){
            sqlQuery("UPDATE `".$this->table."` SET `new` = 0"); // clear current new
        }

        if($item['paid']){
            sqlQuery("UPDATE `".$this->table."` SET `paid` = 0"); // clear current paid
        }

        $item['new'] = $item['new'] ? 1 : 0;
        $item['paid'] = $item['paid'] ? 1 : 0;
        $item['canceled'] = $item['canceled']? 1 : 0;

        return true;

    }

    function GetEditData($page, &$count, &$recordposition, $order = ''){

        $query_params = $this->GetDefaultQueryParams($page, $order);
        $count = $this->ItemCount($query_params['countwhere']);
        if(isset($recordposition) && $recordposition)
        {
        $recordposition = $this->GetDBRecordPosition($query_params, $recordposition);
        }
        $data = $this->GetDBData($query_params);

        foreach($data as &$entry){

        $entry['color'] = $entry['color'] ? ("<div style='float: left;margin-right: 10px;border: 1px solid black;width: 50px;height:50px;background:".$entry['color']."'></div> ".$entry['color']) : "";
        $entry['new'] = $entry['new']? "Jā" : "Nē";
        $entry['paid'] = $entry['paid']? "Jā" : "Nē";
        $entry['canceled'] = $entry['canceled']? "Jā" : "Nē";

        }

        return $data;

    }

    public function getPaidStatusRow()
    {
        return $this->GetRow(array("where" => 'paid = 1'));
    }

    function getCancelledStatusId()
    {
        return sqlQueryValue("SELECT item_id FROM `".$this->table."` WHERE `canceled` = 1");
    }

    function getNewStatusRow()
    {
        return sqlQueryRow("SELECT * FROM `".$this->table."` WHERE `new` = 1");
    }

    function IsEditableOutside(){
        $this->description = 'Order Statuses';
        $this->longname = $this->name;
        return true;
    }

}