<?
//Serveris.LV Constructor component
//Created by Aivars Irmejs, 2006, aivars@datateks.lv

include_once('class.dbcollection.php');

class newcoltest1comments extends dbcollection
{
  //Class initialization
  function newcoltest1comments($name,$id)
  {
    $this->DBCollection($name,$id);
    $this->type = "newcoltest1comments";

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "Category:",
        "size"      => "35",
        "type"      => "relation",
        "related_collection" => "newcoltest1",
        "relation_format" => "{%vards%} - {%epasts%}",
        "column_name" => "category",
        "validate"  => "required",
      ),

      "it1"    => Array(
        "label"     => "Autors:",
        "size"      => "35",
        "type"      => "str",
        "column_name" => "autors",
        "validate"  => "required",
      ),

      "it2"    => Array(
        "label"     => "Komentārs:",
        "size"      => "35",
        "type"      => "str",
        "column_name" => "komentars",
        "validate"  => "required",
      ),

      "it3"    => Array(
        "label"     => "Laiks:",
        "size"      => "35",
        "type"      => "date",
        "time"      => true,
        "column_name" => "laiks",
      ),


    );

    //Collection display table definition
    $this->columns = Array(

      "category"  => Array(
        "width"     => "",
        "title"     => "Kategorija",
      ),

      "autors"        => Array(
        "width"     => "",
        "title"     => "Name",
        "format"    => '<b>{%autors%}</b>',
      ),

      "komentars"        => Array(
        "width"     => "",
        "title"     => "Komentārs",
      ),

      "laiks"       => Array(
        "width"     => "",
        "title"     => "Laiks",
      ),
    );

    $this->postInit();

  }

  function IsEditableOutside()
  {
    $this->description = 'Testējam piesaistīto kolekciju';
    $this->longname = $this->name;
    return true;
  }


  function IsAddToBottomMode()
  {
    return false;
  }



}

?>
