<?

include_once('class.dbcollection.php');

class forum4postscollection extends dbcollection
{
  //Class initialization
  function forum4postscollection($name,$id)
  {
    $this->dbcollection($name,$id);
    $this->type = "forum4postscollection";

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "Thread ID",
        "type"      => "relation",
        "related_collection" => "forum4threadscollection",
        "relation_format" => "{%th_title%}",
        "column_name" => "post_threadid",
        "column_type" => "int(32)",
        "column_index" => 1
      ),

      "it1"    => Array(
        "label"     => "Author ID",
        "type"      => "relation",
        "related_collection" => "forum4userscollection",
        "relation_format" => "{%login%}",
        "column_name" => "post_authorid",
        "column_type" => "int(32)",
        "column_index" => 1
      ),

      "it2"    => Array(
        "label"     => "Author",
        "type"      => "str",
        "column_name" => "post_author",

      ),

      "it3"    => Array(
        "label"     => "Text",
        "type"      => "html",
        "rows"      => "6",
        "cols"      => "30",
        "column_name" => "post_text",
      ),

      "it4"    => Array(
        "label"     => "Post time",
        "type"      => "date",
        "time"      => true,
        "column_name" => "post_time",
        "column_type" => "varchar(30)",
        "column_index" => 1
      ),

      "it5"    => Array(
        "label"     => "IP",
        "type"      => "str",
        "column_name" => "post_ip"
      ),



    );

    //Collection display table definition
    $this->columns = Array(

      "post_text"        => Array(
        "width"     => "40%",
        "title"     => "Text"
      ),

      "post_author"        => Array(
        "width"     => "20%",
        "title"     => "Author"
      ),

      "post_ip"        => Array(
        "width"     => "20%",
        "title"     => "IP"
      ),

    );

    $this->postInit(); //standarta peecinicializaacija nevis postu inicializaacija
  }

  function getPostsForThread($th_id, $activepage = 0, $pagesize = 0)
  {
    if($pagesize == 0)
      return sqlQueryData('SELECT * FROM `' . $this->table . '` WHERE post_threadid = "' . $th_id . '" ORDER BY post_time');
    else
      return sqlQueryData('SELECT * FROM `' . $this->table . '` WHERE post_threadid = "' . $th_id . '" ORDER BY post_time LIMIT '.($activepage * $pagesize).', '.$pagesize.'');
  }

  function countPostsForThread($th_id)
  {
    return sqlQueryValue('SELECT COUNT(*) FROM `' . $this->table . '` WHERE post_threadid = "' . $th_id . '" ');
  }

  function getPageCount($cat_id, $th_id, $pagesize)
  {
    $itemcount = $this->countPostsForThread($th_id);
    return (intval(ceil($itemcount / $pagesize)));
  }

  function DelPostsByIP($ip, $threadcol)
  {
    $todel = sqlQueryData('SELECT * FROM `' . $this->table . '` WHERE post_ip = "'.$ip.'"');
    sqlQuery('DELETE FROM `' . $this->table . '` WHERE post_ip = "'.$ip.'"');
    foreach($todel as $delpost)
    {
      $threadcol->ThreadPostsChanged($deltpost['post_threadid'], $this);
    }
  }



}

?>
