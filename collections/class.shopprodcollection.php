<?php
//Serveris.LV Constructor component
//Created by Aivars Irmejs, 2006, aivars@datateks.lv


// queryProd() rezultatu atgrieshanai:
define('QUERYPROD_DATA', 1);   // datus
define('QUERYPROD_COUNT', 2);  // skaitu
define('QUERYPROD_DATACOUNT', QUERYPROD_DATA | QUERYPROD_COUNT);     // abus

class shopprodcollection extends pdocollection
{

  function __construct($name = '', $id = '')
  {

    parent::__construct($name, $id);

    $this->properties_assoc = Array(
      "name"    => Array(
        "label"     => "Name",
        "type"      => "str",
        "multilang" => true,
        'column_type' => 'varchar(500)'
      ),

      "small_text"    => Array(
        "label"     => "Small text",
        "type"      => "str",
        "multilang" => true,
        'on_panel'  => 'col1'
      ),

      "price"    => Array(
        "label"     => "Price",
        "size"      => "35",
        "type"      => "str",
        'column_type' => 'decimal(8,2)'
      ),

      "sale_price"    => Array(
        "label"     => "Saleout Price",
        "size"      => "35",
        "type"      => "str",
        'column_type' => 'decimal(8,2)'
      ),

      "item_weight"    => Array(
        "label"     => "Item weight",
        "type"      => "str",
        'column_type' => 'decimal(8,3)'
      ),

      "picture"    => Array(
        "label"     => "Picture",
        "type"      => "file",
      ),

      "description"      => Array(
        "label"     => "Apraksts:",
        "type"      => "wysiwyg",
        "rows"      => "7",
        "cols"      => "50",
        "dialog"    => "?module=wysiwyg&site_id=".$GLOBALS['site_id'],
        "dialogw"   => "720",
        "dialogh"   => "450",
        "dlg_res"   => true,
        "multilang" => true,
        'on_panel'  => 'col2'
      ),

      "specification"      => Array(
        "label"     => "Specifikācija:",
        "type"      => "wysiwyg",
        "rows"      => "7",
        "cols"      => "50",
        "dialog"    => "?module=wysiwyg&site_id=".$GLOBALS['site_id'],
        "dialogw"   => "720",
        "dialogh"   => "450",
        "dlg_res"   => true,
        "multilang" => true,
        'on_panel'  => 'col2'
      ),

      "certificates"      => Array(
        "label"     => "Sertifikāti:",
        "type"      => "wysiwyg",
        "rows"      => "7",
        "cols"      => "50",
        "dialog"    => "?module=wysiwyg&site_id=".$GLOBALS['site_id'],
        "dialogw"   => "720",
        "dialogh"   => "450",
        "dlg_res"   => true,
        "multilang" => true,
        'on_panel'  => 'col2'
      ),

      "category_id"    => Array(
        "label"     => "Category",
        "type"      => "hidden",
        "column_type" => "int(32)",
        "column_index" => 1
      ),

      "cat_ind"     => Array(
        "label"     => "Cat_order",
        "type"      => "hidden",
        "column_type" => "int(32)",
        "column_index" => 1
      ),

      "has_colors" => Array(
        "label" => "Has colors",
        "type" => "hidden",
        "column_type" => "tinyint(1)",
        "default_value" => "0",
      ),

      "has_sizes" => Array(
        "label" => "Has sizes",
        "type" => "hidden",
        "column_type" => "tinyint(1)",
        "default_value" => "0",
      ),

      "has_prices" => Array(
        "label" => "Has prices",
        "type" => "hidden",
        "column_type" => "tinyint(1)",
        "default_value"	=> "0",
      ),

      "featured"    => Array(
        "label"     => "Featured product",
        "type"      => "check",
        "column_type" => "tinyint(1)",
        "column_index" => 1
      ),

      "available_sample"    => Array(
        "label"     => "Pieejams paraugs veikalā",
        "type"      => "check",
        "column_type" => "tinyint(1)",
        "column_index" => 1
      ),

      "available_in_shop"    => Array(
        "label"     => "Pieejams veikalā",
        "type"      => "check",
        "column_type" => "tinyint(1)",
        "column_index" => 1
      ),

      "not_available"    => Array(
        "label"     => "Nav pieejama",
        "type"      => "check",
        "column_type" => "tinyint(1)",
        "column_index" => 1
      ),

      "featured_count"    => Array(
        "label"     => "Featured on count",
        "type"      => "check",
        "column_type" => "tinyint(1)",
        "column_index" => 1
      ),

      "featured_from"    => Array(
        "label"     => "Featured from",
        "type"      => "hidden",
        "column_type" => "date",
      ),

      "featured_to"    => Array(
        "label"     => "Featured to",
        "type"      => "hidden",
        "column_type" => "date",
      ),

      "date_added"     => Array(
        "label"     => "Date added",
        "type"      => "hidden",
        'column_type' => 'int(10)'
      ),

      "date_modified"     => Array(
        "label"     => "Date modified",
        "type"      => "hidden",
        'column_type' => 'int(10)'
      ),

      "disabled"     => Array(
        "label"     => "Disabled",
        "type"      => "check",
        "column_type" => "tinyint(1)",
        "column_index" => 1
      ),

      "pvn"     => Array(
        "label"         => "PVN (%)",
        "type"          => "str",
        'column_type' => 'float'
      ),

      "keywords"    => Array(
        "label"     => "Keywords:",
        "type"      => "html",
        "rows"      => 6,
        "cols"      => 40,
        "multilang" => true,
        'on_panel'  => 'col2'
      ),

      "meta_description"    => Array(
        "label"     => "Meta description:",
        "type"      => "html",
        "rows"      => 6,
        "cols"      => 40,
        "multilang" => true,
        'on_panel'  => 'col2'
      ),

      "additional_header_tags"    => Array(
        "label"     => "Additional header tags:",
        "type"      => "html",
        "rows"      => 20,
        "cols"      => 40,
        "multilang" => true,
        'on_panel'  => 'col2'
      ),

      "url"    => Array(
        "label"     => "URL:",
        "type"      => "str",
        "column_type" => "varchar(255)",
        "column_index" => 1,
        "multilang" => true
      ),

      "shortcut"     => Array(
        "label"     => "Shortcut",
        "type"          => "hidden",
        "column_type"   => "int(10)",
        "column_index"  => 1
      ),

      "brand_id"     => Array(
        "label"         => "Brand",
        "type"          => "list",
        "column_type"   => "int(10)"
      ),

      "code"     => Array(
        "label"         => "Code",
        "type"          => "str",
        "column_index"  => 1,
        'column_type'   => 'varchar(255)'
      ),

      "ean"     => Array(
        "label"         => "EAN",
        "type"          => "str",
        "column_index"  => 1,
        'column_type'   => 'varchar(255)'
      ),

      "times_purchased"     => Array(
        "label"         => "Times purchased",
        "type"          => "hidden",
        "column_type"   => "int(10)",
        "column_index"  => 1,
//        'on_panel'         => 'col1'
      ),

      "times_searched"     => Array(
        "label"         => "Times searched",
        "type"          => "hidden",
        "column_type"   => "int(10)",
        "column_index"  => "1",
//        'on_panel'         => 'col2'
      ),

      "is_new"    => Array(
        "label"     => "Is new product",
        "type"      => "check",
        "column_type" => "tinyint(1)",
        "column_index" => 1
      ),

      "is_top"    => Array(
        "label"     => "Is top product",
        "type"      => "check",
        "column_type" => "tinyint(1)",
        "column_index" => 1
      ),

      "new_until"    => Array(
        "label"     => "New until",
        "type"      => "str",
        "column_type" => "date",
      ),

      "comment"    => Array(
        "label"     => "Comment",
        "type"      => "str",
      ),

      "status"    => Array(
        "label"     => "Status",
        "type"      => "hidden",
        "column_type" => 'tinyint(1)'
      ),

      'prod_count' => array(
        'type' => 'hidden',
        'column_type' => 'int',
        'label' => 'Count:',
        'column_index' => 1
      ),

    );

    //Collection display table definition
    $this->columns = Array(

      "name"        => Array(
        "width"     => "20%",
        "title"     => "Name",
        "multilang" => true,
      ),

      "price"        => Array(
        "width"     => "10%",
        "title"     => "Price"
      ),

      "description"        => Array(
        "width"     => "10%",
        "title"     => "Description",
        "multilang" => true,
      ),

      "sale_price"        => Array(
        "width"     => "10%",
        "title"     => "Saleout price"
      ),

    );

    $this->postInit();
  }

  // $prod: (array) product row or (int) product item_id
  function getProductInfo($prod)
  {
    static $cache = Array();

    $prod_id = is_array($prod) ? $prod['item_id'] : $prod;
    $ckey = $this->site_id.'_'.$this->collection_id.'_'.$prod_id;

    if (isset($cache[$ckey]))
      return $cache[$ckey];

    if (!is_array($prod))
      $prod = $this->getItemByIdAssoc($prod);

    if($prod['shortcut'])
    {
      $cat_id = $prod['category_id'];

      $old_prod = $prod;
      $prod = $this->getItemByIdAssoc(intval($prod['shortcut']));
      if ($prod)
        $prod['category_id'] = $cat_id;

      $langs = getLanguages($this->site_id);
      foreach ($langs as $key => $lrow)
      {
        $prod['url_'.$key] = $old_prod['url_'.$key];
      }

      $prod['item_id'] = $prod_id;

    }


    $cache[$ckey] = $prod;
    return $prod;
  }

  function getHighestProductPrice($cats){
    if(empty($cats)) return 0;
    return sqlQueryValue("SELECT max(price) FROM `".$this->table."` WHERE category_id IN(".implode(",", $cats).") AND disabled = 0");
  }

  public function getByCode($code)
  {
      return DB::GetRow('SELECT * FROM `'.$this->table.'` WHERE code = :code', [':code' => $code]);
  }

  function getByField($field, $value){
    return sqlQueryRow("SELECT * FROM `".$this->table."` WHERE ".$field." = '".mysql_real_escape_string($value)."'");
  }
  
  function getActive(){
    return sqlQueryData("SELECT * FROM `".$this->table."` WHERE disabled=0 and shortcut=0");
  }
  
  function getMaxCatInd($category_id)
  {
    return sqlQueryValue('SELECT MAX(cat_ind) FROM `' . $this->table . '` WHERE category_id = ' . intval($category_id));
  }

  function getProductCategory($prod_id)
  {
    return sqlQueryValue('SELECT category_id FROM `' . $this->table . '` WHERE item_id = ' . intval($prod_id));
  }

  function getProductByURL($url, $lang){
    return DB::GetRow("SELECT * FROM `".$this->table."` WHERE url_".$lang." = :url", [':url' => $url]);
  }

  function getProductByCategoryAndUrl($url, $cat_id, $lang)
  {
    return sqlQueryRow("SELECT * FROM `".$this->table."` WHERE category_id=".$cat_id." AND url_".$lang."='".$url."' LIMIT 1");
  }

  function queryProducts($condition, $page=0, $pagesize=0, $orderby='name_lv', $what='p.*', $datacountflag=QUERYPROD_DATA, $pricescol=null)
  {
    if (!$datacountflag) return null;

    if ($what == '*') $what = 'p.*';

    $where = Array();
    foreach ($condition as $val)
    {
      $where[] = is_array($val) ? '`'.$val[0].'`'.$val[1] : $val;
    }

    if ($where){
    	$where = ' WHERE '.implode(' AND ', $where);
    }else{
    	$where = '';
    }


    $calcrows = '';
    $orderby = $orderby ? ' ORDER BY '.$orderby : '';
    $limit = $page ? ' LIMIT '.(($page - 1) * $pagesize).','.$pagesize : '';

    if ($datacountflag == QUERYPROD_DATACOUNT){
      $calcrows = ' SQL_CALC_FOUND_ROWS';
    }
    else if ($datacountflag == QUERYPROD_COUNT){
      $what = 'COUNT(*) AS count';
      $orderby = '';
    }

    $sql = '';

    if ($pricescol)
    {
      $sql .= ' LEFT JOIN '.$pricescol->table.' pr ON p.item_id=pr.product_id ';
      $orderby = ' GROUP BY p.item_id '.$orderby;
    }

    $result = sqlQueryDataAssoc("SELECT".$calcrows." ".$what." FROM `".$this->table."` p ".$sql.$where.$orderby.$limit);

    if ($datacountflag == QUERYPROD_DATACOUNT)
      return Array($result, sqlQueryValue("SELECT FOUND_ROWS()") );

    return $result;
  }

  function queryProductsCount($condition)
  {
    $data = $this->queryProducts($condition, 0, 0, '', '*', QUERYPROD_COUNT);
    return $data[0]['count'];
  }

  function getCategoryProducts($cat_id)
  {
    return $this->queryProducts(Array(Array('category_id', '='.$cat_id)), 0, 0, 'cat_ind');
  }

  function getProductsAddedAfter($time, $page = 0, $pagesize = 0, $orderby = 'name_lv', $returndisabled = false, $musthavefields = Array())
  {
    $where = '';
    if(!$returndisabled)
    {
      $where = ' AND disabled=0';
      foreach($musthavefields as $mhfield)
      {
        $where .= ' AND `' . $mhfield . '` <> ""';
      }
    }
    if($page)
    {
      $limit = ' LIMIT ' . (($page - 1) * $pagesize) . ',' . $pagesize;
    }else
    {
      $limit = '';
    }
    return sqlQueryData('SELECT * FROM `' . $this->table . '` WHERE date_added + 0 > ' . intval($time) . $where . ' ORDER BY date_added + 0, '.$orderby.' DESC' . $limit);
  }

  function condSearchProducts(&$cond, $query, $searchcols = Array('name_lv', 'description_lv'))
  {
    $q = explode(' ', strtr($query, '!@#$%^&*()_+-=[]{}:;\'",.<>/?~', '                                       '));

    $qwords = Array();
    foreach ($q as $qval)
    {
      $v = trim($qval);
      if ($v != '') $qwords[] = $v;
    }
    if (!$qwords) return false;

    $searchwhere = '';
    foreach($searchcols as $colname)
    {
      if($searchwhere)
        $searchwhere .= ' OR ';

      $a = Array();
      foreach ($qwords as $word)
      {
        $a[] = $colname.' LIKE "%'.$word.'%"';
      }
      $searchwhere .= '('.implode(' AND ', $a).')';
    }
    if($searchwhere)
    {
      $cond[] = '(' . $searchwhere . ')';
    }
    return true;
  }

  public function getFeaturedFromCategory($category_id, $limit = false)
  {

    $cond = ['disabled = 0', 'shortcut = 0'];

    $cond['feat_cond'] = 'featured=1 AND (
      (featured_from = "0000-00-00" AND featured_to = "0000-00-00") OR
      IF (featured_from != "0000-00-00" AND featured_to != "0000-00-00",
        CURDATE() >= featured_from AND CURDATE() <= featured_to,
        IF (featured_from != "0000-00-00",
          CURDATE() >= featured_from,
          CURDATE() <= featured_to
        )
      )
    )';

    $category_ids = \shopcatcollection::getInstance()->getPredecessorIds($category_id);
    $category_ids[] = $category_id;

    $cond[] = 'category_id IN('.implode(",", $category_ids).')';

    $result = DB::GetTable("SELECT * FROM `".$this->table."` WHERE ".implode(" AND ", $cond)." ORDER BY RAND()".($limit ? " LIMIT ".(int)$limit : ''));

    if(!$result){
        unset($cond['feat_cond']);
        $result = DB::GetTable("SELECT * FROM `".$this->table."` WHERE ".implode(" AND ", $cond)." ORDER BY RAND()".($limit ? " LIMIT ".(int)$limit : ''));
    }

    return $result;

  }

  function getFeaturedProducts($page=0, $pagesize=0, $orderby='name_lv', $returndisabled=false, $musthavefields = Array())
  {
    $cond = array();

    foreach ($musthavefields as $name)
    {
      $cond[] = Array($name, '<> ""');
    }

    $cond[] = array('shortcut', '= 0');

    if ($returndisabled)
      $cond[] = Array('disabled', '= 1');
    else
      $cond[] = Array('disabled', '= 0');

    $cond[] = 'featured=1 AND (
      (featured_from = "0000-00-00" AND featured_to = "0000-00-00") OR
      IF (featured_from != "0000-00-00" AND featured_to != "0000-00-00",
        DATE(NOW()) >= featured_from AND DATE(NOW()) <= featured_to,
        IF (featured_from != "0000-00-00",
          DATE(NOW()) >= featured_from,
          DATE(NOW()) <= featured_to
        )
      )
    )';


    return $this->queryProducts($cond, $page, $pagesize, $orderby, '*', QUERYPROD_DATACOUNT);
  }


  function getNewProducts($page=0, $pagesize=0, $orderby='name_lv', $returndisabled=false, $musthavefields = Array())
  {
    $cond = array();

    foreach ($musthavefields as $name)
    {
      $cond[] = Array($name, '<> ""');
    }

    $cond[] = array('shortcut', '=0');

    if ($returndisabled)
      $cond[] = Array('disabled', '= 1');
    else
      $cond[] = Array('disabled', '= 0');

    $cond[] = '(is_new=1 AND (new_until="0000-00-00" OR NOW() < new_until))';

    return $this->queryProducts($cond, $page, $pagesize, $orderby, '*', QUERYPROD_DATACOUNT);
  }

  function isFeaturedProduct($row)
  {
    if (!$row['featured']) return false;

    $from = $row['featured_from'] != '0000-00-00';
    $to = $row['featured_to'] != '0000-00-00';

    if ($row['featured'] && !$from && !$to)
      return true;

    $t = time();

    if ($from && $to)
    {
      if ($t >= strtotime($row['featured_from']) && $t < (strtotime($row['featured_to']) + 86400))
        return true;

    }
    else if ($from)
    {
      if ($t >= strtotime($row['featured_from']))
        return true;

    }
    else
    {
      if ($t < (strtotime($row['featured_to']) + 86400))
        return true;

    }

    return false;
  }


  function isNewProduct($row)
  {
    return $row['is_new'] && (!$row[$prefix.'new_until'] || $row['new_until'] == '0000-00-00' || time() < (strtotime($row['new_until']) + 86400 ) );
  }
  
  function getNewProductCount($time, $returndisabled = false, $musthavefields = Array())
  {
    $where = '';
    if(!$returndisabled)
    {
      $where = ' AND disabled=0';
      foreach($musthavefields as $mhfield)
      {
        $where .= ' AND `' . $mhfield . '` <> ""';
      }
    }
    return sqlQueryValue('SELECT COUNT(*) FROM `' . $this->table . '` WHERE date_added + 0 > ' . intval($time) . $where);
  }

  function initProductProperties(&$properties){

    if (!class_exists('shopmanager')){
      require_once($GLOBALS['cfgDirRoot'].'components/class.shopmanager.php');
    }

    $newprops = $properties;

    foreach ($properties as $key => $val){
      if (substr($key, 0, 5) == 'name_'){
        $klang = substr($key, 5);
        $url = ($properties['url_'.$klang] != '') ? $properties['url_'.$klang] : $val;
        $newprops['url_'.$klang] = shopmanager::transliteratePageName($url);
      }

    }

    $properties = $newprops;

  }

  function add($properties){

    if(!$properties['category_id']) return false;

    static $cache;

    if($cache[$properties['category_id']]){
        $properties['cat_ind'] = $cache[$properties['category_id']] + 1;
    }else{
        $properties['cat_ind'] = DB::GetValue('SELECT MAX(cat_ind) FROM `'.$this->table.'` WHERE category_id = :cat_id', array(":cat_id" => $properties['category_id'])) + 1;
    }

    $cache[$properties['category_id']] = $properties['cat_ind'];

    $properties['date_added'] = $properties['date_added'] ? $properties['date_added'] : time();

    $properties = $this->processPropertyDataTypes($properties);

    return $this->Insert($properties);

  }

    public function Insert($data)
    {

        static $cache;

        if($cache[$data['category_id']]){
            $data['cat_ind'] = $cache[$data['category_id']] + 1;
        }else{
            $data['cat_ind'] = DB::GetValue('SELECT MAX(cat_ind) FROM `'.$this->table.'` WHERE category_id = :cat_id', array(":cat_id" => $data['category_id'])) + 1;
        }

        $langs = getLanguages();
        foreach($langs as $short => $lang){

            if((!isset($data['url_'.$short]) || !$data['url_'.$short]) && $data['name_'.$short]){
                $data['url_'.$short] = transliterateURL($data['name_'.$short]);
            }

        }

        return parent::Insert($data);

    }


	// USE $this->add instead. This method is left for backwards compatibility
  function addProduct($properties){

    if($properties['category_id']){
      $this->initProductProperties($properties);
			$properties = $this->processPropertyDataTypes($properties);

      $maxind = sqlQueryValue('SELECT MAX(cat_ind) FROM `' . $this->table . '` WHERE category_id = ' . intval($properties['category_id']));
      $properties['cat_ind'] = $maxind + 1;

      if (!isset($properties['date_added']))
        $properties['date_added'] = time();


      $properties = $this->processPropertyDataTypes($properties);
      return $this->Insert($properties);
    }
  }

  function changeProduct($id, $properties){
    $this->initProductProperties($properties);

    $properties = $this->processPropertyDataTypes($properties);

    //change all shortcuts to this product
    $data = sqlQueryData('SELECT item_id FROM `' . $this->table . '` WHERE shortcut = ' . intval($id));
    $shproperties = $properties;
    unset($shproperties['shortcut']);
    unset($shproperties['cat_ind']);
    unset($shproperties['category_id']);
    foreach($data as $row)
    {
      $this->ChangeItemByIDAsoc($row['item_id'],$shproperties);
    }
    return $this->ChangeItemByIDAsoc($id,$properties);
  }

  function deleteProduct($item_id)
  {
    //delete all shortcuts to this product
    $data = sqlQueryData('SELECT item_id FROM `' . $this->table . '` WHERE shortcut = ' . intval($item_id));
    foreach($data as $row)
    {
      if($row['item_id'] != $item_id)
        $this->deleteProduct($row['item_id']);
    }

    $p = $this->GetItemByIDAssoc($item_id);
    if($p['category_id'])
    {
      //reorder category products
      sqlQuery('UPDATE `' . $this->table . '` SET cat_ind = cat_ind - 1 WHERE cat_ind > ' . intval($p['cat_ind']) . ' AND category_id = ' . $p['category_id']);
    }
    $this->DelDBItem($item_id);

    //delete from paramvalues
    $row = sqlQueryRow('SHOW TABLE STATUS LIKE "'.$this->table.'_paramvals"');
    if ($row)
      sqlQuery('DELETE FROM `' . $this->table . '_paramvals` WHERE prod_id = ' . $item_id);

  }

  function getImageFolder($prod, $catcol){

  	$prod = is_numeric($prod) ? $this->getItemByIDAssoc($prod) : $prod;

    if(!$prod) return "";

		$catpath = $catcol->getCategoryPath($prod['category_id']);

		$path = '';
		$langs = getLanguages($this->site_id);


    foreach ($catpath as $row){
      $c = "";
			foreach ($langs as $key => $lang){
	      if ($row['title_'.$key]){
	        $c = $row['title_'.$key];
	        break;
        }
      }

      if (!$c) $c = $row['item_id'];
      $path .= transliterateURL($c, '-_0123456789abcdefghijklmnopqrstuvwxyz') . '/';
    }

    if ($prod){
    	foreach ($langs as $key => $lang){
	      $c = "";
	      if ($prod['name_'.$key]){
					$c = $prod['name_'.$key];
	        break;
	      }
      }
      if (!$c) $c = $prod['item_id'];
    }

		$path .= transliterateURL($c, '-_0123456789abcdefghijklmnopqrstuvwxyz') . '/';

    return $path;

  }

  function getBrandsByCategoryIds($cats, $brandscat){

    return sqlQueryData("
      SELECT p.brand_id AS id, count(brand_id) AS count, b.brandname AS brandname
      FROM `".$this->table."` AS p
      LEFT JOIN `".$brandscat->table."` AS b ON b.item_id = p.brand_id
      WHERE
        p.category_id IN(".implode(",", $cats).") AND
        p.disabled = 0 AND
        p.brand_id > 0
      GROUP BY p.brand_id
    ");

  }

  function changeDisabled($prod_id, $disabled)
  {
    $disabled = (int)(boolean)$disabled;

    $this->changeItemByIdAssoc($prod_id, array(
      'disabled' => $disabled
    ));

    sqlQuery('UPDATE '.$this->table.' SET disabled='.$disabled.' WHERE shortcut='.(int)$prod_id);

  }
  
  function deleteCategoryProducts($cat)
  {
    return sqlQuery('DELETE FROM `' . $this->table . '` WHERE category_id = ' . intval($cat));
  }


  function getPrevProductInd($cat_ind, $category_id)
  {
    return sqlQueryRow('SELECT cat_ind, item_id FROM ' . $this->table . ' WHERE cat_ind < ' . $cat_ind . ' AND category_id = ' . $category_id . ' ORDER BY cat_ind DESC LIMIT 1');
  }

  function getNextProductInd($cat_ind, $category_id)
  {
    return sqlQueryRow('SELECT cat_ind, item_id FROM `' . $this->table . '` WHERE cat_ind > ' . $cat_ind . ' AND category_id = ' . $category_id . ' ORDER BY cat_ind LIMIT 1');
  }


  function moveUpProduct($item_id)
  {
    $p = $this->GetItemByIDAssoc($item_id);
    if($p['category_id'])
    {
      //get previous ind
      list($ind, $otherrow) = $this->getPrevProductInd(intval($p['cat_ind']), $p['category_id']);
      if($ind)
      {
        sqlQuery('UPDATE `' . $this->table . '` SET cat_ind = ' . $ind . ' WHERE item_id = ' . $p['item_id']);
        sqlQuery('UPDATE `' . $this->table . '` SET cat_ind = ' . $p['cat_ind'] . ' WHERE item_id = ' . $otherrow);
      }
    }

  }

  function moveDownProduct($item_id)
  {
    $p = $this->GetItemByIDAssoc($item_id);
    if($p['category_id'])
    {
      //get previous ind
      list($ind, $otherrow) = $this->getNextProductInd(intval($p['cat_ind']), $p['category_id']);
      if($ind)
      {
        sqlQuery('UPDATE `' . $this->table . '` SET cat_ind = ' . $ind . ' WHERE item_id = ' . $p['item_id']);
        sqlQuery('UPDATE `' . $this->table . '` SET cat_ind = ' . $p['cat_ind'] . ' WHERE item_id = ' . $otherrow);
      }
    }

  }

  function getFullProductList($catcol, $lang)
  {
    $data = sqlQueryData('SELECT P.*, C.title_' . $lang . ' FROM `' . $this->table . '` P LEFT JOIN `'.$catcol->table.'` C ON P.category_id = C.item_id ORDER BY P.name_' . $lang);
    return $data;
  }

  function getRelatedProducts($prod_id, $relatedprodscol)
  {
    return sqlQueryData('SELECT p.*, r.item_id AS relation_id FROM `'.$this->table.'` AS p LEFT JOIN `'.$relatedprodscol->table.'` AS r ON p.item_id=r.related_id WHERE r.prod_id='.$prod_id.' ORDER BY r.ind');
  }

  function getDuplicateURLProducts($prod)
  {
    if (!$prod || !$prod['category_id']) return null;

    $urls = Array();
    foreach ($prod as $key => $val)
    {
      if (substr($key, 0, 4) != 'url_') continue;
      if (empty($val)) continue;

      $urls[$key] = $key.'="'.addslashes($val).'"';
    }
    if (!$urls) return null;

    return sqlQueryData("SELECT * FROM `".$this->table."` WHERE category_id=".$prod['category_id']." AND (".implode(' OR ', $urls).") AND item_id!=".$prod['item_id']);
  }

  function getCategoryProductByURL($cat_id, $lang, $url, $except_id=0)
  {
    $sql = '';
    if ($except_id)
      $sql = ' AND item_id != '.$except_id;

    return sqlQueryRow("SELECT * FROM ".$this->table." WHERE category_id=".(int)$cat_id." AND url_".$lang."=:url".$sql.' LIMIT 1', array(":url" => $url));
  }

  function getTopPurchasedProducts($cat_id, $limit=0)
  {
    $lq = $limit ? ' LIMIT '.$limit : '';
    return sqlQueryDataAssoc("SELECT * FROM `".$this->table."` WHERE category_id=".$cat_id." AND times_purchased > 0 ORDER BY times_purchased DESC ".$lq);
  }

  function getTopSearchedProducts($cat_id, $limit=0)
  {
    $lq = $limit ? ' LIMIT '.$limit : '';
    return sqlQueryDataAssoc("SELECT * FROM `".$this->table."` WHERE category_id=".$cat_id." AND times_searched > 0 ORDER BY times_searched DESC ".$lq);
  }
  
	
	function updateProductsTimesPurchased($pids, $automatic_stock_update = false)
  {
    foreach ($pids as $id => $count)
    {
      if($automatic_stock_update)
				sqlQuery("UPDATE `".$this->table."` SET times_purchased=times_purchased+".((int)$count).",prod_count=prod_count-".((int)$count).", disabled=IF(((prod_count-".((int)$count).") <= 0), 1, 0) WHERE item_id=".$id);
			else
				sqlQuery("UPDATE `".$this->table."` SET times_purchased=times_purchased+".((int)$count)." WHERE item_id=".$id);
		}
  }
	
	function changeBackProdCount($pids)
  {
    foreach ($pids as $id => $count)
    {
      sqlQuery("UPDATE `".$this->table."` SET times_purchased=times_purchased-".((int)$count).",prod_count=prod_count+".((int)$count).", disabled=IF(((prod_count+".((int)$count).") <= 0), 1, 0) WHERE item_id=".$id);
    }
  }
	
  function addTimesSearched($prod_id, $times)
  {
    sqlQuery("UPDATE `".$this->table."` SET times_searched=times_searched+".$times." WHERE item_id=".$prod_id);
  }

  function IsEditableOutside()
  {
    $this->description = $this->type;
    $this->longname = $this->name;
    return false;
  }

  function getProdByCategory($cat_id, $code='')
  {
    $sql = '';
    if ($code != '')
      $sql .= ' AND code="'.$code.'"';

    return sqlQueryRow('SELECT * FROM '.$this->table.' WHERE category_id='.$cat_id.$sql.' LIMIT 1');
  }


}

?>
