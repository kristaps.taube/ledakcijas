<?
/*
 * Datateks.lv Constructor component
 * Created by Raivo Fismeisters, 2008, raivo@datateks.lv
*/

include_once('class.dbcollection.php');

class newnewscommentscollection  extends dbcollection
{
  function newnewscommentscollection($name, $id)
  {
    $this->DBcollection($name, $id);
    $this->type = "newnewscommentscollection";

    $this->properties = Array(
      "it0" => Array(
        "label"     => "Ziņa:",
        "type"      => "relation",
        "related_collection" => "newnewscollection",
        "relation_format" => "{%title%}",
        "column_name" => "news_id",
        "column_type" => "int",
        "column_index" => 1
      ),

      'it1' => Array(
        'label'         => 'Niks',
        'type'          => 'str',
        'column_name'   => 'nick'
      ),

      'it2' => Array(
        'label'         => 'Komentārs',
        'type'          => 'html',
        'column_name'   => 'comment_text',
        'cols'          => 50,
        'rows'          => 7,
        'search'        => true
      ),

      'it3' => Array(
        'label'         => 'Laiks',
        'type'          => 'date',
        'column_name'   => 'log_time',
        'column_type'   => 'int(10)',
        'time'          => 1
      ),

      'it4' => Array(
        'label'         => 'IP adrese',
        'type'          => 'str',
        'column_name'   => 'log_ip',
        'column_type'   => 'int(10)'
      ),

      'it5' => Array(
        'label'         => 'Last update',
        'type'          => 'hidden',
        'column_name'   => 'last_update',
        'column_type'   => 'int(10)',
        'column_index'  => 1
      ),

    );

    $this->columns = Array(
      "news_id"      => Array(
        "title"     => "Ziņa:"
      ),

      'nick' => Array(
        'title'         => 'Niks'
      ),

      'comment_text' => Array(
        'title'         => 'Komentārs'
      ),

      'log_time' => Array(
        'title'         => 'Laiks',
        'sortdesc'      => true
      ),

      'log_ip' => Array(
        'title'         => 'IP adrese'
      ),

    );

    $this->postInit();
  }

  function doAutoLastUpdate()
  {
    return true;
  }

  function IsEditableOutside()
  {
    $this->description = $this->type;
    $this->longname = $this->name;
    return true;
  }

  function processFormBeforeDisplay(&$form_data, $create)
  {
    $format = 'Y-m-d H:i:s';
    $form_data['log_time']['value'] = $form_data['log_time']['value'] ? date($format, $form_data['log_time']['value']) : '';
    $form_data['log_ip']['value'] = long2ip($form_data['log_ip']['value']);
  }

  function processItemBeforeSave($item_id, &$item, $create, &$jsmessage)
  {
    $item['log_time'] = strtotime($item['log_time']);
    $item['log_ip'] = ip2long($item['log_ip']);
    return parent::processItemBeforeSave($item_id, $item, $create, $jsmessage);
  }

  function GetEditData($page, &$count, &$recordposition, $order = '')
  {
    $data = parent::GetEditData($page, $count, $recordposition, $order);
    $format = 'Y-m-d H:i:s';

    foreach ($data as $key => $row)
    {
      if ($data[$key]['log_time'])
        $data[$key]['log_time'] = date($format, $row['log_time']);

      $data[$key]['log_ip'] = long2ip($row['log_ip']);
    }
    return $data;
  }

  function getNewsCommentCount($news_id)
  {
    return sqlQueryValue("SELECT COUNT(*) FROM `".$this->table."` WHERE news_id=".$news_id);
  }

  function getNewsComments($news_id, $offset, $count)
  {
    return sqlQueryData("SELECT * FROM `".$this->table."` WHERE news_id=".$news_id." ORDER BY log_time LIMIT $offset,$count");
  }

  function getItemsForIndex($time)
  {
    return sqlQueryData('SELECT * FROM `'.$this->table.'` WHERE last_update >= '.$time);
  }

  function deleteNewsComments($news_id)
  {
    sqlQuery("DELETE FROM `".$this->table."` WHERE news_id=".$news_id);
  }

  function getLastComments($count,$newscol)
  {
    return sqlQueryData("SELECT DISTINCT n.* FROM `".$this->table."` AS c JOIN $newscol->table AS n ON c.news_id=n.item_id  ORDER BY log_time DESC LIMIT $count");
  }

}

?>