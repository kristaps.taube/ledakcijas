<?php

class WishlistCollection extends pdocollection
{

    function __construct($name = '', $id = '')
    {

        parent::__construct($name,$id);

        //Properties array definition
        $this->properties_assoc = [

            "product_id" => Array(
                "label" => "Produkts:",
                "type" => "relation",
                "column_type" => "int(32)",
                "column_index" => 1,
                "related_collection" => "shopprodcollection",
                "relation_format" => "{%name_lv%}",
            ),

            "user_id" => Array(
                "label" => "Lietotājs:",
                "type" => "relation",
                "column_type" => "int(32)",
                "column_index" => 1,
                "related_collection" => "shopusercollection",
                "relation_format" => "{%epasts%}",
            ),

        ];

        //Collection display table definition
        $this->columns = [
            "user_id" => ["title" => "Lietotājs"],
            "price" => ["title" => "Cena"],
        ];

        $this->PostInit();

    }

    public function getProductsForUser($user_id)
    {
        return DB::GetTable("
            SELECT p.*
            FROM `".$this->table."` AS w
            JOIN `".$this->product_id->table."` AS p ON p.item_id = w.product_id
            WHERE
                user_id = :id AND
                p.disabled = 0
            ", [':id' => $user_id]);
    }

    public function getByProductAndUser($product_id, $user_id)
    {

        return DB::GetRow('SELECT * FROM `'.$this->table.'` WHERE user_id = :uid AND product_id = :pid', [':uid' => $user_id, ':pid' => $product_id]);

    }

    public function add($user_id, $product_id)
    {

        $check = DB::GetRow('SELECT * FROM `'.$this->table.'` WHERE user_id = :uid AND product_id = :pid', [':uid' => $user_id, ':pid' => $product_id]);

        $result = false;

        if(!$check){

            $prod = $this->product_id->getById($product_id);
            if($prod && !$prod['disabled']){

                $this->Insert([
                    'user_id' => $user_id,
                    'product_id' => $product_id
                ]);
                $result = true;

            }

        }

        return $result;

    }

    function IsEditableOutside()
    {
        $this->description = 'Collection: '.get_class();
        $this->longname = $this->name;
        return true;
    }

}