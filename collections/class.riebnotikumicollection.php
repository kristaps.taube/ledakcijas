<?
//Serveris.LV Constructor component
//Created by Kristaps Grinbergs, 2006, kristaps@datateks.lv

include_once('class.dbcollection.php');

class riebnotikumicollection extends dbcollection
{
  //Class initialization
  function riebnotikumicollection($name,$id)
  {
    $this->dbcollection($name,$id);
    $this->type = "riebnotikumicollection";

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "Datums, Laiks:",
        "type"      => "date",
        "dialog"    => "?module=dialogs&action=date&site_id=".$GLOBALS['site_id']."&time=1"
      ),

      "it1"    => Array(
        "label"     => "Vieta:",
        "size"      => "35",
        "type"      => "hidden"
      ),

      "it2"    => Array(
        "label"     => "Nosaukums:",
        "type"      => "str"
      ),

      "it3" => Array(
        "label"     => "Apraksts:",
        "type"      => "customtext",
        "rows"      => "7",
        "cols"      => "50",
        "dialog"    => "?module=wysiwyg&site_id=".$this->site_id,
        "dialogw"   => "720",
        "dialogh"   => "450",
        "dlg_res"   => true
      )

    );

    //Collection display table definition
    $this->columns = Array(

      "col0"        => Array(
        "width"     => "20%",
        "title"     => "Datums, laiks"
      ),

      "col1"        => Array(
        "width"     => "20%",
        "title"     => "Vieta"
      ),

      "col2"        => Array(
        "width"     => "20%",
        "title"     => "Nosaukums"
      ),

      "col3"        => Array(
        "width"     => "15%",
        "title"     => "Apraksts"
      )

    );
  }

  function getDBDataForMonth($date)
  {
    //echo $date;
    $d = split(' ',$date);
    $d = split('-',$d[0]);
    if(!$this->name)
    {
      $this->error = true;
      return false;
    }
    $data = sqlQueryData("SELECT * FROM `".$this->table."` WHERE EXTRACT(YEAR_MONTH FROM col0) = '".$d[0].$d[1]  ."' ORDER BY ind, col0");
    return $data;
  }

  function getDBDataForDaty ($date)
  {
    $d = split(' ',$date);
    $d = split('-',$d[0]);
    if(!$this->name)
    {
      $this->error = true;
      return false;
    }
    $data = sqlQueryData("SELECT * FROM `".$this->table."` WHERE EXTRACT(DAY FROM col0) = '".$d[2]  ."' AND EXTRACT(MONTH FROM col0) = '".$d[1]  ."' AND EXTRACT(YEAR FROM col0) = '".$d[0]  ."' ORDER BY ind, col0");
    return $data;
  }


  function IsEditableOutside()
  {
    $this->description = 'Notikumi';
    $this->longname = $this->name;
    return true;
  }

}

?>
