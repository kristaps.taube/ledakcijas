<?
include_once('class.dbcollection.php');

class EVeikalsProdArticlesCol extends dbcollection{
  
  function __construct($name,$id)
  {
    $this->dbCollection($name,$id);
    $this->type = get_class();

    //Properties array definition
    $this->properties_assoc = Array(
			"prod_id"    => Array(
        "label"     => "Prod ID:",
        "type"      => "str",
				'column_type' => 'int(11)'				
      ),		
     "price_id"    => Array(
        "label"     => "Prod ID:",
        "type"      => "str",
				'column_type' => 'int(11)',
				'default_value' => 0	
      ),
			"count"    => Array(
        "label"     => "Produktu skaits:",
        "type"      => "str",
        'column_type' => 'int(11)'				
      ),
			"custom_price"    => Array(
        "label"     => "Mana cena:",
        "type"      => "str",
        'column_type' => 'decimal(8,2)',
				'default_value' => '0.00'	
      ),
			
      "piedavajums"					=> Array(
				"label"     => "Piedāvājums:",
				"type"      => "relation",
				"related_collection" => "EVeikalsPiedavajumiCol",
				"relation_format" => "{%name%}",
			),
			
    );

		
    //Collection display table definition
    $this->columns = Array(

      "count"        => Array("title"     => "Produktu skaits"),
			"custom_price"        => Array("title"     => "Mana cena"),
			"piedavajums"        => Array("title"     => "Piedāvājums"),
			
    );

     $this->PostInit();
  }

  function IsEditableOutside()
  {
    $this->description = 'Collection: '.get_class();
    $this->longname = $this->name;
    return true;
  }

}