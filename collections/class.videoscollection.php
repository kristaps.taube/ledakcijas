<?

include_once('class.dbcollection.php');

class videoscollection extends dbcollection
{
  function videoscollection($name, $id)
  {
    $this->DBcollection($name, $id);
    $this->type = "videoscollection";

    $this->properties = Array(

      'it0' => Array(
        'label'         => 'Title',
        'type'          => 'str',
        'column_name'   => 'title',
        'multilang' => true
      ),

      'it1' => Array(
        'label'         => 'Flash video file (flv; mp4)',
        'type'          => 'file',
        'column_name'   => 'video',
        'upload_dir'    => 'files/videos/'
      ),

      'it2' => Array(
        'label'         => 'Image',
        'type'          => 'file',
        'column_name'   => 'image',
        'upload_dir'    => 'files/videos/thumbs/'
      ),

      "it3"    => Array(
        "label"     => "Video HTML embed code:",
        "type"      => "customtext",
        "rows"      => "6",
        "cols"      => "30",
        "dialog"    => "?module=wysiwyg&canpreview='+((window.parent.dialogArguments)?1:0)+'&site_id=".$GLOBALS['site_id']."&page_id=".$this->page_id,
        "dialogw"   => "720",
        "dialogh"   => "450",
        "dlg_res"   => true,
        "column_name" => "embed_code",
      )

    );

    $this->columns = Array(
      'title' => array('title' => 'Title', 'multilang' => true)
/*
      "url" => array("title" => "Link" , "format" => "<a href='{%url%}' target='_blank'>{%url%}</a>"  )
*/
    );

    $this->postInit();
  }

  function IsEditableOutside()
  {
    $this->description = $this->type;
    $this->longname = $this->name;
    return true;
  }

}

?>