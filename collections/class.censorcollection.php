<?
include_once('class.dbcollection.php');

class censorcollection extends dbcollection
{
  //Class initialization
  function censorcollection($name,$id)
  {
    $this->DBCollection($name,$id);
    $this->type = "censorcollection";

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "Vards:",
        "size"      => "35",
        "type"      => "string",
        "column_name" => "vards"
      ),
    );

    //Collection display table definition
    $this->columns = Array(

      "vards"        => Array(
        "width"     => "25%",
        "title"     => "vards"
      ),
    );
    $this->postInit();
  }

  function IsEditableOutside()
  {
    $this->description = 'Censor Words';
    $this->longname = $this->name;
    return true;
  }

  function IsCensoredText($s)
  {
    $words = $this->GetDBData();
    foreach($words as $word)
    {
      $word = str_replace('*', '.*?', $word['vards']);
      
      //reg exp: mekl� v�rdu word tekst� t�, lai pirms �� v�rda ir
      //  vai nu simbols, kas nav burts, vai teksta s�kums un, lai p�c
      //  �� v�rda ir vai nu simbols, kas nav burts, vai teksta beigas
      //flagi: case insensitive un utf
      if(preg_match('/([^\w]|^)('.$word.')([^\w]|$)/iu', $s))
      {
        return true;
      }
    }
    return false;
  }

}

?>
