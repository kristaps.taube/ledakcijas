<?
/*
 * Datateks.lv Constructor component
 * Created by Raivo Fismeisters, 2009, raivo@datateks.lv
*/

include_once('class.dbcollection.php');

class articlecollection  extends dbcollection
{
  function articlecollection($name, $id)
  {
    $this->DBcollection($name, $id);
    $this->type = "articlecollection";

    $this->properties_assoc = Array(
      'page_id' => Array(
        'label'         => 'Lapa',
        'type'          => 'hidden',
        'column_type'   => 'int(10)'
      ),

      'title' => Array(
        'label'         => 'Virsraksts',
        'type'          => 'str',
      ),

      'urlpart' => Array(
        'label'         => 'URL part',
        'type'          => 'hidden',
        'column_index'  => 1,
        'column_type'   => 'varchar(255)'
      ),

      'short_text' => Array(
        'label'         => 'Īsais teksts',
        'type'          => 'wysiwyg',
        'cols'          => 50,
        'rows'          => 7,
        'dialog'        => '?module=wysiwyg&site_id='.$this->site_id,
        'dialogw'       => 800,
        'dialogh'       => 600,
        'dlg_res'       => true
      ),

      'full_text' => Array(
        'label'         => 'Pilnais teksts',
        'type'          => 'wysiwyg',
        'cols'          => 50,
        'rows'          => 7,
        'dialog'        => '?module=wysiwyg&site_id='.$this->site_id,
        'dialogw'       => 800,
        'dialogh'       => 600,
        'dlg_res'       => true
      ),

      'image' => Array(
        'label'         => 'Attēls',
        'type'          => 'file',
        'upload_dir'    => 'images/articles/'
      ),

      'img_alt' => Array(
        'label'         => 'Attēla alt teksts',
        'type'          => 'str',
      ),
      'img_width' => Array(
        'label'         => 'Attēla platums',
        'type'          => 'str',
        'column_type'   => 'int(10)',
        'width'         => 40
      ),
      'img_height' => Array(
        'label'         => 'Attēla augstums',
        'type'          => 'str',
        'column_type'   => 'int(10)',
        'width'         => 40
      ),
      'img_resizemode' => Array(
        'label'     => 'Attēla resize mode:',
        'type'      => 'list',
        'lookup'    => Array("1:By width", "2:By height", "3:By width if too big", "4:By height if too big", "5:By width and height", "6:By width and height if too big", "7:Crop by width and height"),
        'value'     => 6,
        'column_type' => 'int(10)'
      ),
      'img2_width' => Array(
        'label'         => 'Attēla platums 2',
        'type'          => 'str',
        'column_type'   => 'int(10)',
        'width'         => 40
      ),
      'img2_height' => Array(
        'label'         => 'Attēla augstums 2',
        'type'          => 'str',
        'column_type'   => 'int(10)',
        'width'         => 40
      ),
      'img2_resizemode' => Array(
        'label'     => 'Attēla resize mode 2:',
        'type'      => 'list',
        'lookup'    => Array("1:By width", "2:By height", "3:By width if too big", "4:By height if too big", "5:By width and height", "6:By width and height if too big", "7:Crop by width and height"),
        'value'     => 6,
        'column_type' => 'int(10)'
      ),


      'adate' => Array(
        'label'         => 'Datums',
        'type'          => 'date',
        'column_type'   => 'int',
        'time'          => 1,
        //'value'         => time()
      ),

      'disabled' => Array(
        'label'         => 'Slēpts',
        'type'          => 'hidden',
        'column_type'   => 'tinyint(1)',
        'column_index'  => 1,
        'value'         => 0
      ),

      'archived' => Array(
        'label'         => 'Arhivēts',
        'type'          => 'check',
        'column_type'   => 'tinyint(1)',
        'column_index'  => 1,
      ),
      
      'last_update' => Array(
        'label'         => 'Last update',
        'type'          => 'hidden',
        'column_type'   => 'int(10)',
        'column_index'  => 1
      ),

      'url' => Array(
        'label'         => 'Links (nav obligāts)',
        'type'      => 'dialog',
        'dialog'    => '?module=dialogs&action=links&site_id='.$this->site_id,
      ),
      
      'add_to_cal' => Array(
        'label'         => 'Pievienot kalendāram',
        'type'          => 'check',
        'column_type'   => 'tinyint(1)',
        'column_index'  => 1,
        'validate'      => 'custom',
        'validate_code' => 'if ({%form_name%}.add_to_cal.checked && {%form_name%}.eventdate.value.length == 0) { alert("Kalendāra rakstam ir obligāti jānorāda norises laiks!"); {%form_name%}.eventdate.focus(); return false; }'
      ),

      'eventdate' => Array(
        'label'         => 'Notikuma norises laiks',
        'type'          => 'date',
        'column_type'   => 'int',
        'time'          => 1,
      ),

    );

    $this->columns = Array(
      'title' => Array(
        'title'         => 'Virsraksts'
      ),

      'short_text' => Array(
        'title'         => 'Īsais apraksts'
      ),

      'add_to_cal' => array('title' => 'Ir kalendārā?', 'sortdesc' => 1),
      'eventdate' => array('title' => 'Notikuma datums', 'sortdesc' => 1, 'format' => '<span style="white-space:nowrap">{%_self%}</span>'),      
    );

    $this->postInit();
  }

  function getCalendarArticles($start, $end, $month)
  {
    return sqlQueryData('SELECT * FROM `'.$this->table.'` WHERE add_to_cal=1 AND eventdate >= '.$start.' AND eventdate < '.$end.' AND MONTH(FROM_UNIXTIME(eventdate))='.$month);
  }
  
  function getArticles($page_ids, $offset=0, $limit=0, $order='ind', $lastindextime=0, $date_begin=0, $date_end=0, $date_month=0, $archived=null)
  {
    if (is_array($page_ids) && !count($page_ids) && $archived === null)
      return array(array(), 0);
    
    $sql = '';
    
    $where = array();
      
    if ($lastindextime > 0)
      $where[] = 'last_update >= '.$lastindextime; 

    if ($date_begin && $date_end)
    {
             
      $archived = null;
      $where[] = '((page_id='.$page_ids[0].' AND add_to_cal=0) OR (add_to_cal=1 AND eventdate >= '.$date_begin.' AND eventdate < '.$date_end.'))';
      if ($date_month) $where[] = 'MONTH(FROM_UNIXTIME(eventdate))='.$date_month;
      
      $page_ids = null;
    }
      
    if ($page_ids)
    {
      if (count($page_ids) == 1)
        $where[] = 'page_id='.(int)$page_ids[0];
      else
      {
        $ids = implode(',', $page_ids);
        $where[] = 'page_id IN ('.$ids.')';
      }
    
    }

    if ($archived !== null)
      $where[] = 'archived='.(int)(boolean)$archived; 
    
    if ($where)    
      $sql .= ' WHERE '.implode(' AND ', $where);

    if ($order) $sql .= ' ORDER BY '.$order;
    if ($limit) $sql .= ' LIMIT '.$offset.','.$limit;

    $data = sqlQueryData('SELECT * FROM `'.$this->table.'`'.$sql);
    $count = sqlQueryValue('SELECT FOUND_ROWS()');
    return array($data, $count);
  }

  function getItemByURL($url)
  {
    return sqlQueryRow('SELECT * FROM `'.$this->table.'` WHERE urlpart="'.db_escape($url).'"');
  }
  
  function doAutoLastUpdate()
  {
    return true;
  }

  function isAddToBottomMode()
  {
    return false;
  }

  function IsEditableOutside()
  {
    $this->description = 'Articles';
    $this->longname = $this->name;
    return true;
  }

  function processDBproperties(&$form)
  {
    $hide = array();

    $keys = Array(
      'full_text', 'image', 'adate', 'url', 'eventdate'
    );    
    foreach ($keys as $k)
    {
      if (!$this->getProperty('show_'.$k))
        $hide[] = $k;

    }
    
    if (!$this->getProperty('show_image'))
    {
      $hide = array_merge($hide, array('image', 'img_alt', 'img_width', 'img_height', 'img_resizemode') );
    }
    
    foreach ($hide as $k)
    {
      $form[$k]['type'] = 'hidden';
    }    

    //array_unshift($form, array('type' => 'title', 'label' => 'Saturs'));
    
    $advfield = '';
    if ($form['adate']['type'] != 'hidden') $advfield = 'adate';
    else if ($form['url']['type'] != 'hidden') $advfield = 'url';
    else $advfield = 'add_to_cal';
      
    $a = array();
    foreach ($form as $key => $fld)
    {
      if ($key == 'image' && $fld['type'] != 'hidden') 
        $a[$key.'_label'] = array('type' => 'title', 'label' => 'Attēls'); 
      else if ($advfield && $key == $advfield)
        $a[$key.'_label'] = array('type' => 'title', 'label' => 'Papildus'); 
      
      $a[$key] = $fld;
    }
    $form = $a;       
  }

  function processFormBeforeDisplay(&$form, $create)
  {
    $format = 'Y-m-d H:i:s';
    if (!$form['adate']['dontsetcurrent'])
      $form['adate']['value'] = $form['adate']['value'] ? date($format, $form['adate']['value']) : '';

    $form['eventdate']['value'] = $form['eventdate']['value'] ? date($format, $form['eventdate']['value']) : '';

    if (!$form['page_id']['fromvisualedit'])
    {
      $form['page_id']['type'] = 'list';
      
      $pages = sqlQueryData("SELECT page_id,name,parent,title FROM " . $this->site_id . "_pages ORDER BY ind");
      ListNodes($pages, $combopages, 0, 0);
      array_unshift($combopages, '0: (none)');
      
      $form['page_id']['lookup'] = $combopages;      
    }
  }

  function processItemBeforeSave($item_id, &$item, $create, &$jsmessage)
  {
    $urlpart = transliterateURL($item['title']);
    $u = '';
    while ($this->getItemByURL($urlpart.(empty($u) ? '' : '-'.$u)) )
    {
      $u = intval($u) + 1;
    }

    $item['urlpart'] = $urlpart.(empty($u) ? '' : '-'.$u);
    $item['adate'] = strtotime($item['adate']);
    $item['eventdate'] = strtotime($item['eventdate']);
    $item['add_to_cal'] = (int)(boolean)$item['add_to_cal'];
    $item['archived'] = (int)(boolean)$item['archived'];
    return parent::processItemBeforeSave($item_id, $item, $create, $jsmessage);
  }

  function getItemsForIndex($time)
  {
    return sqlQueryData('SELECT * FROM `'.$this->table.'` WHERE last_update >= '.$time);
  }
  
  function GetEditData($page, &$count, &$recordposition, $order = '')
  {
    $data = parent::GetEditData($page, $count, $recordposition, $order);
    foreach ($data as $key => $row)
    {
      $data[$key]['add_to_cal'] = $row['add_to_cal'] ? 'JĀ' : 'nē';    
      $data[$key]['eventdate'] = $row['eventdate'] ? date('d.m.Y, H:i', $row['eventdate']) : '';
    }
    return $data;
  }  
}

?>