<?
//Serveris.LV Constructor component
//Created by Aleksandrs Selickis, 2004, alex@datateks.lv

include_once('class.dbcollection.php');

class picturescollectionex extends dbcollection
{
  //Class initialization
  function picturescollectionex($name,$id)
  {
    $this->dbcollection($name,$id);
    $this->type = "picturescollectionex";

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "Gallery ID",
        "type"      => "relation",
        "related_collection" => "galleriescollectionex",
        "relation_format" => "{%title%}",
        "column_name" => "gal_id"
      ),

      "it1"    => Array(
        "label"     => "Thumbnail path",
        "size"      => "35",
        "type"      => "str",
        "column_name" => "thumb",
      ),

      "it2"    => Array(
        "label"     => "Image path",
        "size"      => "35",
        "type"      => "str",
        "column_name" => "image"
      ),

      "it3"    => Array(
        "label"     => "Photographer",
        "size"      => "35",
        "type"      => "str",
        "column_name" => "photographer"
      ),

      "it4"    => Array(
        "label"     => "Phone",
        "size"      => "35",
        "type"      => "str",
        "column_name" => "phone"
      ),

      "it5" => Array(
        "label"     => "Text:",
        "type"      => "customtext",
        "rows"      => "7",
        "cols"      => "50",
        "dialog"    => "?module=wysiwyg&site_id=".$this->site_id,
        "dialogw"   => "720",
        "dialogh"   => "450",
        "dlg_res"   => true,
        "column_name" => "text"
      ),

      "it6"    => Array(
        "label"     => "Orig. Image path",
        "size"      => "35",
        "type"      => "str",
        "column_name" => "origimage"
      ),

      "it7"    => Array(
        "label"     => "Disabled",
        "type"      => "check",
        "column_name" => "disabled"
      )

    );

    //Collection display table definition
    $this->columns = Array(

      "gal_id"        => Array(
        "width"     => "20%",
        "title"     => "Gallery ID"
      ),

      "thumb"        => Array(
        "width"     => "20%",
        "title"     => "Thumbnail path",
        "format"    => "[if {%thumb%}]<img src=\"{%thumb%}\" />[/if]",
      ),

      "image"        => Array(
        "width"     => "20%",
        "title"     => "Image path",
      ),

      "photographer"        => Array(
        "width"     => "15%",
        "title"     => "Photographer"
      ),

      "phone"        => Array(
        "width"     => "10%",
        "title"     => "Phone"
      ),

      "text"        => Array(
        "width"     => "30%",
        "title"     => "Text"
      ),

      "origimage"        => Array(
        "width"     => "20%",
        "title"     => "Orig image path"
      )

    );

    $this->postInit();




  }

  function getFirstPictureForGallery ($gallery_id)
  {
    if(!$this->name)
    {
      $this->error = true;
      return false;
    }
    $data = sqlQueryRow("SELECT * FROM `".$this->table."` WHERE gal_id = ".$gallery_id." AND `disabled` = '' ORDER BY ind LIMIT 1");
    return $data;
  }

  function getPicturesForGallery ($gallery_id, $skipdisabled = false)
  {
    if(!$this->name)
    {
      $this->error = true;
      return false;
    }
    if($skipdisabled)
      $where = ' AND `disabled` = \'\'';
    else
      $where = '';
    $data = sqlQueryData("SELECT * FROM `".$this->table."` WHERE gal_id = ".$gallery_id."".$where." ORDER BY ind");
    return $data;
  }

  function countPicturesForGallery ($gallery_id, $skipdisabled = false)
  {
    if(!$this->name)
    {
      $this->error = true;
      return false;
    }
    if($skipdisabled)
      $where = ' AND `disabled` = \'\'';
    else
      $where = '';
    $data = sqlQueryValue("SELECT COUNT(*) FROM `".$this->table."` WHERE gal_id = ".$gallery_id."".$where);
    return $data;
  }

  function IsEditableOutside()
  {
    $this->description = 'Galerijas bildes';
    $this->longname = $this->name;
    return false;
  }

  function getPreviousPicture($gallery_id, $ind)
  {
    return sqlQueryRow("SELECT * FROM `".$this->table."` WHERE gal_id = ".$gallery_id." AND ind < $ind AND `disabled` = '' ORDER BY ind DESC LIMIT 1");
  }

  function getNextPicture($gallery_id, $ind)
  {
    return sqlQueryRow("SELECT * FROM `".$this->table."` WHERE gal_id = ".$gallery_id." AND ind > $ind AND `disabled` = '' ORDER BY ind LIMIT 1");
  }

  function movePicUp($ind)
  {
    $item = $this->GetItem($ind);
    $gallery_id = $item[0];
    $otherrow = sqlQueryRow("SELECT * FROM `".$this->site_id."_coltable_".$this->name."` WHERE gal_id = ".$gallery_id." AND ind < $ind ORDER BY ind DESC LIMIT 1");
    if(($otherrow)and($otherrow['ind']))
    {
      $this->SwapItems($ind, $otherrow['ind']);
    }
  }

  function movePicDown($ind)
  {
    $item = $this->GetItem($ind);
    $gallery_id = $item[0];
    $otherrow = sqlQueryRow("SELECT * FROM `".$this->site_id."_coltable_".$this->name."` WHERE gal_id = ".$gallery_id." AND ind > $ind ORDER BY ind LIMIT 1");
    if(($otherrow)and($otherrow['ind']))
    {
      $this->SwapItems($ind, $otherrow['ind']);
    }
  }

  function countTimesPictureUsed($pic)
  {
    return sqlQueryValue('SELECT COUNT(*) FROM `'.$this->table.'` WHERE thumb="'.$pic.'" OR image="'.$pic.'" OR origimage="'.$pic.'"');
  }



}

?>
