<?php
require_once ('class.pdocollection.php');

class EVeikalsPaymentMethods extends pdocollection
{

	public function __construct($name, $id)
	{
		parent::__construct($name, $id);
		$this->type = get_class();
		//Properties array definition
		$this->properties_assoc = Array(
			"key" => Array(
				"label" => "Atslēga:",
				"type" => "str"
			),
			"name" => Array(
				"label" => "Nosaukums:",
				"type" => "str", "multilang" => true
			),
			"icon" => Array(
				"label" => "Ikona",
				"type" => "dialog",
				"dialog" => "?module=dialogs&action=filex&site_id=" . $GLOBALS['site_id'] . "&view=thumbs", "dialogw" => "600",
			),
			"output_type" => Array(
				"label" => "Attēlot:",
				"type" => "list",
				"lookupassoc" => array(
					'text_only' => 'Tikai nosaukumu',
					'icon_only' => 'Tikai ikonu',
					'both' => 'Nosaukumu un ikonu',
				),
				"column_type" => "varchar(100)",
				"default_value" => "both"
			),
			"visible" => Array(
				"label" => "Redzama:",
				"type" => "check",
				"column_type" => "tinyint(1)",
				"default_value" => 1
			),
		);
		//Collection display table definition
		$this->columns = Array(
			"name_lv" => Array(
				"width" => "60%",
				"title" => "Nosakums:"
			),
			"icon" => Array("title" => "Ikona:"),
			"visible" => Array("title" => "Redzama:"),

		);
		$this->PostInit();
	}

	public function getVisible()
	{
		return $this->GetDBData(array("where" => "visible = 1"));
	}

	public function processItemBeforeSave($item_id, & $item, $create, & $jsmessage)
	{
		$item['visible'] = $item['visible'] ? 1 : 0;
		return true;
	}

	public function getByKey($key)
	{
		return $this->getRow(array("where" => "`key` = :key", "params" => array(":key" => $key)));
	}

	public function GetEditData($page, & $count, & $recordposition, $order = '')
	{
		$data = parent::GetEditData($page, $count, $recordposition, $order);
		foreach ($data as $key => $row)
		{
			$data[$key]['visible'] = $data[$key]['visible'] ? "Jā" : "Nē";
			$data[$key]['icon'] = $data[$key]['icon'] ? "<img src='".$data[$key]['icon']."' />" : "Nav norādīts";
		}
		return $data;
	}

	public function IsEditableOutside()
	{
		$this->description = 'Collection: ' . get_class();
		$this->longname = $this->name;
		return true;
	}

	public function processFormBeforeDisplay(& $form_data, $create, $item)
	{
	}

}