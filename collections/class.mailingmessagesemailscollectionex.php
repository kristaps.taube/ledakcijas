<?
//Serveris.LV Constructor component
//Created by Aleksandrs Selickis, 2004, alex@datateks.lv

include_once('class.dbcollection.php');

class mailingmessagesemailscollectionex extends dbcollection
{
  //Class initialization
  function mailingmessagesemailscollectionex($name,$id)
  {
    $this->dbCollection($name,$id);
    $this->type = "mailingmessagesemailscollectionex";

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "E-mail",
        "type"      => "str",
        "column_name" => "email",
        "column_type" => "VARCHAR(250)",
      ),
      
      "it1"    => Array(
        "label"     => "Message id",
        "type"      => "str",
        "column_name" => "message_id",
        "column_type" => "int(11)",
      ),
    );

    //Collection display table definition
    $this->columns = Array(


      "message_id"  => Array(
        "title"     => "Email"
      ),

      "message_id"   => Array(
        "title"     => "Message_id"
      ),

    );

    $this->PostInit();
  }

}

?>