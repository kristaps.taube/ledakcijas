<?
//Serveris.LV Constructor component
//Created by Aleksandrs Selickis, 2004, alex@datateks.lv

include_once('class.collection.php');

class randombannerscollection extends collection
{
  //Class initialization
  function randombannerscollection($name,$id)
  {
    $this->Collection($name,$id);
    $this->type = "randombannerscollection";

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "Image path",
        "type"      => "dialog",
        "dialog"    => "?module=dialogs&action=filex&site_id=".$GLOBALS['site_id']."&view=thumbs",
        "dialogw"   => "600"
      ),

	  "it1"    => Array(
        "label"     => "Link:",
        "type"      => "dialog",
        "dialog"    => "?module=dialogs&action=links&site_id=".$GLOBALS['site_id'],
      ),

      "it2"    => Array(
        "label"     => "Description",
        "size"      => "35",
        "type"      => "str"
      )

    );

    //Collection display table definition
    $this->columns = Array(

      "col0"        => Array(
        "width"     => "40%",
        "title"     => "Image path"
      ),

      "col1"        => Array(
        "width"     => "30%",
        "title"     => "Link"
      ),

      "col2"        => Array(
        "width"     => "30%",
        "title"     => "Description"
      )

    );

  }

}

?>