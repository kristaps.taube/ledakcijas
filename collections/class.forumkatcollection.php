<?
//Serveris.LV Constructor collection module
//Created by Aleksandrs Selickis, 2004

include_once('class.collection.php');

class ForumKatCollection extends collection
{
  //Class initialization
  function ForumKatCollection($name,$id)
  {
    $this->Collection($name,$id);
    $this->type = "forumkatcollection";

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "Category name",
        "size"      => "35",
        "type"      => "str"
      ),

      "it1"    => Array(
        "label"     => "Description",
        "type"      => "html",
        "rows"      => "6",
        "cols"      => "30",
        "search"    => true
      ),

      "it2"    => Array(
        "label"     => "Date",
        "size"      => "35",
        "type"      => "str"
      ),

      "it3"    => Array(
        "label"     => "Posted by",
        "size"      => "35",
        "type"      => "str"
      ),

    );

    //Collection display table definition
    $this->columns = Array(

      "col0"        => Array(
        "width"     => "20%",
        "title"     => "Category name"
      ),

      "col1"        => Array(
        "width"     => "40%",
        "title"     => "Description"
      ),

      "col2"        => Array(
        "width"     => "20%",
        "title"     => "Date"
      ),

      "col3"        => Array(
        "width"     => "20%",
        "title"     => "Posted by"
      ),

    );




  }


  //Override GetDBData for postprocessing the retrieved data
  function GetDBData($changeDate = true)
  {
    if (!function_exists("cmpind"))
      {
        function cmpind($a, $b) 
        {
           if ($a['ind'] == $b['ind']) {
               return 0;
           }
           return ($a['ind'] > $b['ind']) ? -1 : 1;
        }
    }
    
    //Call original GetDBData()
    $r = parent::GetDBData();
    //order by ind desc
    usort($r, "cmpind");
    return $r;
  }


}

?>
