<?php
//Serveris.LV Constructor component
//Created by Aivars Irmejs, 2006, aivars@datateks.lv

require_once('class.pdocollection.php');

class shopusercollection extends pdocollection{

    //Class initialization
    function __construct($name = '', $id = ''){

        parent::__construct($name, $id);
        
        $this->type = get_class();

        //Properties array definition
        $this->properties_assoc = Array(

        "savedfields"    => Array(
            "label"     => "Saved fields",
            "size"      => "35",
            "type"      => "hidden",
        ),

        "discount"    => Array(
            "label"     => "Discount (%)",
            "size"      => "35",
            "type"      => "str",
            "value"    => "0"
        ),

        "email"    => Array(
            "label"     => "Email",
            "size"      => "35",
            "type"      => "str",
        ),

        "password"    => Array(
            "label"     => "Parole",
            "size"      => "45",
            "type"      => "mask",
        ),

        "name"    => Array(
            "label"     => "Name",
            "size"      => "35",
            "type"      => "str",
        ),

        "lang"    => Array(
            "label"     => "Lang",
            "size"      => "35",
            "type"      => "str",
        ),

        "surname"    => Array(
            "label"     => "Surname",
            "size"      => "35",
            "type"      => "str",
        ),

        "active"    => Array(
            "label"     => "Active",
            "size"      => "35",
            "type"      => "check",
            "column_type" => "int(10)"
        ),

        "passkey"    => Array(
            "label"     => "Pass key",
            "type"      => "hidden",
        ),

        "recover_hash"    => Array(
            "label"     => "Recover hash",
            "type"      => "hidden",
        ),

        "activation_hash"    => Array(
            "label"     => "Activation hash",
            "type"      => "hidden",
        ),

        "fb_id"    => Array(
            "label"     => "Facebook id",
            "type"      => "hidden",
        ),

        "fb_name"    => Array(
            "label"     => "Facebook name",
            "type"      => "hidden",
        ),

        "google_id"    => Array(
            "label"     => "Google id",
            "type"      => "hidden",
        ),

        "google_name"    => Array(
            "label"     => "Google name",
            "type"      => "hidden",
        ),

        "draugiem_id"    => Array(
            "label"     => "Draugiem id",
            "type"      => "hidden",
        ),

        "draugiem_name"    => Array(
            "label"     => "Draugiem name",
            "type"      => "hidden",
        ),

        "logoimage"    => Array(
            "label"     => "Logo img",
            "type"      => "hidden",
        ),

        "card_data"    => Array(
            "label"     => "Swedbank kartes dati",
            "type"      => "hidden",
        ),

    );

    //Collection display table definition
        $this->columns = Array(

        "email"        => Array(
            "title"     => "E-pasts",
        ),

        "name"        => Array(
            "title"     => "Vārds",
        ),

        "discount"        => Array(
            "title"     => "Atlaide"
        ),

        "savedfields" => Array(
            "title"     => "Lietotāja informācija",
            "format"    => '[adlg viewsavedfields]Apskatīt[/adlg]',
        ),

        "catdiscounts" => Array(
            "title"     => "Atlaides kategorijās",
            "subrelated_collection" => "shopusercatatlaidescollection",
            "subrelated_field" => "atl_lietotajs",
            "format"    => '[adlg changecatdiscounts]Mainīt ({%_self%})[/adlg]',
        ),

        "active"        => Array(
            "title"     => "Aktīvs"
        ),

    );

    $this->postInit();
    }

  function getByRecoverHash($hash){
    return sqlQueryRow("SELECT * FROM `".$this->table."` WHERE recover_hash = :hash", array(":hash" => $hash));
  }


  function IsEditableOutside(){
    $this->description = $this->type;
    $this->longname = $this->name;
    return true;
  }

  function viewsavedfields($ids){

    $item = $this->getItemByIdAssoc($ids[0], true);
    $savedfields = unserialize($item['savedfields']);

		if($savedfields){
    	echo '<table border="0">';
	    foreach($savedfields as $key => $field){
	      echo '<tr>';
	      echo '<td><b>' . $key . '</b></td>';
	      echo '<td>' . $field . '</td>';
	      echo '</tr>';
	    }
	    echo '</table>';
		}else{
    	echo "Šim lietotājam nav saglabāti nekādi dati";
		}

  }

	function GetEditData($page, &$count, &$recordposition, $order = ''){

		$data = parent::GetEditData($page, $count, $recordposition, $order);

    foreach($data as &$entry){

      $entry['active'] = $entry['active']? "Jā" : "Nē";

    }

    return $data;

  }

  function changecatdiscounts($ids){
    $user = $this->getItemByIdAssoc($ids[0], true);
    $discountcol = $this->InitializeRelatedCollection('catdiscounts', 'shopusercatatlaidescollection');
    if($discountcol){

      $discounts = $discountcol->getDiscountsForUser($user['item_id']);
      $catdiscounts = Array();
      foreach($discounts as $discount){
        $catdiscounts[$discount['atl_cat']] = $discount['atl_atlaide'];
      }

      $catcol = $discountcol->InitializeRelatedCollection('atl_cat', 'shopcatcollection');

      if($catcol){
        $cats = $catcol->GetDBData();
        echo '<form action="?module=collections&action=dlgcolmethodexec&method_name=changecatdiscountssave&site_id='. $this->site_id . '&page_id=' . $this->page_id . '&category_id=' . $this->collection_id . '&coltype='.$this->type.'&session_id='.session_id().'&session_name='.session_name().'&id='.$ids[0].'" method="POST">';
        echo '<table border="0" cellspacing="0" cellpadding="4">';
        $bg = false;
        foreach($cats as $cat)
        {
          $bg = !$bg;
          echo '<tr'.($bg ? ' style="background-color: #DDDDDD;"' : '').'>
            <td>';
          echo str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', $cat['lvl']);
          echo $cat['title_lv'] . '</td><td><input name="catdiscount['.$cat['item_id'].']" type="text" size="4" value="'.intval($catdiscounts[$cat['item_id']]).'"></td>
          </tr>';
        }
        echo '<tr><td colspan="2" align="center">';
        echo '&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" name="update_values" value="&nbsp;&nbsp;&nbsp;&nbsp;OK&nbsp;&nbsp;&nbsp;&nbsp;">';
        echo '</td></tr>';
        echo '</table>';
        echo '</form>';
      }
    }
  }

  function ChangeCatDiscountsSave($ids)
  {
    $user_id = intval($ids[0]);

    if(isset($_POST['catdiscount']))
    {
      $discountcol = $this->InitializeRelatedCollection('catdiscounts', 'shopusercatatlaidescollection');
      if($discountcol)
      {
        $discountcol->deleteDiscountsForUser($user_id);

        foreach($_POST['catdiscount'] as $cat_id => $value)
        {
          if($value)
          {
            $item = Array('atl_atlaide' => $value, 'atl_lietotajs' => $user_id, 'atl_cat' => $cat_id);
            $discountcol->AddItemAssoc($item);
          }
        }
      }
    }
  }

  function getUserId($user)
  {
    return sqlQueryValue('SELECT item_id FROM ' . $this->table . ' WHERE username = "' . $user . '"');
  }

  function getUserPassId($user, $pass)
  {
    return sqlQueryValue('SELECT item_id FROM ' . $this->table . ' WHERE username = "' . $user . '" AND password = "' . $this->encodePassword($pass) . '"');
  }

  function getUserByFBId($id){
    return sqlQueryRow('SELECT * FROM ' . $this->table . ' WHERE fb_id = :id LIMIT 1', array(":id" => $id));
  }

  function getUserByDraugiemId($id){
    return sqlQueryRow('SELECT * FROM ' . $this->table . ' WHERE draugiem_id = :id LIMIT 1', array(":id" => $id));
  }

  function getUserByGoogleId($id){
    return sqlQueryRow('SELECT * FROM ' . $this->table . ' WHERE google_id = :id LIMIT 1', array(":id" => $id));
  }

  function getUserByEmail($email){
  	return $this->GetRow(array("where" => "email = :email", "params" => array(":email" => $email)));
  }

  function GetListData()
  {
    return $this->GetDBData(Array('order' => 'username'));
  }

  function encodePassword($password){

		$salt = generate_random_string(5);
  	$password = $salt.sha1($salt.$password);

    return $password;

  }

  function setupPasswordChange($user_id, $password, $page_id)
  {
    $passkey = generate_random_string(32, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789');

    $this->changeItemByIdAssoc($user_id, Array(
      'tmppass' => $this->encodePassword($password),
      'passkey' => $passkey
    ));

    return 'http://'.$_SERVER['SERVER_NAME'].'/'.PagePathById($page_id, $this->site_id).'?chpasskey='.$user_id.'_'.$passkey;
  }

  function processPasswordChange()
  {
    if (!isset($_GET['chpasskey'])) return null;

    $a = explode('_', $_GET['chpasskey']);
    $id = intval($a[0]);
    if ($id)
    {
      $user = $this->getItemByIdAssoc($id);
      if ($user['passkey'] == $a[1] && !empty($user['tmppass']))
      {
        $this->changeItemByIdAssoc($id, Array(
          'password' => $user['tmppass'],
          'passkey' => '',
        ));

        return true;
      }

    }

    return false;
  }


  function processFormBeforeDisplay(&$form_data, $create, $item)
  {
    $form_data['password']['value'] = '';
  }

  function processItemBeforeSave($item_id, &$item, $create, &$jsmessage){
    if (strlen($item['password']))
      $item['password'] = $this->encodePassword($item['password']);
    else
      unset($item['password']);


		if(!$item_id && !$item['password']){
			$jsmessage = "Lūdzu, ievadiet lietotāja paroli";
			return false;
		}
           
		$item['active'] = $item['active']? 1 : 0;
		$item['savedfields'] = addslashes($item['savedfields']);
    $jsmessage = '';
    return true;

  }


}
