<?php

class EVeikalsShopCollection extends pdocollection{

  function __construct($name = '', $id = ''){

    parent::__construct($name, $id);

    $this->properties_assoc = Array(

      "title"    => Array(
        "label"     => "Nosaukums:",
        "type"      => "text",
        "multilang" => true
      ),

      "desc"    => Array(
        "label"     => "Apraksts:",
        "type" => "wysiwyg",
        'cols'          => 50,
        'rows'          => 7,
        'dialog'        => '?module=wysiwyg&site_id='.$this->site_id,
        'dialogw'       => 800,
        'dialogh'       => 600,
        'dlg_res'       => true,
        "multilang" => true
      ),

      'can_pickup' => Array(
        'label'         => 'Var saņemt preci',
        'type'          => 'check',
      ),

      'coords' => Array(
        'label'         => 'Kartes koordinātes',
        'type'          => 'str',
      ),

    );

    //Collection display table definition
    $this->columns = Array(

      "title_lv"        => Array(
        "width"     => "25%",
        "title"     => "Nosaukums"
      ),

      "desc_lv"        => Array(
        "width"     => "50%",
        "title"     => "Apraksts"
      ),

      "can_pickup"        => Array(
        "title"     => "Var saņemt preces"
      ),

    );

     $this->PostInit();

  }

  function IsEditableOutside(){
    $this->description = 'Veikali';
    $this->longname = $this->name;
    return true;
  }

  function GetEditData($page, &$count, &$recordposition, $order = ''){
    $query_params = $this->GetDefaultQueryParams($page, $order);
    $count = $this->ItemCount($query_params['countwhere']);
    if(isset($recordposition) && $recordposition)
    {
      $recordposition = $this->GetDBRecordPosition($query_params, $recordposition);
    }
    $data = $this->GetDBData($query_params);

    foreach($data as &$entry){

      $entry['can_pickup'] = $entry['can_pickup']? "Jā" : "Nē";

    }

    return $data;

  }

  function processItemBeforeSave($item_id, &$item, $create, &$jsmessage){

    $item['can_pickup'] = $item['can_pickup']? 1 : 0;

    return true;

  }

  function processFormBeforeDisplay(&$form, $create, $item){
    $coords = trim($form['coords']['value']);
    $a = array(56.885, 22.159);

    if (!empty($coords)){
      $c = explode(',', $coords);
      if (count($c) > 1)
      {
        $a[0] = (float)$c[0];
        $a[1] = (float)$c[1];
      }
    }

    ob_start();
?>
  <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
  <script type="text/javascript" src="<?=$GLOBALS['cfgWebRoot'] ?>gui/jquery.min.js"></script>
  <script type="text/javascript">

  var $gm = google.maps;

  var map = null;

  function init_map()
  {
    var latlng = new $gm.LatLng(<?php echo json_encode($a[0]) ?>, <?php echo json_encode($a[1]) ?>);

    map = new $gm.Map(document.getElementById("map_canvas"), {
      zoom: 7,
      center: latlng,
      mapTypeId: $gm.MapTypeId.ROADMAP
    });

    var m = new $gm.Marker({
      position: map.getCenter(),
      map: map,
      draggable: true
    });

    google.maps.event.addListener(m, 'dragend', function(event)
    {
      $('#coords').val(m.position.lat()+', '+m.position.lng());
    });

  }

  $(function(){
    $('#show_map a').click(function(){
      parent.resizeTo(600, 600);
      $('#map_canvas').css('display', '');
      $('#show_map').css('display', 'none');
      $('#hide_map').css('display', '');

      init_map();
      return false;
    });

    $('#hide_map a').click(function(){
      $('#map_canvas').css('display', 'none');
      $('#show_map').css('display', '');
      $('#hide_map').css('display', 'none');

      return false;
    });

  });

  </script>
  <p id="show_map"><a href="#">Show map &raquo;</a></p>
  <p id="hide_map" style="display:none"><a href="#">&laquo; Hide map</a></p>
  <div id="map_canvas" style="display:none; width:100%; height:300px"></div>
<?
    $s = ob_get_contents();
    ob_end_clean();

    $form['_code'] = array(
      'type' => 'code',
      'value' => $s
    );
  }


}
