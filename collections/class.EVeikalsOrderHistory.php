<?
require_once('class.dbcollection.php');

class EVeikalsOrderHistory extends dbcollection{

  function __construct($name,$id){
    $this->dbCollection($name,$id);
    $this->type = get_class();

    //Properties array definition
    $this->properties_assoc = Array(

      "order_id" => Array(
        "label"     => "Order:",
        "type"      => "relation",
        "related_collection" => "EVeikalsOrderCollection",
        "relation_format" => "Order #{%item_id%}",
      ),

      "status_from_id" => Array(
        "label"     => "Status(from):",
        "type"      => "relation",
        "related_collection" => "EVeikalsOrderStatuses",
        "relation_format" => "{%status%}",
      ),

      "status_to_id" => Array(
        "label"     => "Status(to):",
        "type"      => "relation",
        "related_collection" => "EVeikalsOrderStatuses",
        "relation_format" => "{%status%}",
      ),

      "user"    => Array(
        "label"     => "User:",
        "type"      => "str"
      ),

      "comments"    => Array(
        "label"     => "Comments:",
        "type"      => "str"
      ),

      "date"    => Array(
        "label"     => "Date:",
        "type"      => "date"
      ),

    );

    //Collection display table definition
    $this->columns = Array(

      "status_to_id"    => Array(
        "title"     => "Status(to)",
      ),

      "user"        => Array(
        "width" => "20%",
        "title"     => "User:"
      ),

      "comments"    => Array(
        "width" => "40%",
        "title"     => "Comments",
      ),

      "date"    => Array(
        "title"     => "Date",
      ),


    );

     $this->PostInit();
  }

  function IsEditableOutside(){

    $this->description = 'Rēķinu vēsture';
    $this->longname = $this->name;
    return true;
  }

  function IsAddToBottomMode(){
    return false;
  }

  function processFormBeforeDisplay(&$form_data, $create, $item){


  }


}
