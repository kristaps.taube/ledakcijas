<?
//Serveris.LV Constructor collection module
//Created by Aleksandrs Selickis, 2004, alex@datateks.lv

include_once('class.collection.php');

class DzivokliNewsCollection extends collection
{
  //Class initialization
  function DzivokliNewsCollection($name,$id)
  {
    $this->Collection($name,$id);
    $this->type = "dzivoklinewscollection";

    //Properties array definition
    $this->properties = Array(

      "it0" => Array(
        "label"     => "Text:",
        "type"      => "customtext",
        "rows"      => "7",
        "cols"      => "50",
        "dialog"    => "?module=wysiwyg&site_id=".$GLOBALS['site_id'],
        "dialogw"   => "720",
        "dialogh"   => "450",
        "dlg_res"   => true
      ),

      "it1"    => Array(
        "label"     => "Date:",
        "type"      => "str",
        "size"      => "30"
      )

    );

    //Collection display table definition
    $this->columns = Array(

      "col0"        => Array(
        "width"     => "70%",
        "title"     => "Text"
      ),

      "col1"    => Array(
        "width"     => "30%",
        "title"     => "Date"
      )


    );

    //Set default value of date to current time
    $this->properties['it1']['value'] = date("d/m/Y", time());


  }

  function IsEditableOutside()
  {
    $this->description = 'Edit site news';
    $this->longname = $this->name;
    return true;
  }



  //Override GetDBData for postprocessing the retrieved data
  /*
  function GetDBData($changeDate = true)
  {
    //Call original GetDBData()
    $r = parent::GetDBData();
    //Change date to short date format
    if($changeDate)
    {
      foreach($r as $key => $row)
      {
        $r[$key]["col1"] = date('d/m/Y',strtotime($r[$key]["col1"]));
      }
    }
    return $r;
  }
  */

  //Override ChangeItem to maintain order of news items sorted by date
  function ChangeItem($ind, $properties)
  {
    //Call the original ChangeItem()
    parent::ChangeItem($ind, $properties);
    //Sort list by date
    $a = $this->getItem($ind);
    $date = $a[1];
    do
    {
      $lastind = $ind;
      $a = $this->getItem($ind-1);
      if($a[1]<$date)
      {
        $ind = parent::moveItemUp($ind);
      }else
      {
        $a = $this->getItem($ind+1);
        if($a[1]>$date)
        {
          $ind = parent::moveItemDown($ind);
        }
      }
    }while($ind!=$lastind);  //Stop when row hasn't been moved
    return $ind;
  }


}

?>
