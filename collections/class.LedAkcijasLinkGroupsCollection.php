<?php

class LedAkcijasLinkGroupsCollection extends pdocollection
{

    public function __construct($name = '',$id = '')
    {

        parent::__construct($name, $id);

        //Properties array definition
        $this->properties_assoc = [

            "category_id" => Array(
                "label"     => "Kategorija:",
                "type"      => "relation",
                "related_collection" => "shopcatcollection",
                "relation_format" => "{%title_lv%}",
            ),

            "group_name" => [
                "label" => "Nosaukums:",
                "type" => "text",
                "multilang" => true
            ],

        ];

        //Collection display table definition
        $this->columns = [
            "category_id" => ["title" => "Kategorija"],
            "group_name_lv" => ["title" => "Nosaukums"],
            "links"    => Array(
                "title"     => "Linki",
                "subrelated_collection" => "LedAkcijasLinkCollection",
                "subrelated_field" => "group_id",
                "format"    => '[if {%_self%}==1]<b>{%_self%}</b> links[else]<b>{%_self%}</b> linki[/if]',
            ),
        ];

        $this->PostInit();

    }

    public function IsEditableOutside()
    {
        $this->description = 'Collection: '.get_class();
        $this->longname = $this->name;
        return true;
    }

    public function getByCategory($category_id)
    {

        static $cache;

        if(!$cache){

            foreach($this->getDBData() as $group){
                if(!isset($cache[$group['category_id']])) $cache[$group['category_id']] = [];
                $cache[$group['category_id']][] = $group;
            }

        }

        return isset($cache[$category_id]) ? $cache[$category_id] : [];

    }

}