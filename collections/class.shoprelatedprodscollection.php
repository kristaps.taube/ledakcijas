<?php

class shoprelatedprodscollection extends pdocollection
{

    function __construct($name = '', $id = '')
    {

        parent::__construct($name, $id);

        $this->properties_assoc = Array(
            "prod_id"    => Array(
                "label"     => "Product ID:",
                "type"      => "hidden",
                "column_type" => "int(32)",
                "column_index" => 1
            ),

            "related_id"    => Array(
                "label"     => "Related product ID:",
                "type"      => "hidden",
                "column_type" => "int(32)"
            ),

        );

        $this->columns = Array(
            "prod_id"        => Array(
                "title"     => "Product ID",
            ),

            "related_id"        => Array(
                "title"     => "Related product ID",
            )

        );

        $this->postInit();

    }

    function getRelatedIds($prod_id)
    {

        $ids = DB::GetColumn("SELECT related_id FROM `".$this->table."` WHERE prod_id = :pid", [':pid' => $prod_id]);
        return $ids;

    }

    public function addRelation($prod_id, $related_id)
    {

        $relation = $this->getRelation($prod_id, $related_id);
        if(!$relation){

            $this->Insert([
                'prod_id' => $prod_id,
                'related_id' => $related_id,
            ]);

            $this->Insert([
                'prod_id' => $related_id,
                'related_id' => $prod_id,
            ]);

        }

    }

    function getProductRelations($prod_id)
    {
        return sqlQueryData('SELECT * FROM `'.$this->table.'` WHERE prod_id='.$prod_id);
    }

    function getRelation($prod_id, $related_id)
    {
        return sqlQueryData('SELECT * FROM `'.$this->table.'` WHERE prod_id='.$prod_id.' AND related_id='.$related_id);
    }

    function deleteRelation($prod_id, $related_id)
    {
        sqlQuery('DELETE FROM `'.$this->table.'` WHERE prod_id='.$prod_id.' AND related_id='.$related_id);
    }

}


