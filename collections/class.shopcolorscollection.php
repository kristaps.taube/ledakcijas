<?
//Serveris.LV Constructor component
//Created by Aivars Irmejs, 2006, aivars@datateks.lv

include_once('class.dbcollection.php');

class shopcolorscollection extends dbcollection
{
  //Class initialization
  function shopcolorscollection($name,$id)
  {
    $this->DBCollection($name,$id);
    $this->type = "shopcolorscollection";

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "Code",
        "size"      => "35",
        "column_name" => "colorcode",
        "type"      => "str",
      ),

      "it1"    => Array(
        "label"     => "Color",
        "size"      => "35",
        "type"      => "str",
        "column_name" => "color",
        "multilang" => true,
      )

    );

    //Collection display table definition
    $this->columns = Array(

      "colorcode"        => Array(
        "width"     => "50%",
        "title"     => "Code"
      ),

      "color"        => Array(
        "width"     => "",
        "title"     => "Color",
        "multilang" => true,
      )
    );

    $this->postInit();
  }

  function findItems($codes)
  {
    $codes = explode(',', $codes);
    $s = '';
    foreach($codes as $c)
    {
      if($s) $s .= ',';
      $s .= '"' . $c . '"';
    }
    return sqlQueryData("SELECT * FROM " . $this->table . " WHERE colorcode IN ($s) ORDER BY ind");
  }

}

