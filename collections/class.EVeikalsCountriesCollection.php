<?
require_once('class.pdocollection.php');

class EVeikalsCountriesCollection extends dbcollection
{
  //Class initialization
   function __construct($name,$id)
  {
    $this->dbCollection($name,$id);
    $this->type = get_class();

    //Properties array definition
    $this->properties_assoc = Array(
		
			"name"    => Array(
          "label"     => "Region name:",
          "type"      => "str",
          "validate" => "required",
          "multilang" => true          
      ),

      "code"    => Array(
          "label"     => "International code:",
          "type"      => "str",
          "validate" => "required",          	
      ),		
			
			"in_eu"    => Array(
				"label"     => "In Europe:",
				"type"      => "check",          	
      )
			
    );

    //Collection display table definition
    $this->columns = Array(

      "name"        => Array(
        "title"     => "Region name",
        "multilang" => true
      ),
			
			"code"        => Array(
        "title"     => "Intl. code"
      ),			
			
      "in_eu"        => Array(
        "title"     => "In Europe"
      )
			
    );

    $this->postInit();
  }


  function IsEditableOutside()
  {
    $this->description = 'Collection: '.get_class();
    $this->longname = $this->name;
    return true;
  }

}

?>
