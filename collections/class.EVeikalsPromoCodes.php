<?
include_once('class.dbcollection.php');

class EVeikalsPromoCodes extends dbcollection{

  function __construct($name,$id){
    $this->dbCollection($name,$id);
    $this->type = get_class();

    //Properties array definition
    $this->properties_assoc = Array(

      "type" => Array(
        "label"     => "Atlaidi piemērot:",
        "type"      => "list",
        "lookupassoc"    => Array(1 => "Piegādei", 2 => "Produktam", 3 => "Kategorijai", 4 => "Gala summai"),
      ),

      "discount_type" => Array(
        "label"     => "Atlaides veids:",
        "type"      => "list",
        "lookupassoc"    => Array("sum" => "Summa", "precentage" => "Procentuāli"),
      ),

      "category" => Array(
        "label"     => "Kategorijai:",
        "type"      => "relation",
        "related_collection" => "shopcatcollection",
        "relation_format" => "{%title_lv%}",
      ),

      "product" => Array(
        "label"     => "Produktam:",
        "type"      => "relation",
        "related_collection" => "shopprodcollection",
        "relation_format" => "{%name_lv%}",
      ),

      "code"    => Array(
        "label"     => "Code:",
        "type"      => "text"
      ),

      "discount_size"    => Array(
        "label"     => "Atlaides apjoms:",
        "type"      => "text"
      ),

      'sum_discount' => Array(
        'label'         => 'Summēt atlaidi',
        'type'          => 'check',
        'column_type' => "int"
      ),

      'onetime' => Array(
        'label'         => 'Vienreizējas lietošanas',
        'type'          => 'check',
        'column_type' => "int"
      ),

      "pdf"    => Array(
        "label"     => "Pdf:",
        "type"      => "file",
        "upload_dir"    => "files/promocodes/"
      ),

      'used' => Array(
        'label'         => 'Izlietots',
        'type'          => 'check',
        'column_type' => "int"
      ),

      "comments"    => Array(
        "label"     => "Komentāri:",
        "type"      => "html",
        "rows"      => "5",
      ),

    );

    //Collection display table definition
    $this->columns = Array(
      "item_id"        => Array(
        "title"     => "ID"
      ),
      "type"        => Array(
        "title"     => "Piemērot"
      ),
      "discount_type"        => Array(
        "title"     => "Veids"
      ),
      "category"        => Array(
        "title"     => "Kategorijai"
      ),
      "product"        => Array(
        "title"     => "Produktam"
      ),
      "code"        => Array(
        "title"     => "Kods"
      ),
      "discount_size"        => Array(
        "title"     => "Apjoms"
      ),
      "sum_discount"        => Array(
        "title"     => "Summēt atlaidi"
      ),
      "onetime"        => Array(
        "title"     => "Vienreiz lietojams"
      ),
      "used"        => Array(
        "title"     => "Izlietots"
      ),
      "pdf"        => Array(
        "title"     => "PDF",
        "format"    => "[if {%pdf%}]<a href=\"{%pdf%}\" target=\"_blank\">Promo code</a>[/if]"
      ),
       "comments"        => Array(
        "title"     => "Komentāri"
      ),

    );

     $this->PostInit();

  }

  function IsEditableOutside(){
    $this->description = 'Promo codes';
    $this->longname = $this->name;
    return true;
  }

  function getByCode($code){

    static $codes;

    if($codes[$code]) return $codes[$code];

    $codes[$code] = DB::GetRow("SELECT * FROM `".$this->table."` WHERE code = :code", array(":code" => $code));

    return $codes[$code];

  }

  function processItemBeforeSave($item_id, &$item, $create, &$jsmessage){

    $item['onetime'] = $item['onetime']? 1 : 0;
    $item['used'] = $item['used']? 1 : 0;
    $item['sum_discount'] = $item['sum_discount']? 1 : 0;

    return true;

  }

  function GetEditData($page, &$count, &$recordposition, $order = ''){
    $data = parent::GetEditData($page, $count, $recordposition, $order);

    foreach ($data as $key => $row){

      $data[$key]['used'] = $data[$key]['used']? "Jā" : "Nē";
      $data[$key]['onetime'] = $data[$key]['onetime']? "Jā" : "Nē";
      $data[$key]['sum_discount'] = $data[$key]['sum_discount']? "Jā" : "Nē";

      switch($data[$key]['type']){
        case 1: $type = 'Piegādei'; break;
        case 2: $type = 'Produktam'; break;
        case 3: $type = 'Kategorijai'; break;
        case 4: $type = 'Gala summai';  break;
      }
      $data[$key]['type'] = $type;

      switch($data[$key]['discount_type']){
        case 'sum': $type = 'Summa'; break;
        case 'precentage': $type = 'Procentuāli';  break;
      }
      $data[$key]['discount_type'] = $type;

    }
    return $data;
  }

}

