<?
//Serveris.LV Constructor component
//Created by Kristaps Grinbergs, 2006, kristaps@datateks.lv

include_once('class.dbcollection.php');

class riebiepirkumicollection extends dbcollection
{
  //Class initialization
  function riebiepirkumicollection ($name,$id)
  {
    $this->dbcollection($name,$id);
    $this->type = "picturescollection";

    //Properties array definition
    $this->properties = Array(
    
      "it0"    => Array(
        "label"     => "Iepirkuma nosaukums:",
        "type"      => "str"
      ),

      "it1"    => Array(
        "label"     => "Izsludinasanas datums, Laiks:",
        "type"      => "date",
        "dialog"    => "?module=dialogs&action=date&site_id=".$GLOBALS['site_id']."&time=1"
      ),
      
      'it2'   => array (
        'label'   => 'Konkursa nolikuma faila tips:',
        'type'    => 'list',
        'lookup'  => Array('e:Excel', 'w:Word','PDF:PDF')
      ),
      
      "it3"    => Array(
      "label"     => "Konkursa nolikuma fails:",
      "type"      => "dialog",
      "dialog"    => "?module=dialogs&action=filex&site_id=".$GLOBALS['site_id']."&view=thumbs",
      "dialogw"   => "600"
    ),
    
    "it4"    => Array(
        "label"     => "Beigu datums, Laiks:",
        "type"      => "date",
        "dialog"    => "?module=dialogs&action=date&site_id=".$GLOBALS['site_id']."&time=1"
      ),
      
      'it5'   => array (
        'label'   => 'Konkursa rezultatu faila tips:',
        'type'    => 'list',
        'lookup'  => Array('e:Excel', 'w:Word','PDF:PDF')
      ),
      
      "it6"    => Array(
      "label"     => "Konkursa rezultatu fails:",
      "type"      => "dialog",
      "dialog"    => "?module=dialogs&action=filex&site_id=".$GLOBALS['site_id']."&view=thumbs",
      "dialogw"   => "600"
    )

    );

    //Collection display table definition
    $this->columns = Array(

      "col0"        => Array(
        "width"     => "14%",
        "title"     => "Iepirkuma nosaukums"
      ),

      "col1"        => Array(
        "width"     => "14%",
        "title"     => "Izsludinasanas datums, Laiks"
      ),

      "col2"        => Array(
        "width"     => "14%",
        "title"     => "Konkursa nolikuma faila tips"
      ),

      "col3"        => Array(
        "width"     => "14%",
        "title"     => "Konkursa nolikuma fails"
      ),

      "col4"        => Array(
        "width"     => "14%",
        "title"     => "Beigu datums, Laiks"
      ),

      "col5"        => Array(
        "width"     => "14%",
        "title"     => "Konkursa rezultatu faila tips"
      ),

      "col6"        => Array(
        "width"     => "14%",
        "title"     => "Konkursa rezultatu fails"
      )

    );
  }


  function IsEditableOutside()
  {
    $this->description = 'Dokumenti';
    $this->longname = $this->name;
    return true;
  }

}

?>
