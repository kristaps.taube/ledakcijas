<?php

class LedAkcijasLinkCollection extends pdocollection
{

    public function __construct($name = '',$id = '')
    {

        parent::__construct($name, $id);

        //Properties array definition
        $this->properties_assoc = [

            "group_id" => Array(
                "label"     => "Grupa:",
                "type"      => "relation",
                "related_collection" => "LedAkcijasLinkGroupsCollection",
                "relation_format" => "{%group_name_lv%}",
            ),

            "category_id" => Array(
                "label"     => "Kategorija(ja nesakrīt ar grupas):",
                "type"      => "relation",
                "related_collection" => "shopcatcollection",
                "relation_format" => "{%title_lv%}",
            ),

            "name" => [
                "label" => "Nosaukums:",
                "type" => "text",
                "multilang" => true
            ],

            "url" => [
                "label" => "URL:",
                "type" => "text",
                "multilang" => true
            ],

            "title_tag" => [
                "label" => "Title tag:",
                "type" => "text",
                "multilang" => true
            ],

            "meta_description" => [
                "label" => "Meta description:",
                "type" => "text",
                "multilang" => true
            ],

            'text' => [
                'label'         => 'Teksts',
                'type'          => 'wysiwyg',
                'cols'          => 50,
                'rows'          => 7,
                'dialog'        => '?module=wysiwyg&site_id='.$this->site_id,
                'dialogw'       => 800,
                'dialogh'       => 600,
                'dlg_res'       => true,
                "multilang" => true
            ],

            "filters" => [
                "label"     => "Filtri",
                "type"      => "code",
            ],

            "show_feat_cat"    => [
                "label"     => "Rādīt pie izceltajām kategorijām:",
                "type"      => "check",
                "column_type" => "int(10)",
            ],

            "show_in_menu"    => [
                "label"     => "Rādīt izvēlnē:",
                "type"      => "check",
                "column_type" => "int(10)",
            ],

        ];

        //Collection display table definition
        $this->columns = [
            "group_id" => ["title" => "Grupa"],
            "name_lv" => ["title" => "Nosaukums"],
            "show_feat_cat" => ["title" => "Rādīt pie izceltajām kategorijām"],
            "show_in_menu" => ["title" => "Rādīt izvēlnē"],
        ];

        $this->PostInit();

    }

    public function IsEditableOutside()
    {
        $this->description = 'Collection: '.get_class();
        $this->longname = $this->name;
        return true;
    }

    public function GetEditData($page, &$count, &$recordposition, $order = '')
    {

        $data = parent::GetEditData($page, $count, $recordposition, $order);

        foreach($data as &$entry){

            $entry['show_feat_cat'] = $entry['show_feat_cat'] ? "Jā" : "Nē";
            $entry['show_in_menu'] = $entry['show_in_menu'] ? "Jā" : "Nē";

        }

        return $data;

    }

    function processItemBeforeSave($item_id, &$item, $create, &$jsmessage)
    {

        foreach(getLanguages() as $short => $lang){

            if(!$item['name_'.$short] && $item['name_lv']) $item['name_'.$short] = $item['name_lv'];
            if(!$item['url_'.$short] && $item['name_'.$short]) $item['url_'.$short] = transliterateURL($item['name_'.$short].'-'.$short);

            $item['show_feat_cat'] = $item['show_feat_cat'] ? 1 : 0;
            $item['show_in_menu'] = $item['show_in_menu'] ? 1 : 0;

        }

        return parent::processItemBeforeSave($item_id, $item, $create, $jsmessage);

    }

    public function getForAllCategories($category_id)
    {

        return DB::GetTable("
            SELECT l.*
            FROM `".$this->table."` AS l
            JOIN `".$this->group_id->table."` AS g ON g.item_id = l.group_id
            WHERE
                g.category_id = :category_id
        ", [':category_id' => $category_id]);

    }

    public function getForFeaturedCategory($category_id)
    {

        return DB::GetTable("
            SELECT l.*
            FROM `".$this->table."` AS l
            JOIN `".$this->group_id->table."` AS g ON g.item_id = l.group_id
            WHERE
                g.category_id = :category_id AND
                l.show_feat_cat = 1
        ", [':category_id' => $category_id]);

    }

    public function getForMenu($group_id)
    {

        return DB::GetTable("
            SELECT l.*
            FROM `".$this->table."` AS l
            JOIN `".$this->group_id->table."` AS g ON g.item_id = l.group_id
            WHERE
                l.group_id = :group_id AND
                l.show_in_menu = 1
        ", [':group_id' => $group_id]);

    }

    public function getByUrlAndCategory($url, $category_id)
    {

        $cond = $params = [];

        $or_cond =[];
        foreach(getLanguages() as $short => $lang){
            $or_cond[] = 'l.url_'.$short.' = :url';
        }
        $params[':url'] = $url;
        $cond[] = '('.implode(" OR ", $or_cond).')';

        $cond[] = '(g.category_id = :category_id OR l.category_id = :category_id)';
        $params[':category_id'] = $category_id;

        $link = DB::GetRow("
            SELECT l.*
            FROM `".$this->table."` AS l
            JOIN `".$this->group_id->table."` AS g ON g.item_id = l.group_id
            WHERE ".implode(" AND ", $cond)."
        ", $params);

        return $link;

    }

    public function getByGroup($group_id)
    {

        static $cache;

        if(!$cache){

            foreach($this->getDBData() as $filter){
                if(!isset($cache[$filter['group_id']])) $cache[$filter['group_id']] = [];
                $cache[$filter['group_id']][] = $filter;
            }

        }

        return isset($cache[$group_id]) ? $cache[$group_id] : [];

    }

    public function processFormBeforeDisplay(&$form_data, $create, $item)
    {

        $link_filter_col = LedAkcijasLinkFiltersCollection::getInstance();
        $cat_filter_groups_col = EVeikalsCatFilterGroupRel::getInstance();

        $link_group = $item['group_id'] ? $this->group_id->getByID($item['group_id']) : false;

        $filter_options = [];

        if($link_group){

            $cat_id = $item['category_id'] ? $item['category_id'] : $link_group['category_id'];

            $filter_group_ids = $cat_filter_groups_col->getGroupIds($cat_id);
            foreach($filter_group_ids as $filter_group_id){

                $filter = $cat_filter_groups_col->filter_group_id->getById($filter_group_id);

                $options = $cat_filter_groups_col->filter_group_id->params->getTable(['where' => '`group` = :group', 'params' => [':group' => $filter_group_id]]);

                $filter_options[$filter['title_lv']] = $options;

            }

        }

        $current_filter_ids = isset($item['item_id']) && $item['item_id'] ? $link_filter_col->getFilterIdsByLink($item['item_id']) : [];

        ob_start();
        ?>
        <select name="filter_ids[]" multiple class='formEdit' style='height: 200px;width: 100%;'>
        <?php foreach($filter_options as $group => $options){ ?>
            <optgroup label="<?php echo $group ?>">
            <?php foreach($options as $option){ ?>
            <option value="<?php echo $option['item_id']?>" <?php echo in_array($option['item_id'], $current_filter_ids) ? "selected='selected'" : "" ?>><?php echo $option['param_title_lv']?></option>
            <?php } ?>
            </optgroup>
        <?php } ?>
        </select>
        <?php
        $form_data['filters']['value'] = ob_get_clean();

    }

    function processItemAfterSave($item_id, $item, $create)
    {

        $link_filter_col = LedAkcijasLinkFiltersCollection::getInstance();

        $selected_filters = $_POST['filter_ids'];
        $current_filter_ids = $item_id ? $link_filter_col->getFilterIdsByLink($item_id) : [];

        foreach($selected_filters as $selected_filter){
            if(!in_array($selected_filter, $current_filter_ids)){ // adding new
                $link_filter_col->Insert(['link_id' => $item_id, 'filter_id' => $selected_filter]);
            }
        }

        foreach($current_filter_ids as $current_filter_id){
            if(!in_array($current_filter_id, $selected_filters)){
                $link_filter_col->removeByFilterIdAndLinkId($current_filter_id, $item_id); // remove missing
            }
        }

    }

}