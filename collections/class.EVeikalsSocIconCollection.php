<?
include_once('class.dbcollection.php');

class EVeikalsSocIconCollection extends dbcollection{

  function __construct($name,$id)
  {
    $this->dbCollection($name,$id);
    $this->type = get_class();

    //Properties array definition
    $this->properties_assoc = Array(

      "title"    => Array(
        "label"     => "Nosaukums:",
        "type"      => "text"
      ),

      "link"    => Array(
        "label"     => "Saite:",
        "type"      => "dialog",
        "dialog"    => "?module=dialogs&action=links&site_id=".$GLOBALS['site_id']
      ),

      "icon_top"    => Array(
        "label"     => "Ikona augšā:",
        "type"      => "file",
        'upload_dir'    => 'images/soc_icons/'
      ),

      "icon_bottom"    => Array(
        "label"     => "Ikona apaksā:",
        "type"      => "file",
        'upload_dir'    => 'images/soc_icons/'
      ),

      "icon_mobile"    => Array(
        "label"     => "Ikona mobīlajā izvēlnē:",
        "type"      => "file",
        'upload_dir'    => 'images/soc_icons/'
      ),

      "show_top"    => Array(
        'label'         => 'Rādīt augšā',
        'type'          => 'check',
        'column_type'   => 'tinyint(1)'
      ),

      "show_bottom"    => Array(
        'label'         => 'Rādīt apakšā',
        'type'          => 'check',
        'column_type'   => 'tinyint(1)'
      ),

      "show_mobile"    => Array(
        'label'         => 'Rādīt mobīlajā izvēlnē',
        'type'          => 'check',
        'column_type'   => 'tinyint(1)'
      ),


    );

    //Collection display table definition
    $this->columns = Array(

      "title" => Array("title"     => "Nosaukums"),
      "icon_top" => Array(
        "title" => "Ikona augšā",
        "format"    => "[if {%icon_top%}]<img src='{%icon_top%}' alt='' />[/if]"
      ),
      "icon_bottom" => Array(
        "title" => "Ikona augšā",
        "format"    => "[if {%icon_bottom%}]<img src='{%icon_bottom%}' alt='' />[/if]"
      ),
      "icon_mobile" => Array(
        "title" => "Ikona mobīlajā izvēlnē",
        "format"    => "[if {%icon_mobile%}]<img src='{%icon_mobile%}' alt='' />[/if]"
      ),
      "show_top" => Array("title"     => "Rādīt augšā"),
      "show_bottom" => Array("title"     => "Rādīt apakšā"),
      "show_mobile" => Array("title"     => "Rādīt mobīlajā izvēlnē"),

    );

     $this->PostInit();
  }

  function getBottomIcons(){
    return $this->getDBData(array("where" => "show_bottom = 1", "order" => "ind"));
  }

  function getTopIcons(){
    return $this->getDBData(array("where" => "show_top = 1", "order" => "ind"));
  }

  function getMobileIcons(){
    return $this->getDBData(array("where" => "show_mobile = 1", "order" => "ind"));
  }

  function IsEditableOutside()
  {
    $this->description = 'Soc. icons';
    $this->longname = $this->name;
    return true;
  }

  function processItemBeforeSave($item_id, &$item, $create, &$jsmessage){

    $item['show_top'] = $item['show_top']? 1 : 0;
    $item['show_bottom'] = $item['show_bottom']? 1 : 0;
    $item['show_mobile'] = $item['show_mobile']? 1 : 0;

    return true;

  }

  function GetEditData($page, &$count, &$recordposition, $order = ''){
    $query_params = $this->GetDefaultQueryParams($page, $order);
    $count = $this->ItemCount($query_params['countwhere']);
    if(isset($recordposition) && $recordposition)
    {
      $recordposition = $this->GetDBRecordPosition($query_params, $recordposition);
    }
    $data = $this->GetDBData($query_params);

    foreach($data as &$entry){

      $entry['show_top'] = $entry['show_top']? "Jā" : "Nē";
      $entry['show_bottom'] = $entry['show_bottom']? "Jā" : "Nē";
      $entry['show_mobile'] = $entry['show_mobile']? "Jā" : "Nē";

    }

    return $data;

  }


}

?>