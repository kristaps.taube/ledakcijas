<?
//Serveris.LV Constructor collection module
//Created by Aleksandrs Selickis, 2004

include_once('class.dbcollection.php');

class forum2katcollection extends dbcollection
{
  //Class initialization
  function forum2katcollection($name,$id)
  {
    $this->dbcollection($name,$id);
    $this->type = "forum2katcollection";

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "Category name",
        "size"      => "35",
        "type"      => "str"
      ),

      "it1"    => Array(
        "label"     => "Description",
        "type"      => "html",
        "rows"      => "6",
        "cols"      => "30",
        "search"    => true
      ),

      "it2"    => Array(
        "label"     => "Date",
        "size"      => "35",
        "type"      => "str"
      ),

      "it3"    => Array(
        "label"     => "Posted by",
        "size"      => "35",
        "type"      => "str"
      ),

      "it4"    => Array(
        "label"     => "Can comment:",
        "type"      => "hidden",
        "size"      => "30"
      )

    );

    //Collection display table definition
    $this->columns = Array(

      "col0"        => Array(
        "width"     => "20%",
        "title"     => "Category name"
      ),

      "col1"        => Array(
        "width"     => "40%",
        "title"     => "Description"
      ),

      "col2"        => Array(
        "width"     => "20%",
        "title"     => "Date"
      ),

      "col3"        => Array(
        "width"     => "20%",
        "title"     => "Posted by"
      ),

    );

  }

  function GetItemByID($ind)
  {
    if(!$this->name)
    {
      $this->error = true;
      return false;
    }
    $row = sqlQueryRow("SELECT * FROM `".$this->site_id."_coltable_".$this->name."` WHERE item_id='$ind'");
    $a = Array();
    $f = 0;
    for($f = 0; $f < count($row); $f++)
    {
      if(isset($row['col'.$f]))
      {
        $a[$f] = $row['col'.$f];
      }
    }
    return $a;
  }


  function GetAllCategories($min,$max)
  {
    return sqlQueryData("SELECT * FROM `".$this->site_id."_coltable_".$this->name."` ORDER BY ind LIMIT $min,$max");
  }

  //Override GetDBData for postprocessing the retrieved data
  /*
  function GetDBData()
  {
    if (!function_exists("cmpind"))
      {
        function cmpind($a, $b) 
        {
           if ($a['ind'] == $b['ind']) {
               return 0;
           }
           return ($a['ind'] > $b['ind']) ? -1 : 1;
        }
    }
    
    //Call original GetDBData()
    $r = parent::GetDBData();
    //order by ind desc
    usort($r, "cmpind");
    return $r;
  }
  */

    //news
  function IsCategory($item_id, $name)
  {
    if(!$name)
    {
      $this->error = true;
      return false;
    }
    return sqlQueryValue("SELECT count(item_id) FROM `".$this->site_id."_coltable_".$name."` WHERE item_id=$item_id");
  }

  //news
  function AddNewItemByID($item_id, $name)
  {
    if(!$name)
    {
      $this->error = true;
      return false;
    }
    $newind = 1 + sqlQueryValue("SELECT MAX(ind) FROM `".$this->site_id."_coltable_".$name."`");
    $f = 0;
    $cols = "";
    $data = "";
    foreach($this->properties as $key => $prop)
    {
      if($cols)
        $cols .= ",";
      $cols .= "col" . $f;
      if($data)
        $data .= ",";
      $data .= "''";
      $f++;
    }
    sqlQuery("INSERT INTO `".$this->site_id."_coltable_".$name."` (item_id, ".$cols.", ind) VALUES (".$item_id.", ".$data.", '".$newind."')");

    return $newind;
  }



}

?>
