<?php

class EMailNotificationCollection extends pdocollection
{

    public function __construct($name = '',$id = '')
    {

        parent::__construct($name, $id);

        //Properties array definition
        $this->properties_assoc = [

            "date" => [
                "label" => "Datums:",
                'type'          => 'date',
                'column_type'   => 'datetime',
            ],

            "email" => [
                "label" => "E-pasts:",
                "type" => "text",
            ],

            "hash" => [
                "label" => "Hash:",
                "type" => "text",
            ],

            "subject" => [
                "label" => "Tēma:",
                "type" => "text",
            ],

            "content" => [
                "label" => "Saturs:",
                'type'          => 'wysiwyg',
                'cols'          => 50,
                'rows'          => 7,
                'dialog'        => '?module=wysiwyg&site_id='.$this->site_id,
                'dialogw'       => 800,
                'dialogh'       => 600,
                'dlg_res'       => true
            ],

        ];

        //Collection display table definition
        $this->columns = [
            "date" => ["title" => "Datums"],
            "subject" => ["title" => "Tēma"],
            "email" => ["title" => "Epasts"],
        ];

        $this->PostInit();

    }

    public function getByHash($hash)
    {
        return $this->getRow(['where' => "hash = :hash", 'params' => [':hash' => $hash]]);
    }

    public function IsEditableOutside()
    {
        $this->description = 'Collection: '.get_class();
        $this->longname = $this->name;
        return true;
    }

}