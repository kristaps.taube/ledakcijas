<?
/*
 * Datateks.lv Constructor component
 * Created by Raivo Fismeisters, 2008, raivo@datateks.lv
*/

include_once('class.dbcollection.php');

class test2collection  extends dbcollection
{
  function test2collection($name, $id)
  {
    $this->DBcollection($name, $id);
    $this->type = "test2collection";

    $this->properties_assoc = Array(
      'title' => Array(
        'label'         => 'Virsraksts',
        'type'          => 'str',
      ),

      'short_text' => Array(
        'label'         => 'Īsais teksts',
        'type'          => 'wysiwyg',
        'cols'          => 50,
        'rows'          => 7,
        'dialog'        => '?module=wysiwyg&site_id='.$this->site_id,
        'dialogw'       => 800,
        'dialogh'       => 600,
        'dlg_res'       => true
      ),

      'full_text' => Array(
        'label'         => 'Pilnais teksts',
        'type'          => 'wysiwyg',
        'cols'          => 50,
        'rows'          => 7,
        'dialog'        => '?module=wysiwyg&site_id='.$this->site_id,
        'dialogw'       => 800,
        'dialogh'       => 600,
        'dlg_res'       => true
      ),

      'image' => Array(
        'label'         => 'Attēls',
        'type'          => 'file',
        'upload_dir'    => 'images/newnewscollection/'
      ),

      'news_date' => Array(
        'label'         => 'Datums',
        'type'          => 'date',
        'column_type'   => 'int',
        'time'          => 1,
        'value'         => time()
      ),

      'start_date' => Array(
        'label'         => 'Sākuma datums',
        'type'          => 'date',
        'column_type'   => 'int',
        'column_index'  => 1,
        'time'          => 1
      ),

      'end_date' => Array(
        'label'         => 'Beigu datums',
        'type'          => 'date',
        'column_type'   => 'int',
        'column_index'  => 1,
        'time'          => 1
      ),

      'author' => Array(
        'label'         => 'Autors',
        'type'          => 'str',
      ),

      'archived' => Array(
        'label'         => 'Arhivēta',
        'type'          => 'check',
        'column_index'  => 1,
        'column_type'   => 'tinyint(1)'
      ),

      'allow_comments' => Array(
        'label'         => 'Atļaut komentārus',
        'type'          => 'check',
        'column_type'   => 'tinyint(1)',
        'value'         => 1
      ),

      'disabled' => Array(
        'label'         => 'Slēpts',
        'type'          => 'hidden',
        'column_type'   => 'tinyint(1)',
        'column_index'  => 1,
        'value'         => 0
      ),

      'last_update' => Array(
        'label'         => 'Last update',
        'type'          => 'hidden',
        'column_type'   => 'int(10)',
        'column_index'  => 1
      ),

    );

    $this->columns = Array(
      'title' => Array(
        'title'         => 'Virsraksts'
      ),

      'short_text' => Array(
        'title'         => 'Īsais apraksts'
      ),

      "comments"      => Array(
        "title"     => "Komentāri:",
        "subrelated_collection" => "newnewscommentscollection",
        "subrelated_field" => "news_id",
        "format"        => "[if {%_self%}==1]<b>{%_self%}</b> komentārs[else]<b>{%_self%}</b> komentāri[/if]"
      ),

    );

    $this->postInit();
  }

  function doAutoLastUpdate()
  {
    return true;
  }

  function isAddToBottomMode()
  {
    return false;
  }

  function IsEditableOutside()
  {
    $this->description = 'News collections';
    $this->longname = $this->name;
    return true;
  }

  function processFormBeforeDisplay(&$form_data, $create)
  {
    $format = 'Y-m-d H:i:s';
    $form_data['news_date']['value'] = $form_data['news_date']['value'] ? date($format, $form_data['news_date']['value']) : '';
    $form_data['start_date']['value'] = $form_data['start_date']['value'] ? date($format, $form_data['start_date']['value']) : '';
    $form_data['end_date']['value'] = $form_data['start_date']['value'] ? date($format, $form_data['end_date']['value']) : '';
  }

  function processItemBeforeSave($item_id, &$item, $create, &$jsmessage)
  {
    $item['archived'] = (int)((boolean)$item['archived']);
    $item['allow_comments'] = ($item['allow_comments'] != '');
    $item['news_date'] = strtotime($item['news_date']);
    $item['start_date'] = strtotime($item['start_date']);
    $item['end_date'] = strtotime($item['end_date']);
    return parent::processItemBeforeSave($item_id, $item, $create, $jsmessage);
  }

  function processDBproperties(&$form_data)
  {
    $keys = Array('image');

    foreach ($keys as $k)
    {
      $width = $this->getProperty($k.'_width');
      $height = $this->getProperty($k.'_height');
      if ($width && $height)
      {
        $form_data[$k]['resize_mode'] = $this->getProperty($k.'_resizemode');
        $form_data[$k]['resize_width'] = $width;
        $form_data[$k]['resize_height'] = $height;
      }
    }

    $keys = Array(
      'full_text', 'image', 'news_date', 'start_date',
      'end_date', 'archived', 'author'
    );

    foreach ($keys as $k)
    {
      if (!$this->getProperty('show_'.$k))
        $form_data[$k]['type'] = 'hidden';

    }
  }

  function getNews($where, $offset, $count, $order='')
  {
    $where = ($where != '') ? ' WHERE '.$where : '';
    $order = ($order != '') ? ' ORDER BY '.$order : '';
    $limit = ' LIMIT '.$offset.','.$count;
//($offset != '' && $count != '') ?
//echo "SELECT SQL_CALC_FOUND_ROWS * FROM `".$this->table."`".$where.$order.$limit;

    $data = sqlQueryData("SELECT SQL_CALC_FOUND_ROWS * FROM `".$this->table."`".$where.$order.$limit);
    $count = sqlQueryValue("SELECT FOUND_ROWS()");
    return Array(
      'data'    => $data,
      'count'   => $count
    );
  }

  function updateStartEndNews()
  {
    $t = time();
    sqlQuery("UPDATE `".$this->table."` SET disabled=1 WHERE disabled=0 AND start_date!=0 AND end_date!=0 AND archived=0 AND $t < start_date");
    sqlQuery("UPDATE `".$this->table."` SET disabled=0 WHERE disabled=1 AND $t >= start_date AND $t <= end_date");
    sqlQuery("UPDATE `".$this->table."` SET disabled=0, archived=1 WHERE archived=0 AND start_date!=0 AND end_date!=0 AND $t > end_date");
  }

  function updateAutomaticArchive($archiving_age)
  {
    $t = time();
    sqlQuery("UPDATE `".$this->table."` SET archived=1 WHERE archived=0 AND $t >= (news_date + $archiving_age)");
  }

  function getItemsForIndex($time)
  {
    return sqlQueryData('SELECT * FROM `'.$this->table.'` WHERE last_update >= '.$time);
  }
}

?>