<?
//Serveris.LV Constructor collection module
//Created by Aleksandrs Selickis, 2004

include_once('class.collection.php');

class formgeneratorCollection extends collection
{
  //Class initialization
  function formgeneratorCollection($name,$id)
  {
    $this->Collection($name,$id);
    $this->type = "formgeneratorcollection";

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"   => "Input type:",
        "type"    => "list",
        "width"   => "200",
        "lookup"  => Array("text:text", "textarea:textarea", "checkbox:checkbox", "password:password", "hidden:hidden", "image:image", "reset:reset", "submit:submit")
      ),

      "it1"    => Array(
        "label"     => "Name",
        "size"      => "35",
        "type"      => "str"
      ),

      "it2"    => Array(
        "label"     => "Value",
        "size"      => "35",
        "type"      => "str"
      ),

      "it4"    => Array(
        "label"     => "Properties",
        "size"      => "35",
        "type"      => "str"
      )

    );

    //Collection display table definition
    $this->columns = Array(

      "col0"        => Array(
        "width"     => "20%",
        "title"     => "Param name"
      ),

      "col1"    => Array(
        "width"     => "20%",
        "title"     => "Param value"
      ),

      "col2"        => Array(
        "width"     => "20%",
        "title"     => "Param name"
      ),

      "col3"    => Array(
        "width"     => "20%",
        "title"     => "Param value"
      )

    );




  }




}

?>
