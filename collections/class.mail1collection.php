<?
//Serveris.LV Constructor collection module
//Created by Aivars Irmejs, 2003, aivars@serveris.lv

include_once('class.collection.php');

class Mail1Collection extends collection
{
  //Class initialization
  function Mail1Collection($name,$id)
  {
    $this->Collection($name,$id);
    $this->type = "mail1collection";

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "Name:",
        "size"      => "35",
        "type"      => "str"
      ),

      "it1" => Array(
        "label"     => "Surname:",
        "size"      => "35",
        "type"      => "str"
      ),

      "it2"    => Array(
        "label"     => "E-mail:",
        "type"      => "str",
        "size"      => "30"
      ),

	  "it3" => Array(
        "label"     => "Phone number:",
        "size"      => "35",
        "type"      => "str"
      ),

      "it4"    => Array(
        "label"     => "Message:",
        "type"      => "str",
        "size"      => "30"
      )

    );

    //Collection display table definition
    $this->columns = Array(

      "col0"        => Array(
        "width"     => "15%",
        "title"     => "Name:"
      ),

      "col1"    => Array(
        "width"     => "15%",
        "title"     => "Surname:"
      ),

      "col2"     => Array(
        "width"     => "15%",
        "title"     => "E-mail"
      ),

	  "col3"    => Array(
        "width"     => "15%",
        "title"     => "Phone:"
      ),

      "col4"     => Array(
        "width"     => "40%",
        "title"     => "Message"
      )


    );


  }



}

?>
