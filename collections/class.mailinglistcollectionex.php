<?
//Serveris.LV Constructor component
//Created by Aleksandrs Selickis, 2004, alex@datateks.lv

include_once('class.dbcollection.php');

class mailinglistcollectionex extends dbcollection
{
  //Class initialization
  function mailinglistcollectionex($name,$id)
  {
    $this->dbCollection($name,$id);
    $this->type = "mailinglistcollectionex";

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "Name",
        "size"      => "35",
        "type"      => "str",
        "column_name" => "maillist_name"
      ),

      "it1"    => Array(
        "label"     => "E-mail",
        "size"      => "35",
        "type"      => "str",
        "validate"  => "email",
        "column_name" => "maillist_email"
      ),

      "it2"    => Array(
        "label"     => "Status",
        "size"      => "35",
        "type"      => "list",
        "lookupassoc" => Array(0 => 'Unconfirmed', 1 => 'Active', 2 => 'Unsubscribed'),
        "column_name" => "maillist_status",
        "column_type" => "tinyint(1)",
        "column_index" => 1
      ),

      "it3"    => Array(
        "label"     => "IP",
        "size"      => "35",
        "type"      => "str",
        "column_name" => "maillist_ip"
      ),

      "it4"    => Array(
        "label"     => "Subscription time",
        "size"      => "35",
        "type"      => "date",
        "time"      => 1,
        "column_name" => "maillist_time",
      ),

      "it5"    => Array(
        "label"     => "hash",
        "size"      => "35",
        "type"      => "hidden",
        "column_name" => "maillist_hash"
      ),

      "it6"    => Array(
        "label"     => "lang",
        "size"      => "35",
        "type"      => "hidden",
        "column_name" => "lang"
      ),

    );

    //Collection display table definition
    $this->columns = Array(

      "maillist_name"        => Array(
        "title"     => "Name"
      ),

      "maillist_email"        => Array(
        "title"     => "E-mail"
      ),

      "maillist_status"        => Array(
        "title"     => "Status",
        "format"    => "[if {%_self%}==0]Unconfirmed[/if][if {%_self%}==1]Active[/if][if {%_self%}==2]Unsubscribed[/if]"
      ),

      "maillist_ip"        => Array(
        "title"     => "IP"
      ),

      "maillist_time"        => Array(
        "title"     => "Subscribed"
      ),

      "categories"    => Array(
        "width"     => "",
        "title"     => "Categories",
        "subrelated_collection" => "mailingcategoriesemailscollectionex",
        "subrelated_field" => "mailcatemail_email",
        "format"    => '<b>{%_self%}</b> categories',
        "sortdesc"  => true,
      )

    );


    $this->PostInit();
  }


  function GetEmailsFromCategories($ids, $relcol, $count = 0, $offset = 0 )
  {
    if(!$ids || !is_array($ids))
      return false;
    
    if($count != 0 )
    {
      $limit_sql = 'LIMIT ' . $offset . ', ' . $count . ' ';
    }
    else
    {
      $limit_sql = '';
    }
    
    $idlist = implode(',', $ids);
    return sqlQueryData('
      SELECT *
      FROM `' . $this->table . '` A
        LEFT JOIN `' . $relcol->table . '` B ON A.item_id = B.mailcatemail_email
      WHERE
        B.mailcatemail_cat IN (' . $idlist . ') AND
        A.maillist_status = 1 
    ' . $limit_sql);
  }
  
  function GetEmailsFromCategoriesCount($ids, $relcol)
  {
    if(!$ids || !is_array($ids))
      return false;

    $idlist = implode(',', $ids);
    return sqlQueryData('
      SELECT count(*) as count
      FROM `' . $this->table . '` A
        LEFT JOIN `' . $relcol->table . '` B ON A.item_id = B.mailcatemail_email
      WHERE
        B.mailcatemail_cat IN (' . $idlist . ') AND
        A.maillist_status = 1
    ');
  }
  
  function GetUnsetEmailsFromCategoriesCount($message_id, $ids, $relcol, $logcol) {
    if(!$ids || !is_array($ids))
      return false;

    $idlist = implode(',', $ids);
    $total_emails =  sqlQueryData('
      SELECT *
      FROM `' . $this->table . '` A
        LEFT JOIN `' . $relcol->table . '` B ON A.item_id = B.mailcatemail_email
      WHERE
        B.mailcatemail_cat IN (' . $idlist . ') AND
        A.maillist_status = 1
    ');
    $count = 0;

    if(!empty($total_emails)) {
      foreach( $total_emails as $email) {
        $sent = $logcol->GetDBData(array(
          'where' => ' maillog_email = "' . $email['maillist_email'] . '" AND message_id = ' . $message_id . '   ',
        ));
        if(empty($sent)) {
          $count = $count + 1;
        }
      }
    }
    return $count;
  }
  
  function GetUnsetEmailsFromCategories($message_id, $ids, $relcol, $logcol) {
    if(!$ids || !is_array($ids))
      return false;

    $idlist = implode(',', $ids);
    $total_emails =  sqlQueryData('
      SELECT *
      FROM `' . $this->table . '` A
        LEFT JOIN `' . $relcol->table . '` B ON A.item_id = B.mailcatemail_email
      WHERE
        B.mailcatemail_cat IN (' . $idlist . ') AND
        A.maillist_status = 1
    ');
    $return = array();
    if(!empty($total_emails)) {
      foreach( $total_emails as $email) {
        $sent = $logcol->GetDBData(array(
          'where' => ' maillog_email = "' . $email['maillist_email'] . '" AND message_id = ' . $message_id . '   ',
        ));
        if(empty($sent)) {
          $return[] = $email;
        }
      }
    }
    return $return;
  }
  
  
  function getAllUsentEmailsCount ($message_id, $logcol) {
    $total_emails = $this->GetDBData(array(
    'where' => 'maillist_status = 1',
    ));
    
    $count = 0;
    if(!empty($total_emails)) {
      foreach( $total_emails as $email) {

        $sent = $logcol->GetDBData(array(
          'where' => ' maillog_email = "' . $email['maillist_email'] . '" AND message_id = ' . $message_id . '   ',
        ));
        if(empty($sent)) {
          $count = $count + 1;
        }
      }
    }
    return $count;
  }
  
  function getAllUnsentEmails ($message_id, $logcol) {
    $total_emails = $this->GetDBData(array(
    'where' => 'maillist_status = 1',
    ));
    
    $return = array();
    if(!empty($total_emails)) {
      foreach( $total_emails as $email) {
        $sent = $logcol->GetDBData(array(
          'where' => ' maillog_email = "' . $email['maillist_email'] . '" AND message_id = ' . $message_id . '   ',
        ));
        if(empty($sent)) {
          $return[] = $email;
        }
      }
    }
    return $return;
  
  }
  

  


  //parameters need to be escaped before calling
  function HasEmail($email)
  {
    return sqlQueryValue('SELECT COUNT(*) FROM `' . $this->table . '` WHERE maillist_email = "' . $email . '"');
  }


  //parameters need to be escaped before calling
  function GetItemByEmail($email)
  {
    return sqlQueryRow('SELECT * FROM `' . $this->table . '` WHERE maillist_email = "' . $email . '"');
  }

  //parameters need to be escaped before calling
  function GetItemByHash($hash)
  {
    return sqlQueryRow('SELECT * FROM `' . $this->table . '` WHERE maillist_hash = "' . $hash . '"');
  }

  //parameters need to be escaped before calling
  function AddEmail($name, $email, $status = 0, $lang)
  {
    $item = $this->GetItemByEmail($email);
    if(!$item)
    {
      return $this->AddItemAssoc(
        Array(
          'maillist_name' => $name,
          'maillist_email' => $email,
          'maillist_status' => $status,
          'maillist_ip' => $_SERVER['REMOTE_ADDR'],
          'maillist_time' => date('Y-m-d H:i:s'),
          'maillist_hash' => md5(stripslashes($email) . rand(1, 999999)),
          'lang' => $lang
        )
      );
    }else
    {
      return $item['ind'];
    }
  }

  function isEmailSubscribed($email)
  {
    return (boolean)sqlQueryValue('SELECT COUNT(*) FROM '.$this->table.' WHERE maillist_email="'.$email.'" AND maillist_status = 1');
  }

  function IsEditableOutside()
  {
    $this->description = 'Mailing list subscribers';
    $this->longname = $this->name;
    return true;
  }







}

?>