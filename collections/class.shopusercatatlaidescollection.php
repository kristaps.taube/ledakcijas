<?
//Serveris.LV Constructor component
//Created by Aivars Irmejs, 2006, aivars@datateks.lv

include_once('class.dbcollection.php');

class shopusercatatlaidescollection extends dbcollection
{
  //Class initialization
  function shopusercatatlaidescollection($name,$id)
  {
    $this->DBCollection($name,$id);
    $this->type = "shopusercatatlaidescollection";

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "Discount:",
        "size"      => "35",
        "type"      => "str",
        "column_name" => "atl_atlaide",
      ),

      "it1"    => Array(
        "label"     => "User:",
        "type"      => "relation",
        "related_collection" => "shopusercollection",
        "relation_format" => "{%email%}",
        "column_name" => "atl_lietotajs",
        "column_type" => "INT",
      ),

      "it2"    => Array(
        "label"     => "Category:",
        "type"      => "relation",
        "related_collection" => "shopcatcollection",
        "relation_format" => "{%title_lv%}",
        "column_name" => "atl_cat",
        "column_type" => "INT",
      ),

    );

    //Collection display table definition
    $this->columns = Array(

      "atl_atlaide"    => Array(
        "title"     => "Atlaide",
      ),

      "atl_lietotajs"        => Array(
        "title"     => "Lietotājs",
      ),

      "atl_cat"        => Array(
        "width"     => "",
        "title"     => "Kategorija",
      ),

    );

    $this->postInit();
  }


  function getDiscountForCat($cat_id, $user_id){

  	static $cache;

    if(!is_array($cache[$user_id])){

    	$cache[$user_id] = array();
			foreach(DB::GetTable("SELECT * FROM `".$this->table."` WHERE atl_lietotajs = :id", array(":id" => $user_id)) as $entry){
      	$cache[$user_id][$entry['atl_cat']] = $entry['atl_atlaide'];
			}

    }

		return isset($cache[$user_id][$cat_id]) ? $cache[$user_id][$cat_id] : 0;

  }

  function getDiscountsForUser($user_id)
  {
    return sqlQueryData('SELECT * FROM `' . $this->table . '` WHERE atl_lietotajs = ' . intval($user_id));
  }

  function deleteDiscountsForUser($user_id)
  {
    $data = $this->getDiscountsForUser($user_id);
    foreach($data as $row)
    {
      $this->DelDBItem($row['item_id']);
    }
  }


}

