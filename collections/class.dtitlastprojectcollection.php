<?
//Serveris.LV Constructor component
//Created by Aleksandrs Selickis, 2004, alex@datateks.lv

include_once('class.dbcollection.php');

class dtitlastprojectcollection extends dbcollection
{
  //Class initialization
  function dtitlastprojectcollection($name,$id)
  {
    $this->dbcollection($name,$id);
    $this->type = "dtitlastprojectcollection";

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "Image path",
        "type"      => "dialog",
        "dialog"    => "?module=dialogs&action=filex&site_id=".$GLOBALS['site_id']."&view=thumbs",
        "dialogw"   => "600"
      ),

      "it1"    => Array(
        "label"     => "Title",
        "size"      => "35",
        "type"      => "str"
      ),

  	  "it2"    => Array(
        "label"     => "Link:",
        "type"      => "dialog",
        "dialog"    => "?module=dialogs&action=links&site_id=".$GLOBALS['site_id'],
      ),

      "it3"    => Array(
        "label"     => "Inside Image path",
        "type"      => "dialog",
        "dialog"    => "?module=dialogs&action=filex&site_id=".$GLOBALS['site_id']."&view=thumbs",
        "dialogw"   => "600"
      ),

      "it4"        => Array(
      "label"     => "Open links in:",
      "type"      => "list",
      "lookup"    => array("0:the same window","1:new window")
    ),





    );

    //Collection display table definition
    $this->columns = Array(

      "col0"        => Array(
        "width"     => "40%",
        "title"     => "Image path"
      ),

      "col1"        => Array(
        "width"     => "30%",
        "title"     => "Title"
      ),

      "col2"        => Array(
        "width"     => "30%",
        "title"     => "Link"
      ),




    );

    $this->postInit();


  }

  function GetDBData()
  {
    if(!$this->name)
    {
      $this->error = true;
      return false;
    }
    $data = sqlQueryData("SELECT * FROM `".$this->table."` ORDER BY ind DESC");
    return $data;
  }






}

?>
