<?
/*
 * Datateks.lv Constructor component
 * Created by Raivo Fismeisters, 2010, raivo@datateks.lv
 * Project:
*/

include_once('class.dbcollection.php');

class pollchoicescollection extends dbcollection
{
  function pollchoicescollection($name, $id)
  {
    $this->DBcollection($name, $id);
    $this->type = "pollchoicescollection";

    $this->properties_assoc = Array(
      "question_id" => Array(
        "label"     => "Question:",
        "type"      => "relation",
        "related_collection" => "pollscollection",
        "relation_format" => "{%question_lv%}",
        "column_index" => 1,
        "column_type" => "int(10)"
      ),

      "choice" => Array(
        "label"     => "Choice:",
        "type"      => "str",
        "multilang" => true
      ),

      "votes" => Array(
        "label"     => "Votes:",
        "type"      => "label",
        "column_type" => "int(10)"
      ),

    );

    $this->columns = Array(
      "choice_lv" => array('title' => 'Choice'),

      "question_id"     => Array(
        "title"     => "Question:"
      ),

      "votes" => array('title' => 'Votes')

    );

    $this->postInit();
  }

  function getChoicesByQuestion($question_id)
  {
    return sqlQueryData("SELECT * FROM `".$this->table."` WHERE question_id=".$question_id.' ORDER BY ind');
  }

  function addVote($choice_id)
  {
    sqlQuery('UPDATE '.$this->table.' SET votes=votes+1 WHERE item_id='.$choice_id);
  }

}

?>