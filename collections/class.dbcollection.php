<?php

use Constructor\App;

include_once("class.collection.php");
class dbcollection extends Collection{

  var $name;
  var $longname;
  var $site_id;
  var $type;
  var $collection_id;
  var $properties;
  var $columns;
  var $description;
  var $colproperties;
  var $error;
  var $table;

  //Class initialization
  function dbcollection($name, $id){

    $this->name = $name;
    $this->site_id = App::$app->getSiteId();

    $this->type = $this->type ? $this->type : "dbcollection";

    if(!$id && $name){
        $id = $this->FindCollection($name);
    }

    $this->collection_id = intval($id);

    if(!$name && $id){
    	$this->name = $this->GetName();
    }

    $this->properties = Array();
    $this->columns = Array();

    $this->table = ($this->site_id && $this->name) ? strtolower($this->site_id."_coltable_".$this->name) : false;

    $this->colproperties = Array();

  }

	function postInit(){

		// do me once
  	if($this->postInited === true) return;

    if ($this->properties_assoc){

      $this->properties = array();
      $i = 0;
      foreach ($this->properties_assoc as $key => $prop){
        $prop['column_name'] = $key;
        $this->properties['it'.$i] = $prop;
        $i++;
      }

    }

    $this->addLanguageColumns();
    if($GLOBALS['developmode'] && $this->table){

    	$check = DB::GetTable("SHOW TABLES LIKE '" . $this->table . "'");
			if(count($check)){ // check if table is already created

	      $data = sqlQueryData('DESCRIBE `' . $this->table . '`');

	      if($data){
	        $f = 0;
	        foreach($this->properties as $property){
	          $colname = 'col' . $f;
	          $f++;
	          if($property['column_name'])
	            $colname = $property['column_name'];
	          $hascol = false;
	          foreach($data as $row){
	            if($row['Field'] == $colname){
	            	$hascol = true;
	            }

	          }

						// add missing columns
	          if(!$hascol){

	            if(!$property['column_type']){
	            	$coltype = 'MEDIUMTEXT';
	            }else{
	            	$coltype = $property['column_type'];
	            }

							if (isset($property['default_value'])){
								if (isset($property['column_type']) and in_array($property['column_type'],array('tinyint','smallint','mediumint','int','bigint','integer'))){
									$coltype .= ' DEFAULT '.$property['default_value'];
								}
								else{
									$coltype .= " DEFAULT '".$property['default_value']."'";
								}
							}

	            $null = $property['not_null'] ? "NOT NULL" : "NULL";

	            $query = 'ALTER TABLE `'.$this->table.'` ADD `'.$colname.'` '.$coltype.' '.$null;

	            sqlQuery($query);

						}

	        }

	        // drop removed columns
					foreach($data as $entry){

	         	if(in_array($entry['Field'], array("item_id", "ind"))) continue;

						$found = false;
						foreach($this->properties as $property){
							if($property['column_name'] == $entry['Field']){ // we found column
								$found = true;
								break;
							}
						}

						if(!$found){ // column is missing in collection definition, remove from DB
	           	DB::Query("ALTER TABLE `".$this->table."` DROP COLUMN `".$entry['Field']."`");
						}

					}

	      }
      }

    }

    $this->postInited = true;

  }

  function CanBeRelated()
  {
    return true;
  }


  function addLanguageColumns()
  {
    if(isset($GLOBALS['language_shorname_list']))
      $langs = $GLOBALS['language_shorname_list'];
    else
    {
      $langs = sqlQueryColumn('SELECT shortname FROM '.$this->site_id.'_languages ORDER BY is_default DESC');
      $GLOBALS['language_shorname_list'] = $langs;
    }

    $props = Array();
    $n = 0;
    foreach($this->properties as $key => $prop)
    {
      if($prop['multilang'])
      {
        foreach($langs as $lang)
        {
          $props['it' . $n] = $prop;
          $props['it' . $n]['label'] = '(' . $lang . ') ' . $props['it' . $n]['label'];
          $props['it' . $n]['original_column_name'] = $props['it' . $n]['column_name'];
          $props['it' . $n]['column_name'] .= '_' . $lang;
          $n++;
        }
      }else
      {
        $props['it' . $n] = $prop;
        $n++;
      }
    }
    $this->properties = $props;

    $cols = Array();
    foreach($this->columns as $key => $col)
    {
      if($col['multilang'])
      {
        foreach($langs as $lang)
        {
          $cols[$key . '_' . $lang] = $col;
          $cols[$key . '_' . $lang]['original_column_name'] = $key;
        }
      }else
      {
        $cols[$key] = $col;
      }
    }
    $this->columns = $cols;
  }





  function GetDBData($query_params = Array(), $returnresource = false)
  {
    if(!$this->name)
    {
      $this->error = true;
      return false;
    }

    if(isset($query_params['what']))
      $what = $query_params['what'];
    else
      $what = '*';
    if(isset($query_params['table']))
      $table = $query_params['table'];
    else
      $table = '`' . $this->table . '`';
    if(isset($query_params['where']))
      $where = $query_params['where'];
    else
      $where = '1';
    if(isset($query_params['order']))
      $order = $query_params['order'];
    else
      $order = 'ind';
    if(isset($query_params['offset']))
      $offset = $query_params['offset'];
    else
      $offset = '0';
    if(isset($query_params['count']))
      $count = $query_params['count'];
    else
      $count = '20000000';

    $joins = array();
    if(isset($query_params['joins'])){

      foreach($query_params['joins'] as $join){

        $as = $join["as"]? $join["as"] : getFirstCharFromString($join['table']);
        $joins[] = ' LEFT JOIN `'.$join['table'].'` as '.$as.' ON '.$join['on'];

      }
    }

    $pdo_params = $query_params['params'] ? $query_params['params'] : array();

    $query = "SELECT ".$what." FROM ".$table." ".implode(" ", $joins)." WHERE ".$where." ORDER BY ".$order." LIMIT ".$offset.",".$count;
    if($returnresource)
      $data = sqlQuery($query, $pdo_params);
    else
    if(isset($query_params['assoc']) && $query_params['assoc'])
      $data = sqlQueryDataAssoc($query, $pdo_params);
    else
      $data = sqlQueryData($query, $pdo_params);
    return $data;
  }

  function OpenQuery($query_params = Array())
  {
    return $this->GetDBData($query_params, true);
  }

  function FetchRow($resource)
  {
    return db_fetch_array_assoc($resource);
  }

  function GetDBRecordPosition($query_params, $recpos)
  {
    sqlQuery('SET @ord=NULL');
    $pos = sqlQueryValue('SELECT ord FROM (SELECT item_id, @ord:= IFNULL(@ord + 1, 0) AS ord FROM `'.$this->table.'` WHERE '.$query_params['countwhere'].' ORDER BY '.$query_params['order'].') AS subtable WHERE item_id = ' . $recpos);
    return $pos;
  }

  function collectionExists($name)
  {
    return (bool)sqlQueryRow("SELECT collection_id FROM ".$this->site_id."_collections WHERE name='".$name."'");
  }

  function AddNewCollection($name){

    $row = sqlQueryRow("SELECT collection_id FROM ".$this->site_id."_collections WHERE name = :name", array(":name" => $name));
    if ($row){
        $this->collection_id = $row['collection_id'];
        return $row['collection_id'];
    }

    //Create table
    $datacols = '';
    $indexes = '';
    for($f = 0; $f < count($this->properties); $f++)
    {   
			if(!$this->properties['it' . $f]['column_type'])
        $coltype = 'MEDIUMTEXT';
      else
        $coltype = $this->properties['it' . $f]['column_type'];

      if($this->properties['it' . $f]['column_name'])
      {
        $colname = $this->properties['it' . $f]['column_name'];
      }else
      {
        $colname = 'col' . $f;
      }
			
			if (isset($this->properties['it' . $f]['default_value'])){
				if (isset($this->properties['it' . $f]['column_type']) and in_array($this->properties['it' . $f]['column_type'],array('tinyint','smallint','mediumint','int','bigint','integer'))){
					$coltype .= ' DEFAULT '.$this->properties['it' . $f]['default_value'];
				}
				else{
					$coltype .= " DEFAULT '".$this->properties['it' . $f]['default_value']."'";
				}
			}
			$null = $this->properties['it' . $f]['not_null'] ? "NOT NULL" : "NULL";

					
      $datacols .= "`" . $colname ."` ".$coltype." ".$null." ,";

      if($this->properties['it' . $f]['column_index'])
      {
        $indexes .= ',
                     KEY `'.$colname.'` (`'.$colname.'`)';
      }
    }
    $table = $this->site_id."_coltable_".$name;
    $table = strtolower($table);

    sqlQuery("CREATE TABLE IF NOT EXISTS `".$table."` (
               `item_id` int(32) NOT NULL auto_increment,
               `ind` int(11) NOT NULL default '0',
               ".$datacols."
               KEY `ind` (`ind`),
               PRIMARY KEY  (`item_id`)" . $indexes . "
              ) DEFAULT CHARSET utf8  ENGINE=".DATABASE_DEFAULT_TABLE_ENGINE);

    $row = sqlQueryRow("SHOW TABLES LIKE '".$table."'");
    if (!$row) return 0;

    $this->name = $name;
    $this->table = $table;
    sqlQuery("INSERT INTO ".$this->site_id."_collections (name, type) VALUES ('$name', '".$this->type."')");
    $lid = sqlQueryValue('SELECT LAST_INSERT_ID()');

    $this->collection_id = $lid;

    return $lid;
  }

  function DelCollection()
  {
    sqlQuery("DELETE FROM ".$this->site_id."_collections WHERE collection_id=".$this->collection_id);
    sqlQuery("DROP TABLE IF EXISTS `".$this->table."`");
  }

  function AddItemSlashes($item)
  {
    foreach($item as $key => $value)
    {
      $item[$key] = addslashes($value);
    }
    return $item;
  }

  function StripItemSlashes($item)
  {
    foreach($item as $key => $value)
    {
      $item[$key] = stripslashes($value);
    }
    return $item;
  }

  function Clear()
  {
    sqlQuery("DELETE FROM `".$this->table."`");
  }

  function AddNewItem($aslast = true){

    if(!$this->name){
      $this->error = true;
      return false;
    }

    if($aslast){
      $newind = 1 + sqlQueryValue("SELECT MAX(ind) FROM `".$this->table."`");
    }else{
      $newind = 1;
      sqlQuery("UPDATE `".$this->table."` SET ind=ind+1");
    }

    $f = 0;
    $cols = "";
    $data = "";
    foreach($this->properties as $key => $prop){
      if($cols)
        $cols .= ",";
      if($prop['column_name'])
        $cols .= '`' . $prop['column_name'] . '`';
      else
        $cols .= "col" . $f;
      if($data)
        $data .= ",";
      $data .= "''";
      $f++;
    }
    sqlQuery("INSERT INTO `".$this->table."` (".$cols.", ind) VALUES (".$data.", '".$newind."')");

    return $newind;
  }

  function AddItem($properties)
  {
    if(!$this->name)
    {
      $this->error = true;
      return false;
    }
    $newind = 1 + sqlQueryValue("SELECT MAX(ind) FROM `".$this->table."`");
    $f = 0;
    $cols = "";
    $data = "";

    foreach($this->properties as $key => $prop)
    {
      if($cols)
        $cols .= ",";

      if($prop['column_name'])
        $cols .= '`' . $prop['column_name'] . '`';
      else
        $cols .= "col" . $f;
      if($data)
        $data .= ",";
      $data .= "'".$properties[$f]."'";
      $f++;
    }
    sqlQuery("INSERT INTO `".$this->table."` (".$cols.", ind) VALUES (".$data.", '".$newind."')");

    return $newind;
  }

  function CleanAssocArray($properties)
  {
    $allowed = Array();
    $f = 0;
    foreach($this->properties as $prop)
    {
      if($prop['column_name'])
        $allowed[] = $prop['column_name'];
      else
        $allowed[] = 'col' . $f;
      $f++;
    }
    if($properties)
    {
      foreach($properties as $key => $value)
      {
        if(!in_array($key, $allowed) || $key=="0") //key=="0" = haks - kaut kaadu iemeslu deelj in_array uz 0 nereagjee
        {
          unset($properties[$key]);
        }
      }
    }
    return $properties;
  }

	function processPropertyDataTypes($properties){

    foreach($properties as $key => $value){

    	if(!$this->properties_assoc[$key]) continue;

			if($this->properties_assoc[$key]['column_type'] == 'date' && !$value){

      	$properties[$key] = $this->properties_assoc[$key]['not_null'] ? '1970-01-01' : NULL;

			}elseif($this->properties_assoc[$key]['column_type'] == 'datetime' && !$value){

      	$properties[$key] = $this->properties_assoc[$key]['not_null'] ? '1970-01-01 00:00:00' : NULL;

			}elseif($this->properties_assoc[$key]['column_type'] == 'file' && !$value){

      	$properties[$key] = $this->properties_assoc[$key]['not_null'] ? '' : NULL;

			}elseif(in_array($this->properties_assoc[$key]['column_type'], array('integer', 'int', 'float', 'tinyint', 'tinyint(1)', 'bigint', 'int(32)', 'decimal(10,2)', 'decimal(10,3)')) && !$value){

      	$properties[$key] = 0;

      }elseif($this->properties_assoc[$key]['not_null'] && is_null($value)){

      	$value = "";

			}

    }

  	return $properties;

  }

  //backwards compatibility
  function AddItemAsoc($properties)
  {
    return $this->AddItemAssoc($properties);
  }

  //properties = array('colname' => 'value', 'colname' => 'value', ...)
  function AddItemAssoc($properties, $aslast = true){

    if(!$this->name){
      $this->error = true;
      return false;
    }

    if($aslast){
      $newind = 1 + DB::GetValue("SELECT MAX(ind) FROM `".$this->table."`");
    }else{
      $newind = 1;
      DB::Query("UPDATE `".$this->table."` SET ind=ind+1");
    }

		$properties['ind'] = $newind;

		$id = $this->Insert($properties);

		return $newind;

  }

  //backwards compatibility
  function ChangeItemAsoc($ind, $properties)
  {
    return $this->ChangeItemAssoc($ind, $properties);
  }

  //properties = array('colname' => 'value', 'colname' => 'value', ...)
  function ChangeItemAssoc($ind, $properties)
  {
    $properties = $this->CleanAssocArray($properties);
    if ($this->doAutoLastUpdate())
    {
      $properties['last_update'] = time();
      $this->reindex();
    }


    $data = '';
    foreach($properties as $pname => $pval)
    {
      if($data)
        $data .= ', ';
      $data .= '`' . $pname . '`="' . $pval . '"';
    }
    if($data)
      sqlQuery("UPDATE `".$this->table."` SET ".$data." WHERE ind='".$ind."'");

    return $ind;
  }

  //backwards compatibility
  function ChangeItemByIdAsoc($id, $properties)
  {
    return $this->ChangeItemByIdAssoc($id, $properties);
  }

  function ChangeItemByIdAssoc($id, $properties)
  {

    $properties = $this->CleanAssocArray($properties);
    if ($this->doAutoLastUpdate())
    {
      $properties['last_update'] = time();
      $this->reindex();
    }

    DB::Update($this->table, ['item_id' => $id], $properties);

    return $id;

  }

  function ChangeItemByIdAssoc2($id, $properties)
  {
    $properties = $this->CleanAssocArray($properties);
    if ($this->doAutoLastUpdate())
    {
      $properties['last_update'] = time();
      $this->reindex();
    }

    $data = '';
    foreach($properties as $pname => $pval)
    {
      if($data)
        $data .= ', ';
      $data .= '`' . $pname . '`="' . $pval . '"';
    }
    if($data)
      sqlQuery("UPDATE `".$this->table."` SET ".$data." WHERE item_id='".$id."'");
    return $id;
  }

  function AddNewItemTop()
  {
    return $this->AddNewItem(false);
  }

  function ChangeItemProperty($ind, $property_num, $property_val)
  {
    if(!$this->name)
    {
      $this->error = true;
      return false;
    }
    if($this->properties['it' . $property_num]['column_name'])
    {
      sqlQuery("UPDATE `".$this->table."` SET ".$this->properties['it' . $property_num]['column_name']."='".$property_val."' WHERE ind='".$ind."'");
    }
    else
      sqlQuery("UPDATE `".$this->table."` SET col".$property_num."='".$property_val."' WHERE ind='".$ind."'");
  }

  function ChangeDBItemProperty($item_id, $property_num, $property_val)
  {
    if(!$this->name)
    {
      $this->error = true;
      return false;
    }
    if($this->properties['it' . $property_num]['column_name'])
      sqlQuery("UPDATE `".$this->table."` SET ".$this->properties['it' . $property_num]['column_name']."='".$property_val."' WHERE item_id='".$item_id."'");
    else
      sqlQuery("UPDATE `".$this->table."` SET col".$property_num."='".$property_val."' WHERE item_id='".$item_id."'");
  }

  //properties = array(value1, value2, ... )
  function ChangeItem($ind, $properties)
  {
    if(!$this->name)
    {
      $this->error = true;
      return false;
    }
    $f = 0;
    $cols = '';
    $data = '';
    foreach($properties as $property)
    {
      if($data)
        $data .= ",";
      if($this->properties['it' . $f]['column_name'])
        $data .= '`' . $this->properties['it' . $f]['column_name'].'`'."='".$property."'";
      else
        $data .= "col".$f."='".$property."'";
      $f++;
    }
    sqlQuery("UPDATE `".$this->table."` SET ".$data." WHERE ind='".$ind."'");
    return $ind;
  }

  //properties = array(value1, value2, ... )
  function ChangeItemByID($id, $properties)
  {
    if(!$this->name)
    {
      $this->error = true;
      return false;
    }
    $f = 0;
    $cols = '';
    $data = '';
    foreach($properties as $property)
    {
      if($data)
        $data .= ",";
      if($this->properties['it' . $f]['column_name'])
        $data .= '`' . $this->properties['it' . $f]['column_name']."`='".$property."'";
      else
        $data .= "col".$f."='".$property."'";
      $f++;
    }
    sqlQuery("UPDATE `".$this->table."` SET ".$data." WHERE item_id='".$id."'");
    return $id;
  }

  function DelItem($ind)
  {
    if(!$this->name)
    {
      $this->error = true;
      return false;
    }
    sqlQuery("DELETE FROM `".$this->table."` WHERE ind='$ind'");
    sqlQuery("UPDATE `".$this->table."` SET ind=ind-1 WHERE ind>'$ind'");
  }

  function DelDBItem($item_id)
  {
    if(!$this->name)
    {
      $this->error = true;
      return false;
    }
    $ind = sqlQueryValue("SELECT ind FROM `".$this->table."` WHERE item_id='$item_id'");
    sqlQuery("DELETE FROM `".$this->table."` WHERE item_id='$item_id'");
    sqlQuery("UPDATE `".$this->table."` SET ind=ind-1 WHERE ind>'$ind'");
  }

  function GetItem($ind)
  {
    if(!$this->name)
    {
      $this->error = true;
      return false;
    }
    $row = sqlQueryRow("SELECT * FROM `".$this->table."` WHERE ind='$ind'");
    $a = Array();
    $f = 0;
    for($f = 0; $f < count($row); $f++)
    {
      $colname = 'col'.$f;
      if($this->properties['it' . $f]['column_name'])
        $colname = $this->properties['it' . $f]['column_name'];
      if(isset($row[$colname]))
      {
        $a[$f] = $row[$colname];
      }
    }
    return $a;
  }

  function GetItemByID($item_id)
  {
    if(!$this->name)
    {
      $this->error = true;
      return false;
    }
    $row = sqlQueryRow("SELECT * FROM `".$this->table."` WHERE item_id='$item_id'");
    $a = Array();
    $f = 0;
    for($f = 0; $f < count($row); $f++)
    {
      $colname = 'col'.$f;
      if($this->properties['it' . $f]['column_name'])
        $colname = $this->properties['it' . $f]['column_name'];
      if(isset($row[$colname]))
      {
        $a[$f] = $row[$colname];
      }
    }
    return $a;
  }

  function GetItemByIDAssoc($item_id, $justassoc = false)
  {
    if(!$this->name)
    {
      $this->error = true;
      return false;
    }
    if($justassoc)
      $row = sqlQueryRowAssoc("SELECT * FROM `".$this->table."` WHERE item_id='$item_id'");
    else
      $row = sqlQueryRow("SELECT * FROM `".$this->table."` WHERE item_id='$item_id'");
    return $row;
  }

  function GetItemID($ind)
  {
    if(!$this->name)
    {
      $this->error = true;
      return false;
    }
    $item_id = sqlQueryValue("SELECT item_id FROM `".$this->table."` WHERE ind='$ind'");
    return $item_id;
  }

  function GetItemInd($id)
  {
    if(!$this->name)
    {
      $this->error = true;
      return false;
    }
    $ind = sqlQueryValue("SELECT ind FROM `".$this->table."` WHERE item_id='$id'");
    return $ind;
  }

  function ExistsItem($ind)
  {
    if(!$this->name)
    {
      $this->error = true;
      return false;
    }
    $i = sqlQueryValue("SELECT ind FROM `".$this->table."` WHERE ind='$ind'");
    if($i)
      return true;
    else
      return false;
  }

  function FirstItem()
  {
    if(!$this->name)
    {
      $this->error = true;
      return false;
    }
    $i = sqlQueryValue("SELECT Min(ind) FROM `".$this->table."` LIMIT 1");
    return $i;
  }

  function LastItem()
  {
    if(!$this->name)
    {
      $this->error = true;
      return false;
    }
    $i = sqlQueryValue("SELECT Max(ind) FROM `".$this->table."` LIMIT 1");
    return $i;
  }
  
  function LastItemById()
  {
    if(!$this->name)
    {
      $this->error = true;
      return false;
    }
    $i = sqlQueryValue("SELECT Max(item_id) FROM `".$this->table."` LIMIT 1");
    return $i;
  }

  function ItemCount($where = '')
  {
    if(!$this->name)
    {
      $this->error = true;
      return false;
    }
    if(!$where)
      $where = 1;
    $i = sqlQueryValue("SELECT Count(*) FROM `".$this->table."` WHERE " . $where);
    return $i;
  }

  function SwapItems($ind1, $ind2)
  {
    if(!$this->name)
    {
      $this->error = true;
      return false;
    }
    if($ind1 > 0 && $ind2 > 0)
    {
      sqlQuery("DELETE FROM `".$this->table."` WHERE ind='-1'");
      sqlQuery("UPDATE `".$this->table."` SET ind='-1' WHERE ind='$ind1'");
      sqlQuery("UPDATE `".$this->table."` SET ind='$ind1' WHERE ind='$ind2'");
      sqlQuery("UPDATE `".$this->table."` SET ind='$ind2' WHERE ind='-1'");
    }
  }

  function InitializeSubRelatedCollection($fieldname){

		$field = $this->columns[$fieldname];

    if(!$field) return false;

    $colid = $this->getProperty('relation_' . $this->name . '_' . $fieldname);

    if($colid){
      $type = $field['subrelated_collection'];
    	require_once($GLOBALS['cfgDirRoot'] . "collections/class.".$type.".php");
			$colname = '';
      $collection = new $type($colname,$colid);
			if($collection->name){
        return $collection;
      }else{
        return false;
      }

    }

  }

    function InitializeRelatedCollection($fieldname, $coltype = false)
    {

        $coltype = $coltype ? $coltype : $this->properties_assoc[$fieldname]['related_collection'];
        $type = $coltype;

        $property_name = 'relation_' . $this->name . '_' . $fieldname;

        $colid = $this->getProperty($property_name);
        if(!$colid){ // we dont have collection_id set, but maybe we can find/create one?

            $collections = DB::GetTable("SELECT * FROM ".$this->site_id."_collections WHERE `type` = :type", array(":type" => $type));

            if(count($collections) == 1){ // we have only one collection of this type.. lets assume it's THE ONE!
                $this->setProperty($property_name, $collections[0]['collection_id']);
                $colid = $collections[0]['collection_id'];
            }elseif(count($collections) == 0){ // ooo.. we dont have any collections of this type.. let's auto-create one!

                require_once($GLOBALS['cfgDirRoot'] . "collections/class." . $type . ".php");
                $colname = strtolower($type);
                $collection = new $type($colname, 0);
                $collection->addNewCollection($colname);
                $colid = $collection->collection_id;
                $this->setProperty($property_name, $colid);

            }

        }

        if($colid){

            include_once($GLOBALS['cfgDirRoot'] . "collections/class.".$type.".php");
            $colname = '';
            $collection = new $type($colname,$colid);

            if($collection->name){
                return $collection;
            }else{
                return false;
            }
        } else{
            return false;
        }

    }

  //returns $this->properties but swaps key indexes it0, it1, ... to named column values if specified
  function GetDBProperties()
  {

    $properties = $this->properties;
    $f = 0;
    foreach($properties as $key => $property)
    {
      if($property['column_name'])
      {
        $property['old_key'] = $key;
        $half1 = array_slice($properties, 0, $f);
        $half2 = array_slice($properties, $f+1);
        $properties = array_merge($half1, array($property['column_name'] => $property), $half2);
        $key = $property['column_name'];
      }

      if($property['type'] == 'relation')
      {

        $relcol = $this->InitializeRelatedCollection($key, $property['related_collection']);


        if($GLOBALS['user_site_permissions_managetemplates'])
          $properties[$key]['onclick'] = "javascript:openDialog('?module=collections&action=setrelatedcollection&site_id=".$this->site_id."&sourcecol=".$this->collection_id."&sourcecoltype=".$this->type."&relatedcoltype=".$property['related_collection']."&fieldname=".$key."&relatedcolid=".$relcol->collection_id."', 400, 550, 1, 0);";

        if($relcol && $relcol->CanBeRelated())
        {
          //create lookup with values from related collection
          $properties[$key]['type'] = 'list';
          $properties[$key]['lookup'] = $relcol->GetIdSelectionLookup($property['relation_format'], $property['value']);
          if(!$property['value'] && $_SESSION['collections_viewfilter_' . $this->name][$key])
          {
            $properties[$key]['value'] = $_SESSION['collections_viewfilter_' . $this->name][$key];
          }

        }else
        {
          //create dialog, allowing to specify related collection
          $properties[$key]['type'] = 'scriptdialog';
        }
      }
      $f++;
    }
    $this->processDBProperties($properties);
    return $properties;
  }

  function ExtractFieldsFromFormat($format)
  {
    preg_match_all('/{%(.*?)%}/s', $format, $matches);
    $fieldarr = $matches[1];
    return $fieldarr;
  }

  function GetIdSelectionLookup($format, $value)
  {
    if(!$format)
    {
      $property = $this->properties['it0'];
      if($property['column_name'])
        $format = '{%' . $property['column_name'] . '%}';
      else
        $format = '{%col0%}';
    }

    $fieldarr = $this->ExtractFieldsFromFormat($format);
    $fields = 'item_id';
    foreach($fieldarr as $field)
    {
      $fields .= ',';
      $fields .= $field;
      $format = str_replace('{%' . $field . '%}', '%s', $format);
    }
    $query_params = Array('what' => $fields, 'count' => 100000);
    $data = $this->GetDBData($query_params);
    $lookup = Array();

    $foundvalue = false;
    foreach($data as $row)
    {
      $items = Array();
      foreach($fieldarr as $field)
      {
        $items[] = $row[$field];
      }
      $lookup[] = $row['item_id'] . ':' . vsprintf($format, $items);
      if($row['item_id'] == $value)
        $foundvalue = true;
    }

    if(!$foundvalue)
    {
      if($value || is_numeric($value))
      {
        $val = '&lt;' . $value . '&gt;';
      }else
      {
        $val = '&lt;empty&gt;';
      }
      $lookup = array_merge(Array($value . ':' . $val), $lookup);
    }

    return $lookup;

  }

  function GetItemAssocFromPost($properties = false)
  {
    if(!$properties)
      $properties = $this->getDBProperties();
    $newproperties = Array();
    $files = Array();
    foreach($properties as $key => $property)
    {
      //fix collections without column_name(s)
      if($property['column_name'])
      {
        $newkey = $key;
      }else
      {
        $newkey = str_replace('it', 'col', $key);
      }

      if($property['postkey'])
        $key = $property['postkey'];

      if($property['type'] != 'file')
      //set property to posted value for non-uploads
      {
        $newproperties[$newkey] = $_POST[$key];
      }
      else
      //process automatic file uploads
      {     
        if($property['upload_dir'])
          $upload_dir = $property['upload_dir'];
        else
          $upload_dir = 'upload/' . $this->name . '/';

        //depending on user's settings we either upload file or use link to file
        if($_POST[$key . '_file_select_mode'] == 'upload' || $property['uploadonly'])
        {
          require_once($GLOBALS['cfgDirRoot']."library/"."func.files.php");
          forceDirectories($this->site_id, $upload_dir);
          $up_data = uploadSiteFile($this->site_id, $_FILES[$key], $upload_dir);
          if($up_data)
          {
            $files[$newkey] = fileDuplicate($this->site_id, $up_data['path']);
            $newproperties[$newkey] = $up_data['url'];
            if($property['resize_mode'])
            {
              resizeImage($up_data['path'], $property['resize_width'], $property['resize_height'], $property['resize_mode']);
            }
          }
        }else
        //user selected link to file
        {
          $newproperties[$newkey] = $_POST[$key . '_link'];
        }
      }
    }
    //assign and resize uploaded files to fields with copy_file
    foreach($properties as $key => $property)
    {
      if($property['column_name'])
      {
        $newkey = $key;
      }else
      {
        $newkey = str_replace('it', 'col', $key);
      }

      if($property['postkey'])
        $key = $property['postkey'];

      if($property['copy_file'] && $files[$property['copy_file']])
      {
        forceDirectories($this->site_id, $property['upload_dir']);
        $filedata = fileDuplicate($this->site_id, $files[$property['copy_file']]['path'], '', $property['upload_dir']);
        if($filedata)
        {
          $newproperties[$newkey] = $filedata['url'];
          if($property['resize_mode'])
          {
            resizeImage($filedata['path'], $property['resize_width'], $property['resize_height'], $property['resize_mode']);
          }
        }
      }
    }
    //remove temp file copies
    foreach($files as $filedata)
    {
      unlink($filedata['path']);
    }
    return $newproperties;
  }

  //compatibility misspelling
  function LoadItemInDBPorperties(&$properties, $item)
  {
    $this->LoadItemInDBProperties($properties, $item);
  }

  //item can be obtained using $this->getItem for an example
  function LoadItemInDBProperties(&$properties, $item)
  {
    $keys = array_keys($properties);
    foreach($item as $key => $value)
    {
      if (isset($properties[$keys[$key]]))
        $properties[$keys[$key]]['value'] = $value;

    }
  }


  //filters and sorts to show specific fields only
  function FilterDBProperties(&$properties, $fields)
  {
    $props = Array();
    foreach($fields as $field)
    {
      if(isset($properties[$field]))
      {
        $props[$field] = $properties[$field];
      }
    }
    $properties = $props;
  }


  //filters and sorts to show specific fields only
  function HideDBProperties(&$properties, $fields)
  {
    foreach($fields as $field)
    {
      if(isset($properties[$field]))
      {
        $properties[$field]['type'] = 'hidden';
      }
    }
  }


  //creates array for form generator, containing all collection fields
  //if id is specified then field values are filled with data from corresponding row
  function getFormData($id = 0)
  {
    $form_data = $this->properties;

    if($id)
    {
      $item = $this->getItemByID($id);
      foreach($item as $key => $value)
      {
        $form_data["it".$key]['value'] = $value;
      }
    }
    return $form_data;
  }

  function GetDisplayColumns()
  {
    $properties = $this->GetDBProperties();
    $cols = $this->columns;
    foreach($cols as $key => $colproperty)
    {
      if($colproperty['subrelated_collection'])
      {
        $relcol = $this->InitializeRelatedCollection($key, $colproperty['subrelated_collection']);

        $cols[$key]['popup'] = "?module=collections&action=setrelatedcollection&site_id=".$this->site_id."&sourcecol=".$this->collection_id."&sourcecoltype=".$this->type."&relatedcoltype=".$colproperty['subrelated_collection']."&fieldname=".$key."&relatedcolid=".$relcol->collection_id;
        if($relcol && $cols[$key]['format'])
          $cols[$key]['format'] = "<a href=\"?module=collections&site_id=".$this->site_id."&action=editcollection&id=".$relcol->collection_id."&justedit=1&setfiltervalue={%item_id%}&setfilterfield=".$colproperty['subrelated_field']."\">" . $cols[$key]['format'] . "</a>";
      }

      if($properties[$key]['related_collection'])
      {
        $relcol = $this->InitializeRelatedCollection($key, $properties[$key]['related_collection']);

        if($GLOBALS['user_site_permissions_managetemplates'])
          $cols[$key]['popup'] = "?module=collections&action=setrelatedcollection&site_id=".$this->site_id."&sourcecol=".$this->collection_id."&sourcecoltype=".$this->type."&relatedcoltype=".$properties[$key]['related_collection']."&fieldname=".$key."&relatedcolid=".$relcol->collection_id;

        if($cols[$key]['format'])
          $format = $cols[$key]['format'];
        else
          $format = $properties[$key]['relation_format'];
        if($relcol && $format)
        {
          $cols[$key]['format'] = "<a href=\"?module=collections&site_id=".$this->site_id."&action=filtercollectionclear&category_id=".$relcol->collection_id."&coltype=".$relcol->type."&justedit=".$_GET['justedit']."&main=1&selrow={%".$key."%}\">" . $format . "</a>";
        }
      }

      if($colproperty['format'])
      {
        //checkboxes
        while (preg_match("/\[check (.*?) (.*?)\]/", $cols[$key]['format'], $regs)) {
          $method_name = $regs[1];
          $truecond = $regs[2];
          $cols[$key]['format'] = str_replace($regs[0], '<input type="checkbox" [if '.$truecond.'] checked="checked"[/if] onclick="window.location=\'?module=collections&site_id='. $this->site_id . '&page_id=' . $this->page_id . '&action=execcolmethod&method_name=' . $method_name . '&category_id=' . $this->collection_id . '&coltype='.$this->type.'&justedit='.$_GET['justedit'].'&session_id='.session_id().'&session_name='.session_name().'&main=1&selrow={%item_id%}&id={%item_id%}&checked=\' + this.checked;" />', $cols[$key]['format']);
        }

        //action links
        while (preg_match("/\[ahref (.*?)\](.*?)\[\/ahref\]/", $cols[$key]['format'], $regs)) {
          $method_name = $regs[1];
          $urltext = $regs[2];
          $cols[$key]['format'] = str_replace($regs[0], '<a href="?module=collections&site_id='. $this->site_id . '&page_id=' . $this->page_id . '&action=execcolmethod&method_name=' . $method_name . '&category_id=' . $this->collection_id . '&coltype='.$this->type.'&justedit='.$_GET['justedit'].'&session_id='.session_id().'&session_name='.session_name().'&main=1&selrow={%item_id%}&id={%item_id%}">' . $urltext . '</a>', $cols[$key]['format']);
        }

        //dialog links
        while (preg_match("/\[adlg (.*?)\](.*?)\[\/adlg\]/", $cols[$key]['format'], $regs)) {
          $method_name = $regs[1];
          $urltext = $regs[2];
          $cols[$key]['format'] = str_replace($regs[0], '<a href="javascript:void(0);" onclick="javascript:openDialog(\'?module=collections&action=dlgcolmethod&method_name='.$method_name.'&site_id='. $this->site_id . '&page_id=' . $this->page_id . '&category_id=' . $this->collection_id . '&coltype='.$this->type.'&session_id='.session_id().'&session_name='.session_name().'&selrow={%item_id%}&id={%item_id%}\', 400, 460, 1, 1); return false;">' . $urltext . '</a>', $cols[$key]['format']);
        }

        //edit links
        while (preg_match("/\[edit\](.*?)\[\/edit\]/", $cols[$key]['format'], $regs)) {
          $urltext = $regs[1];
          $cols[$key]['format'] = str_replace($regs[0], '<a href="javascript:void(0);" onclick="openDialog(\'?module=collections&action=editcolitem&site_id='. $this->site_id . '&page_id=' . $this->page_id . '&category_id=' . $this->collection_id . '&coltype='.$this->type.'&session_id='.session_id().'&session_name='.session_name().'&id={%item_id%}&selrow={%item_id%}\', 400, 460, 1, 1);">' . $urltext . '</a>', $cols[$key]['format']);
        }
      }


    }
    return $cols;
  }

  function MoveItemUp($ind, $where = '1')
  {
    $ind = intval($ind);
    if($ind > 1)
    {
      $indabove = sqlQueryValue('SELECT ind FROM `' . $this->table . '` WHERE ind < ' . $ind . ' AND ('.$where.') ORDER BY ind DESC LIMIT 1');
      if($indabove > 0)
      {
        $this->SwapItems($ind, $indabove);
        return $indabove;
      }
    }
    return $ind;
  }

  function MoveItemDown($ind, $where = '1')
  {
    $ind = intval($ind);
    if($ind)
    {
      $indbelow = sqlQueryValue('SELECT ind FROM `' . $this->table . '` WHERE ind > ' . $ind . ' AND ('.$where.') ORDER BY ind ASC LIMIT 1');
      if($indbelow > $ind)
      {
        $this->SwapItems($ind, $indbelow);
        return $indbelow;
      }
    }
    return $ind;
  }

  function doAutoLastUpdate()
  {
    return false;
  }

  function indexAtPage($page_id)
  {
    static $rels = Array();

    $ckey = $this->site_id.'_'.$page_id.'_'.$this->collection_id;
    if (isset($rels[$ckey])) return;

    if (sqlQueryValue('SELECT id FROM `'.$this->site_id.'_search_page_col_relation` WHERE page_id='.$page_id.' AND collection_id='.$this->collection_id))
    {
      $rels[$ckey] = true;
      return;
    }

    sqlQuery('INSERT INTO `'.$this->site_id.'_search_page_col_relation` SET page_id='.$page_id.', collection_id='.$this->collection_id);
    $rels[$ckey] = true;
  }

  var $reindexed = false;

  function reindex()
  {
    if ($reindexed) return;

    $reindexed = true;
    sqlQuery('UPDATE `'.$this->site_id.'_pages` SET mustsearchindex=1 WHERE page_id IN (SELECT page_id FROM `'.$this->site_id.'_search_page_col_relation` WHERE collection_id='.$this->collection_id.')');

  }

  /**
	* PDO Methods
	* These methods were left here for backwards compatibility
	* and are overwritten in next generation PDO collection class (class.pdocollection.php).
	*
	*/

  function Update($data, $cond){
    return DB::Update($this->table, $cond, $data);
  }

  function Insert($data){

  	static $ind;

    if(!$ind){
    	$ind = DB::GetValue("SELECT max(ind) FROM `".$this->table."`");
    }

		if(!$data['ind']){
			$data['ind'] = ++$ind;
		}

    return DB::Insert($this->table, $data);
  }

  function Truncate(){
    return DB::Truncate($this->table);
  }

  function Delete($cond){

    $cond = is_array($cond)? $cond : array(DB::GetTablePK($this->table) => $cond);

    return DB::Delete($this->table, $cond);
  }

  function GetValue($data){

    $what = isset($data['what']) ? $data['what'] : '*';
    $where = isset($data['where'])? $data['where'] : '1';
    $order = isset($data['order']) ? $data['order'] : 'ind';

    $query = "SELECT ".$what." FROM `".$this->table."` WHERE ".$where."  ORDER BY ".$order." LIMIT 1";

    return DB::GetValue($query, $data['params']);

  }

  function GetTable($data){

    $what = isset($data['what']) ? $data['what'] : '*';
    $where = isset($data['where'])? $data['where'] : '1';
    $order = isset($data['order']) ? $data['order'] : 'ind';
    $limit = isset($data['limit']) ? " LIMIT ".$data['limit'] : '';

    $query = "SELECT ".$what." FROM `".$this->table."` WHERE ".$where."  ORDER BY ".$order.$limit;

    return DB::GetTable($query, $data['params']);

  }

  function GetColumn($data){

    $return = array();

    foreach($this->GetTable($data) as $row){
      foreach($row as $key => $value){
        $return[] = $value;
      }

    }

    return ($data['unique']) ? $return : array_unique($return);

  }

  function GetRow($data){

    $what = isset($data['what']) ? $data['what'] : '*';
    $where = isset($data['where'])? $data['where'] : '1';
    $order = isset($data['order']) ? $data['order'] : 'ind';
    $limit = isset($data['limit']) ? " LIMIT ".$data['limit'] : '';

    $query = "SELECT ".$what." FROM `".$this->table."` WHERE ".$where."  ORDER BY ".$order.$limit;

    return DB::GetRow($query, $data['params']);

  }

}
