<?
include_once('class.dbcollection.php');

class EVeikalsProdChangeLog extends dbcollection{
  
  function __construct($name,$id)
  {
    $this->dbCollection($name,$id);
    $this->type = get_class();

    //Properties array definition
    $this->properties_assoc = Array(
			"prod"					=> Array(
				"label"     => "Produkts:",
				"type"      => "relation",
				"related_collection" => "shopprodcollection",
				"relation_format" => "{%name_lv%}",
				"column_type" => "int(32)",
				"column_index" => 1,
			),

			"change_type"    => Array(
       "label"     => "Izmaiņu veids:",
       "type"      => "list",
       "lookupassoc" => array("Insert" => "Insert", "Update" => "Update", "Delete" => "Delete")      
			),
      "user"    => Array(
        "label"     => "Izmaiņas veica:",
        "type"      => "text"
      ),
			"change_comment"    => Array(
        "label"     => "Izmaiņu komentārs:",
        "type"      => "str",				
      ),   
			
			"change_time"    => Array(
        "label"     => "Izmaiņu laiks:",
        "type"      => "str",
				"column_type" => "datetime",
      ),
    );

    //Collection display table definition
    $this->columns = Array(

      "prod"        => Array("title"     => "Produkts"),
			"change_type"        => Array("title"     => "Izmaiņu veids"),
			"user"        => Array("title"     => "Izmaiņas veica"),
			"change_comment"        => Array("title"     => "Izmaiņu komentārs"),
			"change_time"        => Array("title"     => "Izmaiņu laiks"),
			
    );

     $this->PostInit();
  }
	
	function CanChangeOrder(){
		return false;
	}
	
	function GetDefaultOrder()
	{
		return 'item_id DESC';
	}

	// extending parent::Insert()
  function Insert($data){

  	if(!$data['change_time'] || !verifyDate($data['change_time'])) $data['change_time'] = date("Y-m-d H:i:s");
  	if(!$data['user']) $data['user'] = $_SESSION['userdata']['username'] ? $_SESSION['userdata']['username'] : "Unknown";

  	return parent::Insert($data);

  }

  function GetEditData($page, &$count, &$recordposition, $order = ''){
    $data = parent::GetEditData($page, $count, $recordposition, $order);

    foreach ($data as $key => $row){

      $data[$key]['change_time'] = $data[$key]['change_time'] ? date("d.m.Y H:i:s", strtotime($data[$key]['change_time'])) : "";

    }

    return $data;

	}

	function EditToolbar()
  {
   $site_id = $this->site_id;
    $category_id = $this->collection_id;
    $collectiontype = $this->type;

    $Toolbar->backendroot = $GLOBALS['cfgWebRoot'];

     require($GLOBALS['cfgDirRoot']."library/"."class.toolbar2.php");
    $Toolbar = new Toolbar2();

    $Toolbar->AddSpacer();

    // $Toolbar->AddButtonImageText("add", 'collection_new', "{%langstr:add_new%}", "", "openDialog('?module=collections&action=newcolitem&site_id=$site_id&page_id=$page_id&category_id=$category_id&coltype=".$collectiontype."&session_id=".session_id()."&session_name=".session_name()."', 400, 460, 1, 1);", 85, "{%langstr:hint_new_collection_item%}");
    // $Toolbar->AddButtonImage("edit", 'collection_edit', "{%langstr:col_edit%}", "", "openDialog('?module=collections&action=editcolitem&site_id=$site_id&page_id=$page_id&category_id=$category_id&coltype=".$collectiontype."&session_id=".session_id()."&session_name=".session_name()."&id='+SelectedRowID, 400, 460, 1, 1);", 31, "{%langstr:hint_edit_collection_item%}");
    // if($this->CanChangeOrder())
    // {
      // $Toolbar->AddButtonImage("up", 'up', "{%langstr:up%}", "javascript:if(SelectedRowID)window.location='?module=collections&site_id=$site_id&page_id=$page_id&action=moveup&category_id=$category_id&coltype=".$_GET['coltype']."&justedit=".$_GET['justedit']."&session_id=".session_id()."&session_name=".session_name()."&main=1&selrow='+SelectedRowID+'&id='+SelectedRowID;", '', 31, "");
      // $Toolbar->AddButtonImage("down", 'down', "{%langstr:down%}", "javascript:if(SelectedRowID)window.location='?module=collections&site_id=$site_id&page_id=$page_id&action=movedown&category_id=$category_id&coltype=".$_GET['coltype']."&justedit=".$_GET['justedit']."&session_id=".session_id()."&session_name=".session_name()."&main=1&selrow='+SelectedRowID+'&id='+SelectedRowID;", '', 31, "");
    // }
    // $Toolbar->AddButtonImage("del", 'delete', "{%langstr:col_del%}", "javascript: if(SelectedRowID && SelectedRowID!=-1) window.location='?module=collections&action=delcolitem&site_id=$site_id&page_id=$page_id&category_id=$category_id&coltype=".$collectiontype."&justedit=".$_GET['justedit']."&session_id=".session_id()."&session_name=".session_name()."&main=1&id='+SelectedRowID; else if(!SelectedRowIDsEmpty) window.location='?module=collections&action=delcolitem&site_id=$site_id&page_id=$page_id&category_id=$category_id&coltype=".$collectiontype."&justedit=".$_GET['justedit']."&session_id=".session_id()."&session_name=".session_name()."&main=1&id='+SelectedRowIDs;", '', 31, "{%langstr:hint_delete_collection_item%}", "{%langstr:ask_delete_collection_item%}");
    // $Toolbar->AddSeperator();
    if(!is_array($_SESSION['collections_viewfilter_' . $this->name]) || !count($_SESSION['collections_viewfilter_' . $this->name]))
      $filterimg = 'collection_find';
    else
      $filterimg = 'collection_find_checked';
    $Toolbar->AddButtonImage("filter", $filterimg, "{%langstr:col_filter%}", "", "openDialog('?module=collections&action=filtercollection&site_id=$site_id&page_id=$page_id&category_id=$category_id&coltype=".$collectiontype."&session_id=".session_id()."&session_name=".session_name()."', 400, 460, 1, 1);", 31, "{%langstr:hint_filter_collection%}");
    if($filterimg == 'collection_find_checked')
    {
      $Toolbar->AddButtonImageText("remfilter", 'collection_find_clear', "{%langstr:col_showall%}", "javascript:window.location='?module=collections&action=filtercollectionclear&site_id=$site_id&page_id=$page_id&category_id=$category_id&coltype=".$collectiontype."&justedit=".$_GET['justedit']."&session_id=".session_id()."&session_name=".session_name()."&main=1'", '', 66, "{%langstr:hint_collection_find_clear%}");
    }
    if(!$_GET['justedit'])
    {
      $Toolbar->AddSeperator();
      $Toolbar->AddButtonImage("properties", 'sitemap_properties', "{%langstr:toolbar_properties%}", "", "openDialog('?module=collections&action=colproperties&site_id=$site_id&page_id=$page_id&category_id=$category_id&coltype=".$collectiontype."&session_id=".session_id()."&session_name=".session_name()."', 400, 460, 1, 1);", 31, "{%langstr:hint_collection_properties%}");
      $Toolbar->AddButtonImage("copycol", 'sitemap_copy', "{%langstr:toolbar_copy_collection%}", "", "openDialog('?module=collections&action=copycollection&site_id=$site_id&page_id=$page_id&category_id=$category_id&coltype=".$collectiontype."&session_id=".session_id()."&session_name=".session_name()."', 400, 460, 1, 1);", 31, "{%langstr:hint_copy_collection%}");

      $Toolbar->AddSeperator();
      $Toolbar->AddSeperator();
      $Toolbar->AddButtonImage("delc", 'collection_delete', "{%langstr:toolbar_delete_collection%}", "javascript: window.location='?module=collections&action=delcollection&site_id=$site_id&page_id=$page_id&category_id=$category_id&coltype=".$collectiontype."&main=1&session_id=".session_id()."&session_name=".session_name()."'", '', 31, "{%langstr:hint_delete_collection%}", "{%langstr:ask_delete_collection%}");


    }

    //$Toolbar->AddSeperator();

    $this->GetExtraToolbarButtons($Toolbar, $extraitembuttons, $extrabuttons);
    if($extraitembuttons)
    {
      foreach($extraitembuttons as $key => $item)
      {
        $extraitembuttons[$key] = '"' . $item . '"';
      }
      $extraitembuttons = ',' . implode(', ', $extraitembuttons);
    }else
    {
      $extraitembuttons = '';
    }
    if($extrabuttons)
    {
      foreach($extrabuttons as $key => $item)
      {
        $extrabuttons[$key] = '"' . $item . '"';
      }
      $extrabuttons = ',' . implode(', ', $extrabuttons);
    }else
    {
      $extrabuttons = '';
    }

    $return = $Toolbar->output();


    echo '<script language="JavaScript" type="text/javascript">
		/*<![CDATA[*/
		che = new Array("edit", "up", "down", "del"'.$extraitembuttons.');
		che2 = new Array("add","edit", "up", "down", "del", "properties", "delc", "filter", "remfilter"'.$extrabuttons.');

		DisableToolbarButtons(che);
		/*]]>*/
		</script>';

		if($_GET['selrow'])
		{
			echo '<script language="JavaScript">
						<!--
							var tablerow' . $_GET['selrow'] . ' = document.getElementById("tablerow' . $_GET['selrow'] . '");
							if(typeof(tablerow' . $_GET['selrow'] . ')!= "undefined" && tablerow' . $_GET['selrow'] . ')
							{
								preEl = tablerow' . $_GET['selrow'] . ';
								ChangeTextColor(tablerow' . $_GET['selrow'] . ',\'tableRowSel\');
								SelectedRowID = ' . $_GET['selrow'] . ';
								if(SelectedRowID){ EnableToolbarButtons(che); }else { DisableToolbarButtons(che); }
							}
						<!---->
						</script>';
		}


    require($GLOBALS['cfgDirRoot']."library/"."class.rightmenu.php");
    $rightmenu = new rightmenu;

    // $rightmenu->addItem('{%langstr:add_new%}','openDialog(\'?module=collections&action=newcolitem&site_id='.$site_id.'&page_id='.$page_id.'&category_id='.$category_id.'&coltype='.$collectiontype.'&session_id='.session_id().'&session_name='.session_name().'\', 400, 460, 1, 1);',false,false,'collection_new');
    // $rightmenu->addItem('{%langstr:col_edit%}',"openDialog('?module=collections&action=editcolitem&site_id=$site_id&page_id=$page_id&category_id=$category_id&coltype=".$collectiontype."&session_id=".session_id()."&session_name=".session_name()."&id='+SelectedRowID, 400, 460, 1, 1);",false,false,'collection_edit');
    // $rightmenu->addItem('{%langstr:up%}',"javascript:if(SelectedRowID)window.location='?module=collections&site_id=$site_id&page_id=$page_id&action=moveup&category_id=$category_id&coltype=".$_GET['coltype']."&justedit=".$_GET['justedit']."&session_id=".session_id()."&session_name=".session_name()."&main=1&selrow='+SelectedRowID+'&id='+SelectedRowID",false,false,'up');
    // $rightmenu->addItem('{%langstr:down%}',"javascript:if(SelectedRowID)window.location='?module=collections&site_id=$site_id&page_id=$page_id&action=movedown&category_id=$category_id&coltype=".$_GET['coltype']."&justedit=".$_GET['justedit']."&session_id=".session_id()."&session_name=".session_name()."&main=1&selrow='+SelectedRowID+'&id='+SelectedRowID;",false,false,'down');
    // $rightmenu->addItem('{%langstr:col_del%}',"javascript: var agree = confirm(unescape('Are you sure to delete?')); if (agree) { if(SelectedRowID)window.location='?module=collections&action=delcolitem&site_id=$site_id&page_id=$page_id&category_id=$category_id&coltype=".$collectiontype."&justedit=".$_GET['justedit']."&session_id=".session_id()."&session_name=".session_name()."&main=1&id='+SelectedRowID; }",false,false,'delete');

/*    if(!$_GET['justedit'])
    {
      $rightmenu->addSpacer();
      $rightmenu->addSpacer();
      $rightmenu->addSpacer();
      $rightmenu->addItem('{%langstr:toolbar_delete_collection%}',"javascript: var agree = confirm(unescape('Are you sure to delete?')); if (agree) { window.location='?module=collections&action=delcollection&site_id=$site_id&page_id=$page_id&category_id=$category_id&coltype=".$collectiontype."&main=1&session_id=".session_id()."&session_name=".session_name()."' }",false,false,'collection_delete');
    }*/ 

    return  $return.$rightmenu->output();
  }
	
  function IsEditableOutside()
  {
    $this->description = 'Collection: '.get_class();
    $this->longname = $this->name;
    return true;
  }

}