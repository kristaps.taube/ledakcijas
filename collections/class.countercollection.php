<?
//Serveris.LV Constructor collection module
//Created by Aleksandrs Selickis, 2004

include_once('class.collection.php');

class CounterCollection extends collection
{
  //Class initialization
  function CounterCollection($name,$id)
  {
    $this->Collection($name,$id);
    $this->type = "countercollection";

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "IP",
        "size"      => "35",
        "type"      => "str"
      ),

      "it1" => Array(
        "label"     => "Last visited page ID",
        "type"      => "customtext",
        "rows"      => "7",
        "cols"      => "50",
        "dialog"    => "?module=wysiwyg&site_id=".$GLOBALS['site_id'],
        "dialogw"   => "720",
        "dialogh"   => "450",
        "dlg_res"   => true
      )

    );

    //Collection display table definition
    $this->columns = Array(

      "col0"        => Array(
        "width"     => "70%",
        "title"     => "IP"
      ),

      "col1"    => Array(
        "width"     => "30%",
        "title"     => "Last viseted page ID"
      )

    );




  }




}

?>
