<?php

class LedAkcijasArticleCollection extends pdocollection
{

    public function __construct($name = '', $id = '')
    {

        parent::__construct($name, $id);

        $langs = [];
        foreach(getLanguages() as $short => $lang){
            $langs[$short] = $lang['fullname'];
        }

        //Properties array definition
        $this->properties_assoc = [

            "lang" => [
                "label" => "Valoda:",
                "type" => "list",
                "lookupassoc" => $langs
            ],
            "category_id" => Array(
                "label"     => "Kategorija:",
                "type"      => "relation",
                "related_collection" => "LedAkcijasArticleCategoryCollection",
                "relation_format" => "{%category_title_lv%}",
            ),
            "title" => [
                "label" => "Nosaukums:",
                "type" => "text"
            ],
            "url" => [
                "label" => "URL:",
                "type" => "text"
            ],
            "image" => [
                "label" => "Bilde:",
                'type'          => 'file',
                'upload_dir'    => 'images/articles/'
            ],
            "text" => [
                "label" => "Teksts:",
                'type'          => 'wysiwyg',
                'cols'          => 50,
                'rows'          => 7,
                'dialog'        => '?module=wysiwyg&site_id='.$this->site_id,
                'dialogw'       => 800,
                'dialogh'       => 600,
                'dlg_res'       => true
            ],
            "date" => [
                "label" => "Datums:",
                'type'          => 'date',
                'column_type'   => 'datetime',
            ],
            "products" => [
                "label" => "Artikuli(atdalīti ar ,):",
                'type' => 'text',
            ],
        ];

        //Collection display table definition
        $this->columns = [
            'lang' => ['title' => 'Valoda'],
            'title' => ['title' => 'Nosaukums'],
            'date' => ['title' => 'Datums'],
        ];

        $this->PostInit();

    }

    function getByUrl($url)
    {
        return DB::GetRow('SELECT * FROM `'.$this->table.'` WHERE url = :url', [':url' => $url]);
    }

    function getArticles($lang, $category = false)
    {

        $langs = [];

        $cond = $params = [];

        $cond[] = 'lang = :lang';
        $params[':lang'] = $lang;

        if($category){
            $cond[] = 'category_id = :cid';
            $params[':cid'] = $category;
        }

        return DB::GetTable('SELECT * FROM `'.$this->table.'` WHERE '.implode(' AND ', $cond), $params);

    }

    function processItemBeforeSave($item_id, &$item, $create, &$jsmessage)
    {

        if(!$item['url']) $item['url'] = transliterateURL($item['title']);
        if(!$item['date']) $item['date'] = date("Y-m-d H:i:s");

        return parent::processItemBeforeSave($item_id, $item, $create, $jsmessage);
    }

    public function IsEditableOutside()
    {
        $this->description = 'Collection: '.get_class();
        $this->longname = $this->name;
        return true;
    }

}