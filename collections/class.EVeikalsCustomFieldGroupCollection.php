<?
require_once('class.pdocollection.php');

class EVeikalsCustomFieldGroupCollection extends pdocollection{

  function __construct($name,$id){

    parent::__construct($name,$id);
    $this->type = get_class();

    //Properties array definition
    $this->properties_assoc = Array(

			"title"    => Array(
        "label"     => "Nosaukums:",
        "type"      => "str",
        "multilang" => true
      ),

    );

    //Collection display table definition
    $this->columns = Array(

      "title_lv"        => Array(
        "width"     => "60%",
        "title"     => "Nosakums:"
      ),

      "fields"    => Array(
        "title"     => "Lauki",
        "subrelated_collection" => "EVeikalsCustomFieldCollection",
        "subrelated_field" => "group_id",
        "format"    => '[if {%_self%}==1]<b>{%_self%}</b> lauks[else]<b>{%_self%}</b> lauki[/if]',
      ),

    );

     $this->PostInit();
  }

  function IsEditableOutside(){
    $this->description = 'Collection: '.get_class(); 
    $this->longname = $this->name;
    return true;
  }

  function processFormBeforeDisplay(&$form_data, $create, $item){
    

  }


}