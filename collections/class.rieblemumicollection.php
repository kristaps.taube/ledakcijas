<?
//Serveris.LV Constructor component
//Created by Kristaps Grinbergs, 2006, kristaps@datateks.lv

include_once('class.dbcollection.php');

class rieblemumicollection extends dbcollection
{
  //Class initialization
  function rieblemumicollection($name,$id)
  {
    $this->dbcollection($name,$id);
    $this->type = "picturescollection";

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "Datums, Laiks:",
        "type"      => "date",
        "dialog"    => "?module=dialogs&action=date&site_id=".$GLOBALS['site_id']."&time=1"
      ),
      
      'it1'   => array (
        'label'   => 'Faila tips:',
        'type'    => 'list',
        'lookup'  => Array('e:Excel', 'w:Word')
      ),
      
      "it2"    => Array(
      "label"     => "Fails:",
      "type"      => "dialog",
      "dialog"    => "?module=dialogs&action=filex&site_id=".$GLOBALS['site_id']."&view=thumbs",
      "dialogw"   => "600"
    )

      "title"    => Array(
        "label"     => "Nosaukums:",
        "size"      => "35",
        "type"      => "str"
      )

    );

    //Collection display table definition
    $this->columns = Array(

      "col0"        => Array(
        "width"     => "25%",
        "title"     => "Datums, laiks"
      ),

      "col1"        => Array(
        "width"     => "25%",
        "title"     => "Faila tips"
      ),

      "col2"        => Array(
        "width"     => "25%",
        "title"     => "Fails"
      ),

      "col3"        => Array(
        "width"     => "25%",
        "title"     => "Nosaukums"
      )

    );
  }


  function IsEditableOutside()
  {
    $this->description = 'Dokumenti';
    $this->longname = $this->name;
    return true;
  }

}

?>
