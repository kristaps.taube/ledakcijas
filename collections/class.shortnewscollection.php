<?
//Serveris.LV Constructor collection module
//Created by Aivars Irmejs, 2003, aivars@serveris.lv

include_once('class.newscollection.php');

class shortnewscollection extends NewsCollection
{
  //Class initialization
  function shortnewscollection($name,$id)
  {
    $this->Collection($name,$id);
    $this->type = "shortnewscollection";

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "Title:",
        "size"      => "35",
        "type"      => "str"
      ),

      "it1" => Array(
        "label"     => "Short text:",
        "type"      => "wysiwyg",
        "rows"      => "7",
        "cols"      => "50",
        "dialog"    => "?module=wysiwyg&site_id=".$GLOBALS['site_id'],
        "dialogw"   => "720",
        "dialogh"   => "450",
        "dlg_res"   => true
      ),

      "it2" => Array(
        "label"     => "Long text:",
        "type"      => "hidden"
      ),

      "it3"    => Array(
        "label"     => "Date:",
        "type"      => "str",
        "size"      => "30"
      )

    );

    //Collection display table definition
    $this->columns = Array(

      "col0"        => Array(
        "width"     => "20%",
        "title"     => "Title"
      ),

      "col1"    => Array(
        "width"     => "30%",
        "title"     => "Short Text"
      ),

      "col2"    => Array(
        "width"     => "0%",
        "title"     => ""
      ),

      "col3"     => Array(
        "width"     => "20%",
        "title"     => "Date"
      )


    );

    //Set default value of date to current time
    $this->properties['it3']['value'] = date("Y-m-d H:i:s", time());


  }

  function IsEditableOutside()
  {
    $this->description = 'Edit site short news';
    $this->longname = $this->name;
    return true;
  }



 

}

?>
