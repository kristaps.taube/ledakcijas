<?
//Serveris.LV Constructor component
//Created by Aleksandrs Selickis, 2004, alex@datateks.lv

include_once('class.dbcollection.php');

class picturescollection extends dbcollection
{
  //Class initialization
  function picturescollection($name,$id)
  {
    $this->dbcollection($name,$id);
    $this->type = "picturescollection";

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "Gallery ID",
        "type"      => "integer"
      ),

      "it1"    => Array(
        "label"     => "Thumbnail path",
        "size"      => "35",
        "type"      => "str"
      ),

      "it2"    => Array(
        "label"     => "Image path",
        "size"      => "35",
        "type"      => "str"
      ),

      "it3"    => Array(
        "label"     => "Photographer",
        "size"      => "35",
        "type"      => "str"
      ),

      "it4"    => Array(
        "label"     => "Phone",
        "size"      => "35",
        "type"      => "str"
      ),

      "it5" => Array(
        "label"     => "Text:",
        "type"      => "customtext",
        "rows"      => "7",
        "cols"      => "50",
        "dialog"    => "?module=wysiwyg&site_id=".$this->site_id,
        "dialogw"   => "720",
        "dialogh"   => "450",
        "dlg_res"   => true
      )

    );

    //Collection display table definition
    $this->columns = Array(

      "col0"        => Array(
        "width"     => "20%",
        "title"     => "Gallery ID"
      ),

      "col1"        => Array(
        "width"     => "20%",
        "title"     => "Thumbnail path"
      ),

      "col2"        => Array(
        "width"     => "20%",
        "title"     => "Image path"
      ),

      "col3"        => Array(
        "width"     => "15%",
        "title"     => "Photographer"
      ),

      "col4"        => Array(
        "width"     => "10%",
        "title"     => "Phone"
      ),

      "col5"        => Array(
        "width"     => "30%",
        "title"     => "Text"
      ),

      "col6"        => Array(
        "width"     => "20%",
        "title"     => "Big thumb path"
      )

    );

    $this->postInit();




  }

  function getPicturesForGallery ($gallery_id)
  {
    if(!$this->name)
    {
      $this->error = true;
      return false;
    }
    $data = sqlQueryData("SELECT * FROM `".$this->table."` WHERE col0 = ".$gallery_id." ORDER BY ind");
    return $data;
  }

  function countPicturesForGallery ($gallery_id)
  {
    if(!$this->name)
    {
      $this->error = true;
      return false;
    }
    $data = sqlQueryValue("SELECT COUNT(*) FROM `".$this->table."` WHERE col0 = ".$gallery_id);
    return $data;
  }

  function IsEditableOutside()
  {
    $this->description = 'Galerijas bildes';
    $this->longname = $this->name;
    return false;
  }

  function movePicUp($ind)
  {
    $item = $this->GetItem($ind);
    $gallery_id = $item[0];
    $otherrow = sqlQueryRow("SELECT * FROM `".$this->site_id."_coltable_".$this->name."` WHERE col0 = ".$gallery_id." AND ind < $ind ORDER BY ind DESC LIMIT 1");
    if(($otherrow)and($otherrow['ind']))
    {
      $this->SwapItems($ind, $otherrow['ind']);
    }
  }

  function movePicDown($ind)
  {
    $item = $this->GetItem($ind);
    $gallery_id = $item[0];
    $otherrow = sqlQueryRow("SELECT * FROM `".$this->site_id."_coltable_".$this->name."` WHERE col0 = ".$gallery_id." AND ind > $ind ORDER BY ind LIMIT 1");
    if(($otherrow)and($otherrow['ind']))
    {
      $this->SwapItems($ind, $otherrow['ind']);
    }
  }



}

?>
