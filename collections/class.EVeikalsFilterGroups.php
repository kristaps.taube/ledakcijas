<?php

class EVeikalsFilterGroups extends pdocollection{

  function __construct($name = '', $id = ''){

    parent::__construct($name,$id);

    //Properties array definition
    $this->properties_assoc = Array(

        "title"    => Array(
            "label"     => "Nosaukums:",
            "type"      => "str",
            "multilang" => true
        ),

        "sys_title"    => Array(
            "label"     => "Sistēmas nosaukums:",
            "type"      => "str",
        ),

        "imported"    => Array(
            "label"     => "Importētie:",
            "type"      => "check",
            "column_type" => "int(10)",
        ),

        "imported_for_cat"    => Array(
            "label"     => "Importēta priekš kategorijas:",
            "type"      => "hidden",
            "column_type" => "int(10)",
        ),

    );

    //Collection display table definition
    $this->columns = Array(

      "title_lv"        => Array(
        "width"     => "40%",
        "title"     => "Nosakums:"
      ),

      "sys_title"        => Array(
        "width"     => "40%",
        "title"     => "Sistēmas nosakums:"
      ),

      "params"    => Array(
        "title"     => "Filtri",
        "subrelated_collection" => "EVeikalsFilters",
        "subrelated_field" => "group",
        "format"    => '[if {%_self%}==1]<b>{%_self%}</b> filtrs[else]<b>{%_self%}</b> filtri[/if]',
      ),

    );


     $this->PostInit();
  }

  function IsEditableOutside(){
    $this->description = 'Filtru grupas';
    $this->longname = $this->name;
    return true;
  }

  function processFormBeforeDisplay(&$form_data, $create, $item){

  }

    function processItemBeforeSave($item_id, &$item, $create, &$jsmessage)
    {

        $item['imported'] = $item['imported'] ? 1 : 0;
        $item['sys_title'] = $item['sys_title'] ? $item['sys_title'] : $item['title_lv'];

        return true;

    }

  function getByIds($ids){
    return sqlQueryData("SELECT * FROM `".$this->table."` WHERE item_id IN (".implode(",", $ids).") ORDER BY ind");
  }

    public function addIfNotExist($title, $sys_title, $category_id)
    {

        static $cache;

        if(!isset($cache[$title])){

            $check = DB::GetRow("SELECT * FROM `".$this->table."` WHERE title_lv = :title AND imported_for_cat = :imported_for_cat", [':title' => $title, ':imported_for_cat' => $category_id]);

            if(!$check){

                $data= ['imported' => 1, 'imported_for_cat' => $category_id, 'sys_title' => $sys_title];
                foreach(getLanguages() as $short => $lang){
                    $data['title_'.$short] = $title;
                }

                $id = $this->Insert($data);

            }

            $result = $check ? $check['item_id'] : $id;
            $cache[$title] = $result;

        }

        return $cache[$title];

    }

}
