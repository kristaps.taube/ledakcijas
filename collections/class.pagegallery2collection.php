<?
//Datateks.lv Constructor component
//Created by Raivo Fismeisters, 2008, raivo@datateks.lv
//Project:

include_once('class.dbcollection.php');

class pagegallery2collection extends dbcollection
{
  function pagegallery2collection($name, $id)
  {
    $this->DBcollection($name, $id);
    $this->type = "pagegallery2collection";

    $this->properties = Array(
      "it0" => Array(
        "label"     => "Page:",
        "type"      => "hidden",
        "column_name" => "page_id",
        "column_type" => "int",
        "column_index" => 1
      ),

      "it1" => Array(
        "label"     => "Name:",
        "type"      => "str",
        "column_name" => "name",
      ),

      "it2"    => Array(
        "label"     => "Image:",
        "type"      => "file",
        "column_name" => "image",
      )

    );

    $this->columns = Array(
      "name"     => Array(
        "title"     => "Name:"
      ),

      "image"     => Array(
        "title"     => "Image:"
      )

    );

    $this->postInit();
  }

  function getImagesByPage($page_id)
  {
    return sqlQueryData('SELECT * FROM '.$this->table.' WHERE page_id = ' .$page_id . ' ORDER BY ind');
  }

}

?>