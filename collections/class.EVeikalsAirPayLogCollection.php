<?
include_once('class.dbcollection.php');

class EVeikalsAirPayLogCollection extends dbcollection{

  function __construct($name,$id){

    $this->dbCollection($name,$id);
    $this->type = get_class();

    //Properties array definition
    $this->properties_assoc = Array(

      "order_id"    => Array(
        "label"     => "Pasūtījuma id:",
        "type"      => "text"
      ),

      "airpay_response_date"    => Array(
        "label"     => "Airpay response date:",
        "type"      => "text"
      ),

      "airpay_status"    => Array(
        "label"     => "Airpay status:",
        "type"      => "text"
      ),

      "airpay_raw_response"    => Array(
        "label"     => "Raw response:",
        "type"      => "html"
      ),



    );

    //Collection display table definition
    $this->columns = Array(

      "order_id"        => Array("title"     => "Rēķina nr."),
      "airpay_response_date"        => Array("title"     => "Datums"),
      "airpay_status"        => Array("title"     => "Status"),
      "airpay_raw_response"        => Array("title"     => "Raw response", "width"     => "40%"),

    );

     $this->PostInit();

  }

  function IsEditableOutside(){
    $this->description = 'AirPay response Log';
    $this->longname = $this->name;
    return true;
  }

}
