<?
//Serveris.LV Constructor component
//Created by Aivars Irmejs, 2006, aivars@serveris.lv

include_once('class.dbcollection.php');

class shopcatcollection extends pdocollection
{
  //Class initialization
  function shopcatcollection($name = '', $id = '')
  {

    parent::__construct($name, $id);

    //Properties array definition
    $this->properties_assoc = Array(

        "lvl"    => Array(
            "label"     => "Level:",
            "size"      => "35",
            "type"      => "hidden",
            "column_type" => "int(32)",
            "column_index" => 1
        ),

        "parent"    => Array(
            "label"     => "Parent:",
            "type"      => "hidden",
            "column_type" => "int(32)",
            "column_index" => 1
        ),

        "title"    => Array(
            "label"     => "Title:",
            "type"      => "str",
            "width"      => "150px",
            "multilang" => true,
        ),

        "show_in_menu"    => Array(
            "label"     => "Show in menu:",
            "type"      => "check",
            "column_type" => "tinyint(1)"
        ),

        "featured"    => Array(
            "label"     => "Featured:",
            "type"      => "check",
            "column_type" => "tinyint(1)",
            "column_index" => 1
        ),

        "featured_image"    => Array(
            "label"     => "Featured image:",
            "type"      => "file",
        ),

        "icon"    => Array(
            "label"     => "Menu icon:",
            "type"      => "file",
        ),

        "mob_icon"    => Array(
            "label"     => "Mobile menu icon:",
            "type"      => "file",
        ),

        "desc"    => Array(
            "label"     => "Description:",
            "type"      => "wysiwyg",
            "rows"      => 3,
            "cols"      => 40,
            "multilang" => true,
            'dialog'        => '?module=wysiwyg&site_id='.$this->site_id,
            'dialogw'       => 800,
            'dialogh'       => 600,
            'dlg_res'       => true
        ),

        "description"    => Array(
            "label"     => "Meta Description:",
            "type"      => "html",
            "rows"      => 6,
            "cols"      => 40,
            "multilang" => true
        ),

        "additional_header_tags"    => Array(
            "label"     => "Additional header tags:",
            "type"      => "html",
            "rows"      => 6,
            "cols"      => 40,
            "multilang" => true
        ),

        "url"    => Array(
            "label"     => "URL:",
            "type"      => "str",
            "column_type" => "varchar(255)",
            "column_index" => 1,
            "multilang" => true
        ),

        "image"    => Array(
            "label"     => "Image:",
            "type"      => "file",
        ),

        "disabled"    => Array(
            "label"     => "Disabled:",
            "type"      => "check",
            "column_type" => "tinyint(1)"
        ),

        "color_group" => Array(
            "label"     => "Krāsu grupa:",
            "type"      => "relation",
            "column_type" => "int(32)",
            "related_collection" => "EVeikalsColorGroups",
            "relation_format" => "{%title%}",
        ),

        "size_group" => Array(
            "label"     => "Izmēru grupa:",
            "type"      => "relation",
            "column_type" => "int(32)",
            "related_collection" => "EVeikalsSizeGroups",
            "relation_format" => "{%group_title_lv%}",
        ),

        "custom_field_group" => Array(
            "label"     => "Custom lauku grupa:",
            "type"      => "relation",
            "column_type" => "int(32)",
            "related_collection" => "EVeikalsCustomFieldGroupCollection",
            "relation_format" => "{%title_lv%}",
        ),

        "uncategorized_item_cat"    => Array(
            "label"     => "Uncategorized item cat.:",
            "type"      => "check",
            "column_type" => "tinyint(1)",
            "column_index" => 1
        ),

    );

    //Collection display table definition
    $this->columns = Array(

      "title"        => Array(
        "width"     => "100%",
        "title"     => "Title",
        "multilang" => true,
      )
    );

    $this->postInit();



  }

  function getUncategorizedItemCatId(){

    static $id;

    if($id) return $id;

    $id = sqlqueryValue("SELECT item_id FROM `".$this->table."` WHERE uncategorized_item_cat = 1");

    if(!$id){ // not found? Add! NOW!!!

      $ind = $this->addCategory(array("title_lv" => "Nesakārtotie produkti", "disabled" => 1, "uncategorized_item_cat" => 1));
      $id = $this->GetItemID($ind);

    }

    return $id;

  }

  function checkIfUrlIsUnique($url, $langs, $prod_id = false){

   	$cond = $params = array();
		foreach($langs as $lang){
			$cond[] = "url_".$lang." = :".$lang;
			$params[":".$lang] = $url;
		}

    $rows = DB::GetTable("SELECT * FROM `".$this->table."` WHERE ".implode(" OR ", $cond), $params);

    if(count($rows) > 1){
    	return false;
    }elseif(count($rows) == 1 && $prod_id && $rows[0]['item_id'] != $prod_id){ // ja ir viens ieraksts, bet tas nav mū
    	return false;
    }elseif(!$prod_id && count($rows) > 0){
    	return false;
		}else{
    	return true;
    }

  }

  function getAllCategories($prodscol, $disabled=null)
  {
    $sql = $prodscol ? ', (SELECT COUNT(*) FROM '.$prodscol->table.' WHERE category_id=c.item_id) AS prodcount' : '';

    $sql2 = '';
    if ($disabled !== null)
      $sql2 = ' WHERE c.disabled='.(int)$disabled;
    
    $sql .= ', (SELECT COUNT(*) FROM '.$this->table.' WHERE parent=c.item_id) AS subcatcount';

    return sqlQueryDataAssoc('SELECT c.*'.$sql.' FROM '.$this->table.' c '.$sql2.' ORDER BY ind');
  }

  function getActivePredecessorIds($cat_id){

    #$cats = sqlQueryData("SELECT * FROM `".$this->table."` WHERE disabled=0 AND parent = ".$cat_id);
    $cats = $this->getAllCatsByParent($cat_id);

    $ids = array();

    if($cats){
      foreach($cats as $cat){
        $ids[] = $cat['item_id'];
        $ids = array_merge($ids, $this->getActivePredecessorIds($cat['item_id']));
      }
    }

    return $ids;

  }

  function getCategoryProductCount($category, $prods){

    static $cache;

    if(isset($cache[$category])) return $cache[$category];

    $counts = DB::GetTable("SELECT count(*) AS count, category_id FROM ".$prods->table." WHERE disabled = 0 GROUP BY category_id");
		foreach($counts as $count){
    	$cache[$count['category_id']] = $count['count'];
		}

    #$sub_cats = $this->getDBData(array("where" => "parent = ".$category." AND disabled = 0", "assoc" => true));
    $sub_cats = $this->getAllCatsByParent($category);

    foreach($sub_cats as $cat){
      $cache[$category] += $this->getCategoryProductCount($cat['item_id'], $prods);
    }

    return $cache[$category];

  }

  function getRandomLowestLevelCat(){

    static $cats;

    if(!$cats){
      $cats = sqlQueryData("
        SELECT c.*
        FROM `".$this->table."` as c
        WHERE
          0 = (
            SELECT count(*)
            FROM `".$this->table."`
            WHERE parent = c.item_id
          )
      ");
    }

    $cat = $cats[rand(0, count($cats) - 1)];

    return $cat;

  }

  function addCategory($properties)
  {

		$my_urls = array();
    $langs = getLanguages();
    foreach($langs as $lang){

      if(!$properties['url_'.$lang['shortname']]){
        if($properties['title_'.$lang['shortname']]){
          $properties['url_'.$lang['shortname']] = transliterateURL($properties['title_'.$lang['shortname']]);
        }else{
          $check = true;
          while($check){
            $url = strtolower("category-".$lang['shortname']."-".rand(1,10000));
            $check = $this->getCategoryByURL($url, $lang['shortname']) || in_array($url, $my_urls);
          }
          $properties['url_'.$lang['shortname']] = $url;
					$my_urls[] = $url;

        }

      }

    }

    if(!$properties['parent'])
      return $this->AddItemAsoc($properties);
    else
    { //adding subcategory
      $parent = $this->GetItemByIDAssoc($properties['parent']);
      $properties['lvl'] = $parent['lvl'] + 1;
      //find last item with the same parent
      $item = sqlQueryRow('SELECT * FROM ' . $this->table . ' WHERE parent = ' . $parent['item_id'] . ' ORDER BY ind DESC LIMIT 1');
      if($item)
      {
        do {
          $subitem = sqlQueryRow('SELECT * FROM ' . $this->table . ' WHERE parent = ' . intval($item['item_id']) . ' ORDER BY ind DESC LIMIT 1');
          if($subitem['item_id'])
            $item = $subitem;
        } while($subitem['item_id']);
      }



      $ind = $this->AddItemAsoc($properties);
      if($item['item_id'])
      {
        $newind = $item['ind'] + 1;
      }else
      {
        $newind = $parent['ind'] + 1;
      }
      while($ind > $newind)
      {
        $ind = $this->MoveItemUp($ind);
      }



      return $ind;
    }
  }


  function changeCategory($id, $properties)
  {
    return $this->ChangeItemByIDAsoc($id,$properties);
  }


  //delete categories and subcategories
  function deleteCategory($item_id, $productcol)
  {
    $data = sqlQueryData('SELECT * FROM ' . $this->table . ' WHERE parent = ' . $item_id . ' ORDER BY ind');

    foreach($data as $row)
    {
      if($row['parent'] == $item_id)
      {
        //delete all subcategories
        $this->deleteCategory($row['item_id'], $productcol);
      }
    }
    $productcol->deleteCategoryProducts($item_id);
    $this->DelDBItem($item_id);

    //todo: check why this line was here
    //delete from category-parameters
    //sqlQuery('DELETE FROM `' . $this->table . '_catparams` WHERE cat_id = ' . $item_id);
  }


  function getDataSubIndexes(&$data, &$item)
  {
    //find our item
    for($f = 0; $f < count($data) && $data[$f]['item_id'] != $item['item_id']; $f++)
    {
    }
    $f1 = $f;
    //find all its subitems
    for($f = $f1 + 1; $f < count($data) && $data[$f]['lvl'] > $item['lvl']; $f++)
    {
    }
    $f2 = $f - 1;
    if(!$data[$f2]['item_id'])
      $f2 = $f1;

    return array($f1, $f2);
  }

  function moveUpCategory($item_id)
  {
    //find moveable category and all its subcategories
    $item = $this->getItemByIdAssoc($item_id);
    $data = $this->getDBData();
    list($f1, $f2) = $this->getDataSubIndexes($data, $item);

    //find index of previous category in same level
    for($f = $f1 - 1; $f >= 0 && $data[$f]['lvl'] > $item['lvl']; $f--)
    {
    }
    $f3 = $f;
//    print_r($data[$f3]); echo '<br /><br /><br />';
    //move category and all its subcategories to that index
    if($data[$f3]['item_id'] && $data[$f3]['lvl'] == $data[$f1]['lvl'])
    {
      for($f = $f1; $f <= $f2; $f++)
      {
        for($i = 0; $i < $f1 - $f3; $i++)
        {
          $data[$f]['ind'] = $this->MoveItemUp($data[$f]['ind']);
        }
      }
    }
  }

  function moveDownCategory($item_id)
  {
    //find moveable category and all its subcategories
    $item = $this->getItemByIdAssoc($item_id);
    $data = $this->getDBData();
    list($f1, $f2) = $this->getDataSubIndexes($data, $item);

//    print_r($data[$f1]); echo '<br /><br /><br />';
//    print_r($data[$f2]); echo '<br /><br /><br />';
    //find index of next category in same level
    for($f = $f2 + 1; $f >= 0 && $data[$f]['lvl'] > $item['lvl']; $f++)
    {
    }
    $f3 = $f;
    if($data[$f3]['item_id'] && $data[$f3]['lvl'] == $data[$f1]['lvl'])
      $this->moveUpCategory($data[$f3]['item_id']);
  }

  function moveSubcategory($parent_id, $cat_id)
  {
    if (!$cat_id) return;

    //find moveable category and all its subcategories
    $cat = $this->getItemByIdAssoc($cat_id);
    $data = $this->getDBData();

    list($f1, $f2) = $this->getDataSubIndexes($data, $cat);

    $cat_count = $f2 - $f1 + 1;
    $newlevel1 = 0;

    if ($parent_id)
    {
      $parent = $this->getItemByIdAssoc($parent_id);
      list($e1, $e2) = $this->getDataSubIndexes($data, $parent);
      $maxind = $data[$e2+1] ? $data[$e2+1]['ind'] : $data[$e2]['ind']+1;

      if ($data[$e2+1])
        sqlQuery('UPDATE '.$this->table.' SET ind=ind+'.$cat_count.' WHERE ind >= '.$data[$e2+1]['ind']);

      $newlevel1 = $data[$e1]['lvl'] + 1;
    }
    else
    {
      $maxind = $data[count($data)-1]['ind'] + 1;
    }


//    var_dump($f1.', '.$f2);die;
    $lvl1 = $data[$f1]['lvl'];

    for ($i=$f1; $i<=$f2; $i++)
    {
      sqlQuery('UPDATE '.$this->table.' SET ind='.($maxind+$i-$f1).', lvl='.($data[$i]['lvl']-$data[$f1]['lvl'] + $newlevel1).' WHERE item_id='.$data[$i]['item_id']);

    }

    if ($data[$f2+1])
      sqlQuery('UPDATE '.$this->table.' SET ind=ind-'.$cat_count.' WHERE ind >= '.$data[$f2+1]['ind']);


  }


  function getSubCategories($item_id, $disabled=null)
  {
    $sql = '';
    if ($disabled !== null)
      $sql = ' AND disabled='.(int)$disabled;

    $data = DB::getTable("SELECT * FROM `".$this->table."` WHERE parent = :id ".$sql." ORDER BY ind", [':id' => $item_id]);
    return $data;
  }

  function getSubCatCount($item_id, $disabled=null)
  {
    $sql = '';
    if ($disabled !== null)
      $sql = ' AND disabled='.(int)$disabled;

    return sqlQueryValue("SELECT COUNT(*) FROM `".$this->table."` WHERE parent=".$item_id.$sql);
  }

  function getSizeGroupId($cat_id){

    $cat = $this->getFromCache($cat_id);

    if($cat['size_group']){
      return $cat['size_group'];
    }elseif($cat['parent']){
      return $this->getSizeGroupId($cat['parent']);
    }else{
      return false;
    }

  }

  function getColorGroupId($cat_id){

    $cat = $this->getFromCache($cat_id);
    if($cat['color_group']){
      return $cat['color_group'];
    }elseif($cat['parent']){
      return $this->getColorGroupId($cat['parent']);
    }else{
      return false;
    }

  }

  function getCustomFieldGroupId($cat_id){

    $cat = $this->getFromCache($cat_id);
    if($cat['custom_field_group']){
      return $cat['custom_field_group'];
    }elseif($cat['parent']){
      return $this->getCustomFieldGroupId($cat['parent']);
    }else{
      return false;
    }

  }

  function getCatsByIds($ids)
  {
    return sqlQueryData("SELECT * FROM `".$this->table."` WHERE item_id IN (".implode(',', $ids).")");
  }

  function getInactiveCatIds(){
		return DB::GetColumn("SELECT item_id FROM `".$this->table."` WHERE disabled = 1 OR uncategorized_item_cat = 1");
  }

  function listCatIds(&$list, $parent_id)
  {
    $data = sqlQueryData('SELECT * FROM '.$this->table.' WHERE parent='.$parent_id);
    foreach ($data as $row)
    {
      $list[] = $row['item_id'];

      $this->listCatIds($list, $row['item_id']);
    }
  }

  function getAllSubCategories($item_id){
    $list = array();
    $this->listCatIds($list, $item_id);

    return $list;

  }

  function getAllSubCategoriesRows($item_id)
  {
    $list = array();
    $this->listCatRows($list, $item_id);

    return $list;
  }

  function listCatRows(&$list, $parent_id)
  {
    $data = sqlQueryData('SELECT * FROM '.$this->table.' WHERE parent='.$parent_id.' AND disabled=0 ORDER BY ind');
    foreach ($data as $row)
    {
      $list[] = $row;

      $this->listCatRows($list, $row['item_id']);
    }
  }

  function getCategoriesByPath($path, $lang){

    $path = array_reverse($path);

    $cats = array();

    $parent = 0;
    foreach($path as $item){

      $cond = array();
      if($parent) $cond[] = 'parent = '.$parent;

      $cond[] = "url_".$lang." = '".$item."'";

      $q = "SELECT * FROM `".$this->table."` WHERE ".implode(" AND ", $cond);

      $cat = sqlQueryRow($q);
      if($cat){
        $cats[] = $cat;
        $parent = $cat['item_id'];
      }

    }

    return $cats;

  }

  function getCategoryTreeFlat($parent = 0, $only_active = true){

    $cond = array("parent = ". $parent);

    if($only_active){
      $cond[] = "disabled = 0";
    }

    $cats = array();

    $kids = $this->getDBData(array(
      "where" => implode(" AND ", $cond),
      "assoc" => true
    ));

    foreach($kids as $kid){
      $cats[] = $kid;
      $cats = array_merge($cats, $this->getCategoryTreeFlat($kid['item_id']));
    }

    return $cats;

  }

  function getCategoryTree($parent = 0, $only_active = true){

    $cond = array("parent" => $parent);

    if($only_active){
      $cond["disabled"] = "0";
    }

    $cats = array();
    foreach($this->getAllCats() as $cat){

      $ok = true;
      foreach($cond as $key => $value){

        if($cat[$key] != $value){
          $ok = false;
          break;
        }

      }

      if($ok){
        $cats[] = $cat;
      }

    }

    if($cats){
      foreach($cats as &$cat){
        $cat['kids'] = $this->getCategoryTree($cat['item_id']);
      }
    }else{
      $cats = array();
    }

    return $cats;

  }

    function getFromCache($id){

        $cats = $this->getAllCats();

        return isset($cats[$id]) ? $cats[$id] : false;

    }

    public function getForMenu()
    {

        $cats = [];
        foreach($this->getAllCatsByParent(0) as $cat){
            if($cat['show_in_menu']){
                $cats[] = $cat;
            }
        }

        return $cats;

    }

    function getAllCatsByParent($id, $include_disabled = false){

        $return = array();
        foreach($this->getAllCats() as $cat){
            if($cat['parent'] == $id && ($include_disabled || (!$cat['disabled'] && !$cat['uncategorized_item_cat']) )) $return[] = $cat;
        }
        return $return;

    }

    public function getFeatured()
    {
        return DB::GetTable('SELECT * FROM `'.$this->table.'` WHERE featured = 1');
    }

    function getAllCats()
    {

        static $cats;

        if(!$cats){

            $cats = array();
            foreach($this->getDBData(array("assoc" => true, "order" => "ind")) as $cat){
                $cats[$cat['item_id']] = $cat;
            }

        }

        return $cats;

    }

    function getPredecessorIds($cat){

        $kids = $this->getAllCatsByParent($cat);

        $ids = array();

        foreach($kids as $kid){
            $ids[] = $kid['item_id'];
            $ids = array_merge($ids, $this->getPredecessorIds($kid['item_id']));
        }

        return $ids;

    }

  function getAncestorIds($cat){

    $cat = (is_array($cat))? $cat : $this->getFromCache($cat);

    $ids = array();
		if($cat['parent']){
	    $parent = $this->getFromCache($cat['parent']);
	    if($parent){
	      $ids[] = $parent['item_id'];
	    }
    }

		while($parent['parent']){
      $parent = $this->getFromCache($parent['parent']);
      $ids[] = $parent['item_id'];
    }

		return $ids;

  }

  function getCategoryPath($cat_id){
    $data = Array();
    while($cat_id){

      foreach($this->getAllCats() as $c){
        if($c['item_id'] == $cat_id){
          $row = $c;
        }
      }


      if (!$row) break;
      $data[] = $row;
      $cat_id = intval($row['parent']);
    }
    $data = array_reverse($data);
    return($data);
  }

  function getCategoryByURL($url, $lang){
    return sqlQueryRow("SELECT * FROM `".$this->table."` WHERE url_".$lang." = '".$url."'");
  }

  function getCategoryPathByURL($urlpath, $lang)
  {
    static $cats = Array();
    $cat_id = 0;
    $data = Array();
    foreach ($urlpath as $url)
    {
      $cat_code = $this->table.':'.$cat_id.':'.$lang.':'.$url;

      if (isset($cats[$cat_code]))  {
        $row = $cats[$cat_code];
      }
      else
      {
        $row = sqlQueryRow("SELECT * FROM `".$this->table."` WHERE parent=".$cat_id." AND url_".$lang."='".$url."' LIMIT 1");
        if (!$row)
        {
          $cid = intval(substr($url, 1));
          if ($url == 'c'.$cid )
          {
            $row = sqlQueryRow("SELECT * FROM `".$this->table."` WHERE item_id=".$cid);
          }
          if (!$row || $row['parent'] != $cat_id) break;
        }
      }

      $cats[$cat_code] = $row;
      $data[] = $row;
      $cat_id = $row['item_id'];
    }
    return $data;
  }

  function getLevelCategories($level=0)
  {
    return sqlQueryData("SELECT * FROM `".$this->table."` WHERE lvl=".$level." ORDER BY ind");
  }

  function CanChangeOrder()
  {
    return false;
  }


}
