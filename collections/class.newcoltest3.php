<?
//Serveris.LV Constructor component
//Created by Aivars Irmejs, 2006, aivars@datateks.lv

include_once('class.collection.php');

class newcoltest3 extends collection
{
  //Class initialization
  function newcoltest3($name,$id)
  {
    $this->Collection($name,$id);
    $this->type = "newcoltest3";

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "Vards:",
        "size"      => "35",
        "type"      => "str",
      ),

      "it1"    => Array(
        "label"     => "E-pasts:",
        "size"      => "35",
        "type"      => "str",
      )

    );

    //Collection display table definition
    $this->columns = Array(

      "col0"        => Array(
        "width"     => "",
        "title"     => "Name",
      ),

      "col1"        => Array(
        "width"     => "",
        "title"     => "E-pasts",
      )
    );

    $this->postInit();
  }

  function processItemBeforeSave($ind, &$item, $create, &$jsmessage)
  {
    if($item['col0'])
    {
      $jsmessage = '';
      return true;
    }else
    {
      $jsmessage = 'Ludzu noradiet vardu!';
      return false;
    }
  }


  function IsEditableOutside()
  {
    $this->description = 'Testejam ne-asociativo kolekciju redigesanu';
    $this->longname = $this->name;
    return true;
  }



}

?>
