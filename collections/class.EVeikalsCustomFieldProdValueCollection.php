<?
require_once('class.pdocollection.php');

class EVeikalsCustomFieldProdValueCollection extends pdocollection{

  function __construct($name,$id){

    parent::__construct($name,$id);
    $this->type = get_class();

    //Properties array definition
    $this->properties_assoc = Array(

      "field_id" => Array(
        "label"     => "Lauks:",
        "type"      => "relation",
				"column_type" => "int(32)",
				"column_index" => 1,
        "related_collection" => "EVeikalsCustomFieldCollection",
        "relation_format" => "{%fieldname_".CMS_LANG."%}",
      ),

			"prod_id" => Array(
        "label"     => "Produkts:",
        "type"      => "relation",
				"column_type" => "int(32)",
				"column_index" => 1,
        "related_collection" => "shopprodcollection",
        "relation_format" => "{%name_".CMS_LANG."%}",
      ),

			"value"    => Array(
        "label"     => "Value:",
        "type"      => "str",
        "multilang" => true
      ),

    );

    //Collection display table definition
    $this->columns = Array(

      "field_id"        => Array(
        "title"     => "Lauks:"
      ),

			"prod_id"        => Array(
        "title"     => "Produkts:"
      ),

			"value_".CMS_LANG        => Array(
        "title"     => "Vērtība:"
      ),

    );

     $this->PostInit();
  }

  function IsEditableOutside(){
    $this->description = 'Collection: '.get_class(); 
    $this->longname = $this->name;
    return true;
  }

  function processFormBeforeDisplay(&$form_data, $create, $item){

  }

	function getProdFieldValues($prod_id, $field_id){
		return DB::GetRow("SELECT * FROM `".$this->table."` WHERE prod_id = :pid AND field_id = :fid", array(":pid" => $prod_id, ":fid" => $field_id));
	}

  function getProdValues($prod_id){

		$data = DB::GetTable("SELECT * FROM `".$this->table."` WHERE prod_id = :pid", array(":pid" => $prod_id));
		$return = array();

		foreach($data as $row){
    	$return[$row['field_id']] = $row;
		}

    return $return;

	}

}