<?

include_once('class.collection.php');

class merkistestimonialscollection extends collection
{
  //Class initialization
  function merkistestimonialscollection($name,$id)
  {
    $this->Collection($name,$id);
    $this->type = "merkistestimonialscollection";

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "Text",
        "type"      => "wysiwyg",
        "rows"      => "7",
        "cols"      => "50",
        "dialog"    => "?module=wysiwyg&site_id=".$GLOBALS['site_id'],
        "dialogw"   => "720",
        "dialogh"   => "450",
        "dlg_res"   => true
      ),

      "it1"    => Array(
        "label"     => "Active",
        "size"      => "35",
        "type"      => "list",
        "lookup"    => Array("0:Inactive", "1:Active")
      )

    );

    //Collection display table definition
    $this->columns = Array(

      "col0"        => Array(
        "width"     => "80%",
        "title"     => "Text"
      ),

      "col1"        => Array(
        "width"     => "20%",
        "title"     => "Active/Inactive"
      )

    );
  }

}

?>