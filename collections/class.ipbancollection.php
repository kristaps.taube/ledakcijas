<?
include_once('class.dbcollection.php');

class ipbancollection extends dbcollection
{
  //Class initialization
  function ipbancollection($name,$id)
  {
    $this->DBCollection($name,$id);
    $this->type = "ipbancollection";

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "IP:",
        "size"      => "35",
        "type"      => "string",
        "column_name" => "ip",
        "column_type" => "varchar(16)",
        "column_index" => 1
      ),

      "it1"    => Array(
        "label"     => "Ban time",
        "type"      => "date",
        "time"      => true,
        "column_name" => "ban_time",
        "column_type" => "varchar(30)",
        "column_index" => 1
      ),

      "it2"    => Array(
        "label"     => "Reason",
        "type"      => "str",
        "column_name" => "ban_reason",
      ),
    );

    //Collection display table definition
    $this->columns = Array(

      "ip"        => Array(
        "width"     => "20%",
        "title"     => "IP"
      ),

      "ban_time"    => Array(
        "width"     => "20%",
        "title"     => "Time"
      ),

      "ban_reason"     => Array(
        "width"     => "60%",
        "title"     => "Reason"
      ),
    );
    $this->postInit();
  }

  function SetBanOnIP($ip){
    if(!$this->name)
    {
      $this->error = true;
      return false;
    }
    sqlQuery("INSERT INTO `".$this->table."` (`ip`) VALUES ('".$ip."')");
  }

  function IsBlackListedIP($ip){
    if(!$this->name)
    {
      $this->error = true;
      return false;
    }
    $data = sqlQueryRow("SELECT `item_id` FROM `".$this->table."` WHERE `ip`='".$ip."' LIMIT 1");
    if($data) return true; else return false;
  }

  function GetBlackListedIPInfo($ip){
    return sqlQueryRow("SELECT * FROM `".$this->table."` WHERE `ip`='".$ip."' LIMIT 1");
  }

  function IsEditableOutside()
  {
    $this->description = 'Banned IP list';
    $this->longname = $this->name;
    return true;
  }

}

?>
