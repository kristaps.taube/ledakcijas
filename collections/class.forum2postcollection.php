<?
//Serveris.LV Constructor collection module
//Created by Aleksandrs Selickis, 2004

include_once('class.dbcollection.php');

class forum2postcollection extends dbcollection
{
  //Class initialization
  function forum2postcollection($name,$id)
  {
    $this->dbcollection($name,$id);
    $this->type = "forum2postcollection";

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "Category index",
        "size"      => "35",
        "type"      => "str"
      ),

      "it1"    => Array(
        "label"     => "User name",
        "size"      => "35",
        "type"      => "str"
      ),

      "it2"    => Array(
        "label"     => "Text",
        "size"      => "35",
        "type"      => "html",
        "rows"      => "6",
        "cols"      => "30",
        "search"    => true
      ),

      "it3"    => Array(
        "label"     => "Time",
        "size"      => "35",
        "type"      => "str"
      ),

      "it4"    => Array(
        "label"     => "IP",
        "size"      => "35",
        "type"      => "str"
      )

    );

    //Collection display table definition
    $this->columns = Array(

      "col0"        => Array(
        "width"     => "10%",
        "title"     => "Category index"
      ),

      "col1"        => Array(
        "width"     => "20%",
        "title"     => "User name"
      ),

      "col2"        => Array(
        "width"     => "45%",
        "title"     => "Text"
      ),

      "col3"        => Array(
        "width"     => "10%",
        "title"     => "Time"
      ),

      "col4"        => Array(
        "width"     => "15%",
        "title"     => "IP"
      )

    );

  }
  function GetCommentCount($id)
  {
    if(!$this->name)
    {
      $this->error = true;
      return false;
    }
    $item_id = sqlQueryValue("SELECT COUNT(*) FROM `".$this->table."` WHERE col0='$id'");
    return $item_id;
  }

  function FilterCensor($comment, $table)
  {
    $data =  sqlQueryData("SELECT `vards` FROM `".$table->table."`");
    foreach($data as $row){
      $pos = strpos(strtolower($comment), strtolower($row["vards"]));
      if ($pos === 0) return true;
    }
    return false;
  }

  function CheckIPBan($ip, $table)
  {
    $data =  sqlQueryData("SELECT `ip` FROM `".$table->table."`");
    foreach($data as $row){
      $pos = strcmp($ip, $row["ip"]);
      if ($pos === 0) return true;
    }
    return false;
  }

  function IsEditableOutside()
  {
    $this->description = "Forum comments:";
    $this->longname = $this->name;
    return true;
  }
}

?>
