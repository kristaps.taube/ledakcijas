<?
//Serveris.LV Constructor component
//Created by Aleksandrs Selickis, 2004, alex@datateks.lv

include_once('class.dbcollection.php');

class mailingcategoriescollectionex extends dbcollection
{
  //Class initialization
  function mailingcategoriescollectionex($name,$id)
  {
    $this->dbCollection($name,$id);
    $this->type = "mailingcategoriescollectionex";

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "Title",
        "size"      => "35",
        "type"      => "str",
        "column_name" => "mailcat_title"
      )

    );

    //Collection display table definition
    $this->columns = Array(

      "mailcat_title"        => Array(
        "title"     => "Title"
      ),

      "subscribers"    => Array(
        "width"     => "",
        "title"     => "Subscribers",
        "subrelated_collection" => "mailingcategoriesemailscollectionex",
        "subrelated_field" => "mailcatemail_cat",
        "format"    => '<b>{%_self%}</b> subscribers',
        "sortdesc"  => true,
      )

    );

    $this->PostInit();
  }

	function getAddByName($name){

    $cat = DB::GetRow("SELECT * FROM `".$this->table."` WHERE mailcat_title = :title", array(":title" => $name));

		if(!$cat){
			$id = $this->Insert(array("mailcat_title" => $name));
			$cat = $this->getItemByIDAssoc($id);
		}

		return $cat;

  }

  function GetCategoriesByIds($ids)
  {
    if(!$ids || !is_array($ids))
      return false;

    $idlist = implode(',', $ids);

    return sqlQueryData('SELECT * FROM `' . $this->table . '` WHERE item_id IN (' . $idlist . ');');
  }


  function GetCategoriesForEmails($ids, $relcol)
  {
    if(!$ids || !is_array($ids))
      return false;

    $idlist = implode(',', $ids);
    return sqlQueryData('SELECT *, A.item_id FROM `' . $this->table . '` A LEFT JOIN `' . $relcol->table . '` B ON A.item_id = B.mailcatemail_cat WHERE B.mailcatemail_email IN (' . $idlist . ');');
  }


  function IsEditableOutside()
  {
    $this->description = 'Mailing list categories';
    $this->longname = $this->name;
    return true;
  }







}

?>