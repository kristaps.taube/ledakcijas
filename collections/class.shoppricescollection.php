<?php

class shoppricescollection extends pdocollection
{

  function __construct($name = '', $id = '')
  {

    parent::__construct($name, $id);

    $this->properties_assoc = Array(
      "product_id"    => Array(
        "label"     => "Product ID",
        "type"      => "hidden",
        "column_type" => "int(10)",
        "column_index" => 1
      ),     

      "name"    => Array(
        "label"     => "Name",
        "type"      => "str",
        "multilang"      => true,
      ),

      "price"    => Array(
        "label"     => "Price",
        "type"      => "str",
				"column_type" => "decimal(10,2)",
      ),

      "saleout_price"    => Array(
        "label"     => "Saleout price",
        "type"      => "str",
				"column_type" => "decimal(10,2)",
      ),

      "code"    => Array(
        "label"     => "Code",
        "type"      => "str",
      ),

    );

    $this->columns = Array(
      "name_lv" => Array("title" => "Name"),
      "price" => Array("title" => "Price"),
      "saleout_price" => Array("title" => "Saleout price"),
      "code" => Array("title" => "Code"),
      "weight" => Array("title" => "Weight"),
    );

    $this->postInit();
  }

  function productPriceCount($prod){
  	return DB::GetValue("SELECT COUNT(*) FROM `".$this->table."` WHERE product_id = :id" , array(":id" => $prod));
  }

  function hasPrices($prod){
    return $this->productPriceCount($prod) ? true : false;
  }

  function replaceProductId($from, $to)
  {
    sqlQuery('UPDATE '.$this->table.' SET product_id='.$to.' WHERE product_id='.$from);
  }

  function getPricesByProduct($prod_id)
  {
    return sqlQueryDataAssoc("SELECT * FROM `".$this->table."` WHERE product_id=".$prod_id." ORDER BY ind");
  }

	function getPriceIdsByProduct($prod_id){
  	return DB::GetColumn("SELECT item_id FROM `".$this->table."` WHERE product_id = :id" , array(":id" => $prod_id));
  }

  function dropDeadSessionItems()
  {
    sqlQuery('DELETE p FROM `'.$this->table.'` p LEFT JOIN sessions s ON -p.product_id=s.id WHERE p.product_id < 0 AND s.id IS NULL');
  }
}

?>
