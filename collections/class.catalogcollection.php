<?
//Serveris.LV Constructor component
//Created by Rihards Grundmanis, 2007, rihards@datateks.lv
//Project: ATV Zone

include_once('class.dbcollection.php');

class catalogcollection extends dbcollection
{
    //produkta nosaukumu, cenu, galveno attēlu, galvenā attēla krāsu (kombobox), pieejamos izmērus, pieejamās krāsas
  //Class initialization
  function catalogcollection($name,$id)
  {
    $this->DBCollection($name,$id);
    $this->type = "catalogcollection";

    //initialize color collection
    /*
    $coltype = 'spsimagecollection';
    include_once($GLOBALS['cfgDirRoot'] . "collections/class." . $coltype . ".php");
    $colname = '';
    $categoryID = $_GET['colors'];
    $collection = new $coltype($colname,$categoryID);

    $colors = $collection->getDBData();   */
    $lookup = array('link_1','link_2','link_3');  /*
    foreach ($pages as $page)
    {
        $lookup[] = $page;
    }
                                                    */

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "Name:",
        "size"      => "35",
        "type"      => "str"
      ),

      "it1"    => Array(
        "label"     => "Code:",
        "size"      => "35",
        "type"      => "str"
      ),
         /*
      "it2"    => Array(
        "label"     => "Link to file:",     //Tehniskaa specifikaacija jaaraada tikai tad, kad ir kaut kas izveeleets
        "size"      => "100",
        "type"      => "list",
        "lookup"     => array('a:link_1','b:link_2','c:link_3')
      ),  //*/

      "it2"    => Array(
        "label"     => "Link to file:",
        "type"      => "dialog",
        "dialog"    => "?module=dialogs&action=filex&site_id=".$GLOBALS['site_id']."&view=thumbs",
        "dialogw"   => "600"
      ),


                               //Name Code Link to file   Price    Picture  Description   Bigpic

      "it3"    => Array(
        "label"     => "Price:",
        "size"      => "35",
        "type"      => "str"
      ),

      "it4"    => Array(
        "label"     => "Image (path):",
        "type"      => "file"
      ),


      "it5"     => Array(
        "label"     => "Description:",
        "type"      => "wysiwyg",
        "rows"      => "7",
        "cols"      => "50",
        "dialog"    => "?module=wysiwyg&site_id=".$GLOBALS['site_id'],
        "dialogw"   => "720",
        "dialogh"   => "450",
        "dlg_res"   => true
      ),

      "it6"    => Array(
        "label"     => "Big Picture",
        "type"      => "hidden"
      ),

      "it7"    => Array(
        "label"     => "Thumbnail Picture",
        "type"      => "hidden"
      ),
                //*/



    );

    //Collection display table definition
    $this->columns = Array(

      "col0"        => Array(
        "width"     => "20%",
        "title"     => "Name"
      ),

      "col1"        => Array(
        "width"     => "20%",
        "title"     => "Code"
      ),

      "col2"        => Array(
        "width"     => "20%",
        "title"     => "Link to file"
      ),

      "col3"        => Array(
        "width"     => "20%",
        "title"     => "Price"
      ),

      "col4"        => Array(
        "width"     => "10%",
        "title"     => "Image path"
      ),

      "col5"        => Array(
        "width"     => "20%",
        "title"     => "Description"
      ),

      "col6"        => Array(
        "width"     => "10%",
        "title"     => "Bigpic"
      )
       //*/
    );

    $this->postInit(); //ja develop mode ir true tad pieliek truukstoshaas kolonnas




  }



  function IsEditableOutside()
  {
    $this->description = 'Edit ATV Zone catalog';
    $this->longname = $this->name;
    return true;
  }

}

?>
