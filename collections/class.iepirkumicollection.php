<?
//Serveris.LV Constructor component
//Created by Kristaps Grinbergs, 2006, kristaps@datateks.lv

include_once('class.dbcollection.php');

class iepirkumicollection extends dbcollection
{
  //Class initialization
  function iepirkumicollection ($name,$id)
  {
    $this->dbcollection($name,$id);
    $this->type = "iepirkumicollection";

    //Properties array definition
    $this->properties = Array(

    "it0"    => Array(
        "label"     => "Iepirkuma nosaukums:",
        "type"      => "str",
        "column_name" => "virsraksts"
    ),

    "it1"    => Array(
        "label"     => "Izsludinasanas datums, Laiks:",
        "type"      => "date",
        "dialog"    => "?module=dialogs&action=date&site_id=".$GLOBALS['site_id']."&time=1",
        "column_name" => "datums1"
      ),

    "it2"    => Array(
      "label"     => "Konkursa nolikuma fails:",
      "type"      => "dialog",
      "dialog"    => "?module=dialogs&action=filex&site_id=".$GLOBALS['site_id']."&view=thumbs",
      "dialogw"   => "600",
      "column_name" => "fails1"
    ),

    "it3"    => Array(
        "label"     => "Beigu datums, Laiks:",
        "type"      => "date",
        "dialog"    => "?module=dialogs&action=date&site_id=".$GLOBALS['site_id']."&time=1",
        "column_name" => "datums2"
      ),

    "it4"    => Array(
      "label"     => "Konkursa rezultatu fails:",
      "type"      => "dialog",
      "dialog"    => "?module=dialogs&action=filex&site_id=".$GLOBALS['site_id']."&view=thumbs",
      "dialogw"   => "600",
      "column_name" => "fails2"
    )

    );

    //Collection display table definition
    $this->columns = Array(

      "virsraksts"        => Array(
        "width"     => "14%",
        "title"     => "Iepirkuma nosaukums"
      ),

      "datums1"        => Array(
        "width"     => "14%",
        "title"     => "Izsludinasanas datums, Laiks"
      ),

      "fails1"        => Array(
        "width"     => "14%",
        "title"     => "Konkursa nolikuma fails"
      ),

      "datums2"        => Array(
        "width"     => "14%",
        "title"     => "Beigu datums, Laiks"
      ),

      "fails2"        => Array(
        "width"     => "14%",
        "title"     => "Konkursa rezultatu fails"
      )

    );

    $this->postInit();
  }

  function IsAddToBottomMode()
  {
    return false;
  }


  function IsEditableOutside()
  {
    $this->description = 'Iepirkumi';
    $this->longname = $this->name;
    return false;
  }

}

?>
