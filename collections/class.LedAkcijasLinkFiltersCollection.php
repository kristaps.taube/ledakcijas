<?php

class LedAkcijasLinkFiltersCollection extends pdocollection
{

    public function __construct($name = '',$id = '')
    {

        parent::__construct($name, $id);

        //Properties array definition
        $this->properties_assoc = [

            "link_id" => [
                "label" => "Links:",
                "type" => "relation",
                "related_collection" => "LedAkcijasLinkCollection",
                "relation_format" => "{%name_lv%}",
            ],

            "filter_id" => [
                "label" => "Filtrs:",
                "type" => "relation",
                "related_collection" => "EVeikalsFilters",
                "relation_format" => "{%param_title_lv%}",
            ],

        ];

        //Collection display table definition
        $this->columns = [
            "link_id" => ["title" => "Links"],
            "filter_id" => ["title" => "Filtrs"],
        ];

        $this->PostInit();

    }

    public function getFilterIdsByLink($link_id)
    {
        return $this->GetColumn(['what' => 'filter_id','where' => 'link_id = :link_id', 'params' => [':link_id' => $link_id]]);
    }

    public function removeByFilterIdAndLinkId($filter_id, $link_id)
    {
        DB::Query("DELETE FROM `".$this->table."` WHERE link_id = :link_id AND filter_id = :filter_id", [':link_id' => $link_id, ':filter_id' => $filter_id]);
    }

    public function IsEditableOutside()
    {
        $this->description = 'Collection: '.get_class();
        $this->longname = $this->name;
        return true;
    }

}