<?
//Serveris.LV Constructor component
//Created by Aivars Irmejs, 2006, aivars@datateks.lv

include_once('class.dbcollection.php');

class newcoltest1 extends dbcollection
{
  //Class initialization
  function newcoltest1($name,$id)
  {
    $this->DBCollection($name,$id);
    $this->type = "newcoltest1";

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "Vards:",
        "size"      => "35",
        "type"      => "str",
        "column_name" => "vards",
        "validate"  => "required",
      ),

      "it1"    => Array(
        "label"     => "E-pasts:",
        "size"      => "35",
        "type"      => "str",
        "column_name" => "epasts",
        "validate"  => "email",
        "validate_msg" => "Laukā \"%s\" vajag korektu e-pasta adresi!"
      ),

      "it2"    => Array(
        "label"     => "Bilde:",
        "size"      => "35",
        "type"      => "file",
        "column_name" => "bilde",
        "upload_dir"  => "upload/newcoltest1bildes/",
        "resize_mode" => 7,
        "resize_width" => 100,
        "resize_height" => 100,
      ),

      "it3"    => Array(
        "label"     => "Lielā bilde:",
        "size"      => "35",
        "type"      => "str",
        "column_name" => "lielabilde",
        "copy_file"   => "bilde",
        "upload_dir"  => "upload/newcoltest1bildes/",
        "resize_mode" => 7,
        "resize_width" => 300,
        "resize_height" => 100,
      ),

      "it4"    => Array(
        "label"     => "Custom validation test:",
        "size"      => "35",
        "type"      => "str",
        "column_name" => "validate_test",
        "validate"  => "custom",
        "validate_code" => "if({%form_name%}.validate_test.value == 'bad') { alert('Laukā ievadīts slikts vārds!'); {%form_name%}.validate_test.focus(); return false; }"
      ),

      "it5"    => Array(
        "label"     => "Featured:",
        "size"      => "35",
        "type"      => "check",
        "column_name" => "featured",
      ),

      "it6"    => Array(
        "label"     => "Ikonas:",
        "type"      => "dialog",
        "dialog"    => "?module=dialogs&action=multichecklist&site_id=" . $this->site_id . "&coltype=travelrspikonascollection&categoryid=" . $this->GetProperty('icons_collection') . "&useid=0&columnname=icon_name&idcolumnname=icon_name",
        "column_name" => "ikonas",
      )

    );

    //Collection display table definition
    $this->columns = Array(

      "featured"    => Array(
        "width"     => "",
        "title"     => "Feat.",
        "format"    => "[check togglefeatured {%_self%}]",
      ),

      "vards"        => Array(
        "width"     => "",
        "title"     => "Name",
        "format"    => '<b>{%vards%}</b> <br />
                        [ahref reversename]Apgriezt[/ahref]&nbsp;&nbsp;&nbsp;&nbsp;
                        [adlg viewitemdetails]Detaļas[/adlg]',
      ),

      "epasts"        => Array(
        "width"     => "",
        "title"     => "E-pasts",
      ),

      "bilde"       => Array(
        "width"     => "",
        "title"     => "Bilde",
        "format"    => "[if {%_self%}]<img src=\"{%_self%}\" />[/if]",
        "sortdesc"  => true,
      ),

      "lielabilde"       => Array(
        "width"     => "",
        "title"     => "Lielā Bilde",
        "format"    => "[if {%_self%}]<img src=\"{%_self%}\" />[/if]",
        "sortdesc"  => true,
      ),

      "validate_test"   => Array(
        "width"     => "",
        "title"     => ".",
      ),

      "comments"    => Array(
        "width"     => "",
        "title"     => "Komentāri",
        "subrelated_collection" => "newcoltest1comments",
        "subrelated_field" => "category",
        "format"    => '[if {%_self%}==1]<b>{%_self%}</b> ieraksts[else]<b>{%_self%}</b> ieraksti[/if]',
        "sortdesc"  => true,
      ),

      "ikonas"   => Array(
        "width"     => "",
        "title"     => "Ikonas",
      ),
    );


    $this->colproperties = Array(

      "icons_collection"    => Array(
        "label"       => "Ikonas:",
        "type"        => "collection",
        "collectiontype" => "travelrspikonascollection",
        "lookup"      => GetCollections($this->site_id, "travelrspikonascollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      )

    );

    $this->postInit();
  }

  function processItemBeforeSave($item_id, &$item, $create, &$jsmessage)
  {
    parent::processItemBeforeSave($item_id, $item, $create, $jsmessage);
    if($item['vards'] && $item['vards'] != 'aaa')
    {
      $jsmessage = '';
      return true;
    }else
    {
      $jsmessage = 'Ludzu noradiet vardu, kas nav aaa!';
      return false;
    }
  }

  function GetExtraToolbarButtons(&$Toolbar, &$extraitembuttons, &$extrabuttons)
  {
    $Toolbar->AddButtonImageText("movetotop", '2uparrow', "Move to top", "javascript:if(SelectedRowID)window.location='?module=collections&site_id=". $this->site_id . "&page_id=" . $this->page_id . "&action=execcolmethod&method_name=movetotop&category_id=". $this->collection_id . "&coltype=".$this->type."&justedit=".$_GET['justedit']."&session_id=".session_id()."&session_name=".session_name()."&main=1&selrow='+SelectedRowID+'&id='+(SelectedRowID == -1 ? SelectedRowIDs : SelectedRowID);", '', 61, "Move selected item(s) to very top");
    $Toolbar->AddButtonImageText("reversename", 'bookmark', "Reverse", "javascript:if(SelectedRowID)window.location='?module=collections&site_id=". $this->site_id . "&page_id=" . $this->page_id . "&action=execcolmethod&method_name=reversename&category_id=". $this->collection_id . "&coltype=".$this->type."&justedit=".$_GET['justedit']."&session_id=".session_id()."&session_name=".session_name()."&main=1&selrow='+SelectedRowID+'&id='+SelectedRowID;", '', 61, "Reverse name of selected item");
    $Toolbar->AddButtonImageText("viewdetails", 'view_text', "View details", "", "openDialog('?module=collections&action=dlgcolmethod&method_name=viewitemdetails&site_id=". $this->site_id . "&page_id=" . $this->page_id . "&category_id=" . $this->collection_id . "&coltype=".$this->type."&session_id=".session_id()."&session_name=".session_name()."&id='+SelectedRowID, 400, 460, 1, 1);", 61, "View some information about selected item");
    $Toolbar->AddSeperator();
    $extrabuttons = Array('movetotop', 'viewdetails', 'reversename');
    $extraitembuttons = Array('movetotop', 'viewdetails', 'reversename');
  }


  //custom toolbar dialog handler: display item info dialog and form
  function ViewItemDetails($ids)
  {
    $item = $this->getItemByIdAssoc($ids[0], true);
    foreach($item as $key => $val)
    {
      echo '<b>' . $key . '</b>: ' . $val . '<br />';
    }
    //test form and saving
    echo '<form name="Form" action="?module=collections&action=dlgcolmethodexec&method_name=viewitemdetailssave&site_id='. $this->site_id . '&page_id=' . $this->page_id . '&category_id=' . $this->collection_id . '&coltype='.$this->type.'&session_id='.session_id().'&session_name='.session_name().'&id='.$ids[0].'" method="post">
    <input type="submit" value="pielikt punktu validate_test lauka beigās" />
    </form>';
  }


  //custom toolbar save handler: Append "." to validate_test field.
  function ViewItemDetailsSave($ids)
  {
    $item = $this->getItemByIdAssoc($ids[0], true);
    $item['validate_test'] .= '.';

    $item = $this->AddItemSlashes($item);
    $this->ChangeItemByIdAssoc($ids[0], $item);
  }


  //custom toolbar action handler: Move selected items to very top
  function MoveToTop($ids)
  {
    foreach($ids as $id)
    {
      $ind = $this->GetItemInd($id);
      while($ind > 1)
      {
        $ind = $this->MoveItemUp($ind);
      }
    }
  }


  //custom table item checkbox handler
  function togglefeatured($ids)
  {
    $item = $this->getItemByIdAssoc($ids[0], true);
    if($_GET['checked'] == 'true')
      $item['featured'] = 'on';
    else
      $item['featured'] = '';

    $item = $this->AddItemSlashes($item);
    $this->ChangeItemByIdAssoc($ids[0], $item);
  }


  //custom table link handler
  function reversename($ids)
  {
    $item = $this->getItemByIdAssoc($ids[0], true);
    $item['vards'] = $this->mbStringToArray($item['vards']);
    $item['vards'] = array_reverse($item['vards']);
    $item['vards'] = implode('', $item['vards']);

    $item = $this->AddItemSlashes($item);
    $this->ChangeItemByIdAssoc($ids[0], $item);
  }

  function mbStringToArray ($string) {
    $strlen = mb_strlen($string);
    while ($strlen) {
        $array[] = mb_substr($string,0,1,"UTF-8");
        $string = mb_substr($string,1,$strlen,"UTF-8");
        $strlen = mb_strlen($string);
    }
    return $array;
  }


  function IsEditableOutside()
  {
    $this->description = 'Testejam asociativo kolekciju redigesanu';
    $this->longname = $this->name;
    return true;
  }


  function IsAddToBottomMode()
  {
    return false;
  }


  function processDBProperties(&$form_data)
  {
    $customwidth = $this->getProperty('custom_lielabilde_width');
    if($customwidth)
    {
      $form_data['lielabilde']['resize_width'] = $customwidth;
    }
  }





}

?>
