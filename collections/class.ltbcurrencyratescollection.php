<?
//Serveris.LV Constructor component
//Created by Aleksandrs Selickis, 2005, alex@datateks.lv
//Project: Latvijas Tirdzniecibas Banka

include_once('class.collection.php');

class ltbcurrencyratescollection extends collection
{
  //Class initialization
  function ltbcurrencyratescollection($name,$id)
  {
    $this->Collection($name,$id);
    $this->type = "ltbcurrencyratescollection";

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "Currency",
        "size"      => "35",
        "type"      => "str"
      ),

      "it1"    => Array(
        "label"     => "Value",
        "size"      => "35",
        "type"      => "str"
      ),

	  "it2"    => Array(
        "label"     => "buy EUR",
        "size"      => "35",
        "type"      => "str"
      ),

	  "it3"    => Array(
        "label"     => "sell EUR",
        "size"      => "35",
        "type"      => "str"
      ),

	  "it4"    => Array(
        "label"     => "buy USD",
        "size"      => "35",
        "type"      => "str"
      ),

	  "it5"    => Array(
        "label"     => "sell USD",
        "size"      => "35",
        "type"      => "str"
      ),

	  "it6"    => Array(
        "label"     => "buy LVL",
        "size"      => "35",
        "type"      => "str"
      ),

	  "it7"    => Array(
        "label"     => "sell LVL",
        "size"      => "35",
        "type"      => "str"
      ),

    );

    //Collection display table definition
    $this->columns = Array(

      "col0"        => Array(
        "width"     => "20%",
        "title"     => "Currency"
      ),

      "col1"        => Array(
        "width"     => "20%",
        "title"     => "Value"
      ),

      "col2"        => Array(
        "width"     => "10%",
        "title"     => "buy EUR"
      ),

      "col3"        => Array(
        "width"     => "10%",
        "title"     => "sell EUR"
      ),

      "col4"        => Array(
        "width"     => "10%",
        "title"     => "buy USD"
      ),

      "col5"        => Array(
        "width"     => "10%",
        "title"     => "sell USD"
      ),

      "col6"        => Array(
        "width"     => "10%",
        "title"     => "buy LVL"
      ),

      "col7"        => Array(
        "width"     => "10%",
        "title"     => "sell LVL"
      ),

    );

  }

    function IsEditableOutside()
  {
    $this->description = "Currency rates collection";
    $this->longname = "Currency rates (" . $this->name . ")";
    return True;
  }



}

?>
