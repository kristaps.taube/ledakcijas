<?php

class shopbrandscollection extends pdocollection
{

    public function __construct($name = '',$id = '')
    {

        parent::__construct($name, $id);

        $this->properties_assoc = Array(

        "brandname" => Array(
            "label"     => "Brand name:",
            "column_name" => "brandname"
        ),

        "image" => Array(
            "label"     => "Logo image:",
            "type"      => "file",
            "upload_dir" => "/images/brands/"
        )


        );

        $this->columns = Array(
            "brandname"     => Array(
                "title"     => "Brand name:"
            ),

            "thumb"     => Array(
                "title"     => "Image:",
                "format"    => "[if {%image%}]<a href=\"{%image%}\" target=\"_blank\"><img src=\"{%imagethumb%}\" /></a>[/if]"
            ),

        );

        $this->postInit();
    }

    function GetEditData($page, &$count, &$recordposition, $order = '')
    {
        $data = parent::GetEditData($page, $count, $recordposition, $order);
        foreach ($data as $key => $row)
        {
            if (!$row['image']) continue;
            $data[$key]['imagethumb'] = getThumbURL($row['image'], 120, 90, 6);
        }
        return $data;
    }

    function IsEditableOutside()
    {
        $this->description = $this->type;
        $this->longname = "Brands";
        return true;
    }

    function getBrandnames($ids)
    {
        if (!count($ids)) return Array();
        $ids = implode(',', $ids);
        return sqlQueryData('SELECT * FROM `'.$this->table.'` WHERE item_id IN ('.$ids.') ORDER BY brandname');
    }

    function getItemByBrandname($name)
    {
        return sqlQueryRow('SELECT * FROM `'.$this->table.'` WHERE brandname="'.$name.'" LIMIT 1');
    }

    public function getFromCache($id)
    {

        static $cache;

        if(!$cache){

            $cache = [];
            foreach($this->getDBData(array("assoc" => true, "order" => "ind")) as $item){
                $cache[$item['item_id']] = $item;
            }

        }

        return isset($cache[$id]) ? $cache[$id] : false;

    }

}
