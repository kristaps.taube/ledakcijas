<?
//Serveris.LV Constructor collection module
//Created by Aleksandrs Selickis, 2004
//Edited by Kristaps Grinbergs, 2006, kristaps@datateks.lv

include_once('class.dbcollection.php');

class newscommentscollection extends dbcollection
{
  //Class initialization
  function newscommentscollection($name,$id)
  {
    $this->dbcollection($name,$id);
    $this->type = "newscommentscollection";

    //Properties array definition
    $this->properties = Array(
    
      "it0"    => Array(
        "label"     => "News id",
        "size"      => "35",
        "type"      => "relation",
        "related_collection" => "newscollectionex",
        "relation_format" => "{%col0%}",
      ),
    
      "it1"    => Array(
        "label"     => "{%langstr:name_nick%}",
        "size"      => "35",
        "type"      => "str"
      ),

      "it2"    => Array(
        "label"     => "{%langstr:text%}",
        "size"      => "35",
        "type"      => "html",
        "rows"      => "6",
        "cols"      => "30",
        "search"    => true
      ),

      "it3"    => Array(
        "label"     => "{%langstr:log_time%}",
        "size"      => "35",
        "type"      => "str"
      ),

      "it4"    => Array(
        "label"     => "{%langstr:log_ip%}",
        "size"      => "35",
        "type"      => "str"
      )

    );

    //Collection display table definition
    $this->columns = Array(

      "col0"        => Array(
        "width"     => "20%",
        "title"     => "News id"
      ),
      
      "col1"        => Array(
        "width"     => "20%",
        "title"     => "Name"
      ),

      "col2"        => Array(
        "width"     => "45%",
        "title"     => "Text"
      ),

      "col3"        => Array(
        "width"     => "10%",
        "title"     => "Time"
      ),

      "col4"        => Array(
        "width"     => "15%",
        "title"     => "IP Address"
      )

    );

  }
  
  function getNewsComments ($news_id=null)
  {
    if(!$this->name)
    {
      $this->error = true;
      return false;
    }
    $data = sqlQueryData("SELECT * FROM `".$this->table."` WHERE col0 = ".$news_id."  ORDER BY ind");
    return $data;

  }

  function getNewsCommentsCount ($news_id=null)
  {
    if(!$this->name)
    {
      $this->error = true;
      return false;
    }
    $data = sqlQueryValue("SELECT COUNT(*) FROM `".$this->table."` WHERE col0 = ".$news_id."");
    return $data;

  }

  function IsEditableOutside()
  {
    $this->description = 'Komentari';
    $this->longname = $this->name;
    return true;
  }

  function GetallDBData($start, $limit)
  {
    if(!$this->name)
    {
      $this->error = true;
      return false;
    }
    $data = sqlQueryData("SELECT * FROM  `".$this->table."` ORDER BY ind DESC LIMIT ".$start.", ".$limit."");
    return $data;
  }

  function ArrangeByDB($start, $limit, $order, $sort)
  {
    if(!$this->name)
    {
      $this->error = true;
      return false;
    }
    $data = sqlQueryData("SELECT * FROM `".$this->table."` ORDER BY `".$order."` ".$sort." LIMIT ".$start.", ".$limit."");
    return $data;
  }

  function SearchInDb($field, $query)
  {
    if(!$this->name)
    {
      $this->error = true;
      return false;
    }
    if(strlen($field)<3 || strlen($query)<3) return false;
    $search = substr($query, 0, 64);
    $search = preg_replace("/[^\w\x7F-\xFF\s]/", " ", $search);
    $good = trim(preg_replace("/\s(\S{1,2})\s/", " ", ereg_replace(" +", "  "," $search ")));
    $good = ereg_replace(" +", " ", $good);
    $data = sqlQueryData("SELECT * FROM `".$this->table."` WHERE `".$field."` LIKE '%". $good. "%'");
    return $data;
  }

  function UpdateFieldsDB($field, $value)
  {
    sqlQuery("UPDATE `".$this->table."` SET `".$field."`='".$value."'");
  }

  function ChangeItemProperty($item_id, $property_name, $property_val)
  {
    if(!$this->name)
    {
      $this->error = true;
      return false;
    }
    sqlQuery("UPDATE `".$this->table."` SET `".$property_name."`='".$property_val."' WHERE `item_id`='".$item_id."' LIMIT 1");
  }

  function SelectNewFromDB($ind)
  {
    if(!$this->name)
    {
      $this->error = true;
      return false;
    }
    $data = sqlQueryData("SELECT * FROM `".$this->table."` WHERE `col0` = ".$ind."");
    return $data;
  }
    function GetNewsNameById($news_id)
  {
    if(!$this->name)
    {
      $this->error = true;
      return false;
    }
    $data = sqlQueryData("SELECT `col0` FROM  `".$this->table."` WHERE `item_id`=".$news_id." LIMIT 1");
    return $data;
  }

  function GetAllNewsNames()
  {
    if(!$this->name)
    {
      $this->error = true;
      return false;
    }
    $data = sqlQueryData("SELECT `item_id` FROM  `".$this->table."`");
    return $data;
  }

  function SearchInDbZina($field)
  {
    if(!$this->name)
    {
      $this->error = true;
      return false;
    }
    $data = sqlQueryData("SELECT `item_id` FROM `".$this->table."` WHERE `item_id`='".$field."'");
    return $data;
  }
}

?>
