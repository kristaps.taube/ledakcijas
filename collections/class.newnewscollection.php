<?php


class newnewscollection extends pdocollection
{

  function __construct($name = '', $id = '')
  {

    parent::__construct($name, $id);

    $this->properties_assoc = Array(

      'title' => Array(
        'label'         => 'Virsraksts',
        'type'          => 'str',
      ),

      'url' => Array(
        'label'         => 'URL',
        'type'          => 'str',
      ),

      'short_text' => Array(
        'label'         => 'Īsais teksts',
        'type'          => 'wysiwyg',
        'cols'          => 50,
        'rows'          => 7,
        'dialog'        => '?module=wysiwyg&site_id='.$this->site_id,
        'dialogw'       => 800,
        'dialogh'       => 600,
        'dlg_res'       => true
      ),

      'full_text' => Array(
        'label'         => 'Pilnais teksts',
        'type'          => 'wysiwyg',
        'cols'          => 50,
        'rows'          => 7,
        'dialog'        => '?module=wysiwyg&site_id='.$this->site_id,
        'dialogw'       => 800,
        'dialogh'       => 600,
        'dlg_res'       => true
      ),

      'image' => Array(
        'label'         => 'Attēls',
        'type'          => 'file',
        'upload_dir'    => 'images/newnewscollection/'
      ),

      'date' => Array(
        'label'         => 'Datums',
        'type'          => 'date',
        'column_type'   => 'datetime',

      ),

    );

    $this->columns = Array(
      'title' => Array(
        'title'         => 'Virsraksts'
      ),

      'short_text' => Array(
        'title'         => 'Īsais apraksts'
      ),

    );

    $this->postInit();
  }

  function isAddToBottomMode()
  {
    return false;
  }

  function IsEditableOutside()
  {
    $this->description = 'News collections';
    $this->longname = $this->name;
    return true;
  }

  function processFormBeforeDisplay(&$form_data, $create, $item)
  {
  }

  function processItemBeforeSave($item_id, &$item, $create, &$jsmessage)
  {

    if(!$item['url']) $item['url'] = transliterateURL($item['title']);
    if(!$item['date']) $item['date'] = date("Y-m-d H:i:s");

    return parent::processItemBeforeSave($item_id, $item, $create, $jsmessage);
  }


  function getNews($where, $offset, $count, $order='')
  {
    $where = ($where != '') ? ' WHERE '.$where : '';
    $order = ($order != '') ? ' ORDER BY '.$order : '';
    $limit = ' LIMIT '.$offset.','.$count;

    $data = sqlQueryData("SELECT SQL_CALC_FOUND_ROWS * FROM `".$this->table."`".$where.$order.$limit);
    $count = sqlQueryValue("SELECT FOUND_ROWS()");
    return Array(
      'data'    => $data,
      'count'   => $count
    );
  }

  function getNewsIds($where, $order='')
  {
    $w = $where ? ' WHERE '.$where : '';
    return sqlQueryValue('SELECT GROUP_CONCAT(item_id) FROM (SELECT item_id FROM '.$this->table.$w.' ORDER BY '.$order.') AS t');
  }

}

