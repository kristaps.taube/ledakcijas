<?
require_once('class.pdocollection.php');

class EVeikalsCustomFieldCollection extends pdocollection{

  function __construct($name,$id){

    parent::__construct($name,$id);
    $this->type = get_class();

    //Properties array definition
    $this->properties_assoc = Array(

      "group_id" => Array(
        "label"     => "Group:",
        "type"      => "relation",
				"column_type" => "int(32)",
				"column_index" => 1,
        "related_collection" => "EVeikalsCustomFieldGroupCollection",
        "relation_format" => "{%title_lv%}",
      ),

			"type" => Array(
        "label"     => "Tips:",
        "type"      => "list",
        "lookupassoc"    => Array('text' => "Teksts", 'select' => "Definēti"),
      ),

			"fieldname"    => Array(
        "label"     => "Nosaukums:",
        "type"      => "str",
        "multilang" => true
      ),

    );

    //Collection display table definition
    $this->columns = Array(

      "group_id"        => Array(
        "title"     => "Grupa:"
      ),

			"type"        => Array(
        "title"     => "Tips:"
      ),

			"fieldname_lv"        => Array(
        "title"     => "Nosakums:"
      ),

			"values"    => Array(
        "title"     => "Vērtības",
        "subrelated_collection" => "EVeikalsCustomFieldValuesCollection",
        "subrelated_field" => "field_id",
        "format"    => '[if {%_self%}==1]<b>{%_self%}</b> vērtība[else]<b>{%_self%}</b> vērtības[/if]',
      ),

			"prod_values"    => Array(
        "title"     => "Produktu vērtības",
        "subrelated_collection" => "EVeikalsCustomFieldProdValueCollection",
        "subrelated_field" => "field_id",
        "format"    => '[if {%_self%}==1]<b>{%_self%}</b> vērtība[else]<b>{%_self%}</b> vērtības[/if]',
      ),

    );

     $this->PostInit();
  }

  function IsEditableOutside(){
    $this->description = 'Collection: '.get_class(); 
    $this->longname = $this->name;
    return true;
  }

	function getFieldsByGroup($group_id, $prod_id = false){

  	if($prod_id){

    	$value_fields = array();
			foreach(getLanguages() as $lang => $lang_data){
      	$value_fields[] = "if(f.type = 'text', pv.value_".$lang.", v.value_".$lang.") as value_".$lang; // is it text or select field
      	$value_fields[] = "if(f.type = 'select', pv.value_".$lang.", '') as value_id"; // is it text or select field
			}

      # pv.value_* can be from any lang, cuz all it only contains ID and it's same in all langs!
    	$sql = '
				SELECT f.*, '.implode(", ", $value_fields).'
				FROM `'.$this->table.'` AS f
				LEFT JOIN `'.$this->prod_values->table.'` AS pv ON pv.field_id = f.item_id AND pv.prod_id = :pid
				LEFT JOIN `'.$this->values->table.'` AS v ON v.field_id = f.item_id AND v.item_id = pv.value_'.$lang.'
				WHERE group_id = :gid
			';

			$params = array(":gid" => $group_id, ":pid" => $prod_id);


  	}else{
    	$params = array(":gid" => $group_id);
      $sql = 'SELECT * FROM `'.$this->table.'` WHERE group_id = :gid';
  	}

  	return DB::GetTable($sql, $params);

	}

  function GetEditData($page, &$count, &$recordposition, $order = ''){
    $data = parent::GetEditData($page, $count, $recordposition, $order);

    foreach ($data as $key => $row){

      $data[$key]['type'] = $this->properties_assoc['type']['lookupassoc'][$data[$key]['type']];

    }

		return $data;

  }


  function processFormBeforeDisplay(&$form_data, $create, $item){
    

  }


}