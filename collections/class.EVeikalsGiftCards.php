<?
require_once('class.dbcollection.php');

class EVeikalsGiftCards extends dbcollection{

  function __construct($name,$id){
    $this->dbCollection($name,$id);
    $this->type = get_class();

    //Properties array definition
    $this->properties_assoc = Array(

      "id"    => Array(
        "label"     => "Code:",
        "type"      => "str",
      ),

      "sum"    => Array(
        "label"     => "Sum:",
        "type"      => "str",
      ),

      "spent"    => Array(
        "label"     => "Spent:",
        "type"      => "str",
      ),

      "valid_until"    => Array(
        "label"     => "Valid until:",
        "type"      => "date",
      ),

      "pdf"    => Array(
        "label"     => "Pdf:",
        "type"      => "file",
        "upload_dir"    => "files/giftcards/"
      ),

      "paid"    => Array(
        "label"     => "Paid:",
        "type"      => "check",
        "column_type" => "tinyint(1)"
      ),

      "order_id"    => Array(
        "label"     => "Order id:",
        "column_type" => "int(10)"
      ),

      "view_hash"    => Array(
        "label"     => "View hash:",
        "type"      => "hidden",
      ),

    );

    //Collection display table definition
    $this->columns = Array(

      "item_id"        => Array(
        "width" => "10%",
        "title"     => "Item id:"
      ),

      "id"        => Array(
        "title"     => "Code:"
      ),

      "sum"        => Array(
        "title"     => "Sum:"
      ),

      "spent"        => Array(
        "title"     => "Spent:"
      ),

      "valid_until"        => Array(
        "title"     => "Valid until:"
      ),

      "pdf"        => Array(
        "title"     => "Pdf:",
        "format"    => "[if {%pdf%}]<a href=\"/{%pdf%}\" target=\"_blank\">Gift card</a>[/if]"
      ),

      "paid"        => Array(
        "title"     => "Paid:"
      ),

    );

     $this->PostInit();
  }

  function getByOrderId($order_id){
    return sqlQueryData("SELECT * FROM `".$this->table."` WHERE order_id = '".((int)$order_id)."'");
  }

  function getByViewHash($hash){
    return DB::GetRow("SELECT * FROM `".$this->table."` WHERE view_hash = :hash", array(":hash" => $hash));
  }

  function IsEditableOutside(){
    $this->description = 'Dāvanu kartes';
    $this->longname = $this->name;
    return true;
  }

  function getByCode($code){
    return DB::GetRow("SELECT * FROM `".$this->table."` WHERE id = :code", array(":code" => $code));
  }

  function createNew(){

    do{

      $code = generate_random_string(8, '0123456789');
      $row = $this->getByCode($code);

    }while($row);

    $ind = $this->addItemAssoc(array("id" => $code));

    $id = $this->GetItemID($ind);

    return $this->getItemByIDAssoc($id);

  }

  function processItemBeforeSave($item_id, &$item, $create, &$jsmessage){

    $item['valid_until'] = strtotime($item['valid_until']);
    $item['valid_until'] = date("Y-m-d", $item['valid_until']);
    $item['paid'] = $item['paid']? 1 : 0;

    return true;
  }

  function processFormBeforeDisplay(&$form_data, $create, $item){


  }


}
