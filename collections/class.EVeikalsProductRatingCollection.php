<?
include_once('class.dbcollection.php');

class EVeikalsProductRatingCollection extends dbcollection{
  
  function __construct($name,$id)
  {
    $this->dbCollection($name,$id);
    $this->type = get_class();

    //Properties array definition
    $this->properties_assoc = Array(

      "product_id" => Array(
        "label"     => "Produkts:",
        "type"      => "relation",
        "related_collection" => "shopprodcollection",
        "relation_format" => "{%name_lv%}",
      ),

      "user_id" => Array(
        "label"     => "Lietotājs:",
        "type"      => "relation",
        "related_collection" => "shopusercollection",
        "relation_format" => "{%name%} {%surname%}",
      ),

      "rating"    => Array(
        "label"     => "Vērtējums:",
        "type"      => "str"
      ),

    );

    //Collection display table definition
    $this->columns = Array(

      "product_id"    => Array("title"     => "Produkts"),
      "user_id"    => Array("title"     => "Lietotājs"),
      "rating"    => Array("title"     => "Vērtējums"),

    );

     $this->PostInit();
  }

  function getRating($prod_id, $user = false){

    static $ratings;

    if(isset($ratings[$prod_id])) return $ratings[$prod_id];

    $query = "SELECT AVG(rating) AS rating ";
    if($user){
      $query .= ", (SELECT rating FROM `".$this->table."` WHERE user_id = ".$user." AND product_id = '".$prod_id."') as mine ";
    }
    $query .= "FROM `".$this->table."` WHERE product_id = '".$prod_id."'";

    $ratings[$prod_id] = sqlQueryRow($query);

    return $ratings[$prod_id];

  }

  function rateProduct($user, $product, $rating){

    $rating = ($rating < 5)? $rating : 5; // 6 => 5
    $rating = ($rating > 0)? $rating : 0; // -1 => 0

    $row = sqlQueryRow("SELECT * FROM `".$this->table."` WHERE product_id = '".$product."' AND user_id = '".$user."'");

    if($row){

      if($row['rating'] != $rating){
        $this->changeItemByIDAssoc($row['item_id'], array("rating" => $rating));
      }

    }else{
      $this->addItemAssoc(array("user_id" => $user, "product_id" => $product, "rating" => $rating));
    }

  }

  function IsEditableOutside()
  {
    $this->description = 'Product ratings';
    $this->longname = $this->name;
    return true;
  }

}

?>