<?
//Serveris.LV Constructor component
//Created by Aivars Irmejs, 2006, aivars@datateks.lv

include_once('class.dbcollection.php');

class shopunitscollection extends dbcollection
{
    //produkta nosaukumu, cenu, galveno att�lu, galven� att�la kr�su (kombobox), pieejamos izm�rus, pieejam�s kr�sas
  //Class initialization
  function shopunitscollection($name,$id)
  {
    $this->DBCollection($name,$id);
    $this->type = "shopunitscollection";

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "Full name",
        "size"      => "35",
        "type"      => "str",
        "column_name" => "unit",
        "multilang" => true,
      ),

      "it1"    => Array(
        "label"     => "Short name",
        "size"      => "35",
        "type"      => "str",
        "column_name" => "shortname",
        "multilang" => true,
      )

    );

    //Collection display table definition
    $this->columns = Array(

      "unit"        => Array(
        "width"     => "",
        "title"     => "Name",
        "multilang" => true,
      ),

      "shortname"        => Array(
        "width"     => "",
        "title"     => "Short name",
        "multilang" => true,
      )

    );

    $this->postInit();
  }

  function getUnitByShortname($name, $lang='lv', $fields='item_id')
  {
    return sqlQueryRow("SELECT ".$fields." FROM `".$this->table."` WHERE shortname_".$lang."='".$name."' LIMIT 1");
  }

  function addUnit($name, $shortname='')
  {
    if (!strlen($shortname)) $shortname = $name;

    $name = trim($name);
    $shortname = trim($shortname);

    $units_id = 0;
    if (!empty($name))
    {
      $row = $this->getUnitByShortname($name, 'lv', 'item_id');
      $units_id = $row['item_id'];
      if (!$units_id)
      {
        $ind = $this->addItemAssoc(Array(
          'unit_lv' => addslashes($name),
          'shortname_lv' => addslashes($shortname),
        ));
        $units_id = $this->getItemID($ind);
      }
    }
    return $units_id;
  }

  function IsEditableOutside()
  {
    $this->description = $this->type;
    $this->longname = $this->name;
    return true;
  }

}

?>
