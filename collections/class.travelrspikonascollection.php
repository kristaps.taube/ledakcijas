<?
//Serveris.LV Constructor component
//Created by Kristaps Grinbergs, 2006, kristaps@datateks.lv

include_once('class.dbcollection.php');

class travelrspikonascollection extends dbcollection
{
  //Class initialization
  function travelrspikonascollection ($name,$id)
  {
    $this->dbcollection($name,$id);
    $this->type = "travelrspikonascollection";

    //Properties array definition
    $this->properties = Array(


      "it0"    => Array(
          "label"     => "Nosaukums:",
          "type"      => "str",
          "column_name" => "icon_name",
          "validate" => "required"
      ),

      "it1"    => Array(
          "label"     => "Ikona:",
          "type"      => "file",
          "upload_dir"  => "images/",
          "column_name" => "icon_file"
      )

    );

    //Collection display table definition
    $this->columns = Array(

      "icon_name"        => Array(
        "width"     => "50%",
        "title"     => "Nosaukums"
      ),

      "icon_file"        => Array(
        "width"     => "50%",
        "title"     => "Ikonas fails"
      )

    );

    $this->postInit();
  }

  function IsAddToBottomMode()
  {
    return false;
  }


  function IsEditableOutside()
  {
    $this->description = 'Braucienu ikonas';
    $this->longname = $this->name;
    return true;
  }

}

?>
