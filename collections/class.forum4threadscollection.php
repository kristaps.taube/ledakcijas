<?

include_once('class.dbcollection.php');

class forum4threadscollection extends dbcollection
{
  //Class initialization
  function forum4threadscollection($name,$id)
  {
    $this->dbcollection($name,$id);
    $this->type = "forum4threadscollection";

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "Category ID",
        "type"      => "relation",
        "related_collection" => "forum4categoriescollection",
        "relation_format" => "{%cat_title%}",
        "column_name" => "th_catid",
        "column_type" => "int(32)",
        "column_index" => 1
      ),

      "it1"    => Array(
        "label"     => "Author ID",
        "type"      => "relation",
        "related_collection" => "forum4userscollection",
        "relation_format" => "{%login%}",
        "column_name" => "th_authorid",
        "column_type" => "int(32)",
        "column_index" => 1
      ),

      "it2"    => Array(
        "label"     => "Author",
        "type"      => "str",
        "column_name" => "th_author",

      ),

      "it3"    => Array(
        "label"     => "Title",
        "type"      => "str",
        "column_name" => "th_title",
      ),

      "it4"    => Array(
        "label"     => "Text",
        "type"      => "html",
        "rows"      => "6",
        "cols"      => "30",
        "column_name" => "th_text",
      ),

      "it5"    => Array(
        "label"     => "Creation time",
        "type"      => "date",
        "time"      => true,
        "column_name" => "th_time",
        "column_type" => "varchar(30)",
        "column_index" => 1
      ),

      "it6"    => Array(
        "label"     => "IP",
        "type"      => "str",
        "column_name" => "th_ip"
      ),

      "it7"    => Array(
        "label"     => "Locked",
        "type"      => "list",
        "lookup"    => Array("0:No", "1:Yes"),
        "column_name" => "th_locked"
      ),

      "it8"    => Array(
        "label"     => "Post count",
        "type"      => "str",
        "column_name" => "th_postcount"
      ),

      "it9"    => Array(
        "label"     => "Update time",
        "type"      => "date",
        "time"      => true,
        "column_name" => "th_updatetime",
        "column_type" => "varchar(30)",
        "column_index" => 1
      ),

    );

    //Collection display table definition
    $this->columns = Array(

      "th_title"        => Array(
        "width"     => "20%",
        "title"     => "Title"
      ),

      "th_text"        => Array(
        "width"     => "40%",
        "title"     => "Text"
      ),

      "th_author"        => Array(
        "width"     => "20%",
        "title"     => "Author"
      ),

      "th_ip"        => Array(
        "width"     => "20%",
        "title"     => "IP"
      ),


    );

    $this->postInit();
  }

  function getThreadsForCategory($cat_id, $activepage = 0, $pagesize = 0)
  {
    if($pagesize == 0)
      return sqlQueryData('SELECT * FROM `' . $this->table . '` WHERE th_catid = "' . $cat_id . '" ORDER BY th_updatetime DESC');
    else
      return sqlQueryData('SELECT * FROM `' . $this->table . '` WHERE th_catid = "' . $cat_id . '" ORDER BY th_updatetime DESC LIMIT '.($activepage * $pagesize).', '.$pagesize.'');
  }

  function countThreadsForCategory($cat_id)
  {
    return sqlQueryValue('SELECT COUNT(*) FROM `' . $this->table . '` WHERE th_catid = "' . $cat_id . '"');
  }

  function getPageCount($cat_id, $pagesize)
  {
    $itemcount = $this->countThreadsForCategory($cat_id);
    return (intval(ceil($itemcount / $pagesize)));
  }

  function ThreadPostsChanged($thread_id, $postcol)
  {
    $updatetime = date('Y-m-d H:i:s');
    if($postcol)
      $postcount = $postcol->countPostsForThread($thread_id);
    else
      $postcount = 0;
    $this->changeItemByIdAssoc($thread_id, Array('th_updatetime' => $updatetime, 'th_postcount' => $postcount));
  }

  function DelThreadsByIP($ip, $catcol)
  {
    $todel = sqlQueryData('SELECT * FROM `' . $this->table . '` WHERE th_ip = "'.$ip.'"');
    sqlQuery('DELETE FROM `' . $this->table . '` WHERE th_ip = "'.$ip.'"');
    foreach($todel as $delthread)
    {
      $catcol->CategoryThreadsChanged($delthread['th_catid'], $this);
    }
  }

}

?>
