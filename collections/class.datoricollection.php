<?
//Serveris.LV Constructor collection module
//Created by Aleksandrs Selickis, 2004

include_once('class.catsubcollection.php');

class DatoriCollection extends catsubcollection
{
  //Class initialization
  function DatoriCollection($name,$id)
  {
    $this->Collection($name,$id);
    $this->type = "datoricollection";

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "Adv date",
        "size"      => "35",
        "type"      => "str"
      ),
	  
	  "it1"    => Array(
        "label"     => "Nosaukums",
        "size"      => "35",
        "type"      => "str"
      ),
	
	  "it2"    => Array(
        "label"     => "Apraksts",
        "size"      => "35",
        "type"      => "str"
      ),

      "it3"    => Array(
        "label"     => "Contacts",
        "size"      => "35",
        "type"      => "str"
      )

    );

    //Collection display table definition
    $this->columns = Array(

      "col0"        => Array(
        "width"     => "20%",
        "title"     => "Adv date"
      ),

	  "col1"        => Array(
        "width"     => "20%",
        "title"     => "Nosaukums"
      ),

      "col2"        => Array(
        "width"     => "30%",
        "title"     => "Apraksts"
      ),

      "col3"        => Array(
        "width"     => "15%",
        "title"     => "Contacts"
      )

    );

  }


function DisplayItemText($id)
	{
		$row = $this->GetItem($id);
		echo "<b>".$row['1'].'</b><br />';
		echo $row['2']."<br />";
	}


function DisplayItemInfo($id)
	{
		$row = $this->GetItem($id);
		echo "<i>".$row['3'].'</i><br />';
	}


function GetItemDate($id)
	{
		$row = $this->GetItem($id);
		echo "<i>".$row['0'].'</i><br />';
	}

function DisplayForm($name)
{
	echo '<table border=0 cellspacing=0 cellpadding=0>';
	echo '<form action="?add_adv=1&cat_id='.$name.'" method="POST">';
	echo '<tr><td>Nosaukums: </td><td><input name="col1" type="text" size=40></td></tr>';
	echo '<tr><td valign=top>Apraksts: </td><td><textarea name="col2" rows=5 cols=32></textarea></td></tr>';
	echo '<tr><td>Kontaktinformaacija: </td><td><input name="col3" type="text" size=40></td></tr>';
	echo '<tr><td></td><td align="right"><input type="submit" value="Pievienot"></td></tr>';
	echo '</form></table>';

}

}

?>

