<?php
//Serveris.LV Constructor component
//Created by Aivars Irmejs, 2006, aivars@datateks.lv

class shoporderprodcollection extends pdocollection
{

    function __construct($name = '', $id ='')
    {

        parent::__construct($name, $id);
        
    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "Price",
        "size"      => "35",
        "type"      => "str",
        "column_name" => "price"
      ),

      "it9"    => Array(
        "label"     => "Base Price",
        "size"      => "35",
        "type"      => "str",
        "column_name" => "base_price"
      ),

      "it1"    => Array(
        "label"     => "Count",
        "size"      => "35",
        "type"      => "str",
        "column_name" => "count"
      ),

      "it2"    => Array(
        "label"     => "Product",
        "type"      => "hidden",
        "column_name" => "prod_id",
        "column_type" => "int(32)",
        "column_index" => 1
      ),

      "it3"     => Array(
        "label"     => "Order",
        "type"      => "hidden",
        "column_name" => "order_id",
        "column_type" => "int(32)",
        "column_index" => 1
      ),

      "it4"    => Array(
        "label"     => "Price ID",
        "type"      => "hidden",
        "column_name" => "price_id",
        "column_type" => "int(10)",
      ),

      "it6"    => Array(
        "label"     => "Color ID",
        "type"      => "hidden",
        "column_name" => "color_id",
        "column_type" => "int(10)",
      ),

      "it7"    => Array(
        "label"     => "Size ID",
        "type"      => "hidden",
        "column_name" => "size_id",
        "column_type" => "int(10)",
      ),

      "it8"    => Array(
        "label"     => "Cart key",
        "type"      => "hidden",
        "column_name" => "cart_key",
        "column_type" => "int(10)",
      ),

      "it5"    => Array(
        "label"     => "Giftcarddata",
        "type"      => "hidden",
        "column_name" => "giftcard_data",
      ),

    );

    //Collection display table definition
    $this->columns = Array(

      "price"        => Array(
        "width"     => "20%",
        "title"     => "Name",
      ),

      "price"        => Array(
        "width"     => "10%",
        "count"     => "Price"
      ),
    );

    $this->postInit();
  }

  function IsEditableOutside()
  {
    $this->description = $this->type;
    $this->longname = $this->name;
    return false;
  }

  function getProductsByOrder($order_id, $with_cart_keys = false){
    $data = DB::GetTable('SELECT * FROM `'.$this->table.'` WHERE order_id='.$order_id);


    if($with_cart_keys){
      $tmp = array();
      foreach($data as $row){
        $tmp[$row['cart_key']] = $row;
      }
      $data = $tmp;
    }

    return $data;

  }
	
	function getProdIDS($order_id){
    $data = DB::GetTable('SELECT prod_id,count FROM `'.$this->table.'` WHERE order_id='.$order_id);
		$tmp = array();
		foreach($data as $row){
			$tmp[$row['prod_id']] = $row['count'];
		}
		$data = $tmp;
    

    return $data;

  }

	function getProductByID($order_id, $prod_id, $price_id = false){
    
		if($price_id)
			$data = DB::GetRow('SELECT item_id,prod_id,count FROM `'.$this->table.'` WHERE order_id='.$order_id.' AND prod_id='.$prod_id.' AND price_id='.$price_id);
		else
			$data = DB::GetRow('SELECT item_id,prod_id,count FROM `'.$this->table.'` WHERE order_id='.$order_id.' AND prod_id='.$prod_id);
		
    return $data;

  }

  function deleteProductsByOrder($order_id)
  {
    sqlQuery('DELETE FROM '.$this->table.' WHERE order_id='.(int)$order_id);
  }


  public function getOrderProducts($order_id)
  {

    $prod_col = shopprodcollection::getInstance();

    return DB::GetTable("
        SELECT p.*,op.price,op.base_price,op.count,op.item_id as order_prod_id
        FROM `".$this->table."` AS op
        LEFT JOIN `".$prod_col->table."` AS p ON p.item_id = op.prod_id
        WHERE op.order_id = :oid
    ", [':oid' => $order_id]);

  }

}


