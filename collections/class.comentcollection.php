<?
//Serveris.LV Constructor collection module
//Created by Aivars Irmejs, 2003, aivars@serveris.lv

include_once('class.collection.php');

class ComentCollection extends collection
{
  //Class initialization
  function ComentCollection($name,$id)
  {
    $this->Collection($name,$id);
    $this->type = "comentcollection";

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "Author:",
        "size"      => "35",
        "type"      => "str"
      ),

      "it1" => Array(
        "label"     => "Coment text:",
        "type"      => "customtext",
        "rows"      => "7",
        "cols"      => "50",
        "dialog"    => "?module=wysiwyg&site_id=".$GLOBALS['site_id'],
        "dialogw"   => "720",
        "dialogh"   => "450",
        "dlg_res"   => true
      ),

      "it2"    => Array(
        "label"     => "Date:",
        "type"      => "str",
        "size"      => "30"
      )

    );

    //Collection display table definition
    $this->columns = Array(

      "col0"        => Array(
        "width"     => "20%",
        "title"     => "Author"
      ),

      "col1"    => Array(
        "width"     => "60%",
        "title"     => "Coment Text"
      ),

      "col2"     => Array(
        "width"     => "20%",
        "title"     => "Date"
      )


    );

    //Set default value of date to current time
    //$this->properties['it3']['value'] = date("Y-m-d H:i:s", time());


  }

  function IsEditableOutside()
  {
    $this->description = 'Edit site comments';
    $s = explode('_', $this->name);
    $s = sqlQueryValue("SELECT title FROM " . $this->site_id . "_pages WHERE page_id = " . intval($s[count($s) - 1]));
    if(!$s)
    {
      $this->longname = $this->name;
    }else
    {
      $this->longname = "Comments in page $s";
    }
    return true;
  }





}

?>
