<?php

class PastsCollection extends pdocollection
{

    public function __construct($name = '',$id = '')
    {

        parent::__construct($name, $id);

        //Properties array definition
        $this->properties_assoc = [

            "name" => [
                "label" => "Nosaukums:",
                "type" => "text",
                "multilang" => true
            ],

            "address" => [
                "label" => "Adrese:",
                "type" => "text",
                "multilang" => true
            ],

            "coords" => [
                "label" => "Koordinātas:",
                "type" => "text",
            ],

            "zip"    => Array(
                "label"     => "Indekss:",
                "type"      => "hidden",
                "column_type"      => "varchar(7)",
            ),

        ];

        //Collection display table definition
        $this->columns = [
            "name_lv" => ["title" => "Nosaukums"],
            "address_lv" => ["title" => "Adrese"],
        ];

        $this->PostInit();

    }

    public function getByZip($zip)
    {
        return $this->getRow(['where' => "zip = :zip", 'params' => [':zip' => $zip]]);
    }

    public function IsEditableOutside()
    {
        $this->description = 'Collection: '.get_class();
        $this->longname = $this->name;
        return true;
    }

}