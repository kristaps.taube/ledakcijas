<?
//Serveris.LV Constructor component
//Created by Aleksandrs Selickis, 2004, alex@datateks.lv

include_once('class.dbcollection.php');

class memkonkursicollection extends dbcollection
{
  //Class initialization
  function memkonkursicollection($name,$id)
  {
    $this->dbCollection($name,$id);
    $this->type = "memkonkursicollection";

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "title",
        "size"      => "35",
        "type"      => "str"
      ),

      "it1"    => Array(
        "label"     => "start date <br>(YYYY-MM-DD)",
        "size"      => "35",
        "type"      => "str"
      ),

      "it2"    => Array(
        "label"     => "end date <br>(YYYY-MM-DD)",
        "size"      => "35",
        "type"      => "str"
      ),

      "it3"    => Array(
        "label"     => "check date <br>(YYYY-MM-DD)",
        "size"      => "35",
        "type"      => "str"
      ),
      
      "it4"    => Array(
        "label"     => "text",
        "type"      => "customtext",
        "rows"      => "6",
        "cols"      => "30",
        "dialog"    => "?module=wysiwyg&canpreview='+((window.parent.dialogArguments)?1:0)+'&site_id=".$GLOBALS['site_id']."&page_id=".$this->page_id,
        "dialogw"   => "720",
        "dialogh"   => "450",
        "dlg_res"   => true,
        "search"    => true
      ),

      "it5"    => Array(
        "label"     => "winner",
        "size"      => "35",
        "type"      => "str"
      ),

      "it6"    => Array(
        "label"     => "finaltext",
        "type"      => "customtext",
        "rows"      => "6",
        "cols"      => "30",
        "dialog"    => "?module=wysiwyg&canpreview='+((window.parent.dialogArguments)?1:0)+'&site_id=".$GLOBALS['site_id']."&page_id=".$this->page_id,
        "dialogw"   => "720",
        "dialogh"   => "450",
        "dlg_res"   => true,
        "search"    => true
      )

    );

    //Collection display table definition
    $this->columns = Array(

      "col0"        => Array(
        "width"     => "10%",
        "title"     => "Title"
      ),

      "col1"        => Array(
        "width"     => "10%",
        "title"     => "Start Date"
      ),

      "col2"        => Array(
        "width"     => "10%",
        "title"     => "End Date"
      ),

      "col3"        => Array(
        "width"     => "10%",
        "title"     => "Check date"
      ),

      "col4"        => Array(
        "width"     => "30%",
        "title"     => "Text"
      ),

      "col5"        => Array(
        "width"     => "10%",
        "title"     => "Winner"
      ),

      "col6"        => Array(
        "width"     => "30%",
        "title"     => "Final text"
      )

    );

  }


 function GetYears()
 {
    $data = $this->getDBData();
        
    $years = array();
    foreach ($data as $key=>$row)
    {
        //get years array
        $year = date("Y",$row['col1']);
        if (!in_array($year,$years)) $years[] = $year;
    }
    return $years;
 }

 function GetDBDataByYear($year)
 {
    $data = $this->getDBData();
    foreach ($data as $key=>$row)
    {
        $y = date("Y",$row['col1']);
        if ($y==$year) $return_data[] = $row;
    }
    return $return_data;
 }

 function GetActiveItems()
 {
    $data = $this->getDBData();

    foreach ($data as $key=>$row)
        if ($row['col2'] < time()) unset($data[$key]);

    return $data;
 }

 function GetDbData()
 {
    $data = parent::getDBData();
    
    if (!function_exists("cmp1"))
    {
        function cmp1($a, $b) 
        {
           if ($a['col1'] == $b['col1']) {
               return 0;
           }
           return ($a['col1'] > $b['col1']) ? -1 : 1;
        }
    }

    usort($data, "cmp1");

    return $data;
 }

}

?>