<?
//Serveris.LV Constructor collection module
//Created by Aleksandrs Selickis, 2005

include_once('class.dbcollection.php');

class newscollectionex extends dbcollection
{
  //Class initialization
  function NewsCollectionEx($name,$id)
  {
    $this->dbCollection($name,$id);
    $this->type = "newscollectionex";

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "Title:",
        "size"      => "35",
        "type"      => "str"
      ),

      "it1" => Array(
        "label"     => "Short text:",
        "type"      => "wysiwyg",
        "rows"      => "7",
        "cols"      => "50",
        "dialog"    => "?module=wysiwyg&site_id=".$GLOBALS['site_id'],
        "dialogw"   => "720",
        "dialogh"   => "450",
        "dlg_res"   => true,
        "value"     => '<P>&nbsp;</P>'
      ),

      "it2" => Array(
        "label"     => "Long text:",
        "type"      => "wysiwyg",
        "rows"      => "7",
        "cols"      => "50",
        "dialog"    => "?module=wysiwyg&site_id=".$GLOBALS['site_id'],
        "dialogw"   => "720",
        "dialogh"   => "450",
        "dlg_res"   => true,
        "value"     => '<P>&nbsp;</P>'
      ),

      "it3"    => Array(
        "label"     => "Date:",
        "type"      => "date",
        "time"      => 1
      ),

      "it4"    => Array(
        "label"     => "Author:",
        "type"      => "str",
        "size"      => "30"
      ),

      "it5"    => Array(
        "label"     => "Authorized:",
        "type"      => "hidden",
        "size"      => "30",
      ),

      "it6"    => Array(
        "label"     => "Archived:",
        "type"      => "hidden",
        "size"      => "30"
      ),

      "it7"    => Array(
        "label"     => "Start Date:",
        "type"      => "date",
        "time"      => 1
      ),

      "it8"    => Array(
        "label"     => "End Date:",
        "type"      => "date",
        "time"      => 1
      ),

      "it9"    => Array(
        "label"     => "Allow comments:",
        "type"      => "hidden",
        "size"      => "30"
      ),

      "it10"    => Array(
        "label"     => "Thumb image:",
        "type"      => "hidden"
      ),

      "it11"    => Array(
        "label"     => "Resized image:",
        "type"      => "hidden"
      ),

      "it12"    => Array(
        "label"     => "Original Image:",
        "type"      => "file",
        "uploadonly" => true,
      ),
    );

    //Collection display table definition
    $this->columns = Array(

      "col0"        => Array(
        "width"     => "10%",
        "title"     => "Title"
      ),

      "col1"    => Array(
        "width"     => "20%",
        "title"     => "Short Text"
      ),

      "col2"    => Array(
        "width"     => "30%",
        "title"     => "Text"
      ),

      "col3"     => Array(
        "width"     => "10%",
        "title"     => "Date",
        "timestamp" => true
      ),

      "col4"     => Array(
        "width"     => "10%",
        "title"     => "Author"
      ),

      "col7"     => Array(
        "width"     => "10%",
        "title"     => "Start Date",
        "timestamp" => true
      ),

      "col8"     => Array(
        "width"     => "10%",
        "title"     => "End Date",
        "timestamp" => true
      ),

    );

    //Set default value of date to current time
    //$this->properties['it3']['value'] = date("Y-m-d H:i:s", time());
      

    if ($_SESSION['userdata']['fname']!="" || $_SESSION['userdata']['lname']!="")
      $name = $_SESSION['userdata']['username']." (".$_SESSION['userdata']['fname']." ".$_SESSION['userdata']['lname'].")";
    else
      $name = $_SESSION['userdata']['username'];
    
    $this->properties['it4']['value'] = $name;

    $this->postInit();

  }

  function IsEditableOutside()
  {
    $this->description = 'Edit site news';
    $this->longname = $this->name;
    return false;
  }



  //Override GetDBData for postprocessing the retrieved data
  function GetDBData($listtype = 0, $isdesign = 0, $sortby = 0, $limit = 0)
  {

   if($limit)
     $limit = ' LIMIT ' . $limit;
   else
     $limit = '';

   if($sortby == 0)
     $sortcol = 'ind';
   else if($sortby == 1)
     $sortcol = 'col3 DESC';
   else if($sortby == 2)
     $sortcol = 'col7 DESC';
   else if($sortby == 3)
     $sortcol = 'col8';
   else
     $sortcol = 'ind';


   if ($_GET['module']=="collections")
   {
        $listtype = 2;
        $isdesign = 1;
   }

    //move expired to archive and set expiration date
    $today = getdate();
    $time = mktime(0, 0, 0, $today['mon'], $today['mday'], $today['year']+10);
    sqlQuery("UPDATE `".$this->site_id."_coltable_".$this->name."` SET col6=1, col8=".$time." WHERE col8<".time());

    if ($listtype==2)
    {
        //get all authorized items
        //$r = sqlQueryData("SELECT * FROM `".$this->site_id."_coltable_".$this->name."` WHERE col5='1' ORDER BY ind");
        $r = sqlQueryData("SELECT * FROM `".$this->site_id."_coltable_".$this->name."` ".(($isdesign==0)?"WHERE col5='1' AND col7<".time():"WHERE col7<".time())." AND col8>".time()." ORDER BY " . $sortcol . $limit);
    }
    else if ($listtype==1)
    {
        //get archived and authorized items
        //$r = sqlQueryData("SELECT * FROM `".$this->site_id."_coltable_".$this->name."` WHERE col5='1' AND col6='1' ORDER BY ind");
        $r = sqlQueryData("SELECT * FROM `".$this->site_id."_coltable_".$this->name."` WHERE ".($isdesign==0?" col5='1' AND ":"")." col6='1' AND col7<".time()." AND col8>".time()." ORDER BY " . $sortcol . $limit);
    }
    else
    {
        //get not archived and authorized items
        //$r = sqlQueryData("SELECT * FROM `".$this->site_id."_coltable_".$this->name."` WHERE col5='1' AND col6='0' ORDER BY ind");
        $r = sqlQueryData("SELECT * FROM `".$this->site_id."_coltable_".$this->name."` WHERE ".($isdesign==0?" col5='1' AND ":"")." col6='0' AND col7<".time()." AND col8>".time()." ORDER BY " . $sortcol . $limit);
    }

    return $r;
  }

  function FirstValidItem($listtype=0)
  {
    if(!$this->name)
    {
      $this->error = true;
      return false;
    }

    if ($listtype==2) //all
    {
        //all
        $i = sqlQueryValue("SELECT Min(ind) FROM `".$this->site_id."_coltable_".$this->name."` WHERE col5='1' AND col7<".time()." AND col8>".time()." LIMIT 1");
        return $i;
    }
    else if ($listtype==1) //archived
    {
        //archived
        $i = sqlQueryValue("SELECT Min(ind) FROM `".$this->site_id."_coltable_".$this->name."` WHERE col5='1' AND col6='1' AND col7<".time()." AND col8>".time()." LIMIT 1");
        return $i;
    }
    else //not archived 
    {
        //not archived
        $i = sqlQueryValue("SELECT Min(ind) FROM `".$this->site_id."_coltable_".$this->name."` WHERE col5='1' AND col6='0' AND col7<".time()." AND col8>".time()." LIMIT 1");
        return $i;
    }
  }

  function Archive($itemcount)
  {
    sqlQuery("UPDATE `".$this->site_id."_coltable_".$this->name."` SET col6='1' WHERE ind>$itemcount");
  }


  //Override ChangeItem to maintain order of news items sorted by date
  function ChangeItem($ind, $properties)
  {
    //Call the original ChangeItem()
    parent::ChangeItem($ind, $properties);
    //Sort list by date
    $a = $this->getItem($ind);
    $date = $a[3];
    do
    {
      $lastind = $ind;
      $a = $this->getItem($ind-1);
      if($a[3]<$date)
      {
        $ind = parent::moveItemUp($ind);
      }else
      {
        $a = $this->getItem($ind+1);
        if($a[3]>$date)
        {
          $ind = parent::moveItemDown($ind);
        }
      }
    }while($ind!=$lastind);  //Stop when row hasn't been moved
    return $ind;
  }

  function ExistsItemEx($ind,$direction,$listtype)
  {
    if ($listtype==2) $cond = " AND col5='1'";
    else if ($listtype==1) $cond = " AND col5='1' AND col6='1'";
    else $cond = " AND col5='1' AND col6='0'";

    if(!$this->name)
    {
      $this->error = true;
      return false;
    }
    //$i = sqlQueryValue("SELECT ind FROM `".$this->site_id."_coltable_".$this->name."` WHERE ind='$ind'");
    if ($direction=="+")
        $i = sqlQueryValue("SELECT min(ind) FROM `".$this->site_id."_coltable_".$this->name."` WHERE ind>'$ind'".$cond);
    if ($direction=="-")
        $i = sqlQueryValue("SELECT max(ind) FROM `".$this->site_id."_coltable_".$this->name."` WHERE ind<'$ind'".$cond);

    if($i)
      return $i;
    else
      return 0;
  }

  //find next item to change of same type
  function MoveItemUp($ind, $listtype)
  {
    //find item with lower index
    $newind = $this->ExistsItemEx($ind,"-",$listtype);
    if($newind > 0)
    {
      $this->SwapItems($ind, $newind);
      return($newind);
    }
    return($ind);
  }
  
  //find next item to change of same type
  function MoveItemDown($ind, $listtype)
  {
    //find item with greater index
    $newind = $this->ExistsItemEx($ind,"+",$listtype);
    if($newind > 0)
    {
      $this->SwapItems($ind,$newind);
      return($newind);
    }
    return($ind);
  }

    function GetNewsNameById($news_id)
  {
    if(!$this->name)
    {
      $this->error = true;
      return false;
    }
    $data = sqlQueryData("SELECT `col0` FROM  `".$this->table."` WHERE `item_id`=".$news_id." LIMIT 1");
    return $data;
  }

  function GetAllNewsNames()
  {
    if(!$this->name)
    {
      $this->error = true;
      return false;
    }
    $data = sqlQueryData("SELECT `col0`, `item_id` FROM  `".$this->table."` ORDER BY `item_id` DESC");
    return $data;
  }

  function SearchInDbZina($field)
  {
    if(!$this->name)
    {
      $this->error = true;
      return false;
    }
    $data = sqlQueryData("SELECT `col0`, `item_id` FROM `".$this->table."` WHERE `item_id`='".$field."'");
    return $data;
  }


}

?>
