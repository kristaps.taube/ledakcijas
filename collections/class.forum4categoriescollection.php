<?

include_once('class.dbcollection.php');

class forum4categoriescollection extends dbcollection
{
  //Class initialization
  function forum4categoriescollection($name,$id)
  {
    $this->dbcollection($name,$id);
    $this->type = "forum4categoriescollection";

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "Category title",
        "type"      => "str",
        "column_name" => "cat_title"
      ),

      "it1"    => Array(
        "label"     => "Description",
        "type"      => "html",
        "rows"      => "6",
        "cols"      => "30",
        "column_name" => "cat_description",

      ),

      "it2"    => Array(
        "label"     => "Update time",
        "type"      => "date",
        "time"      => true,
        "column_name" => "cat_updatetime"
      ),

      "it3"    => Array(
        "label"     => "Thread count",
        "type"      => "str",
        "column_name" => "cat_threadcount"
      ),

    );

    //Collection display table definition
    $this->columns = Array(

      "cat_title"        => Array(
        "width"     => "20%",
        "title"     => "Title"
      ),

      "cat_description"        => Array(
        "width"     => "40%",
        "title"     => "Description"
      ),

      "cat_updatetime"        => Array(
        "width"     => "20%",
        "title"     => "Update time"
      ),

      "cat_threadcount"        => Array(
        "width"     => "20%",
        "title"     => "Thread count"
      ),

    );

    $this->postInit();
  }


  function CategoryThreadsChanged($cat_id, $threadcol)
  {
    $updatetime = date('Y-m-d H:i:s');
    if($threadcol)
      $threadcount = $threadcol->countThreadsForCategory($cat_id);
    else
      $threadcount = 0;
    $this->changeItemByIdAssoc($cat_id, Array('cat_updatetime' => $updatetime, 'cat_threadcount' => $threadcount));
  }

}

?>
