<?
//Serveris.LV Constructor collection module
//Created by Aivars Irmejs, 2003, aivars@serveris.lv

include_once('class.collection.php');

class NewsCollection extends collection
{
  //Class initialization
  function NewsCollection($name,$id)
  {
    $this->Collection($name,$id);
    $this->type = "newscollection";

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "Title:",
        "size"      => "35",
        "type"      => "str"
      ),

      "it1" => Array(
        "label"     => "Short text:",
        "type"      => "wysiwyg",
        "rows"      => "7",
        "cols"      => "50",
        "dialog"    => "?module=wysiwyg&site_id=".$GLOBALS['site_id'],
        "dialogw"   => "720",
        "dialogh"   => "450",
        "dlg_res"   => true
      ),

      "it2" => Array(
        "label"     => "Long text:",
        "type"      => "wysiwyg",
        "rows"      => "7",
        "cols"      => "50",
        "dialog"    => "?module=wysiwyg&site_id=".$GLOBALS['site_id'],
        "dialogw"   => "720",
        "dialogh"   => "450",
        "dlg_res"   => true
      ),

      "it3"    => Array(
        "label"     => "Date:",
        "type"      => "date",
        "dialog"    => "?module=dialogs&action=date&site_id=".$GLOBALS['site_id']."&time=1",
        "dialogw"   => "430",
        "dialogh"   => "250",
      )

    );

    //Collection display table definition
    $this->columns = Array(

      "col0"        => Array(
        "width"     => "20%",
        "title"     => "Title"
      ),

      "col1"    => Array(
        "width"     => "30%",
        "title"     => "Short Text"
      ),

      "col2"    => Array(
        "width"     => "30%",
        "title"     => "Text"
      ),

      "col3"     => Array(
        "width"     => "20%",
        "title"     => "Date"
      )


    );

    //Set default value of date to current time
    $this->properties['it3']['value'] = date("Y-m-d H:i:s", time());


  }

  function IsEditableOutside()
  {
    $this->description = 'Edit site news';
    $this->longname = $this->name;
    return true;
  }



  //Override GetDBData for postprocessing the retrieved data
  function GetDBData($changeDate = true)
  {
    //Call original GetDBData()
    $r = parent::GetDBData();
    //Change date to short date format
    if($changeDate)
    {
      foreach($r as $key => $row)
      {
        $r[$key]["col3"] = date('d.m.Y',strtotime($r[$key]["col3"]));
      }
    }
    return $r;
  }

  //Override ChangeItem to maintain order of news items sorted by date
  function ChangeItem($ind, $properties)
  {
    //Call the original ChangeItem()
    parent::ChangeItem($ind, $properties);
    //Sort list by date
    $a = $this->getItem($ind);
    $date = $a[3];
    do
    {
      $lastind = $ind;
      $a = $this->getItem($ind-1);
      if($a[3]<$date)
      {
        $ind = parent::moveItemUp($ind);
      }else
      {
        $a = $this->getItem($ind+1);
        if($a[3]>$date)
        {
          $ind = parent::moveItemDown($ind);
        }
      }
    }while($ind!=$lastind);  //Stop when row hasn't been moved
    return $ind;
  }


}

?>
