<?
//Serveris.LV Constructor collection module
//Created by Kristaps Grinbergs, 2006

include_once('class.dbcollection.php');

class shoprekinicollection extends dbcollection
{
  //Class initialization
  function shoprekinicollection ($name,$id)
  {
    $this->dbCollection($name,$id);
    $this->type = "shoprekinicollection";

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "User Id:",
        "type"      => "relation",
        "related_collection" => "shopusercollection",
        "relation_format" => "{%email%}",
        "column_name" => "user_id",
        "column_type" => "int(10)",
        "column_index" => 1
      ),

      "it1" => Array(
        "label"     => "Datums:",
        "type"      => "date",
        'timestamp' => true,
        "column_name" => "datums",
        "column_type" => "datetime",
        "column_index" => 1
      ),

      "it2" => Array(
        "label"     => "Rekins:",
        "type"      => "wysiwyg",
        "rows"      => "7",
        "cols"      => "50",
        "dialog"    => "?module=wysiwyg&site_id=".$GLOBALS['site_id'],
        "dialogw"   => "720",
        "dialogh"   => "450",
        "dlg_res"   => true,
        "column_name" => "rekins"
      ),

      "it3" => Array(
        "label"     => "Summa:",
        "type"      => "str",
        "column_name" => "summa",
        'column_type' => 'decimal(8,2)'
      ),

      "it4" => Array(
        "label"     => "Pasūtītājs:",
        "type"      => "str",
        "column_name" => "pasutitajs",
        "column_type" => "varchar(255)"
      ),

      "it5" => Array(
        "label"     => "Piegādāts:",
        "type"      => "check",
        "column_name" => "piegadats",
        "column_type" => "tinyint(1)"
      ),

      "it6" => Array(
        "label"     => "Apmaksāts:",
        "type"      => "check",
        "column_name" => "apmaksats",
        "column_type" => "tinyint(1)"
      ),

      "it7" => Array(
        "label"     => "Apmaksas veids:",
        "type"      => "list",
        "lookup"    => Array("sanemot: Saņemot rēķinu", "karte: Ar norēķinu karti"),
        "column_name" => "apmaksas_veids",
        "column_type" => "varchar(255)"
      ),

      "it8" => Array(
        "label"     => "Kopējā summa:",
        "type"      => "str",
        "column_name" => "kopeja_summa",
        'column_type' => 'decimal(8,2)'
      ),

      "it9" => Array(
        "label"     => "Order data:",
        "type"      => "hidden",
        "column_name" => "order_data"
      ),

      "it10" => Array(
        "label"     => "Nosūtīts:",
        "type"      => "hidden",
        "column_name" => "sent",
        "column_type" => "tinyint(1)"
      ),

      "it11" => Array(
        "label"     => "Reģistrēts lietotājs:",
        "type"      => "check",
        "column_name" => "user_is_reg",
        "column_type" => "tinyint(1)",
        "column_index" => 1
      ),

      "it12" => Array(
        "label"     => "Telefons:",
        "type"      => "str",
        "column_name" => "telefons",
        "column_type" => "varchar(100)",
        "column_index" => 1
      ),

      "it13" => Array(
        "label"     => "E-pasts:",
        "type"      => "str",
        "column_name" => "epasts",
        "column_type" => "varchar(100)",
        "column_index" => 1
      ),

      "it14" => Array(
        "label"     => "Adrese:",
        "type"      => "str",
        "column_name" => "adrese",
        "column_type" => "varchar(255)",
        "column_index" => 1
      ),

      "it15"    => Array(
        "column_name" => "airpay_response",
        "label"     => "Airpay Response",
        "type"      => "hidden",
      ),

      "it16"    => Array(
        "column_name" => "airpay_status",
        "label"       => "Airpay Status",
        "type"        => "hidden",
        'column_type' => 'varchar(3)',
      ),

      "it17" => Array(
        "label"     => "Status:",
        "type"      => "str",
        "column_name" => "status_id",
        "value" => 1,
        "column_type" => "int(10)",
        "column_index" => 1
      ),

      "it18" => Array(
        "label"     => "Apmaksas datums:",
        "type"      => "date",
        'timestamp' => true,
        "column_name" => "apmaksas_datums",
        "column_type" => "datetime",
        "column_index" => 1
      ),

      "it19" => Array(
        "label"     => "Piegādes datums:",
        "type"      => "date",
        'timestamp' => true,
        "column_name" => "piegades_datums",
        "column_type" => "datetime",
        "column_index" => 1
      ),

			"it20" => Array(
        "label"     => "Reminder sent:",
        "type"      => "str",
        "column_name" => "reminder_sent",
        "default_value" => 0,
        "column_type" => "tinyint(1)",
        "column_index" => 1
      ),
			
			"it21" => Array(
        "label"     => "Review sent:",
        "type"      => "str",
        "column_name" => "review_sent",
        "default_value" => 0,
        "column_type" => "tinyint(1)",
        "column_index" => 1
      ),
			
			"it22" => Array(
        "label"     => "Review stars:",
        "type"      => "str",
        "column_name" => "review_stars",
        "default_value" => 0,
        "column_type" => "int(1)",
        "column_index" => 1
      ),
			
			"it23" => Array(
        "label"     => "Review hash:",
        "type"      => "str",
        "column_name" => "review_hash",
        "default_value" => 0,
        "column_type" => "VARCHAR(32)",
        "column_index" => 1
      ),
			
    );

    //Collection display table definition
    $this->columns = Array(

      "user_id"        => Array(
        "title"     => "User Id"
      ),

      "datums"    => Array(
        "title"     => "Datums"
      ),

      "rekins"    => Array(
        "title"     => "Rēķins",
        "format"   => '<a href="#" onclick="javascript:document.getElementById(\'rekdiv{%item_id%}\').style.display = \'block\'; this.style.display = \'none\';">Rādīt</a><div id="rekdiv{%item_id%}" style="display: none;">{%_self%}</div>'
      ),

      "summa"    => Array(
        "title"     => "Summa"
      ),
			
			"review_sent"    => Array(
        "title"     => "Novērtējumam sūtīts"
      ),
			
			"review_stars"    => Array(
        "title"     => "Novērtējums"
      ),
			
			"review_hash"    => Array(
        "title"     => "Novērtējuma hash",
      ),
			
    );

    $this->postInit();

  }

  function IsEditableOutside()
  {
    $this->description = 'Lietotaju rekini';
    $this->longname = $this->name;
    return true;
  }

  function getDataForUser($id)
  {
    if(!$this->name)
    {
      $this->error = true;
      return false;
    }
    $data = sqlQueryData("SELECT * FROM `".$this->table."` WHERE user_id= ".$id." ORDER BY datums");
    return $data;
  }

  function getNextId(){
    $row = DB::GetRow("SHOW TABLE STATUS LIKE '".$this->table."'");
    return $row['Auto_increment'];
  }

  function GetOrdersWithUsers($usercol, $sortby, $page, $pagesize, $asc, $params=null)
  {
    if(!$sortby)
    {
      $sortby = 'O.datums';
      $asc = 0;
    }

    $cond = array();

    if (!empty($params['phone']))
      $cond[] = 'O.telefons LIKE "%'.$params['phone'].'%"';

    if (!empty($params['email']))
      $cond[] = 'O.epasts LIKE "%'.$params['email'].'%"';

    if (!empty($params['name']))
      $cond[] = 'O.pasutitajs LIKE "%'.$params['name'].'%"';

    if (!empty($params['address']))
      $cond[] = 'O.adrese LIKE "%'.$params['address'].'%"';

    if(!empty($params['date_from'])){
      $cond[] = "O.datums > '".convertDateToMySQL($params['date_from'])." 00:00:00'";
    }

    if(!empty($params['date_to'])){
      $cond[] = "O.datums < '".convertDateToMySQL($params['date_to'])." 23:59:59'";
    }

   $sql = '';

    if ($cond)
      $sql .= ' WHERE '.implode(' AND ', $cond);


    return array(
      sqlQueryData("SELECT SQL_CALC_FOUND_ROWS O.*, U.email
                         FROM `".$this->table."` O
                         LEFT JOIN `".$usercol->table."` U
                           ON O.user_id = U.item_id
                         ".$sql."
                         ORDER BY " . $sortby . ($asc ? ' ASC' : ' DESC') . "
                         LIMIT " . ($page * $pagesize) . ',' . $pagesize),
      sqlQueryValue('SELECT FOUND_ROWS()')
    );
  }

  function getOrdersByDateInterval($from, $to)
  {
    if (!$from && !$to) return null;

    $where = Array();
    if ($from) $where[]= "datums >= '".date('Y-m-d H:i', $from)."'";
    if ($to) $where[] = "datums <= '".date('Y-m-d H:i', $to)."'";

    return sqlQueryData("SELECT * FROM `".$this->table."` WHERE ".implode(' AND ', $where)." ORDER BY datums DESC");
  }

  function getData($params)
  {
    $sql = '';

    $cond = array();
    if (!empty($params['datums_from']))
      $cond[] = 'datums >= "'.date('Y-m-d H:i:s', strtotime($params['datums_from'])).'"';

    if (!empty($params['datums_to']))
    {
      if (strpos($params['datums_to'], ':') !== false)
        $to = strtotime($params['datums_to']);
      else
        $to = strtotime($params['datums_to']) + 86400;

      $cond[] = 'datums < "'.date('Y-m-d H:i:s', $to).'"';
    }

    if ($cond)
      $sql .= ' WHERE '.implode(' AND ', $cond);

//  echo 'SELECT * FROM '.$this->table.$sql.' ORDER BY item_id DESC';

    return sqlQueryDataAssoc('SELECT * FROM '.$this->table.$sql.' ORDER BY item_id DESC');
  }

  function getProductStats($orderprodcol, $prodscol, $from, $to, $params){

    $cond = array(
      'datums >= "'.date('Y-m-d', strtotime($from)).' 00:00:00"',
      'datums <= "'.date('Y-m-d', strtotime($to)).' 23:59:59"'
    );

    if ($params['min_times']){
      $cond[] = '
        (
          SELECT
            SUM(op2.count)
          FROM
            '.$this->table.' o2
              JOIN '.$orderprodcol->table.' op2 ON op2.order_id=o2.item_id
              JOIN '.$prodscol->table.' p2 ON op2.prod_id=p2.item_id
          WHERE
            datums >= "'.date('Y-m-d H:i:s', strtotime($from)).'" AND
            datums <= "'.date('Y-m-d H:i:s', strtotime($to)).'" AND
            op2.prod_id=op.prod_id
        ) > '.(int)$params['min_times'].'
      ';
    }

    $orderby = 'o.datums';
    if ($params['type'] == 'most_popular')
    {
      $orderby = '
        (
          SELECT
            SUM(op2.count)
          FROM
            '.$this->table.' o2
              JOIN '.$orderprodcol->table.' op2 ON op2.order_id=o2.item_id
              JOIN '.$prodscol->table.' p2 ON op2.prod_id=p2.item_id
          WHERE
            datums >= "'.date('Y-m-d', strtotime($from)).' 00:00:00" AND
            datums <= "'.date('Y-m-d', strtotime($to)).' 23:59:59" AND
            op2.prod_id=op.prod_id
        ) DESC
      ';
    }

    if ($params['prod_limit'])
    {
      $data = $this->findMostPopularProducts($orderprodcol, $prodscol, $from, $to, $params['prod_limit']);
      $ids = array();
      foreach ($data as $row)
      {
        $ids[] = $row['prod_id'];
      }

      if (!$ids) return array();

      $cond[] = 'op.prod_id IN ('.implode(',', $ids).')';
      $orderby = 'count DESC';
    }

    $sql = '
      SELECT
        op.prod_id,
        SUM(op.count) AS count,
        DATE(o.datums) AS date,
        p.name_lv
      FROM
        '.$this->table.' o
          JOIN '.$orderprodcol->table.' op ON op.order_id=o.item_id
          JOIN '.$prodscol->table.' p ON op.prod_id=p.item_id
      WHERE
       '.implode(' AND ', $cond).'
      GROUP BY o.datums, op.prod_id		
      ORDER BY '.$orderby.'
    ';

    return sqlQueryData($sql);

  }

  function findMostPopularProducts($orderprodcol, $prodscol, $from, $to, $limit)
  {
    $cond = array(
      'datums >= "'.date('Y-m-d H:i:s', strtotime($from)).'"',
      'datums < "'.date('Y-m-d H:i:s', strtotime($to)).'"'
    );

    $sql = '
      SELECT prod_id, count FROM (
        SELECT
          op.prod_id,
          SUM(op.count) AS count
        FROM
          '.$this->table.' o
            JOIN '.$orderprodcol->table.' op ON op.order_id=o.item_id
            JOIN '.$prodscol->table.' p ON op.prod_id=p.item_id
        WHERE
         '.implode(' AND ', $cond).'
        GROUP BY op.prod_id
      ) t ORDER BY count DESC LIMIT '.(int)$limit.'
    ';

    return sqlQueryData($sql);
  }
	
	function EditToolbar()
  {
   $site_id = $this->site_id;
    $category_id = $this->collection_id;
    $collectiontype = $this->type;

    $Toolbar->backendroot = $GLOBALS['cfgWebRoot'];

     require($GLOBALS['cfgDirRoot']."library/"."class.toolbar2.php");
    $Toolbar = new Toolbar2();

    $Toolbar->AddSpacer();

    // $Toolbar->AddButtonImageText("add", 'collection_new', "{%langstr:add_new%}", "", "openDialog('?module=collections&action=newcolitem&site_id=$site_id&page_id=$page_id&category_id=$category_id&coltype=".$collectiontype."&session_id=".session_id()."&session_name=".session_name()."', 400, 460, 1, 1);", 85, "{%langstr:hint_new_collection_item%}");
    // $Toolbar->AddButtonImage("edit", 'collection_edit', "{%langstr:col_edit%}", "", "openDialog('?module=collections&action=editcolitem&site_id=$site_id&page_id=$page_id&category_id=$category_id&coltype=".$collectiontype."&session_id=".session_id()."&session_name=".session_name()."&id='+SelectedRowID, 400, 460, 1, 1);", 31, "{%langstr:hint_edit_collection_item%}");
    // if($this->CanChangeOrder())
    // {
      // $Toolbar->AddButtonImage("up", 'up', "{%langstr:up%}", "javascript:if(SelectedRowID)window.location='?module=collections&site_id=$site_id&page_id=$page_id&action=moveup&category_id=$category_id&coltype=".$_GET['coltype']."&justedit=".$_GET['justedit']."&session_id=".session_id()."&session_name=".session_name()."&main=1&selrow='+SelectedRowID+'&id='+SelectedRowID;", '', 31, "");
      // $Toolbar->AddButtonImage("down", 'down', "{%langstr:down%}", "javascript:if(SelectedRowID)window.location='?module=collections&site_id=$site_id&page_id=$page_id&action=movedown&category_id=$category_id&coltype=".$_GET['coltype']."&justedit=".$_GET['justedit']."&session_id=".session_id()."&session_name=".session_name()."&main=1&selrow='+SelectedRowID+'&id='+SelectedRowID;", '', 31, "");
    // }
    // $Toolbar->AddButtonImage("del", 'delete', "{%langstr:col_del%}", "javascript: if(SelectedRowID && SelectedRowID!=-1) window.location='?module=collections&action=delcolitem&site_id=$site_id&page_id=$page_id&category_id=$category_id&coltype=".$collectiontype."&justedit=".$_GET['justedit']."&session_id=".session_id()."&session_name=".session_name()."&main=1&id='+SelectedRowID; else if(!SelectedRowIDsEmpty) window.location='?module=collections&action=delcolitem&site_id=$site_id&page_id=$page_id&category_id=$category_id&coltype=".$collectiontype."&justedit=".$_GET['justedit']."&session_id=".session_id()."&session_name=".session_name()."&main=1&id='+SelectedRowIDs;", '', 31, "{%langstr:hint_delete_collection_item%}", "{%langstr:ask_delete_collection_item%}");
    // $Toolbar->AddSeperator();
    if(!is_array($_SESSION['collections_viewfilter_' . $this->name]) || !count($_SESSION['collections_viewfilter_' . $this->name]))
      $filterimg = 'collection_find';
    else
      $filterimg = 'collection_find_checked';
    $Toolbar->AddButtonImage("filter", $filterimg, "{%langstr:col_filter%}", "", "openDialog('?module=collections&action=filtercollection&site_id=$site_id&page_id=$page_id&category_id=$category_id&coltype=".$collectiontype."&session_id=".session_id()."&session_name=".session_name()."', 400, 460, 1, 1);", 31, "{%langstr:hint_filter_collection%}");
    if($filterimg == 'collection_find_checked')
    {
      $Toolbar->AddButtonImageText("remfilter", 'collection_find_clear', "{%langstr:col_showall%}", "javascript:window.location='?module=collections&action=filtercollectionclear&site_id=$site_id&page_id=$page_id&category_id=$category_id&coltype=".$collectiontype."&justedit=".$_GET['justedit']."&session_id=".session_id()."&session_name=".session_name()."&main=1'", '', 66, "{%langstr:hint_collection_find_clear%}");
    }
    if(!$_GET['justedit'])
    {
      $Toolbar->AddSeperator();
      $Toolbar->AddButtonImage("properties", 'sitemap_properties', "{%langstr:toolbar_properties%}", "", "openDialog('?module=collections&action=colproperties&site_id=$site_id&page_id=$page_id&category_id=$category_id&coltype=".$collectiontype."&session_id=".session_id()."&session_name=".session_name()."', 400, 460, 1, 1);", 31, "{%langstr:hint_collection_properties%}");
      $Toolbar->AddButtonImage("copycol", 'sitemap_copy', "{%langstr:toolbar_copy_collection%}", "", "openDialog('?module=collections&action=copycollection&site_id=$site_id&page_id=$page_id&category_id=$category_id&coltype=".$collectiontype."&session_id=".session_id()."&session_name=".session_name()."', 400, 460, 1, 1);", 31, "{%langstr:hint_copy_collection%}");

      $Toolbar->AddSeperator();
      $Toolbar->AddSeperator();
      $Toolbar->AddButtonImage("delc", 'collection_delete', "{%langstr:toolbar_delete_collection%}", "javascript: window.location='?module=collections&action=delcollection&site_id=$site_id&page_id=$page_id&category_id=$category_id&coltype=".$collectiontype."&main=1&session_id=".session_id()."&session_name=".session_name()."'", '', 31, "{%langstr:hint_delete_collection%}", "{%langstr:ask_delete_collection%}");


    }

    //$Toolbar->AddSeperator();

    $this->GetExtraToolbarButtons($Toolbar, $extraitembuttons, $extrabuttons);
    if($extraitembuttons)
    {
      foreach($extraitembuttons as $key => $item)
      {
        $extraitembuttons[$key] = '"' . $item . '"';
      }
      $extraitembuttons = ',' . implode(', ', $extraitembuttons);
    }else
    {
      $extraitembuttons = '';
    }
    if($extrabuttons)
    {
      foreach($extrabuttons as $key => $item)
      {
        $extrabuttons[$key] = '"' . $item . '"';
      }
      $extrabuttons = ',' . implode(', ', $extrabuttons);
    }else
    {
      $extrabuttons = '';
    }

    $return = $Toolbar->output();


    echo '<script language="JavaScript" type="text/javascript">
		/*<![CDATA[*/
		che = new Array("edit", "up", "down", "del"'.$extraitembuttons.');
		che2 = new Array("add","edit", "up", "down", "del", "properties", "delc", "filter", "remfilter"'.$extrabuttons.');

		DisableToolbarButtons(che);
		/*]]>*/
		</script>';

		if($_GET['selrow'])
		{
			echo '<script language="JavaScript">
						<!--
							var tablerow' . $_GET['selrow'] . ' = document.getElementById("tablerow' . $_GET['selrow'] . '");
							if(typeof(tablerow' . $_GET['selrow'] . ')!= "undefined" && tablerow' . $_GET['selrow'] . ')
							{
								preEl = tablerow' . $_GET['selrow'] . ';
								ChangeTextColor(tablerow' . $_GET['selrow'] . ',\'tableRowSel\');
								SelectedRowID = ' . $_GET['selrow'] . ';
								if(SelectedRowID){ EnableToolbarButtons(che); }else { DisableToolbarButtons(che); }
							}
						<!---->
						</script>';
		}


    require($GLOBALS['cfgDirRoot']."library/"."class.rightmenu.php");
    $rightmenu = new rightmenu;

    // $rightmenu->addItem('{%langstr:add_new%}','openDialog(\'?module=collections&action=newcolitem&site_id='.$site_id.'&page_id='.$page_id.'&category_id='.$category_id.'&coltype='.$collectiontype.'&session_id='.session_id().'&session_name='.session_name().'\', 400, 460, 1, 1);',false,false,'collection_new');
    // $rightmenu->addItem('{%langstr:col_edit%}',"openDialog('?module=collections&action=editcolitem&site_id=$site_id&page_id=$page_id&category_id=$category_id&coltype=".$collectiontype."&session_id=".session_id()."&session_name=".session_name()."&id='+SelectedRowID, 400, 460, 1, 1);",false,false,'collection_edit');
    // $rightmenu->addItem('{%langstr:up%}',"javascript:if(SelectedRowID)window.location='?module=collections&site_id=$site_id&page_id=$page_id&action=moveup&category_id=$category_id&coltype=".$_GET['coltype']."&justedit=".$_GET['justedit']."&session_id=".session_id()."&session_name=".session_name()."&main=1&selrow='+SelectedRowID+'&id='+SelectedRowID",false,false,'up');
    // $rightmenu->addItem('{%langstr:down%}',"javascript:if(SelectedRowID)window.location='?module=collections&site_id=$site_id&page_id=$page_id&action=movedown&category_id=$category_id&coltype=".$_GET['coltype']."&justedit=".$_GET['justedit']."&session_id=".session_id()."&session_name=".session_name()."&main=1&selrow='+SelectedRowID+'&id='+SelectedRowID;",false,false,'down');
    // $rightmenu->addItem('{%langstr:col_del%}',"javascript: var agree = confirm(unescape('Are you sure to delete?')); if (agree) { if(SelectedRowID)window.location='?module=collections&action=delcolitem&site_id=$site_id&page_id=$page_id&category_id=$category_id&coltype=".$collectiontype."&justedit=".$_GET['justedit']."&session_id=".session_id()."&session_name=".session_name()."&main=1&id='+SelectedRowID; }",false,false,'delete');

/*    if(!$_GET['justedit'])
    {
      $rightmenu->addSpacer();
      $rightmenu->addSpacer();
      $rightmenu->addSpacer();
      $rightmenu->addItem('{%langstr:toolbar_delete_collection%}',"javascript: var agree = confirm(unescape('Are you sure to delete?')); if (agree) { window.location='?module=collections&action=delcollection&site_id=$site_id&page_id=$page_id&category_id=$category_id&coltype=".$collectiontype."&main=1&session_id=".session_id()."&session_name=".session_name()."' }",false,false,'collection_delete');
    }*/ 

    return  $return.$rightmenu->output();
  }
	
	function GetEditData($page, &$count, &$recordposition, $order = ''){
    $query_params = $this->GetDefaultQueryParams($page, $order);
    $count = $this->ItemCount($query_params['countwhere']);
    if(isset($recordposition) && $recordposition)
    {
      $recordposition = $this->GetDBRecordPosition($query_params, $recordposition);
    }
    $data = $this->GetDBData($query_params);

    foreach($data as &$entry){

      $entry['review_sent'] = $entry['review_sent'] ? "Jā" : "Nē";
      $entry['review_stars'] = $entry['review_stars'] ? $entry['review_stars'] : "Nav vērtēts";

    }

    return $data;

  }
	
}	
?>