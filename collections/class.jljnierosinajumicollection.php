<?
//Serveris.LV Constructor component
//Created by Aleksandrs Selickis, 2005, alex@datateks.lv
//Project: JLJN

include_once('class.collection.php');

class jljnierosinajumicollection extends collection
{
  //Class initialization
  function jljnierosinajumicollection($name,$id)
  {
    $this->Collection($name,$id);
    $this->type = "jljnierosinajumicollection";

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "Author",
        "size"      => "35",
        "type"      => "str"
      ),

	  "it1"    => Array(
        "label"     => "Text:",
        "type"    => "html",
        "wrap"    => "none",
        "rows"    => "6",
        "cols"    => "30",

      ),

      "it2"    => Array(
        "label"     => "Date",
        "size"      => "35",
        "type"      => "hidden"
      )

    );

    //Collection display table definition
    $this->columns = Array(

      "col0"        => Array(
        "width"     => "30%",
        "title"     => "Author"
      ),

      "col1"        => Array(
        "width"     => "60%",
        "title"     => "Text"
      ),

      "col2"        => Array(
        "width"     => "10%",
        "title"     => "Text"
      )    

    );

  }

}

?>