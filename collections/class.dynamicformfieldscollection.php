<?

include_once('class.dbcollection.php');

class dynamicformfieldscollection extends dbcollection
{
  function dynamicformfieldscollection($name, $id)
  {
    $this->DBcollection($name, $id);
    $this->type = "dynamicformfieldscollection";

    $this->properties_assoc = Array(

      "component_id" => Array(
        "label"     => "Component ID:",
        "type"      => "hidden",
        "column_type" => "int(10)",
        "column_index" => 1
      ),

      "type" => Array(
        "label"     => "Type:",
        "type"      => "list",
        'lookupassoc' => array(
          '' => '(neviens)',
          'text' => 'Text',
          'textarea' => 'Multiline text',
          'submit' => 'Submit button',
          'title' => 'Title',
          'list' => 'Drop-down list',
          'radiolist' => 'Radio buttons',
          'checklist' => 'Checkboxes',
          'line' => 'Separator - line',
          'space' => 'Separator - empty line'
        )
      ),

      "label" => Array(
        "label"     => "Name:",
        "type"      => "str",
        "multilang" => 1
      ),

      "items" => Array(
        "label"     => "Items:",
        "type"      => "customtext",
        "rows"      => 7,
        "multilang" => 1
      ),

      "first_item_selected" => Array(
        "label"     => "First item selected:",
        "type"      => "check",
        "column_type" => "tinyint(1)"
      ),

      "validate_type" => Array(
        "label"     => "Validation type:",
        "type"      => "list",
        "lookupassoc" => array('' => 'without validation', 'email' => 'e-mail address')
      ),

      "req" => Array(
        "label"     => "Required to fill:",
        "type"      => "check",
        "column_type" => "tinyint(1)"
      ),

    );

    $this->columns = Array(
      "name_lv"     => Array(
        "title"     => "Name:",
      ),

    );

    $this->postInit();
  }

  function getData($params)
  {
    $sql = '';

    $cond = array();
    if ($params['component_id'])
      $cond[] = 'component_id='.(int)$params['component_id'];

    if ($cond)
      $sql .= ' WHERE '.implode(' AND ', $cond);

    return sqlQueryDataAssoc('SELECT * FROM '.$this->table.$sql.' ORDER BY ind');
  }

  function copyItems($src_component_id, $dest_component_id)
  {
    $data = $this->getData(array('component_id' => $src_component_id));
    foreach ($data as $row)
    {
      unset($row['id']);
      unset($row['ind']);
      $row['component_id'] = $dest_component_id;

      foreach ($row as $key => $val)
      {
        $row[$key] = db_escape($val);
      }

      $this->addItemAssoc($row);
    }

  }

  function deleteItems($component_id)
  {
    sqlQuery('DELETE FROM '.$this->table.' WHERE component_id='.(int)$component_id);
  }

}

?>