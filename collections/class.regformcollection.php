<?
//Serveris.LV Constructor collection module
//Created by Aleksandrs Selickis, 2004

include_once('class.collection.php');

class regformCollection extends collection
{
  //Class initialization
  function regformCollection($name,$id)
  {
    $this->Collection($name,$id);
    $this->type = "regformcollection";

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "User name",
        "size"      => "35",
        "type"      => "str"
      ),

      "it1"    => Array(
        "label"     => "E-mail",
        "size"      => "35",
        "type"      => "str"
      ),

	  "it2"    => Array(
        "label"     => "Password",
        "size"      => "35",
        "type"      => "str"
      ),

      "it3"    => Array(
        "label"     => "Validation parameter",
        "size"      => "35",
        "type"      => "str"
      ),

    );

    //Collection display table definition
    $this->columns = Array(

      "col0"        => Array(
        "width"     => "20%",
        "title"     => "User name"
      ),

      "col1"    => Array(
        "width"     => "20%",
        "title"     => "E-mail"
      ),

	  "col2"        => Array(
        "width"     => "20%",
        "title"     => "Password"
      ),

      "col3"    => Array(
        "width"     => "40%",
        "title"     => "Validation parameter"
      )

    );




  }




}

?>
