<?
//Serveris.LV Constructor component
//Created by Aleksandrs Selickis, 2004, alex@datateks.lv

include_once('class.dbcollection.php');

class regform2collection extends dbcollection
{
  //Class initialization
  function regform2collection($name,$id)
  {
    $this->dbCollection($name,$id);
    $this->type = "regform2collection";

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "Login",
        "size"      => "35",
        "type"      => "str"
      ),

      "it1"    => Array(
        "label"     => "Password",
        "size"      => "35",
        "type"      => "str"
      ),

      "it2"    => Array(
        "label"     => "Name",
        "size"      => "35",
        "type"      => "str"
      ),

      "it3"    => Array(
        "label"     => "Surname",
        "size"      => "35",
        "type"      => "str"
      ),

      "it4"    => Array(
        "label"     => "E-mail",
        "size"      => "35",
        "type"      => "str"
      ),

      "it5"    => Array(
        "label"     => "Phone",
        "size"      => "35",
        "type"      => "str"
      )

    );

    //Collection display table definition
    $this->columns = Array(

      "col0"        => Array(
        "width"     => "20%",
        "title"     => "Login"
      ),

      "col1"        => Array(
        "width"     => "20%",
        "title"     => "Password"
      ),

      "col2"        => Array(
        "width"     => "25%",
        "title"     => "Name"
      ),

      "col3"        => Array(
        "width"     => "15%",
        "title"     => "Surname"
      ),

      "col4"        => Array(
        "width"     => "15%",
        "title"     => "E-mail"
      ),

      "col5"        => Array(
        "width"     => "15%",
        "title"     => "Phone"
      )

    );

  }


}

?>