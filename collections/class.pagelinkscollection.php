<?

include_once('class.collection.php');

class pagelinkscollection extends collection
{
  //Class initialization
  function pagelinkscollection($name,$id)
  {
    $this->Collection($name,$id);
    $this->type = "pagelinkscollection";

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "Link",
        "type"      => "dialog",
        "dialog"    => "?module=dialogs&action=links&site_id=".$GLOBALS['site_id']
      ),

      "it1"    => Array(
        "label"     => "text",
        "size"      => "35",
        "type"      => "str"
      ),

      "it2"    => Array(
        "label"     => "target",
        "size"      => "35",
        "type"      => "list",
        "lookup"    => Array("0:Self", "1:New Page")
      ),

    );

    //Collection display table definition
    $this->columns = Array(

      "col0"        => Array(
        "width"     => "40%",
        "title"     => "Link"
      ),

      "col1"        => Array(
        "width"     => "40%",
        "title"     => "Text"
      ),

      "col2"        => Array(
        "width"     => "20%",
        "title"     => "target"
      )

    );
  }

}

?>