<?
//Serveris.LV Constructor component
//Created by Aleksandrs Selickis, 2004, alex@datateks.lv

include_once('class.dbcollection.php');

class mailingmessagescollectionex extends dbcollection
{
  //Class initialization
  function mailingmessagescollectionex($name,$id)
  {
    $this->dbCollection($name,$id);
    $this->type = "mailingmessagescollectionex";

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "Date",
        "type"      => "date",
        "time"      => true,
        "column_name" => "time",
        "column_type" => "datetime",
        "column_index" => 1
      ),
      
      "it1"    => Array(
        "label"     => "Name",
        "type"      => "str",
        "column_name" => "name",
        "column_type" => "VARCHAR(250)",
      ),

      "it2"    => Array(
        "label"     => "E-mail From",
        "type"      => "str",
        "column_name" => "email_from",
        "column_type" => "VARCHAR(250)",
      ),

      "it3"    => Array(
        "label"     => "Subject",
        "type"      => "str",
        "column_name" => "subject",
        "column_type" => "VARCHAR(250)",
      ),
      
      "it4"    => Array(
        "label"     => "Text",
        "type"      => "str",
        "column_name" => "text",
        "column_type" => "longtext",
      ),
      
      "it5"    => Array(
        "label"     => "Attachment",
        "type"      => "str",
        "column_name" => "attachment",
        "column_type" => "varchar(250)",
      ),
      
      "it6"    => Array(
        "label"     => "Groups",
        "type"      => "str",
        "column_name" => "groups",
        "column_type" => "varchar(250)",
      ),
      
      "it7"    => Array(
        "label"     => "Total emails count",
        "type"      => "str",
        "column_name" => "total_emails_count",
        "column_type" => "int(11)",
      ),
      
      "it8"    => Array(
        "label"     => "Sent emails count",
        "type"      => "str",
        "column_name" => "sent_emails_count",
        "column_type" => "int(11)",
      ),

      "it9"    => Array(
        "label"     => "Status",
        "type"      => "str",
        "column_name" => "status"
      ),
      "it10"    => Array(
        "label"     => "Send type",
        "type"      => "str",
        "column_name" => "send_type"
      ),
    );

    //Collection display table definition
    $this->columns = Array(

      "Time"        => Array(
        "title"     => "Time"
      ),

      "subject"        => Array(
        "title"     => "Subject"
      ),

      "status"        => Array(
        "title"     => "Status"
      ),

    );

    $this->PostInit();
  }


  function AddLogItem($email, $subject, $status)
  {
    $date = date("d.m.Y. H:i:s");
    $item = Array(
      'time'       => $date,
      'email'      => $email,
      'subject'    => $subject,
      'text'       => $text,
      'attachment' => serialize($attachment),
      'groups'     => serialize($groups),
      'status'     => $status,
    );
    $item = $this->AddItemSlashes($item);
    $this->AddItemAssoc($item);
  }



  function IsEditableOutside()
  {
    $this->description = 'Sent messages';
    $this->longname = $this->name;
    return true;
  }
  
  function getMessageById($message_id) {
    $message = $this->GetItemByIDAssoc($message_id);
    $message['groups'] = unserialize($message['groups']);
    $message['attachment'] = unserialize($message['attachment']);
    return $message;
  }







}

?>