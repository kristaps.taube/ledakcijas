<?php
/**
*
*  Title: PDO collection
*  Author: Kristaps Taube <ktaube@datateks.lv>
*  Project: Constructor
*
*/

require_once("class.dbcollection.php");

use \Constructor\WebApp;

abstract class pdocollection extends dbcollection
{

    public $name;
    public $longname;
    public $site_id;
    public $type;
    public $collection_id;
    public $properties;
    public $columns;
    public $description;
    public $colproperties;
    public $error;
    public $table;
    public $subrelated_collections;
    public $related_collections;
    private $errors = [];
    private static $_instances = [];

    public function __construct($name = '', $id = false)
    {

        $this->type = self::classnameToType(get_called_class());
        $this->site_id = WebApp::$app->getSiteId();

        if(!$name && !$id){
            $collections = $this->getCollectionsByType($this->type);

            if(count($collections) == 1){
                $name = $collections[0]['name'];
            }elseif(count($collections) > 1){
                die("Can't get collection by type, multiple collections found");
            }
        }elseif(!$name && $id){

            $collection = self::getCollectionById($this->site_id, $id);
            if($collection){
                $name = $collection['name'];
            }

        }

        $name = $name ? $name : strtolower($this->type);

        if(!$id){

            $collection = $this->getCollectionByTypeAndName($this->type, $name);

            if($collection){ // collection exists
                $id = $collection['collection_id'];
            }else{ // new collection

                $t = $this->addNewCollection($name);
                $id = $this->collection_id;

            }

        }

        self::$_instances[$this->type][$name] = $this;

        parent::__construct($name, $id);

    }

    public static function typeToClassname($type)
    {
        return str_replace("_", "\\", $type);
    }

    public static function classnameToType($classname)
    {
        return str_replace("\\", "_", $classname);
    }

    public static function getInstance($name = false)
    {

        $called_class = get_called_class();

        if(!$name){
            $obj = new $called_class();
            $result = $obj;
        }else{

            $type = strtolower(self::classnameToType($called_class));

            if(isset(self::$_instances[$type][$name])){
                $result = self::$_instances[$type][$name];
            }else{
                $result = new $called_class($name);
                #die('Instance for class `'.$type.'` with name `'.$name.'` not found');
            }

        }

        return $result;

    }

    private function getCollectionByTypeAndName($type, $name)
    {

        $result = null;
        foreach($this->getAllCollections() as $collection){
            if($type == $collection['type'] && $name == $collection['name']){
                $result = $collection;
            }
        }

        return $result;

    }

    private function getCollectionsByType($type)
    {

        $result = [];
        foreach($this->getAllCollections() as $collection){
            if($type == $collection['type']){
                $result[] = $collection;
            }
        }

        return $result;

    }

    public function __get($name)
    {

        if($this->columns[$name]['subrelated_collection']){ // its a subrelated collection

            if(!$this->subrelated_collections[$name]){
                $this->subrelated_collections[$name] = $this->InitializeSubRelatedCollection($name);
            }

            return $this->subrelated_collections[$name];

        }elseif($this->properties_assoc[$name]['type'] == 'relation'){ // its a related collection

        if(!$this->related_collections[$name]){

            $this->related_collections[$name] = $this->InitializeRelatedCollection($name, $this->properties_assoc[$name]['related_collection']);

        }

        return $this->related_collections[$name];

        }

        return null;

    }

    public function GetById($id)
    {
        return DB::GetRow("SELECT * FROM `".$this->table."` WHERE item_id = :id", array(":id" => $id));
    }

    public function ChangeById($id, $properties)
    {

        $properties = $this->CleanAssocArray($properties);

        if ($this->doAutoLastUpdate()){
            $properties['last_update'] = time();
            $this->reindex();
        }

        $this->Update($properties, array("item_id" => $id));

        return $id;

    }

	protected function validateUpdate($data, $cond)
	{
		return true;
	}

	protected function beforeUpdate($data, $cond)
	{
		return $this->validateUpdate($data, $cond);
	}

	protected function afterUpdate($data, $cond){}

    public function Update($data, $cond)
    {

        if($this->beforeUpdate($data, $cond)){
            $result = DB::Update($this->table, $cond, $data);
            $this->afterUpdate($data, $cond);
            return $result;
        }else{
            return false;
        }

    }

	public function validateInsert($data)
	{
		return true;
	}

	protected function beforeInsert(&$data)
	{
		return $this->validateInsert($data);
	}

	protected function afterInsert($data, $id){}

	public function getErrors($field = false)
	{

		if($field){

			if(isset($this->errors[$field])){
				return $this->errors[$field];
			}else{
				return array();
			}

		}else{
			return $this->errors;
		}

	}

	protected function addError($field, $error)
	{

		if(!isset($this->errors[$field])){
			$this->errors[$field] = array();
		}

		$this->errors[$field][] = $error;

	}

    public function Insert($data)
    {

        static $ind;

        if(!$ind[$this->table]){
            $ind[$this->table] = DB::GetValue("SELECT max(ind) FROM `".$this->table."`");
        }

        if(!$data['ind']){
            $data['ind'] = ++$ind[$this->table];
        }

        if($this->beforeInsert($data)){
            $id = DB::Insert($this->table, $data);

            if($id){
                $this->afterInsert($data, $id);
            }

        }

        return isset($id) && $id ? $id : false;

    }

  public function Truncate()
	{
    return DB::Truncate($this->table);
  }

  public function Delete($cond)
	{

    $cond = is_array($cond)? $cond : array(DB::GetTablePK($this->table) => $cond);

    return DB::Delete($this->table, $cond);
  }

  public function GetValue($data)
	{

    $what = isset($data['what']) ? $data['what'] : '*';
    $where = isset($data['where'])? $data['where'] : '1';
    $order = isset($data['order']) ? $data['order'] : 'ind';

    $query = "SELECT ".$what." FROM `".$this->table."` WHERE ".$where."  ORDER BY ".$order." LIMIT 1";

    return DB::GetValue($query, $data['params']);

  }

  public function GetTable($data)
	{

    $what = isset($data['what']) ? $data['what'] : '*';
    $where = isset($data['where'])? $data['where'] : '1';
    $order = isset($data['order']) ? $data['order'] : 'ind';
    $limit = isset($data['limit']) ? " LIMIT ".$data['limit'] : '';

    $query = "SELECT ".$what." FROM `".$this->table."` WHERE ".$where."  ORDER BY ".$order.$limit;

    return DB::GetTable($query, isset($data['params']) ? $data['params'] : []);

  }

  public function GetColumn($data)
	{

    $return = array();

    foreach($this->GetTable($data) as $row){
      foreach($row as $key => $value){
        $return[] = $value;
      }

    }

    return ($data['unique']) ? $return : array_unique($return);

  }

  public function GetRow($data)
	{

    $what = isset($data['what']) ? $data['what'] : '*';
    $where = isset($data['where'])? $data['where'] : '1';
    $order = isset($data['order']) ? $data['order'] : 'ind';
    $limit = isset($data['limit']) ? " LIMIT ".$data['limit'] : '';

    $query = "SELECT ".$what." FROM `".$this->table."` WHERE ".$where."  ORDER BY ".$order.$limit;

    return DB::GetRow($query, $data['params']);

  }

}
