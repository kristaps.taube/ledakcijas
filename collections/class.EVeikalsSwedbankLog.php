<?php

require_once('class.pdocollection.php');

class EVeikalsSwedbankLog extends pdocollection
{

  public function __construct($name,$id)
	{

    parent::__construct($name,$id);
    $this->type = get_class();

    //Properties array definition
    $this->properties_assoc = Array(

      "order_id" => Array(
        "label"     => "Rēķins:",
        "type"      => "relation",
				"column_type" => "int(32)",
				"column_index" => 1,
        "related_collection" => "EVeikalsOrderCollection",
        "relation_format" => "xzc",
      ),

			"event"    => Array(
        "label"     => "Notikums:",
        "type"      => "list",
        "lookupassoc" => array(
					"card_create_request" => "[1] Karšu apmaksas uzsākšana",
					"banklink_create_request" => "[1] Banklink apmaksas uzsākšana",
					"payment_status_query" => "[2] Maksājuma statusa pārbaude",
					"get_payment_details" => "[3] Kartes datu ieguve",
					"process_notification" => "[3] Notifikācijas apstrāde"
				)
      ),

			"result"    => Array(
        "label"     => "Iznākums:",
        "type"      => "str",
				"column_type" => "mediumtext",
      ),

			"merchant_reference"    => Array(
        "label"     => "Merchant reference:",
        "type"      => "str",
				"column_type" => "varchar(20)",
				"column_index" => 1,
      ),

			"datacash_reference"    => Array(
        "label"     => "Datacash reference:",
        "type"      => "str",
      ),

			"date"    => Array(
        "label"     => "Datums:",
        "type"      => "date",
				"column_type" => "datetime",
      ),

			"request"    => Array(
        "label"     => "Request:",
        "type"      => "customtext",
				"style" => "height: 300px;"
      ),

			"response"    => Array(
        "label"     => "Response:",
        "type"      => "customtext",
				"style" => "height: 300px;"
      ),

    );

    //Collection display table definition
    $this->columns = Array(

      "order_id" => array("title" => "Rēķins:",),
			"event" => array("title" => "Notikums:"),
			"result" => array("title" => "Iznākums:"),
			"merchant_reference" => array("title" => "Reference:"),
			"datacash_reference" => array("title" => "DT Reference:"),
			"date" => array("title" => "Datums:")

    );

		$this->PostInit();

  }

	public function beforeInsert(&$data)
	{

		$data['date'] = isset($data['date']) ? $data['date'] : date("Y-m-d H:i:s");

		return parent::beforeInsert($data);

	}

	public function GetDefaultOrder()
	{
		return $this->table.'.item_id DESC';
	}

	public function GetEditData($page, &$count, &$recordposition, $order = '')
	{

    $data = parent::GetEditData($page, $count, $recordposition, $order);

    foreach($data as &$entry){

      $entry['event'] = $this->properties_assoc['event']['lookupassoc'][$entry['event']];
      $entry['date'] = $entry['date'] ? str_replace(" ", "<br />", date("d.m.Y H:i:s", strtotime($entry['date']))) : 'Nav norādīts';

    }

    return $data;

  }

	public function getByOrder($order_id){

		return DB::GetTable("
			SELECT *
			FROM `".$this->table."`
			WHERE
				order_id = :oid
			ORDER BY date DESC
		", array(":oid" => $order_id));

	}

	public function getEntriesForPaymentCheck()
	{

		$query = "
			SELECT l.*
			FROM `".$this->table."` AS l
			JOIN `".$this->order_id->table."` AS o ON o.item_id = l.order_id
			WHERE
				date > DATE_SUB(NOW(), INTERVAL 30 MINUTE) AND
				event = 'card_create_request' AND
				o.apmaksats != 1
		";

		return DB::GetTable($query);

	}

	public function getNewMerchantReference($start)
	{

		$ref = $start;
		$i = 0;

		while(!empty($this->getByMerchantReference($ref))){
			$ref = $start."/".(++$i);
		}

		return $ref;

	}

	public function getByMerchantReference($ref)
	{
		return $this->getTable(array("where" => "merchant_reference = :ref", "params" => array(":ref" => $ref)));
	}

	public function getByDatacashReference($ref)
	{
		return $this->getRow(array("where" => "datacash_reference = :ref", "params" => array(":ref" => $ref)));
	}

  public function IsEditableOutside()
	{
    $this->description = 'Collection: '.get_class(); 
    $this->longname = $this->name;
    return true;
  }

  public function processFormBeforeDisplay(&$form_data, $create, $item)
	{

  }

}