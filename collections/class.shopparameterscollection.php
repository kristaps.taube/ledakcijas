<?
//Serveris.LV Constructor component
//Created by Aivars Irmejs, 2006, aivars@datateks.lv

include_once('class.dbcollection.php');

class shopparameterscollection extends dbcollection
{
  //Class initialization
  function shopparameterscollection($name,$id)
  {
    $this->DBCollection($name,$id);
    $this->type = "shopparameterscollection";

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "Nosaukums",
        "size"      => "35",
        "type"      => "string",
        "column_name" => "title",
        "multilang" => true,
      ),

      "it1"    => Array(
        "label"     => "Komentārs",
        "size"      => "35",
        "type"      => "str",
        "column_name" => "comment",
        "multilang" => true,
      ),

      "it2"    => Array(
        "label"     => "Iespējamās vērtības",
        "size"      => "35",
        "type"      => "list",
        "lookup"    => Array("0:+/-", "1:Brīvs teksts"),
        "column_name" => "type"
      ),

      "it3"    => Array(
        "label"     => "Rādīt",
        "size"      => "35",
        "type"      => "check",
        "column_name" => "visible",
        "value"     => "on"
      ),

      "it4"    => Array(
        "label"     => "Filtrēt pēc",
        "size"      => "35",
        "type"      => "hidden", //"check",
        "column_name" => "filter"
      ),

      "it5"    => Array(
        "label"     => "Īpašības grupa",
        "size"      => "35",
        "type"      => "list",
        "lookup"    => Array(),
        "combo"     => true,
        "column_name" => "group",
        "multilang" => true,
      )

    );

    //Collection display table definition
    $this->columns = Array(

      "title"        => Array(
        "title"     => "Nosaukums",
        "multilang" => true,
      ),

      "comment"        => Array(
        "title"     => "Komentārs",
        "multilang" => true
      ),

      "group"       => Array(
        "title"     => "Grupa",
        "multilang" => true,
      )
    );

    $this->postInit();
  }

  function AddNewCollection($name)
  {
    $lid = parent::AddNewCollection($name);

    //create two utility tables
    sqlQuery("CREATE TABLE IF NOT EXISTS `".$this->table."_catparams` (
               `item_id` int(32) NOT NULL auto_increment,
               `cat_id` int(32) NOT NULL,
               `param_id` int(32) NOT NULL,
               PRIMARY KEY  (`item_id`)
              )");
    sqlQuery("CREATE TABLE IF NOT EXISTS `".$this->table."_paramvals` (
               `item_id` int(32) NOT NULL auto_increment,
               `value` MEDIUMTEXT NOT NULL,
               `param_id` int(32) NOT NULL,
               `prod_id` int(32) NOT NULL,
               PRIMARY KEY  (`item_id`)
              )");
    return $lid;
  }

  function DelCollection()
  {
    parent::DelCollection();
    sqlQuery("DROP TABLE IF EXISTS `".$this->table."_catparams`");
    sqlQuery("DROP TABLE IF EXISTS `".$this->table."_paramvals`");
  }

    //returns associative array "id" => "1" for all params that belong to this cat
  function getCategoryParameters($cat_id)
  {
    $data = sqlQueryData("SELECT * FROM ". $this->table . "_catparams WHERE cat_id = " . $cat_id);
    $result = Array();
    foreach($data as $d)
    {
      $result[$d['param_id']] = '1';
    }
    return $result;
  }

  function setCategoryParameters($cat_id, $params)
  {
    sqlQuery("DELETE FROM ". $this->table . "_catparams WHERE cat_id = " . $cat_id);
    foreach($params as $param_id => $p)
    {
      if($p == '1' || $p == 'on')
      {
        sqlQuery("INSERT INTO ". $this->table . "_catparams (cat_id, param_id) VALUES (" . $cat_id . ", " . $param_id . ")");
      }
    }
  }

  function getProductParameterValues($prod_id, $readvalues = false)
  {
    $result = Array();

    $data = sqlQueryData("SELECT * FROM ". $this->table . "_paramvals WHERE prod_id = " . $prod_id);
    foreach($data as $d)
    {
      $result[$d['param_id']]['value'] = $d['value'];
    }

    if($readvalues)
    {
      $data = sqlQueryData("SELECT * FROM ". $this->table . "_paramvals");
      foreach($data as $d)
      {
        if(isset($result[$d['param_id']]['value']) || !$prod_id)
        {
          if($d['value'])
          {
            $result[$d['param_id']]['list'][] = $d['value'];
          }
        }
      }
    }

    return $result;
  }

  function setProductParameterValues($prod_id, $params)
  {
    sqlQuery("DELETE FROM ". $this->table . "_paramvals WHERE prod_id = " . $prod_id);
    foreach($params as $param_id => $p)
    {
      sqlQuery("INSERT INTO ". $this->table . "_paramvals (prod_id, param_id, value) VALUES (" . $prod_id . ", " . $param_id . ", '".$p."')");
    }
  }


  //Form processing
  function processFormBeforeDisplay(&$form_data, $create, $item)
  {
    $langs = sqlQueryColumn('SELECT shortname FROM '.$this->site_id.'_languages ORDER BY is_default DESC');
    foreach($langs as $lang)
    {
      $pcategories = sqlQueryData('SELECT DISTINCT `group_'.$lang.'` FROM `' . $this->table . '` ORDER BY ind');
      $hasvalue = false;
      $value = $form_data['group_' . $lang]['value'];
      foreach($pcategories as $pcat)
      {
        $form_data['group_' . $lang]['lookup'][] = $pcat['group_' . $lang] . ':' . $pcat['group_' . $lang];
        if($pcat['group_' . $lang] == $value)
        {
          $hasvalue = true;
        }
      }
      if($value && !$hasvalue)
      {
        $form_data['group_' . $lang]['lookup'][] = $value . ':' . $value;
      }
    }
  }

  function getGroupedData($lang)
  {
    return sqlQueryData('SELECT * FROM `' . $this->table . '` ORDER BY `ind`');
  }

  function processItemBeforeSave($ind, &$item, $create, &$jsmessage)
  {
    $jsmessage = '';
    return true;
  }


}

?>