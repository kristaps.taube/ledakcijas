<?
//Serveris.LV Constructor collection module
//Created by Aleksandrs Selickis, 2004

include_once('class.collection.php');

class formgeneratorCollection extends collection
{
  //Class initialization
  function formgeneratorCollection($name,$id)
  {
    $this->Collection($name,$id);
    $this->type = "formgeneratorcollection";

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "Field definition text",
        "size"      => "35",
        "type"      => "str"
      ),

      "it1"    => Array(
        "label"   => "Input type:",
        "type"    => "list",
        "width"   => "200",
        "lookup"  => Array("text:text", "textarea:textarea", "checkbox:checkbox", "radio:radio", "password:password", "select:select", "hidden:hidden", "reset:reset", "submit:submit")
      ),

      "it2"    => Array(
        "label"     => "Name:",
        "size"      => "35",
        "type"      => "str"
      ),

      "it3"    => Array(
        "label"     => "Value:",
        "size"      => "35",
        "type"      => "str"
      ),

      "it4"    => Array(
        "label"     => "Additoinal properties:",
        "size"      => "35",
        "type"      => "str"
      ),

	 "it5"        => Array( 
		  "label"     => "OPTOIN list for SELECT element:",
		  "type"      => "html",
		  "wrap"      => "none",
		  "width"     => "230",
		  "rows"      => "5",
		  "cols"      => "35",
		  "custom"    => ""
      ),

      "it6"    => Array(
        "label"   => "Field available values",
        "type"    => "list",
        "width"   => "200",
        "lookup"  => Array("0:Do Not Check", "1:Not Null String", "2:Check For Email", "3:Check For Phone")
      )

    );

    //Collection display table definition
    $this->columns = Array(

      "col0"        => Array(
        "width"     => "10%",
        "title"     => "Field definition text"
      ),

      "col1"    => Array(
        "width"     => "10%",
        "title"     => "Input type:"
      ),

      "col2"        => Array(
        "width"     => "10%",
        "title"     => "Name:"
      ),

      "col3"    => Array(
        "width"     => "10%",
        "title"     => "Value:"
      ),

      "col4"    => Array(
        "width"     => "15%",
        "title"     => "Additoinal properties:"
      ),

      "col5"    => Array(
        "width"     => "40%",
        "title"     => "OPTOIN list for SELECT element:"
      ),

      "col6"    => Array(
        "width"     => "15%",
        "title"     => "Field available values:"
      ),

    );




  }




}

?>
