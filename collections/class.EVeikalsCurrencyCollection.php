<?
include_once('class.dbcollection.php');

class EVeikalsCurrencyCollection extends dbcollection{
  
  function __construct($name,$id){

    parent::__construct($name, $id);
    $this->type = get_class();

    //Properties array definition
    $this->properties_assoc = Array(

      "title"    => Array(
        "label"     => "Nosaukums:",
        "type"      => "text"
      ),

      "label"    => Array(
        "label"     => "Apzīmējums:",
        "type"      => "text"
      ),

      "rate"    => Array(
        "label"     => "Kurss:",
        "type"      => "text"
      ),

      "ls"    => Array(
        "label"     => "Viens {eiro}:",
        "type"      => "text",
        "value" => "eiro"
      ),

      "lsx"    => Array(
        "label"     => "Divi {eiro}:",
        "type"      => "text",
        "value" => "eiro"
      ),

      "snt"    => Array(
        "label"     => "Viens {cents}:",
        "type"      => "text",
        "value" => "cents"
      ),

      "sntx"    => Array(
        "label"     => "Divi {centi}:",
        "type"      => "text",
        "value" => "centi"
      ),

      "snt0"    => Array(
        "label"     => "Nulle {centu}:",
        "type"      => "text",
        "value" => "centu"
      ),

      "default"    => Array(
        "label"     => "Pamata:",
        "type"      => "check",
        'column_type'   => 'tinyint(1)'
      ),

      "secondary"    => Array(
        "label"     => "Sekundārā:",
        "type"      => "check",
        'column_type'   => 'tinyint(1)'
      ),

    );

    //Collection display table definition
    $this->columns = Array(

      "title"        => Array("title"     => "Nosaukums"),
      "label"        => Array("title"     => "Apzīmējums"),
      "rate"        => Array("title"     => "Kurss"),
      "default"        => Array("title"     => "Pamata"),
      "secondary"        => Array("title"     => "Sekundārā"),

    );

     $this->PostInit();
  }

  function processItemBeforeSave($item_id, &$item, $create, &$jsmessage){

    $item['default'] = $item['default']? 1 : 0;
    $item['secondary'] = $item['secondary']? 1 : 0;
    $item['rate'] = str_replace(",", ".", $item['rate']); // 1,42 => 1.42

    if($item['default']){
      sqlQuery("UPDATE `".$this->table."` SET `default` = 0"); // clear current default
    }

    if($item['secondary']){
      sqlQuery("UPDATE `".$this->table."` SET `secondary` = 0"); // clear current secondary
    }

    return true;

  }

  function getDefault(){
    return sqlQueryRow("SELECT * FROM `".$this->table."` WHERE `default` = 1");
  }

  function getSecondary(){
    return sqlQueryRow("SELECT * FROM `".$this->table."` WHERE `secondary` = 1");
  }

  function GetEditData($page, &$count, &$recordposition, $order = ''){
    $query_params = $this->GetDefaultQueryParams($page, $order);
    $count = $this->ItemCount($query_params['countwhere']);
    if(isset($recordposition) && $recordposition)
    {
      $recordposition = $this->GetDBRecordPosition($query_params, $recordposition);
    }
    $data = $this->GetDBData($query_params);

    foreach($data as &$entry){

      $entry['default'] = $entry['default']? "Jā" : "Nē";
      $entry['secondary'] = $entry['secondary']? "Jā" : "Nē";

    }

    return $data;

  }

  function IsEditableOutside(){
    $this->description = 'Currencies';
    $this->longname = $this->name;
    return true;
  }

}

