<?

include_once('class.dbcollection.php');

class forum4userscollection extends dbcollection
{
  //Class initialization
  function forum4userscollection($name,$id)
  {
    $this->dbcollection($name,$id);
    $this->type = "forum4userscollection";

    //Properties array definition
    $this->properties = Array(

      "it0"    => Array(
        "label"     => "Login",
        "type"      => "str",
        "column_name" => "login",
      ),

      "it1"    => Array(
        "label"     => "Password",
        "type"      => "pass",
        "column_name" => "password",
      ),

      "it2"    => Array(
        "label"     => "Name",
        "type"      => "str",
        "column_name" => "name",
      ),

      "it3"    => Array(
        "label"     => "Surname",
        "type"      => "str",
        "column_name" => "surname",
      ),

      "it4"    => Array(
        "label"     => "E-mail",
        "type"      => "str",
        "column_name" => "email",
      ),

      "it5"    => Array(
        "label"     => "Phone",
        "type"      => "str",
        "column_name" => "phone",
      ),

      "it6"    => Array(
        "label"     => "Userpic",
        "type"      => "str",
        "column_name" => "userpic",
      ),

      "it7"    => Array(
        "label"     => "Hash",
        "type"      => "str",
        "column_name" => "hash",
      ),

      "it8"    => Array(
        "label"     => "Status",
        "type"      => "list",
        "lookup"    => Array("0:Registered", "1:Confirmed e-mail", "2:Approved by administrator"),
        "column_name" => "status",
      )

    );

    //Collection display table definition
    $this->columns = Array(

      "login"        => Array(
        "width"     => "40%",
        "title"     => "Login"
      ),

      "email"        => Array(
        "width"     => "20%",
        "title"     => "E-mail"
      ),

      "userpic"     => Array(
        "width"     => "20%",
        "title"     => "Userpic",
        "format"    => "<img src=\"{%userpic%}\" />"
      ),

    );

    $this->postInit(); //standarta peecinicializaacija nevis postu inicializaacija
  }


  function getUserByLogin($username)
  {
    return sqlQueryRow('SELECT * FROM `'.$this->table.'` WHERE login = "'.$username.'"');
  }

  function getUserByLoginAndPass($username, $pass)
  {
    return sqlQueryRow('SELECT * FROM `'.$this->table.'` WHERE login = "'.$username.'" AND password = "'.$pass.'"');
  }

  function getUserByEmail($email)
  {
    return sqlQueryRow('SELECT * FROM `'.$this->table.'` WHERE email = "'.$email.'"');
  }


}

?>
