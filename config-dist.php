<?php

/* CMS Version */
define("CMS_VERSION", "6.7.7.3");

/* Configuration */
$cfgDomain = "veikals.dev"; /* Domain of CMS, e.g: demo1.datateks.lv */
$cfgLanguage = "lv"; /* Language(lv, en or ru) */
$cfgAdminMail = "ktaube@datateks.lv"; /* DB Administators e-mail*/
$cfgLicType = "paid"; /* Licence type, possible values: paid, free, vip */

/*Database Configuration */
$cfgDBHost = "localhost"; /* MySQL database host */
$cfgDBName = ""; /* MySQL database name, e.g: demo_datateks_lv_-_main1 */
$cfgDBPort = "";   // 3306 - mysql
$cfgDBUser = ""; /* MySQL username */
$cfgDBPass = ""; /* MySQL password */

/* Other configuration */
$GLOBALS['developmode'] = false;

$cfgWebRoot = isset($_SERVER['SERVER_NAME']) ? "http://".$_SERVER['SERVER_NAME']."/cms/backend/" : "";
$cfgDirRoot = dirname(__FILE__);

$GLOBALS['use_cache'] = false;
$GLOBALS['cache_folder'] = $cfgDirRoot.'/cache/';
$GLOBALS['cache_config'] = $cfgDirRoot.'/cache_config.php';
$GLOBALS['default_cache_time'] = 300; // sec

// errors
define("ERROR_REPORTING_LEVEL", E_ALL & ~E_NOTICE & ~E_WARNING & ~E_DEPRECATED);
define("DISPLAY_ERRORS", false);
define("LOG_ERRORS", true);

define("DATABASE_DEFAULT_TABLE_ENGINE", "InnoDB");
define("MEMORY_LIMIT", "256M");

/* FOR Constructor PDO */
define("DB_HOST", $cfgDBHost);
define("DB_PORT", $cfgDBPort);
define("DB_USERNAME", $cfgDBUser);
define("DB_PASSWORD", $cfgDBPass);
define("DB_DATABASE", $cfgDBName);

define("TRANSLATION_FOLDER", $cfgDirRoot."/translations/");
define("LOG_FOLDER", $cfgDirRoot."/logs/");

define("ERROR_LOG", LOG_FOLDER."php_errors.txt");

// BACKUPS
define("BACKUP_FOLDER", $cfgDirRoot."/backup/");
define("DB_BACKUP_FILE", BACKUP_FOLDER."db_backup.gz");

// CRONS
define("CRON_FOLDER", $cfgDirRoot."/crons/");
define("CRONTAB_CONF", CRON_FOLDER."crontab.conf");

// Execution time logging
define("LOG_EXECUTION_TIME", false); // log execution time?
define("EXECUTION_TIME_FILE", LOG_FOLDER."exec_time.txt"); // file

// Component output time logging
define("LOG_COMPONENT_OUTPUT_TIME", false); // log component output time?
define("COMPONENT_OUTPUT_TIME_LOG", LOG_FOLDER."component_output_time.txt"); // file

// MySQL query logging
define("LOG_SLOW_QUERIES", true); // log slow queries?
define("SLOW_QUERY_TIME_LIMIT", 0.3); // sec
define("SLOW_QUERY_FILE", LOG_FOLDER."slow_mysql_queries.txt"); // file

define("LOG_QUERIES", false); // log queries?
define("QUERY_FILE", LOG_FOLDER."requests/queries_".uniqid().".txt"); // file
define("PDO_QUERY_FILE", QUERY_FILE); // same as mysql query log

// PDO query logging
define("PDO_ERROR_LOG_FOLDER", LOG_FOLDER."pdo_errors/");
define("LOG_SLOW_PDO_QUERIES", true); // log queries?
define("SLOW_PDO_QUERY_TIME_LIMIT", 0.3); // sec
define("SLOW_PDO_QUERY_FILE", LOG_FOLDER."slow_pdo_queries.txt"); // file

// templates
define("TEMPLATE_FOLDER", $cfgDirRoot."/templates/");

// timezone
define("DEFAULT_TIMEZONE", "Europe/Riga");

// index components for search
define("PROCESS_SEARCH_PARTS", false);

/* Disabled frontend */
$GLOBALS['disabled_frontend'] = false;
$GLOBALS['frontend_user'] = '';
$GLOBALS['frontend_password'] = '';

/* OpenID Server */
$GLOBALS['openid_server'] = 'id.serveris.lv';

$GLOBALS['cfgUseProfiler'] = false;

$cfgUseStatistics = true;
$cfgStatiscticsPeriod = 2;

$GLOBALS['cfgCMSLanguages'] = array('en' => 'English', 'lv' => 'Latvian', 'ru' => 'Russian') ;
$GLOBALS['cfgLanguage'] = $GLOBALS['cfgCMSLanguages'][$cfgLanguage];
$GLOBALS['StorePageHistory'] = false;

$GLOBALS['openid_user_group'] = $cfgLicType;

if (substr($cfgDirRoot, strlen($cfgDirRoot)-1)!='/') $cfgDirRoot = $cfgDirRoot.'/';

$win = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN');
if ($win) $sep = ";"; else $sep = ":";
ini_set("include_path", ".".$sep.$cfgDirRoot."library");

require_once($cfgDirRoot."/init.php");
