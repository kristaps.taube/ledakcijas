<?php

// adding Constructor autoloader
require __DIR__ . DIRECTORY_SEPARATOR . "library" . DIRECTORY_SEPARATOR . "class.AutoLoader.php";
spl_autoload_register(array('AutoLoader', 'load'));

// vendor autoloader
require(__DIR__ . DIRECTORY_SEPARATOR ."vendor/autoload.php");

$config = ArrayHelper::merge(
    require __DIR__ . DIRECTORY_SEPARATOR . 'config_array.php',
    require __DIR__ . DIRECTORY_SEPARATOR . 'config_array_local.php'
);

$app = new Constructor\ConsoleApp($config);
$app->run($argv);