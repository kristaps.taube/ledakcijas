<?php

namespace controllers;

use Constructor\Controller;
use Constructor\WebApp;
use Constructor\Url;
use EVeikalsRegistrationForm;

require_once($GLOBALS['cfgDirRoot']."library/google/Google_Client.php");
require_once($GLOBALS['cfgDirRoot']."library/google/contrib/Google_PlusService.php");
require_once($GLOBALS['cfgDirRoot']."library/google/contrib/Google_Oauth2Service.php");

class RegistrationController extends Controller
{

    private $google;
    private $google_conf;
    private $google_pluss;
    private $googleoauth2Service;

    public function __construct($controller_name)
    {

        parent::__construct($controller_name);

        // google conf.
        $this->google_conf = array();
        $this->google_conf['client_id'] = option('Login\\g_client_id', null, 'Google Client Id', array("is_advanced" => true), '');
        $this->google_conf['client_secret'] = option('Login\\g_client_secret', null, 'Google Client Secret', array("is_advanced" => true), '');

        $this->google = new \Google_Client();
        $this->google->setClientId($this->google_conf['client_id']);
        $this->google->setClientSecret($this->google_conf['client_secret']);
        $redirect_uri = Url::get('/registration/google-return', [], true);
        $this->google->setRedirectUri($redirect_uri);
        $this->google->setApprovalPrompt('auto');
        $this->google_pluss = new \Google_PlusService($this->google);
        $this->googleoauth2Service = new \Google_Oauth2Service($this->google);


    }

    public function actionIndex()
    {

        $this->setTemplateVar('breadcrumbs', ['<a href="'.Url::get("registration").'">'.WebApp::l("Reģistrācija").'</a>']);

        if(isset($_GET['view']) && $_GET['view'] == 'activation_sent'){
            return $this->actionActivationSent();
        }

        $user = WebApp::$app->user->get();

        if($user){
            return $this->render("logged_in", ['user' => $user]);
        }

        $form = EVeikalsRegistrationForm::getInstance();
        if(isset($_POST['register'])){

            $form->setData($_POST);

            if($form->validateForm()){ // no errors, all good
                $form->processRegistration();
                return $this->redirect('?view=activation_sent');
            }

        }

        return $this->render("index", ['form' => $form]);

    }

    public function actionGoogleReturn()
    {

        $this->google->authenticate($_GET['code']);
        $_SESSION['google_access_token'] = $this->google->getAccessToken();

        $user_col = \shopusercollection::getInstance();

        $me = $this->google_pluss->people->get('me');

        if($me){

            $user = $user_col->getUserByGoogleId($me['id']);

            if(!$user){

                $info = $this->googleoauth2Service->userinfo->get();

                $check = $user_col->getUserByEmail($info["email"]);

                $user_data = [
                    "google_id" => $me['id'],
                    "email" => $check ? '' : $info["email"],
                    "active" => 1,
                    "name" => $me['name']['givenName'],
                    "google_name" => $me['name']['givenName']." ".$me['name']['familyName'],
                    "surname" => $me['name']['familyName'],
                ];

                $id = $user_col->Insert($user_data);
                $user = $user_col->getById($id);

            }

        }

        if(isset($user) && $user){
            WebApp::$app->user->authorizeUserEntry($user);
        }

        return $this->redirect(Url::get('/'));

    }

    public function actionGoogle()
    {

        $url = $this->google->createAuthUrl();

        return $this->redirect($url);

    }

    public function actionFacebook()
    {

        $fb = new \Facebook\Facebook([
            'app_id' => '215389602375595',
            'app_secret' => '955900cb7a38d0339192726cc4ac0c63',
            'default_graph_version' => 'v2.12',
        ]);

        $helper = $fb->getRedirectLoginHelper();

        $permissions = ['email']; // Optional permissions
        $loginUrl = $helper->getLoginUrl(Url::get('/registration/facebook-return', [], true), $permissions);

        return $this->redirect($loginUrl);

    }

    public function actionFacebookReturn()
    {

        $fb = new \Facebook\Facebook([
            'app_id' => '215389602375595',
            'app_secret' => '955900cb7a38d0339192726cc4ac0c63',
            'default_graph_version' => 'v2.12',
        ]);

        $helper = $fb->getRedirectLoginHelper();

        try {
            $accessToken = $helper->getAccessToken();
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        $at = $accessToken->getValue();

        $response = $fb->get('/me?fields=id,name,last_name,email,picture', $at);
        $graph_user = $response->getGraphUser();

        $user_col = \shopusercollection::getInstance();

        $user = $user_col->getUserByFBId($graph_user['id']);

        if(!$user){

            $check = $user_col->getUserByEmail($graph_user["email"]);

            $user_data = [
                "fb_id" => $graph_user['id'],
                "email" => $check ? '' : $graph_user["email"],
                "name" => $graph_user['name'],
                "active" => 1,
                "fb_name" => $graph_user['name']." ".$graph_user['last_name'],
                "surname" => $graph_user['last_name'],
            ];

            $id = $user_col->Insert($user_data);
            $user = $user_col->getById($id);

        }

        if($user){
            WebApp::$app->user->authorizeUserEntry($user);
        }

        return $this->redirect(Url::get('/'));

    }

    public function actionAccountActivated()
    {

        $user_id = WebApp::$app->session->getFlash('user_id', false);
        $user = \shopusercollection::getInstance()->getById($user_id);

        return $this->render('account-activated', ['user' => $user]);

    }

    public function actionActivationSent()
    {

        $form = EVeikalsRegistrationForm::getInstance();
        return $this->render('activation-sent', ['form' => $form]);

    }

    public function actionActivation()
    {

        $success = false;

        $usercol = \shopusercollection::getInstance();

        $hash = isset($_GET['hash']) ? $_GET['hash'] : false;
        $user = $hash ? $usercol->GetRow(["where" => "activation_hash = :hash", "params" => [":hash" => $hash]]) : false;

        if($user){
            $usercol->Update(["active" => "1"], $user['item_id']);
            WebApp::$app->user->authorizeUserEntry($user);
            $success = true;
        }

        return $this->redirect($success ? Url::get("registration/account-activated", ['lang' => $user['lang']]) : Url::get("/"));

    }

}