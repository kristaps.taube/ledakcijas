<?php

namespace controllers;

use Constructor\Controller;
use Constructor\WebApp;
use Constructor\Url;

class BlogController extends Controller
{

    private $page_size = 5;

    public function actionIndex()
    {

        $this->setTemplateVar('breadcrumbs', ['<a href="'.Url::get("blog").'">'.WebApp::l("Blogs").'</a>']);

        $this->setTitleTag(WebApp::l("Blogs"));

        $page = isset($_GET['p']) ? (int)$_GET['p'] : 1;
        $page = $page >= 1? $page : 1;

        $cat_col = \LedAkcijasArticleCategoryCollection::getInstance();
        $col = \LedAkcijasArticleCollection::getInstance();

        $article_data = $this->getArticles();
        $articles = $article_data['articles'];
        $article_count = $article_data['count'];

        return $this->render("index", [
            'articles' => $articles,
            'categories' => $cat_col->getDBData(),
            'article_count' => $article_count,
            'page' => $page,
            'page_size' => $this->page_size
        ]);

    }

    public function actionCategory($category)
    {

        $this->setTitleTag($category['category_title_'.WebApp::$app->getLanguage()]);

        $this->setTemplateVar('breadcrumbs', [
            '<a href="'.Url::get("blog").'">'.WebApp::l("Blogs").'</a>',
            '<a href="'.Url::get("blog_category").'">'.$category['category_title_'.WebApp::$app->getLanguage()].'</a>',
        ]);

        $page = isset($_GET['p']) ? (int)$_GET['p'] : 1;
        $page = $page >= 1? $page : 1;

        $cat_col = \LedAkcijasArticleCategoryCollection::getInstance();

        $article_data = $this->getArticles($category['item_id']);
        $articles = $article_data['articles'];
        $article_count = $article_data['count'];

        return $this->render("index", [
            'articles' => $articles,
            'categories' => $cat_col->getDBData(),
            'article_count' => $article_count,
            'page' => $page,
            'page_size' => $this->page_size
        ]);

    }

    public function actionView($article)
    {

        $col = \LedAkcijasArticleCollection::getInstance();
        $cat_col = \LedAkcijasArticleCategoryCollection::getInstance();

        $category = $cat_col->getById($article['category_id']);
        $this->setTemplateVar('breadcrumbs', [
            '<a href="'.Url::get("blog").'">'.WebApp::l("Blogs").'</a>',
            '<a href="'.Url::get("blog_category", ['category' => $category]).'">'.$category['category_title_'.WebApp::$app->getLanguage()].'</a>',
            '<a href="'.Url::get("blog_article", ['article' => $article]).'">'.$article['title'].'</a>',
        ]);

        $products = $this->getArticleProducts($article);

        return $this->render('view', [
            'article' => $article,
            'categories' => $cat_col->getDBData(),
            'products' => $products
        ]);

    }

    private function getArticleProducts($article)
    {

        $products = [];
        $codes = explode(',', $article['products']);

        if($codes){

            $cond = $params = [];

            foreach($codes as $i => $code){
                $cond[] = 'code = :code'.$i;
                $params[':code'.$i] = $code;
            }

            $col = \shopprodcollection::getInstance();
            $products = $col->getTable(['where' => "disabled = 0 AND (".implode(' OR ', $cond).")", 'params' => $params]);

        }

        return $products;

    }

    private function getArticles($category = false)
    {

        $page_size = 5;
        $page = isset($_GET['p']) ? (int)$_GET['p'] : 1;
        $page = $page >= 1? $page : 1;

        $col = \LedAkcijasArticleCollection::getInstance();
        $articles = $col->getArticles(WebApp::$app->getLanguage(), $category);

        # count it
        $article_count = count($articles);

        # page it
        $articles = array_slice($articles, ($page - 1) * $this->page_size, $this->page_size);

        return ['count' => $article_count, 'articles' => $articles];

    }

}