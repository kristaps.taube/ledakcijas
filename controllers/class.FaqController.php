<?php

namespace controllers;

use Constructor\Controller;
use Constructor\WebApp;
use Constructor\Url;

class FaqController extends Controller
{

    public function actionIndex()
    {

        $this->setTemplateVar('breadcrumbs', ['<a href="'.Url::get("buj").'">'.WebApp::l("Biežāk uzdotie jautajumi").'</a>']);

        $errors = false;
        if(isset($_POST['ask']) && (!$errors = $this->processQuestion($_POST))){
            WebApp::$app->session->setFlash('buj_success', WebApp::l("Jūsu jautājums ir veiksmīgi nosūtīts"));
            return $this->redirect(Url::get('buj')."#q");
        }

        $faq_col = \LedAkcijasBUJCollection::getInstance();
        $questions = [];
        foreach($faq_col->getDBData(['where' => 'lang = :lang', 'params' => [':lang' => WebApp::$app->getLanguage()]]) as $question){

            if(!isset($questions[$question['category_id']])) $questions[$question['category_id']] = [];

            $questions[$question['category_id']][] = $question;

        }


        $cat_col = \LedAkcijasBUJCategoryCollection::getInstance();
        $categories = $cat_col->getDBData();

        return $this->render('index', [
            'categories' => $categories,
            'questions' => $questions,
            'post' => $_POST,
            'errors' => $errors,
            'buj_success' => WebApp::$app->session->getFlash('buj_success')
        ]);

    }

    private function processQuestion($data)
    {

        $errors = $this->validateQuestion($data);

        if(!$errors){

            $text = "<h1>Jautājums no BUJ formas</h1>";
            $text .= "Valoda: ".strtoupper(WebApp::$app->getLanguage())."<br /><br />";
            $text .= "Vārds: ".$data['name']."<br />";
            $text .= "E-pasts: ".$data['email']."<br />";
            $text .= "Jautājums: ".$data['comment']."<br />";
            $text .= "<br />======================<br/>";
            $text .= date("d.m.Y H:i:s")."<br />";
            $text .= $_SERVER['REMOTE_ADDR']."<br/>";

            $email = option("admin_emailaddress", null, "Admin e-mail", array("value_on_create" => "ktaube@datateks.lv"));
            $from = option("ContactForm\\from", null, "From", array("value_on_create" => "Web"));
            $from_email = option("ContactForm\\from_email", null, "From e-mail", array("value_on_create" => 'web@'.WebApp::$app->getConfigValue('domain')));

            $sent = phpMailerSend(
                $text,
                'Jautājums',
                $from,
                $from_email,
                $email,
                true,
                false,
                Array()
            );

        }

        return $errors;

    }

    private function validateQuestion($data)
    {

        $errors = [];

        if(!isset($data['name']) || !$data['name']) $errors[] = WebApp::l("Tukšs lauks").": ".WebApp::l("Vārds, uzvārds..");
        if(!isset($data['email']) || !$data['email']) $errors['email'] = WebApp::l("Tukšs lauks").": ".WebApp::l("E-pasts");
        if(!isset($errors['email']) && !filter_var($data['email'], FILTER_VALIDATE_EMAIL)) $errors['email'] = WebApp::l("Nekorekts e-pasts");
        if(!isset($data['comment']) || !$data['comment']) $errors[] = WebApp::l("Tukšs lauks").": ".WebApp::l("Jautājums vai komentārs");

        if(!isset($data['r2d2']) || $data['r2d2']) $errors[] = WebApp::l("Kļūda"); // vs bots

        return $errors;

    }

}
