<?php

namespace controllers;

use Constructor\Controller;
use Constructor\WebApp;
use Constructor\Url;
use \shopsmallcart;
use \shopmanager;

class ProductController extends Controller
{

    public $template = 'product';
    private $question_topics;

    public function beforeAction($action, $params = [])
    {

        $this->question_topics = [];
        foreach(explode(",", WebApp::l("Tēma1, Tēma2, Tēma3")) as $i => $item){
            $this->question_topics[$i+1] = $item;
        }


        return parent::beforeAction($action, $params);

    }

    public function actionIndex(array $product, $category = null)
    {

        if(isset($_POST['question'])){
            if(!($question_errors = $this->processAskQuestion($_POST, $product))){
                WebApp::$app->session->setFlash('product-question', WebApp::l("Jautājums nosūtīts"));
                return $this->refresh();
            }
        }

        $related_products = $this->getRelatedProducts($product);

        $this->setTemplateVar('breadcrumbs', $this->makeBreadcrumbsItems($product, $category));

        $user = WebApp::$app->user->get();
        if($user){
            $wishlistcol = \WishlistCollection::getInstance();
            $in_wishlist = $wishlistcol->getByProductAndUser($product['item_id'], $user['item_id']);
        }else{
            $in_wishlist = false;
        }

        return $this->render("index", [
            'product' => $product,
            'category' => $category,
            'topics' => $this->question_topics,
            'question_flash_message' => WebApp::$app->session->getFlash('product-question'),
            'question_errors' => isset($question_errors) ? $question_errors : false,
            'post' => $_POST,
            'related_products' => $related_products,
            'in_wishlist' => $in_wishlist
        ]);

    }
    
    public function actionAddToWishlist()
    {



    }

    public function actionAddToCart()
    {

        $prod = isset($_POST['p']) ? (int)$_POST['p'] : false;
        $count = isset($_POST['count']) ? (int)$_POST['count'] : false;
        $count = $count > 0 ? $count : 1;

        if($prod){
            $item = ['id' => $prod];
            WebApp::$app->cart->addItem($item, $count);
        }

        return $this->redirectBack();

    }

    private function getRelatedProducts($product)
    {

        $prods_col = \shopprodcollection::getInstance();
        $rel_col = \shoprelatedprodscollection::getInstance();

        $cond = $params = [];
        $cond[] = 'r.prod_id = :pid';
        $cond[] = 'p.disabled = 0';
        $cond[] = 'p.shortcut = 0';
        $params[':pid'] = $product['item_id'];

        $related = \DB::GetTable("
            SELECT p.*
            FROM `".$prods_col->table."` AS p
            LEFT JOIN `".$rel_col->table."` AS r ON p.item_id = r.related_id
            WHERE ".implode(" AND ", $cond)."
            ORDER BY rand()
            LIMIT 4
        ", $params);

        return $related;
    }

    private function processAskQuestion($data, $product)
    {

        $errors = $this->validateAskQuestion($data);

        if(!$errors){

            $text = "<h1>Jautājums par produktu</h1>";
            $text .= "Produkts: ".$product['name_lv']."<br />";
            $text .= "Valoda: ".strtoupper(WebApp::$app->getLanguage())."<br /><br />";
            $text .= "Vārds: ".$data['name']."<br />";
            $text .= "E-pasts: ".$data['email']."<br />";
            $text .= "Tēma: ".$this->question_topics[$data['subject']]."<br />";
            $text .= "Jautājums: ".$data['comment']."<br />";
            $text .= "<br />======================<br/>";
            $text .= date("d.m.Y H:i:s")."<br />";
            $text .= $_SERVER['REMOTE_ADDR']."<br/>";

            $email = option("admin_emailaddress", null, "Admin e-mail", array("value_on_create" => "ktaube@datateks.lv"));
            $from = option("ContactForm\\from", null, "From", array("value_on_create" => "Web"));
            $from_email = option("ContactForm\\from_email", null, "From e-mail", array("value_on_create" => 'web@'.WebApp::$app->getConfigValue('domain')));

            $sent = phpMailerSend(
                $text,
                'Jautājums par produktu',
                $from,
                $from_email,
                $email,
                true,
                false,
                Array()
            );

            if(!$sent) $errors[] = WebApp::l("Sistēmas kļūda. Neizdevās nosūtīt");

            $email = $email ? $email : option("admin_emailaddress", null, "Admin e-mail", array("value_on_create" => "ktaube@datateks.lv"));

        }

        return $errors;

    }

    private function validateAskQuestion($data)
    {

        $errors = [];

        if(!isset($data['name']) || !$data['name']) $errors[] = WebApp::l("Tukšs lauks").": ".WebApp::l("Vārds, uzvārds..");
        if(!isset($data['subject']) || !$data['subject'] || !isset($this->question_topics[$data['subject']])) $errors[] = WebApp::l("Nav norādīta tēma");
        if(!isset($data['email']) || !$data['email']) $errors['email'] = WebApp::l("Tukšs lauks").": ".WebApp::l("E-pasts");
        if(!isset($errors['email']) && !filter_var($data['email'], FILTER_VALIDATE_EMAIL)) $errors['email'] = WebApp::l("Nekorekts e-pasts");
        if(!isset($data['comment']) || !$data['comment']) $errors[] = WebApp::l("Tukšs lauks").": ".WebApp::l("Jautājums vai komentārs");

        if(!isset($data['r2d2']) || $data['r2d2']) $errors[] = WebApp::l("Kļūda"); // vs bots

        return $errors;

    }

    private function makeBreadcrumbsItems($product, $category)
    {

        $category = $category ? $category : $product['category_id'];
        $lang = WebApp::$app->getLanguage();

        $items = $this->getCategoryBreadcrumbsItems($category, $product);
        $items[] = "<a href='".Url::get('product', ['product' => $product])."'>".$product['name_'.$lang]."</a>";

        return $items;

    }

    private function getCategoryBreadcrumbsItems($category, $product)
    {

        $items = [];
        $category = is_array($category) ? $category : \shopcatcollection::getInstance()->getFromCache($category);

        if($category){
            $items[] = "<a href='".Url::get("category", ['category' => $category])."'>".$category['title_'.WebApp::$app->getLanguage()]."</a>";

            while($category && $category['parent']){
                $category = \shopcatcollection::getInstance()->getFromCache($category['parent']);
                if($category){
                    $items[] = "<a href='".Url::get("category", ['category' => $category])."'>".$category['title_'.WebApp::$app->getLanguage()]."</a>";
                }
            }
        }
        return array_reverse($items);

    }

    private function processMetaTags($product)
    {

        $lang = WebApp::$app->getLanguage();

        $title_tag_suffix = readOption('title_tag_suffix');

        $GLOBALS['meta_keywords'] .= $product['keywords_' . $lang];
        $GLOBALS['meta_description'] .= strip_tags($product['meta_description_'.$lang]);
        $GLOBALS['additional_header_tags'] .= $product['additional_header_tags_'.$lang];
        $GLOBALS['page_title'] = $product['name_'.$lang];
        $GLOBALS['force_title'] = $product['name_'.$lang].$title_tag_suffix;

    }

}