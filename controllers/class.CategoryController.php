<?php

namespace controllers;

use Constructor\Controller;
use Constructor\WebApp;
use Constructor\Url;
use ProductSorter;
use ProductFilter;

class CategoryController extends Controller
{

    public $template = 'category';
    private $orders;
    private $page_sizes;

    public function __construct($controller_name)
    {

        parent::__construct($controller_name);

        $this->orders = [
            'popularity' => WebApp::l("Pirktākās vispirms"),
            'price-asc' => WebApp::l("Lētākās vispirms"),
            'price-desc' => WebApp::l("Dārgākās vispirms"),
        ];

        $this->page_sizes = [12, 15, 18, 21];

    }

    public function beforeAction($action)
    {

        return true;

    }

    public function actionIndex(array $category, $link = false)
    {

        $lang = WebApp::$app->getLanguage();

        $page = isset($_GET['p']) ? (int)$_GET['p'] : 1;
        $page = $page >= 1? $page : 1;

        $this->setTemplateVar('category', $category);
        $this->setTemplateVar('filter_link', $link);
        $this->setTemplateVar('breadcrumbs', $this->makeBreadcrumbsItems($category, $link));

        $this->processMetaTags($category, $link);

        # get it!
        $products = $this->getProducts($category);

        # filter it!
        $products = $link ? ProductFilter::filterByLink($products, $link['item_id']) : ProductFilter::filterByCategory($products, $category['item_id']);

        # count it!
        $product_count = count($products);

        # sort it!
        $order = WebApp::$app->session->get('order', 'popular');
        $products = ProductSorter::sort($products, $order);

        # page it!
        $page_size = WebApp::$app->session->get('page_size', $this->page_sizes[0]);
        $products = array_slice($products, ($page - 1) * $page_size, $page_size);

        return $this->render("index", [
            'category' => $category,
            'title' => $link ? $link['name_'.$lang] : $category['title_'.$lang],
            'products' => $products,
            'page_size' => $page_size,
            'order' => $order,
            'orders' => $this->orders,
            'page_sizes' => $this->page_sizes,
            'page' => $page,
            'product_count' => $product_count,
            'has_filters' => true,
            'link' => $link
        ]);

    }

    public function actionFilterLink($category, $link)
    {

        if($category && $link){

            $link_filter_col = \LedAkcijasLinkFiltersCollection::getInstance();
            $filter_ids = $link_filter_col->getFilterIdsByLink($link['item_id']);

            if($filter_ids){

                # if there are manually removed filters, do not force them
                $removed_filters = ProductFilter::getLinkRemovedFilters($link['item_id']);
                foreach($filter_ids as $i => $id){
                    if(in_array($id, $removed_filters)){
                        unset($filter_ids[$i]);
                    }
                }

                ProductFilter::addLinkFilters($link['item_id'], $filter_ids);

            }

        }

        return $this->actionIndex($category, $link);

    }

    public function actionClearFilters()
    {

        if(isset($_GET['c'])){
            $category_id = (int)$_GET['c'];
            ProductFilter::clearCategoryFilters($category_id);
        }elseif(isset($_GET['l'])){
            $link_id = (int)$_GET['l'];
            ProductFilter::clearLinkFilters($link_id);
        }else{
            ProductFilter::clearCategoryFilters(0);
        }

        return $this->redirectBack();

    }

    public function actionProcessFilters()
    {

        $filters = isset($_POST['filters']) ? $_POST['filters'] : [];

        if(isset($_GET['c'])){
            $category_id = (int)$_GET['c'];
            ProductFilter::setCategoryFilters($category_id, $filters);
        }elseif(isset($_GET['l'])){
            $link_id = (int)$_GET['l'];
            ProductFilter::setLinkFilters($link_id, $filters);
        }else{
            ProductFilter::setCategoryFilters(0, $filters);
        }

        return $this->redirectBack();

    }

    private function getProducts($category)
    {

        $cond = ['disabled = 0'];

        $cat_ids = \shopcatcollection::getInstance()->getActivePredecessorIds($category['item_id']);
        $cat_ids[] = $category['item_id'];

        $cond = $params = [];

        $cond[] = 'disabled = 0';
        $cond[] = 'category_id IN('.implode(", ", $cat_ids).')';

        $products = \shopprodcollection::getInstance()->getDBData([
            'where' => implode(" AND ", $cond),
            'params' => $params
        ]);

        return $products;

    }

    private function makeBreadcrumbsItems($category, $link = false)
    {

        $lang = WebApp::$app->getLanguage();

        $items = [];
        $category = is_array($category) ? $category : \shopcatcollection::getInstance()->getFromCache($category);

        if($category){
            $items[] = "<a href='".Url::get("category", ['category' => $category])."'>".$category['title_'.$lang]."</a>";

            while($category && $category['parent']){
                $category = \shopcatcollection::getInstance()->getFromCache($category['parent']);
                if($category){
                    $items[] = "<a href='".Url::get("category", ['category' => $category])."'>".$category['title_'.$lang]."</a>";
                }
            }
        }

        $arr = array_reverse($items);

        if($link){
            $arr[] = "<a href='".Url::get("filter_link", ['category' => $category, 'link' => $link])."'>".$link['name_'.$lang]."</a>";
        }

        return $arr;

    }

    private function processMetaTags($category, $link = false)
    {

        $lang = WebApp::$app->getLanguage();

        $this->setMetaDescription($category['description_'.$lang]);
        $GLOBALS['additional_header_tags'] = $category['additional_header_tags_'.$lang];

        if($link && $link['title_tag_'.$lang]){
            $this->setTitleTag($link['title_tag_'.$lang]);
        }else{
            $title_tag = $category['title_tag_'.$lang] ? $category['title_tag_'.$lang] : $category['title_'.$lang];
            $this->setTitleTag($title_tag);
        }

        if($link && $link['meta_description_'.$lang]){
            $this->setMetaDescription($link['meta_description_'.$lang]);
        }

    }

    public function actionChangePageSize()
    {

        if(isset($_POST['page_size']) && in_array($_POST['page_size'], $this->page_sizes)){
            WebApp::$app->session->set('page_size', $_POST['page_size']);
        }

        return $this->renderJSON('1');

    }

    public function actionChangeOrder()
    {

        if(isset($_POST['order']) && isset($this->orders[$_POST['order']])){
            WebApp::$app->session->set('order', $_POST['order']);
        }

        return $this->renderJSON('1');

    }

}