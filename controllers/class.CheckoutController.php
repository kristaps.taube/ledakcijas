<?php

namespace controllers;

use Constructor\Controller;


class CheckoutController extends Controller
{

    public function beforeAction($action)
    {

        if(isset($_GET['done'])){
            return $this->render('done');
        }

        return true;

    }

    public function actionIndex()
    {

        return $this->render('index', []);

    }

}