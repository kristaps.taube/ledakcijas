<?php

namespace controllers;

use Constructor\Controller;
use Constructor\WebApp;
use Constructor\Url;

class ExportController extends Controller
{

    public function actionIndex()
    {

        return $this->render('index');

    }

    public function actionSalidzini()
    {

        $manager = \shopmanager::getInstance();
        $cats = \shopcatcollection::getInstance();
        $brands = \shopbrandscollection::getInstance();
        $products = $this->getProducts();
        $lang = WebApp::$app->getLanguage();

        $data = [];

        foreach($products as $product){

            $product_item = [];
            $product_item['name'] = $product['name_'.$lang];
            $product_item['link'] = Url::get("product", ['product' => $product], true);

            $prices = $manager->getProductPrice($product);
            $product_item['price'] = $prices['price'];
            $product_item['image'] = Url::get($product['picture'] ? $product['picture'] : "/images/html/blank_product.png", [], true);

            $brand = false;
            if($product['brand_id']){
                $brand = $brands->getFromCache($product['brand_id']);
            }

            $product_item['manufacturer'] = $brand ? $brand['brandname'] : WebApp::l("Nav norādīts");
            $cat = $cats->getFromCache($product['category_id']);
            $product_item['category'] = $cat['title_'.$lang];
            $product_item['category_full'] = $this->getCategoryFull($product['category_id']);
            $category = $cats->getFromCache($product['category_id']);
            $product_item['category_link'] = Url::get("category", ['category' => $category], true);


            $data[] = $product_item;

        }

        return $this->renderXML('root', $data);

    }

    public function actionKurpirkt()
    {

        return $this->actionSalidzini();

    }

    public function actionGudriem()
    {

        $manager = \shopmanager::getInstance();
        $cats = \shopcatcollection::getInstance();
        $brands = \shopbrandscollection::getInstance();
        $products = $this->getProducts();
        $lang = WebApp::$app->getLanguage();

        $data = [];

        foreach($products as $product){

            $product_item = [];
            $product_item['name'] = $product['name_'.$lang];
            $product_item['name_ru'] = $product['name_ru'];
            $product_item['link'] = Url::get("product", ['product' => $product], true);
            $product_item['link_ru'] = Url::get("product", ['product' => $product, 'lang' => 'ru'], true);

            $prices = $manager->getProductPrice($product);
            $product_item['price'] = $prices['price'];
            $product_item['image'] = Url::get($product['picture'] ? $product['picture'] : "/images/html/blank_product.png", [], true);

            $brand = false;
            if($product['brand_id']){
                $brand = $brands->getFromCache($product['brand_id']);
            }

            $product_item['manufacturer'] = $brand ? $brand['brandname'] : WebApp::l("Nav norādīts");
            $cat = $cats->getFromCache($product['category_id']);
            $product_item['category'] = $cat['title_'.$lang];
            $product_item['category_ru'] = $cat['title_ru'];
            $product_item['category_full'] = $this->getCategoryFull($product['category_id']);
            $product_item['category_full_ru'] = $this->getCategoryFull($product['category_id'], 'ru');
            $category = $cats->getFromCache($product['category_id']);
            $product_item['category_link'] = Url::get("category", ['category' => $category], true);
            $product_item['category_link_ru'] = Url::get("category", ['category' => $category, 'lang' => 'ru'], true);

            $data[] = $product_item;

        }

        return $this->renderXML('root', $data);

    }

    public function actionOrders()
    {

        return $this->end(); // currently not using this

        $xml = new \SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><Rekini></Rekini>');

        foreach($this->getOrderExportData() as $order){

            #$rekins_node = $xml->addChild('Rekins');
            $AccrualPZ = $xml->addChild('AccrualPZ');
            $header_node = $AccrualPZ->addChild('PZHeader');

            foreach($order['PZHeader'] as $key => $value){
                $header_node->addChild($key, htmlspecialchars($value));
            }

            $Ieraksti = $AccrualPZ->addChild("Ieraksti");
            foreach($order['Ieraksti'] as $item){

                $Ieraksts = $Ieraksti->addChild("Ieraksts");
                foreach($item as $key => $value){
                    $Ieraksts->addChild($key, htmlspecialchars($value));
                }

            }

        }

        $this->setContentType('text/xml');
        return $this->renderPartial('blank', ['content' => $xml->asXML()]);

    }

    private function getOrderExportData()
    {

        $order_col = \EVeikalsOrderCollection::getInstance();
        $order_prod_col = \shoporderprodcollection::getInstance();
        #$prod_col = \shopprodcollection::getInstance();

        $vat = option('Shop\\pvn', null, 'PVN (%)', null, 22) / 100;
        $orders = $order_col->getDBData(['assoc' => true]);

        $xml_orders = [];
        foreach($orders as $order){

            $orderdata = unserialize($order['order_data']);
            $allfields = $orderdata['allfields'];

            $xml_order = [];
            $header = [];

            $header['WEB'] = $order['item_id'];
            $header['Type'] = 6;
            $header['Nr'] = $order['num'];
            $header['Datums'] = $order['datums'];
            $header['PartnNosaukums'] = $order['pasutitajs'];
            $header['PartnKods'] = $allfields['payer'] == 'legal' ? $allfields['registration_nr'] : '';
            $header['Adrese'] = '';

            $header['Summa'] = round($order['kopeja_summa'] / (1 + $vat), 2);
            $header['PVNSumma'] = $order['kopeja_summa'] - $header['Summa'];
            $header['GalaSumma'] = $header['Summa'] + $header['PVNSumma'];
            $header['Valuta'] = 'EUR';

            $xml_order['PZHeader'] = $header;

            $items = [];
            foreach($order_prod_col->getOrderProducts($order['item_id']) as $product){

                $item = [];

                $item['ArticleId'] = $product['code'];
                $item['Artikuls'] = $product['code'];
                $item['EAN'] = $product['code'];
                $item['Nosaukums'] = $product['name_lv'];
                $item['Mervieniba'] = 'gab.';
                $item['PrecuCena'] = (float)round($product['base_price'] / (1 + $vat), 2);
                $item['CenaArPVN'] = (float)$product['base_price'];
                $item['Atlaide'] = (float)($product['base_price'] - $product['price']);
                $item['Cena'] = (float)$product['price'];
                $item['Daudzums'] = $product['count'];
                $item['Summa'] = (float)($product['count'] * $product['price']);
                $item['Nodoklis'] = 'PVN '.number_format($vat * 100).'%';
                $item['Likme'] = $vat * 100;

                $items[] = $item;

            }

            $xml_order['Ieraksti'] = $items;
            $xml_orders[] = $xml_order;

        }

        return $xml_orders;

    }

    private function getProducts()
    {

        $cond = ['disabled = 0 AND shortcut = 0'];

        $cats = \shopcatcollection::getInstance();
        $prods_col = \shopprodcollection::getInstance();

        $disabled_cat_ids = $cats->getInactiveCatIds();
        if(!empty($disabled_cat_ids)){
            $cond[] = "category_id NOT IN(".implode(",", $disabled_cat_ids).")";
        }

        $products = \DB::GetTable("
            SELECT *
            FROM `".$prods_col->table."`
            WHERE ".implode(" AND ", $cond)."
        ");

        return $products;

    }

    private function getCategoryFull($category_id, $lang = false)
    {

        static $cache;

        $cache_key = $category_id.'-'.$lang;

        if(!isset($cache[$cache_key])){

            $lang = $lang ? $lang : WebApp::$app->getLanguage();
            $cats = \shopcatcollection::getInstance();
            $cat = $cats->getFromCache($category_id);

            $category_full = [];

            do{
                $category_full[] = $cat['title_'.$lang];
                $cat = $cat['parent'] ? $cats->getFromCache($cat['parent']) : false;
            }while($cat);

            $category_full = implode("&gt;&gt;", array_reverse($category_full));
            $cache[$cache_key] = $category_full;

        }

        return isset($cache[$cache_key]) ? $cache[$cache_key] : '';

    }

}