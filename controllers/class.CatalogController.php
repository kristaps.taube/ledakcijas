<?php

namespace controllers;

use Constructor\Controller;
use Constructor\Url;
use Constructor\WebApp;

class CatalogController extends Controller
{

    public function actionIndex()
    {

        $this->setTemplateVar('breadcrumbs', ['<a href="'.Url::get("catalog").'">'.WebApp::l("Visas kategorijas").'</a>']);
        $this->setTitleTag(WebApp::l("Katalogs"));


        $categories = $this->getCategoryGroups();

        return $this->render("index", [
            'categories' => $categories
        ]);

    }

    private function getCategoryGroups()
    {

        $col = \shopcatcollection::getInstance();

        $categories = $col->getAllCatsByParent(0);

        return $categories;

    }

}