<?php

namespace controllers;

use Constructor\Controller;
use Constructor\Url;

class PayseraController extends Controller
{

    public function beforeAction()
    {

        $this->paysera = \EVeikalsPaysera::getInstance();
        $this->orders = \EVeikalsOrderCollection::getInstance();

        return parent::beforeAction();

    }

    public function actionPaymentSuccessful()
    {

        return $this->render('/empty', ['content' => WebApp::l("Pasūtījuma apmaksa veiksmīga")]);

    }

    public function actionPaymentSuccessful()
    {

        return $this->render('/empty', ['content' => WebApp::l("Pasūtījuma apmaksa atcelta")]);

    }

    public function actionAccept()
    {

        $id = isset($_GET['orderid']) ? $_GET['orderid'] : false;
        $order = $id ? $this->orders->getById($id): false;

        $url = ($order) ? Url::get('paysera/payment-successful', ['lang' => $order['valoda']]) : Url::get('paysera/payment-successful');

        return $this->redirect($url);

    }

    public function actionCancel()
    {

        $id = isset($_GET['orderid']) ? $_GET['orderid'] : false;
        $order = $id ? $this->orders->getById($id): false;

        $url = ($order) ? Url::get('paysera/payment-cancelled', ['lang' => $order['valoda']]) : Url::get('paysera/payment-cancelled');

        return $this->redirect($url);


    }

    public function actionCallback()
    {

        $response = $this->paysera->processCallback($_GET);
        return $this->renderPartial('/empty', ['content' => $response ? 'OK' : '']);

    }

}