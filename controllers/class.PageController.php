<?php

namespace controllers;

use Constructor\Controller;
use Constructor\WebApp;
use Constructor\Url;

class PageController extends Controller
{

    public function beforeAction($action, $params = [])
    {

        if($action == 'index' && isset($params['page'])){
            if($params['page']['redirect_url']){
                return $this->redirect($params['page']['redirect_url']);
            }elseif($params['page']['redirect']){
                $path = PagePathById($params['page']['redirect'], WebApp::$app->getSiteId());
                if($path){
                    return $this->redirect('/'.$path);
                }
            }
        }

        return true;

    }

    public function actionIndex(array $page)
    {

        $this->setTemplateVar('breadcrumbs', $this->makeBreadcrumbsItems($page));
        $this->setTemplateVar('page_id', $page['page_id']);

        $this->setTitleTag($page['title']);

        return $this->render("index", []);

    }

    private function makeBreadcrumbsItems($page)
    {

        $items = [];

        $items[] = "<a href='".Url::get('page', ['page' => $page])."'>".$page['title']."</a>";

        while($page && $page['parent']){

            $page = GetPageByID($page['parent']);

            if($page && $page['enabled'] && !$page['in_trash']){
                $items[] = "<a href='".Url::get('page', ['page' => $page])."'>".$page['title']."</a>";
            }else{
                $page = false;
            }
        }


        return array_reverse($items);

    }

}