<?php

namespace controllers;

use Constructor\Controller;
use Constructor\App;

class IndexController extends Controller
{

    protected $template = 'landing';

    public function actionIndex()
    {

        $this->forceTitleTag('LEDAkcijas.lv');

        $response = $this->render("index", []);
        return $response;

    }

}