<?php

namespace controllers;

use Constructor\Controller;
use Constructor\WebApp;
use Constructor\Url;
use ProductSorter;

class ProductListController extends Controller
{

    private $orders;
    private $page_sizes;

    public function __construct($controller_name)
    {

        parent::__construct($controller_name);

        $this->orders = [
            'popularity' => WebApp::l("Pirktākās vispirms"),
            'price-asc' => WebApp::l("Lētākās vispirms"),
            'price-desc' => WebApp::l("Dārgākās vispirms"),
        ];

        $this->page_sizes = [12, 15, 18, 21];

    }

    public function actionNew()
    {
        return $this->outputList('new');
    }

    public function actionPopular()
    {
        return $this->outputList('popular');
    }

    private function outputList($type)
    {

        $breadcrumbs = ["<a href='".Url::get($type == 'new' ? Url::get('new') : Url::get('popular'))."'>".($type == 'new' ? WebApp::l("Jaunākās preces") : WebApp::l("Pirktākās preces"))."</a>"];
        $this->setTemplateVar('breadcrumbs', $breadcrumbs);

        $page = isset($_GET['p']) ? (int)$_GET['p'] : 1;
        $page = $page >= 1? $page : 1;

        $products = $this->getProducts($type);
        $product_count = count($products);

        # sort it!
        $order = WebApp::$app->session->get('order', 'popular');
        $products = ProductSorter::sort($products, $order);

        # page it!
        $page_size = WebApp::$app->session->get('page_size', $this->page_sizes[0]);
        $products = array_slice($products, ($page - 1) * $page_size, $page_size);

        return $this->render('/category/index',[
            'title' => $type == 'new' ? WebApp::l("Jaunākās preces") : WebApp::l("Pirktākās preces"),
            'products' => $products,
            'page_size' => $page_size,
            'order' => $order,
            'orders' => $this->orders,
            'page_sizes' => $this->page_sizes,
            'page' => $page,
            'product_count' => $product_count,
        ]);

    }

    private function getProducts($type)
    {

        $cond = $params = [];
        $cond = ['shortcut = 0', 'disabled = 0'];

        if($type == 'new'){
            $cond[] = '(is_new = 1 AND (ISNULL(new_until) OR new_until="0000-00-00" OR NOW() < new_until))';
        }else{
            $cond[] = 'is_top = 1';
        }

        $products = \shopprodcollection::getInstance()->getDBData([
            'where' => implode(" AND ", $cond),
            'params' => $params
        ]);

        return $products;

    }

}