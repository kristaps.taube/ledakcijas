<?php

namespace controllers;

use Constructor\Controller;
use Constructor\Url;
use shopcatcollection;
use shopprodcollection;
use EVeikalsFrontendShop;
use Constructor\WebApp;
use DB;
use EVeikalsShopFilter;
use shopmanager;

class SearchController extends Controller
{

    private $orders;
    private $page_sizes;

    public function __construct($controller_name)
    {

        parent::__construct($controller_name);

        $this->orders = [
            'popularity' => WebApp::l("Pirktākās vispirms"),
            'price-asc' => WebApp::l("Lētākās vispirms"),
            'price-desc' => WebApp::l("Dārgākās vispirms"),
        ];

        $this->page_sizes = [12, 15, 18, 21];

    }

    public function beforeAction($action)
    {

        if(isset($_POST['needle'])){

            $needle = trim($_POST['needle']);
            $category = isset($_POST['category']) ? $_POST['category'] : 0;

            return $this->processPostSearch($needle, $category);

        }

        return true;

    }

    public function actionIndex()
    {

        $this->setTemplateVar('breadcrumbs', ['<span>'.WebApp::l("Meklēšanas rezultāti").'</span>']);

        $query = isset($_GET['q']) ? $_GET['q'] : '';
        $lang = WebApp::$app->getLanguage();

        $page = isset($_GET['p']) ? (int)$_GET['p'] : 1;
        $page = $page >= 1? $page : 1;

        $params = [];
        $cond = ["shortcut = 0 AND disabled = 0"]; // no shortcuts please!

        #get it
        $products = $this->searchProduts($query);

        #count it!
        $product_count = count($products);

        # sort it!
        $order = WebApp::$app->session->get('order', 'popular');
        $products = \ProductSorter::sort($products, $order);

        # page it!
        $page_size = WebApp::$app->session->get('page_size', $this->page_sizes[0]);
        $products = array_slice($products, ($page - 1) * $page_size, $page_size);

        return $this->render('/category/index', [
            'products' => $products,
            'title' => WebApp::l('Meklēšanas rezultāti'),
            'orders' => $this->orders,
            'order' => $order,
            'page_sizes' => $this->page_sizes,
            'page_size' => $page_size,
            'page' => $page,
            'product_count' => $product_count,
        ]);

    }

    private function searchProduts($query)
    {

        $lang = WebApp::$app->getLanguage();
        $cats = shopcatcollection::getInstance();
        $prods_col = shopprodcollection::getInstance();

        $disabled_cat_ids = $cats->getInactiveCatIds();
        if(!empty($disabled_cat_ids)){
            $cond[] = "category_id NOT IN(".implode(",", $disabled_cat_ids).")";
        }

        $search_cond = [];
        foreach(['name_'.$lang, 'code'] as $in){
            $search_cond[] = "lower(".$in.") LIKE :".$in;
            $params[":".$in] = '%'.strtolower($query).'%';
        }

        $cond[] = "(".implode(" OR ", $search_cond).")";

        $products = DB::GetTable("
            SELECT *
            FROM `".$prods_col->table."`
            WHERE ".implode(" AND ", $cond)."
        ", $params);

        return $products;

    }

    private function processPostSearch($needle, $category = null)
    {

        EVeikalsShopFilter::getInstance()->ClearCategoryFilter();
        $manager = shopmanager::getInstance();

        $url = $category ? $manager->getCategoryURL($category) : Url::get('search');
        $needle = trim($_POST['needle']);

        $url .= "?search=".urlencode($needle);

        return $this->redirect($url);

    }

    public function actionAutocomplete()
    {

        $query = isset($_GET['q']) ? $_GET['q'] : '';
        $lang = isset($_GET['l']) ? $_GET['l'] : '';
        $langs = getLanguages(WebApp::$app->getSiteId());
        $lang = isset($langs[$lang]) ? $lang : WebApp::$app->getLanguage();

        $params = [];
        $cond = ["shortcut = 0 AND disabled = 0"]; // no shortcuts please!

        if($query){

            $cats = shopcatcollection::getInstance();
            $prods_col = shopprodcollection::getInstance();

            $disabled_cat_ids = $cats->getInactiveCatIds();
            if(!empty($disabled_cat_ids)){
                $cond[] = "category_id NOT IN(".implode(",", $disabled_cat_ids).")";
            }

            $search_cond = [];
            foreach(['name_'.$lang, 'code'] as $in){
                $search_cond[] = "lower(".$in.") LIKE :".$in;
                $params[":".$in] = '%'.strtolower($query).'%';
            }

            $cond[] = "(".implode(" OR ", $search_cond).")";

            $prods = DB::GetTable("
                SELECT *
                FROM `".$prods_col->table."`
                WHERE ".implode(" AND ", $cond)."
                ORDER BY name_".$lang."
                LIMIT 100
            ", $params);

        }else{
            $prods = [];
        }

        return $this->renderPartial('autocomplete', [
            'products' => $prods,
            'lang' => $lang,
            'query' => $query
        ]);

    }

}
