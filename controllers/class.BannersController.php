<?php

namespace controllers;

use Constructor\Controller;
use Constructor\URL;
use Constructor\App;
use shopsmallcart;

class BannersController extends Controller
{

    public function actionClick()
    {

        $redirect_url = '/';

        $id = isset($_GET['b']) ? (int)$_GET : 0;
        $lang = isset($_GET['lang']) ? $_GET['lang'] : false;

        $langs = getLanguages(App::$app->getSiteId());

        if($lang && isset($langs[$lang]) && $id){

            $collection = \bannerlistdbcollection::getInstance('banners_'.$lang);
            $banner = $collection->getById($id);
            if($banner){
                $collection->Update(["hits" => $banner['hits'] + 1], $banner['item_id']);
                $redirect_url = $banner['link'] ? $banner['link'] : '/';
            }

        }

        #
        return $this->redirect($redirect_url);

    }



}