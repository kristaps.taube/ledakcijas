<?php

namespace controllers;

use Constructor\Controller;
use Constructor\URL;
use Constructor\WebApp;
use shopsmallcart;

class CompareController extends Controller
{

    public function actionIndex()
    {

        $this->setTemplateVar('breadcrumbs', ['<a href="'.Url::get("compare").'">'.WebApp::l("Preču salīdzināšana").'</a>']);

        $products = $this->getProducts();

        $compare_data = $this->getCompareData($products);
        
        return $this->render('index', [
            'products' => $products,
            'compare_data' => $compare_data,
            'error' => WebApp::$app->session->getFlash('compare_error')
        ]);

    }

    private function getCompareData($products)
    {

        $lang = WebApp::$app->getLanguage();

        $data = [];

        if($products){

            $prod_ids = [];
            foreach($products as $product){
                $prod_ids[] = $product['item_id'];
            }

            $filter_groups = \EVeikalsFilterGroups::getInstance();
            $filters = \EVeikalsFilters::getInstance();
            $prod_filters = \EVeikalsProdFilterRel::getInstance();

            $table = \DB::GetTable("
                SELECT fg.*,f.*, pf.product_id
                FROM `".$filters->table."` AS f
                JOIN `".$filter_groups->table."` AS fg ON fg.item_id = f.group
                JOIN `".$prod_filters->table."` AS pf ON pf.filter_id = f.item_id AND pf.product_id IN(".implode(",", $prod_ids).")
                ORDER BY fg.ind
            ");

            foreach($table as $row){
                if(!isset($data[$row['product_id']])){
                    $data[$row['product_id']] = [];
                }
                $data[$row['product_id']][] = $row;
            }

        }

        return $data;

    }

    private function getProducts()
    {

        $ids = WebApp::$app->session->get('compare', []);

        if($ids){

            $cond = ['disabled = 0'];
            $cond[] = 'item_id IN('.implode(',', $ids).')';

            $products = \shopprodcollection::getInstance()->getTable(['where' => implode(" AND ", $cond)]);
        }else{
            $products = [];
        }

        return $products;

    }

    public function actionAdd()
    {

        $id = isset($_GET['id']) ? $_GET['id'] : false;
        $lang = isset($_GET['lang']) ? $_GET['lang'] : WebApp::$app->getLangauge();

        if($id){
            $compare = WebApp::$app->session->get('compare', []);

            if(count($compare) < 4){

                if(!in_array($id, $compare)){
                    $compare[] = $id;
                }
                WebApp::$app->session->set('compare', $compare);
            }else{

                WebApp::$app->session->setFlash('compare_error', WebApp::l("Jūs varat salīdzīnāt līdz 4 precēm", ['lang' => $lang]));

            }
        }

        return $this->redirect(Url::get("compare"));

    }

    public function actionRemove()
    {

        $id = isset($_GET['id']) ? $_GET['id'] : 0;
        $ids = WebApp::$app->session->get('compare', []);
        foreach($ids as $k => $iid){
            if($iid == $id){
                unset($ids[$k]);
            }
        }

        WebApp::$app->session->set('compare', array_values($ids));
        WebApp::$app->session->setFlash('compare_removed', true);
        return $this->redirectBack();

    }

}