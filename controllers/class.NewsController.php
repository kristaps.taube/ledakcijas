<?php

namespace controllers;

use Constructor\Controller;
use Constructor\App;

class NewsController extends Controller
{

    private $page_size = 10;

    public function beforeAction($action, $params = [])
    {

        # var_dump($action);
        # var_dump($params);
        # die();
        return true;

    }

    public function actionIndex()
    {

        $page = isset($_GET['p']) ? (int)$_GET['p'] : 1;
        $page = $page > 1 ? $page : 1;

        $data = $this->getNews($page);

        return $this->render("index", [
            'news' => $data['data'],
            'count' => $data['count'],
            'page' => $page,
            'page_size' => $this->page_size
        ]);

    }

    public function actionView(array $entry)
    {
        return $this->render('view', ['entry' => $entry]);
    }

    private function getNews($page)
    {

        $newscol = \newnewscollection::getInstance('news_'.App::$app->getLanguage());

        $cond = [];

        return $newscol->getNews(implode(' AND ', $cond), $page, $this->page_size);

    }

}