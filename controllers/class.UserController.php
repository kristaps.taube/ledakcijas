<?php

namespace controllers;

use Constructor\Controller;
use EVeikalsLogin;
use shopusercollection;
use Constructor\WebApp;
use Constructor\Url;

class UserController extends Controller
{

    public function beforeAction($action, $params = [])
    {

        if(isset($_GET['action']) && $_GET['action'] == 'logout'){
            return $this->processLogout();
        }

        return true;

    }

    public function actionIndex()
    {
        return $this->redirect('/');
    }

    public function actionOrderHistory()
    {

        $this->setTemplateVar('breadcrumbs', ['<a href="'.Url::get("profile/order-history").'">'.WebApp::l("Pasūtījumu vēsture").'</a>']);

        if(isset($_POST["date-from"]) && isset($_POST["date-to"])){

            WebApp::$app->session->set('order_date_from', date('Y-m-d 00:00:00', strtotime($_POST["date-from"])));
            WebApp::$app->session->set('order_date_to', date('Y-m-d 23:59:59', strtotime($_POST["date-to"])));
            return $this->redirectBack();

        }

        $user = WebApp::$app->user->get();

        $status_col = \EVeikalsOrderStatuses::getInstance();
        $order_col = \EVeikalsOrderCollection::getInstance();

        $order_status_filter = WebApp::$app->session->get('order_status_filter', false);

        $cond = ['user_id = :uid'];
        $params = [':uid' => $user['item_id']];

        if($order_status_filter){
            $cond[] = 'status_id = :sid';
            $params[':sid'] = $order_status_filter;
        }

        $date_from = WebApp::$app->session->get('order_date_from', date('Y-m-01 00:00:00'));
        $date_to = WebApp::$app->session->get('order_date_to', date('Y-m-d 21:59:59'));

        $cond[] = 'datums >= :date_from';
        $params[':date_from'] = $date_from;

        $cond[] = 'datums <= :date_to';
        $params[':date_to'] = $date_to;

        $orders = $order_col->getDBData(['where' => implode(" AND ", $cond), 'order' => 'datums DESC', 'params' => $params]);

        $statuses = [];
        foreach($status_col->getDBData() as $status){
            $statuses[$status['item_id']] = $status;
        }

        return $this->render('order-history', [
            'statuses' => $statuses,
            'orders' => $orders,
            'order_status_filter' => $order_status_filter,
            'date_from' => $date_from,
            'date_to' => $date_to,
        ]);

    }

    public function actionAddToCart()
    {

        $id = isset($_GET['id']) ? $_GET['id'] : false;

        $user = WebApp::$app->user->get();

        $order_col = \EVeikalsOrderCollection::getInstance();
        $order_prod_col = \shoporderprodcollection::getInstance();
        $entry = $order_prod_col->getById($id);

        if($entry){

            $order = $order_col->getById($entry['order_id']);
            if($order && $order['user_id'] == $user['item_id']){


                WebApp::$app->cart->addItem(['id' => $entry['prod_id']], $entry['count']);

            }

        }


        return $this->redirectBack();


    }

    public function actionChangePassword()
    {

        return $this->render('change-password', []);

    }

    public function actionDownloadOrder()
    {

        $id = isset($_GET['o']) ? $_GET['o'] : false;

        $user = WebApp::$app->user->get();

        $order = \EVeikalsOrderCollection::getInstance()->GetRow(array("where" => "item_id = :id AND user_id = :uid", "params" => array(":id" => $id, ":uid" => $user['item_id'])));

        if($order){
            return $this->renderPdf($order['num'].".pdf", $order['rekins']);
        }else{
            return $this->redirect(Url::get("profile/order-history"));
        }

    }

    public function actionChangeStatusFilter()
    {

        $status = isset($_GET['status']) ? $_GET['status'] : false;

        $ok = false;
        $status_col = \EVeikalsOrderStatuses::getInstance();
        foreach($status_col->getDBData() as $s){
            if($s['item_id'] == $status){
                $ok = true;
                break;
            }
        }

        if($ok){
            WebApp::$app->session->set('order_status_filter', $status);
        }elseif(!$status){
            WebApp::$app->session->clear('order_status_filter');
        }

        return $this->redirectBack();

    }

    private function processOrderView($id)
    {

        $order = \EVeikalsOrderCollection::getInstance()->GetRow(array("where" => "item_id = :id AND user_id = :uid", "params" => array(":id" => $id, ":uid" => $_SESSION['logged_in']['item_id'])));
        return $this->render("order-view", ['order' => $order]);// this should be written differently and u know it

    }

    private function processAddOrder($id)
    {
        $order = \EVeikalsOrderCollection::getInstance()->GetRow(array("where" => "item_id = :id AND user_id = :uid", "params" => array(":id" => $id, ":uid" => $_SESSION['logged_in']['item_id'])));

        if($order){

            $shopmanager = \shopmanager::getInstance();

            $order_data = unserialize($order['order_data']);
            $prods = $order_data['products'];

            $_SESSION['cartitems'] = array(); // clear current basket
            foreach($prods as $prod){

                if(!$prod['giftcard']){
                    $item = ["id" => $prod['id']];
                    $item['price_id'] = $prod['price_id'];
                    \shopsmallcart::getInstance()->addCartItem($item, $prod['count']);
                }else{
                    $shopmanager->addGiftCard($prod);
                }

            }

            $url = "?action=view&id=".$order['item_id']."&added=1";

        }else{
            $url = Url::get("profile/order-history");
        }

        return $this->redirect($url);

    }

    public function actionLogin()
    {

        return $this->render('login', []);

    }

    public function actionLogout()
    {

        WebApp::$app->user->logout();

        $ref = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '/';
        return $this->redirect($ref);

    }

    public function actionPasswordRecovery()
    {

        return $this->render('password-recovery', []);

    }

    public function actionProcessLogin()
    {

        $data = $_POST;

        $user = isset($data['email']) ? shopusercollection::getInstance()->getUserByEmail($data['email']) : false;
        $login = EVeikalsLogin::getInstance();
        $response = false;

        if($user){

            $salt = substr($user['password'], 0, 5);
            $hash = substr($user['password'], 5);

            if(sha1($salt.$data['password']) != $hash){

                $error= $login->l("Nepareiza parole");

            }elseif($user['active'] != 1){

                $error = $login->l("Konts nav aktivizēts");

            }else{

                WebApp::$app->user->authorizeUserEntry($user);
                $response = true;

            }

        }else{

            $error = $login->l("Lietotājs nav atrasts");

        }

        if($error){
            $login->setError($error);
        }

        $ref = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '/';

        return $this->redirect($ref);

    }

}