<?php

namespace controllers;

use Constructor\Controller;
use Constructor\URL;
use Constructor\WebApp;
use ProductSorter;

class WishlistController extends Controller
{

    private $user;
    private $orders;
    private $page_sizes;

    public function __construct($controller_name)
    {

        parent::__construct($controller_name);

        $this->orders = [
            'popularity' => WebApp::l("Pirktākās vispirms"),
            'price-asc' => WebApp::l("Lētākās vispirms"),
            'price-desc' => WebApp::l("Dārgākās vispirms"),
        ];

        $this->page_sizes = [12, 15, 18, 21];

    }

    public function beforeAction($action, $params = [])
    {

        $this->user = WebApp::$app->user->get();

        return $this->user ? true : $this->redirect('/');

    }

    public function actionIndex()
    {

        $this->setTemplateVar('breadcrumbs', ['<a href="'.Url::get("wishlist").'">'.WebApp::l("Vēlmju saraksts").'</a>']);

        $col = \WishlistCollection::getInstance();

        $page = isset($_GET['p']) ? (int)$_GET['p'] : 1;
        $page = $page >= 1? $page : 1;

        #get it
        $products = $col->getProductsForUser($this->user['item_id']);

        #count it!
        $product_count = count($products);

        # sort it!
        $order = WebApp::$app->session->get('order', 'popular');
        $products = ProductSorter::sort($products, $order);

        # page it!
        $page_size = WebApp::$app->session->get('page_size', $this->page_sizes[0]);
        $products = array_slice($products, ($page - 1) * $page_size, $page_size);

        return $this->render('/category/index', [
            'products' => $products,
            'title' => WebApp::l('Mans vēlmju saraksts'),
            'add_success' => WebApp::$app->session->getFlash('wishlist_add_success'),
            'add_error' => WebApp::$app->session->getFlash('wishlist_add_error'),
            'remove_success' => WebApp::$app->session->getFlash('wishlist_remove_success'),
            'orders' => $this->orders,
            'order' => $order,
            'page_sizes' => $this->page_sizes,
            'page_size' => $page_size,
            'page' => $page,
            'product_count' => $product_count,
        ]);

    }

    public function actionToggle()
    {

        $id = isset($_GET['id']) ? $_GET['id'] : false;

        if($id){
            $col = \WishlistCollection::getInstance();

            $check = $col->getByProductAndUser($id, $this->user['item_id']);

            if($check){
                $response = 0;
                $col->Delete($check['item_id']);
            }else{

                $col->Insert([
                    'product_id' => $_GET['id'],
                    'user_id' => $this->user['item_id']
                ]);
                $response = 1;

            }

        }

        if($response){
            WebApp::$app->session->setFlash('wishlist_add_success', true);
        }else{
            WebApp::$app->session->setFlash('wishlist_remove_success', true);
        }

        return $this->redirect(Url::get("wishlist"));

    }

    public function actionAdd()
    {

        $id = isset($_GET['id']) ? $_GET['id'] : false;

        if($id){
            $col = \WishlistCollection::getInstance();
            if($col->add($this->user['item_id'], $id)){
                WebApp::$app->session->setFlash('wishlist_add_success', true);
            }else{
                WebApp::$app->session->setFlash('wishlist_add_error', true);
            }
        }

        return $this->redirect(Url::get("wishlist"));

    }

}