<?php

namespace controllers;

use Constructor\WebApp;
use Constructor\Controller;
use Constructor\URL;
use shopsmallcart;

class CartController extends Controller
{

    public function actionIndex()
    {

        $this->setTemplateVar('breadcrumbs', ['<a href="'.Url::get("cart").'">'.WebApp::l("Pasūtījuma noformēšana").'</a>']);
        $this->setTitleTag(WebApp::l("Pasūtījuma noformēšana"));

        $orderform = \OrderForm::getInstance();

        $buyers_info = WebApp::$app->session->get('buyers_information', []);
        $orderform->loadFieldData($buyers_info);

        if(isset($_POST['checkout']) && ($order_id = $orderform->processCheckout($_POST))){

            $fields = $orderform->getFieldValues();
            if($fields['payment_method'] == 'online'){
                $paysera = \EVeikalsPaysera::getInstance();
                $paysera->redirectToPayment($order_id);
            }else{
                $redirect_url = Url::get('cart/done', ['d' => $order_id]);
            }

            // all good, we should redirect
            return $this->redirect($redirect_url);

        }

        $cart = WebApp::$app->cart;
        $items = $cart->getItems();

        if($items){

            return $this->render("index", [
                'cart' => $cart,
                'items' => $items,
                'orderform' => $orderform
            ]);

        }else{

            return $this->render("empty", []);

        }

    }

    public function actionPayseraTest()
    {

        $paysera = \EVeikalsPaysera::getInstance();

        $url = $paysera->redirectToPayment(59);

        echo $url;

        return $this->end();
    }

    public function actionDone()
    {

        $d = isset($_GET['d']) ? $_GET['d'] : null;
        $this->setTemplateVar('breadcrumbs', ['<a href="'.Url::get("cart/done", ['d' => $d]).'">'.WebApp::l("Pasūtījums noformēts").'</a>']);
        return $this->render('done');

    }

    public function actionAdd()
    {

        $count = isset($_GET['count']) ? (int)$_GET['count'] : 1;
        $count = $count > 0 ? $count : 1;

        $product = \shopprodcollection::getInstance()->getById((int)$_GET['p']);

        if($product){
            $item = ['id' => $product['item_id']];
            WebApp::$app->cart->addItem($item, $count);
        }

        return $this->redirectBack();

    }

    public function actionChangeCount()
    {

        $key = isset($_POST['key']) ? $_POST['key'] : false;
        $count = isset($_POST['count']) ? $_POST['count'] : false;

        if($key !== false && $count !== false && $count > 0){
            WebApp::$app->cart->changeCount($key, $count);
        }

        return $this->end();

    }

    public function actionRemove()
    {

        $t = isset($_GET['key']) ? WebApp::$app->cart->removeItemByKey($_GET['key']) : false;

        $ajax = isset($_GET['a']) && $_GET['a'];

        if($ajax){
            $count = count(WebApp::$app->cart->getItems());
            $sum = WebApp::$app->cart->getSum(true);

            $response = [
                'status' => $t,
                'sum' => $sum,
                'count' => $count
            ];
        }

        return $ajax ? $this->renderJSON($response) : $this->redirectBack();

    }

}