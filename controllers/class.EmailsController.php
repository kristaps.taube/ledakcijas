<?php

namespace controllers;

use Constructor\Controller;

class EmailsController extends Controller
{

    public function actionIndex()
    {

        $col = \EMailNotificationCollection::getInstance();

        $hash = isset($_GET['h']) ? $_GET['h'] : false;

        $notification = false;
        if($hash){
            $notification = $col->getByHash($hash);
            if($notification){
                $this->template = 'empty';
            }
        }

        return $this->render('index', ['notification' => $notification]);

    }

}