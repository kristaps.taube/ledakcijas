<?php

namespace controllers;

use Constructor\Controller;
use Constructor\App;

require_once($GLOBALS['cfgDirRoot'].'library/PHPExcel.php');
require_once($GLOBALS['cfgDirRoot'].'library/PHPExcel/IOFactory.php');

class MaintenanceController extends Controller
{

    public function actionTest()
    {

        # delete old collections
        /*$data = \DB::Gettable('SELECT * FROM veikals.4_collections');

        foreach($data as $row){

            $table_name = '4_coltable_'.strtolower($row['name']);

            $check = \DB::GetValue("SHOW TABLES LIKE '".$table_name."'");

            if(!$check){
                echo "No table for ".$row['name'].'('.$row['type'].')<br />';
                \DB::query('DELETE FROM 4_collections WHERE collection_id = '.$row['collection_id']);
            }else{

                $entries = \DB::GetValue('SELECT count(*) FROM `'.$table_name.'`');
                if(!$entries){
                    echo "No entries for collection ".$row['name'].'('.$row['type'].') table: '.$table_name.'<br />';

                    $col = new $row['type']($row['name'], $row['collection_id']);
                    echo 'Table name: '.$col->table."<br />";

                    $col->DelCollection();
                }

            }

        }   */

    }

    public function actionTestOne()
    {

        return '';

    }

    public function actionRenewPasts()
    {

        $local_file = App::$app->getConfigValue('temp_folder').'Pasts.xls';

        $objReader = \PHPExcel_IOFactory::createReader('Excel5');
        $objPHPExcel = $objReader->load($local_file);

        $worksheet = $objPHPExcel->getActiveSheet();
        $table = \ExcelHelper::worksheetToArray($worksheet);

        $collection = \PastsCollection::getInstance();
        #$collection->Truncate();

        unset($table[0],$table[1],$table[2],$table[3],$table[4],$table[5]); // headers
        foreach($table as $row){

            if(!$collection->getByzip($row[1])){

                $coords = $this->addressToCoords($row[2], $row[0],', '.$row[1]);

                $entry = ['zip' => $row[1]];
                if($coords){
                    $entry['coords'] = str_replace(',', '.', $coords['lat']).','.str_replace(',', '.', $coords['lng']);
                }

                foreach(getLanguages() as $short => $lang_data){
                    $entry['address_'.$short] = $row[2];
                    $entry['name_'.$short] = $row[0];
                }

                $collection->Insert($entry);

            }

        }

        return $this->renderJSON($table); 

    }

    public function actionSmsSenders()
    {

        $sender = new \SalesTraffic('d2d4fe98db58b6c221bc60a46a8483bdae180ea9');

        $response = $sender->makeAPICall([
            'Command' => 'GetSenders'
        ]);

        return $this->renderJSON($response);

    }

    public function actionRenewCircleK()
    {

        $local_file = App::$app->getConfigValue('temp_folder').'CircleK.xls';

        $objReader = \PHPExcel_IOFactory::createReader('Excel5');
        $objPHPExcel = $objReader->load($local_file);

        $worksheet = $objPHPExcel->getActiveSheet();
        $table = \ExcelHelper::worksheetToArray($worksheet);

        $collection = \CircleKCollection::getInstance();
        #$collection->Truncate();

        unset($table[0]); // header
        foreach($table as $row){

            if(!$collection->getByNr($row[2])){

                $entry = ['nr' => $row[2]];

                $coords = $this->addressToCoords($row[3]);
                if($coords){
                    $entry['coords'] = str_replace(',', '.', $coords['lat']).','.str_replace(',', '.', $coords['lng']);
                }else{
                    echo 'Nav coords: '.$row[0].'<br />';
                }

                foreach(getLanguages() as $short => $lang_data){
                    $entry['address_'.$short] = $row[3];
                    $entry['name_'.$short] = $row[0];
                }

                $collection->Insert($entry);

            }

        }

        return $this->renderJSON($table);

    }

    public function actionRenewDPDPlaces()
    {

        $dpd_collection = \DPDPickupCollection::getInstance();
        #$dpd_collection->Truncate();

        $ftp_server = 'mail.dpd.lv';
        $ftp_user_name = 'pudo';
        $ftp_user_pass = 'dpdpudo';
        $local_file = App::$app->getConfigValue('temp_folder').'dpd.xlsx';

        $conn_id = ftp_connect($ftp_server);
        $login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);

        if($login_result){

            #error_reporting(E_ALL);
            #ini_set('display_errors', 1);
            $handle = fopen($local_file, 'w');
            $result = ftp_fget($conn_id, $handle, 'PUDO 1a.xlsx', FTP_BINARY);

            if($result){

                $objReader = \PHPExcel_IOFactory::createReader('Excel2007');
                $objPHPExcel = $objReader->load($local_file);

                $worksheet = $objPHPExcel->getActiveSheet();
                $table = \ExcelHelper::worksheetToArray($worksheet);

                unset($table[0]); // header
                $current_places = [];
                foreach($table as $row){

                    $id = $row[0];

                    if($row[9]){
                        $city = $row[9];
                    }elseif($row[10]){
                        $city = $row[10];
                    }elseif($row[8]){
                        $city = $row[8];
                    }

                    $name = ($row[3] ? $row[3]." ".$row[4] : $row[2]).", ".$city.", ".$row[5]."-".$row[6];

                    $check = $dpd_collection->getByImportID($id);
                    if(!$check){
                        $data = ['import_id' => $id];

                        $coords = $this->addressToCoords($name);
                        if($coords){
                            $data['coords'] = str_replace(',', '.', $coords['lat']).','.str_replace(',', '.', $coords['lng']);
                        }else{
                            echo 'Nav coords: '.$name.'<br />';
                        }

                        foreach(getLanguages() as $short => $lang_data){
                            $data['address_'.$short] = $name;
                            $data['city_'.$short] = $city;
                        }
                        $dpd_collection->Insert($data);
                    }

                }

            }

        }

        return $this->end();

    }

    public function actionRenewPastastacija()
    {

        $url = 'http://klients.pastastacija.lv/api/terminals.json';
        $data = json_decode(file_get_contents($url), true);

        $collection = \PastastacijaCollection::getInstance();
        #$collection->Truncate();

        $processed_ids = [];

        foreach($data as $entry){

            $processed_ids[] = $entry['id'];

            if(!$collection->getByImportID($entry['id'])){

                $data = ['import_id' => $entry['id']];

                $coords = $this->addressToCoords($entry['name'], $entry['terminal_address']);
                if($coords){
                    $data['coords'] = str_replace(',', '.', $coords['lat']).','.str_replace(',', '.', $coords['lng']);
                }else{
                    echo 'Nav coords: '.$entry['name'].'<br />';
                }

                foreach(getLanguages() as $short => $lang_data){
                    $data['address_'.$short] = $entry['terminal_address'];
                    $data['city_'.$short] = $entry['city_name'];
                }

                $collection->Insert($data);

            }

        }

        # delete missing addresses
        foreach($collection->getDBData() as $entry)
        {

            if(!in_array($entry['import_id'], $processed_ids)){
                $collection->Delete($entry['item_id']);
            }

        }

        return $this->renderJSON(1);

    }

    public function actionRenewOmnivaPlaces()
    {

        $address_col = \OmnivaAddressCollection::getInstance();
        $address_col->Truncate();

        $get = file_get_contents('http://www.omniva.lv/locations.xml');
        $arr = simplexml_load_string($get);

        $processed_zips = [];
        $addresses = [];

        foreach($arr as $a){

            if($a->A0_NAME != 'LV') continue;

            $city = (string)$a->A1_NAME;
            $address = (string)$a->A2_NAME."(".(string)$a->NAME.")";
            $zip = (string)$a->ZIP;
            $processed_zips[] = $zip;

            if(!$address_col->getByZip($zip)){

                $coords = $a->Y_COORDINATE.','.$a->X_COORDINATE;

                $data = ['zip' => $zip, 'coords' => $coords];

                foreach(getLanguages() as $short => $language){

                    $data['city_'.$short] = $city ;
                    $data['address_'.$short] = $address ;

                }

                $address_col->Insert($data);

            }

        }

        # delete missing addresses
        foreach($address_col->getDBData() as $address)
        {

            if(!in_array($address['zip'], $processed_zips)){
                $address_col->Delete($address['item_id']);
            }

        }

        return $this->renderJSON(1);

    }

    private function addressToCoords($address, $alt = false)
    {

       $result = file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($address)."&sensor=true&key=AIzaSyCnZd998kcoxLoJ4SaSOE3ty05VlXRZSxE");

       $result = json_decode($result, true);
       $location = $result['results'][0]['geometry']['location'];

       if(!$location && $alt){
           $location = $this->addressToCoords($alt);
       }

       if(!$location){

        #var_dump($address);
        #var_dump($result);

       }

       return $location;

    }




}