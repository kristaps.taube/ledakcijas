<?php
/**
* Constructor Autoloader
*
* Version 1.0 (25-01-2018)
* @author Kristaps Taube <ktaube@datateks.lv>
*
**/

class AutoLoader {

    protected static $paths = [
        '',
        'library',
        'components',
        'collections',
        'controllers'
    ];

    public static $base;

    public static function setBase($base)
    {
        self::$base = realpath($base);
    }

    public static function addPath($path)
    {
        $path = realpath($path);
        if ($path) {
            self::$paths[] = $path;
        }
    }

    public static function load($class)
    {

        $file = str_replace('\\', DIRECTORY_SEPARATOR, $class); // namespaces
        $parts = array_filter(explode(DIRECTORY_SEPARATOR, $file));

        if(count($parts) > 1){
            $file = $parts[count($parts)-1];
            unset($parts[count($parts)-1]);
            $classPath = DIRECTORY_SEPARATOR.implode(DIRECTORY_SEPARATOR, $parts).DIRECTORY_SEPARATOR.'class.'.$file.'.php'; // filename
        }else{
            $classPath = DIRECTORY_SEPARATOR.'class.'.$file.'.php'; // filename
        }

        foreach (self::$paths as $path) {

            if(self::$base){
                $path = self::$base.DIRECTORY_SEPARATOR.$path;
            }

            $path = realpath($path);

            if (is_file($path . $classPath)){
                require_once $path . $classPath;
                return;
            }

        }

    }

}

