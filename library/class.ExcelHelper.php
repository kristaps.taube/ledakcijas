<?php


class ExcelHelper
{

    public static function worksheetToArray($worksheet)
    {

        $columncount = self::colNumber($worksheet->getHighestColumn());

        // excel to array
        $table = [];
        for($i = 1; $i <= $worksheet->getHighestRow(); $i++){
            $row = [];
            for($j = 0; $j <= $columncount; $j++){
                $value = trim($worksheet->getCell(self::colLetter($j).$i)->getCalculatedValue());
                $row[] = $value;

            }
            $table[] = $row;
        }

        return $table;

    }


    private static function colNumber($c){
        return ord(strtolower($c)) - 96;
    }

    private static function colLetter($i){
        return chr(ord('A')+$i);
    }


}