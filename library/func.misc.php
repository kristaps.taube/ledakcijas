<?

$stdlib = 1;

/* Misc function library */

function microtime_float()
{
   list($usec, $sec) = explode(" ", microtime());
   return ((float)$usec + (float)$sec);
}

function output_captcha($reload = "Nevarat salasīt? Nomainīt bildi!"){
?>
<img src="http://<?=$_SERVER["HTTP_HOST"]?>/cms/library/captcha/captcha.php" id="captcha" /><br/>
<a href="#" onclick="
    document.getElementById('captcha').src='http://<?=$_SERVER["HTTP_HOST"]?>/cms/library/captcha/captcha.php?'+Math.random();
    document.getElementById('captcha-form').focus(); return false;"
    id="change-image"><?=$reload?></a> <br />
<input type="text" name="captcha" id="captcha-form"  />

<?
}

function verifyDate($date, $format = 'Y-m-d H:i:s', $strict = true){
    $dateTime = DateTime::createFromFormat($format, $date);
    if ($strict) {
        $errors = DateTime::getLastErrors();
        if (!empty($errors['warning_count'])) {
            return false;
        }
    }
    return $dateTime !== false;
}

function stripslashes_array(&$arr){

	if (is_array($arr)){
  	foreach($arr as $k => $v){
    	if (is_array($v)){
      	$arr[$k] = stripslashes_array($v);
      }elseif (is_string($v)){
      	$arr[$k] = stripslashes ($v);
      }
    }
  }

}

function addslashes_array($arr){

	if (is_array($arr)){
  	foreach($arr as $k => $v){
    	if (is_array($v)){
      	$arr[$k] = addslashes_array($v);
      }elseif (is_string($v)){
      	$arr[$k] = addslashes($v);
      }
    }
  }

  return $arr;

}

function replace_all($text, $replacements){

  foreach($replacements as $old => $new){

    $text = str_replace($old, $new, $text);

  }

  return $text;

}

// 21.12.2013 => 2013-12-21
function convertDateToMySQL($date){

  $parts = explode(".", $date);
  if(count($parts) != 3) return false;

  return $parts[2]."-".$parts[1]."-".$parts[0];

}

// 2013-12-21 => 21.12.2013
function convertDateFromMySQL($date){

  $parts0 = explode(" ", $date);
  $parts1 = explode("-", $parts0[0]);
  $parts2 = explode(":", $parts0[1]);

  $time = mktime($parts2[0]?$parts2[0]:0, $parts2[1]?$parts2[2]:0, $parts2[2]?$parts2[2]:0, $parts1[1], $parts1[2], $parts1[0]);

  return date("d.m.Y", $time);

}


//$action_type
//  1 - start sql query
//  2 - end sql query
//  3 - start component output
//  4 - end compoent output
//  5 - start component design
//  6 - end component design
//  7 - start profiler session
//  8 - end profiler session
function profiler_add($action, $action_type)
{

  if($GLOBALS['cfgUseProfiler'] && (!$GLOBALS['developmode'] || $_GET['module'] != 'profiler'))
  {
    $GLOBALS['cfgUseProfiler'] = false;
    if(!$GLOBALS['profiler_start_time'])
      $GLOBALS['profiler_start_time'] = time();
    $t = microtime_float();
    $index = 'last_profiler_time' . ($action_type - 1);
    if($GLOBALS[$index] && ($action_type % 2) == 0)
      $dt = $t - $GLOBALS[$index];
    else
      $dt = 0;
    sqlQuery('INSERT INTO `profiler` (`profilestart`, `action`, `action_type`, `time`, `deltatime`, `component`) VALUES ("'.$GLOBALS['profiler_start_time'].'", "' . addslashes($action) . '", '.$action_type.', '.number_format($t, 2, '.', '').', '.number_format($dt, 2, '.', '').', "'.$GLOBALS['profiler_component'].'")');
    $GLOBALS['last_profiler_time' . $action_type] = microtime_float();
    $GLOBALS['cfgUseProfiler'] = true;
  }
}

function image_preview_out($filename, $webname, $w, $h) {
  $size = GetImageSize($filename);
  $width = $size[0];
  $height = $size[1];
  if ($w < $width || $h < $height) {
    $ratio = min($w/$width, $h/$height);
    $width = ceil($width * $ratio);
    $height = ceil($height * $ratio);
  }
  $html = "<img src=\"$webname\" width=\"$width\" height=\"$height\" border=\"1\">";
  return $html;
}

function ShortenString($s, $n)
{
  if(strlen($s) <= $n)
    return $s;
  $s = substr($s, 0, $n);
  $s1 = $s;
  for(;$n>0;$n--)
  {
    if(strchr(" \n\t.,-", $s{$n}) !== false)
      return substr($s, 0, $n) . "...";
  }
  return $s1;
}


function image_tag($site_id, $webname) {
  if($webname)
  {
    $domain = sqlQueryValue("SELECT domain FROM sites WHERE site_id=" . $site_id);
    $size = GetImageSize('http://' . $domain . '/' . $webname);
    $width = $size[0];
    $height = $size[1];
    if(($width)and($height))
      $html = "<img src=\"$webname\" width=\"$width\" height=\"$height\" border=\"0\">";
    else
      $html = "<img src=\"$webname\" border=\"0\">";
    return $html;
  }
}


function pv(&$var) {
  echo isset($var) ? nl2br(stripslashes($var)) : "";
}

function pt($var) {
  echo nl2br(stripslashes($var));
}

function strip_querystring($url) {
  if ($commapos = strpos($url, '?')) {
    return substr($url, 0, $commapos);
  } else {
    return $url;
  }
}

function get_querystring($url) {
  if ($commapos = strpos($url, '?')) {
    return substr($url, $commapos+1);
  } else {
    return $url;
  }
}

function me() {
  if (getenv("REQUEST_URI")) {
    $me = getenv("REQUEST_URI");
  } elseif (getenv("PATH_INFO")) {
    $me = getenv("PATH_INFO");
  } elseif ($GLOBALS["PHP_SELF"]) {
    $me = $GLOBALS["PHP_SELF"];
  }
  return strip_querystring($me);
}

function mep() {
  if (getenv("REQUEST_URI")) {
    $me = getenv("REQUEST_URI");
  } elseif (getenv("PATH_INFO")) {
    $me = getenv("PATH_INFO");
  } elseif ($GLOBALS["PHP_SELF"]) {
    $me = $GLOBALS["PHP_SELF"];
  }
  return $me;
}

function qualified_me() {
  $HTTPS = getenv("HTTPS");
  $SERVER_PROTOCOL = getenv("SERVER_PROTOCOL");
  $HTTP_HOST = getenv("HTTP_HOST");
  $protocol = (isset($HTTPS) && $HTTPS == "on") ? "https://" : "http://";
  $url_prefix = "$protocol$HTTP_HOST";
  return $url_prefix . me();
}

function AddZero($s)
{
  return str_pad($s, 2, '0', STR_PAD_LEFT);
}

function IIF($cond, $val1, $val2)
{
  if($cond)
    return $val1;
  else
    return $val2;
}

function Redirect($url) {
  if ($url != "") {
    header("Location: ".$url);
    exit;
  }
}


function RedirectFrontend($url) {
  if(!$GLOBALS['prevent_frontend_redirects'])
  {
    if ($url != "") {
      header("Location: ".$url);
      exit;
    }
  }else
  {
    exit;
  }
}


function IsEmpty($s)
{
  return !($s);
}

function IsGoodSymbols($s, $extrasym = '')
{
  return preg_match("/^[a-zA-Z0-9" . $extrasym . "_\-]*$/", $s, $regs);
}

function IsBadSymbols($s, $extrasym = '')
{
  return preg_match("/[^a-zA-Z0-9" . $extrasym . "_\-]+/", $s, $regs);
}

function IsLtGtSymbols($s, $extrasym = '')
{
  return preg_match("/[<>" . $extrasym . "]+/", $s, $regs);
}

function IsBadHost($s){
  return preg_match('/[^a-zA-Z0-9\.\-:]/', $s, $regs);
}

function IsBadRoot($s)
{
  return preg_match("/[^a-zA-Z0-9: _~.\/\-]/", $s, $regs);
}

function StringLimit($str, $len) {
  if (strlen($str) > $len)
    return (substr($str, 0, $len)."...");
  else
    return ($str);
}

function exec_cron($cron, $params = array()){

   if(!empty($params)){
     $query = Array();
     foreach($params as $key => $value){
       $query[] = $key."=".$value;
     }
     $query = implode(" ", $query);

   }else{
     $query = '';
   }

   $comand = "php ".$GLOBALS['cfgDirRoot']."crons/".$cron.".cron.php ".$query;

   exec_in_background($comand);

}

function exec_in_background($cmd) {

  if(substr(php_uname(), 0, 7) == "Windows"){
		pclose(popen("start /B ". $cmd, "r"));
  }else{
    exec($cmd. " > /dev/null &");
  }

}

function ExtractDomain($str) {
  // strip http:
  if (strtolower(substr($str, 0, 7)) == 'http://') $str = substr($str, 7);
  // get domain
  $pos = strpos($str, '/');
  if (!($pos === false)) $str = substr($str, 0, $pos);
  // results
  return $str;
}

function parseForJScript($newDiv,$singleQuotes = false){
  $newDiv = str_replace("\\", "\\\\", $newDiv);
  if(!$singleQuotes){
    $newDiv = str_replace('"', '\"', $newDiv);
    $newDiv = str_replace("</script>", "</scr\"+\"ipt>", $newDiv);
  }else{
    $newDiv = str_replace("'", "\\'", $newDiv);
    $newDiv = str_replace("</script>", "</scr'+'ipt>", $newDiv);
  }
  $newDiv = str_replace("\n", "\\n", $newDiv);
  $newDiv = str_replace("\r", "\\r", $newDiv);
  return $newDiv;
}


//function for sorting data in tree structure
function ListNodes($data, &$newdata, $parent, $level, $addtoid = "")
{
  if(!$level)
  {
    $newdata[] = "0:(root)";
    $level++;
  }
  foreach($data as $row)
  {
    if($parent == $row['parent'])
    {
        //$s = $row['page_id'] . ":" . str_repeat('&nbsp;&nbsp;', $level) . $row['title'] . " [" . $row['name'] . "]";
        $s = $addtoid . $row['page_id'] . ":" . str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;', $level) . $row['title'];
        if(strlen($s)>50+$level*20)
        {

          $s = $addtoid . $row['page_id'] . ":" . str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;', $level) . substr($row['title'], 0, 50+$level*20-strlen($s)) . "...";
        }
        $newdata[] = $s;
        ListNodes($data, $newdata, $row['page_id'], $level + 1, $addtoid);
    }
  }
}

//$data is 2-dimensional array, function returns string with list/combobox options
function ListBox($data, $value)
{
  $s = '';
  foreach($data as $row)
  {
    if($row[0]==$value)
      $s .= '<option value="' . $row[0] . '" selected="selected">' . $row[1] . '</option>';
    else
      $s .= '<option value="' . $row[0] . '">' . $row[1] . '</option>';
  }
  return $s;
}

function AccessDenied($C, $dontdie = false)
{
  echo '
  <script language="JavaScript">
  <!--
  alert("Access Denied!");
  ';
  if($C)
  {
    echo 'close();';
  }
  echo '
  <!---->
  </script>
  ';
  if(!$dontdie)
    Die();
}

function RedirectNoLicense()
{
  echo '
  <script language="JavaScript">
  <!--
  alert("License file for this site is invalid or expired!\nPlease renew your license file at http://www.serveris.lv/constructor_licenses/\n");
  ';
  echo '
  window.location.href = "'.$GLOBALS['cfgWebRoot'].'index.php";
  <!---->
  </script>
  ';
  Die();
}

function GetLanguageString($site_id, $ls, $component, $language)
{
  return sqlQueryValue('SELECT value FROM ' . $site_id . '_languagestrings WHERE name="'.$ls.'" AND component="'.$component.'" AND language='.intval($language).'');
}

function GetLanguageStringRow($site_id, $ls, $component, $language)
{
  return sqlQueryRow('SELECT * FROM ' . $site_id . '_languagestrings WHERE name="'.$ls.'" AND component="'.$component.'" AND language='.intval($language).'');
}

function GetPageString($site_id, $page_id, $stringname)
{
  $row = sqlQueryRow("SELECT string_id,stringvalue FROM " . $site_id . "_strings WHERE stringname='$stringname' AND page='$page_id'");
  $value     = $row['stringvalue'];
  $string_id = $row['string_id'];

  if(($value == null) && !$string_id)
  {
    if($page_id < 0)
      $copypage = $page_id;
    else
    {
      $copypage = sqlQueryValue("SELECT copypage FROM " . $site_id . "_pages WHERE page_id='$page_id'");
    }
  }
  else
  {
      return $value;
  }

  while(($value == null)and($copypage))
  {
    //view values are stored in stringsdev
    $value = sqlQueryValue("SELECT stringvalue FROM " . $site_id . "_stringsdev WHERE stringname='$stringname' AND pagedev='$copypage'");
    $copypage = sqlQueryValue("SELECT -parent FROM " . $site_id . "_views WHERE view_id=" . Abs($copypage));
  }

  //try to select template default language view
  if($value == null)
  {
    list($template, $language) = sqlQueryRow("SELECT template, language FROM " . $site_id . "_pages WHERE page_id='$page_id'");
    if($template && $language)
    {
      $lang_view = sqlQueryValue('SELECT view_id FROM ' . $site_id . '_views WHERE template_id = ' . intval($template) . ' AND language_id = ' . intval($language));
      if($lang_view)
        $value = sqlQueryValue("SELECT stringvalue FROM " . $site_id . "_stringsdev WHERE stringname='$stringname' AND pagedev='-$lang_view'");
    }
  }


  if($value == null)
  {
    $value = sqlQueryValue("SELECT stringvalue FROM " . $site_id . "_strings WHERE stringname='$stringname' AND page='-1'");
  }
  return $value;
}

function GetPageDevString($site_id, $pagedev_id, $stringname)
{
  $value = sqlQueryValue("SELECT stringvalue FROM " . $site_id . "_stringsdev WHERE stringname='$stringname' AND pagedev='$pagedev_id'");
  /*if(($value == null))
  {
    if($page_id < 0)
      $copypage = $page_id;
    else
    {
      $copypage = sqlQueryValue("SELECT copypage FROM " . $site_id . "_pages WHERE page_id='$page_id'");
    }
  }

  while(($value == null)and($copypage))
  {
    $value = sqlQueryValue("SELECT stringvalue FROM " . $site_id . "_strings WHERE stringname='$stringname' AND page='$copypage'");
    $copypage = sqlQueryValue("SELECT -parent FROM " . $site_id . "_views WHERE view_id=" . Abs($copypage));
  }*/

  if($value == null)
  {
    $value = sqlQueryValue("SELECT stringvalue FROM " . $site_id . "_strings WHERE stringname='$stringname' AND page='-1'");
  }
  return $value;
}

function LoadPageCache($site_id = false){

	$site_id = $site_id ? $site_id : $GLOBALS['site_id'];

	$GLOBALS['pages_cache'] = array();
	foreach(DB::GetTable("SELECT * FROM " . $site_id . "_pages ORDER BY ind") as $page){
  	$GLOBALS['pages_cache'][$page['page_id']] = $page;
	}
}

function getLangIdByPageId($page_id){

	$page = GetPageByID($page_id);
	return $page['language'];

}

function GetPageByID($page_id, $site_id = false){

	if(!$GLOBALS['pages_cache']){
  	LoadPageCache($site_id);
	}

  foreach($GLOBALS['pages_cache'] as $page){
  	if($page['page_id'] == $page_id) return $page;
  }

	return false;

}

function PagePathById($page_id, $site_id = false) {
  $path = '';
  $top = true;

  $site_id = $site_id ? $site_id : $GLOBALS['site_id'];

  while($page_id){

  	$page = GetPageByID($page_id, $site_id);
    $page_id = $page['page_id'];
    $parent = $page['parent'];
    $name = $page['name'];

    #list($page_id, $parent, $name) = sqlQueryRow("SELECT page_id, parent, name FROM " . $site_id . "_pages WHERE page_id=$page_id");
    $path = $name . '/' . $path;
    if(($top)and(strtolower($path) == 'index/'))
      $path = '';
    $page_id = $parent;
    $top = false;
  }
  return $path;
}

function PagePathRowsById($page_id, $site_id) {
  $path = array();
  $top = true;
  while($page_id)
  {
    $row = sqlQueryRow("SELECT * FROM " . $site_id . "_pages WHERE page_id=$page_id");
    $path[] = $row;

    $page_id = $row['parent'];
  }
  $path = array_reverse($path);
  $i = 0;
  foreach($path as &$page){
    $page['level'] = $i++;
  }
  return $path;
}

function BrowserLanguage(){

	global $HTTP_SERVER_VARS;

	if(isset($HTTP_SERVER_VARS["HTTP_ACCEPT_LANGUAGE"])) {
		if(strpos($HTTP_SERVER_VARS["HTTP_ACCEPT_LANGUAGE"],"-") === false){
			$browserlang = $HTTP_SERVER_VARS["HTTP_ACCEPT_LANGUAGE"];
		}else{
			list($browserlang) = explode ("-", $HTTP_SERVER_VARS["HTTP_ACCEPT_LANGUAGE"]);
		}
	}

	return $browserlang;

}

function PageIdByPath($path, $site_id) {
  //Obtain page_id
  $path = trim($path, '/');
  $path = explode("/", $path);

  if(!$path[0])
  {
    $path = Array();
    $tryindex = true;
  }
  $page_id = '0';
  for($f=0; (($f<count($path))and($page_id!=null)); $f++)
  {
    list($parent, $page_id) = sqlQueryRow("SELECT parent, page_id FROM " . $site_id . "_pages WHERE parent='" . $page_id . "' AND name='" . $path[$f] . "'");
  }

  if(($tryindex)and($page_id!=null))
  {
    list($iparent, $ipage_id) = sqlQueryRow("SELECT parent, page_id FROM " . $site_id . "_pages WHERE parent='" . $page_id . "' AND name='" . "index" . "'");
    if($ipage_id)
    {
      $parent = $iparent;
      $page_id = $ipage_id;
    }
  }

  return $page_id;
}

function WapPagePathById($page_id, $site_id) {
  $path = '';
  $top = true;
  while($page_id)
  {
    list($page_id, $parent, $name) = sqlQueryRow("SELECT wappage_id, parent, name FROM " . $site_id . "_wappages WHERE wappage_id=$page_id");
    $path = $name . '/' . $path;
    if(($top)and(strtolower($path) == 'index/'))
      $path = '';
    $page_id = $parent;
    $top = false;
  }
  return $path;
}

function FullEncoding($encoding)
{
  return '<meta http-equiv="Content-Type" content="text/html; charset='.$encoding.'">';
}

function PageEncoding($page_id, $site_id, $full=true)
{
  if($page_id)
  {
    $language = sqlQueryValue("SELECT language FROM " . $site_id . "_pages WHERE page_id=$page_id");
    if($language)
    {
      $encoding = sqlQueryValue("SELECT encoding FROM " . $site_id . "_languages WHERE language_id=" . $language);
      if($full)
        $encoding = '<meta http-equiv="Content-Type" content="text/html; charset='.$encoding.'">';
    }else
    {
      if($full)
        $encoding = '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">';
      else
        $encoding = 'UTF-8';
    }
  }else
  {
    if($full)
      $encoding = '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">';
    else
      $encoding = 'UTF-8';
  }
  return $encoding;
}

function PageDevEncoding($pagedev_id, $site_id, $full=true)
{
  if($pagedev_id)
  {
    $language = sqlQueryValue("SELECT language FROM " . $site_id . "_pagesdev WHERE pagedev_id=$pagedev_id");
    if($language)
    {
      $encoding = sqlQueryValue("SELECT encoding FROM " . $site_id . "_languages WHERE language_id=" . $language);
      if($full)
        $encoding = '<meta http-equiv="Content-Type" content="text/html; charset='.$encoding.'">';
    }else
    {
      if($full)
        $encoding = '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">';
      else
        $encoding = 'UTF-8';
    }
  }else
  {
    if($full)
      $encoding = '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">';
    else
      $encoding = 'UTF-8';
  }
  return $encoding;
}


  function send_mail($myname, $myemail, $contactname, $contactemail, $charset, $subject, $message, $content_type = "text/plain") {
    if($charset == 'utf-8' && function_exists('mb_encode_mimeheader'))
    {
      mb_internal_encoding('UTF-8');
      $subject = mb_encode_mimeheader($subject,"UTF-8", "B", "\n");
      $contactname = mb_encode_mimeheader($contactname, "UTF-8", "B", "\n");
      $myname = mb_encode_mimeheader($myname, "UTF-8", "B", "\n");
    }
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-type: $content_type; charset=".$charset."\r\n";
    $headers .= "To: ".$contactname." <".$contactemail.">\r\n";
    $headers .= "From: ".$myname." <".$myemail.">\r\n";
    $headers .= "X-Mailer: php\r\n";
    $r = (mail($contactemail, $subject, $message, $headers));
    add_log('Sent e-mail to ' . $contactemail . ' : ' . ($r ? 'success' : 'failed'), $GLOBALS['site_id']);
    return $r;
  }

  function send_mail_ex($sendername, $senderemail, $toname, $toemail, $mailsubj, $htmlmail, $mailtext = '', $attachments = Array(), $images = Array(), $charset = 'utf-8', $site_id = 0)
  {
    if(!$site_id)
      $site_id = $GLOBALS['site_id'];
    $dirroot = getFilePathFromLink($site_id, '');

    require_once($GLOBALS['cfgDirRoot']."library/"."class.mimemail.php");

    $mail = new htmlMimeMail5();
    $mail->setTextCharset($charset);
    $mail->setHTMLCharset($charset);
    $mail->setHeadCharset($charset);
    $mail->setFrom($sendername.' <'.$senderemail.'>');
    $mail->setSubject($mailsubj);

    if($toname)
      $rcpt = $toname . ' <' . $toemail . '>';
    else
      $rcpt = $toemail;

    if($images)
    {
      foreach($images as $image)
      {
        //remove leading slash
        $image = preg_replace('/^\//s', '', $image);
        $imgparts = explode('.', $image);
        $ext = $imgparts[count($imgparts) - 1];
        $image = $dirroot . $image;
        $mail->addEmbeddedImage(new fileEmbeddedImage($image, 'image/' . $ext, new Base64Encoding()));
      }
    }

    //outlook express has problems with QPrintable, so use something else
    $mail->setHTMLEncoding(new Base64Encoding());
    $mail->setHTML($htmlmail);

    $mail->setText($mailtext);

    //attach files
    if($attachments)
    {
      foreach($attachments as $attachment)
      {
        if($attachment)
        {
          //remove leading slash
          $attachment = preg_replace('/^\//s', '', $attachment);
          $attachment = $dirroot . $attachment;
          $mail->addAttachment(new fileAttachment($attachment));
        }
      }
    }

    $r = $mail->send(Array($rcpt));
    add_log('Sent e-mail to ' . $toemail . ' : ' . ($r ? 'success' : 'failed'), $site_id);
    return $r;
  }

  function phpMailerSend($msg, $subject, $myname, $myemail, $contacts, $html=false, $sendseparate=false, $attachments=null, $charset='utf-8')
  {
    require_once($GLOBALS['cfgDirRoot']."library/"."class.phpmailer.php");
    $mail = new PHPMailer();
    $mail->IsMail();
    $mail->From     = $myemail;
    $mail->FromName = $myname;

    $mail->Sender = $myemail;
  	$mail->AddReplyTo($myemail);

    if ($attachments) foreach ($attachments as $key => $path)
    {
      if (is_numeric($key))
        $mail->AddAttachment($path);
      else
        $mail->AddStringAttachment($path, $key);

    }
    $mail->WordWrap = 70;
    $mail->CharSet = $charset;
    if ($html)
      $mail->IsHTML(true);

    $mail->Subject  = $subject;
    $mail->Body     = $msg;
    if ($html)
      $mail->AltBody  =  strip_tags($msg);

    $emails = '';
    if (is_array($contacts))
    {
      $emails = implode(', ', $contacts);
      foreach ($contacts as $name => $email)
      {
        if (is_numeric($name))
          $mail->AddAddress($email);
        else
          $mail->AddAddress($email, $name);

        if ($sendseparate)
        {
          if (!$mail->Send())
          {
            $msg = 'phpMailerSend() failed: '.$email.' failed: '.$mail->ErrorInfo;
            add_log($msg, $GLOBALS['site_id']);
            return false;
          }
          $mail->ClearAddresses();
        }
      }
    }
    else
    {
      $mail->AddAddress($contacts);
      $emails = $contacts;
      $sendseparate = false;
    }


    if (!$sendseparate)
    {
      if (!$mail->Send())
      {
        $msg = 'phpMailerSend() failed: '.$emails.' failed: '.$mail->ErrorInfo;
        add_log($msg, $GLOBALS['site_id']);
        return false;
      }
    }
    return true;
  }

  function GetCollections($site_id, $type){

  	static $cache;

    if($GLOBALS['runningfrombackend']){

    	if($cache[$type]){
      	$data = $cache[$type];
    	}else{

      	foreach(DB::GetTable("SELECT collection_id, name, `type` FROM ".$site_id."_collections") as $row){
        	if(!is_array($cache[$row['type']])) $cache[$row['type']] = array();
          $cache[$row['type']][] = $row;
      	}

				$data = $cache[$type];
				
    	}

      #$data = sqlQueryData("SELECT collection_id, name FROM ".$site_id."_collections WHERE type='".$type."'");
      $r = Array("0:(new...)");
      if($data){
        foreach($data as $row){
          $r[] = $row['collection_id'] . ":" . $row['name'];
        }
      }
      return $r;
    }else{
      return Array("0:(unsupported!)");
    }

  }

  function SendFileHeaders($filename, $ext)
  {
    global $HTTP_USER_AGENT;

    $GLOBALS['now'] = gmdate('D, d M Y H:i:s') . ' GMT';
    header('Expires: ' . $GLOBALS['now']); // rfc2616 - Section 14.21
    header('Last-Modified: ' . $GLOBALS['now']);
    header('Cache-Control: no-store, no-cache, must-revalidate, pre-check=0, post-check=0, max-age=0'); // HTTP/1.1
    header('Pragma: no-cache'); // HTTP/1.0
    header('Content-Type: text/html');

    // 2. browser and version
    if (ereg('Opera(/| )([0-9].[0-9]{1,2})', $HTTP_USER_AGENT, $log_version)) {
        define('PMA_USR_BROWSER_AGENT', 'OPERA');
    } else if (ereg('MSIE ([0-9].[0-9]{1,2})', $HTTP_USER_AGENT, $log_version)) {
        define('PMA_USR_BROWSER_AGENT', 'IE');
    } else {
        define('PMA_USR_BROWSER_AGENT', 'OTHER');
    }

    // loic1: 'application/octet-stream' is the registered IANA type but
    //        MSIE and Opera seems to prefer 'application/octetstream'
    $mime_type = (PMA_USR_BROWSER_AGENT == 'IE' || PMA_USR_BROWSER_AGENT == 'OPERA')
                   ? 'application/octetstream'
                   : 'application/octet-stream';

    $now = gmdate('D, d M Y H:i:s') . ' GMT';



    // Send headers
    header('Content-Type: ' . $mime_type);
    header('Expires: ' . $now);
    // lem9 & loic1: IE need specific headers
    if (PMA_USR_BROWSER_AGENT == 'IE') {

        header('Content-Disposition: inline; filename="' . $filename . '.' . $ext . '"');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
    } else {
        header('Content-Disposition: attachment; filename="' . $filename . '.' . $ext . '"');
        header('Pragma: no-cache');
    }
  }

  function SaXoro($s, $key )
  {
    $n = 0;
    For ($f = 0;$f<strlen($s);$f++){
        if($s[$f]!=$key[$n]){
            $s[$f]= Chr(ord($s[$f]) ^ ord($key[$n]));
        }
        $n = $n + 1;
        If ($n >= StrLen($key)) {
             $n = 0;
        }
    }
    return $s;
  }

  function CheckLicense($domain)
  {
    //License checking disabled!!!
    $GLOBALS['licensereply'] = SaXoro('license: valid', $laiks);
    $GLOBALS['licensetext'] = 'unlimited licenses';
    return true;


    $laiks = strval(time());
    $GLOBALS['licenseparam'] = SaXoro($laiks,'ats');
    $GLOBALS['licensedomain'] = $domain;
    if(file_exists($GLOBALS['cfgDirRoot'] . 'licenses/' . $domain . '.license.php'))
    {
      include($GLOBALS['cfgDirRoot'] . 'licenses/' . $domain . '.license.php');
    }else
    {
      $GLOBALS['licensetext'] = '<a href="http://www.serveris.lv/constructor_licenses/"><font color="red">license missing</font></a>';
      return false;
    }
    $r = Saxoro($GLOBALS['licensereply'],$laiks);
    if($r == 'license: valid')
    {
      $GLOBALS['licensetext'] = 'license valid';
      return true;
    }else if($r == 'license: expires')
    {
      $GLOBALS['licensetext'] = '<a href="http://www.serveris.lv/constructor_licenses/"><font color="red">renew license</font></a>';
      return true;
    }else if($r == 'license: expired')
    {
      $GLOBALS['licensetext'] = '<a href="http://www.serveris.lv/constructor_licenses/"><font color="red">license expired</font></a>';
      return false;
    }else
    {
      $GLOBALS['licensetext'] = '<font color="red">license invalid</font>';
      return false;
    }
  }

  function getBody($name, $site_id, $wap=0)
  {
    if ($wap==0)
        $body = sqlQueryValue("SELECT body FROM ".$site_id."_templates WHERE name='".$name."'");
    else
        $body = sqlQueryValue("SELECT body FROM ".$site_id."_waptemplates WHERE name='".$name."'");
    return $body;
  }

  function attachSubTemplates($data, $site_id, $wap=0){

  	while (preg_match('/\{%template:([a-zA-Z0-9 _]+)%\}/U', $data, $regs)){
    	$data = str_replace($regs[0], getBody($regs[1],$site_id), $data);
    }

		return $data;

	}

    function getBodyNew($name)
    {

        $path = Constructor\WebApp::$app->getConfigValue("template_folder").$name.'.php';
        if(file_exists($path)){
            ob_start();
            require $path;
            return ob_get_clean();
        }else{
            die("Template `".$name."` not found");
        }

    }

    function attachSubTemplates_new($data){

        while (preg_match('/\{%template:([a-zA-Z0-9 _]+)%\}/U', $data, $regs)){
            $data = str_replace($regs[0], getBodyNew($regs[1],$site_id), $data);
        }

        return $data;

    }

  function GetPageStylesheet($site_id, $page_id)
  {
    $pageData = sqlQueryRow("SELECT * FROM " . $site_id . "_pages WHERE page_id='$page_id'");
    $template_id = sqlQueryValue("SELECT template_id FROM " . $site_id . "_templates WHERE template_id='" . $pageData['template'] . "'");
    list($body, $copybody) = sqlQueryRow("SELECT body, copybody FROM " . $site_id . "_templates WHERE template_id=$template_id");
    while($copybody)
      list($body, $copybody) = sqlQueryRow("SELECT body, copybody FROM " . $site_id . "_templates WHERE template_id=$copybody");

  $body = attachSubTemplates($body, $site_id);

    if(preg_match("/<link [^<>]*rel[ ]*=[ ]*[\\'\\\"]?stylesheet[\\'\\\"]?[^<>]*>/i", $body, $regs))
    {
      $stylesheet = $regs[0];
      if(preg_match("/[ ]*href[ ]*=[ ]*[\\'\\\"]?([^ \\'\\\"]*)[\\'\\\"]?/i", $stylesheet, $regs))
      {
        $href = $regs[1];
        return $href;
      }
    }

    return "";
  }

  function DeleteCompiledTemplate($site_id, $page_id)
  {
    sqlQuery("DELETE FROM " . $site_id . "_ctemplates WHERE page_id=" . $page_id);
    sqlQuery("DELETE FROM " . $site_id . "_componentcache WHERE page_id=" . $page_id);
  }

  function DeleteCompiledTemplates($site_id, $template_id)
  {
    sqlQuery("DELETE FROM " . $site_id . "_ctemplates WHERE template_id=" . $template_id);
    DeleteComponentCache($site_id);
  }

  function DeleteAllCompiledTemplates($site_id)
  {
    sqlQuery("TRUNCATE TABLE " . $site_id . "_ctemplates");
    DeleteComponentCache($site_id);
  }

  function DeleteAllCompiledWapTemplates($site_id)
  {
    sqlQuery("TRUNCATE TABLE " . $site_id . "_wapctemplates");
    DeleteComponentCache($site_id);
  }

  function DeleteComponentCache($site_id)
  {
    sqlQuery("TRUNCATE TABLE " . $site_id . "_componentcache");
  }

  function CreateCompiledTemplate($site_id, $page_id)
  {

  }

function to_lv($str) {
  $lvchars = array(
    "ā",
    "č",
    "ē",
    "ģ",
    "ī",
    "ķ",
    "ļ",
    "ņ",
    "ō",
    "š",
    "ū",
    "ž",
    "Ā",
    "Č",
    "Ē",
    "Ģ",
    "Ī",
    "Ķ",
    "Ļ",
    "Ņ",
    "Ō",
    "Š",
    "Ū",
    "Ž"
    );
  $lventities = array(
    "&#257;",
    "&#269;",
    "&#275;",
    "&#291;",
    "&#299;",
    "&#311;",
    "&#316;",
    "&#326;",
    "&#333;",
    "&#353;",
    "&#363;",
    "&#382;",
    "&#256;",
    "&#268;",
    "&#274;",
    "&#290;",
    "&#298;",
    "&#310;",
    "&#315;",
    "&#325;",
    "&#332;",
    "&#352;",
    "&#362;",
    "&#381;"
    );
  foreach(array_keys($lvchars) as $key) {
    $str = str_replace ($lvchars[$key], $lventities[$key], $str);
  }
  return $str;
}

//function for publishing wap page contents
function autopublish($site_id, $wappagedev_id)
{

    //current dev page data
    $row = sqlQueryRow("select * from ".$site_id."_wappagesdev where wappagedev_id='".$wappagedev_id."'");

    //update normal page using dev page data
    sqlQuery("UPDATE " . $site_id . "_wappages SET name='".$row['name']."', title='".$row['title']."', description='".$row['description']."', template='".$row['template']."', lastmod='".$row['lastmod']."', modby='".$row['modby']."', enabled='".$row['enabled']."', visible='".$row['visible']."', redirect='".$row['redirect']."', copypage='".$row['copypage']."', is_card='".$row['is_card']."', is_root='".$row['is_root']."' WHERE wappage_id='".$row['wappage_id']."'");

    //delete old
    sqlQuery("DELETE FROM ".$site_id."_wapcontents WHERE wappage=".$row['wappage_id']);

    //copy contents from wapcontentsdev to wapcontents
    if ($row['template']==0)
    {
        //get page contents if no template
        $row_contents = sqlQueryRow("select * from ".$site_id."_wapcontentsdev where wappagedev=".$_GET['wappagedev_id']." AND componentname='stdcontents'");

        if (sqlQueryValue("select * from ".$site_id."_wapcontents where wappage=".$row['wappage_id']))
        {
            sqlQuery("update ".$site_id."_wapcontents SET propertyvalue='".addslashes($row_contents['propertyvalue'])."',propertyname='".$row_contents['propertyname']."',componentname='".$row_contents['componentname']."' where wappage=".$row['wappage_id']);
        }
        else
        {
            sqlQuery("insert into ".$site_id."_wapcontents (wappage,componentname,propertyname,propertyvalue) values (".$row['wappage_id'].",'".$row_contents['componentname']."','".$row_contents['propertyname']."','".addslashes($row_contents['propertyvalue'])."')");
        }
    }
    else
    {
        sqlQuery("INSERT INTO ".$site_id."_wapcontents (wappage,componentname,propertyname,propertyvalue) SELECT ".$row['wappage_id']." as wappage,componentname,propertyname,propertyvalue FROM ".$site_id."_wapcontentsdev WHERE wappagedev=".$wappagedev_id);
        /*
        $data_contents = sqlQueryData("select * from ".$site_id."_wapcontentsdev where wappagedev=".$_GET['wappagedev_id']);
        foreach ($data_contents as $row_contents)
        {
            if (sqlQueryValue("select * from ".$site_id."_wapcontents where wappage=".$row['wappage_id']." AND componentname='".$row_contents['componentname']."' AND propertyname='".$row_contents['propertyname']."'"))
            {
                sqlQuery("update ".$site_id."_wapcontents SET propertyvalue='".addslashes($row_contents['propertyvalue'])."' WHERE wappage=".$row['wappage_id']." AND componentname='".$row_contents['componentname']."' AND propertyname='".$row_contents['propertyname']."'");
            }
            else
            {
                sqlQuery("insert into ".$site_id."_wapcontents (wappage,componentname,propertyname,propertyvalue) values (".$row['wappage_id'].",'".$row_contents['componentname']."','".$row_contents['propertyname']."','".addslashes($row_contents['propertyvalue'])."')");
            }
        }
        */
     }

     DeleteAllCompiledWapTemplates($site_id);
}

  //function for autopublishing page contents
  function pageautopublish($site_id, $pagedev_id, $skipIndexing = false)
  {
        //current dev page data
        $row = sqlQueryRow("select * from ".$site_id."_pagesdev where pagedev_id='".$pagedev_id."'");

        //update normal page using dev page data
        sqlQuery("UPDATE " . $site_id . "_pages SET
            name='".$row['name']."',
            title='".addslashes($row['title'])."',
            description='".addslashes($row['description'])."',
            template='".$row['template']."',
            language='".$row['language']."',
            lastmod='".$row['lastmod']."',
            modby='".$row['modby']."',
            enabled='".$row['enabled']."',
            visible='".$row['visible']."',
            passprotect='".$row['passprotect']."',
            ind='".$row['ind']."',
            parent='".$row['parent']."',
            redirect='".$row['redirect']."',
            copypage='".$row['copypage']."',
            redirect_url='".$row['redirect_url']."',
            in_trash='".$row['in_trash']."'
            WHERE page_id='".$row['page_id']."'");

        //delete old contents of the original page
        sqlQuery("DELETE FROM ".$site_id."_contents WHERE page=".$row['page_id']);
        //delete old strings of the original page
        sqlQuery("DELETE FROM ".$site_id."_strings WHERE page=".$row['page_id']);

        //insert new contents for the original page from dev page
        sqlQuery("INSERT INTO ".$site_id."_contents (page,componentname,propertyname,propertyvalue) SELECT ".$row['page_id']." as page, componentname, propertyname, propertyvalue FROM ".$site_id."_contentsdev WHERE pagedev=".$pagedev_id);
        //insert new strings for the original page from dev page
        sqlQuery("INSERT INTO ".$site_id."_strings (page,stringname,stringvalue) SELECT ".$row['page_id']." as page, stringname, stringvalue FROM ".$site_id."_stringsdev WHERE pagedev=".$pagedev_id);

        if ($GLOBALS['StorePageHistory']) PageHistory($site_id, $row['page_id'], $row['title']);

        if(!$skipIndexing)
        {
          IndexPageForSearch($site_id, $row['page_id']);
        }
        DeleteAllCompiledTemplates($site_id);
  }

  function PageHistory($site_id, $page_id, $title)
  {
      if($page_id <= 0)
        return false;
      ob_start();
      $GLOBALS['forced_site_id'] = $site_id;
      $GLOBALS['forced_page_id'] = $page_id;
      $GLOBALS['dontneedauth'] = true;
      include($GLOBALS['cfgDirRoot'] . "index.php");
      $html = ob_get_contents();
      ob_end_clean();

      sqlQuery("INSERT INTO ".$site_id."_pageshistory (page_id,title,date,html) VALUES ($page_id,'".addslashes($title)."','".time()."','".addslashes($html)."')");
  }

  function LS($var)
  {
    if ($GLOBALS['CMSlanguage'] && isset($GLOBALS['Languages'][$GLOBALS['CMSlanguage']][$var]))
        return $GLOBALS['Languages'][$GLOBALS['CMSlanguage']][$var];
    else return $GLOBALS['Languages'][defaultLanguage][$var];
  }

  function IncludeStartSlash($s)
  {
    if(substr($s, 0, 1) == '/')
      return $s;
    else
      return '/' . $s;
  }

  function transliterate($str)
  {
    $pairs = array(
    'Ā' => 'A',
    'Č' => 'C',
    'Ē' => 'E',
    'Ģ' => 'G',
    'Ī' => 'I',
    'Ķ' => 'K',
    'Ļ' => 'L',
    'Ņ' => 'N',
    'Š' => 'S',
    'Ū' => 'U',
    'Ž' => 'Z',
    'Ō' => 'O',
    'Ŗ' => 'r',

    'ā' => 'a',
    'č' => 'c',
    'ē' => 'e',
    'ģ' => 'g',
    'ī' => 'i',
    'ķ' => 'k',
    'ļ' => 'l',
    'ņ' => 'n',
    'š' => 's',
    'ū' => 'u',
    'ž' => 'z',
    'ō' => 'o',
    'ŗ' => 'r',

    'А' => 'A',
    'Б' => 'B',
    'В' => 'V',
    'Г' => 'G',
    'Д' => 'D',
    'Е' => 'Je',
    'Ё' => 'Jo',
    'Ж' => 'Zh',
    'З' => 'Z',
    'И' => 'I',
    'Й' => 'Ij',
    'К' => 'K',
    'Л' => 'L',
    'М' => 'M',
    'Н' => 'N',
    'О' => 'O',
    'П' => 'P',
    'Р' => 'R',
    'С' => 'S',
    'Т' => 'T',
    'У' => 'U',
    'Ф' => 'F',
    'Х' => 'H',
    'Ц' => 'C',
    'Ч' => 'Ch',
    'Ш' => 'Sh',
    'Щ' => 'Shch',
    'Ь' => '',
    'Ы' => 'I',
    'Ъ' => '',
    'Э' => 'E',
    'Ю' => 'Ju',
    'Я' => 'Ja',

    'а' => 'a',
    'б' => 'b',
    'в' => 'v',
    'г' => 'g',
    'д' => 'd',
    'е' => 'je',
    'ё' => 'jo',
    'ж' => 'zh',
    'з' => 'z',
    'и' => 'i',
    'й' => 'ij',
    'к' => 'k',
    'л' => 'l',
    'м' => 'm',
    'н' => 'n',
    'о' => 'o',
    'п' => 'p',
    'р' => 'r',
    'с' => 's',
    'т' => 't',
    'у' => 'u',
    'ф' => 'f',
    'х' => 'h',
    'ц' => 'c',
    'ч' => 'ch',
    'ш' => 'sh',
    'щ' => 'shch',
    'ь' => '',
    'ы' => 'i',
    'ъ' => '',
    'э' => 'e',
    'ю' => 'ju',
    'я' => 'ja'
    );

    return strtr($str, $pairs);
  }

  function transliterateText($name, $allowedchars='0123456789abcdefghijklmnopqrstuvwxyz'){

    $name = strtolower(transliterate($name));
    $last_sep = false;
    $out = '';
    for ($i=0; $i<strlen($name); $i++){
      if (strpos($allowedchars, $name[$i]) === false){
        if (!$last_sep){
          $out .= '_';
          $last_sep = true;
        }
      }else{
        $last_sep = false;
        $out .= $name[$i];
      }

    }
    return trim($out, '-');

  }

  function transliterateURL($name, $allowedchars='0123456789abcdefghijklmnopqrstuvwxyz')
  {
    $name = strtolower(transliterate($name));
    $last_sep = false;
    $out = '';
    for ($i=0; $i<strlen($name); $i++)
    {
      if (strpos($allowedchars, $name[$i]) === false)
      {
        if (!$last_sep)
        {
          $out .= '-';
          $last_sep = true;
        }
      }
      else
      {
        $last_sep = false;
        $out .= $name[$i];
      }

    }
    return trim($out, '-');
  }

function str_replace_once($needle , $replace , $haystack){
    // Looks for the first occurence of $needle in $haystack
    // and replaces it with $replace.
    $pos = strpos($haystack, $needle);
    if ($pos === false) {
        // Nothing found
      return $haystack;
    }
    return substr_replace($haystack, $replace, $pos, strlen($needle));
}

function copySiteTables($source_id, $dest_id)
{
  $tables = Array();
  $data = sqlQueryData("SHOW TABLES LIKE '".$source_id."_%'");
  foreach ($data as $row)
  {
    $src = $row[0];
    $a = explode('_', $src);
    $a[0] = $dest_id;
    $dest = implode('_', $a);

    sqlQuery("DROP TABLE IF EXISTS ".$dest);
    sqlQuery("CREATE TABLE ".$dest." LIKE ".$src."");
    sqlQuery("INSERT INTO ".$dest." SELECT * FROM ".$src);
    $tables[] = $dest;
  }

/*
  Papildus kveriji saita kopēšanai:

insert into lists select 0 as list_id,
	(24+ind) as ind,	visible,	page_id,	title,	componentname,	componenttype,	5 as site_id,	collection_id,	show_in_menu
 from lists where site_id=4

insert into permissions select 0 as perm_id, perm_num, isuser, id, 5 as data, data2, extradata from permissions where data = 4

*/
  return $tables;
}

if (!function_exists('json_encode'))
{
    require_once($GLOBALS['cfgDirRoot'] . 'library/class.json.php');

    function json_encode($data) {
        $json = new Services_JSON(SERVICES_JSON_LOOSE_TYPE);
        return( $json->encode($data) );
    }
}

if( !function_exists('json_decode') ) {
    require_once($GLOBALS['cfgDirRoot'] . 'library/class.json.php');

    function json_decode($data) {
        $json = new Services_JSON(SERVICES_JSON_LOOSE_TYPE);
        return( $json->decode($data) );
    }
}

function addReferer($ref)
{
  global $site_id, $page_path;
  static $engines = array(
    'Google' => array(
      'host_pattern' => '!google(\\.[a-z]+)+$!',
      'path_pattern' => array('!^/url!', '!^/search!'),
      'query_key' => 'q',
    ),
    'Yahoo!' => array(
      'host_pattern' => '!([a-z]+\\.)*search.yahoo.com!',
      'path_pattern' => '!^/search!',
      'query_key' => 'p',
    ),
    'Live search' => array(
      'host_pattern' => '!search\\.(live|msn)\\.com!',
      'path_pattern' => '!^/results.aspx!',
      'query_key' => 'q',
    ),
    'Yandex' => array(
      'host_pattern' => '!([a-z]+\\.)*yandex.ru!',
      'path_pattern' => '!^/yandsearch!',
      'query_key' => 'text',
    ),
    'Ask' => array(
      'host_pattern' => '!([a-z]+\\.)*ask.com!',
      'path_pattern' => '!^/web!',
      'query_key' => 'q',
    ),
    'Alexa' => array(
      'host_pattern' => '!([a-z]+\\.)*alexa.com!',
      'path_pattern' => '!^/search!',
      'query_key' => 'q',
    ),

  );

  $ref_url = parse_url($ref);

  $a = explode('&', $ref_url['query']);
  $query = Array();
  foreach ($a as $q)
  {
    $v = explode('=', $q);
    $query[urldecode($v[0])] = urldecode($v[1]);
  }

  foreach ($engines as $name => $e)
  {
    if (empty($query[$e['query_key']])) continue;

    $paths = is_array($e['path_pattern']) ? $e['path_pattern'] : array($e['path_pattern']);

    foreach ($paths as $path_pattern)
    {
      if (preg_match($e['host_pattern'], $ref_url['host']) && preg_match($path_pattern, $ref_url['path']) )
      {
        sqlQuery("INSERT INTO `".$site_id."_stats_keywords` SET
          referer='".addslashes($ref)."',
          engine='".addslashes($name)."',
          keywords='".addslashes($query[$e['query_key']])."',
          `time`=".time().",
          page_url='".addslashes($page_path)."'
          ");
        return;
      }

    }
  }
}

function getFirstCharFromString($string, $charset = "qwertyuioplkjhgfdsazxcvbnm"){

  $char = false;

  for($i = 0; $i != strlen($string); $i++){
    if(strpos($charset, $string[$i])){
      $char = $string[$i];
      break;
    }
  }

  return $char;

}

function generate_random_string($len, $charset = "0123456789qwertyuioplkjhgfdsazxcvbnm")
{
  if (!$len) return '';
  $s = '';
  $chlen = strlen($charset);
  while ($len--)
  {
    $s .= $charset[mt_rand(0, $chlen-1)];
  }
  return $s;
}

if ( !function_exists('sys_get_temp_dir') )
{
    // Based on http://www.phpit.net/
    // article/creating-zip-tar-archives-dynamically-php/2/
    function sys_get_temp_dir()
    {
        // Try to get from environment variable
        if ( !empty($_ENV['TMP']) )
        {
            return realpath( $_ENV['TMP'] );
        }
        else if ( !empty($_ENV['TMPDIR']) )
        {
            return realpath( $_ENV['TMPDIR'] );
        }
        else if ( !empty($_ENV['TEMP']) )
        {
            return realpath( $_ENV['TEMP'] );
        }

        // Detect by creating a temporary file
        else
        {
            // Try to use system's temporary directory
            // as random name shouldn't exist
            $temp_file = tempnam( md5(uniqid(rand(), TRUE)), '' );
            if ( $temp_file )
            {
                $temp_dir = realpath( dirname($temp_file) );
                unlink( $temp_file );
                return $temp_dir;
            }
            else
            {
                return FALSE;
            }
        }
    }
}

if(!function_exists('http_build_query'))
{
    function http_build_query( $formdata, $numeric_prefix = null, $key = null ) {
        $res = array();
        foreach ((array)$formdata as $k=>$v) {
            $tmp_key = urlencode(is_int($k) ? $numeric_prefix.$k : $k);
            if ($key) {
                $tmp_key = $key.'['.$tmp_key.']';
            }
            if ( is_array($v) || is_object($v) ) {
                $res[] = http_build_query($v, null, $tmp_key);
            } else {
                $res[] = $tmp_key."=".urlencode($v);
            }
        }
        return implode("&", $res);
    }
}

if (!function_exists('array_combine'))
{
  function array_combine($arr1, $arr2)
  {
      $out = array();

      $arr1 = array_values($arr1);
      $arr2 = array_values($arr2);

      foreach($arr1 as $key1 => $value1) {
          $out[(string)$value1] = $arr2[$key1];
      }

      return $out;
  }
}

function getCurrentLanguage(){

  static $lang;

  if($lang) return $lang;

  $site_id = $GLOBALS['site_id'];

  if(!isset($_SESSION['currentlanguagenum'])){
  	$_SESSION['currentlanguagenum'] = DB::GetValue("SELECT language_id FROM ".$site_id."_languages WHERE is_default = 1");
  }

  $lang = sqlQueryRow('SELECT * FROM '.$site_id.'_languages WHERE language_id = '.$_SESSION['currentlanguagenum']);

  return $lang;

}

function makeMethodParameters($class, $method, $params = [])
{

    // ordering params in correct order
    $ref = new ReflectionMethod($class, $method);
    $result_params = [];
    foreach ($ref->getParameters() as $param) {
        $found = false;
        foreach($params as $k => $p){
            if($k == $param->name){
                $result_params[] = $p;
                $found = true;
                break;
            }
        }
        if(!$found){

            try{
                $default = $param->getDefaultValue();
            }catch(Exception $e){
                $default = null;
            }

            $result_params[] = $default;
            
        }
    }

    return $result_params;

    }

function getLanguages($site_id=0){

	static $cache = array();

  $site_id = $site_id ? $site_id : $GLOBALS['site_id'];

  $ckey = $site_id;
  if ($cache[$ckey]) return $cache[$ckey];

  $langs = array();
  $data = sqlQueryData('SELECT * FROM '.$site_id.'_languages ORDER BY language_id');
  foreach ($data as $row){
    $langs[$row['shortname']] = $row;
  }

  $cache[$ckey] = $langs;
  return $langs;

}


function dump($var)
{
  ob_start();
  var_dump($var);
  $s = ob_get_contents();
  ob_end_clean();

  echo '<pre>'.htmlspecialchars($s).'</pre>';
}

function formatedDebugBacktrace()
{
  $bt = debug_backtrace();
?>
  <style type="text/css">
  ._dbg {
    border: 1px solid black;
    margin: 6px;
    border-collapse: collapse;
    font-family: Verdana, Tahoma, Arial, sans;
    font-size: 14px;
    background: white;
  }

  ._dbg th {
    text-align: left;
    background: orange;
  }

  ._dbg td, ._dbg th {
    border: 1px solid black;
    padding-left: 4px;
    padding-right: 4px;
  }

  ._dbg .function {
    font-weight: bold;
  }

  </style>
  <table class="_dbg">
    <tr>
      <th>Location</th>
      <th>Function</th>
    </tr>
    <? foreach ($bt as $b) { ?>
    <tr>
      <td><?=substr($b['file'], -40).':'.$b['line'] ?></td>
      <td class="function"><?=$b['class'].$b['type'].$b['function'] ?></td>
    </tr>
    <? } ?>
  </table>
<?
}

function strip_only_tags($str, $tags, $stripContent=false)
{
  $content = '';
  if(!is_array($tags)) {
      $tags = (strpos($str, '>') !== false ? explode('>', str_replace('<', '', $tags)) : array($tags));
      if(end($tags) == '') array_pop($tags);
  }
  foreach($tags as $tag) {
      if ($stripContent)
           $content = '(.+</'.$tag.'(>|\s[^>]*>)|)';
       $str = preg_replace('#</?'.$tag.'(>|\s[^>]*>)'.$content.'#is', '', $str);
  }
  return $str;
}

function hsc($str){
	return htmlspecialchars($str, ENT_QUOTES);
}


function lcms($string=false){
	if(!$string) return;

    static $cache = array();

    $key = transliterateText($string);
    
    if(isset($cache[$key])) return $cache[$key];

    $text_row = DB::GetRow("
      SELECT l.*, li.*
      FROM phrasenames AS l
      LEFT JOIN phrases AS li ON l.phrasename_id = li.phrasename      
      WHERE
        li.lang = '". CMS_LANG ."' AND
        l.name=:key
      ", array("key" => $key));

    // new text, lets add
    if(!$text_row){
			if (!$site_id)
				$site_id = $GLOBALS['site_id'];
			if (!$site_id)
				$site_id = 4;
			$langs = getLanguages($site_id);
			$id = DB::Insert("phrasenames", array("name" => $key));
			foreach($langs as $l){
				sqlQuery("INSERT INTO `phrases`(lang, phrase, phrasename) VALUES('".$l['fullname']."', :string, '".$id."')", array(":string" => $string));
      }
			//default value
			sqlQuery("INSERT INTO `phrases`(lang, phrase, phrasename) VALUES('', :string, '".$id."')", array(":string" => $string));
			$value = $string;
    }else{
      $value = $text_row['phrase'];
    } 


    return $value;

  }

	function is_win(){
		return strtoupper(substr(PHP_OS, 0, 3)) === 'WIN' ? true : false;
	}

	function clear_cache($cache_name, $folder = false){

		$folder = $folder ? $folder."/" : "";

		$file = $GLOBALS['cache_folder'].$folder.$cache_name.".inc";

		unlink($file);

	}

  function getYoutubeVideoThumb($url, $folder = '/images/youtube/', $site_id = false){

  	$site_id = $site_id ? $site_id : $GLOBALS['site_id'];

    $dir = "/";

    $parts = explode("=", $url);

    if(count($parts) != 2) return false;

    $key = $parts[1];

    $web_path = $folder.$key.".jpg";

    $where_to_put = getFilePathFromLink($site_id, $web_path);

    if(!file_exists($where_to_put)){
      file_put_contents($where_to_put, file_get_contents("http://img.youtube.com/vi/".$key."/0.jpg"));
    }

    return $web_path;

  }

  function get_file_extension($file_name) {
		return substr(strrchr($file_name,'.'),1);
	}

    function file_force_contents($dir, $contents, $append = false)
    {

        $parts = explode("/", $dir);
        $file = array_pop($parts);
        $dir = '';
        foreach($parts as $part){

            if($dir == "" && is_win()){
                if(!is_dir($dir .= $part)) mkdir($dir);
            }else{
                if(!is_dir($dir .= "/".$part)) mkdir($dir);
            }

        }

        $append = $append ? FILE_APPEND : 0;
        return file_put_contents($dir."/".$file, $contents, $append);

    }
