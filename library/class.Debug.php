<?php

class Debug{

    public static function outputBacktrace()
    {

        debug_print_backtrace();

    }

    public static function dump($var)
    {

        echo "<pre>".print_r($var, true)."</pre>";

    }

}