<?
// Func.Apache.PHP
// Functions for editing Apache configuration file

//Notes: Before adding virtual hosts, we have to add a virtual host
//       for our primary host, the Constructor 2.0 installation should do it.

//Do not use ereg characters in this constant
DEFINE ("CommentLineForVirtualHost", "#Virtual host added by Constructor 2");

function EscapeEregChars($S)
{
  $S = ereg_replace("\[\^\.\[\$\(\)\|\*\+\?\{\\\]", "\\\\\\0", $S);
  return $S;
}

function EscapePregChars ($foo)
{
  return preg_replace("/([^A-z0-9_ -]|[\\\[\]])/", "\\\\\\1", $foo);
}


function VirtualHostString($ServerName, $DocumentRoot)
{
  global $cfgDirRoot;
  $s = "\n" . CommentLineForVirtualHost . "\n";
  $s .= "<VirtualHost " . $_SERVER['SERVER_ADDR'] . ">\n";
  $s .= "ServerName " . $ServerName . "\n";
  $s .= "DocumentRoot " . $DocumentRoot . "\n";
  $s .= "RewriteEngine  on\n";
  $s .= 'AliasMatch    ^/edit.php  "' . $cfgDirRoot . 'backend/index.php"' . "\n";
  $s .= "RewriteCond    %{REQUEST_URI}  !(^/images)\n";
  $s .= "RewriteCond    %{REQUEST_URI}  !(^/files)\n";
  $s .= "RewriteCond    %{REQUEST_URI}  !(^/upload)\n";
  $s .= "RewriteRule    ^/[a-zA-Z0-9/\-]*$ /valesco.virtual [PT,L]\n";
  $s .= "Alias /valesco.virtual \"" . $cfgDirRoot . "index.php\"\n";
  $s .= "#Insert your lines here\n";
  $s .= "</VirtualHost>\n";
  return $s;
}


function AddVirtualHost($ServerName, $DocumentRoot)
{
  //get path to file from config.php
  global $ApacheConfig;
  
  $fd = fopen($ApacheConfig, "a");
  if($fd)
  {
    fputs($fd, VirtualHostString($ServerName, $DocumentRoot));
    fclose($fd);

    return true;
  }else
  {
    return false;
  }
}

function RemoveVirtualHost($ServerName)
{
  global $ApacheConfig;
  
  if((file_exists($ApacheConfig))and($ApacheConfig))
  {
    $s = file($ApacheConfig);
    $s = join("", $s);
    if(preg_match('/' . EscapePregChars(CommentLineForVirtualHost) . '\\n\\<VirtualHost [0-9\\.]{8,16}\\>\\nServerName ' . EscapePregChars($ServerName) . '\\n.*?\\<\\/VirtualHost\\>\\n/sm', $s, $regs))
    {
      $s = str_replace($regs[0], "", $s);
      //Rewriting a configuration file, better not to lose electricity now
      //Let's make abackup copy
      copy($ApacheConfig, $ApacheConfig . ".valesco.bak");
      $fd = fopen($ApacheConfig, "w");
      if($fd){
        fputs($fd, $s);
        fclose($fd);
        return true;
      }else
      {
        return false;
      }
    }else
    {
      return false;
    }
  }else
  {
    return false;
  }
}


?>
