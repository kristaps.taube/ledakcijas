<?php

use Constructor\WebApp;

class ProductFilter
{

    public static function filterByLink($products, $link_id)
    {

        if(!$products) return [];

        $active_filters = self::getActiveLinkFiltersGrouped($link_id);
        if(!$active_filters){
            return $products;
        }

        return self::filter($products, $active_filters);

    }

    public static function filterByCategory($products, $category)
    {

        if(!$products) return [];

        $active_filters = self::getActiveCategoryFiltersGrouped($category);
        if(!$active_filters){
            return $products;
        }

       return self::filter($products, $active_filters);

    }

    private static function filter($products, $active_filters)
    {

        $product_ids = [];
        foreach($products as $product){
            $product_ids[] = $product['item_id'];
        }

        $filter_col = EVeikalsFilters::getInstance();
        $prod_filter_col = EVeikalsProdFilterRel::getInstance();

        $table = DB::GetTable("
            SELECT pf.product_id, pf.filter_id
            FROM ".$prod_filter_col->table." AS pf
            WHERE product_id IN(".implode(",", $product_ids).")
        ");

        $product_filters = [];
        foreach($table as $row){
            if(!isset($product_filters[$row['product_id']])) $product_filters[$row['product_id']] = [];
            $product_filters[$row['product_id']][] = $row['filter_id'];
        }

        # filter within same group are being OR-ed, so atleast one filter should match from group to keep product
        # filter groups are being AND-ed, so all active filter groups should.. ..match?
        foreach($products as $ind => $product){

            $cur_product_filters = isset($product_filters[$product['item_id']]) ? $product_filters[$product['item_id']] : [];

            $keep_me = true;
            foreach($active_filters as $group => $filters){  // for each filter group

                $group_ok = false;

                foreach($filters as $filter){ // for each active filter in group

                    if(in_array($filter, $cur_product_filters)){
                        $group_ok = true;
                        break;
                    }

                }

                if(!$group_ok){ // this groups fails.. so product fails..
                    $keep_me = false;
                    break;
                }

            }

            if(!$keep_me){
                unset($products[$ind]);
            }

        }

        return $products;

    }

    public static function addLinkFilters($link_id, $filters)
    {

        $session_filters = WebApp::$app->session->get('link_filters');

        if(!isset($session_filters[$link_id])) $session_filters[$link_id] = [];

        foreach($filters as $filter){

            if(!in_array($filter, $session_filters[$link_id])){
                $session_filters[$link_id][] = $filter;
            }

        }

        WebApp::$app->session->set('link_filters', $session_filters);

    }

    public static function getLinkRemovedFilters($link_id)
    {
        $removed_filters = WebApp::$app->session->get('removed_filters');
        return isset($removed_filters[$link_id]) ? $removed_filters[$link_id] : [];
    }

    public static function setCategoryFilters($category_id, $filters)
    {

        $session_filters = WebApp::$app->session->get('filters');
        $session_filters[$category_id] = $filters;
        WebApp::$app->session->set('filters', $session_filters);

    }

    public function setLinkFilters($link_id, $filters)
    {

        $session_filters = WebApp::$app->session->get('link_filters');
        $removed_filters = WebApp::$app->session->get('removed_filters');

        if(!isset($session_filters[$link_id])) $session_filters[$link_id] = [];

        $category_removed_filters = self::getLinkRemovedFilters($link_id);
        foreach($session_filters[$link_id] as $filter){ // for each current filter
            if(!in_array($filter, $filters) && !in_array($filter, $category_removed_filters)){
                $category_removed_filters[] = $filter;
            }
        }

        $removed_filters[$link_id] = $category_removed_filters;
        WebApp::$app->session->set('removed_filters', $removed_filters);

        $session_filters[$link_id] = $filters;
        WebApp::$app->session->set('link_filters', $session_filters);

    }

    public static function clearCategoryFilters($category_id)
    {

        $session_filters = WebApp::$app->session->get('filters');
        if(isset($session_filters[$category_id])){
            $session_filters[$category_id] = NULL;
            unset($session_filters[$category_id]);
        }
        WebApp::$app->session->set('filters', $session_filters);

    }

    public static function clearLinkFilters($link_id)
    {

        $filters = WebApp::$app->session->get('link_filters');
        if(isset($filters[$link_id])){
            $filters[$link_id] = NULL;
            unset($filters[$link_id]);
        }
        WebApp::$app->session->set('link_filters', $filters);

    }

    public static function getActiveLinkFilters($link_id)
    {
        $filters = WebApp::$app->session->get('link_filters');
        return isset($filters[$link_id]) ? $filters[$link_id] : [];
    }

    public static function getActiveCategoryFilters($category_id)
    {

        $session_filters = WebApp::$app->session->get('filters');
        return isset($session_filters[$category_id]) ? $session_filters[$category_id] : [];

    }

    public static function getActiveLinkFiltersGrouped($link_id)
    {

        $filters = self::getActiveLinkFilters($link_id);
        if(!$filters) return [];

        return self::groupFilters($filters);

    }

    public static function getActiveCategoryFiltersGrouped($category_id)
    {

        $filters = self::getActiveCategoryFilters($category_id);
        if(!$filters) return [];

        return self::groupFilters($filters);

    }

    private static function groupFilters($filters)
    {

        if(!$filters) return [];

        $filters_col = EVeikalsFilters::getInstance();
        $table = DB::GetTable("SELECT * FROM `".$filters_col->table."` WHERE item_id IN (".implode(",", $filters).")");

        $data = [];
        foreach($table as $row){
            if(!isset($data[$row['group']])){
                $data[$row['group']] = [];
            }
            $data[$row['group']][] = $row['item_id'];
        }

        return $data;

    }

}