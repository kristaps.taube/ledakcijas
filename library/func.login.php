<?

// Login and session handling related library
// Aivars Irmejs

function valid_login($username, $password, $ip, $ignoreenabledrow=false){

  if ($_SESSION['admin_logged_in']){
    $row = sqlQueryRow('SELECT * FROM users WHERE username="Admin"');
    $_SESSION['userdata'] = $row;
    $GLOBALS["currentUserID"] = $row['user_id'];
    $GLOBALS["currentUserSiteID"] = $row['site_id'];
    $GLOBALS['loginfailmessage'] = '';
    return true;
  }

  if($username){

  	$user = DB::GetRow("SELECT * FROM users WHERE username = :username", array(":username" => $username));

    if(!$user){
    	$GLOBALS['loginfailmessage'] = 'Invalid username or password';
      return false;
    }

		if(!valid_password($password, $user['password'])){
    	$GLOBALS['loginfailmessage'] = 'Incorrect username or password';
      return false;
		}

		if($user['addresses'] && $ip){

     	$IPs = explode("\n", $user['addresses']);
      $IIPs = Array();
      foreach($IPs as $key => $IPadd){
        $IIPs[] = trim($IPadd);
      }
      $IPs = $IIPs;

      if(!in_array($ip, $IPs)){
        $GLOBALS['loginfailmessage'] = 'Logging in from this IP is not allowed for user ' . $username;
        return false;
      }

		}

		$_SESSION['userdata'] = $user;
    $GLOBALS["currentUserID"] = $user['user_id'];
    $GLOBALS["currentUserSiteID"] = $user['site_id'];

    if(($user["enabled"])or($ignoreenabledrow)){
      $GLOBALS['loginfailmessage'] = '';
      return true;
    }else{
      $GLOBALS['loginfailmessage'] = 'User disabled';
      return false;
    }
  }else{
    $GLOBALS['loginfailmessage'] = 'No user specified';
    return false;
  }

}

function encode_password($password){

	$salt = generate_random_string(5);

	return $salt.hash("sha256", $salt.hash("sha256", $password));

}

// pasw is being encoded like this: $password = $salt.sha256($salt.sha256($password));
// sha256($password) part comes from user browser, as it's encoded before sent over
function valid_password($received_pasw, $pasw){

 	$salt = substr($pasw, 0, 5);
  $hash = substr($pasw, 5);

  return (hash("sha256", $salt.$received_pasw) == $hash) ? true : false;

}

function logged_in(){

  if($_GET['forcelogin']){
    return valid_login($_GET["user"], $_GET["pass"], $_SERVER['REMOTE_ADDR']);
  }else{
    return valid_login($_SESSION["session_user"], $_SESSION["session_pass"], $_SERVER['REMOTE_ADDR']);
  }

}


function log_in($username, $password, $ip){

  $minutes = 5;
  $attempts = 5;

  // 5 unsuccessful login attempts and last one was less than 5 min ago
  if($_SESSION['unsuccessful_login_attempts'] >= $attempts && time() - $_SESSION['last_unsuccessful_login_attempt'] < $minutes * 60){
    $GLOBALS['loginfailmessage'] = 'Login form is disabled for '.$minutes.' minutes after '.$attempts.' unsuccessful login attempts';
    return false;
  }

  if(valid_login($username, $password, $ip)){
    $_SESSION['unsuccessful_login_attempts'] = 0;
    login_successful($username, $password);
    return true;
  }else{
    login_failed($username);
    // if we had 5 unsuccessful login attempts we should start counting from 0
    $_SESSION['unsuccessful_login_attempts'] = ($_SESSION['unsuccessful_login_attempts'] >= $attempts) ? 0 : $_SESSION['unsuccessful_login_attempts'];
    $_SESSION['unsuccessful_login_attempts']++;
    $_SESSION['last_unsuccessful_login_attempt'] = time();
    return false;
  }

}

function login_successful($username, $password, $msg='')
{

	$row = DB::GetRow('SELECT * FROM users WHERE username=:username', array(":username" => $username));
  $GLOBALS["currentUserID"] = $row['user_id'];

  session_start();
  $_SESSION['session_user'] = $username;
  $_SESSION['session_pass'] = $password;
  add_log('Logged in: ' . $username.($msg ? '( '.$msg.' )' : ''), 0);
}

function login_failed($username, $msg='')
{
  session_start();
  $_SESSION["session_user"] = '';
  $_SESSION["session_pass"] = '';
  add_log('Login failed: ' . $username.($msg ? '( '.$msg.' )' : ''), 0);
}

function log_out()
{
  session_destroy();
}


?>
