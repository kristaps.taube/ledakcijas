<?

// CLASSES

class rightmenu {

  var $menuHtml;
  var $backendroot;
  var $count;
  var $items;


  
  function rightmenu() {
    //$this->menuHtml = 'var m1=new Array;'."\n".'m1[0]=new Array("");'."\n";
    $this->menuHtml = '';
    $this->count = 0;
    $this->items = Array();
  }

  function addItem ($name,$action,$link=false,$blank=false,$icon='')
  {
    $this->count++;
    $this->items[] = Array('name' => $name, 'action' => $action, 'link' => $link, 'blank' => $blank, 'icon' => $icon);
/*    if ($link)
    {
      $this->menuHtml .= 'm1['.$this->count.']=new Array("'.$name.'", "", "goto(\''.$action.'\','.($blank ? 1 : 0).')","0","'.$icon.'");'."\n";
    }
    else
    {
      $this->menuHtml .= 'm1['.$this->count.']=new Array("'.$name.'","","'.$action.'","0","'.$icon.'");'."\n";
    } */
  }

  function addSpacer ()
  {
    $this->count++;
//    $this->menuHtml .= 'm1['.$this->count.']=new Array(spc);'."\n";
    $this->items[] = '-';
  }

  function output()
  {
/*    return
      '<script language="javascript">

      '.$this->menuHtml.'

      var m2=new Array;
      m2[0]=new Array("");

      var mvect=new Array(1,2);


      createMenus();

      document.onmouseup=function()
      {
        hideMenu(event);
        return false;
      }

    </script>';
  } */

    $s = '
    <script language="javascript">
    ';
    if($this->functionname)
    {
      $s .= 'function ' . $this->functionname . '() {
      ';
    }
    $s .= '
    window.context_menu = new Ext.menu.Menu({
      items: [
    ';
    $first = true;
    foreach($this->items as $item)
    {
      if(!$first)
        $s .= ',';
      else
        $first = false;

      if($item == '-')
      {
        $s .= '"-"';
      }
      else
      {
        if($item['link'])
          $handler = 'goto(\''.$item['action'].'\','.($blank ? 1 : 0).')';
        else
          $handler = $item['action'];

        $s .= '{
          text: "'.$item['name'].'",
          handler: function()
          {
            ' . $handler . '
          }';
        if($item['icon'])
        {
          $s .= ',
          icon: "'.$GLOBALS['cfgWebRoot'] . 'gui/rightMenu/' . $item['icon'].'.gif"';
        }
        $s .= '
        }';
      }
    }
    $s .= '
      ]
    });
    ';
    if($this->functionname)
    {
      $s .= '}';
    }
    $s .= '


    function mouseX(evt) {
    if (evt.pageX) return evt.pageX;
    else if (evt.clientX)
        return evt.clientX + (document.documentElement.scrollLeft ?
        document.documentElement.scrollLeft :
        document.body.scrollLeft);
    else return null;
    }
    function mouseY(evt) {
    if (evt.pageY) return evt.pageY;
    else if (evt.clientY)
        return evt.clientY + (document.documentElement.scrollTop ?
        document.documentElement.scrollTop :
        document.body.scrollTop);
    else return null;
    }


    function goto(url){
        myWindow = window.open("", "", "")
        myWindow.location=url;
        return false;
    }

    function showMenu(el, event)
    {
      if(event.getXY)
        window.context_menu.showAt(event.getXY());
      else
        window.context_menu.showAt([mouseX(event), mouseY(event)]);
    }
    </script>
    ';

    return $s;

  }


}

?>
