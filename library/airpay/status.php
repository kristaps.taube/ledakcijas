<?
 date_default_timezone_set('Europe/Riga');

 ##
 ## status receiving. this is where AirPay pings You in background.
 ##


 /**
 * merchant constants
 */

 $merchant_id		= 0;			// merchant ID provided by AirPay
 $merchant_secret	= "";			// merchant secret code provided by merchant


 # include AirPay class
 include("airpay.class.php");

 # create AirPay object
 $airpay = new airpay( $merchant_id, $merchant_secret );


 # process the response to make sure we can trust the data
 if (!$ret = $airpay->response($_POST, 'status')) {
	// error
	// update Your logs for the investigation
 } else {
	# $ret array description
	#
	# Common data submitted by merchant:
	# [version] 			- version of the AirPay used
	# [merchant_id] 		- merchant ID
	# [mc_transaction_id] 		- merchant invoice value
	# [transaction_id]		- transaction ID in AirPay system
	# [amount]			- amount in minor units
	# [currency]			- currency code
	# [description] 		- description
	# [optional1]			- optional fields
	#
	# Payment data
	# [payment_system] 		- alias of the payment system that was used
	# [status_id]			- status of the transaction
	# [payment_system_status]	- original status of the transaction provided by the payment system above
	#
	# Sign
	# [hash] 			- hash sign
	#
	# Additional data
	# [is_reference_of] 		- presents if the transaction is a reference of any other transaction. used when refund happened
	#
	# Possible payment statuses
	# -1	- transaction expired with no try for the payment, means that the client just closed the AirPay window without proceeding the payment
	#  0	- transaction is created by payment() or payment_req() methods of the AirPay class, means that the AirPay window opened
	#  1	- transaction is successfull, means that the merchant can provide the service or product to the client
	#  2	- transaction is pending, means that the client have tried to pay and the payment system is still processing the payment
	#  3	- transaction is rejected, means that the client have no funds or cancelled the payment etc
	#  4	- transaction is expired, means that it didn't receive any status after the pending for a long time (36 hours). consider it as rejected.
	#  5	- transaction is refunded, means that the client received the funds back and Your account was charged for this amount

	// update Your local database according to the received data, send e-mail etc

	# Always check the following for successfull transactions:
	# the transaction was never successfull before (except it is refunded now)
	# the amount and currency is exactly what You expect to receive according to Your local database
 }
?>