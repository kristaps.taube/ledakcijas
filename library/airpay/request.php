<?
 date_default_timezone_set('Europe/Riga');

 ##
 ## request data for the transaction
 ##

 /**
 * merchant constants
 */

 $merchant_id		= 0;			// merchant ID provided by AirPay
 $merchant_secret	= "";			// merchant secret code provided by merchant


 # include AirPay class
 include("airpay.class.php");

 # create AirPay object
 $airpay = new airpay( $merchant_id, $merchant_secret );


 # transaction_id in the AirPay system
 # @return associative array with data of the transaction or (bool)FALSE on error
 $ret = $airpay->request( $transaction_id );


 # print the result
 if ($ret) {
	print "<pre>";
	print_r($ret);
	print "</pre>";
 } else	
	print "error";


 # $ret array description
 #
 # Common data submitted by merchant:
 # [version] 			- version of the AirPay used
 # [merchant_id] 		- merchant ID
 # [mc_transaction_id] 		- merchant invoice value
 # [transaction_id]		- transaction ID in AirPay system
 # [amount]			- amount in minor units
 # [currency]			- currency code
 # [description] 		- description
 # [optional1]			- optional fields
 #
 # Payment data
 # [payment_system] 		- alias of the payment system that was used
 # [status_id]			- status of the transaction
 # [payment_system_status]	- original status of the transaction provided by the payment system above
 #
 # Sign
 # [hash] 			- hash sign
 #
 # Additional data
 # [is_reference_of] 		- presents if the transaction is a reference of any other transaction. used when refund happened
 # [has_referers] 		- presents if the transaction has referers
 # [ref] 			- associative arrays for the above case
 #	[transaction_id] 	- transaction ID of the referrer
 #	[amount]		- amount in minor units
 #	[currency]		- currency code
 #	[payment_system]	- alias of the payment system that was used
 #	[status_id]		- status of the transaction
 #	[payment_system_status]	- original status of the transaction provided by the payment system above
 #
 # Possible payment statuses
 # -1	- transaction expired with no try for the payment, means that the client just closed the AirPay window without proceeding the payment
 #  0	- transaction is created by payment() or payment_req() methods of the AirPay class, means that the AirPay window opened
 #  1	- transaction is successfull, means that the merchant can provide the service or product to the client
 #  2	- transaction is pending, means that the client have tried to pay and the payment system is still processing the payment
 #  3	- transaction is rejected, means that the client have no funds or cancelled the payment etc
 #  4	- transaction is expired, means that it didn't receive any status after the pending for a long time (36 hours). consider it as rejected.
 #  5	- transaction is refunded, means that the client received the funds back and Your account was charged for this amount
?>