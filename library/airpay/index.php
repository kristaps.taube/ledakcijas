<?
 date_default_timezone_set('Europe/Riga');

 ##
 ## initiate payment
 ##


 /**
 * merchant constants
 */

 $merchant_id		= 0;			// merchant ID provided by AirPay
 $merchant_secret	= "";			// merchant secret code provided by merchant


 # include AirPay class
 include("airpay.class.php");

 # create AirPay object
 $airpay = new airpay( $merchant_id, $merchant_secret );


 # @return associative array of all available payment systems
 # it's recommended to cache this result instead of requesting the AirPay server for each transaction
 print "<pre>";
 print_r($airpay->psystems());
 print "</pre>";


 /**
 * array for payment data
 */

 $invoice = array(
		'amount'	=> 1,					// minor units, e.g. 1 for 0.01
		'currency'	=> 'LVL',				// currency code in ISO 4217
		'invoice'	=> 'HA'.date("Y/m/d").'-'.time(),	// unique transaction value
		'language'	=> 'LAT',				// language: LAT, RUS, ENG
		'cl_fname'	=> 'Peter',				// client's first name
		'cl_lname'	=> 'Smith',				// client's last name
		'cl_email'	=> 'peter.smith@some.domain.com',	// client's e-mail address
		'cl_country'	=> 'LV',				// country code in ISO 3166-1-alpha-2
		'cl_city'	=> 'Riga',				// city name
		'description'	=> 'Real transaction',			// description of the transaction, visible to the client, e.g. description of the product
		'psys'		=> 'paypal', 				// payment system alias. empty for default or taken from $airpay->psystems
 );


 # starting the payment
 # draws HTML form
 $airpay->payment($invoice);

 # OPTIONAL
 # requesting AirPay for transaction initiation to redirect the client manually
 # @return associative array
 # [url]		- full URL for the redirection
 # [status] 		- status: either "OK" or error
 # [transaction_id] 	- transaction ID in the AirPay system to refer to
 # [hash] 		- hash value
 // $ret = $airpay->payment_req($invoice);
 // You may want to save the $ret['transaction_id'] in Your local database
 // header("Location: ".$ret['url']);

 exit();
?>