<?
 date_default_timezone_set('Europe/Riga');

 ##
 ## returning client. the payment process is finished by the client and we get him back here
 ##


 /**
 * merchant constants
 */

 $merchant_id		= 0;			// merchant ID provided by AirPay
 $merchant_secret	= "";			// merchant secret code provided by merchant


 # include AirPay class
 include("airpay.class.php");

 # create AirPay object
 $airpay = new airpay( $merchant_id, $merchant_secret );


 # process the response to make sure we can trust the data
 if (!$ret = $airpay->response($_GET, 'return')) 
	print "error";
 else {
	## it is not recommended to update local database on this step
	## all the information here is just for the redirection of the client
	## status:
	## 1 = SUCCESS
	## 2 = CANCELLED

	if ($ret['status'] == 1) {
		// where SUCCESS does not means that the payment is successfull
		// You just have to tell the client that the payment is processing and there are still no errors

		## the redirection to Your local page is supposed to be here
		// header ("Location: http://www.merchant-website.com/thank_you.html");
	}

	if ($ret['status'] == 2) {
		// status CANCELLED means that the client clicked "cancel" button somewhere

		## the redirection to Your local page is supposed to be here
		// header ("Location: http://www.merchant-website.com/cancelled.html");
	}
 }

?>