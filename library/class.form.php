<?

class Form
{
  var $name;
  var $fields;
  var $texts;
  var $errors;
  var $fielderrorhtml = '<span class="err">%s</span>';
  var $trim_values = true;

  function Form($fields, $texts, $name='f')
  {
    $this->fields = $fields;
    $this->texts = $texts;
    $this->name = $name;

    $this->initFields($this->fields);
  }

  function initFields(&$fields)
  {
    foreach ($fields as $name => $fld)
    {
      if (!isset($fields[$name]['name'])) $fields[$name]['name'] = $name;
      if ($fld['type'] == 'security') $fields[$name]['req'] = 1;
    }

  }

  function addFields($fields)
  {
    $this->initFields($fields);
    $this->fields = array_merge($this->fields, $fields);
  }

  // static
  function getLanguageStrings()
  {
    return Array(
      '_nofields' => 'Nav aizpildīti vajadzīgie lauki!',
      '_noemail' => 'Nepareiza e-pasta adrese!',
      '_none' => '(neviens)',
      '_nosecurity' => 'Ievadīts nepareizs drošības kods!',
      '_field' => 'Lauks'
    );
  }

  function getValues()
  {
    $a = Array();
    foreach ($this->fields as $key => $fld)
    {
      $a[$key] = $fld['value'];
    }
    return $a;
  }

  function getValueFormatters()
  {
    return array();
  }

  function getFormatedValues($html=false)
  {
    $formatters = $this->getValueFormatters();

    $a = Array();
    foreach ($this->fields as $name => $fld)
    {
      $fld = $this->fields[$name];
      if (!$fld['type'] || $fld['hidden'] || $fld['noformat']) continue;

      $val = $fld['value'];
      $sval = htmlspecialchars($val);

      $type = isset($formatters[$fld['type']]) ? $formatters[$fld['type']] : $fld['type'];
      if ($type == 'separator')
      {
        $a[$name] = array('field' => &$this->fields[$name], 'value' => '---');
      }

      switch ($type)
      {
        case 'list':
          if (is_array($val))
          {
            $vals = array();
            foreach ($val as $v)
            {
              $skey = $name.'_'.$v;
              $vals[] = isset($this->texts[$skey]) ? $this->texts[$skey] : $fld['items'][$v];
            }
            $val = implode('; ', $vals);
          }
          else
          {
            $skey = $name.'_'.$val;
            $val = isset($this->texts[$skey]) ? $this->texts[$skey] : $fld['items'][$val];
          }
          break;
        case 'radiolist':
          $skey = $name.'_'.$val;
          $val = isset($this->texts[$skey]) ? $this->texts[$skey] : $fld['items'][$val];
          break;
        case 'checklist':
          $items = array();
          foreach ($fld['items'] as $key => $v)
          {
            if (!$v) continue;
            $items[] = '['.(isset($this->texts[$name.'_'.$key]) ? $this->texts[$name.'_'.$key] : $key).']';
          }

          if (!$items) continue 2;
          $val = $items ? implode('; ', $items) : $this->texts['_none'];
          break;
        case 'email':
          if ($val && $html)
            $val = '<a href="mailto:'.$sval.'">'.$sval.'</a>';
          else
            $val = $sval;

          break;
        case 'textarea':
          $val = $html ? nl2br($sval) : $sval;
          break;
        case 'text':
          $val = $html ? $sval : $val;
          break;
        case 'checkbox':
          if ($fld['checked']) $val = '[x]';
          break;
        case 'label':
          $val = " ";
          break;
        default: continue 2;
      }

      if (!strlen($val)) continue;

      $label = $this->texts[$name];
      if (!empty($fld['label']))
      {
        $label = $this->texts[$fld['label']];
        if (empty($label))
          $label = $fld['label'];

      }

      if ($fld['type'] == 'label')
        $label = "\n".$label;

      $a[$name] = array('field' => &$this->fields[$name], 'label' => $label, 'value' => $val);
    }
    return $a;
  }

  function validate()
  {
    $email_pattern = '/^([a-z0-9])(([-a-z0-9._])*([a-z0-9]))*\@([a-z0-9])' . '(([a-z0-9-])*([a-z0-9]))+' . '(\.([a-z0-9])([-a-z0-9_-])?([a-z0-9])+)+$/i';

    $is_valid = true;
    $this->errors = array();
    $reqfieldsmsg = false;
    foreach ($this->fields as $name => $field)
    {
      $fld = &$this->fields[$name];
      if ($fld['hidden']) continue;

      if ($fld['type'] == 'checklist' && $fld['req'])
      {
        $value = 0;
        foreach ($fld['items'] as $val)
        {
          $value |= $val;
        }

        if (!$value)
        {
          $is_valid = false;
          $this->addError('nofields');
          $reqfieldsmsg = true;
          $fld['error'] = true;
        }
      }
      else if ($fld['req'] && empty($fld['value']))
      {
        $subvalid = false;
        if ($fld['subfields'])
        {
          $sv = 0; $svtotal = 0;
          foreach ($fld['subfields'] as $sub)
          {
            if (!isset($this->fields[$sub])) continue;
            if (!empty($this->fields[$sub]['value'])) $sv++;
            $svtotal++;
          }
          $subvalid = ($sv == $svtotal);
        }

        if (!$subvalid)
        {
          $is_valid = false;
          $fld['error'] = true;

          if (!$reqfieldsmsg)
          {
            $this->addError('nofields');
            $reqfieldsmsg = true;
          }
        }
      }
      else if ($fld['type'] == 'email' && !empty($fld['value']) && !preg_match($email_pattern, $fld['value']) )
      {
        $is_valid = false;
        $fld['error'] = true;
        $this->addError('noemail', $this->texts['_field'].' "'.$this->texts[$fld['name']].'"');
      }
      else if ($fld['type'] == 'security' && $fld['value'] != $_SESSION[$this->name.'_'.$fld['name'].'_security_code'])
      {
        $fld['value'] = '';
        $is_valid = false;
        $fld['error'] = true;
        $this->addError('nosecurity', '"'.$this->texts[$fld['name']].'"');
      }

    }

    return $is_valid;
  }

  function stripslashes_array(&$src)
  {
    $a = Array();
    foreach ($src as $key => $val)
    {
      $a[$key] = is_array($val) ? $this->stripslashes_array($val) : stripslashes($val);
    }

    return $a;
  }

  function readPost($stripslashes=false)
  {
    if (!$stripslashes && !get_magic_quotes_gpc())
      return $this->readValues($_POST);

    $a = $this->stripslashes_array($_POST);

    return $this->readValues($a);
  }

  function readGet($stripslashes=false)
  {
    if (!$stripslashes && !get_magic_quotes_gpc())
      return $this->readValues($_GET);

    $a = $this->stripslashes_array($_GET);

    return $this->readValues($a);
  }

  function readValues(&$values)
  {
    $this->values_read = true;

    foreach ($values as $key => $val)
    {
      if (!isset($this->fields[$key]) || $this->fields[$key]['hidden']) continue;
      $this->fields[$key]['value'] = $this->trim_values ? trim($val) : $val;
    }

    foreach ($this->fields as $name => $fld)
    {
      $fld = &$this->fields[$name];
      if ($fld['type'] == 'checklist')
      {
//        var_dump($fld['items']);
        $i = 0;
        foreach ($fld['items'] as $key => $val)
        {
          $fld['items'][$key] = (int)isset($values[$fld['name'].'_'.$i]);
          $i++;
        }
      }
      else if ($fld['type'] == 'checkbox')
      {
        $fld['checked'] = (boolean)$values[$name];
      }
      unset($fld);
    }
  }

  function getAttribs(&$fld, $attribs)
  {
    $html = '';
    foreach ($attribs as $name)
    {
      if (!isset($fld[$name])) continue;
      $html .= ' '.$name.'="'.htmlspecialchars($fld[$name]).'"';
    }
    return $html;
  }


  function fieldText(&$fld)
  {
    return array(
      'fld' => $fld,
      'attribs' => $this->getAttribs($fld, Array('id','class','style','maxlength','title'))
    );
  }
  function tplText($data)
  {
    extract($data);
    return '<input type="text" name="'.$fld['name'].'" value="'.htmlspecialchars($fld['value']).'"'.$attribs.'/>';
  }

  function fieldEmail(&$fld)
  {
    return $this->fieldText($fld);
  }
  function tplEmail($data)
  {
    return $this->tplText($data);
  }

  function fieldPassword(&$fld)
  {
    return array(
      'fld' => $fld,
      'attribs' => $this->getAttribs($fld, Array('id','class','style','maxlength','title'))
    );
  }
  function tplPassword($data)
  {
    extract($data);
    return '<input type="password" name="'.$fld['name'].'" value="'.htmlspecialchars($fld['value']).'"'.$attribs.'/>';
  }

  function fieldFile(&$fld)
  {
    return array(
      'fld' => $fld,
      'attribs' => $this->getAttribs($fld, Array('id','class','style','title'))
    );
  }
  function tplFile($data)
  {
    extract($data);
    return '<input type="file" name="'.$fld['name'].'"'.$attribs.'/>';
  }

  function fieldTextarea(&$fld)
  {
    return array(
      'fld' => $fld,
      'attribs' => $this->getAttribs($fld, Array('id','class','style','title')),
      'cols' => $fld['cols'] ? $fld['cols'] : 30,
      'rows' => $fld['rows'] ? $fld['rows'] : 5
    );
  }
  function tplTextarea($data)
  {
    extract($data);
    return '<textarea name="'.$fld['name'].'" cols="'.$cols.'" rows="'.$rows.'"'.$attribs.'>'.htmlspecialchars($fld['value']).'</textarea>';
  }

  function fieldList(&$fld)
  {
    $data = array(
      'fld' => $fld,
      'attribs' => $this->getAttribs($fld, Array('id','class','style','title','size','multiple')),
      'items' => array()
    );

    $lblprefix = isset($fld['label']) ? $fld['label'] : $fld['name'];
    foreach ($fld['items'] as $key => $item)
    {
      $label_key = $lblprefix.'_'.$item;

      $data['items'][$key] = array(
        'selected' => is_array($fld['value']) ? in_array($key, $fld['value']) : ($fld['value'] == $key),
        'label' => isset($this->texts[$label_key]) ? $this->texts[$label_key] : $item
      );

    }
    return $data;
  }
  function tplList($data)
  {
    extract($data);
    $html = '';
    foreach ($items as $key => $item)
    {
      $html .= '<option value="'.$key.'"'.($item['selected'] ? ' selected="selected"' : '').'>'.$item['label'].'</option>'."\n";
    }
    return '<select name="'.$fld['name'].'"'.$attribs.'>'.$html.'</select>';
  }

  function fieldRadiolist(&$fld)
  {
    $attribs = $this->getAttribs($fld, Array('id','class','style','title','onclick','onchange'));
    $items = '';
    $separator = isset($fld['separator']) ? $fld['separator'] : '<br/>';
    $lblprefix = isset($fld['label']) ? $fld['label'] : $fld['name'];

    $data = array(
      'fld' => $fld,
      'attribs' => $attribs,
      'separator' => $separator,
      'items' => array()
    );
    foreach ($fld['items'] as $key => $item)
    {
      $label_key = $lblprefix.'_'.$item;
      $label = isset($this->texts[$label_key]) ? $this->texts[$label_key] : $item;
      $id = $this->name.'_'.$fld['name'].'_'.$key;

      $data['items'][] = array(
        'id' => $id,
        'label' => $label,
        'name' => $fld['name'],
        'value' => $key,
        'checked' => ($fld['value'] == $key)
      );
    }
    return $data; //$this->template(isset($fld['tpl']) ? $fld['tpl'] : 'radiolist', $data);
  }
  function tplRadiolist($data)
  {
    extract($data);
    $itemshtml = '';
    foreach ($items as $item)
    {
      $itemshtml .= '<label><input id="'.$item['id'].'" type="radio" name="'.$item['name'].'" value="'.$item['value'].'"'.($item['value'] == $fld['value'] ? ' checked="checked"' : '').'/> '.$item['label'].'</label>'.$separator;
    }
    return '<div id="'.$this->name.'_'.$fld['name'].'"'.$attribs.'>'.$itemshtml.'</div>';
  }

  function fieldChecklist(&$fld)
  {
    $attribs = $this->getAttribs($fld, Array('id','class','style','title'));
    $items = '';
    $data = array(
      'fld' => $fld,
      'attribs' => 'attribs',
      'items' => array()
    );
    $i = 0;
    foreach ($fld['items'] as $item => $val)
    {
      $label_key = $fld['name'].'_'.$item;
      $label = isset($this->texts[$label_key]) ? $this->texts[$label_key] : $item;
      $id = $this->name.'_'.$fld['name'].'_'.$i;

      $data['items'][] = array(
        'id' => $id,
        'label' => $label,
        'name' => $fld['name'].'_'.$i, //$item,
        'checked' => (boolean)$val,
      );
      $i++;
    }
    return $data; // $this->template(isset($fld['tpl']) ? $fld['tpl'] : 'checklist', $data);
  }
  function tplChecklist($data)
  {
    extract($data);
    $itemshtml = '';
    foreach ($items as $item)
    {
      $itemshtml .= '<label><input id="'.$item['id'].'" type="checkbox" name="'.$item['name'].'"'.($item['checked'] ? ' checked="checked"' : '').'/> '.$item['label'].'</label><br/>';
    }
    return '<div id="'.$this->name.'_'.$fld['name'].'"'.$attribs.'>'.$itemshtml.'</div>';
  }


  function fieldSubmit(&$fld)
  {
    return array(
      'fld' => $fld,
      'attribs' => $this->getAttribs($fld, Array('id','class','style','title'))
    );
  }
  function tplSubmit($data)
  {
    extract($data);
    return '<input type="submit" name="'.$fld['name'].'" value="'.htmlspecialchars($this->texts[$fld['name']]).'"'.$attribs.'/>';
  }

  function fieldLabel(&$fld)
  {
    return array(
      'fld' => $fld,
      'attribs' => $this->getAttribs($fld, Array('id','class','style','title'))
    );
  }
  function tplLabel($data)
  {
    extract($data);
    return '<span'.$attribs.'>'.$fld['value'].'</span>';
  }

  function fieldCheckbox(&$fld)
  {
    return array(
      'fld' => $fld,
      'attribs' => $this->getAttribs($fld, Array('id','class','style','title','value'))
    );
  }
  function tplCheckbox($data)
  {
    extract($data);
    return '<input type="checkbox" name="'.$fld['name'].'"'.($fld['checked'] ? ' checked="checked"' : '').$attribs.'/>';
  }


  function fieldHidden(&$fld)
  {
    return array(
      'fld' => $fld,
      'attribs' => $this->getAttribs($fld, Array('id','class'))
    );
  }
  function tplHidden($data)
  {
    extract($data);
    return '<input type="hidden" name="'.$fld['name'].'" value="'.htmlspecialchars($fld['value']).'"'.$attribs.'/>';
  }


  function fieldHtml(&$fld)
  {
    return array(
      'fld' => $fld,
    );
  }
  function tplHtml($data)
  {
    extract($data);
    return $fld['value'];
  }


  function fieldSecurity(&$fld)
  {
    if (!$this->values_read || $fld['error'])
    {
      $code = $this->generateRandomString(2, 'abcdefghijkmnopqrstuvwxyz023456789');
      $_SESSION[$this->name.'_'.$fld['name'].'_security_code'] = $code;
    }
    else
      $code = $_SESSION[$this->name.'_'.$fld['name'].'_security_code'];

    $code_html = '&#'.ord($code[0]).';' . '&#'.ord($code[1]).';';

    $data = array(
      'fld' => $fld,
      'label' => isset($fld['label']) ? $fld['label'] : $this->texts[$fld['name']],
      'code' => $code_html
    );

    return $data;
  }
  function tplSecurity($data)
  {
    extract($data);
    return '
      <div id="'.$fld['name'].'_cnt">
        '.$label.' <strong>'.$code.'</strong> * '.Form::fieldText($fld).'
      </div>
      <script type="text/javascript">

      function '.$this->name.'_'.$fld['name'].'_submit()
      {
        var form = document.getElementById("'.$this->name.'");

        form.'.$fld['name'].'.value = String.fromCharCode('.ord($code[0]).') + String.fromCharCode('.ord($code[1]).');
        return true;
      }

       var e = document.getElementById("'.$fld['name'].'_cnt");
       if (e) e.style.display = "none";

      </script>
      ';
  }


  function generateRandomString($len, $charset)
  {
    if (!$len) return '';
    $s = '';
    $chlen = strlen($charset);
    while ($len--)
    {
      $s .= $charset[mt_rand(0, $chlen-1)];
    }
    return $s;
  }

  function template($type, $data)
  {
    $method = 'tpl'.ucfirst($type);
    if (!method_exists($this, $method)) return null;

    return $this->$method($data);
  }

  function addError($key, $param='')
  {
    $key = '_'.$key;
    $msg = isset($this->texts[$key]) ? $this->texts[$key] : $key;
    if ($param) $msg .= ' : '.$param;

    if (!in_array($msg, $this->errors))
      $this->errors[] = $msg;

  }

  function addUserError($msg)
  {
    $this->errors[] = $msg;
  }

  function extractCheckValues($keys, $value=0)
  {
    if (!is_array($keys))
      $keys = explode(' ', $keys);

    return array_combine($keys, array_fill(0, count($keys), $value));
  }

  function value($key)
  {
    if (!isset($this->fields[$key])) return null;
    return $this->fields[$key]['value'];
  }

  function field($key)
  {
    if (!isset($this->fields[$key]) || $this->fields[$key]['hidden']) return null;

    $fld = &$this->fields[$key];

    if (!$fld['type'] || !$fld['name']) return null;

    $method = 'field'.ucfirst($fld['type']);
    if (!method_exists($this, $method)) return null;

    $data = $this->$method($fld);
    return $this->template($fld['tpl'] ? $fld['tpl'] : $fld['type'], $data);
  }

  function label($key)
  {
    if (!isset($this->fields[$key]))
    {
      return isset($this->texts[$key]) ? $this->texts[$key] : $key;
    }

    if ($this->fields[$key]['hidden']) return '';

    $lbl = isset($this->fields[$key]['label']) ? $this->fields[$key]['label'] : $key;
    if (!isset($this->texts[$lbl])) return $lbl;

    $text = htmlspecialchars($this->texts[$lbl]);
    if ($this->fields[$key]['req'] && $this->fields[$key]['req'] != 2) $text .= ' *';
    return $this->fields[$key]['error'] ? sprintf($this->fielderrorhtml, $text) : $text;
  }

  function isHidden($key)
  {
    if (!isset($this->fields[$key]) || $this->fields[$key]['hidden']) return true;

    return false;
  }

}

?>
