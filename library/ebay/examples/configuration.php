<?php
/**
 * Copyright 2014 David T. Sadler
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Configuration settings used by all of the examples.
 * 
 * Specify your eBay application keys in the appropriate places.
 *
 * Be careful not to commit this file into an SCM repository.
 * You risk exposing your eBay application keys to more people than intended.
 *
 * For more information about the configuration, see:
 * http://devbay.net/sdk/guides/sample-project/
 */
return array(
    'sandbox' => array(
        'devId' => '504adeaf-b7c0-4144-bcf6-c2944b26923c',
        'appId' => 'somecomp-74e2-4a87-b769-d23d4d82156e',
        'certId' => '43c4759d-9372-471c-84bd-7083fbbc2031',
        'userToken' => 'AgAAAA**AQAAAA**aAAAAA**ogGyVg**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4GhDpaLpwqdj6x9nY+seQ**IrADAA**AAMAAA**HEIMBBDYSyQS7E6J03oJC3AcX8uTiOf7PQ9NUquGdlFgyBrPzbmK/vFi+KdTmhP0zIpyqc6RhUys6zVDmfVpkX2IhDxb1ASs3mjQYXud1TSRMmDGEYa4Cky+ipX03/PTcWVebwG7GIJu4cP1ktWbQRfjAKoie1KAPCgh2u+P7EqGTxxydhoyH9sU2FuR4wsU7WKg6ccK9ISfzg8ifSsB50Nc61QLKmYm1dsQce5DvK9vScTjwvjRz6Zu7QJCPHNFk9syO8/D5o1lC7LLChhvcTEtxPmeG4LXC2Av/IToHGI+g7mJQ5/0+UGAHN8x11dOcCfI7t/2paGvnySpL54h55ErmD6H/zrg/iMaVZxDJX2BSp5dLbtiatKSC622Vdw8VbUeTVZzfoIilBZC6NDBMmXu2+OWUTXqyWeolBgAMQR3HOcvRzbnKAzlOlhHsMWHlnu2E3ZjqGVHyz+NuWLhbrzoqLlXobcv0OHCWn0Wv11xKI4drLuPmxKLnu1FOicmhaYBngWskWKW2K+zLDyEcrFJL+BADB+aNjx03fvt4diZ+Fnt+TJzbEfrroXNiyht6j7xKa+fbBN3v5PcFt6lYJIJYE4hNmmYRBpNVKBEdCZZ8juiNHEAHssZnvrZloELIdf4pm3XnU/P+wGzPzwzbfH7MLz2W/+3cO4b8ayqI9QXTH2LbD7wyyQdE4b6kmMu1CQ2tGuqMGvRQEf9vUHPBajIH5quS9hXJJ6eSadN8KrXOiutXFNjIf4lWK+jVHqX'
				),
    'production' => array(
        'devId' => 'YOUR_PRODUCTION_DEVID_APPLICATION_KEY',
        'appId' => 'YOUR_PRODUCTION_APPID_APPLICATION_KEY',
        'certId' => 'YOUR_PRODUCTION_CERTID_APPLICATION_KEY',
        'userToken' => 'YOUR_PRODUCTION_USER_TOKEN_APPLICATION_KEY'
    ),
    'findingApiVersion' => '1.13.0',
    'tradingApiVersion' => '951',//871,
    'shoppingApiVersion' => '951',//871,
    'halfFindingApiVersion' => '1.2.0'
);
