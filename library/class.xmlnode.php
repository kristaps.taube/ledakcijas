<?
/*
 * Created by Raivo Fismeisters, 2008, raivo@datateks.lv
*/

class XMLNode
{
  var $name = null;
  var $attribs = Array();
  var $children = Array();
  var $keys = Array();
  var $vars = Array();
  var $parent = null;
  var $text = null;
  var $cdata = false;

  function XMLNode($name='')
  {
    $this->name = $name;
  }

  function load($xml_file, $encoding='utf-8')
  {
    $xml_parser = $this->initParser($encoding);

    $fp = fopen($xml_file, "r");
    if (!$fp) return null;
    while (!feof($fp))
    {
      $data = fread($fp, 65536);
      if ($data === false) return null;
      if(!(xml_parse($xml_parser, $data, feof($fp)))) {
          //die("Error on line " . xml_get_current_line_number($xml_parser));
          return null;
      }
    }
    xml_parser_free($xml_parser);
    fclose($fp);
    return $this;
  }

  function loadFromString($str, $encoding='utf-8')
  {
    $xml_parser = $this->initParser($encoding);

    if(!(xml_parse($xml_parser, $str, true))) {
        //die("Error on line " . xml_get_current_line_number($xml_parser));
        return null;
    }
    xml_parser_free($xml_parser);
    return $this;
  }

  function initParser($encoding)
  {
    $xml_parser = xml_parser_create($encoding);
    xml_parser_set_option($xml_parser, XML_OPTION_CASE_FOLDING, 0);
    xml_parser_set_option($xml_parser, XML_OPTION_SKIP_WHITE, 1);
    xml_set_element_handler($xml_parser, Array(&$this, 'xmlStartTag'), Array(&$this, 'xmlEndTag') );
    xml_set_character_data_handler($xml_parser, Array(&$this, "xmlContents"));

    $this->_curnode = null;

    return $xml_parser;
  }

  function xmlStartTag($parser, $name, $attribs)
  {
    if ($this->_curnode) $node = new XMLNode();
    else $node = &$this;

//    if ($name == 'CatalogItem')
//      echo 'Tag '.$name.'<br/>';

    $node->name = $name;
    $node->attribs = $attribs;
    $node->children = Array();
    $node->keys = Array();
    $node->vars = Array();
    $node->parent = &$this->_curnode;
    $node->text = null;

    if (!$this->_curnode)
    {
      $this->_curnode = &$this;
      return;
    }

    $this->_curnode->children[] = &$node;
    if (!isset($this->_curnode->keys[$name]))
    {
      $this->_curnode->keys[$name] = &$node;
    }
    $this->_curnode = &$node;
  }

  function xmlEndTag($parser, $name)
  {
    if (!$this->_curnode->parent) return;


/*    if (!isset($this->_curnode->parent->vars[$name]) &&
        $this->_curnode->parent->keys[$name] == $this->_curnode &&
        !count($this->_curnode->parent->keys[$name]->children) &&
        $this->_curnode->parent->keys[$name]->text != '')
    {
      $this->_curnode->parent->vars[$name] = &$this->_curnode->parent->keys[$name]->text;
    }*/

    $this->_curnode = &$this->_curnode->parent;
  }

  function xmlContents($parser, $data)
  {
    $data = trim($data);
    if (!$data) return;

    if (!$this->_curnode) return;
    if ($this->_curnode->text === null)
    {
      $this->_curnode->text = $data;
      return;
    }
    $this->_curnode->text .= $data;
  }

  function display($level=0)
  {
    $out = str_repeat('&nbsp;&nbsp;&nbsp;', $level);
    $out .= $this->name;
    if (count($this->attribs))
    {
      $out .= ' (';
      $a = Array();
      foreach ($this->attribs as $key=>$val)
      {
        array_push($a, $key.'="'.$val.'"');
      }
      $out .= implode(', ', $a) . ')';
    }
    if ($this->text != '' && !count($this->children))
    {
      $out .= ' = ' . htmlspecialchars($this->text);
    }

    $out .= '<br/>';
    $level++;
    foreach ($this->children as $ch)
    {
      $out .= $ch->output($level);
    }
    return $out;
  }

  function xmlEntities($string)
  {
     return str_replace ( array ( '&', '"', "'", '<', '>' ), array ( '&amp;' , '&quot;', '&apos;' , '&lt;' , '&gt;' ), $string );
  }

  function output($level=0, $putheader=false, $format=false)
  {
    $hdr = ($level == 0 && $putheader) ? '<?xml version="1.0" encoding="UTF-8"?>'.($format ? "\n" : '') : '';
    return $hdr.$this->outputLevel($level, $format);
  }

  function outputLevel($level=0, $format=false)
  {
    $out = '';
    if ($format)
      $out .= str_repeat(' ', $level * 2);

    $out .= '<'.$this->name;
    foreach ($this->attribs as $key => $value)
    {
      $out .= ' '.$key.'="'.$this->xmlEntities($value).'"';
    }
    if ($this->text != '' || count($this->children))
    {
      $out .= '>';
      if ($this->text != '')
      {
        $out .= $this->cdata ? '<![CDATA['.$this->text.']]>' : $this->xmlEntities($this->text);
      }

      if ($format && $this->children)
        $out .= "\n";

      foreach ($this->children as $ch)
      {
        $out .= $ch->outputLevel($level+1, $format);
      }

      if ($format && !$this->text)
        $out .= str_repeat(' ', $level * 2);

      $out .= '</'.$this->name.'>'.($format ? "\n" : '');
    }
    else
    {
      $out .= '/>'.($format ? "\n" : '');
    }
    return $out;
  }

  function addChild($node)
  {
    if (!$node) return;

    $node->parent = $this;
    if (!isset($this->keys[$node->name]))
    {
      $this->keys[$node->name] = &$node;
    }
    $this->children[] = &$node;
  }

  function getChildrenByName($name, $count=0)
  {
    $a = Array();
    foreach ($this->children as $key => $ch)
    {
      if ($ch->name == $name)
      {
        $a[] = &$this->children[$key];
        if ($count && count($a) >= $count) break;
      }
    }
    return $a;
  }

  function getChildByName($name)
  {
    $a = $this->getChildrenByName($name);
    return $a[0];
  }

  function getChildText($name)
  {
    $ch = $this->getChildByName($name);
    if (!$ch) return '';

    return $ch->text;
  }

  function createTextNode($name, $text, $cdata=false)
  {
    $node = new XMLNode($name);
    $node->cdata = $cdata;
    $node->text = $text;
    $this->addChild($node);

    return $node;
  }
}


?>