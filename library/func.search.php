<?

  function FormatForSearch($s)
  {
    while (preg_match("/\<\!\-\-(.*)?\-\-\>/Usi", $s, $regs))
    {
      $s = str_replace($regs[0], '', $s);
      //echo htmlentities($regs[0]);
    }
    while (preg_match("/\<title\>(.*)?\<\/title\>/Usi", $s, $regs))
    {
      $s = str_replace($regs[0], '', $s);
//      echo htmlentities($regs[0]);
    }
    while (preg_match("/\<head\>(.*)?\<\/head\>/Usi", $s, $regs))
    {
      $s = str_replace($regs[0], '', $s);
//      echo htmlentities($regs[0]);
    }
    while (preg_match("/\<style(.*)?\<\/style\>/Usi", $s, $regs))
    {
      $s = str_replace($regs[0], '', $s);
//      echo htmlentities($regs[0]);
    }
    while (preg_match("/\<script(.*)?\<\/script\>/Usi", $s, $regs))
    {
      $s = str_replace($regs[0], '', $s);
//      echo htmlentities($regs[0]);
    }

    while (preg_match("/{%nosearch%}(.*)?{%onsearch%}/Usi", $s, $regs))
    {
      $s = str_replace($regs[0], '', $s);
    }


    $s = str_replace("<", " <", $s );
    $s = strip_tags($s);

    $seperators = ".,\'\"\\/\n\r\t;:-+!*()_[]{}|?<>";

    for($f=0;$f<=strlen($seperators);$f++)
    {
      $s = str_replace($seperators[$f], " ", $s );
    }

    $seperators = Array("&nbsp;", "&lt;", "&gt;", "&quot;");
    for($f=0;$f<=strlen($seperators);$f++)
    {
      $s = str_replace($seperators[$f], " ", $s );
    }

    $s = preg_replace("(\s+)", " ", $s );
    $s = strtolower($s);
    return $s;
  }



  function FormatForObject($s)
  {
    while (preg_match("/\<\!\-\-(.*)?\-\-\>/Usi", $s, $regs))
    {
      $s = str_replace($regs[0], '', $s);
    }
    while (preg_match("/\<title\>(.*)?\<\/title\>/Usi", $s, $regs))
    {
      $s = str_replace($regs[0], '', $s);
    }
    while (preg_match("/\<head\>(.*)?\<\/head\>/Usi", $s, $regs))
    {
      $s = str_replace($regs[0], '', $s);
    }
    while (preg_match("/\<style(.*)?\<\/style\>/Usi", $s, $regs))
    {
      $s = str_replace($regs[0], '', $s);
    }
    while (preg_match("/\<script(.*)?\<\/script\>/Usi", $s, $regs))
    {
      $s = str_replace($regs[0], '', $s);
    }
    while (preg_match("/{%nosearch%}(.*)?{%onsearch%}/Usi", $s, $regs))
    {
      $s = str_replace($regs[0], '', $s);
    }


    $s = str_replace("<", " <", $s );
    $s = strip_tags($s);

    $seperators = "\n\r\t";

    for($f=0;$f<=strlen($seperators);$f++)
    {
      $s = str_replace($seperators[$f], " ", $s );
    }

    $seperators = Array("&nbsp;", "&lt;", "&gt;", "&quot;");
    for($f=0;$f<=strlen($seperators);$f++)
    {
      $s = str_replace($seperators[$f], " ", $s );
    }

    $s = preg_replace("(\s+)", " ", $s );
    return $s;
  }



  function IndexPage($site_id, $page_id, $PageText, $filename = '')
  {
    $IndexText = FormatForSearch($PageText);
    //echo "Index text = $IndexText<br>";
    $ObjectText = FormatForObject($PageText);
    //echo "ObjectText = $ObjectText<br>";

    // Split text on whitespace
    $indexArray =& explode( " ", $IndexText );

    // Count the total words in index text
    $totalWordCount = count( $indexArray );

    // Count the number of instances of each word
    $wordCountArray = array_count_values( $indexArray );

    // Strip double words
    $indexArray = array_unique( $indexArray );

    // Count unique words
    $uniqueWordCount = count( $indexArray );

    if($filename)
    {
      $linkwords = sqlQueryColumn("SELECT word_id FROM ".$site_id."_filesearch_link WHERE filename='$filename'");
    }else if($GLOBALS['cfgUseOldSearchIndexMethod'])
      $linkwords = sqlQueryColumn("SELECT word_id FROM ".$site_id."_search_link WHERE page_id='$page_id'");
    else
      $linkwords = false;

    if($linkwords)
    {
      $wordids = '';
      foreach($linkwords as $wordid)
      {
        if($wordids)
          $wordids .= ",";
        $wordids .= "'" . $wordid . "'";
      }
      $wordids = "(" . $wordids . ")";
      if($filename)
        sqlQuery(" UPDATE ".$site_id."_filesearch_word SET object_count=object_count-1 WHERE ".$site_id."_filesearch_word.id IN " . $wordids);
      else
        sqlQuery(" UPDATE ".$site_id."_search_word SET object_count=object_count-1 WHERE ".$site_id."_search_word.id IN " . $wordids);
    }

    if($filename)
    {
      sqlQuery(" DELETE FROM ".$site_id."_filesearch_object WHERE filename='$filename'");
      sqlQuery(" DELETE FROM ".$site_id."_filesearch_link WHERE filename='$filename'");
      sqlQuery(" INSERT INTO ".$site_id."_filesearch_object (data, filename)
                 VALUES ('".AddSlashes($ObjectText)."', '$filename')");
    }else
    {
      sqlQuery(" DELETE FROM ".$site_id."_search_object WHERE page_id = '$page_id' AND collection_id=0");
      sqlQuery(" DELETE FROM ".$site_id."_search_link WHERE page_id = '$page_id'");
      sqlQuery(" INSERT INTO ".$site_id."_search_object (page_id, data)
                 VALUES ('$page_id', '".AddSlashes($ObjectText)."')");
    }


    if($GLOBALS['cfgUseOldSearchIndexMethod'])
    {
      foreach ( $indexArray as $indexWord )
      {
          // Store word if it does not exist.

          $wordRes = sqlQueryRow("SELECT * FROM ".$site_id."_".IIF($filename,'file')."search_word WHERE word='$indexWord'" );

          if ($wordRes)
          {
              $wordID = $wordRes["id"];
              sqlQuery( "UPDATE ".$site_id."_".IIF($filename,'file')."search_word SET object_count=( object_count + 1 ) WHERE id='$wordID'" );
          }
          else
          {
              sqlQuery( "INSERT INTO ".$site_id."_".IIF($filename,'file')."search_word ( word, object_count ) VALUES ( '$indexWord', '1' )" );
              $wordID = sqlQueryValue("SELECT id FROM ".$site_id."_".IIF($filename,'file')."search_word WHERE word='$indexWord'");
          }

          // Calculate the relevans ranking for this word
          $frequency = ( $wordCountArray[$indexWord] / $totalWordCount );

          if($filename)
            sqlQuery( "INSERT INTO
                           ".$site_id."_filesearch_link ( word_id, frequency, word_count, filename )
                         VALUES ( '$wordID', '$frequency', '".$wordCountArray[$indexWord]."', '".AddSlashes($filename)."' )" );
          else
            sqlQuery( "INSERT INTO
                           ".$site_id."_search_link ( word_id, page_id, frequency, word_count)
                         VALUES ( '$wordID', '$page_id', '$frequency', '".$wordCountArray[$indexWord]."')" );
      }
    }

    $lastindextime = sqlQueryValue('SELECT lastindextime FROM `'.$site_id.'_pages` WHERE page_id='.$page_id);
    $lastmod = sqlQueryValue('SELECT lastmod FROM `'.$site_id.'_pages` WHERE page_id='.$page_id);
		if($lastmod){
		foreach ($GLOBALS['page_components'] as $comp)
    {
     				
			/* HOLY GRAIL FIX */
			/*if not visible component dont index, AND YEAH 1 MEANS 0 IT's NEW KIND OF MATH :D */
			if($comp['obj']->visible == 1)				
				continue;
			/* FIX END */	
			if ($comp['obj'])
        $data = $comp['obj']->indexData($lastindextime);
        
      foreach ($data as $col_id => $col)
      {
        if (!array_keys($col)) continue;

        sqlQuery('DELETE FROM `'.$site_id.'_search_object` WHERE page_id='.$page_id.' AND collection_id='.$col_id.' ');// WHY THIS? AND item_id IN ('.implode(',', array_keys($col)).') EVERY TIME CLEAN DB TO HAVE NEW RAW DATA

        foreach ($col as $item_id => $item)
        {
					$testData = trim($item['data']);					
          if(empty($testData) OR ($testData == ' '))
						continue;//why insert no DATA? M?
					
						sqlQuery('INSERT INTO `'.$site_id.'_search_object` SET data="'.AddSlashes($item['data']).'", page_id='.$page_id.', query="'.AddSlashes($item['query']).'", title="'.AddSlashes($item['title']).'", collection_id='.$col_id);//, item_id='.$item_id.' EVERY TIME CLEAN DB TO HAVE NEW RAW DATA
        }
				
      }
    }
	
    //mark page as indexed
    sqlQuery('UPDATE '.$site_id.'_pages SET mustsearchindex=0, lastindextime='.time().' WHERE page_id=' . $page_id);
		}
		else
			echo 'SLIKTA LAPA';
  }

  function IndexPageForSearch($site_id, $page_id, $force = false)
  {
      if($page_id <= 0)
        return false;
      if($force)
      {
        ob_start();
        $GLOBALS['forced_site_id'] = $site_id;
        $GLOBALS['forced_page_id'] = $page_id;
        $GLOBALS['dontneedauth'] = true;
        $GLOBALS['search_leavesearchtags'] = true;
        $GLOBALS['prevent_frontend_redirects'] = true;
        include($GLOBALS['cfgDirRoot'] . "index.php");
        $html = ob_get_contents();
        ob_end_clean();

        //sqlQuery('SET NAMES utf8');
        IndexPage($site_id, $page_id, $html);
        //sqlQuery('SET NAMES BINARY');

      }else
      {
        //mark page as "please index this page on next display"
        sqlQuery('UPDATE '.$site_id.'_pages SET mustsearchindex=1 WHERE page_id=' . $page_id);
      }
  }

  function IndexFileForSearch($site_id, $file)
  {
      $dirroot = sqlQueryValue("SELECT dirroot FROM sites WHERE site_id=".$site_id);
      if(preg_match("/\.doc$/i", $file))
      {
        system("/usr/local/bin/antiword -t -m cp1257.txt ". $dirroot . "/" . $file." > ". $dirroot . "/files/tmp/a.htm");
        $html = implode('', file($dirroot . "/files/tmp/a.htm"));
      }else
      {
        $html = implode('', file($dirroot . "/" . $file));
      }

      IndexPage($site_id, 0, $html, $file);
  }

  function SearchPagesFullText($site_id, $query, &$qwords, $limit = false)
  {
    $query2 = FormatForSearch($query);
    $qwords = explode( " ", $query2 );
    $qwords = array_slice($qwords, 0, 50);

    foreach($qwords as $k => $q) {
      $qwords[$k] = $q.'*';
    }

    $query = implode(' ', $qwords);

//    sqlQuery('SET NAMES utf8');
    $c = sqlQueryData("SELECT so.page_id, so.query, so.title,  MATCH(data) AGAINST('".$query."' IN BOOLEAN MODE) AS f, MATCH(data) AGAINST('".$query."' IN BOOLEAN MODE) AS c
                       FROM ".$site_id."_search_object AS so
                       LEFT JOIN ".$site_id."_pages ON (".$site_id."_pages.page_id = so.page_id)
                       WHERE
                          MATCH(data) AGAINST('".$query."' IN BOOLEAN MODE)
                          AND ".$site_id."_pages.passprotect = 0
                          AND ".$site_id."_pages.redirect = 0
                       ".($limit ? $limit : ''));
//    sqlQuery('SET NAMES BINARY');
    return $c;
  }


  function SearchPages($site_id, $query, &$qwords)
  {
    $query = FormatForSearch($query);
    $qwords = explode( " ", $query );
    array_slice($qwords, 0, 50);

    $query = "0 ";
    foreach($qwords as $word)
    {
      $query .= " OR word='" . $word . "'";
    }
//    SELECT frequency, page_id FROM 9_search_link, 9_search_word WHERE word =  'main' AND 9_search_link.word_id = 9_search_word.id
    if($GLOBALS['hasbeenauthorized'])
    {
      $c = sqlQueryData("SELECT page_id, sum( frequency ) AS f, sum(word_count) as c
                          FROM ".$site_id."_search_link, ".$site_id."_search_word
                          WHERE (".$query.")
                          AND ".$site_id."_search_link.word_id = ".$site_id."_search_word.id
                          AND object_count > 0
                          GROUP BY page_id
                          ORDER BY c DESC");
    }else
    {
      $c = sqlQueryData("SELECT ".$site_id."_pages.page_id, sum( frequency ) AS f, sum(word_count) as c
                          FROM ".$site_id."_search_link, ".$site_id."_search_word, ".$site_id."_pages
                          WHERE (".$query.")
                          AND ".$site_id."_search_link.word_id = ".$site_id."_search_word.id
                          AND ".$site_id."_search_link.page_id = ".$site_id."_pages.page_id
                          AND ".$site_id."_pages.passprotect = 0
                          AND object_count > 0
                          AND ".$site_id."_pages.redirect = 0
                          GROUP BY ".$site_id."_pages.page_id
                          ORDER BY c DESC");
    }
    return $c;
  }


  function SearchFiles($site_id, $query, &$qwords)
  {
    $query = FormatForSearch($query);
    $qwords = explode( " ", $query );
    array_slice($qwords, 0, 50);

    $query = "0 ";
    foreach($qwords as $word)
    {
      $query .= " OR word='" . $word . "'";
    }
    $c = sqlQueryData("SELECT filename, sum( frequency ) AS f, sum(word_count) as c
                        FROM ".$site_id."_filesearch_link, ".$site_id."_filesearch_word
                        WHERE (".$query.")
                        AND ".$site_id."_filesearch_link.word_id = ".$site_id."_filesearch_word.id
                        AND filename <> ''
                        AND object_count > 0
                        GROUP BY filename
                        ORDER BY c DESC");
    return $c;
  }


  function SearchFileKeywords($site_id, $query, &$qwords)
  {
    $query = FormatForSearch($query);
    $qwords = explode( " ", $query );
    array_slice($qwords, 0, 50);

    $query = "0 ";
    foreach($qwords as $word)
    {
      $query .= " OR word='" . $word . "'";
    }
    $c = sqlQueryData("SELECT file_path AS filename, 0 AS f, 1 as c, 1 as keywords
                        FROM ".$site_id."_filesearch_keywordlink, ".$site_id."_file_keywords
                        WHERE (".$query.")
                        AND ".$site_id."_filesearch_keywordlink.keyword_id = ".$site_id."_file_keywords.keyword_id
                        AND file_path <> ''
                        GROUP BY filename
                        ORDER BY filename");
    return $c;
  }


  function SearchFileKeywordsByID($site_id, $ids)
  {
    $query = "0 ";
    foreach($ids as $id)
    {
      $query .= " OR keyword_id='" . $id . "'";
    }
    $c = sqlQueryData("SELECT file_path AS filename, 0 AS f, 1 as c, 1 as keywords
                        FROM ".$site_id."_filesearch_keywordlink
                        WHERE (".$query.")
                        AND file_path <> ''
                        GROUP BY filename
                        ORDER BY filename");

    return $c;
  }


  function TextAroundWord($obj, $words)
  {
    $obj .= " ";
    $cleanobj =$obj;
    $seperators = ".,\n\r;:!?";
    for($i=0;$i<=strlen($seperators);$i++)
    {
      $cleanobj = str_replace($seperators[$i], " ", $cleanobj );
    }
    $poss = Array();
    foreach($words as $w)
    {
      if($w)
      {
        if($poss =  strpos($cleanobj, " " . $w . " "))
        {
          $wpos = $poss;
          $word = $w;
        }
      }
    }
    $f = $wpos;
    $b = $f + strlen($word) + 50;
    $f = max(0, $f-50);
    $wpos = max(100, $wpos);

    if($f)
    {
      $start = substr($obj, $wpos-100, 100);
      if($f1 = strpos($start,'.'))
      {
        $f = $wpos + $f1 - 99;
      }else
      {
        for($f1=$f;(($f-$f1<100)and($cleanobj[$f1]!=' '));$f1--)
        {
        }
        $f = $f1;
      }
    }

    $end = substr($obj, $b, 300);
    if($f1 = strrpos($end, '.'))
    {
      $b += $f1 + 1;
    }else
    {
      $b += 300;
      if($b < strlen($obj)-1)
      {
        for($f1=$b;(($b-$f1<200)and($cleanobj[$f1]!=' '));$f1--)
        {
        }
        $b = $f1;
      }else
      {
        $b = strlen($obj)-1;
      }
    }
    $res = substr($obj, $f, $b - $f);
    if($f)
      $res = '... ' . $res;
    if($b < strlen($obj)-1)
      $res = $res . ' ...';
    return $res;

  }


  function BoldKeyWords($obj, $swords)
  {
    $obj .= " ";
    foreach($swords as $word)
    {
      if($word)
      {
        $word = str_replace('*', '', $word);
        $obj = preg_replace("/(".$word.")/iu", "<span class='orange'>\\1</span>", $obj);
      }
    }
    return $obj;
  }


  //http://zez.org/article/view/83/







?>