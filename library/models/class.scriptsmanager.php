<?


class ScriptsManager
{
  var $ext_scripts = array();
  var $script = '';
  var $ext_css = array();
  var $css = '';

  function ScriptsManager($libs)
  {
    $this->libs = $libs;
  }

  function includeScript($path)
  {
    if (in_array($path, $this->ext_scripts)) return;

    $this->ext_scripts[] = $path;
  }

  function addScript($script)
  {
    $this->script .= $script;
  }

  function includeCSS($path)
  {
    if (in_array($path, $this->ext_css)) return;

    $this->ext_css[] = $path;
  }

  function addCSS($script)
  {
    $this->css .= $script;
  }

  function includeLibrary($name)
  {
    if (!isset($this->libs[$name])) return false;

    foreach ($this->libs[$name] as $path)
    {
      $ext = pathinfo($path, PATHINFO_EXTENSION);
      switch (strtolower($ext))
      {
        case 'js': $this->includeScript($path); break;
        case 'css': $this->includeCSS($path); break;
        case '':
          $nm = pathinfo($path, PATHINFO_BASENAME);
          if (isset($this->libs[$nm]))
            $this->includeLibrary($nm);

          break;
      }
    }

    return true;
  }

  function generateHeaderHTML()
  {
    $header = '';

    foreach ($this->ext_css as $path)
    {
      $header .= '  <link type="text/css" rel="stylesheet" href="/scr/components/'.$path.'" />'."\n";
    }

    if ($this->css != '')
      $header .= "  <style type=\"text/css\">\n".$this->css."\n  </style>\n";

    foreach ($this->ext_scripts as $path)
    {
      $header .= '  <script type="text/javascript" src="/scr/components/'.$path.'"></script>'."\n";
    }

    if ($this->script != '')
      $header .= "  <script type=\"text/javascript\">\n".$this->script."\n  </script>\n";
      

    return $header;
  }

}

?>