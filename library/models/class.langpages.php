<?


class LangPages
{

  static function createTable()
  {
    sqlQuery("
    CREATE TABLE IF NOT EXISTS `".$GLOBALS['site_id']."_pages_lang_links` (
      `id` int(10) NOT NULL auto_increment,
      `from_id` int(10) NOT NULL default '0',
      `to_id` int(10) NOT NULL default '0',
      `from_lang` int(11) NOT NULL default '0',
      `to_lang` int(11) NOT NULL default '0',
      PRIMARY KEY  (`id`),
      KEY `from_to` (`from_id`,`to_id`,`from_lang`,`to_lang`)
    ) ENGINE=MyISAM DEFAULT CHARSET=utf8
    ");

  }

  static function getLink($id, $from_lang, $to_lang){

    if(!$from_lang || !$to_lang) return;

    return sqlQueryRow('
      SELECT * FROM '.$GLOBALS['site_id'].'_pages_lang_links
      WHERE
        (from_id='.$id.' AND from_lang='.$from_lang.' AND to_lang='.$to_lang.') OR
        (to_id='.$id.' AND to_lang='.$from_lang.' AND from_lang='.$to_lang.')
      LIMIT 1
    ');
  }


  function getLinksFrom($from_id, $links=null)
  {
    if ($links == null) $links = array();
    $data = sqlQueryDataAssoc('
      SELECT * FROM '.$GLOBALS['site_id'].'_pages_lang_links
      WHERE
        from_id='.$from_id.'
    ');
    foreach ($data as $row)
    {
      if (isset($links[$row['to_lang']])) continue;
      $links[$row['to_lang']] = array(
        'from_id' => $row['from_id'],
        'to_id' => $row['to_id'],
        'from_lang' => $row['from_lang'],
        'to_lang' => $row['to_lang'],
        'row' => $row
      );

    }
    return $links;
  }

  function getLinks($from_id, $to_lang)
  {
    $links = LangPages::getLinksFrom($from_id);

    $data = sqlQueryDataAssoc('
      SELECT * FROM '.$GLOBALS['site_id'].'_pages_lang_links
      WHERE
        to_id='.$from_id.' AND to_lang='.$to_lang.'
    ');
    foreach ($data as $row)
    {
      $links[$row['from_lang']] = array(
        'from_id' => $row['to_id'],
        'to_id' => $row['from_id'],
        'from_lang' => $row['to_lang'],
        'to_lang' => $row['from_lang'],
        'row' => $row
      );

    }

    return $links;
  }

  function addLink($from_id, $to_id, $from_lang, $to_lang)
  {
    sqlQuery('INSERT INTO '.$GLOBALS['site_id'].'_pages_lang_links SET from_id='.(int)$from_id.', to_id='.(int)$to_id.', from_lang='.(int)$from_lang.', to_lang='.(int)$to_lang);
    return sqlLastID();
  }

  function changeLinkFrom($id, $from_id)
  {
    return sqlQuery('UPDATE '.$GLOBALS['site_id'].'_pages_lang_links SET from_id='.(int)$from_id.' WHERE id='.(int)$id);
  }

  function changeLinkTo($id, $to_id)
  {
    return sqlQuery('UPDATE '.$GLOBALS['site_id'].'_pages_lang_links SET to_id='.(int)$to_id.' WHERE id='.(int)$id);
  }

  function deleteLink($id)
  {
    return sqlQuery('DELETE FROM '.$GLOBALS['site_id'].'_pages_lang_links WHERE id='.(int)$id);
  }

}

?>