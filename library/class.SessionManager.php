<?

class SessionManager{

  static $timeout = 7200;
  static $session_row_id = 0;

  static function createTable(){

    sqlQuery("
    CREATE TABLE IF NOT EXISTS `sessions` (
      `id` int(11) NOT NULL auto_increment,
      `session_id` varchar(255) character set latin1 default NULL,
      `last_activity` datetime default NULL,\n  PRIMARY KEY  (`id`),
      KEY `session_id` (`session_id`),
      KEY `last_activity` (`last_activity`)
    ) ENGINE=MyISAM DEFAULT CHARSET=utf8
    ");

  }
            
  static function registerSession(){

		$id = sqlQueryValue('SELECT id FROM sessions WHERE session_id="'.session_id().'"');

		if ($id){
      sqlQuery('UPDATE sessions SET last_activity=NOW() WHERE id='.$id);
      SessionManager::$session_row_id = $id;
    }else{
      sqlQuery('INSERT INTO sessions SET session_id="'.session_id().'", last_activity=NOW()');
      SessionManager::$session_row_id = DB::GetLastInsertId();
    }

    SessionManager::dropDeadSessions();

  }

  static function dropDeadSessions(){
    sqlQuery('DELETE FROM sessions WHERE last_activity < DATE_SUB(NOW(), INTERVAL '.SessionManager::$timeout.' SECOND)');
  }

}
