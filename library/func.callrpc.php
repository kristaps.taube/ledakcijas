<?php
include("func.xmlrpc.php");
$xmlrpc_request = XMLRPC_parse($HTTP_RAW_POST_DATA);
$methodName = XMLRPC_getMethodName($xmlrpc_request);
//library name
$libraryName = explode('.', $methodName);
$libraryName = $libraryName[0];
$params = XMLRPC_getParams($xmlrpc_request);
if((!isset($xmlrpc_methods[$methodName]))or(!isset($xmlrpc_libraries[$libraryName]))){
    $xmlrpc_methods['method_not_found']($methodName);
}else{
    #call the method
    require_once($xmlrpc_libraries[$libraryName]);
    $xmlrpc_methods[$methodName]($params);
}
?>
