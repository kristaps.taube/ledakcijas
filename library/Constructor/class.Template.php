<?php
/**
* Constructor Template class
*
* Version 1.0 (26-01-2018)
* @author Kristaps Taube <ktaube@datateks.lv>
*
**/

namespace Constructor;

use DB;

class Template
{

    private $name;
    private $template;
    private $title_tag;
    private $forced_title_tag;
    private $meta_description;
    private $_params = [];
    private $js_files = [];
    private $css_files = [];
    private static $instance;

    public function __construct($name = '')
    {
        $this->setName($name);
        self::$instance = $this;
    }

    public static function getInstance()
    {
        return self::$instance ? self::$instance : new Template();
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function setTitleTag($title_tag)
    {
        $this->title_tag = $title_tag;
    }

    public function forceTitleTag($title_tag)
    {
        $this->forced_title_tag = $title_tag;
    }

    public function setMetaDescription($meta_description)
    {
        $this->meta_description = $meta_description;
    }

    public function __set($name, $value)
    {
        $this->_params[$name] = $value;
    }

    public function __get($name)
    {
        return isset($this->_params[$name]) ? $this->_params[$name] : null;
    }

    public function render($content = '')
    {

        $this->template = $this->getTemplate($this->name, $content);
        $this->attachSubTemplates();

        # remove comments
        $this->template = preg_replace('/{%\/\/(.*?)\/\/%}/s', '', $this->template);

        $this->processDesignParts();
        $this->processLanguageParts();
        $this->processLanguageStrings();

        $this->processComponents();

        $this->processVariables();

        return $this->template;

    }

    public function startBody(){}

    public function endBody()
    {

        foreach($this->getJSFiles() as $file){
            echo "<script src='".$file."'></script>";
        }

    }

    public function getJSFiles()
    {
        return $this->js_files;
    }

    public function includeJSFile($file)
    {

        if(!in_array($file, $this->js_files)){
            $this->js_files[] = $file;
        }

    }

    public function includeCSSFile($file)
    {

        if(!in_array($file, $this->css_files)){
            $this->css_files[] = $file;
        }

    }

    public function getCSSFiles()
    {
        return $this->css_files;
    }

    public function startHead(){}

    public function endHead(){}

    private function getTemplate($name, $content = '')
    {

        extract($this->_params);

        $path = App::$app->getConfigValue("template_folder").$name.'.php';
        if(file_exists($path)){
            ob_start();
            require $path;
            return ob_get_clean();
        }else{
            die("Template `".$name."` not found");
        }

    }

    private function processVariables()
    {

        $site_id = App::$app->getSiteId();
        $url = App::$app->url_parser->getUrl();

        $this->template = str_replace("{%var:name%}", $page['name'], $this->template);
        $this->template = str_replace("{%var:site_id%}", $site_id, $this->template);
        $this->template = str_replace("{%language:shortname%}", App::$app->getlanguage(), $this->template);

        $this->template = str_replace("{%url:host%}", isset($url['host']) ? $url['host'] : '', $this->template);
        $this->template = str_replace("{%url:path%}", isset($url['path']) ? $url['path'] : '', $this->template);
        $this->template = str_replace("{%url:query%}", isset($url['query']) ? $url['query'] : '', $this->template);

        # {%header%}
        $header = [];

        if(WebApp::$app->getConfigValue('developmode')){
            $header[] = "<meta name='robots' content='noindex, nofollow' />";
        }

        $header[] = $GLOBALS['additional_header_tags'];

        if($this->meta_description){
            $header[] = '<meta name="description" content="'.htmlspecialchars($this->meta_description).'">';
        }

        $this->template = str_replace("{%header%}", implode("\n", $header), $this->template);
        $this->template = str_replace("{%visualeditjs%}", $header, $this->template);

        while (preg_match("/{%var:glo:([a-zA-Z0-9_]+)%}/s", $this->template, $regs)){
            $this->template = str_replace($regs[0], $GLOBALS[$regs[1]], $this->template);
        }

        $title_tag_suffix = option('title_tag_suffix', NULL, 'Title tag suffix', array('value_on_create' => ' - E-Veikals'));

        $title = '';
        if($this->forced_title_tag){
            $title = $this->forced_title_tag;
        }elseif($this->title_tag){
            $title = $this->title_tag.$title_tag_suffix;
        }

        $this->template = str_replace("{%title%}", $title, $this->template);

        # speacial tags


    }

    private function processLanguageStrings()
    {

        $site_id = App::$app->getSiteId();
        $lang_id = App::$app->getLanguageId();
        $translation_folder = App::$app->getConfigValue('translation_folder');

        //languagestrings
        while (preg_match("/{%ls:([a-zA-Z0-9_]+)(:(([^%])*))?%}/s", $this->template, $regs)){

            $ls = GetLanguageString($site_id, $regs[1], '', $lang_id);

            if(!$ls){
            $ls = $regs[3];
            $string = $ls;
            $key = transliterateText($ls);

            $cmp = "_template";

            if (file_exists($translation_folder.$lang_id.'.'.$cmp.'.lang.php')){
                require_once($translation_folder.$lang_id.'.'.$cmp.'.lang.php');
            }

            if (!isset($languagestrings[$lang_id][$cmp][$key])){
                $test=DB::GetValue("select ls_id from ".$site_id."_languagestrings where language=:lang and name=:key and component=:cmp",array(':lang'=>$lang_id,':key'=>$key,':cmp'=>$cmp));
                if (!$test){
                    DB::Query("INSERT INTO `".$site_id."_languagestrings`(language, name, value, `default`, component) VALUES(:lang,:key,:val,:val,:cmp)",array(':lang'=>$lang_id,':key'=>$key,':val'=>$string,':cmp'=>$cmp));
                    DB::Query("INSERT INTO ".$site_id."_languagestrings_info(name, component, is_used) VALUES(:key,:cmp,1)",array(':key'=>$key,':cmp'=>$cmp));
                }
                $res=DB::GetTable("select name,value,`default` from ".$GLOBALS['site_id']."_languagestrings where language=:lang and component=:cmp order by name",array(':lang'=>$lang_id,':cmp'=>$cmp));
                if (!file_exists($GLOBALS['cfgDirRoot'].'lang')){
                    mkdir($GLOBALS['cfgDirRoot'].'lang');
                }
                $f=fopen($translation_folder.$lang_id.'.'.$cmp.'.lang.php','w');
                fwrite($f,'<?php'."\n");
                foreach ($res as $r){
                    $value = $r['value']?$r['value']:$r['default'];
                    fwrite($f,'$languagestrings['.$lang_id.'][\''.$cmp.'\'][\''.$r['name'].'\']=\''.str_replace("'","\'",str_replace('\\','\\\\',$value)).'\';'."\n");
                }
                fwrite($f,'?>');
                fclose($f);
                unset($languagestrings[$shortname][$cmp]);
                clearstatcache();
                include($translation_folder.$lang_id.'.'.$cmp.'.lang.php');
            }

            }else{
                $key = transliterateText($regs[3]);
            }

            $this->template = str_replace($regs[0], $languagestrings[$lang_id][$cmp][$key], $this->template);

        }

    }

    private function processComponents()
    {

        $components = [];

        $component_count = preg_match_all("/{%component:([a-zA-Z0-9_]+):([a-zA-Z0-9]+)(:[a-zA-Z0-9% \\.\\,\\\"\\\'\\#\\!-]+)?%}/s", $this->template, $matches, PREG_SET_ORDER);
        if($component_count)
        {

            foreach($matches as $match){

                $component_cnf = [
                    'fulltag' => $match[0],
                    'type' => $match[1],
                    'name' => $match[2],
                    'params' => $match[3],
                ];

                #$component = $component_cnf['type']::getInstance($component_cnf['name']);
                $component = $component_cnf['type']::getInstance($component_cnf['name']);
                $components[$component_cnf['fulltag']] = $component;

            }

            // execute
            foreach($components as $component){
                $component->execute();
            }

            #var_dump();

            // output
            $i = 0;
            foreach($components as $tag => $component){

                $cstart = microtime(true);
                ob_start();
                $component->output();
                $output = ob_get_clean();

                if(App::$app->getConfigValue('developmode')){
                    $comment_start = "<!-- Component ".$component->name."(".$component->type."). Output time: ".number_format(microtime(true ) - $cstart, 5)."s -->";
                    $comment_end = "<!-- Component ".$component->name."(".$component->type.") output end -->";
                    $output = "\n".$comment_start."\n".$output."\n".$comment_end;
                }

                $this->template = str_replace_once($tag, $output, $this->template);

            }

        }

    }

    private function processLanguageParts()
    {

        $lang = WebApp::$app->getLanguage();

        //process iflanguage parts
        preg_match_all ( '/{%iflanguage([0-9]*?):([a-zA-Z0-9|]*?)%}(.*?){%filanguage\\1%}/s', $body, $matches, PREG_SET_ORDER);
        foreach($matches as $key => $row)
        {
            $s = explode('{%elselanguage' . $row[1] . '%}', $row[3]);
            $languages = explode('|', $row[2]);
            $shortname = $lang;
            $hidden = true;

            foreach($languages as $l){
                if($l == $shortname){
                    $hidden = false;
                }

            }

            if(!$hidden){
                $body = str_replace($row[0], $s[0], $body);
            }else{
                $body = str_replace($row[0], $s[1], $body);
            }
        }

    }

    private function processDesignParts()
    {

        //process ifdesign parts
        preg_match_all ( '/{%ifdesign([0-9]*?)%}(.*?){%fidesign\\1%}/s', $this->template, $matches, PREG_SET_ORDER);
        foreach($matches as $key => $row){
            $s = explode('{%elsedesign' . $row[1] . '%}', $row[2]);
            $this->template = str_replace($row[0], $s[1], $this->template);
        }

    }

    private function attachSubTemplates()
    {

        while (preg_match('/\{%template:([a-zA-Z0-9 _]+)%\}/U', $this->template, $regs)){
            $this->template = str_replace($regs[0], $this->getTemplate($regs[1]), $this->template);
        }

    }

}