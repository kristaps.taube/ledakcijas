<?php
/**
* Constructor base Controller class
*
* Version 1.0 (25-01-2018)
* @author Kristaps Taube <ktaube@datateks.lv>
*
**/

namespace Constructor;

abstract class Controller
{

    private $_name;
    protected $template; // template name
    private $_template; // template obj
    private $content_type = 'text/html';
    private $charset = 'utf-8';
    private $_called_action;
    private $_js_files = [];
    private $_css_files = [];

    public function __construct($controller_name)
    {

        $this->title = ucfirst($controller_name);
        $this->_name = $controller_name;
        $this->template = $this->template ? $this->template : WebApp::$app->getConfigValue('default_template');
        $this->_template = new Template;

    }

    public function getName()
    {
        return $this->_name;
    }

    public function setCharset($charset)
    {
        $this->charset = $charset;
    }

    public function getCharset()
    {
        return $this->charset;
    }

    public function getContentType()
    {
        return $this->content_type;
    }

    public function setContentType($content_type)
    {
        $this->content_type = $content_type;
    }

    public function forceTitleTag($tag)
    {
        $this->_template->forceTitleTag($tag);
    }

    public function setTitleTag($tag)
    {
        $this->_template->setTitleTag($tag);
    }

    public function setMetaDescription($meta_description)
    {
        $this->_template->setMetaDescription($meta_description);
    }

    public function setTemplateVars(array $vars)
    {
        foreach($vars as $key => $value){
            $this->_template->$key = $value;
        }
    }

    public function setTemplateVar($key, $value)
    {
        $this->_template->$key = $value;
    }

    protected function beforeAction($action)
    {
        return true;
    }

    protected function afterAction($action){}

    public function callAction($action, $params = [])
    {

        $action_raw = $action;
        $action = self::createActionName($action);

        if(method_exists($this, $action)){

            $before_action_response = $this->beforeAction($action_raw, $params);

            if($before_action_response === true){

                $params = makeMethodParameters(get_called_class(), $action, $params);
                $response = call_user_func_array(array($this, $action), $params);
                $this->_called_action = $action;
                $this->afterAction($action);

                $response = $this->processActionResponse($response);

            }else{
                $response = $this->processActionResponse($before_action_response);
            }

        }else{
            die('Controller `'.get_called_class().'` missing action `'.$action.'`');
        }

        return $response;

    }

    private function processActionResponse($response_obj)
    {

        if($response_obj instanceof RenderPartialResponse){
            $response = $response_obj->getResponse();
        }elseif($response_obj instanceof RenderResponse){
            $content = $response_obj->getResponse();
            $this->_template->setName($this->template);
            $response = $this->_template->render($content);
        }elseif($response_obj instanceof RedirectResponse){
            return $response_obj;
        }elseif($response_obj instanceof EndResponse){
            $response = $response_obj->getResponse();
        }elseif($response_obj instanceof PDFResponse){
            $response = $response_obj->getResponse();
        }elseif($response_obj instanceof RenderJSONResponse){
            $response = $response_obj->getResponse();
        }elseif($response_obj instanceof RenderXMLResponse){
            $response = $response_obj->getResponse();
        }else{
            die("Unknown `".get_called_class()."` response for action `".$this->_called_action."`");
        }

        return $response;

    }

    protected function redirectBack()
    {

        $url = isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER'] ? $_SERVER['HTTP_REFERER'] : '/';
        return $this->redirect($url, 302);

    }

    protected function refresh()
    {
        $url = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : $_SERVER['SCRIPT_URL'];
        return $this->redirect($url, 302);
    }

    protected function redirect($url, $code = '')
    {
        $response = new RedirectResponse($url, $code);
        return $response;
    }

    protected function render($view, array $data = [])
    {
        $response = new RenderResponse($this, $view, $data);
        return $response;
    }

    protected function end()
    {
        return new EndResponse;
    }

    public function renderComponentView($view, $data)
    {

        return \component::getInstance()->render($view, $data);

    }

    public function _renderView($view, $data)
    {

        $view_path = $this->getViewPath($view);

        if(file_exists($view_path)){

            ob_start();
            extract($data);
            require($view_path);
            $html = ob_get_clean();

        }else{

            die("View not found: ".$view_path);

        }

        return $html;

    }

    protected function renderPartial($view, $data)
    {

        $response = new RenderPartialResponse($this, $view, $data);
        return $response;

    }

    protected function renderPDF($filename, $content, $download = false)
    {

        $response = new PDFResponse($filename, $content, $download);
        return $response;

    }

    protected function renderXML($root_node, $data = [], $numeric_key = 'item')
    {

        $this->setContentType('text/xml');
        $response = new RenderXMLResponse($root_node, $data, $numeric_key);
        return $response;

    }

    protected function renderJSON($data)
    {

        $response = new RenderJSONResponse($data);
        return $response;

    }

    private function getViewPath($path)
    {
        $result = (substr($path,0,1) == '/') ? substr($path,1) : $this->_name."/".$path;
        return App::$app->getConfigValue("template_folder").$result.'.php';
    }

    public static function createClassName($controller_name)
    {
        $controller_name = lcfirst(implode("",array_map('ucfirst', explode("-", $controller_name))));
        return 'controllers\\'.ucfirst($controller_name).'Controller';
    }

    public static function createActionName($action)
    {
        $action = lcfirst(implode("",array_map('ucfirst', explode("-", $action))));
        return 'action'.ucFirst($action);
    }

    public function includeJSFile($file)
    {
        $this->_template->includeJSFile($file);
    }

    public function includeCSSFile($file)
    {
        $this->_template->includeCSSFile($file);
    }

}