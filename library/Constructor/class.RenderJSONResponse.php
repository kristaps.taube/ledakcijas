<?php
/**
* Constructor render json response
*
* Version 1.0 (29-01-2018)
* @author Kristaps Taube <ktaube@datateks.lv>
*
**/

namespace Constructor;

use DB;

class RenderJSONResponse
{

    private $data;

    public function __construct($data = [])
    {
        $this->data = $data;
    }

    public function getResponse()
    {
        return json_encode($this->data);
    }

}