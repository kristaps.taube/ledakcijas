<?php
/**
* Constructor Command parser
*
* Version 1.0 (26-02-2018)
* @author Kristaps Taube <ktaube@datateks.lv>
*
**/

namespace Constructor;

class CommandParser
{

    // create obj by CommandParser::getInstance();
    private function __construct(){}

    public static function getInstance()
    {

        static $instance;

        if(!$instance){
            $instance = new CommandParser();
        }

        return $instance;

    }

    private function getActionName($action)
    {
        return 'action'.ucfirst($action);
    }

    public function parse($argv)
    {

        $result = false;

        unset($argv[0]);
        if(isset($argv[1])){

            $parts = explode("/", $argv[1]);

            $command_name = $parts[0];
            $action_name = isset($parts[1]) ? $parts[1] : 'index';
            $action_name = $this->getActionName($action_name);
            unset($argv[1]);

            $params = [];
            foreach(array_values($argv) as $a){

                $parts = explode("=", $a);
                if(count($parts) == 2){
                    $params[$parts[0]] = $parts[1];
                }

            }

        }else{
            throw new \Exception('No command name given');
        }


        $commands = App::$app->getConfigValue('commands');

        if(is_array($commands)){
            $found = false;
            foreach($commands as $key => $class_name){

                if($key == $command_name){
                    $command = new $class_name;
                    $found = true;
                    break;
                }

            }

            if(!$found){
                throw new \Exception('Command not found');
            }

            if(method_exists($command, $action_name)){

                $result = new CommandParserResult($command, $action_name, $params);

            }else{
                throw new \Exception("Action '".$action_name."' do not exist in command '".get_class($command)."'");
            }

        }

        return $result;

    }


}