<?php
/**
* Constructor URL class
*
* Version 1.0 (02-02-2018)
* @author Kristaps Taube <ktaube@datateks.lv>
*
**/

namespace Constructor;

class Url
{

    public static function get($route, $params = [], $scheme = false)
    {

        $result = self::processUrlRules($route, $params);
        return self::postProcessURL($result, $scheme);

    }

    public static function preProcessUrl(array $url)
    {

        $url['url'] = substr($url['url'], strlen(App::$app->getConfigValue('url_prefix'))); // remove prefix


        return $url;

    }

    private static function postProcessURL($url, $scheme)
    {

        $url = App::$app->getConfigValue('url_prefix').$url;

        if($scheme){

            $url = (App::$app->getConfigValue('https') ? 'https://' : 'http://').App::$app->getConfigValue('domain').$url;

        }

        return $url;

    }

    private static function processUrlRules($route, $params)
    {

        $result = false;

        if(is_array(App::$app->getConfigValue('url_rules'))){

            foreach(App::$app->getConfigValue('url_rules') as $entry){

                $rule = new $entry();
                $result = $rule->createUrl($route, $params);
                if($result){
                    break;
                }

            }

        }

        return $result;

    }

}