<?php

namespace Constructor;

abstract class UrlRule
{

    public function parseRequest($url)
    {

        $result = $this->parseUrl($url);

        if($result){
            $result = $this->postProcessResult($result);
        }


        return $result;

    }

    private function postProcessResult($result)
    {

        return $result;

    }


}