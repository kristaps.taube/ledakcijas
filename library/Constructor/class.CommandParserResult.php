<?php
/**
* Constructor command parser result
*
* Version 1.0 (26-01-2018)
* @author Kristaps Taube <ktaube@datateks.lv>
*
**/

namespace Constructor;

class CommandParserResult
{

    private $command;
    private $action;
    private $params;

    public function __construct($command, $action = 'index', $params = [])
    {

        $this->command = $command;
        $this->action = $action;
        $this->params = $params;

    }

    public function getCommand()
    {
        return $this->command;
    }

    public function getAction()
    {
        return $this->action ? $this->action : 'index';
    }

    public function getParams()
    {
        return $this->params ? $this->params : [];
    }

}