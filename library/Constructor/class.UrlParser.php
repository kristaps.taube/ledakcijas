<?php
/**
* Constructor URL parser
*
* Version 1.0 (25-01-2018)
* @author Kristaps Taube <ktaube@datateks.lv>
*
**/

namespace Constructor;

use DB;

class UrlParser
{

    private $site;
    private $site_id;
    private $url;
    public $rule;

    // create obj by URLParser::getInstance();
    private function __construct(){}

    public static function getInstance()
    {

        static $instance;

        if(!$instance){
            $instance = new URLParser();
        }

        return $instance;

    }

    public function parse()
    {

        $this->setUrl();
        $this->parseForSiteId();

        $params = null;

        $result = $this->processUrlRules();

        if($result){
            $controller = $result[0];
            $action = isset($result[1]) ? $result[1] : null;
            $params = isset($result[2]) ? $result[2] : null;
            $parser_result = new UrlParserResult($controller, $action, $params);
        }else{
            die("Can't parse this request");
        }

        return $parser_result;

    }

    private function processUrlRules()
    {

        $result = false;

        if(is_array(App::$app->getConfigValue('url_rules'))){

            App::$app->logger->logCheckpoint('Url rules start');
            foreach(App::$app->getConfigValue('url_rules') as $entry){

                $rule = new $entry();
                $result = $this->processUrlRule($rule);
                App::$app->logger->logCheckpoint('After '.$entry.' url rule');
                if($result){
                    $this->rule = $rule;
                    break;
                }
            }
            App::$app->logger->logCheckpoint('Url rules end');

        }

        return $result;

    }

    private function processUrlRule(UrlRuleInterface $rule)
    {

        $result = $rule->parseRequest($this->url);
        return $result ? $result : false;

    }

    private function parseForSiteId()
    {

        $site = DB::GetRow("SELECT site_id,dirroot,domain FROM sites WHERE domain = :host", [':host' => $this->url['host']]);
        if(!$site){ // look into aliases
            $sites = DB::GetTable("SELECT site_id, alias, dirroot, domain FROM sites WHERE LOCATE(:host, alias)", [':host' => $this->url['host']]);
            foreach($sites as $site){
                $aliases = explode(";", $site['alias']);
                if(in_array($this->url['host'], $aliases) || in_array('*', $aliases)) break;
            }
        }

        if(!$site) die("Error 404 - site not found");

        $this->site = $site;
        $this->site_id = $site['site_id'];

        App::$app->setSite($site);
        App::$app->setSiteId($site['site_id']);

    }

    private function setUrl()
    {

        $url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        $url = parse_url($url);

        $this->url = Url::preProcessUrl($url);

    }

    public function getUrl()
    {
        return $this->url;
    }

    public function getLanguage()
    {
        return $this->site_lang;
    }

    public function getLanguageId()
    {
        return $this->site_lang_id;
    }

}