<?php

namespace Constructor;

interface UrlRuleInterface
{
    public function createUrl($route, $params = []);
    public function parseRequest($url);
}