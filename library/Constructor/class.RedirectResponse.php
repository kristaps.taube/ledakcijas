<?php
/**
* Constructor redirect response
*
* Version 1.0 (29-01-2018)
* @author Kristaps Taube <ktaube@datateks.lv>
*
**/

namespace Constructor;

class RedirectResponse
{

    private $location;
    private $code = 302;

    public function __construct($location, $code = false)
    {
        $this->location = $location;
        $this->code = $code ? $code : $this->code;
    }

    public function getResponse()
    {
        return json_encode($this->data);
    }

    public function sendHeader()
    {
        header("Location: ".$this->location, true, $this->code);
    }

}