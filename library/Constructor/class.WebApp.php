<?php
/**
* Constructor WebApp class
*
* Version 1.0 (25-01-2018)
* @author Kristaps Taube <ktaube@datateks.lv>
*
**/

namespace Constructor;

use DB;

class WebApp extends App
{

    public $request;
    private $site_id;
    private static $_controller;
    private static $_controller_name;
    private static $_action;

    public function __construct($config = [])
    {

        parent::__construct($config);

    }

    public function __get($name)
    {

        if($name == 'controller'){ // access WebApp::$app->controller
            return self::$_controller;
        }elseif($name == 'action'){ // WebApp::$app->action
            return self::$_action;
        }

        return parent::__get($name);

    }

    protected function init()
    {

        session_start();

        parent::init();

        if($this->getConfigValue('developmode')){ // these methods should be moved outside this class
            $this->processCrontab();
            $this->processTemplates();
        }

    }

    public function initForBackend()
    {

        $site = \DB::getRow('SELECT site_id,dirroot,domain FROM sites');

        if($site)
        {
            App::$app->setSite($site);
            App::$app->setSiteId($site['site_id']);
        }

        return $this;

    }

    public function run()
    {

        $this->url_parser = UrlParser::getInstance();

        $response = false;

        if($result = $this->url_parser->parse()){
            $response = $this->callControllerAction($result);
        }

        if($response){
            if($response instanceof RedirectResponse){
                $response->sendHeader();
            }else{
                header('Content-Type: '.$this->_controller->getContentType().'; charset=' . $this->_controller->getCharset());
                echo $response;
            }
        }

    }

    private function callControllerAction(UrlParserResult $result)
    {

        $controller_name = $result->getController();
        $action = $result->getAction();
        $params = $result->getParams();

        $controller_class_name = Controller::createClassName($controller_name);

        $controller = new $controller_class_name($controller_name);

        $this->_controller = $controller;
        self::$_controller_name = $controller_name;
        self::$_action = $action;

        $response = $controller->callAction($action, $params);

        return $response;

    }

    public static function getControllerName()
    {
        return self::$_controller_name;
    }

    public static function getActionName()
    {
        return self::$_action;
    }

    public function getLanguageId()
    {

        $site_id = WebApp::$app->getSiteId();
        if(!$site_id) die("Site id not found in ".__FILE__." at ".__LINE__." line");

        $langs = getLanguages($site_id);
        $lang = $this->getLanguage();

        return isset($langs[$lang]) ? $langs[$lang]['language_id'] : false;

    }

    private function processTemplates()
    {

        $items = scandir($this->getConfigValue('template_folder'));
        $sites = DB::GetTable("SELECT * FROM sites");

        foreach($sites as $site){

            $templates = DB::GetTable("SELECT * FROM `".$site['site_id']."_templates`");
            $tmp = [];
            foreach($templates as $t){
                $tmp[$t['name']] = $t;
            }
            $templates = $tmp;
            $max_ind = DB::GetValue("SELECT max(ind) FROM `".$site['site_id']."_templates`");

            foreach($items as $item){

                if(get_file_extension($item) == 'php'){

                    $path_to_file = $this->getConfigValue('template_folder').$item;

                    $parts = explode(".", $item);
                    $tpl_name = $parts[0];

                    if(isset($templates[$tpl_name])){ // we have this tpl, check upd time

                        $tpl_row = $templates[$tpl_name];
                        $file_mod_time = filemtime($path_to_file);

                        if(!$tpl_row['updated'] || strtotime($tpl_row['updated']) < $file_mod_time){

                            DB::Update($site['site_id']."_templates", array("template_id" => $tpl_row['template_id']), array(
                                "body" => file_get_contents($path_to_file),
                                "updated" => date("Y-m-d H:i:s", $file_mod_time),
                            ));

                        }

                    }else{ // we dont have this tpl, lets add

                        $template_id = DB::Insert($site['site_id']."_templates", array(
                            "name" => $tpl_name,
                            "body" => file_get_contents($path_to_file),
                            "updated" => date("Y-m-d H:i:s", $file_mod_time),
                            "description" => "Auto created",
                            "ind" => ++$max_ind
                        ));

                        //Add access to the newly created template for Everyone
                        AddGroupPermission(1, 12, $site['site_id'], $template_id);
                        AddGroupPermission(1, 15, $site['site_id'], $template_id);
                        AddGroupPermission(1, 15, $site['site_id'], $template_id);
                        AddGroupPermission(1, 15, $site['site_id'], $template_id);
                        Add_Log("Auto created template '".$tpl_name."' (".$site['site_id'].")", $site['site_id']);

                    }

                }

            }

            // creating missing template files
            foreach($templates as $template){

                $tpl = $template['name'].".php";
                if(!in_array($tpl, $items)){
                    file_put_contents($this->getConfigValue('template_folder').$tpl, $template['body']);
                }

            }

        }

    }

    private function processCrontab()
    {

        // update crontab
        $conf = file_get_contents($this->getConfigValue('crons/config'));
        $conf = "# E-Veikals Crontab conf. start \n".(trim($conf))."\n# E-Veikals Crontab conf. end";

        $conf = str_replace("[DirRoot]", $GLOBALS['cfgDirRoot'], $conf);
        $conf = str_replace("[CronFolder]", $this->getConfigValue('crons/directory'), $conf);
        $conf = trim($conf);
        $tmp_file = $this->getConfigValue('crons/directory')."tmp";

        $crontab = shell_exec('crontab -l');
        if(!is_null($crontab)){ // update existing crontab

            preg_match('/# E-Veikals Crontab conf. start(.+)# E-Veikals Crontab conf. end/s', $crontab, $matches);
            if(count($matches)){
                $conf = str_replace($matches[0], $conf, $crontab);
            }else{
                $conf = $crontab."\n\n".$conf;
            }

        }

        file_put_contents($tmp_file, $conf);
        shell_exec('crontab '.$tmp_file);
        if(is_file($tmp_file)){
            unlink($tmp_file); // delete tmp file
        }


    }

}