<?php
/**
* Constructor render response
*
* Version 1.0 (26-01-2018)
* @author Kristaps Taube <ktaube@datateks.lv>
*
**/

namespace Constructor;

use DB;

class RenderResponse
{

    private $html;
    private $view;
    private $data;
    private $controller;

    public function __construct($controller, $view, $data = [])
    {

        $this->view = $view;
        $this->data = $data;
        $this->controller = $controller;

    }

    public function getResponse()
    {
        $this->html = $this->controller->_renderView($this->view, $this->data);
        return $this->html;
    }

}