<?php
/**
* Constructor Command base class
*
* Version 1.0 (03-04-2018)
* @author Kristaps Taube <ktaube@datateks.lv>
*
**/

namespace Constructor;

abstract class Command
{

    public function index(){}

    public static function run($route, $params = [])
    {

        $param_arr = [];

        foreach($params as $key => $value){
            $param_arr[] = $key.'='.$value;
        }

        $cmd = 'php '.App::$app->getConfigValue('dir_root').'/constructor.php '.$route.' '.implode(" ", $param_arr);

        self::exec($cmd);

    }

    private static function exec($cmd)
    {

        if(substr(php_uname(), 0, 7) == "Windows"){
            pclose(popen("start /B ". $cmd, "r"));
        }else{
            exec($cmd. " > /dev/null &");
        }

    }

}