<?php
/**
* Constructor end response
*
* Version 1.0 (29-01-2018)
* @author Kristaps Taube <ktaube@datateks.lv>
*
**/

namespace Constructor;

class EndResponse
{

    public function getResponse(){}

}