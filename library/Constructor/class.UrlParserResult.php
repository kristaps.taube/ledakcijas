<?php
/**
* Constructor URL parser result
*
* Version 1.0 (26-01-2018)
* @author Kristaps Taube <ktaube@datateks.lv>
*
**/

namespace Constructor;

class UrlParserResult
{

    private $controller;
    private $action;
    private $params;

    public function __construct($controller, $action = 'index', $params = [])
    {

        $this->controller = $controller;
        $this->action = $action;
        $this->params = $params;

    }

    public function getController()
    {
        return $this->controller;
    }

    public function getAction()
    {
        return $this->action ? $this->action : 'index';
    }

    public function getParams()
    {
        return $this->params ? $this->params : [];
    }

}