<?php
/**
* Constructor Translation class
*
* Version 1.0 (01-02-2018)
* @author Kristaps Taube <ktaube@datateks.lv>
*
**/

namespace Constructor;

use DB;

class Translation
{

    private $config;
    private $_texts = [];

    // create obj by Translation::getInstance();
    private function __construct($config){
        $this->config = $config;
    }

    public static function getInstance($config = [])
    {

        static $instance;

        if(!$instance){
            $instance = new Translation($config);
            $instance->loadTexts();
        }

        return $instance;

    }

    public function getTexts()
    {
        return $this->_texts;
    }

    public function searchTexts($needle)
    {

        $result = [];

        foreach($this->_texts as $group => $texts){
            foreach($texts as $text => $langs){
                foreach($langs as $lang => $lang_text){
                    if(strpos(mb_strtolower($lang_text), mb_strtolower($needle)) !== false){ // if we find one
                        if(!isset($result[$group])) $result[$group] = [];
                        $result[$group][$text] = $langs; // add every lang
                        break;
                    }
                }
            }
        }

        return $result;

    }

    public function updateTexts($data)
    {

        foreach($data as $group => $texts){
            foreach($texts as $text =>$langs){
                foreach($langs as $lang => $lang_text){

                    if(!isset($this->_texts[$group])) $this->_texts[$group] = [];
                    if(!isset($this->_texts[$group][$text])) $this->_texts[$group][$text] = [];
                    $this->_texts[$group][$text][$lang] = $lang_text;

                }
            }
        }

        echo "<pre>".print_r($this->_texts, true)."</pre>";

        return $this;

    }

    public function saveTexts()
    {

        if(!isset($this->config['store'])){
            die("No store file defined for ".get_class());
        }

        $data = json_encode($this->_texts);

        file_put_contents($this->config['store'], $data);

    }

    private function loadTexts()
    {

        if(!isset($this->config['store'])){
            die("No store file defined for ".get_class());
        }

        if(file_exists($this->config['store'])){
            $raw = file_get_contents($this->config['store']);
        }else{
            $raw = false;
        }

        $data = $raw ? json_decode($raw, true) : false;
        $data = $data && is_array($data) ? $data : [];

        $this->_texts = $data;

    }

    public function getText($category, $text, $lang)
    {

        if(isset($this->_texts[$category][$text][$lang])){
            $return_text = $this->_texts[$category][$text][$lang];
        }else{
            $return_text = $text;
            $this->_texts[$category][$text][$lang] = $text;
            $this->saveTexts();
        }

        return $return_text;

    }




}