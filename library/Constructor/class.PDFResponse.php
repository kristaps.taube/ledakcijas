<?php
/**
* Constructor pdf response
*
* Version 1.0 (07-02-2018)
* @author Kristaps Taube <ktaube@datateks.lv>
*
**/

namespace Constructor;

require_once($GLOBALS['cfgDirRoot']."library/mPDF/mpdf.php");
use \mPDF;

class PDFResponse
{

    private $filename;
    private $content;
    private $download;

    public function __construct($filename, $content, $download = false)
    {
        $this->filename = $filename;
        $this->content = $content;
        $this->download = $download;
    }

    public function getResponse()
    {

        $mpdf = new mPDF();
        $mpdf->WriteHTML($this->content);
        if($this->download){
            $mpdf->Output($this->filename, "D");
        }else{
            $mpdf->Output();
        }

    }

}
