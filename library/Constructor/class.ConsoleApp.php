<?php
/**
* Constructor ConsoleApp class
*
* Version 1.0 (26-02-2018)
* @author Kristaps Taube <ktaube@datateks.lv>
*
**/

namespace Constructor;

use DB;

class ConsoleApp extends App
{

    private $site_id;
    private $command;

    public function __construct($config = [])
    {

        parent::__construct($config);

    }

    public function init()
    {

        parent::init();

        $site = \DB::getRow('SELECT site_id,dirroot,domain FROM sites');

        if($site)
        {
            App::$app->setSite($site);
            App::$app->setSiteId($site['site_id']);
        }

    }

    public function run($params = [])
    {

        try{

            $this->command = CommandParser::getInstance();

            if($result = $this->command->parse($params)){
                $response = $this->callCommand($result);
            }

        }catch(\Exception $e) {
            echo 'Error: ' .$e->getMessage().PHP_EOL;
        }

    }

    private function callCommand($result)
    {

        $command = $result->getCommand();
        $action = $result->getAction();
        $params = $result->getParams();

        return $command->$action($params);

    }

}