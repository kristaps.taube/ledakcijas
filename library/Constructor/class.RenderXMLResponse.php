<?php
/**
* Constructor render xml response
*
* Version 1.0 (20-03-2018)
* @author Kristaps Taube <ktaube@datateks.lv>
*
**/

namespace Constructor;

use DB;

class RenderXMLResponse
{

    private $data;

    public function __construct($root_node, $data = [], $numeric_key = 'item')
    {
        $this->data = $data;
        $this->root_node = $root_node;
        $this->numeric_key = $numeric_key;
    }

    public function getResponse()
    {

        $xml = new \SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><'.$this->root_node.'></'.$this->root_node.'>');
        $this->arrayToXml($this->data, $xml, $this->numeric_key);

        return $xml->asXML();

    }

    private function arrayToXml($array, &$xml, $numeric_key)
    {
        foreach($array as $key => $value){
            if(is_array($value)) {
                if(!is_numeric($key)){
                    $subnode = $xml->addChild($key);
                    $this->arrayToXml($value, $subnode, $numeric_key);
                }else{
                    $subnode = $xml->addChild($numeric_key);
                    $this->arrayToXml($value, $subnode, $numeric_key);
                }
            }else {
                $xml->addChild($key, htmlspecialchars($value));
            }
        }
    }

}