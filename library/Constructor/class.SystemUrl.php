<?php
/**
* Constructor SystemUrl class
*
* Version 1.0 (29-01-2018)
* @author Kristaps Taube <ktaube@datateks.lv>
*
**/

namespace Constructor;

use DB;

class SystemUrl
{

    private static $cache = [];

    // create obj by SystemUrl::getInstance();
    private function __construct(){}

    public static function getInstance()
    {

        static $instance;

        if(!$instance){
            $instance = new SystemUrl();
            $instance->init();
        }

        return $instance;

    }

    private function init()
    {

        if(WebApp::$app->getConfigValue('developmode')){

            $check = DB::GetValue("SHOW TABLES LIKE '".self::getTableName()."'");

            if(!$check){

                $create_query = '
                    CREATE TABLE `'.self::getTableName().'`(
                    `id` INT NOT NULL AUTO_INCREMENT,
                    `name` VARCHAR(45) NULL,
                    `url` VARCHAR(100) NULL,
                    `lang` VARCHAR(2) NULL,
                    PRIMARY KEY (`id`));
                ';

                DB::Query($create_query);

            }

        }

        self::loadCache();

    }

    public function updateLink($key, $lang, $link)
    {

        DB::Update(self::getTableName(), ['name' => $key, 'lang' => $lang], ['url' => $link]);

    }

    private static function getTableName()
    {

        $site_id = App::$app->getSiteId();
        return $site_id.'_system_urls';

    }

    public function getCache()
    {
        return self::$cache;
    }

    public static function getAll()
    {

        $return = [];

        $data = DB::GetTable("SELECT * FROM `".self::getInstance()->getTableName()."`");
        foreach($data as $entry){
            if(!isset($return[$entry['name']])) $return[$entry['name']] = [];
            $return[$entry['name']][$entry['lang']] = $entry['url'];
        }

        return $return;

    }

    private static function loadCache()
    {

        if(!self::$cache){
            self::$cache = self::getAll();
        }

        return self::$cache;

    }

    public static function getDefault($key, $lang = false)
    {

        $app_lang = isset(WebApp::$app) ? WebApp::$app->getLanguage() : false;
        $lang = $lang ? $lang : $app_lang;
        if(!$lang) return '';

        $definitions = self::getInstance()->getDefinitions();

        return isset($definitions[$key]['default']) ? $definitions[$key]['default'].'-'.$lang : '';

    }

    public static function get($key, $lang = false)
    {

        $app_lang = isset(WebApp::$app) ? WebApp::$app->getLanguage() : false;
        $lang = $lang ? $lang : $app_lang;
        if(!$lang) return '';

        $instance = self::getInstance();

        $definitions = $instance->getDefinitions();

        if(!isset($definitions[$key])){
            die('Unknown system url `'.$key.'`');
        }

        if(isset(self::$cache[$key][$lang])){
            $url = self::$cache[$key][$lang];
        }else{

            $url = $definitions[$key]['default'].'-'.$lang; // default url

            DB::Insert($instance->getTableName(), [
                'name' => $key,
                'lang' => $lang,
                'url' => $url
            ]);

            self::$cache[$key][$lang] = $url;

        }

        if(substr($url,0,1) != '/'){ // is this a good idea?
            $url = '/'.$url;
        }

        return $url;

    }

    public function getDefinitions()
    {

        return [

            /*'login' => [
                'label' => 'Login url',
                'default' => 'login',
                'route' => 'user/login'
            ],*/
            'registration' => [
                'label' => 'Reģistrācija',
                'default' => 'registration',
                'route' => 'registration/index'
            ],
            'registration/account-activated' => [
                'label' => 'Konts aktivizēts',
                'default' => 'registration/account-activated',
                'route' => 'registration/account-activated'
            ],
            'password_recovery' => [
                'label' => 'Paroles atjaunošana',
                'default' => 'password-recovery',
                'route' => 'user/password-recovery'
            ],
            'cart' => [
                'label' => 'Pasūtījuma noformēšana',
                'default' => 'basket',
                'route' => 'cart/index'
            ],
            'cart/done' => [
                'label' => 'Pasūtījums noformēts',
                'default' => 'basket-done',
                'route' => 'cart/done'
            ],
            /*'checkout' => [
                'label' => 'Checkout url',
                'default' => 'checkout',
                'route' => 'checkout/index'
            ],*/
            'paysera/payment-successful' => [
                'label' => 'Apmaksa veiksmīga',
                'default' => 'paysera-error',
                'route' => 'paysera/payment-successful'
            ],
            'paysera/payment-cancelled' => [
                'label' => 'Apmaksa atcelts',
                'default' => 'paysera-error',
                'route' => 'paysera/payment-cancelled'
            ],
            'compare' => [
                'label' => 'Salīdzināšana',
                'default' => 'compare',
                'route' => 'compare/index'
            ],
            'wishlist' => [
                'label' => 'Vēlmju saraksts',
                'default' => 'wishlist',
                'route' => 'wishlist/index'
            ],
            'search' => [
                'label' => 'Meklēšana',
                'default' => 'search',
                'route' => 'search/index'
            ],
            'catalog' => [
                'label' => 'Katalogs',
                'default' => 'catalog',
                'route' => 'catalog/index'
            ],
            'profile/order-history' => [
                'label' => 'Rēķinu vēsture',
                'default' => 'profile/order-history',
                'route' => 'user/order-history'
            ],
            'profile/change-password' => [
                'label' => 'Paroles maiņa',
                'default' => 'profile/change-password',
                'route' => 'user/change-password'
            ],
            /*'news' => [
                'label' => 'News page',
                'default' => 'news',
                'route' => 'news/index'
            ],*/
            'blog' => [
                'label' => 'Bloga lapa',
                'default' => 'blog',
                'route' => 'blog/index'
            ],
            'calculator' => [
                'label' => 'Kalkulatora lapa',
                'default' => 'calculator',
                'route' => 'calculator/index'
            ],
            'buj' => [
                'label' => 'BUJ lapa',
                'default' => 'faq',
                'route' => 'faq/index'
            ],
            'new' => [
                'label' => 'Jauno produktu lapa',
                'default' => 'new',
                'route' => 'product-list/new'
            ],
            'popular' => [
                'label' => 'Populāro produktu lapa',
                'default' => 'popular',
                'route' => 'product-list/popular'
            ]
            
        ];

    }



}