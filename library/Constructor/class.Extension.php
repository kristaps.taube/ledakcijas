<?php

namespace Constructor;

abstract class Extension
{

    protected $config;

    protected function __construct($config)
    {

        $this->config = $config;

    }

    public function __get($name)
    {
        return isset($this->config[$name]) ? $this->config[$name] : null;
    }



}