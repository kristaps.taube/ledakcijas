<?php
/**
* Constructor App base class
*
* Version 1.0 (25-01-2018)
* @author Kristaps Taube <ktaube@datateks.lv>
*
**/

namespace Constructor;

abstract class App
{

    private $config;
    private $language;
    public static $app;
    private $site;
    private $site_id;
    private $_translations;
    private $_start_time;
    private $_extensions;

    public function __construct($config = [])
    {

        $this->_start_time = microtime(true);
        $this->config = $config;
        $this->_translations = Translation::getInstance(['store' => $this->getConfigValue('translations_store')]);

        self::$app = $this;

        $this->init();

    }

    public function __get($name)
    {

        if(isset($this->_extensions[$name])){ // extension already loaded

            return $this->_extensions[$name];

        }elseif($this->getConfigValue('extensions') && is_array($this->getConfigValue('extensions')) && isset($this->getConfigValue('extensions')[$name])){ // if it's extension, load it

            $extension = $this->getConfigValue('extensions')[$name];

            if(isset($extension['class'])){
                $class = $extension['class'];
                unset($extension['class']);
                $extension = new $class($extension);
                $this->_extensions[$name] = $extension;
                return $this->_extensions[$name];
            }

        }

    }

    public function getStartTime()
    {
        return $this->_start_time;
    }

    abstract function run();

    protected function init()
    {

        ini_set('memory_limit', $this->getConfigValue('memory_limit'));
        date_default_timezone_set($this->getConfigValue('timezone'));

        error_reporting($this->getConfigValue('errors/reporting_level'));
        ini_set('error_log', $this->getConfigValue('errors/file'));

        if($this->getConfigValue('errors/display')){
            ini_set('display_errors', 1);
        }else{
            ini_set('display_errors', 0);
        }

        if($this->getConfigValue('errors/log')){
            ini_set('log_errors', 1);
        }else{
            ini_set('log_errors', 0);
        }

        $this->setLanguage($this->getConfigValue('language'));

        setlocale(LC_ALL, 'lv_LV');

        $this->autoload();

    }

    private function autoload()
    {

        if($this->getConfigValue('autoload') && is_array($this->getConfigValue('autoload'))){
            foreach($this->getConfigValue('autoload') as $file){
                require_once $file;
            }
        }

    }

    public function getConfigValue($key)
    {

        $parts = array_filter(explode("/", $key));
        if($parts && count($parts) > 1){

            $config = $this->getConfig();
            $good = false;

            foreach($parts as $part){
                if(isset($config[$part])){
                    $config = $config[$part];
                    $good = true;
                }else{
                    $good = false;
                    break;
                }
            }

            $value = $good ? $config : null;

        }else{
            $key = $parts[0];
            $value = isset($this->config[$key]) ? $this->config[$key] : null;
        }

        return $value;
    }

    public static function l($text, $params = [])
    {

        $category = isset($params['category']) && $params['category'] ? $params['category'] : 'default';
        $lang = isset($params['lang']) && $params['lang'] ? $params['lang'] : App::$app->getLanguage();

        $lang = $lang ? $lang : App::$app->getLanguage();
        $translated_text = App::$app->_translations->getText($category, $text , $lang);

        if(isset($params['replace']) && is_array($params['replace'])){
            $translated_text = replace_all($translated_text, $params['replace']);
        }

        return $translated_text;

    }

    public function getConfig()
    {
        return $this->config;
    }

    public function getLanguage()
    {
        return $this->language;
    }

    public function setLanguage($lang)
    {
        $this->language = $lang;
    }

    public function getSite()
    {
        return $this->site;
    }

    public function getSiteId()
    {
        return $this->site_id;
    }

    public function setSite($site)
    {
        $this->site = $site;
    }

    public function setSiteId($site_id)
    {
        $GLOBALS['site_id'] = $site_id; // for backwards compatibility
        $this->site_id = $site_id;
    }

}