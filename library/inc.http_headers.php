<?

$GLOBALS['now'] = gmdate('D, d M Y H:i:s') . ' GMT';
if($GLOBALS['nocache']){
  header('Expires: ' . $GLOBALS['now']); // rfc2616 - Section 14.21
  header('Last-Modified: ' . $GLOBALS['now']);
  header('Cache-Control: no-store, no-cache, must-revalidate, pre-check=0, post-check=0, max-age=0'); // HTTP/1.1
  header('Pragma: no-cache'); // HTTP/1.0
}

$content_type = ($GLOBALS['content_type'] == "text/xml")? $GLOBALS['content_type'] : "text/html";

if($GLOBALS['maincharset']){
  header('Content-Type: '.$content_type.'; charset=' . $GLOBALS['maincharset']);
}
