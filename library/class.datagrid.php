<?

#===================================================================================#
#  Copyright © 2002 Karlis Blumentals. All Rights Reserved.                         #
#  e-mail: kblums@latnet.lv                                                         #
#===================================================================================#
# Modified by Aivars Irmejs, aivars@serveris.lv

// FUNCTIONS

function CreateColumnBars(&$data, $col_scores, $col_bars, $maxscore, $limit) {
  foreach(array_keys($data) as $key) {
    if ($maxscore > $limit)
      foreach(array_keys($data) as $key) $data[$key][$col_bars] = intval($data[$key][$col_scores]/$maxscore*$limit);
    else
      foreach(array_keys($data) as $key) $data[$key][$col_bars] = $data[$key][$col_scores];
  }
}

include_once($GLOBALS['cfgDirRoot']."library/"."func.misc.php");

// CLASSES

class DataTable {

    var $name;
    var $cols;
    var $rows;
    var $header;
    var $footer;
    var $sortlinkformat;

    function DataTable() {
      $this->name = "datagrid";
      $this->cols = array();
      $this->rows = array();
      $this->header = true;
      $this->footer = true;
    }

    function get_title(){
      $html = "<tr>\n";
      foreach (array_keys($this->cols) as $name) {
        if (isset($this->cols[$name]["align"])) $align = ' align="'.$this->cols[$name]["align"].'"'; else $align="";
        if (isset($this->cols[$name]["width"])) $width = ' width="'.$this->cols[$name]["width"].'"'; else $width="";
        if (isset($this->cols[$name]["sorted"])) $class = ' class="tableSortedHead"'; else $class=' class="tableHead"';
        $title = $this->cols[$name]["title"];
        if((isset($this->cols[$name]["sortdesc"]))and($this->cols[$name]["sortdesc"]))
          $descstring = "&desc=1";
        else
          $descstring = "";
        if (isset($this->cols[$name]["sortable"])) $title = '<a'.$class.' href="'.str_replace('%s', $name, $this->sortlinkformat).$descstring.'">'.$title.'</a>';
        $html .= "  <td".$width.$align.$class.">\n";
        $html .= "    ".$title."\n";
        if($this->cols[$name]['popup'])
        {
          $html .= '&nbsp;<input class="columnButton" type="button" value="..."';
          if (!isset($this->property['dialogw'])) $w = 400; else $w = $this->property['dialogw'];
          if (!isset($this->property['dialogh'])) $h = 450; else $h = $this->property['dialogh'];
          $html .= " OnClick=\"javascript:openDialog('".$this->cols[$name]['popup']."', ".$w.", ".$h.", 1, 0);\">";
        }
        $html .= "  </td>\n";
      }
      $html .= "</tr>\n";
      return($html);
    }

    function get_rows(){
      $html = '';
      foreach($this->rows as $contents) {
        $i = 0;
        $html  .= '<tr bgcolor="#FFFFFF" '.$contents[$i++].">\n";
        foreach (array_keys($this->cols) as $name) {
          if (isset($this->cols[$name]["align"])) $align = ' align="'.$this->cols[$name]["align"].'"'; else $align="";
          if (isset($this->cols[$name]["width"])) $width = ' width="'.$this->cols[$name]["width"].'"'; else $width="";
          if (isset($this->cols[$name]["html"])) $contents[$i] = nl2br(htmlspecialchars($contents[$i]));
          $html .= "  <td ".$width.$align." class=\"tableRow\">\n";
          $c = $contents[$i++];
          if ($c == "") $c = "&nbsp;";
          $html .= "    ".$c."\n";
          $html .= "  </td>\n";
        }
        $html .= "</tr>\n";
      }
      return($html);
    }

    function get_empty(){
      $html = "<tr bgcolor=\"#FFFFFF\">\n";
      $html .= "  <td width=\"100%\" align=\"center\" class=\"tableText\" colspan=\"".sizeof(array_keys($this->cols))."\">\n";
      $html .= "    {%langstr:no_items_display%}\n";
      $html .= "  </td>\n";
      $html .= "</tr>\n";
      return($html);
    }

    function add_row($contents){
      $this->rows[] = $contents;
    }

    function output(){
      $title = $this->get_title();
      $html  = '<table '.($this->id ? 'id="'.$this->id.'"' : '').' width="100%" border="0" cellspacing="0" cellpadding="4" style="border: 1px solid #C0C0C0;">'."\n";
      //$html .= "<form name=\"".$this->name."\" method=\"post\">";
      if ($this->header) $html .= $title;
      if (sizeof($this->rows) > 0) $html .= $this->get_rows(); else $html .= $this->get_empty();
      if ($this->header && $this->footer) $html .= $title;
      //$html .= "</form>";
      $html .= "</table>\n";
      return $html;
    }
}

class DataGrid {

    var $name;
    var $cols;
    var $data;
    var $header;
    var $footer;
    var $pages;
    var $pagenum;
    var $pagecount;
    var $pagelen;
    var $rowformat;
    var $linkformat;
    var $cannopages;
    var $arenopages;
    var $sortlinkformat;
    var $pagesontop;
		var $filter;

    function DataGrid() {
      $this->header = true;
      $this->footer = true;
      $this->pages = "";
      $this->name = "datagrid";
      $this->pagenum = 1;
      $this->pagecount = 1;
      $this->pagelen = 2;
      $this->linkformat = "";
      $this->cannopages = "";
      $this->arenopages = "";
      $this->sortlinkformat = "";
      $this->rowformat = "";
      $this->pagesontop = false;
    }

    function parse_format($format, $datarow, $name = ''){

      if($name)
      {
        $format = str_replace('{%_self%}', '{%' . $name . '%}', $format);
      }

      while (preg_match("/\[if (.*?)\](.*?)\[\/if\]/", $format, $regs)) {
       $name = $regs[1];
       $expr = preg_replace('/{%(.*?)%}/s', '\$datarow[\'\\1\']', $name);
       list($truepart, $falsepart) = explode('[else]', $regs[2]);

       $expr = 'if(' . $expr . ') $b = true; else $b = false;';
       eval($expr);
       if(!$b)
       {
         $res = $falsepart;
       }
       else
       {
         $res = $truepart;
       }

       $format = str_replace($regs[0], $res, $format);
      }

      while (preg_match("/{%([a-zA-Z0-9_]+)%}/", $format, $regs)) {
       $name = $regs[1];
       $regs[1] = "{%".$name."%}";
       $format = preg_replace('/'.$regs[1].'/', "$datarow[$name]", $format);
      }
      return $format;
    }

    function get_navbar() {
      $html  = "<table width=\"100%\" cellpadding=\"4\" cellspacing=\"1\" class=\"datagrid\">";
      $html .= "  <tr>";
      $html .= "    <td width=\"100%\" class=\"thead\">";
      $html .= "    ".$this->get_pages()."&nbsp;&nbsp;";
      $html .= "    </td>";
      $html .= "  </tr>";
      $html .= "</table>";
      return $html;
    }

    function get_pages() {
      $html = "";
      if($this->arenopages)
      {
        $html .= '<a href="'.str_replace('%d', '1', $this->linkformat).'">Show Pages</a>&nbsp; ';
      }else
      {
        $pages = ceil($this->pagecount/$this->pagelen);
        if($this->pagenum > 1)
          $html .= '<a href="'.str_replace('%d', $this->pagenum-1, $this->linkformat).'">&laquo;</a>&nbsp; ';
        else $html .= '&laquo;&nbsp; ';

        for ($i=1;$i<=$pages;$i++) {
          if ($i <> $this->pagenum)
            $html .= '<a href="'.str_replace('%d', $i, $this->linkformat).'">'.$i.'</a>&nbsp; ';
          else
            $html .= '<b>'.$i.'</b>&nbsp; ';
        }
        if($this->cannopages)
        {
          $html .= '<a href="'.str_replace('%d', 'none', $this->linkformat).'">No Pages</a>&nbsp; ';
        }
        if($this->pagenum < $pages)
          $html .= '<a href="'.str_replace('%d', $this->pagenum+1, $this->linkformat).'">&raquo;</a>&nbsp; ';
        else $html .= '&raquo;&nbsp; ';
      }
      return $html;
    }

    function output() {
      $table = new DataTable();
      $table->cols = $this->cols;
      $table->header = $this->header;
      $table->footer = $this->footer;
      $table->name = $this->name;
			if($this->table_id) $table->id = $this->table_id;
      $table->sortlinkformat = $this->sortlinkformat;
      // if column title not set, use property title if possible
      foreach(array_keys($this->cols) as $name) {
        if (!isset($this->cols[$name]['title'])) $this->cols[$name]['title'] = $name;
      }
      if($this->data != null)
      {
        foreach(array_keys($this->data) as $i) {
          $row[] = $this->parse_format($this->rowformat, $this->data[$i]);
          foreach(array_keys($this->cols) as $name) {
            if (isset($this->cols[$name]["maxlength"]))
            {
              $this->data[$i][$name] = ShortenString($this->data[$i][$name], $this->cols[$name]["maxlength"]);
            }

            if (isset($this->cols[$name]["format"]))
            {
              $row[] = $this->parse_format($this->cols[$name]["format"], $this->data[$i], $name);
            }else
              $row[] = $this->data[$i][$name];

          }

          $table->add_row($row);
          unset($row);
        }
      }
      if (($this->pagecount != 1)or($this->arenopages)) {
        $navbar1 = '';//$this->get_navbar()."<br>";
        $navbar2 = $this->get_navbar();
      } else {
        $navbar1 = "";
        $navbar2 = "";
      }

      if($this->filter){
      	$filter = $this->add_filter();
      }else{
      	$filter = '';
      }

      if($this->pagesontop)
        return $navbar2.$navbar1.$table->output().$filter;
      else if($this->pagesontopandbottom)
        return $navbar2.$navbar1.$table->output().$navbar2.$filter;
      else
        return $navbar1.$table->output().$navbar2.$filter;
    }

		function add_filter(){

    	if(!$this->table_id){
        echo "Can't add filter to datagrid: no table id";
				die();
    	}

			ob_start();
      ?>
      	<script type='text/javascript' src='/cms/backend/gui/tablefilter.js'></script>
 			 	<link rel="stylesheet" type="text/css" href="/cms/backend/gui/filtergrid.css">

			  <script type='text/javascript'>
			    $(document).ready(function(){

			      setFilterGrid("<?=$this->table_id?>");

			    });
			  </script>
			<?
			return ob_get_clean();

		}

}


