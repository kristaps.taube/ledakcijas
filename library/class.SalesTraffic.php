<?php

use Constructor\WebApp;

class SalesTraffic
{

    private $service_address = 'https://traffic.sales.lv/API:0.14/';
    private $api_key;

    public function __construct($api_key)
    {

        $this->api_key = $api_key;

    }

    public function sendOne($params = [])
    {

        if($this->validateParams($params)){

            $params['APIKey'] = $this->api_key;
            $params['Command'] = 'SendOne';

            $this->log('Request: '.json_encode($params));

            $response = $this->makeAPICall($params);

            $this->log("Response: ".json_encode($response));

            return $response;

        }

    }

    public function makeAPICall($params)
    {

        if(!isset($params['APIKey'])){
            $params['APIKey'] = $this->api_key;
        }

        $fields = http_build_query($params);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $this->service_address);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = json_decode(curl_exec($ch), true);

        curl_close ($ch);

        return $response;

    }

    private function validateParams($params)
    {

        $required = ['Number', 'Sender', 'Content'];

        $valid = true;

        foreach($required as $r){

            if(!isset($params[$r]) || !$params[$r]){
                $valid = false;
                break;
            }

        }

        return $valid;

    }

    private function log($data)
    {

        $path = WebApp::$app->getConfigValue('log/directory').'sms.log';

        file_put_contents($path, date("d.m.Y H:i:s ").': '.$data.PHP_EOL, FILE_APPEND);

    }

}