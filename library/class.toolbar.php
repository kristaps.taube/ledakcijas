<?

#===================================================================================#
#  Copyright � 2002 Karlis Blumentals. All Rights Reserved.                         #
#  e-mail: kblums@latnet.lv                                                         #
#===================================================================================#

// CLASSES

class Toolbar {

  var $buttons;
  var $backendroot;
  var $cellpadding;
  var $prefix;

  function Toolbar() {
    $this->buttons = array();
    $this->cellpadding = 5;
  }

  function AddLink($id, $title, $picture, $link, $hint="", $alert="", $small = false) {
    $button['title'] = $title;
    $button['picture'] = $picture;
    $button['link'] = $link;
    $button['action'] = '';
    $button['hint'] = $hint;
    $button['alert'] = $alert;
    $button['small'] = $small;
    $this->buttons[$id] = $button;
  }

  function AddButton($id, $title, $picture, $action, $hint="", $alert="", $small = false) {
    $button['title'] = $title;
    $button['picture'] = $picture;
    $button['link'] = '';
    $button['action'] = $action;
    $button['hint'] = $hint;
    $button['alert'] = $alert;
    $button['small'] = $small;
    $this->buttons[$id] = $button;
  }

  function AddSeperator($width)
  {
    $button['width'] = $width;
    $button['seperator'] = true;
    $this->buttons[] = $button;
  }

  function _getButton($textid, $id, $title, $picture, $action, $hint) {
    if ($picture != '')
      $img .= '<img id="toolbarimage_'.$textid.'" src="'.$this->backendroot.'gui/toolbar/'.$picture.'" width="70" height="60" />';
    else
      $img .= '<img id="toolbarimage_'.$textid.'" src="'.$this->backendroot.'gui/toolbar/1x1.gif" width="70" height="60" />';
    $html .= '<td id="toolbarbutontd_'.$textid.'" width="70" OnMouseOver="toolbarOver('.$id.', \''.$hint.'\')" OnMouseOut="toolbarOut('.$id.')" OnMouseDown="toolbarDown('.$id.')" OnMouseUp="toolbarUp('.$id.')"  OnClick="'.$action.'" style="cursor: hand;">'.$img.'<br /><table cellpadding="0" cellspacing="0" border="0" width="100%"><tr><td id="'.$id.'" class="toolbarButton">'.$title.'</td></tr></table></td>';
    return $html;
  }

  function _getButtonSmall($textid, $id, $title, $picture, $action, $hint) {
    if ($picture != '')
      $img .= '<img id="toolbarimage_'.$textid.'" src="'.$this->backendroot.'gui/toolbar/'.$picture.'" width="40" height="40" alt="'.$hint.'" />';
    else
      $img .= '<img id="toolbarimage_'.$textid.'" src="'.$this->backendroot.'gui/toolbar/1x1.gif" width="40" height="40" />';
    $html .= '<td id="toolbarbutontd_'.$textid.'" width="40" OnMouseOver="toolbarOverSmall('.$id.', \''.$hint.'\')" OnMouseOut="toolbarOutSmall('.$id.')" OnMouseDown="toolbarDownSmall('.$id.')" OnMouseUp="toolbarUpSmall('.$id.')"  OnClick="'.$action.'" style="cursor: hand;">'.$img.'<br /><table cellpadding="0" cellspacing="0" border="0" width="100%"><tr><td id="'.$id.'" class="toolbarButtonSmall">'.$title.'</td></tr></table></td>';
    return $html;
  }

  function _getSeperator($width) {
    $img .= '<img src="'.$this->backendroot.'gui/toolbar/1x1.gif" width="'.$width.'" height="1" />';
    $html .= '<td width="'.$width.'">'.$img.'<br /><table cellpadding="0" cellspacing="0" border="0" width="100%"><tr><td class="toolbarButton"></td></tr></table></td>';
    return $html;
  }

  function _finalWrap($code) {

    $html .= '<table border="0" cellpadding="'.$this->cellpadding.'" cellspacing="1">'."\n";
    $html .= '  <tr>'."\n";
    $html .= $code;
    $html .= '  </tr>'."\n";
    $html .= '</table>'."\n";
    return $html;
  }

  function output() {
    $i = 0;
    $html = '';
    foreach($this->buttons as $key => $row) {
      if ($row['hint']== '') $row['hint'] = $row['title'];
      if ($row['action']!= '') $action = $row['action']; else $action = $row['link'];
      if ($row['alert']!= '') $action = "if (!confirm('".$row['alert']."')) return; ".$action;
      if($row['seperator'])
        $html .= $this->_getSeperator($row['width']);
      else if($row['small'])
        $html .= $this->_getButtonSmall($key, "tb_div_".$this->prefix.($i++), $row['title'], $row['picture'], $action, $row['hint']);
      else
        $html .= $this->_getButton($key, "tb_div_".$this->prefix.($i++), $row['title'], $row['picture'], $action, $row['hint']);
    }
    $html = $this->_finalWrap($html);
    return $html;
  }

}

?>
