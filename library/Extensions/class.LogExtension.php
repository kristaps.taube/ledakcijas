<?php

namespace Extensions;

use Constructor\Extension;
use Constructor\App;

class LogExtension extends Extension
{

    private $last_checkpoint_time;
    private $checkpoints = [];

    public function __construct($conf)
    {
        parent::__construct($conf);

        if($this->clear_log){
            file_put_contents($this->file, '');
        }

    }

    public function logCheckpoint($text)
    {

        $app_start = App::$app->getStartTime();

        if(!$this->last_checkpoint_time) $this->last_checkpoint_time = App::$app->getStartTime();

        $time = microtime(true);

        $start_diff = $time - $app_start;
        $checkpoint_diff = $time - $this->last_checkpoint_time;

        $checkpoint = [
            'text' => $text,
            'checkpoint_time' => $checkpoint_diff,
            'total_time' => $start_diff,
        ];

        $this->checkpoints[] = $checkpoint;

        if($this->output_checkpoints){
            echo "<pre>".print_r($checkpoint, true)."</pre>";
        }

        if($this->log_to_file){
            file_put_contents($this->file, print_r($checkpoint, true).PHP_EOL, FILE_APPEND);
        }

        $this->last_checkpoint_time = $time;

    }

    public function log($data)
    {




    }

}