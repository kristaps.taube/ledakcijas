<?php

namespace Extensions;

use Constructor\Extension;
use Constructor\WebApp;

class UserExtension extends Extension
{

    private $user;

    public function __construct($conf)
    {

        parent::__construct($conf);
        $this->user = WebApp::$app->session->get('logged_in');

    }

    public function authorizeUserEntry($user)
    {
        $this->user = $user;
        WebApp::$app->session->set('logged_in', $user);

        $buyers_info = isset($this->user['savedfields']) ? unserialize($this->user['savedfields']) : [];
        $buyers_info = is_array($buyers_info) ? $buyers_info : [];
        WebApp::$app->session->set('buyers_information', $buyers_info);

    }

    public function logout()
    {
        WebApp::$app->cart->clear();
        WebApp::$app->session->clear('buyers_information');
        WebApp::$app->session->clear('logged_in');
        $this->user = null;
    }

    public function get()
    {
        return $this->user;
    }

    public function updateInfo($fields)
    {

        if($this->user){
            $this->user['savedfields'] = serialize($fields); // update user
            WebApp::$app->session->set('logged_in', $this->user); // update session
            \shopusercollection::getInstance()->Update(['savedfields' => $this->user['savedfields']], $this->user['item_id']); // update db
        }

        WebApp::$app->session->set('buyers_information', $fields);

    }

}