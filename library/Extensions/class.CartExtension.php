<?php

namespace Extensions;

use Constructor\Extension;
use Constructor\App;

class CartExtension extends Extension
{

    private $session_key = 'cartitems';

    public function __construct($conf)
    {

        parent::__construct($conf);
        $this->manager = \shopmanager::getInstance(); // so we wouldn't rewrite functionality

    }

    public function getItems($use_cache = false)
    {

        static $cache;

        if($use_cache && $cache) return $cache;

        $items = [];

        $user = App::$app->user->get();
        $lang = App::$app->getLanguage();

        foreach(App::$app->session->get($this->session_key, []) as $key => $val){
            $citem = $this->getCartItem($val, $user, $lang, $key);
            if($citem){
                $items[] = $citem;
            }
        }

        $cache = $items;

        return $items;

    }

    public function getSum($use_cache = false)
    {

        $items = $this->getItems($use_cache);

        $sum = 0;

        foreach($items as $item){

            $sum += ($item['price'] * $item['count']);

        }

        return round($sum, 2);

    }

    private function getCartItem($item, $user, $lang, $key)
    {

        $product = isset($item['id']) ? \shopprodcollection::getInstance()->GetById($item['id']) : false;
        if(!$product || $product['disabled']) return false;

        $price_id = isset($item['price_id']) ? (int)$item['price_id'] : false;
        $additional_params = [];

        if($price_id){

            $price_row = $this->manager->pricescol->GetById($price_id);
            if($price_row) return false;

            $prices = $this->manager->getVolumePrice($price_row, $user, $item['count']);
            $price = $prices['price'];
            $base_price = $prices['base_price'];
            $additional_params[] = $price_row['name'];

        }else{

            $prices = $this->manager->getProductPrice($product, $user, $item['count']);
            $price = $prices['price'];
            $base_price = $prices['base_price'];

        }

        if(isset($item['color']) && $item['color']){
            $color = $this->manager->colors->getItemByIDAssoc($val['color']);
            if($color){
                $additional_params[] = $color['title_'.$lang];
            }
        }

        if(isset($item['size']) && $item['size']){
            $size = $this->manager->sizes->getItemByIDAssoc($item['size']);
            if($size){
                $additional_params[] = $size['title_'.$lang];
            }
        }

        return [
            'id' => $product['item_id'],
            'key' => $key,
            'name' => $product['name_' . $lang].(!empty($additional_params) ? ' ('.implode(", ",$additional_params).')' : ''),
            'count' => $item['count'],

            'price' => $price,
            'base_price' => $base_price,

            'price_id' => $price_id,
            'price_row' => $price_row,

            'cat' => $product['category_id'],
            'pvn' => $product['pvn'],
            'product' => $product,
            'unit' => $this->manager->unitsText($product, false, false),
            'color' => $color,
            'size' => $size,
        ];

    }

    public function addItem($additem, $count = 1)
    {

        $cart_items = App::$app->session->get($this->session_key, []);

        $found = false;
        foreach($cart_items as $key => $cartitem){
            unset($cartitem['count']);
            if($cartitem == $additem){

                $found = true;
                $cart_items[$key]['count'] += $count;
                $additem['count'] = $cart_items[$key]['count'];
                break;

            }
        }

        if(!$found){
            $additem['count'] = $count;
            $cart_items[] = $additem;
        }

        App::$app->session->set($this->session_key, $cart_items);
        App::$app->session->set('shop_last_cart_item', $additem);
        App::$app->session->setFlash('cart_item_added', true);

    }

    public function changeCount($key, $count)
    {

        $items = App::$app->session->get($this->session_key, []);
        if(isset($items[$key])){
            $items[$key]['count'] = $count;
            App::$app->session->set($this->session_key, $items);
        }

    }

    public function removeItemByKey($key)
    {

        $items = App::$app->session->get($this->session_key, []);

        $removed = false;
        if(isset($items[$key])){
            unset($items[$key]);
            App::$app->session->set($this->session_key, $items);
            $removed = true;
        }

        return $removed;

    }

    public function clear()
    {

        App::$app->session->clear($this->session_key);

    }


}