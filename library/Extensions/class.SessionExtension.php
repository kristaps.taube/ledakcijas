<?php

namespace Extensions;

use Constructor\Extension;

class SessionExtension extends Extension
{

    private $_session;
    private $_flash;
    private $_new_flash = [];
    private $_flash_message_key = 'constructor_flash_messages';

    public function __construct($conf)
    {

        parent::__construct($conf);
        $this->_session = isset($_SESSION[$this->session_key]) ? $_SESSION[$this->session_key] : [];
        $this->_flash = $this->get($this->_flash_message_key, []);
        $this->clear($this->_flash_message_key);

    }

    public function set($key, $value)
    {
        $_SESSION[$this->session_key][$key] = $this->_session[$key] = $value;
    }

    public function get($key, $default = null)
    {
        return isset($this->_session[$key]) ? $this->_session[$key] : $default;
    }

    public function clear($key)
    {

        if(isset($this->_session[$key])){
            unset($this->_session[$key]);
            $_SESSION[$this->session_key][$key]= null;
            unset($_SESSION[$this->session_key][$key]);
        }

    }

    public function setFlash($type, $message)
    {
        $this->_flash[$type] = $this->_new_flash[$type] = $message;
        $this->set($this->_flash_message_key, $this->_new_flash);
    }

    public function getFlash($type, $default = null)
    {
        return isset($this->_flash[$type]) ? $this->_flash[$type] : $default;
    }

    public function getFlashMessages()
    {
        return $this->_flash;
    }

}