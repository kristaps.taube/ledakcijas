<?php

namespace Extensions;

use Constructor\Extension;

class ShopManager extends Extension
{

    public function __construct($conf)
    {
        parent::__construct($conf);

        $this->manager = \shopmanager::getInstance(); // so we wouldn't rewrite functionality

    }

}