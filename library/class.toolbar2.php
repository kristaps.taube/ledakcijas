<?

// CLASSES

class Toolbar2 {

  var $toolbarHtml;
  var $backendroot;
  var $prefix;

  
  function Toolbar2() {
    $this->toolbarHtml = '';
  }

  function AddButtonText($text, $href){

    $this->outputHtml .= '<li>';
    $this->outputHtml .= '<a class="btntext" href="'.$href.'">'.$text.'</a>';
    $this->outputHtml .= '</li>';

  }

  function AddButtonImage($btnid, $image, $title="", $link="", $onclick="", $width = 30, $alert="", $confirm="", $target="_self") {

    if (strtolower(pathinfo($image, PATHINFO_EXTENSION)) == 'png')
      $image = $this->backendroot.'gui/img/navigation/' . $image;
    else
      $image = $this->backendroot.'gui/img/navigation/' . $image . '.gif';

    if (!$link) $link = "javascript:void('".$alert."');";

    if ($confirm) $onclick = "if (!confirm('".$confirm."')) return false; ".$onclick;

    $style       = "background: url(".$image.") no-repeat 5px 4px; width:".$width."px;";
    $onMouseOver = "this.className='btnimagehover';this.style.width='".($width-2)."px';"
                   . "this.style.backgroundPositionX = 4; this.style.backgroundPositionY = 3;"
                 ;//. "this.style.background=' #FFF url(".$image.") no-repeat 4px 3px';";
    $onMouseOut  = "this.className='btnimage';this.style.width='".$width."px';"
                    . "this.style.backgroundPositionX = 5; this.style.backgroundPositionY = 4;"
                 ;//. "this.style.background = 'background: url(".$image.") no-repeat 5px 4px'";

    $this->outputHtml .= '
    <li>';

    $this->outputHtml .= '<a id="toolbarimage_'.$btnid.'" class="btnimage" '
                                . 'href="'        . $link        . '" '
                                . 'title="'       . $title       . '" '
                                . 'style="'       . $style       . '" '
                                . 'onClick="'     . $onclick     . '" '
                                . 'target="'     . $target     . '" '
                                . 'onMouseOver="javascript:toolbarOver(\''.$alert.'\');' . $onMouseOver . '" '
                                . 'onMouseMove="javascript:toolbarOver(\''.$alert.'\');" '
                                . 'onMouseOut="javascript:toolbarOut();'  . $onMouseOut  . '" >'
                        .'</a>';

    $this->outputHtml .= '</li>';
  }


  function AddButtonImageText($btnid, $image, $text="button", $link="", $onclick="", $width = 50, $alert="", $confirm = "") {

    $image = ''.$this->backendroot.'gui/img/navigation/' . $image . '.gif';

    if (!$link) $link = "javascript:void('".$alert."');";

    if ($confirm) $onclick = "if (!confirm('".$confirm."')); return false; ".$onclick;

    $style       = "background: url(".$image.") no-repeat 5px 4px; width:".$width."px;";
    $onMouseOver = "this.className='btnimagetexthover';this.style.width='".($width-1)."px';"
                  . "this.style.backgroundPositionX = 4; this.style.backgroundPositionY = 4;"
                 ;//. "this.style.background=' #FFF url(".$image.") no-repeat 4px 4px';";
    $onMouseOut  = "this.className='btnimagetext';this.style.width='".$width."px';"
                 . "this.style.backgroundPositionX = 5; this.style.backgroundPositionY = 4;"
                 ;//. "this.style.background = 'background: url(".$image.") no-repeat 5px 4px'";

    $this->outputHtml .= '<li id="btnimagetext">';

    $this->outputHtml .= '<a id="toolbarimage_'.$btnid.'" class="btnimagetext" '
                                . 'href="'        . $link        . '" '
                                . 'title="'       . $text        . '" '
                                . 'style="'       . $style       . '" '
                                . 'onClick="'     . $onclick     . '" '
                                . 'onMouseOver="javascript:' . $onMouseOver . '" '
                                . 'onMouseMove="javascript:toolbarOver(\''.$alert.'\');" '
                                . 'onMouseOut="javascript:toolbarOut();'  . $onMouseOut  . '" >'
                       . $text.'</a>';

    $this->outputHtml .= '</li>';
  }

  
  function AddButtonImageMenu($btnid, $image, $title="", $width = 40, $submenu="", $alert="", $confirm="") {

    $image = ''.$this->backendroot.'gui/img/navigation/' . $image . '.gif';

    //generate submenu id
    $submenu_id = "sub_".rand();

    $link = "javascript:void('".$alert."');";

    $onclick = "if (document.getElementById('".$submenu_id."').style.visibility=='visible') document.getElementById('".$submenu_id."').style.visibility='hidden'; else { document.getElementById('".$submenu_id."').style.visibility='visible';document.getElementById('".$submenu_id."').style.marginTop='18px';}";

    $style       = "background: url(".$image.") no-repeat 5px 4px; width:".$width."px;";
    $onMouseOver = "this.className='btnimagehover';this.style.width='".($width-2)."px';"
                  . "this.style.backgroundPositionX = 4; this.style.backgroundPositionY = 3;"
                 ;//. "this.style.background=' #FFF url(".$image.") no-repeat 4px 3px';";
    $onMouseOut  = "this.className='btnimage';this.style.width='".$width."px';"
                  . "this.style.backgroundPositionX = 5; this.style.backgroundPositionY = 4;"
                 ;//. "this.style.background = 'background: url(".$image.") no-repeat 5px 4px'; if (window.event.toElement.id != '".$submenu_id."') hideSubMenu(window.event, document.getElementById('".$submenu_id."'), false);";

    $this->outputHtml .= '<li>';

    $this->outputHtml .= '<a id="toolbarimage_'.$btnid.'" class="btnimage" '
                                . 'href="'        . $link        . '" '
                                . 'title="'       . $title       . '" '
                                . 'style="'       . $style       . '" '
                                . 'onClick="'     . $onclick     . '" '
                                . 'onMouseOver="javascript:toolbarOver(\''.$alert.'\');' . $onMouseOver . '" '
                                . 'onMouseMove="javascript:toolbarOver(\''.$alert.'\');" '
                                . 'onMouseOut="javascript:toolbarOut();'  . $onMouseOut  . '" >'
                        .'</a>';

    $this->outputHtml .= '
    <script>
        function hideSubMenu(e, obj, check)
        {
            if (check)
            {
            if (!e) var e = window.event;
            var tg = (window.event) ? e.srcElement : e.target;
            if (tg.nodeName != \'DIV\') return;
            var reltg = (e.relatedTarget) ? e.relatedTarget : e.toElement;
            while (reltg != tg && reltg.nodeName != \'BODY\')
                reltg= reltg.parentNode
            if (reltg== tg) return;
            }
            // Mouseout took place when mouse actually left layer
            // Handle event
            window.setTimeout(obj.id+".style.visibility = \'hidden\';"+obj.id+".style.marginTop=\'18px\';", 100);
        } 
    </script>
    ';

    $this->outputHtml .= '<div id="'.$submenu_id.'" style="margin-top:18px;padding-right: 5px; padding-bottom: 5px;padding-top:5px; padding-left:5px; width: 150px; line-height:20px;background:#FFF; position: absolute; visibility: hidden; z-index: 1; white-space: nowrap; border: 1px #003366; border-style: solid solid solid solid;" onMouseOut="hideSubMenu(window.event, this, true);" onMouseOver="this.style.marginTop=\'20px\';">';
    
    foreach ($submenu as $key=>$row)
    {
        if (!$row['link']) $row['link'] = "javascript:void(0);";

        if ($row['confirm']) $row['onclick'] = "if (!confirm('".$row['confirm']."')); return false; ".$row['onclick'];

        $row['style']       = "background: url(".$row['image'].") no-repeat; background-position: left;padding-left: 20px;";

        $row['onclick']     .= $submenu_id.'.style.visibility=\'hidden\';';

        $this->outputHtml .= '<a '
                                    . 'href="'        . $row['link']        . '" '
                                    . 'title="'       . $row['text']        . '" '
                                    . 'style="'       . $row['style']       . '" '
                                    . 'onClick="'     . $row['onclick']     . '" '
                                    . 'onMouseOver="' . $row['onMouseOver'] . '" '
                                    . 'onMouseOut="'  . $row['onMouseOut']  . '" >'
                           . $row['text'].'</a><br />';
    }

    $this->outputHtml .= '</div>';

    $this->outputHtml .= '</li>';
  }

  
  function AddButtonImageTextMenu($id, $title, $picture, $link, $hint="", $alert="", $small = false) {
    //not implemented yet
  }

  
  function AddSeperator()
  {
    $this->outputHtml .= '<li><img alt="" src="'.$this->backendroot.'gui/img/navigation/hmNavSplitter.gif" /></li>';
  }


  function AddSpacer()
  {
    $this->outputHtml .= '<li>&nbsp;</li>';
  }


  function output()
  {
    $this->outputHtml = '<div id="hmNavWrapper"><ul>' . $this->outputHtml . '</ul></div>';
    return $this->outputHtml;
  }


}

?>
