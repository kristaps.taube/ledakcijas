<?php

function parseVariables($body){

  //Process megaIF tags
  //while (ereg("{%%if:([a-zA-Z0-9_:-]+):([a-zA-Z0-9_:-]+)%%}(.+){%%endif%%}", $body, $regs))
  while (preg_match("/{%%if([0-9]*?):([a-zA-Z0-9_:-]*?):([a-zA-Z0-9_:-]*?)%%}(.*?){%%endif\\1%%}/s", $body, $regs)){

    $branches = explode('{%%else'.$regs[1].'%%}', $regs[4]);
            
    if($GLOBALS[$regs[2]]==$regs[3]){
      $body = str_replace($regs[0], $branches[0], $body);
    }else{
      $body = str_replace($regs[0], $branches[1], $body);
    }

  }

  //language strings before other variables
  processLanguageStrings($body);

  $body = str_replace("{%CMS_VERSION%}", CMS_VERSION, $body);

  $oldbody = $body;
  $tags = Array();
  $tagpos = Array();

  while (preg_match("/{%([^%]+)%}/s", $body, $regs)){
    $tags[] = Array($regs[0], $regs[1]);
    $body = preg_replace($regs[0], '', $body);
  }

  $body = $oldbody;
  //Obtain the positions of all tags
  foreach($tags as $tagrow){
    list($tag, $tagname) = $tagrow;
    $p = 0;
    while(is_integer($p=strpos($body, $tag, $p))){
      $tagpos[] = Array($p,$tag,$tagname);
      $p++;
    }
  }

  usort($tagpos, 'cmp');
  //Replace the tags
  $body = "";
  $last = strlen($oldbody);
  for($f = count($tagpos)-1; $f >= 0; $f--){
    $tag = $tagpos[$f][1];
    $tagname = $tagpos[$f][2];
    $p = $tagpos[$f][0] + strlen($tag);
    $output = outputText($tagname);
    $body = $output . substr($oldbody, $p, $last - $p) . $body;
    $last = $p-strlen($tag);
  }
  $body = substr($oldbody, 0, $last ) . $body;

  //again for the second time
  processLanguageStrings($body);

  return $body;

}

//For sorting the array
function cmp ($a, $b) {
  if ($a[0] == $b[0]) return 0;
  return ($a[0] < $b[0]) ? -1 : 1;
}

function ExecScript($name) {

	$script = $GLOBALS['docScripts'][$name];
	$html_ret = '';
	if(is_array($script)){
		$activetab = intval($_GET['tab']);
		$html_ret .= '<div class="tabs">';
		$i = 0;
		$g = $_GET;
		foreach ($script as $label => $filename){
			$g['tab'] = $i;
			$url = '?'.http_build_query($g);
			$act = ($activetab == $i) ? ' class="active"' : '';
			$html_ret .= '<a href="'.$url.'"'.$act.'>'.$label.'</a>';
			$i++;
		}
		$html_ret .= '</div>';

		$keys = array_keys($script);
		$script = $script[$keys[$activetab]];
	}

	$filename = ($name=="body" && $_GET['cmodule']==1) ? "custommodules/".$script : "modules/".$script;

	if ($script) {
		ob_start();
		require($GLOBALS['cfgDirRoot']."/backend/".$filename);
		$html_ret .= ob_get_contents();
		ob_end_clean();
		return $html_ret;
	} else {
		return '';
	}

}

function outputText($name){

	$params = explode(":", $name);
	switch ($params[0]) {
		case "title": $html = $GLOBALS['docTitle']; break;
		case "date": $html = date("d/m/Y"); break;
		case "scr": $html = ExecScript($params[1]); break;
		case "str": $html = $GLOBALS['docStrings'][$params[1]]; break;
		case "usr":
			if($params[1] == "login") $html = $_SESSION["session_user"];
			if($params[1] == "ip") $html = $_SERVER['REMOTE_ADDR'];
			if($params[1] == "host") $html = gethostbyaddr($_SERVER['REMOTE_ADDR']);
			break;
		case "glo": $html = $GLOBALS[$params[1]]; break;
		case "ifstr":
			if($GLOBALS[$params[1]]==$params[2]){
				$html = $params[3];
			}else{
				$html = $params[4];
			}
			break;
		case "const":
			eval('$out='.$params[1].";");
			$html = $out;
			break;
		default: $html = ""; break;
	}
	return $html;

}

function processLanguageStrings(&$body){
	$langstrs = array();
	$langstrjs = array();

  static $defaults;

	preg_match_all ('/{%(langstr|langstrjs):([a-zA-Z0-9_]*?)%}/s', $body, $matches, PREG_SET_ORDER);
	foreach($matches as $key => $row){
		$langstrs[] = "'" . $row[2] . "'";
		if($row[1] == 'langstrjs') $langstrjs[] = $row[2];
	}

	if(!$defaults){

  	$default_data = DB::GetTable('
			SELECT name, phrase FROM phrases
			LEFT JOIN phrasenames ON phrasename=phrasename_id
			WHERE lang = ""
		');

		foreach($default_data as $row){
    	$defaults[$row['name']] = $row['phrase'];
		}

	}

	$langstrs = join(',', $langstrs);
	if($langstrs){
		$data = DB::GetTable('
			SELECT name, phrase FROM phrases
			LEFT JOIN phrasenames ON phrasename=phrasename_id
			WHERE
				lang = :lang AND
				name IN (' . $langstrs . ')
		', array(":lang" => CMS_LANG));
	}

	$langstrs = array();
	if($data){
		foreach($data as $row){
			if(in_array($row['name'], $langstrjs)){
				$row['phrase'] = mb_convert_encoding($row['phrase'], $GLOBALS['maincharset'], 'HTML-ENTITIES');
				$langstrs[$row['name']] = parseForJScript($row['phrase'], true);
			}else{
				$langstrs[$row['name']] = $row['phrase'];
			}
		}
	}

	foreach($matches as $key => $row){
		if($langstrs[$row[2]]){
    	$body = str_replace($row[0], $langstrs[$row[2]], $body);
		}else{
      $body = str_replace($row[0], $defaults[$row[2]], $body);
		}

	}

}
