<?
//function for creating tar archives with PEAR class Tar.php
//Created by Aleksandrs Selickis, 2004, alex@datateks.lv

//------------------------------------------------------------------//
//funkcijas izsakukuma piemeers
/*
$archive_name = "../../d3bak.tgz";

$files_to_compress[0]="package.xml";
$files_to_compress[1]="Archive_Tar-1.1/"; //(Optional '/' at the end)
$files_to_compress[2]="unc.files.php";

tar_archive($archive_name, $files_to_compress);
*/

function check_date($val,$modified)
{
	$mod_time = filemtime($val);
	if ($mod_time-$modified>0) return true;
	else return false;
}

function check_size($val,$maxsize)
{
	$file_size=filesize($val);
	if ($maxsize-$file_size>0) return true;
	else return false;
}

function get_list(&$v_list, $val, $modified = "", $maxsize = "", $exceptdir = "")
{
  $exceptdir = str_replace('\\', '/', $exceptdir);
  if ($handle = opendir($val))
	{
		while (false !== ($file = readdir($handle)))
		{
			if ($file != "." && $file != "..")
			{
        $curdir = getcwd().'/'.$val.'/'.$file.'/';
        $curdir = str_replace('\\', '/', $curdir);

        if ($curdir != $exceptdir)
        {
				  if (is_dir($val.'/'.$file))
					{
						//echo "DIR: ".$val." | CUR DIR: ".$file."<br>";
						//NESAGLABA tukshas direktorijas
						//$v_list[]=$val.$file;

            get_list($v_list, $val.'/'.$file, $modified, $maxsize, $exceptdir);
						//$v_list = array_merge($v_list,$sublist);
  				}
				  else
					{
						//echo "DIR: ".$val." | CUR FILE: ".$file."<br>";
						$v_list[]=$val."/".$file;
  				}
        }
			}
		}
  	closedir($handle);
	}

}

function tar_archive($archive_name, $files_to_compress, $modified = "", $maxsize = "", $exceptdir = "")
{
        require_once('Tar.php');
        $tar_object = new Archive_Tar($archive_name, 'gz');

		//paarbauda funkcijai doto masiivu, ja ir fails tad paarbuda nosaciijumus
		//ja ir direktorija, tad atrod visus subdirektorijas un failus no taas, saglaba rezultatu masivaa $list
        foreach ($files_to_compress as $key=>$val)
        {
          if (is_file($val))
					{
						if ($modified!="") $date = check_date($val,$modified);
						else $date=true;

						if ($maxsize!="") $size = check_size($val,$maxsize);
						else $size=true;

						if ($size && $date) $v_list[] = $val;
					}
				if  (is_dir($val))
					{
					  $sublist = Array();
            get_list($sublist, $val,$modified,$maxsize,$exceptdir);
						$list[] = $sublist;
					}

		}

		//paarbauda nosaciijumus failiem kuri bija atrasti funkcijai dotajaas direktorijaas
		foreach ($list as $key=>$val)
			foreach ($val as $id=>$name)
			{
          if (is_file($name))
					{
						if ($modified!="") $date = check_date($name,$modified);
						else $date=true;

						if ($maxsize!="") $size = check_size($name,$maxsize);
						else $size=true;

						if ($size && $date) $v_list[] = $name;
					}
				if  (is_dir($name))
					{
						//echo $val."<br>";
						$v_list[] = $name;
					}
				//$v_list[]=$name;
			}

        if ($tar_object->create($v_list)==true) echo "";//action on success
        else echo "";//action on error
		//$tar_object->createModify($v_list,"", "backup/");


		print_r($list);
		foreach ($list as $key=>$val)
			foreach ($val as $id=>$name)
				echo $name."<br>";


}



//------------------------------------------------------------------//
//funkcijas izsaukuma piemeers
/*
$archive_name = "d3bak.tgz";
$path_to_extract="insatll";
tar_extract($archive, $path_to_extract);
*/



function tar_extract($archive_name, $path_to_extract)
{
        require_once('Tar.php');
		$tar_object = new Archive_Tar($archive_name);
		$tar_object->extract($path_to_extract);
}

//------------------------------------------------------------------//


/*
$date=strtotime("18 May 2004");

$archive_name = "d3bak.tgz";

$files_to_compress[0]="package.xml";
$files_to_compress[1]="Archive_Tar-1.1/"; //(Optional '/' at the end)
$files_to_compress[2]="func.files.php";

//tar_archive($archive_name, $files_to_compress,$date);
tar_archive($archive_name, $files_to_compress);
*/

?>
