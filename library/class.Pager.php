<?php
/**
*
*  Title: Pager
*  Author: Kristaps Taube <ktaube@datateks.lv>
*  Date: 12.02.2018
*
*  Usage: Pager::output(['count' => $count, 'page_size' => $this->page_size, 'page' => $this->page, 'url_params' => $_GET, 'count_text' => $this->l('[from] - [to] no [total] rakstiem')]);
*
*/

class Pager
{

    public static function output($options = [])
    {

        $id = isset($options['id']) ? $options['id'] : '';
        $class = isset($options['class']) ? $options['class'] : '';
        $count = $options['count'];
        $page_size = $options['page_size'];
        $page = $options['page'];
        $get_params = isset($options['url_params']) ? $options['url_params'] : $_GET;
        $page_param = isset($options['page_param']) ? $options['page_param'] : 'p';
        $count_text = isset($options['count_text']) ? $options['count_text'] : '';

        $page_count = ceil($count / $page_size);
        if($page_count <= 1) return;

        if($count_text){
            $text = $count_text;
            $text = str_replace('[from]', ($page-1) * $page_size+1, $text);

            $to = ($page-1) * $page_size + $page_size;
            $to = $to <= $count ? $to : $count;

            $text = str_replace('[to]', $to, $text);
            $text = str_replace('[total]', $count, $text);
        }

        $start_size = isset($options['start_size']) ? $options['start_size'] : 3;
        $mid_size = isset($options['mid_size']) ? $options['mid_size'] : 3;
        $end_size = isset($options['end_size']) ? $options['end_size'] : 3;

        $group1 = array("start" => 1, "end" => min($start_size, $page_count)); // start

        $group2 = $group3 = false;

        if($page_count > $start_size){
            $group2 = array("start" => $page - ceil((($mid_size - 1) / 2)), "end" => $page + ceil(($mid_size - 1) / 2)); // m
        }

        if($page_count > $start_size + $mid_size){
            $group3 = array("start" => $page_count-$end_size+1, "end" => $page_count); // end
        }

        if($group2){
            if($group2['start'] <= $group1['end']) $group2['start'] = $group1['end'] + 1;
            if($group3){
                if($group2['end'] >= $group3['start']) $group2['end'] = $group3['start'] - 1;
            }
        }

        $url_params = [];
        foreach($get_params as $key => $value){
            if($key == $page_param) continue;
            $url_params[$key] = $value;
        }
        $url = !empty($url_params) ? http_build_query($url_params)."&" : '';
        ?>
        <div class="pager <?=($class ? $class : '')?>" <?=($id ? 'id="'.$id.'"' : '')?>>
            <?php if($count_text){ ?>
            <p class="count"><?=$text?></p>
            <?php } ?>
            <? if($page > 1){ ?>
            <a href="?<?=$url?><?=$page_param?>=<?=($page-1)?>" class="page prev">&lt;</a>
            <? } ?>
            <? if($page_count >= 10){ ?>

                <? #START ?>
                <? for($i = $group1['start']; $i <= $group1['end']; $i++ ){ ?>
                    <a href='?<?=$url?><?=$page_param?>=<?=$i?>' class='page <?=($page == $i)? "active" : ""?>'><?=$i?></a>
                <? } ?>

                <? #MIDDLE ?>
                <? if($group2){ ?>
                    <? if($group2['start'] - $group1['end'] > 1){ ?>
                    <p class='page'>...</p>
                    <? } ?>

                    <? for($i = $group2['start']; $i <= $group2['end']; $i++ ){ ?>
                    <a href='?<?=$url?><?=$page_param?>=<?=$i?>' class='page <?=($page == $i)? "active" : ""?>'><?=$i?></a>
                    <? } ?>

                    <? if($group3['start'] - $group2['end'] > 1){ ?>
                    <p class='page'>...</p>
                    <? } ?>
                <? } ?>

                <? #END ?>
                <? if($group3){ ?>
                    <? for($i = $group3['start']; $i <= $group3['end']; $i++ ){ ?>
                    <a href='?<?=$url?><?=$page_param?>=<?=$i?>' class='page <?=($page == $i)? "active" : ""?>'><?=$i?></a>
                    <? } ?>
                <? } ?>

            <? }else{ ?>
                <? for($i = 1; $i <= $page_count; $i++){ ?>
                <a href='?<?=$url?><?=$page_param?>=<?=$i?>' class='page <?=($page == $i)? "active" : ""?>'><?=$i?></a>
                <? } ?>
            <? } ?>

            <? if($page < $page_count){ ?>
            <a href="?<?=$url?><?=$page_param?>=<?=($page+1)?>" class="page next">&gt;</a>
            <? } ?>

        </div>
        <?

    }

}