<?

class ComponentToolbar {

  var $images;
  var $JS;
  var $height = 16;
  var $width = 16;

  function Toolbar() {
    $this->images = array();
  }


  function AddImage($id, $picture, $JScode, $alt="", $alert="", $closeblock = 0) {
    $image['onclickname'] = $id.'_onclick()';
    $image['picture'] = $picture;
    $image['alt'] = $alt;
    $image['alert'] = $alert;
    $image['close'] = $closeblock;
    $this->images[$id] = $image;
    $this->JS .= $this->_addJSfunction($image['onclickname'],$JScode);
  }

  function addCombo($id, $optionsStr, $JScode, $style="", $closeblock = 0) {
    $combo['onchangename'] = $id.'_onchange()';
    $options = explode(":",$optionsStr);
    foreach ($options as $option)
        $combo['options'] .= "<option value=\"".$option."\">".$option."</option>";
    $combo['style'] = $style;
    $combo['close'] = $closeblock;
    $this->images[$id] = $combo;
    $this->JS .= $this->_addJSfunction($combo['onchangename'],$JScode);    
  }

  function _addJSfunction($onclickname, $JScode) {
    //$JS .= "\n";
    $JS .= ' function '.$onclickname . "\n";
    $JS .= ' {' ."\n";
    $JS .= $JScode;
    $JS .= '}' ."\n";
    $o = substr($onclickname, 0, strlen($onclickname) - 2);
    $JS .= 'window.'.$o.'='.$o.';'."\n";
    return $JS;
    }

  function _getImgage($btnnum, $picture, $action, $alt) {
    $img .= '<td><span OnClick="'.$action.'" class=button id="'.$btnnum.'" onmouseout=out(this.id) onmouseup=out(this.id) onmousedown=dn(this.id) onmouseover=up(this.id) alt="'.$alt.'" title="'.$alt.'">';
    $img .= '<img src="'.$GLOBALS['cfgWebRoot'].'gui/tbimages/'.$picture.'" width="'.$this->width.'" height="'.$this->height.'" hspace=1 vspace=1 border=0 alt="'.$alt.'" title="'.$alt.'" />';
    $img .= '</span></td>';
    return $img;
  }

  function _getCombo($key, $options, $onchangename, $style) {
    $combo .= '<td><select id="'.$key.'" style="'.$style.'" onchange="return '.$onchangename.'">';
    $combo .= $options;
    $combo .= '</select></td>';
    return $combo;
  }

  function _getDivider()
    {
        $div =  '<td width="1" bgcolor="#808080"></td><td width="1" bgcolor="#FFFFFF"></td>';
        return $div;
    }

  function _finalWrap($code) {

    $html .= '<table height=26 border=0 cellpadding=0 cellspacing=0>';
    $html .= '  <tr>';
    $html .= $code;
    $html .= '  </tr>';
    $html .= '</table>';
    return $html;
  }

  function outputJS() {
    //$htmlJS .= '<script>';
    $htmlJS .= $this->JS;
    //$htmlJS .= '</script>';
    return $htmlJS;
  }

  function getButtonIdList()
  {
    $ids = Array();
    foreach($this->images as $id => $image)
    {
      $ids[] = $id;
    }
    return $ids;
  }

  function outputHTML() {
    $html = '';
    foreach($this->images as $key => $row) {
      if ($row['alert']!= '') $action = "if (!confirm('".$row['alert']."')); return; ".$row['onclickname'];
      else $action = $row['onclickname'];

      if (isset($row['picture']))
        $html .= $this->_getImgage($key, $row['picture'], $action, $row['alt']);
      else if (isset($row['options']))
        $html .= $this->_getCombo($key, $row['options'], $row['onchangename'], $row['style']);

      while ($row['close'])
      {
        $html .= $this->_getDivider();
        $row['close']--;
      }
    }
    $html = $this->_finalWrap($html);
    return $html;
  }

}

?>
