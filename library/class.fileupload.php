<?
/*
Error codes:
        0 - "No file was uploaded"
        1 - "Maximum file size exceeded"
        2 - "Maximum image size exceeded"
        3 - "Only specified file type may be uploaded"
        4 - "File already exists" (save only)
*/

error_reporting(5);

class uploader {

        var $file;
        var $errors;
        var $accepted;
        var $new_file;
        var $max_filesize;
        var $max_image_width;
        var $max_image_height;
        var $saved_filename;

        function max_filesize($size){
                $this->max_filesize = $size;
        }

        function max_image_size($width, $height){
                $this->max_image_width = $width;
                $this->max_image_height = $height;
        }

        function upload($filename, $accept_type, $extention) {
                // get all the properties of the file; register_globals = on;
                /*$index = array("file", "name", "size", "type");
                for($i = 0; $i < 4; $i++) {
                        $file_var = '$' . $filename . (($index[$i] != "file") ? "_" . $index[$i] : "");
                        eval('global ' . $file_var . ';');
                        eval('$this->file[$index[$i]] = ' . $file_var . ';');
                }*/
                
                // get all the properties of the file; register_globals = off;
                $this->file['file'] = $_FILES[$filename]['tmp_name'];
                $this->file['name'] = $_FILES[$filename]['name'];
                $this->file['size'] = $_FILES[$filename]['size'];
                $this->file['type'] = $_FILES[$filename]['type'];

                if($this->file["file"] && $this->file["file"] != "none") {
                        //test max size
                        if($this->max_filesize && $this->file["size"] > $this->max_filesize) {
                                $this->errors[1] = "Maximum file size exceeded. File may be no larger than " . $this->max_filesize/1000 . "KB.";
                                return False;
                        }
                        if(preg_match("/image/", $this->file["type"])) {
                                $image = getimagesize($this->file["file"]);
                                $this->file["width"] = $image[0];
                                $this->file["height"] = $image[1];

                                // test max image size
                                if(($this->max_image_width || $this->max_image_height) && (($this->file["width"] > $this->max_image_width) || ($this->file["height"] > $this->max_image_height))) {
                                        $this->errors[2] = "Maximum image size exceeded. Image may be no more than " . $this->max_image_width . " x " . $this->max_image_height . " pixels";
                                        return False;
                                }
                                switch($image[2]) {
                                        case 1:
                                                $this->file["extention"] = ".gif";
                                                break;
                                        case 2:
                                                $this->file["extention"] = ".jpg";
                                                break;
                                        case 3:
                                                $this->file["extention"] = ".png";
                                                break;
                                        default:
                                                $this->file["extention"] = $extention;
                                                break;
                                }
                        }
                        else if(!preg_match("/(\.)([a-z0-9]{3,5})$/", $this->file["name"]) && !$extention) {
                                // add new mime types here
                                switch($this->file["type"]) {
                                        case "text/plain":
                                                $this->file["extention"] = ".txt";
                                                break;
                                        default:
                                                break;
                                }
                        }
                        else {
                        	preg_match("/(\.)([a-z0-9]{3,5})$/", $this->file["name"], $matches);
                        	$this->file["extention"] = $matches[0];
                        }

                        // check to see if the file is of type specified
                        if($accept_type) {
                                if(preg_match("/".$this->file["type"]."/", $accept_type)) { $this->accepted = True; }
                                else { $this->errors[3] = "Only " . preg_replace("/\|/", " or ", $accept_type) . " files may be uploaded"; }
                        }
                        else { $this->accepted = True; }
                }
                else { $this->errors[0] = "No file was uploaded"; }
                return $this->accepted;
        }

        function save_file($path, $mode){
                global $NEW_NAME;

                if($this->accepted) {
                        // very strict naming of file.. only lowercase letters, numbers and underscores
                        $new_name = preg_replace ("/[^a-z0-9._]/", "", preg_replace ("/ /", "_", preg_replace("/%20/", "_", strtolower($this->file["name"]))));

                        // check for extention and remove
                        if(preg_match("/(\.)([a-z0-9]{3,5})$/", $new_name)) {
                                $pos = strrpos($new_name, ".");
                                if(!$this->file["extention"]) { $this->file["extention"] = substr($new_name, $pos, strlen($new_name)); }
                                $new_name = substr($new_name, 0, $pos);
                                
                        }
                        $this->new_file = $path . $new_name . $this->file["extention"];
                        $NEW_NAME = $new_name . $this->file["extention"];

                        switch($mode) {
                                case 1: // overwrite mode
                                        $aok = true;
                                        if (file_exists($this->new_file))
                                          $aok = @unlink($this->new_file);

                                        if ($aok)
                                        {
                                          $aok = move_uploaded_file($this->file["file"], $this->new_file);
                                          if ($aok)
                                            chmod($this->new_file, 0644);

                                        }


                                        $this->saved_filename = $new_name . $this->file["extention"];
                                        break;
                                case 2: // create new with incremental extention
                                        while(file_exists($path . $new_name . $copy . $this->file["extention"])) {
                                                $copy = "" . $n;
                                                $n++;
                                        }
                                        $this->new_file = $path . $new_name . $copy . $this->file["extention"];
                                        $this->saved_filename = $new_name . $copy . $this->file["extention"];
                                        $aok = move_uploaded_file($this->file["file"], $this->new_file);
                                        if ($aok)
                                          chmod($this->new_file, 0644);

                                        break;
                                case 3: // do nothing if exists, highest protection
                                        if(file_exists($this->new_file)){
                                                $this->errors[4] = "File &quot" . $this->new_file . "&quot already exists";
                                        }
                                        else {
                                                $aok = move_uploaded_file($this->file["file"], $this->new_file);
                                                if ($aok)
                                                  chmod($this->new_file, 0644);

                                                $this->saved_filename = $new_name . $this->file["extention"];
                                        }
                                        break;
                                default:
                                        break;
                        }
                        if(!$aok) { unset($this->new_file); }
                        return $aok;
                }
        }
}
?>
