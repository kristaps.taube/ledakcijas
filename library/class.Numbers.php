<?php

use Constructor\WebApp;

class Numbers
{

    public static function sayNumber($number, $lang, $major, $majors, $minor, $minors, $zerominors)
    {
        $number = round($number, 2);

        $words = self::getNumberWords($lang);

        // majors to words
        $result = self::say(floor($number), $words);

        # major
        if (floor($number-floor($number/10)*10) > 1){ // last diggit is > 1
            $result .= " ".$majors;
        }elseif($number % 10 == 0){ // last diggit is 0
            $result .= " ".$majors;
        }else{
            $result .= " ".$major;
        }

        $result .= " ".WebApp::l('un', ['lang' => $lang])." ";

        // minors to words
        $minor_number = ($number - floor($number)) * 100;
        $result .= self::say(floor($minor_number), $words);

        # minor
        if (floor($number-floor($number/10)*10) > 1){ // last diggit is > 1
            $result .= " ".$minors;
        }elseif($number % 10 == 0){ // last diggit is 0
            $result .= " ".$minor;
        }else{
            $result .= " ".$zerominors;
        }

        return $result;

    }

    private static function say($num, $words)
    {
        if ( ($tmp=floor($num/1000000000))>=1 ) return self::say($tmp, $words).' '.$words['1000000000'.($tmp>1?'x':'')].' '.self::say($num-$tmp*1000000000, $words);
        if ( ($tmp=floor($num/1000000))>=1 ) return self::say($tmp, $words).' '.$words['1000000'.($tmp>1?'x':'')].' '.self::say($num-$tmp*1000000, $words);
        if ( ($tmp=floor($num/1000))>=1 ) return self::say($tmp, $words).' '.$words['1000'.($tmp>1?'x':'')].' '.self::say($num-$tmp*1000, $words);
        if ( ($tmp=floor($num/100))>=1 ) return self::say($tmp, $words).' '.$words['100'.($tmp>1?'x':'')].' '.self::say($num-$tmp*100, $words);
        if ( ($tmp=floor($num/10))>=2 ) if (isset($words[$num])) return $words[$num]; else return $words[$tmp*10].' '.self::say($num-$tmp*10, $words);
        if ( $num>=0 ) return $words[$num];
    }

    private static function getNumberWords($lang)
    {

        $arr = [
            WebApp::l('nulle', ['lang' => $lang]),
            WebApp::l('viens', ['lang' => $lang]),
            WebApp::l('divi', ['lang' => $lang]),
            WebApp::l('trīs', ['lang' => $lang]),
            WebApp::l('četri', ['lang' => $lang]),
            WebApp::l('pieci', ['lang' => $lang]),
            WebApp::l('seši', ['lang' => $lang]),
            WebApp::l('septiņi', ['lang' => $lang]),
            WebApp::l('astoņi', ['lang' => $lang]),
            WebApp::l('deviņi', ['lang' => $lang]),
            WebApp::l('desmit', ['lang' => $lang]),
            WebApp::l('vienpadsmit', ['lang' => $lang]),
            WebApp::l('divpadsmit', ['lang' => $lang]),
            WebApp::l('trīspadsmit', ['lang' => $lang]),
            WebApp::l('četrpadsmit', ['lang' => $lang]),
            WebApp::l('piecpadsmit', ['lang' => $lang]),
            WebApp::l('sešpadsmit', ['lang' => $lang]),
            WebApp::l('septiņpadsmit', ['lang' => $lang]),
            WebApp::l('astoņpadsmit', ['lang' => $lang]),
            WebApp::l('deviņpadsmit', ['lang' => $lang]),
            20 => WebApp::l('divdesmit', ['lang' => $lang]),
            30 => WebApp::l('trīsdesmit', ['lang' => $lang]),
            40 => WebApp::l('četrdesmit', ['lang' => $lang]),
            50 => WebApp::l('piecdesmit', ['lang' => $lang]),
            60 => WebApp::l('sešdesmit', ['lang' => $lang]),
            70 => WebApp::l('septiņdesmit', ['lang' => $lang]),
            80 => WebApp::l('astoņdesmit', ['lang' => $lang]),
            90 => WebApp::l('deviņdesmit', ['lang' => $lang]),
            '100' => WebApp::l('simts', ['lang' => $lang]),
            '100x' => WebApp::l('simti', ['lang' => $lang]),
            '1000' => WebApp::l('tūkstotis', ['lang' => $lang]),
            '1000x' => WebApp::l('tūkstoši', ['lang' => $lang]),
            '1000000' => WebApp::l('miljons', ['lang' => $lang]),
            '1000000x' => WebApp::l('miljoni', ['lang' => $lang]),
            '10000000' => WebApp::l('miljards', ['lang' => $lang]),
            '10000000x' => WebApp::l('miljardi', ['lang' => $lang]),
        ];

        return $arr;

    }

}