<?
  function CheckPermissionByID($userID, $perm_num, $data, $data2)
  {
    $adddata = '';
    if($data)
      $adddata .= " AND data='$data'";
    if($data2)
      $adddata .= " AND data2='$data2'";
    $val = sqlQueryValue("SELECT perm_id FROM permissions WHERE perm_num=$perm_num AND isuser=1 AND id=$userID" . $adddata);
    if($val != null)
      return true;  //User has the permission
    $groups = sqlQueryColumn("SELECT group_id FROM users_groups WHERE user_id=$userID");
    if($groups != null)
    {
      $groups = join(" OR id=", $groups);
      $val = sqlQueryValue("SELECT perm_id FROM permissions WHERE perm_num=$perm_num AND isuser=0 AND (id=$groups)" . $adddata);
      if($val != null)
      {
        return true; //User's group has the permission
      }
    }
    return false;
  }

  function CheckPermission($perm_num, $data = '', $data2 = '')
  {
    return CheckPermissionByID($GLOBALS['currentUserID'], $perm_num, $data, $data2);
  }

  function HasComponentPermission($site_id, $template_id, $component_name)
  {
    $userID = $GLOBALS['currentUserID'];
    $row = sqlQueryRow("SELECT * FROM permissions WHERE perm_num=14 AND isuser=1 AND id=$userID AND data=$site_id AND data2=$template_id");
    $extradata = $row['extradata'];
      $groups = sqlQueryColumn("SELECT group_id FROM users_groups WHERE user_id=$userID");
      if($groups != null)
      {
        foreach($groups as $g)
        {
          $row = sqlQueryRow("SELECT * FROM permissions WHERE perm_num=14 AND isuser=0 AND id=$g AND data=$site_id AND data2=$template_id");
          if($row['extradata'])
          {
            if($extradata)
            {
              $extradata .= ',';
            }
            $extradata .= $row['extradata'];
          }
        }
      }
      if(!$extradata)
      {
        return true;
      }else
      {
        $allowed = explode(',', $extradata);
        return in_array($component_name, $allowed);
      }
  }

  function HasComponentPermission2($site_id, $page_id, $component_name)
  {
    $userID = $GLOBALS['currentUserID'];
    $row = sqlQueryRow("SELECT * FROM permissions WHERE perm_num=16 AND isuser=1 AND id=$userID AND data=$site_id AND data2=$page_id");
    $extradata = $row['extradata'];
      $groups = sqlQueryColumn("SELECT group_id FROM users_groups WHERE user_id=$userID");
      if($groups != null)
      {
        foreach($groups as $g)
        {
          $row = sqlQueryRow("SELECT * FROM permissions WHERE perm_num=16 AND isuser=0 AND id=$g AND data=$site_id AND data2=$page_id");
          if($row['extradata'])
          {
            if($extradata)
            {
              $extradata .= ',';
            }
            $extradata .= $row['extradata'];
          }
        }
      }
      if(!$extradata)
      {
        return true;
      }else
      {
        $allowed = explode(',', $extradata);
        return in_array($component_name, $allowed);
      }
  }

  function HasComponentPermissionPage($site_id, $page_id, $component_name)
  {
    $template_id = sqlQueryValue("SELECT template FROM " . $site_id . "_pages WHERE page_id='$page_id'");
    return (HasComponentPermission($site_id, $template_id, $component_name) && HasComponentPermission2($site_id, $page_id, $component_name));
  }

  //Check if user has any of the permissions specified in the parameter array
  function CheckGlobalPermissions($a)
  {
    foreach($a as $num)
    {
      if(CheckPermission($num))
        return true;
    }
    return false;
  }

  function CheckSitePermissions($a, $siteID)
  {
    global $site_id;
    foreach($a as $num)
    {
      if((CheckPermission($num, $siteID))and($site_id == $siteID))
        return true;
    }
    return false;
  }

  function AddGroupPermission($groupID, $permNum, $data, $data2)
  {
    sqlQuery("INSERT INTO permissions (perm_num, isuser, id, data, data2) VALUES ($permNum, 0, $groupID, $data, $data2)");
  }

  function EchoBoo()
  {
    echo '
          function boooooo()
          {
            FillFromArray();
            FillPagesFromArray(5);
            //fill checked components if possible
            try
            {
              for(f=0;f<compocheckboxes.length;f++)
              {
                compocheckboxes[f].checked = false;
              }
              a = compochecked[parentframes("usergroups.Select_1").value];
              for(f=0;f<a.length;f++)
              {
                parentframes("permlist.check_component_" + a[f]).checked = true;
              }
              for(f=0;f<compocheckboxes.length;f++)
              {
                compocheckboxes[f].onclick();
              }
              parentframes("usergroups.Select_1").disabled=false;

            }catch(e) {}
          }
          function CompCheckHandler(PermID, ChkID, doAdd, ExtraData)
          {
            ChangePerms(PermID,ChkID'.($GLOBALS['site_id']>0?','.$GLOBALS['site_id']:'').');
            ChangeExtraData(doAdd, PermID, ExtraData);
          }
          try {
//          parentframes("usergroups.Select_1").onchange = boooooo;
          } catch (e) {}
          /*]]>*/
          </script>
          ';

  }

  function OutputPermProc($perm_type, $site_id, $data, $data2)
  {
      switch($perm_type)
      {
        /*
        case 2:
          $pages = sqlQueryValue("SELECT page_id FROM " . $data . "_pages WHERE parent=" . $data2);
          if($pages)
          {
            return '<p align="right"><input type="checkbox" name="subpages" value="subpages" /> Apply to subpages</p><br />';
          }
          break;
        */
        case (2 || 3 || 4 || 5):
          $specified_comp = 14;
          $n = 1;
          if ($perm_type==2)
          {
              $specified_comp = 16;
              $n = 2;
              /*
              $pages = sqlQueryValue("SELECT page_id FROM " . $data . "_pages WHERE parent=" . $data2);
              if($pages)
              {
                echo '<p align="right"><input type="checkbox" name="subpages" value="subpages" /> Apply to subpages</p><br />';
              }
              */
          }
          if ($perm_type==4)
          {
              $specified_comp = 22;
              $n = 2;
          }
          if ($perm_type==5)
          {
              $specified_comp = 26;
              $n = 2;
          }
          echo '
          <script language="JavaScript" type="text/javascript">
          /*<![CDATA[*/
          ';

          $a = array();
          if ($data2 != "")
            $crows = sqlQueryData('SELECT * FROM permissions WHERE perm_num='.$specified_comp.' AND extradata<>"" AND data2='.$data2);
          else
            $crows = sqlQueryData('SELECT * FROM permissions WHERE perm_num='.$specified_comp.' AND extradata<>""');
          foreach($crows as $row)
          {
            if($row['isuser'])
              $s = 'u';
            else
              $s = 'g';
            $s .= $row['id'];
            $a[$s] = $row['extradata'];
          }
          //echo '</script>';
          //print_R($a);
          //echo '<script>';
          echo ' var compochecked = new Array();';
          foreach($a as $key => $val)
          {
            $s = explode(',', $val);
            echo ' compochecked["'.$key.'"] = new Array(';
            $comps = '';
            foreach($s as $ss)
            {
              if($comps) $comps .= ',';
              $comps .= '"'.$ss.'"';
            }
            echo $comps;
            echo ');';
          }
          EchoBoo();
          //get component list in template
          if ($perm_type==2) $body =sqlQueryValue("SELECT body FROM ".$data."_templates as t, ".$data."_pages as p where t.template_id=p.template and p.page_id=".$data2);
          if ($perm_type==3) $body = sqlQueryValue("SELECT body FROM " . $data . "_templates WHERE template_id=$data2");
          if ($perm_type==4) $body = sqlQueryValue("SELECT body FROM ".$data."_waptemplates as t, ".$data."_wappagesdev as p where t.template_id=p.template and p.wappagedev_id=".$data2);
          if ($perm_type==5) $body = sqlQueryValue("SELECT body FROM " . $data . "_waptemplates WHERE template_id=$data2");

          while (preg_match("/\{%template:([a-zA-Z0-9 _]+)%\}/U", $body, $regs))
          {
            if ($perm_type==2 || $perm_type==3)
                $body = ereg_replace($regs[0], getBody($regs[1],$site_id,0), $body);
            if ($perm_type==4 || $perm_type==5)
                $body = ereg_replace($regs[0], getBody($regs[1],$site_id,1), $body);
          }

          $components = componentArray($body, 0);
          $s = '<div style="padding-left: 110px">';
          foreach($components as $c)
          {
            $s .= '<input type="checkbox" id="check_component_'.$c['name'].'" name="check_component_'.$c['name'].'"  onClick="javascript:CompCheckHandler('.$specified_comp.', '.$n.', check_component_'.$c['name'].'.checked, \''.$c['name'].'\')" /> ('.$c['type'].') '.$c['name'].'<br />';
          }
          $s.= '
          <script language="JavaScript" type="text/javascript">
          /*<![CDATA[*/
          var compocheckboxes = new Array();
          ';
          foreach($components as $c)
          {
            $s.= '  compocheckboxes[compocheckboxes.length] = parentframes("permlist.check_component_'.$c['name'].'");';
          }
          $s.= '
          boooooo();
          /*]]>*/
          </script>
          ';
          $s .= '</div>';
          return $s;
        break;
        default:
        break;
      }
  }

  function BeforeDeletePermission($num, $isuser, $id, $data, $data2)
  {
    switch($num)
    {
      case 11:
      case 13:
        //page permissions
        //check if "subpage" is checked and act accordingly
        if($_POST['subpages'])
        {
          //data = site_id, data2 = page_id
          //obtain all subpages for this page
          $pages = sqlQueryColumn("SELECT page_id FROM " . $data . "_pages WHERE parent=" . $data2);
          foreach($pages as $page)
          {
            BeforeDeletePermission($num, $isuser, $id, $data, $page);
            sqlQuery("DELETE FROM permissions WHERE perm_num=$num AND isuser=" .
                     IIF($isuser,1,0) . " AND id=$id AND data=$data AND data2=$page");
          }
        }
        break;
      default:
      break;
    }
  }

  function AfterAddPermission($perm_id, $num, $isuser, $id, $data, $data2)
  {
    switch($num)
    {
      case 11:
      case 13:
        if($_POST['subpages'])
        {
          $pages = sqlQueryColumn("SELECT page_id FROM " . $data . "_pages WHERE parent=" . $data2);
          foreach($pages as $page)
          {
            sqlQuery("INSERT INTO permissions (perm_num, isuser, id, data, data2) VALUES ($num, ".IIF($isuser,1,0).", $id, $data, $page)");
            $where = 'WHERE perm_num="' . $num . '"';
            if($isuser)
              $where .= ' AND isuser=1';
            else
              $where .= ' AND isuser=0';
            if($data2)
              $where .= ' AND data2=\'' . $page . '\'';
            if($data)
              $where .= ' AND data=\'' . $data . '\'';
            $where .= ' AND id="' . $id . '"';
            $p = sqlQueryValue("SELECT perm_id FROM permissions " . $where);
            AfterAddPermission($p, $num, $isuser, $id, $data, $page);
          }
        }
        break;
        case 22:
        case 16:
        case 14:
        case 26:
          //go through all permchanges, create extradata from it
          if($isuser)
            $u = 'u';
          else
            $u = 'g';
          $u .= $id;
          $components = Array();
          $commands = split(';', $_POST['permchanges']);
          foreach($commands as $command)
          {
            if($command)
            {
              $s = split(':', $command);
              if($s[3] == $u)
              {
                if($s[0] == '*')
                  $components[$s[4]] = true;
                if($s[0] == '/')
                  unset($components[$s[4]]);
              }
            }
          }
          $s = '';
          foreach($components as $component => $val)
          {
            if($s) $s .= ',';
            $s .= $component;
          }
          sqlQuery("UPDATE permissions SET extradata='$s' WHERE perm_id=$perm_id");
          //echo "UPDATE permissions SET extradata='$s' WHERE perm_id=$perm_id";
        break;
      default:
        break;
    }
  }

function MakeGlobalPermVars()
{
  if((!isset($_SESSION['user_site_permissions']))and($_GET['site_id'])and($GLOBALS['currentUserID']))
  {
    include($GLOBALS['cfgDirRoot'] . "backend/modules/permissions.texts.php");
    foreach($permissions_list as $key => $p)
    {
      if($p['type'] == 1)
      {
        $_SESSION['user_site_permissions'][$key] = CheckPermission($key, $_GET['site_id']);
      }
    }
  }
  $GLOBALS['user_site_permissions_modifysite'] = $_SESSION['user_site_permissions'][6];
  $GLOBALS['user_site_permissions_managepages'] = $_SESSION['user_site_permissions'][8];
  $GLOBALS['user_site_permissions_managetemplates'] = $_SESSION['user_site_permissions'][9];
  $GLOBALS['user_site_permissions_managewappages'] = $_SESSION['user_site_permissions'][19];
  $GLOBALS['user_site_permissions_managewaptemplates'] = $_SESSION['user_site_permissions'][23];
  $GLOBALS['user_site_permissions_managefiles'] = $_SESSION['user_site_permissions'][7];
  $GLOBALS['user_site_permissions_manageusers'] = $_SESSION['user_site_permissions'][10];
  $GLOBALS['user_site_permissions_managecollections'] = $_SESSION['user_site_permissions'][17];
  $GLOBALS['user_site_permissions_managelists'] = $_SESSION['user_site_permissions'][66];
  $GLOBALS['user_site_permissions_managedocuments'] = $_SESSION['user_site_permissions'][28];
  $GLOBALS['user_site_permissions_admin'] = $_SESSION['user_site_permissions'][18];
  $GLOBALS['user_site_permissions_stats'] = $_SESSION['user_site_permissions'][34];

}






?>
