<?

class CrossDB{

  private $connection;

  function __construct($db_name, $conf = false){

    $this->db_name = $db_name;

    $c = $this->connect($db_name, $conf);

    return $c ? $this : false;

  }

  // select synchronization db
  function connect($db_name, $conf = false){

    $host = (isset($conf['host']))? $conf['host'] : $GLOBALS['cfgDBHost'];
    $username = (isset($conf['username']))? $conf['username'] : $GLOBALS['cfgDBUser'];
    $password = (isset($conf['password']))? $conf['password'] : $GLOBALS['cfgDBPass'];

    $this->connection = mysql_connect($host, $username, $password, true);
    mysql_select_db($db_name, $this->connection);
    mysql_query("SET CHARACTER SET 'utf8'", $this->connection);
    mysql_set_charset('utf8', $this->connection);

    return $this->connection ? true : false;

  }

  public function get_table($query){

    $arr = array();
    $q = mysql_query($query, $this->connection) or print "MySQL Error";

    while($result = mysql_fetch_array($q, MYSQL_ASSOC)){
      $arr[] = $result;
    }

    return $arr;
  }

  public function get_row($query){

    $q = mysql_query($query, $this->connection);
    $return = mysql_fetch_array($q, MYSQL_ASSOC);

    return is_array($return)? $return : false;

  }

  public function query($query){

    mysql_query($query, $this->connection);

  }

  public function get_value($query){

    $q = mysql_query($query, $this->connection);
    $return = mysql_fetch_array($q);

    return is_array($return)? $return[0] : false;

  }

  public function insert($where, $what){

    $values = Array();
    $fields = Array();
    foreach($what as $key => $value){
      $fields[] = $key;
      $values[] = "'".mysql_real_escape_string($value, $this->connection)."'";
    }

    $query = "INSERT INTO ".$where."(`".implode("`,`",$fields)."`) VALUES(".implode(",",$values).")";

    mysql_query($query, $this->connection) or die(mysql_error());
    $id = mysql_insert_id($this->connection);

    return $id;
  }

  public function update($table, $id, $fields){

      $upd = Array();
      foreach($fields as $key => $value){
        $value = stripslashes($value);
        $upd[] = "`".$key."`="."'".mysql_real_escape_string($value, $this->connection)."'";
      }

      $where = Array();
      if(is_array($id)){
        foreach($id as $key => $value){
          $where[] = "`".$key."`="."'".mysql_real_escape_string($value, $this->connection)."'";
        }
      }else{
        $where[] = "`id`=".$id;
      }

      $query = "UPDATE ".$table." SET ".implode(",", $upd)." WHERE ".implode(" AND ", $where);

      mysql_query($query, $this->connection);

      $affected_rows = mysql_affected_rows($this->connection);

      return $affected_rows;

    }

}

?>