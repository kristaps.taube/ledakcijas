<?

function convertVideo($site_id, $url, $saveurl, $width=320, $height=240, $delsource=true)
{
  require_once($GLOBALS['cfgDirRoot']."library/"."func.files.php");

  if (pathinfo($url, PATHINFO_EXTENSION) == 'flv') return $url;
  
  $file = getFilePathFromLink($site_id, $url);
  #$name = pathinfo($file, PATHINFO_FILENAME);

  $name = basename($file);
  $d=explode('.',$name);
  unset($d[count($d)-1]);
  $name=implode($d);

  $ext = '.'.pathinfo($file, PATHINFO_EXTENSION);

  forceDirectories($site_id, $saveurl);

  $flvfile = $saveurl.'/'.$name.'.flv';
  $flvfile_path = getFilePathFromLink($site_id, $flvfile);
  if ($flvfile_path == $file) return $flvfile;

  $urltotake = 'http://'.$_SERVER["SERVER_NAME"].$url;
  $uploaddir = getFilePathFromLink($site_id, $saveurl);
  $dir = sqlQueryValue("select dirroot from sites where site_id=".$site_id);
  $file_to_del = $delsource ? $file : '';

  forceDirectories($site_id, $uploaddir);

  $script_location = 'http://'.$_SERVER["SERVER_NAME"].'/cms/backend/tools/media_2_flv.php';

  $sock = fsockopen ("db.datateks.lv", 80, $errno, $errstr, 30);
  if (!$sock)
  {
    echo("Cannot create socket. Error = $errno($errstr)");
    return;
  }
  else
  {
    $out .= "GET /avi_2_flv_converter/scripts/catchmediafile.php?url=".urlencode($urltotake)."&filename=".$name."&dstpath=".urlencode($saveurl)."&servpath=".urlencode($uploaddir)."&scr_loc=".urlencode($script_location)."&w=".$width."&h=".$height."&file_url=".$file."&ext=".$ext."&dir=".urlencode($dir)."&delme=".$file_to_del."&sid=".$site_id." HTTP/1.1\r\n";
    $out .= "Host: db.datateks.lv\r\n";
    $out .= "Accept: text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5\r\n";
    $out .= "Accept-Language: en-us,en;q=0.5\r\n";
    $out .= "Accept-Encoding: gzip,deflate\r\n";
    $out .= "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7\r\n";
    $out .= "Keep-Alive: 300\r\n";
    $out .= "Connection: keep-alive\r\n\r\n";
    fputs ($sock, $out);
  }
  fclose ($sock);
  /// send request for converting to flv // end

  return $flvfile;
}

?>