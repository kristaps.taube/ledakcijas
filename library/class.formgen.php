<?

#===================================================================================#
#  Copyright © 2002 Karlis Blumentals. All Rights Reserved.                         #
#  e-mail: kblums@latnet.lv                                                         #
#===================================================================================#


/* InputControl generates code for input box */

class InputControl {

    var $name;
    var $property;
    var $fname;      // form name (used to access control via javascript)
    var $custom;     // custom code added to input tag
    var $customstyle;
    var $allowwysiwyg = true;
    var $activatefirstcontrol = true;
    var $firstcontrol;

    function InputControl() {
      $this->custom = "";
    }

    function fieldCollection()
    {
			$style = isset($this->property['style']) ? $this->property['style'] : "";

      $html = "<select style=\"" . $this->customstyle.$style;
      if (isset($this->property['width'])) $html .= 'width: '.$this->property['width'].';';
      $html .= '"';
      $html .= " name=\"".$this->name."\" id=\"".$this->name."\" size=\"1\" class=\"formEdit\"";
      $html .= $this->custom.">\n";
      foreach($this->property['lookup'] as $row) {
        $option = explode(':', $row);
        $value = array_slice($option, 1);
        $value = implode('', $value);
        ($option[0] == $this->property['value']) ? $selected = "selected" : $selected = "";
        $html .= "  <option value=\"".$option[0]."\" $selected>".$value."</option>\n";
      }
      $html .= "</select>";
      $html .= '&nbsp;<input class="formButton smallButton" type="button" value="..."';
      if (!isset($this->property['dialogw'])) $w = 400; else $w = $this->property['dialogw'];
      if (!isset($this->property['dialogh'])) $h = 450; else $h = $this->property['dialogh'];
      $html .= " OnClick=\"

                           var newname = '';
                           if(".$this->name.".value==0)
                           {
                             var newname=prompt('{%langstr:enter_collection_name%}:', '');
                           }

                           if((".$this->name.".value!=0)||((newname!='')&&(newname!=null)))
                           {
                             var args = new Array(document.".$this->fname.".".$this->name.".value, window, ".$this->name.");

                             openDlg('".$this->property['dialog']."'+".$this->name.".value+'&newname='+newname+'&coltype=".$this->property['collectiontype']."', args, {w:".$w.", h:".$h."}, function(val){

                             	if((val)||(val=='0')) document.Form.".$this->name.".value=val;
                             	try{ document.Form.check_".$this->name.".checked=false; UnDefaultControl(document.Form.".$this->name."); } catch (e) {}

														 });

                           } \">";
      if(!$this->firstcontrol) $this->firstcontrol = $this->name;
      return $html;
    }

    function fieldColor(){

      $this->property['value'] = $this->property['value'] ? $this->property['value'] : "#ffffff";

      ob_start();
      ?>
        <script type="text/javascript" src="/cms/backend/gui/colorpicker/js/colorpicker.js"></script>
        <link rel="stylesheet" type="text/css" href="/cms/backend/gui/colorpicker/css/colorpicker.css" />
        <div style="width:50px;height: 50px;" class='colorSelector_<?=$this->name?>'><div style="width:50px;height: 50px;background-color: <?=htmlspecialchars($this->property['value'])?>"></div></div>
        <input type='hidden' class='formEdit colorpicker_<?=$this->name?>' name='<?=$this->name?>' value='<?=htmlspecialchars($this->property['value'])?>' />
        <script>
        $('.colorSelector_<?=$this->name?>').ColorPicker({
        	color: '#0000ff',
        	onShow: function (colpkr) {
        		$(colpkr).fadeIn(500);
        		return false;
        	},
        	onHide: function (colpkr) {
        		$(colpkr).fadeOut(500);
        		return false;
        	},
        	onChange: function (hsb, hex, rgb) {
        		$('.colorSelector_<?=$this->name?> div').css('backgroundColor', '#' + hex);
            $("input[name='<?=$this->name?>']").val('#' + hex);
        	}
        });
        </script>
      <?
      $html = ob_get_contents();
      ob_end_clean();
      return $html;

    }

    function fieldHidden(){
      $html .= '<input type="hidden" name="'.$this->name.'" id="'.$this->name.'"';
      if (isset($this->property['value'])) $html .= ' value="'.htmlspecialchars($this->property['value']).'"';
      $html .= $this->custom.'>';
      return $html;
    }

    function fieldList(){
      $html .= "<select style=\"" . $this->customstyle;
      if (isset($this->property['width'])) $html .= 'width: '.$this->property['width'].';';
      $html .= '"';
      $html .= " name=\"".$this->name."\" id=\"".$this->name."\" size=\"1\" class=\"formEdit\"";
      $html .= $this->custom.">\n";
      $found = false;
      if($this->property['lookup'])
      {
        foreach($this->property['lookup'] as $row) {
          $option = explode(':', $row);
          $value = array_slice($option, 1);
          $value = implode(':', $value);
          if($option[0] == $this->property['value'])
            $found = true;
          ($option[0] == $this->property['value']) ? $selected = "selected" : $selected = "";

          $html .= "  <option value=\"".$option[0]."\" $selected>".$value."</option>\n";

        }
      }
      if($this->property['lookupassoc'])
      {
        foreach($this->property['lookupassoc'] as $key => $value) {
          if($key == $this->property['value'])
            $found = true;
          ($key == $this->property['value']) ? $selected = "selected" : $selected = "";

          $html .= "  <option value=\"".$key."\" $selected>".$value."</option>\n";

        }
      }
      if($this->property['value'] && !$found)
      {
        $html .= "  <option value=\"".$this->property['value']."\" selected>&lt;Other: ".$this->property['value']."&gt;</option>\n";
      }
      $html .= "</select>";
      if($this->property['onclick'])
      {
        $html .= '&nbsp;<input class="formButton smallButton" type="button" value="..."';
        if (!isset($this->property['dialogw'])) $w = 400; else $w = $this->property['dialogw'];
        if (!isset($this->property['dialogh'])) $h = 450; else $h = $this->property['dialogh'];
        $html .= " OnClick=\"".$this->property['onclick']."\">";
      }
      if($this->property['combo'])
      {
        $html .= "
        <script>
        var converted_".$this->name." = new Ext.form.ComboBox({
          typeAhead: true,
          triggerAction: 'all',
          transform:'".$this->name."',
          forceSelection:false
        });

        converted_".$this->name.".on('specialkey', function(thiscombo) {
          Form.".$this->name.".value = thiscombo.getRawValue();
        });
        converted_".$this->name.".on('blur', function(thiscombo) {
          Form.".$this->name.".value = thiscombo.getRawValue();
        });

        </script>
        ";

        $this->extrajs = "Form.".$this->name.".value = converted_".$this->name.".getRawValue();";
      }
      if(!$this->firstcontrol) $this->firstcontrol = $this->name;
      return $html;
    }

    function fieldActionList(){
      $html .= "<select style=\"" . $this->customstyle;
      if (isset($this->property['width'])) $html .= 'width: '.$this->property['width'].';';
      $html .= '"';
      $html .= " name=\"".$this->name."\" id=\"".$this->name."\" size=\"1\" class=\"formEdit\" OnChange=\"".$this->property['action']."\">\n";
      foreach($this->property['lookup'] as $row) {
        $option = explode(':', $row);
        ($option[0] == $this->property['value']) ? $selected = "selected" : $selected = "";
        if ($option[1]=='{%langstr')
        {
          $html .= "  <option value=\"".$option[0]."\" $selected>".$option[1].':'.$option[2]."</option>\n";
        }
        else
        {
          $html .= "  <option value=\"".$option[0]."\" $selected>".$option[1]."</option>\n";
        }
      }
      $html .= "</select>";
      if(!$this->firstcontrol) $this->firstcontrol = $this->name;
      return $html;
    }

    function fieldFile(){
      $hasvalue = (bool)$this->property['value'];
      if(!$this->property['uploadonly'])
      {
//        $html .= '<hr class="formRadioGroupHr">';
        $html .= '<input type="hidden" name="'.$this->name.'" value=""/><input type="radio" name="'.$this->name.'_file_select_mode" id="'.$this->name.'_file_select_mode_upload" value="upload" '.(!$hasvalue?' checked="checked"':'').' onclick="document.getElementById(\''.$this->name.'_file_div_select\').style.display=\'none\';document.getElementById(\''.$this->name.'_file_div_upload\').style.display=\'block\';"/><label for="'.$this->name.'_file_select_mode_upload">Upload new file</label><br />';
        $html .= '<input type="radio" name="'.$this->name.'_file_select_mode" id="'.$this->name.'_file_select_mode_select" value="select" '.($hasvalue?' checked="checked"':'').' onclick="document.getElementById(\''.$this->name.'_file_div_upload\').style.display=\'none\';document.getElementById(\''.$this->name.'_file_div_select\').style.display=\'block\';"/><label for="'.$this->name.'_file_select_mode_select">Select from server</label>';
        $html .= '<br />';
      }else
      {
        $hasvalue = false;
      }
      $html .= '<div style="float:left">';
      $html .= '<div id="'.$this->name.'_file_div_upload"'.($hasvalue?' style="display: none;"':'').'>';
      $html .= '<input class="formEdit" type="file"';
      $html .= ' name="'.$this->name.'" id="'.$this->name.'"';
      if (isset($this->property['size'])) $html .= ' size="'.$this->property['size'].'"';
      $html .= $this->custom.'>';
      $html .= '<input type="hidden" name="MAX_FILE_SIZE" value="100000000">';
      if(!$this->firstcontrol) $this->firstcontrol = $this->name;
      $html .= '</div>';
      if(!$this->property['uploadonly'])
      {
        $html .= '<div id="'.$this->name.'_file_div_select"'.(!$hasvalue?' style="display: none;"':'').'>';
        if(!$this->property['dialog'])
          $this->property['dialog'] = "?module=dialogs&action=filex&site_id=".$GLOBALS['site_id']."&view=thumbs";
        $name = $this->name;
        $this->name .= '_link';
        $html .= $this->fieldStringDialog();
        $this->name = $name;
        $html .= '</div>';
      }
      $html .= '</div>';
      return $html;
    }

    function fieldButton(){
      $html .= '<input class="formEdit" type="button"';
      $html .= ' name="'.$this->name.'_btn" id="'.$this->name.'_btn"';
      $html .= " style=\"" . $this->customstyle;
      if (isset($this->property['width'])) $html .= 'width: '.$this->property['width'].';';
      $html .= '"';
      $html .= $this->custom.' value="'.$this->property['text'].'" onClick="'.$this->property['onClick'].'">';
      $html .= '<input type="hidden" name="'.$this->name.'" id="'.$this->name.'" value="'.$this->property['value'].'">';
      if(!$this->firstcontrol) $this->firstcontrol = $this->name;
      return $html;
    }

    function fieldBoolean(){
      $html .= '<input class="formEdit" type="checkbox"';
      $html .= ' name="'.$this->name.'" id="'.$this->name.'"';
      $html .= " style=\"" . $this->customstyle;
      if (isset($this->property['width'])) $html .= 'width: '.$this->property['width'].';';
      $html .= '"';
      if ($this->property['value']=="1"||$this->property['value']=="on") $html .= ' checked="checked"';
      $html .= $this->custom.'>';
      if(!$this->firstcontrol) $this->firstcontrol = $this->name;
      return $html;
    }

    function fieldString(){
      $html .= '<input class="formEdit"';
      $html .= ' name="'.$this->name.'" id="'.$this->name.'"';
      $html .= " style=\"" . $this->customstyle;
      if (isset($this->property['width'])) $html .= 'width: '.$this->property['width'].';';
      $html .= '"';
      if (isset($this->property['size'])) $html .= ' size="'.$this->property['size'].'"';
      if (isset($this->property['maxlength'])) $html .= ' maxlength="'.$this->property['maxlength'].'"';
      if ($this->property['disabled']) $html .= ' disabled="disabled"';
      if ($this->property['type'] == 'pass' || $this->property['type'] == 'mask') $html .= ' type="password"';
      if (isset($this->property['value']) && $this->property['type']!='pass') $html .= ' value="'.htmlspecialchars($this->property['value']).'"';
      $html .= $this->custom.'>';
      if(!$this->firstcontrol) $this->firstcontrol = $this->name;
      return $html;
    }

    function fieldInterval(){
      $values = explode("-",$this->property['value']);

      $html .= '<input class="formEdit"';
      $html .= ' name="'.$this->name.'_1" id="'.$this->name.'_1"';
      $html .= " style=\"" . $this->customstyle;
      if (isset($this->property['width'])) $html .= 'width: '.$this->property['width'].';';
      $html .= '"';
      if (isset($this->property['size'])) $html .= ' size="'.$this->property['size'].'"';
      if (isset($this->property['maxlength'])) $html .= ' maxlength="'.$this->property['maxlength'].'"';
      if ($this->property['disabled']) $html .= ' disabled="disabled"';
      if ($this->property['type'] == 'pass') $html .= ' type="password"';
      if (isset($this->property['value']) && $this->property['type']!='pass') $html .= ' value="'.htmlspecialchars($values[0]).'"';
      $html .= $this->custom.'>&nbsp-&nbsp;';

      $html .= '<input class="formEdit"';
      $html .= ' name="'.$this->name.'_2" id="'.$this->name.'_2"';
      $html .= " style=\"" . $this->customstyle;
      if (isset($this->property['width'])) $html .= 'width: '.$this->property['width'].';';
      $html .= '"';
      if (isset($this->property['size'])) $html .= ' size="'.$this->property['size'].'"';
      if (isset($this->property['maxlength'])) $html .= ' maxlength="'.$this->property['maxlength'].'"';
      if ($this->property['disabled']) $html .= ' disabled="disabled"';
      if ($this->property['type'] == 'pass') $html .= ' type="password"';
      if (isset($this->property['value']) && $this->property['type']!='pass') $html .= ' value="'.htmlspecialchars($values[1]).'"';
      $html .= $this->custom.'>';
      if(!$this->firstcontrol) $this->firstcontrol = $this->name.'_1';
      return $html;
    }

    function fieldReadOnly(){
      $html .= '<font class="formEdit">'.$this->property['value'].'</font>';
      return $html;
    }

    function fieldCustom(){
      $html .= $this->property['value'];
      return $html;
    }

    function fieldText(){
      $html .= '<textarea class="formEdit" style="width: 200px;'.$this->customstyle.'"';
      $html .= ' name="'.$this->name.'" id="'.$this->name.'"';
      if (isset($this->property['wrap'])) $html .= ' wrap="'.$this->property['wrap'].'"';
      if (isset($this->property['rows'])) $html .= ' rows="'.$this->property['rows'].'"';
      if (isset($this->property['cols'])) $html .= ' cols="'.$this->property['cols'].'"';
      if ($this->property['disabled']) $html .= ' disabled="disabled"';
      $html .= $this->custom.'>';
      if (isset($this->property['value'])) $html .= htmlspecialchars($this->property['value']);
      $html .= '</textarea>';
      if($this->property['dialog'])
      {
        $html .= '&nbsp;<input class="formButton smallButton" type="button" value="..."';
        if (!isset($this->property['dialogw'])) $w = 400; else $w = $this->property['dialogw'];
        if (!isset($this->property['dialogh'])) $h = 450; else $h = $this->property['dialogh'];
        if (!isset($this->property['dlg_res'])) $r = 0; else $r = intval($this->property['dlg_res']);
        if (isset($this->property['addtext']))
          $html .= " OnClick=\"var val=openModalWin('".$this->property['dialog']."', {thevalue: document.".$this->fname.".".$this->name.".value}, ".$w.", ".$h.", ".$r."); if (val) { ".$this->property['addtext']."(val, document.Form.".$this->name."); try { document.Form.check_".$this->name.".checked=false; UnDefaultControl(document.Form.".$this->name.");} catch(e) { } }\">";
        else

          $html .= " OnClick=\"if((window.parent.dialogArguments && window.parent.dialogArguments[1])){
                                 window.parent.dialogArguments[1].thevalue = document.".$this->fname.".".$this->name.".value
                               }

                               var val=openModalWin('".$this->property['dialog']."', (window.parent.dialogArguments && window.parent.dialogArguments[1])?window.parent.dialogArguments[1]:{thevalue: document.".$this->fname.".".$this->name.".value}, ".$w.", ".$h.", ".$r.");
                               if (val) {
                                 document.Form.".$this->name.".value=val;
                                 try { document.Form.check_".$this->name.".checked=false; UnDefaultControl(document.Form.".$this->name.");} catch(e) { }
                               }\">";
      }
      if(!$this->firstcontrol) $this->firstcontrol = $this->name;
      return $html;
    }


    function fieldJSDialog(){
      $html .= '<input class="formEdit" style="width: 90%;'.$this->customstyle.'"';
      $html .= ' name="'.$this->name.'" id="'.$this->name.'"';
      if (isset($this->property['size'])) $html .= ' size="'.$this->property['size'].'"';
      if (isset($this->property['maxlength'])) $html .= ' maxlength="'.$this->property['maxlength'].'"';
      if (isset($this->property['value']) && $this->property['type']!='pass') $html .= ' value="'.htmlspecialchars($this->property['value']).'"';
      $html .= $this->custom.'>&nbsp;<input class="formButton smallButton" type="button" value="..."';
      if (!isset($this->property['dialogw'])) $w = 400; else $w = $this->property['dialogw'];
      if (!isset($this->property['dialogh'])) $h = 450; else $h = $this->property['dialogh'];
      $html .= " OnClick=\"".$this->property['onclick']."\">";
      if(!$this->firstcontrol) $this->firstcontrol = $this->name;
      return $html;
    }


    function fieldTemplate(){

      $html .= '<textarea class="formEdit" style="!!width: 200px;'.$this->customstyle.'"';
      $html .= ' name="'.$this->name.'" id="'.$this->name.'"';
      if (isset($this->property['wrap']))
        $html .= ' wrap="'.$this->property['wrap'].'"';
      else
        $html .= ' wrap="off"';
      if (isset($this->property['rows']))
        $html .= ' rows="'.$this->property['rows'].'"';
      else
        $html .= ' rows="8"';
      if (isset($this->property['cols']))
        $html .= ' cols="'.$this->property['cols'].'"';
      else
        $html .= ' cols="23"';
      if ($this->property['disabled']) $html .= ' disabled="disabled"';
      $html .= $this->custom.'>';
      if (isset($this->property['value']))
        $html .= htmlspecialchars($this->property['value']);
      $html .= '</textarea>';
      $html .= '&nbsp;<input class="formButton smallButton" type="button" value="..."';
      if (!isset($this->property['dialogw'])) $w = 800; else $w = $this->property['dialogw'];
      if (!isset($this->property['dialogh'])) $h = 600; else $h = $this->property['dialogh'];
      if (!isset($this->property['dlg_res'])) $r = 1; else $r = intval($this->property['dlg_res']);
      //sample tags

      if (isset($this->property['addtext']))
        $html .= " OnClick=\"var val=openModalWin('"."?module=dialogs&action=componenttemplate"."', document.".$this->fname.".".$this->name.".value, ".$w.", ".$h.", ".$r."); if (val) { ".$this->property['addtext']."(val, document.Form.".$this->name."); try { document.Form.check_".$this->name.".checked=false; UnDefaultControl(document.Form.".$this->name.");} catch(e) { } }\">";
      else

        $html .= " OnClick=\"if((window.parent.dialogArguments && window.parent.dialogArguments[1])){
                               window.parent.dialogArguments[1].thevalue = document.".$this->fname.".".$this->name.".value
                               window.parent.dialogArguments[1].thetags = '".parseForJScript($this->property['tags'], true)."';
                               window.parent.dialogArguments[1].thesamples = new Array();
                               window.parent.dialogArguments[1].thesamplenames = new Array();
                               ";
                               if(isset($this->property['samples']))
                               {
                                 foreach($this->property['samples'] as $samplename => $sample)
                                 {

                                   $html .= "window.parent.dialogArguments[1].thesamples[window.parent.dialogArguments[1].thesamples.length] = '".parseForJScript(htmlspecialchars($sample), true)."';
                                   ";
                                   $html .= "window.parent.dialogArguments[1].thesamplenames[window.parent.dialogArguments[1].thesamplenames.length] = '".parseForJScript($samplename, true)."';
                                   ";
                                 }
                               }
        $html .= "
                             }else
                             {
                               document.".$this->fname.".".$this->name.".thevalue = document.".$this->fname.".".$this->name.".value
                               document.".$this->fname.".".$this->name.".thetags = '".parseForJScript($this->property['tags'], true)."';
                               document.".$this->fname.".".$this->name.".thesamples = new Array();
                               document.".$this->fname.".".$this->name.".thesamplenames = new Array();
                               ";
                               if(isset($this->property['samples']))
                               {
                                 foreach($this->property['samples'] as $samplename => $sample)
                                 {

                                   $html .= "document.".$this->fname.".".$this->name.".thesamples[document.".$this->fname.".".$this->name.".thesamples.length] = '".parseForJScript(htmlspecialchars($sample), true)."';
                                   ";
                                   $html .= "document.".$this->fname.".".$this->name.".thesamplenames[document.".$this->fname.".".$this->name.".thesamplenames.length] = '".parseForJScript($samplename, true)."';
                                   ";
                                 }
                               }


        $html .= "
                             }
                             var val=openModalWin('"."?module=dialogs&action=componenttemplate"."', (window.parent.dialogArguments && window.parent.dialogArguments[1])?window.parent.dialogArguments[1]:document.".$this->fname.".".$this->name.", ".$w.", ".$h.", ".$r.");
                             if (val) {
                               document.Form.".$this->name.".value=val;
                               try { document.Form.check_".$this->name.".checked=false; UnDefaultControl(document.Form.".$this->name.");} catch(e) { }
                             }\">";
      if(!$this->firstcontrol) $this->firstcontrol = $this->name;
      return $html;
    }


    function fieldGoogleMap(){
      if (!isset($this->property['dialogw'])) $this->property['dialogw'] = 600;
      if (!isset($this->property['dialogh'])) $this->property['dialogh'] = 600;
      if (!isset($this->property['mapw'])) $this->property['mapw'] = 496;
      if (!isset($this->property['maph'])) $this->property['maph'] = 419;
      if(isset($this->property['zoomfield']))
      {
        $this->property['jsarguments'] = "new Array(document.".$this->fname.".".$this->name.", document.".$this->fname.".".$this->property['zoomfield'].")";
      }

      if (!isset($this->property['dialog']) || $this->property['dialog']=="")
          $this->property['dialog'] = "?module=dialogs&action=googlemapframe&site_id=".$GLOBALS['site_id']."&mapw=".$this->property['mapw']."&maph=".$this->property['maph'] . "&googlekey=" . $this->property['googlekey'];

      return $this->fieldStringDialog();
    }


    function fieldDate(){
      //ignore dialog property used in old constructor
      //if (!isset($this->property['dialog']) || $this->property['dialog']=="")
      //    $this->property['dialog'] = "?module=dialogs&action=date&site_id=".$GLOBALS['site_id'];
      //if(isset($this->property['time']))
      //    $this->property['dialog'] .= '&time=' . $this->property['time'];

      $html = '<div style="float:left">';


      $html .= '<input class="formEdit"';
      $html .= ' name="'.$this->name.'" id="'.$this->name.'"';
      $html .= " style=\"" . $this->customstyle;
      if (isset($this->property['width'])) $html .= 'width: '.$this->property['width'].';';
      $html .= '"';
      if (isset($this->property['size'])) $html .= ' size="'.$this->property['size'].'"';
      if (isset($this->property['maxlength'])) $html .= ' maxlength="'.$this->property['maxlength'].'"';
      if ($this->property['disabled']) $html .= ' disabled="disabled"';
      if ($this->property['type'] == 'pass') $html .= ' type="password"';
      if (isset($this->property['value']) && $this->property['type']!='pass') $html .= ' value="'.htmlspecialchars($this->property['value']).'"';
      $html .= $this->custom.'>';

      $html .= '&nbsp;<input class="formButton smallButton" id="btn_cal_'.$this->name.'" type="button" value="..." >';

      if(!$this->IncludedCalendarJs)
      {
        $html .= '
        <script type="text/javascript" src="'.$GLOBALS['cfgWebRoot'].'gui/calendarfiles/calendar_stripped.js"></script>
        <script type="text/javascript" src="'.$GLOBALS['cfgWebRoot'].'gui/calendarfiles/calendar-en.js"></script>
        <script type="text/javascript" src="'.$GLOBALS['cfgWebRoot'].'gui/calendarfiles/calendar-setup_stripped.js"></script>
        <link rel="stylesheet" type="text/css" media="all" href="'.$GLOBALS['cfgWebRoot'].'gui/calendarfiles/calendar-blue.css" />';
      }

      $this->IncludedCalendarJs = true;

      $html .= '

      <script type="text/javascript">
      Calendar.setup({
          inputField     :    "'.$this->name.'",      // id of the input field
          ifFormat       :    "%Y-%m-%d %H:%M:%S",       // format of the input field
          showsTime      :    true,            // will display a time selector
          button         :    "btn_cal_'.$this->name.'",   // trigger for the calendar (button ID)
          singleClick    :    true,           // double-click mode
          step           :    1,
          firstDay       :    1

      });
      </script>
      ';
      if(!$this->firstcontrol) $this->firstcontrol = $this->name;
      $html .= '</div>';
      return $html;
    }


    function fieldWapText(){
      $html = '';
      $html .= '<textarea class="formEdit" style="width: 200px;'.$this->customstyle.'" onselect="storeCaret(this);" onclick="storeCaret(this);" onkeyup="storeCaret(this);" onchange="storeCaret(this);"';
      $html .= ' name="'.$this->name.'" id="'.$this->name.'"';
      if (isset($this->property['wrap'])) $html .= ' wrap="'.$this->property['wrap'].'"';
      if (isset($this->property['rows'])) $html .= ' rows="'.$this->property['rows'].'"';
      if (isset($this->property['cols'])) $html .= ' cols="'.$this->property['cols'].'"';
      if ($this->property['disabled']) $html .= ' disabled="disabled"';
      $html .= $this->custom.'>';
      if (isset($this->property['value'])) $html .= htmlspecialchars($this->property['value']);
      $html .= '</textarea>';

      return $html;
    }

    function fieldWYSIWYG()
    {
      $html = '';

      $sitedata = sqlQueryRow("SELECT domain, wwwroot FROM sites WHERE site_id='".$_GET['site_id']."'");
      $base = 'http://' . $sitedata['domain'] . $sitedata['wwwroot'];

//    $html .= '<textarea class="formEdit" style="width: 200px"';
//    $html .= ' name="'.$this->name.'"';
//    if ($this->property['disabled']) $html .= ' disabled="disabled"';
//    $html .= $this->custom.'>';
//    if (isset($this->property['value'])) $html .= htmlspecialchars($this->property['value']);
//    $html .= '</textarea>';

      $html .= '<textarea name="'.$this->name.'" id="'.$this->name.'"></textarea>
      		<script type="text/javascript">
<!--
       	var sBasePath = \''. $GLOBALS['cfgWebRoot'] . 'gui/ckeditor/\' ;

		var oFCKeditor = CKEDITOR.replace( "'.$this->name.'",{
			on:{
				blur:
					function(){
						//$("#cke_'.$this->name.' .cke_toolbox_collapser").trigger("click");
					}
				,
				focus:
					function(){
						//$("#cke_'.$this->name.' .cke_toolbox_collapser").trigger("click");
					}

			}
		} ) ;
		oFCKeditor.config.toolbarCanCollapse = true;
		oFCKeditor.config.toolbarStartupExpanded = true;

		var langs = new Object();
		langs["Latvian"] = "lv";
		langs["Russian"] = "ru";
		langs["English"] = "en";

		var lang = langs["'.$GLOBALS['cfgLanguage'].'"];
		oFCKeditor.config.language = lang;
		oFCKeditor.config.basePath		= sBasePath ;
		oFCKeditor.config.allowedContent		= true ;
		oFCKeditor.config.enterMode = CKEDITOR.ENTER_BR;
		oFCKeditor.config.toolbar = [
			["Bold","Italic","Underline","-","Link","Unlink","Image","-","Source"]
		];
		oFCKeditor.config.filebrowserBrowseUrl = "'.$GLOBALS['cfgWebRoot'].'?module=dialogs&action=filex&site_id='.$GLOBALS['site_id'].'&view=thumbs";
		oFCKeditor.setData(\''.parseForJScript($this->property['value'], true).'\');


//-->
		</script>';


      $html .= '&nbsp;<input class="formButton smallButton" type="button" value="..."';
      if (!isset($this->property['dialogw'])) $w = 400; else $w = $this->property['dialogw'];
      if (!isset($this->property['dialogh'])) $h = 450; else $h = $this->property['dialogh'];
      if (!isset($this->property['dlg_res'])) $r = 1; else $r = intval($this->property['dlg_res']);
      if (isset($this->property['addtext'])){
        $html .= " OnClick=\"var val=openModalWin('".$this->property['dialog']."', {'thevalue': document.".$this->fname.".".$this->name.".value}, ".$w.", ".$h.", ".$r."); if (val) { ".$this->property['addtext']."(val, document.Form.".$this->name."); try { document.Form.check_".$this->name.".checked=false; UnDefaultControl(document.Form.".$this->name.");} catch(e) { } }\">";
      }else{
        $html .= " OnClick=\"var oEditor = CKEDITOR.instances.".$this->name.";
                             if((window.parent.dialogArguments && window.parent.dialogArguments[1])){
                               window.parent.dialogArguments[1].thevalue = oEditor.getData();
                               document.".$this->fname.".".$this->name.".value = oEditor.getData();
                             }
                                     
                             var obj = {'thevalue': oEditor.getData()};
                                openDlg('".$this->property['dialog']."', obj, {w: ".$w.", h: ".$h."}, function(val){

                                    if (val) {
                                        oEditor.setData(val);
                                        document.Form.".$this->name.".value=val;
                                        try { document.Form.check_".$this->name.".checked=false; UnDefaultControl(document.Form.".$this->name.");} catch(e) { }
                                    }

                                });

                \">";
      }
      if(!$this->firstcontrol) $this->firstcontrol = $this->name.'_editor';
      return $html;
    }

    function fieldHTML(){
      $html .= '<textarea class="formEdit"';
      $html .= ' name="'.$this->name.'" id="'.$this->name.'"';
      if (isset($this->property['wrap'])) $html .= ' wrap="'.$this->property['wrap'].'"';
      $html .= " style=\"" . $this->customstyle;
      if (isset($this->property['width'])) $html .= 'width: '.$this->property['width'].';';
      $html .= '"';
      if (isset($this->property['rows'])) $html .= ' rows="'.$this->property['rows'].'"';
      if (isset($this->property['cols'])) $html .= ' cols="'.$this->property['cols'].'"';
      $html .= $this->custom.$this->property['custom'].'>';
      if (isset($this->property['value'])) $html .= htmlspecialchars($this->property['value']);
      $html .= '</textarea>';
      if(!$this->firstcontrol) $this->firstcontrol = $this->name;
      return $html;
    }

    function fieldStringDialog(){
      $html .= '<input class="formEdit" style="width: 90%;'.$this->customstyle.'"';
      $html .= ' name="'.$this->name.'" id="'.$this->name.'"';
      if (isset($this->property['size'])) $html .= ' size="'.$this->property['size'].'"';
      if (isset($this->property['maxlength'])) $html .= ' maxlength="'.$this->property['maxlength'].'"';
      if (isset($this->property['value']) && $this->property['type']!='pass') $html .= ' value="'.htmlspecialchars($this->property['value']).'"';
      $html .= $this->custom.'>&nbsp;<input class="formButton smallButton" type="button" value="..."';
      if (!isset($this->property['dialogw'])) $w = 400; else $w = $this->property['dialogw'];
      if (!isset($this->property['dialogh'])) $h = 450; else $h = $this->property['dialogh'];
      $html .= " OnClick=\"
			openDlg('".$this->property['dialog']."', " . (!isset($this->property['jsarguments']) ? "document.".$this->fname.".".$this->name.".value" : $this->property['jsarguments']) . ", {w:".$w.",h:".$h."}, function(val){
      	if (val) document.".$this->fname.".".$this->name.".value=val;
				try{ document.".$this->fname.".check_".$this->name.".checked=false; UnDefaultControl(document.Form.".$this->name.");} catch(e) {}
			});

			 \">";
      if(!$this->firstcontrol) $this->firstcontrol = $this->name;
      return $html;
    }

    function output(){
      if ($this->custom != "") $this->custom = " ".$this->custom;
      switch ($this->property['type']) {
        case "check":
          $html = $this->fieldBoolean();
          break;
        case "html":
          $html = $this->fieldHTML();
          break;
        case "hidden":
          $html = $this->fieldHidden();
          break;
        case "file":
          $html = $this->fieldFile();
          break;
        case "button":
          $html = $this->fieldButton();
          break;
        case "list":
          $html = $this->fieldList();
          break;
        case "collection":
          $html = $this->fieldCollection();
          break;
        case "action_list":
          $html = $this->fieldActionList();
          break;
        case "customtext":
          $html = $this->fieldText();
          break;
        case "template":
          $html = $this->fieldTemplate();
          break;
        case "dialog":
          $html = $this->fieldStringDialog();
          break;
        case "scriptdialog":
          $html = $this->fieldJSDialog();
          break;
        case "label":
          $html = $this->fieldReadOnly();
          break;
        case "code":
          $html = $this->fieldCustom();
          break;
        case "color":
          $html = $this->fieldColor();
          break;
        case "wysiwyg":
          if($this->allowwysiwyg)
            $html = $this->fieldWYSIWYG();
          else
            $html = $this->fieldText();
          break;
        case "waptext":
          $html = $this->fieldWapText();
          break;
        case "googlemap":
          $html = $this->fieldGoogleMap();
          break;
        case "date":
          $html = $this->fieldDate();
          break;
        case "interval":
          $html = $this->fieldInterval();
          break;
        default:
          $html = $this->fieldString();
      }
      return $html;
    }
}

/* FormGen puts it all together and returns form's HTML code */

class FormGen {

  var $title;
  var $labelwidth;
  var $valuewidth;
  var $rows;
  var $properties; // property array
  var $action; // action script
  var $cancel; // cancel script
  var $buttons; // buttons visible
  var $name;
  var $activatefirstcontrol = true;
  var $allowwysiwyg = true;

  function FormGen() {
    $this->labelwidth = "130";
    $this->valuewidth = "*";
    $this->rows = array();
    $this->buttons = true;
    $this->name = "Form";
  }

  // Generate title row
  function get_title($title){
    $html .= "<tr>\n";
    $html .= "  <td colspan=\"2\" class=\"formTitle\">\n";
    $html .= "    ".$title."\n";
    $html .= "  </td>\n";
    $html .= "</tr>\n";
//A.I.    $html .= "<tr><td width=\"100%\" colspan=\"2\" class=\"formTitle\">&nbsp;</td></tr>\n";
    return $html;
  }

  // Generate property rows
  function get_row($title, $control, $error, $extrahtml='', $hint = ''){
    $value = array();
    isset($this->labelwidth) ? $width1 = ' width="'.$this->labelwidth.'"': $width1 = ' width="200"';
    isset($this->valuewidth) ? $width2 = ' width="'.$this->valuewidth.'"': $width2 = "";
    if ($error) $class="formELabel"; else $class="formLabel";
    $html .= "<tr>\n";
    $html .= "  <td ".$width1."class=\"".$class."\">\n";
    $html .= "    ".$title."\n";
    if($hint)
    {
      $html .= '<img src="'.$GLOBALS['cfgWebRoot'].'gui/images/infoicon.gif" alt="info" style="cursor: help;" onclick="javascript:
         var m = Ext.Msg.show({
             title:\'' . parseForJScript(trim($title, ': '), true) . '\',
             msg: \'' . parseForJScript($hint, true) . '\',
             buttons: Ext.Msg.OK,
             icon: Ext.MessageBox.INFO,
             modal: true,
             animEl: this
         });

      " />';
    }
    $html .= "  </td>\n";
    $html .= "  <td ".$width2."class=\"formControl\">\n";
    $html .= "    ".$control."\n".$extrahtml;
    $html .= "  </td>\n";
    $html .= "</tr>\n";
    return $html;
  }

  function get_buttons() {
    $html .= "<table width=\"100%\" border=\"0\" cellpadding=\"4\" cellspacing=\"4\">\n";
    $html .= "<tr><td>&nbsp;</td></tr>\n";
    //$html .= "<tr><td><hr class=\"formLine\"></td></tr>\n";
    $html .= "<tr>\n";
    $html .= "  <td align=\"right\">\n";
    $html .= "<input ID=\"OKButton\" class=\"formButton\" type=\"submit\" value=\"&nbsp;&nbsp;{%langstr:ok%}&nbsp;&nbsp;\">&nbsp;";
    if ($this->cancel == 'close') $cancel = 'window.close();';
    elseif ($this->cancel == 'closeparent') $cancel = 'window.parent.close();';
    elseif ($this->cancel == 'back') $cancel = 'history.back();';
    else   $cancel = "javascript:self.location='".$this->cancel."'";
    $html .= "<input class=\"formButton\" type=\"button\" value=\"{%langstr:cancel%}\" OnClick=\"$cancel\">&nbsp;";
    if (!$this->hidereset)
      $html .= "<input class=\"formButton\" type=\"reset\" value=\"{%langstr:reset%}\">";
    $html .= "\n  </td>\n";
    $html .= "</tr>\n";
    $html .= "</table>\n";
    return $html;
  }

  function get_JS_for_buttons(){
    $html .= "<script>";
    $html .= "function okenable(){";
    $html .= $this->name.".OKButton.disabled = false;";
    $html .= "}";
    $html .= "function okclick() {";
    //field validation javascript
    $html .= $this->validatejs;
    $html .= $this->extrajs;
    $html .= $this->name.".OKButton.disabled = true;";
    $html .= "setTimeout('okenable()', 3000);";
    $html .= "return true;";
    $html .= "}";
    $html .= "</script>";
    return $html;
    }

  function get_JS_for_nobuttons(){
    $html .= "<script>";
    $html .= "function okenable(){";
    $html .= "}";
    $html .= "function okclick() {";
    $html .= $this->validatejs;
    $html .= $this->extrajs;
    $html .= "return true;";
    $html .= "}";
    $html .= "</script>";
    return $html;
    }

  function final_wrap($code){

    $html .= "<form name=\"".$this->name."\" id=\"".$this->name."\" enctype=\"multipart/form-data\" action=\"".$this->action."\" method=\"POST\" onSubmit=\"return okclick();\">\n";

    if ($this->buttons==true)
      $html .= $this->get_JS_for_buttons();
    else
      $html .= $this->get_JS_for_nobuttons();

    $html .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"formExFrame\">\n";
    $html .= "<tr><td class=\"formFrame\">\n";
    $html .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\" class=\"formInFrame\">\n";
    $html .= "<tr style=\"display: none;\"><td width=\"".$this->labelwidth."\"></td><td width=\"".$this->valuewidth."\"></td></tr>\n";
    $html .= $code;
    $html .= "</table>\n";
    if ($this->buttons==true) $html .= $this->get_buttons();
    $html .= "</td></tr>\n";
    $html .= "</table>\n";
    $html .= "</form>";
    if($this->activatefirstcontrol && $this->firstcontrol)
      $html .= '<script>try{ document.' . $this->name . '.' . $this->firstcontrol . '.focus(); } catch(e) {}</script>';
    return $html;
  }

  function generateJSSubmitErrorCmd($name, $property, $defaultmsg)
  {
    if($property['validate_msg'])
      $defaultmsg = $property['validate_msg'];
    return '
            alert("'.parseForJScript(sprintf($defaultmsg, trim($property['label'], ' :')), false).'");
            try{ document.' . $this->name . '.' . $name . '.focus(); } catch(e) {};
            return false;';
  }

  function output(){
    if (isset($this->title)) $html .= $this->get_title($this->title);
    $this->validatejs = '';
    $this->extrajs = '';

    foreach(array_keys($this->properties) as $name) {
      $property_control = new InputControl();
      $property_control->property = $this->properties[$name];
      $property_control->name = $name;
			$property_control->customstyle = $this->properties[$name]["style"];
      $property_control->fname = $this->name;
      $property_control->activatefirstcontrol = $this->activatefirstcontrol;
      $property_control->firstcontrol = $this->firstcontrol;
      $property_control->IncludedCalendarJs = $this->IncludedCalendarJs;
      $property_control->extrajs = '';
      $property_control->allowwysiwyg = $this->allowwysiwyg;
      if ($this->properties[$name]["type"]=="hidden")
        $html .= $property_control->output()."\n";
      elseif ($this->properties[$name]["type"]=="title")
        $html .= $this->get_title($this->properties[$name]["label"]);
      elseif (!$this->properties[$name]["noshow"]) {
        $html .= $this->get_row($this->properties[$name]["label"], $property_control->output(), $this->properties[$name]["error"], $this->properties[$name]['extrahtml'], $this->properties[$name]['hint']);
      }
      $this->firstcontrol = $property_control->firstcontrol;
      $this->IncludedCalendarJs = $property_control->IncludedCalendarJs;
      $this->extrajs .= $property_control->extrajs;

      switch($property_control->property['validate'])
      {
        case "required":
          $this->validatejs .= 'if(!' . $this->name . '.' . $name . '.value) {';
          $this->validatejs .= $this->generateJSSubmitErrorCmd($name, $property_control->property, 'Please fill in field "%s"!');
          $this->validatejs .= '}';
          break;
        case "email":
          $this->validatejs .= 'var validRegExp = /^[^@]+@[^@]+.[a-z]{2,}$/i;';
          $this->validatejs .= 'if (' . $this->name . '.' . $name . '.value.search(validRegExp) == -1) {';
          $this->validatejs .= $this->generateJSSubmitErrorCmd($name, $property_control->property, 'Field "%s" must be valid e-mail address!');
          $this->validatejs .= '}';
          break;
        case "custom":
          $customjs = $property_control->property['validate_code'];
          $customjs = str_replace('{%form_name%}', $this->name, $customjs);
          $this->validatejs .= $customjs;
          break;
      }

      unset($property_control);
    }
    $html = $this->final_wrap($html);
    return $html;
  }

}

/* Inspector generates property editor */

class Inspector {

  var $title;
  var $labelwidth;
  var $valuewidth;
  var $checkboxwidth;
  var $rows;
  var $properties; // property array
  var $action; // action script
  var $cancel; // cancel script
  var $buttons; // buttons visible
  var $name;
  var $opened;
  var $displayplus;
  var $displaytitle;
  var $hidecheckboxes;
  var $allowwysiwyg = true;

  function Inspector() {
    $this->labelwidth = "130";
    $this->valuewidth = "*";
    $this->checkboxwidth = "20";
    $this->rows = array();
    $this->buttons = true;
    $this->name = "Form";
    $this->displayplus = true;
    $this->displaytitle = true;
  }

  // Generate title row
  function get_title($title, $divid, $opened, $displayplus, $visible = 0){

    // $visible = 0 means that it is visible

    $im1 = $GLOBALS['cfgWebRoot'] . "gui/images/plus.gif";
    $im2 = $GLOBALS['cfgWebRoot'] . "gui/images/minus.gif";
    if($opened)
      $onclick = " OnClick=\"javascript:if($divid.style.display=='none'){".$divid.".style.display='block'; img_$divid.src='".$im2."'; }else{".$divid.".style.display='none';img_$divid.src='".$im1."';}  \"";
    else
      $onclick = " OnClick=\"javascript:if($divid.style.display=='none'){".$divid.".style.display='block'; img_$divid.src='".$im2."'; }else{".$divid.".style.display='none';img_$divid.src='".$im1."';}  \"";

    $html .= "<tr>\n";
    $html .= '  <td colspan="3" class="' . ( $visible == 1 ? 'formTitleNotVisible' : 'formTitle' ) . '"
                '.$onclick.' >'."\n";
    if($displayplus)
    {
      if($opened)
        $html .= "      <img id=\"img_$divid\" src=\"".$im2."\" />";
      else
        $html .= "      <img id=\"img_$divid\" src=\"".$im1."\" />";
    }
    $html .= "    ".$title."\n";
    $html .= "  </td>\n";
    $html .= "</tr>\n";
    return $html;
  }

  function get_div_start($id, $opened)
  {
    if($opened)
      $html  = '<tr><td style="padding: 0;"><div id="' . $id . '" style="display:block; padding-top: 5px; padding-bottom: 5px;"><table width="100%">';
    else
      $html  = '<tr><td style="padding: 0;"><div id="' . $id . '" style="display:none; padding-top: 5px; padding-bottom: 5px;"><table width="100%">';
    return $html;
  }

  function get_div_end()
  {
    $html  = '</table></div></td></tr>';
    return $html;
  }

  // Generate property rows
  function get_row($title, $control, $def, $error){
    $value = array();
    isset($this->labelwidth) ? $width1 = ' width="'.$this->labelwidth.'"': $width1 = "";
    isset($this->valuewidth) ? $width2 = ' width="'.$this->valuewidth.'"': $width2 = "";
    isset($this->checkboxwidth) ? $width3 = ' width="'.$this->checkboxwidth.'"': $width3 = "";
    if ($error) $class="formELabel"; else $class="formLabel";
    $html .= "<tr>\n";
    $html .= "  <td ".$width1."class=\"".$class."\" style=\"white-space: nowrap;\">\n";
    $html .= "    ".$title."\n";
    $html .= "  </td>\n";
    $html .= "  <td ".$width2."class=\"formControl\" style=\"white-space: nowrap;\">\n";
    $html .= "    ".$control."\n";
    $html .= "  </td>\n";
    $html .= "  <td ".$width3."class=\"formControl\">\n";
    $html .= "    ".$def."\n";;
    $html .= "  </td>\n";
    $html .= "</tr>\n";
    return $html;
  }

  function get_def($pname, $pdefval, $pcheck, $pval, &$checked) {
    if ($pcheck) $checked = ' checked="checked"'; else $checked='';
    $script = "DefaultProperty(this,'".htmlspecialchars($pdefval)."', document.getElementById('".$pname."'),'".htmlspecialchars($pval)."')";
    $html .= '<input type="checkbox" name="check_'.$pname.'" id="check_'.$pname.'" OnClick="'.$script.'"'.$checked.'>';
    return $html;
  }

  function get_buttons() {
    $html .= "<table width=\"100%\" border=\"0\">\n";
    $html .= "<tr><td>&nbsp;</td></tr>\n";
    $html .= "<tr>\n";
    $html .= "  <td align=\"right\">\n";
    $html .= "<input ID=\"OKButton\" class=\"formButton\" type=\"submit\" value=\"&nbsp;&nbsp;{%langstr:ok%}&nbsp;&nbsp;\">&nbsp;";
    if ($this->cancel == 'close') $cancel = 'window.close();';
    elseif ($this->cancel == 'closeparent') $cancel = 'window.parent.close();';
    else $cancel = "javascript:self.location='".$this->cancel."'";
    $html .= "<input class=\"formButton\" type=\"button\" value=\"{%langstr:cancel%}\" OnClick=\"$cancel\">&nbsp;";
    $html .= "<input class=\"formButton\" type=\"reset\" value=\"{%langstr:reset%}\">";
    $html .= "\n  </td>\n";
    $html .= "</tr>\n";
    $html .= "</table>\n";
    return $html;
  }

  function final_wrap($code){
    $html .= "<form name=\"".$this->name."\" id=\"".$this->name."\" enctype=\"multipart/form-data\" action=\"".$this->action."\" method=\"POST\">\n";
    $html .= "<table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"0\" class=\"formExFrame\">\n";
    $html .= "<tr><td class=\"formFrame\">\n";
    $html .= "<table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"5\">\n";
    $html .= "<tr style=\"display: none;\"><td width=\"".$this->labelwidth."\"></td><td width=\"".$this->valuewidth."\"></td><td width=\"".$this->checkboxwidth."\"></td></tr>\n";
    $html .= $code;
    $html .= "</table>\n";
    if ($this->buttons==true) $html .= $this->get_buttons();
    $html .= "</td></tr>\n";
    $html .= "</table>\n";
    $html .= "</form>";
    return $html;
  }

  function output(){
    if (isset($this->title)) $html .= $this->get_title($this->title);
    $f = 0;
    $divs = false;

    foreach(array_keys($this->properties) as $name) {
      $f++;
      $property_control = new InputControl();
      $property_control->allowwysiwyg = $this->allowwysiwyg;
      $property_control->property = $this->properties[$name];
      $property_control->name = $name;
      $property_control->fname = $this->name;
      $property_control->custom = ' OnChange="try { check_'.$name.'.checked=false; UnDefaultControl(document.Form.'.$name.'); } catch(e) {}"
                                     OnKeyPress="try { check_'.$name.'.checked=false; UnDefaultControl(document.Form.'.$name.'); } catch(e) {}"';
      if ($this->properties[$name]["type"]=="hidden") {
        $html .= $property_control->output()."\n";
      }
      elseif ($this->properties[$name]["type"]=="title")
      {
        if($divs)
        {
          $html .= $this->get_div_end();
        }
        if($this->displaytitle)
        {
          $visible = $this->properties['component_' . $this->properties[$name]["value"] . '_visible']['value'];
          $html .= $this->get_title($this->properties[$name]["label"],"div" . $f, $this->opened, $this->displayplus, $visible);
        }
        $html .= $this->get_div_start("div" . $f, $this->opened);
        $divs = true;
      }
      elseif (!$this->properties[$name]["noshow"]) {
        if(!$this->hidecheckboxes)
        {
          $def = $this->get_def($name, $this->properties[$name]["defval"], $this->properties[$name]["default"], parseForJScript($this->properties[$name]["value"], true), $checked);
          if($checked)
          {
            $property_control->customstyle .= 'background-color:#F0F0F0;';
          }
        }else
        {
          $def = '';
        }
        $html .= $this->get_row($this->properties[$name]["label"], $property_control->output(), $def, $this->properties[$name]["error"]);
      }
      unset($property_control);
    }
    if($divs)
      $html .= $this->get_div_end();
    $html = $this->final_wrap($html);
    return $html;
  }

}

