<?php

use Constructor\WebApp;

class ProductSorter
{

    public static function sort($products, $order)
    {

        switch($order){
            case 'price-asc': usort($products, array(get_class(), 'sortPriceAsc')); break;
            case 'price-desc': usort($products, array(get_class(), 'sortPriceDesc')); break;
            case 'popular': usort($products, array(get_class(), 'sortPopular')); break;
        }

        return $products;

    }

    private static function sortPopular($a, $b)
    {

        return rand() > rand() ? 1 : -1;

    }

    private static function sortPriceAsc($a, $b)
    {

        $price_a = self::getPriceForSort($a);
        $price_b = self::getPriceForSort($b);

        if($price_a > $price_b){
            return 1;
        }elseif($price_a < $price_b){
            return -1;
        }else{
            return 0;
        }

    }

    private static function sortPriceDesc($a, $b)
    {

        $price_a = self::getPriceForSort($a);
        $price_b = self::getPriceForSort($b);

        if($price_a < $price_b){
            return 1;
        }elseif($price_a > $price_b){
            return -1;
        }else{
            return 0;
        }

    }

    private static function getPriceForSort($product)
    {

        static $cache;

        if(isset($cache[$product['item_id']])){
            $price = $cache[$product['item_id']];
        }else{

            $prices = shopmanager::getInstance()->getProductPrice($product, WebApp::$app->user->get());
            $price = $cache[$product['item_id']] = (float)$prices['price'];

        }

        return $price;

    }

}