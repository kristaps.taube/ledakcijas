<?

/* DB functions */

function var_dump_ret($mixed = null) {
  ob_start();
  var_dump($mixed);
  $content = ob_get_contents();
  ob_end_clean();
  return $content;
}

function db_error($desc, $die=false) {
  if ($GLOBALS['SqlErrorShow'])
  {
      echo "<table border=\"1\" width=\"100%\" bgcolor=\"#FFFFFF\"><tr><td>\n";
      echo "<h3>Database Error</h3>\n";
      echo "<p><b>Description:</b> $desc<br>\n";
      echo "<b>MySQL Error:</b> ", mysql_error(), "<br>\n";
      if ($die) echo "<b>Status:</b> This script cannot continue, terminating.<br>\n";
      echo "</tr></td><tr><td><pre>".print_r(debug_backtrace(), true)."</pre></td></tr></table>";
  }
  if ($GLOBALS['SqlErrorMail'] && mysql_error() != 'No database selected' &&
      $desc != 'Can\'t select database' && strpos($desc, 'Can\'t connect to localhost') === false &&
      mysql_error() != 'Too many connections' && mysql_error() != 'MySQL server has gone away')
    {
      if(!isset($GLOBALS['SqlErrorMailSentTimes']))
        $GLOBALS['SqlErrorMailSentTimes'] = 0;
      $GLOBALS['SqlErrorMailSentTimes']++;

      if($GLOBALS['SqlErrorMailSentTimes'] <= 4)
      {

        mail($GLOBALS['SqlErrorMail'], "MySQL error", "User: " . $_SESSION['session_user'] .
             "\nError: " . mysql_error() .
             "\nLink: http://" . $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] .
             "\nDescription: $desc" .
             "\nStack: ". var_dump_ret(debug_backtrace()));
      }
      //echo $GLOBALS['SqlErrorMail']; die;
    }
  if ($die) die();
}

function db_connect($dbhost, $dbname, $dbuser, $dbpass, $dbport=false) {
	if ($dbport){
		if (! $dbh = mysql_connect($dbhost.':'.$dbport, $dbuser, $dbpass)) {
    	db_error('Can\'t connect to '.$dbhost.':'.$dbport.' as '.$dbuser);
  	}
	}
	else{
  	if (! $dbh = mysql_connect($dbhost, $dbuser, $dbpass)) {
    	db_error("Can't connect to $dbhost as $dbuser");
  	}
	}
  if (! mysql_select_db($dbname)) {
    db_error("Can't select database $dbnam");
    return false;
  }

  $row = mysql_fetch_assoc(mysql_query('SHOW TABLE STATUS LIKE "sites"'));
  if ($row['Collation'] != 'latin1_swedish_ci')
    mysql_query('SET NAMES utf8');

  return $dbh;
}

function db_disconnect($link) {
  mysql_close($link);
}

function log_slow_query($query, $time){
	file_put_contents(SLOW_QUERY_FILE, "Query(MySQL): " . $query . PHP_EOL . "Exec time: ".$time."" . PHP_EOL . "---------------" . PHP_EOL, FILE_APPEND);
}

function log_query($query, $time){
	file_put_contents(QUERY_FILE, "Query(".$GLOBALS['total_queries_executed'].".)((MySQL)): " . $query . PHP_EOL . "Exec time: ".$time . PHP_EOL . "---------------" . PHP_EOL, FILE_APPEND);
}

function db_query($query, $link) {

  $GLOBALS['total_queries_executed']++;
	$start = microtime(true);
	if(LOG_SLOW_QUERIES || LOG_QUERIES){
		$start = microtime(true);
	}

  DB::Query($query);

  if(LOG_SLOW_QUERIES || LOG_QUERIES){
		$end = microtime(true);
		$dif = $end - $start;

    $GLOBALS['total_query_time'] += $dif;

    if(LOG_SLOW_QUERIES){
    	if($dif > SLOW_QUERY_TIME_LIMIT){
      	log_slow_query($query, $dif);
      }
    }

		if(LOG_QUERIES){ // all
    	log_query($query, $dif);
    }

	}

  if (! $qid) {
    db_error("Can't execute query: <pre>$query</pre>");
  }

  return $qid;

}

function db_fetch_object($qid) {
  return mysql_fetch_object($qid);
}

function db_fetch_array($qid) {
  return mysql_fetch_array($qid);
}

function db_fetch_array_assoc($qid)
{
  return mysql_fetch_assoc($qid);
}

function db_insert_id($link) {
  return mysql_insert_id($link);
}

function db_num_rows() {
  return mysql_num_rows();
}

function db_escape($s) {
  return mysql_real_escape_string($s);
}

/* SQL functions */

function sqlLastID() {
  return db_insert_id($GLOBALS['DBH']);
}

function sqlQueryData($query, $params = array(), $fetch = PDO::FETCH_BOTH) {
  $result = DB::GetTable($query, $params, $fetch);
  return $result;
}

function sqlQueryDataAssoc($query, $params = array()) {
  $result = DB::GetTable($query, $params);
  return $result;
}

function sqlQueryColumn($query, $params = array()) {
  $result = DB::GetColumn($query, $params);
  return $result;
}

function sqlQueryRow($query, $params = array(), $fetch = PDO::FETCH_BOTH) {
  $result = DB::GetRow($query, $params, $fetch);
  return $result;
}

function sqlQueryRowAssoc($query, $params = array()) {
  $result = DB::GetRow($query, $params);
  return $result;
}

function sqlQueryValue($query, $params = array()) {
  $result = DB::GetValue($query, $params);
  return $result;
}

function sqlQuery($query, $params = array()) {
  return DB::Query($query, $params);
}

function sqlQueryDataAndID($query, &$qid) {
  $result = false;
  $i = 0;
  if ($qid = db_query($query, $GLOBALS['DBH']))
    while ($r = db_fetch_array($qid)) $result[$i++] = $r;
  return $result;
}

