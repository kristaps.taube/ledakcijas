<?

require_once($GLOBALS['cfgDirRoot'].'backend/const.php');

function fileDropBackslash($path) {
  if (substr($path, strlen($path)-1)=='/') $path = substr($path, 0, strlen($path)-1);
  return $path;
}

function addvideoarrow( $source_file_path, $watermark = "/images/html/play.png" ){

  $opacity = 100; // %
  $quality = 5; // Compression level: from 0 (no compression) to 9;

  $site_id = $GLOBALS['site_id'];
  if (!$site_id) return '';

  $name = basename ($source_file_path);
  $dir = dirname($source_file_path);
  $new_file_name = explode(".", $name);
  $new_file_name[count($new_file_name)-2] .= "_watermarked";
  $new_file_name[count($new_file_name)-2] = md5($new_file_name[count($new_file_name)-2]); // not readable
  $new_file_name[count($new_file_name)-1] = "png"; // output will be png
  $new_file_name = implode(".", $new_file_name);
  $new_file_path = $dir."/".$new_file_name;

  $watermark = getFilePathFromLink($site_id, $watermark);
  $path_to_image = getFilePathFromLink($site_id, $source_file_path);
  $path_to_new_image = getFilePathFromLink($site_id, $new_file_path);

  list( $source_width, $source_height, $source_type ) = getimagesize( $path_to_image ); // original sizes
  list( $o_watermark_width, $o_watermark_height, $watermark_type ) = getimagesize( $watermark ); // original sizes

  if ( $source_type === NULL ){ return ""; }

  switch ( $source_type ){
    case IMAGETYPE_JPEG: $source_gd_image = imagecreatefromjpeg( $path_to_image ); break;
    case IMAGETYPE_PNG: $source_gd_image = imagecreatefrompng( $path_to_image ); break;
    default: return "";
  }

  switch ( $watermark_type ){
    case IMAGETYPE_JPEG: $watermark_gd_image = imagecreatefromjpeg( $watermark ); break;
    case IMAGETYPE_PNG: $watermark_gd_image = imagecreatefrompng( $watermark ); break;
    default: return "";
  }

  if($o_watermark_width > $source_width){  // if watermark is wider we need to resize watermark
    $ratio = $o_watermark_width / $source_width;
    $watermark_width = $o_watermark_width / $ratio;
    $watermark_height = $o_watermark_height / $ratio;
  }

  if($o_watermark_height > $source_height){  // if watermark is taller we need to resize watermark
    $ratio = $o_watermark_height / $source_height;
    $watermark_width = $o_watermark_width / $ratio;
    $watermark_height = $o_watermark_height / $ratio;
  }

  if($o_watermark_width / $source_width < 0.5){ // if watermark is too small
    $ratio = $o_watermark_width /($source_width * 0.5);
    $watermark_width = $o_watermark_width / $ratio;
    $watermark_height = $o_watermark_height / $ratio;
  }

  if($o_watermark_width / $source_width > 0.5){ // if watermark is too big
    $ratio = $o_watermark_width /($source_width * 0.5);
    $watermark_width = $o_watermark_width / $ratio;
    $watermark_height = $o_watermark_height / $ratio;
  }

  $watermark_width = ($watermark_width)? floor($watermark_width) : $o_watermark_width;
  $watermark_height = ($watermark_height)? floor($watermark_height) : $o_watermark_height;

  force_truecolor_image($watermark_gd_image);
  force_truecolor_image($source_gd_image);

  $waterm = imagecreatetruecolor($watermark_width, $watermark_height);
  imagealphablending($waterm, false);
  imagefill($waterm, 0, 0, imagecolortransparent($waterm, imagecolorallocatealpha($waterm, 0, 0, 0, 127))); // better do not try to understand this
  imagesavealpha($waterm, true);
  imagecopyresampled($waterm, $watermark_gd_image, 0, 0, 0, 0, $watermark_width, $watermark_height, $o_watermark_width, $o_watermark_height );
  filter_opacity($waterm, $opacity);
  imagecopy($source_gd_image, $waterm, floor(($source_width-$watermark_width)/2), floor(($source_height-$watermark_height)/2), 0, 0, imagesx($waterm), imagesy($waterm));

  imagepng( $source_gd_image, $path_to_new_image, $quality );

  imagedestroy( $source_gd_image );
  imagedestroy( $waterm );
  imagedestroy( $watermark_gd_image );

  return $new_file_path;

}

function fileAddBackslash($path) {
  if (substr($path, strlen($path)-1)!='/') $path = $path.'/';
  return $path;
}

function fileDropStartBackslash($path) {
  if (substr($path, 0, 1)=='/')
    $path = substr($path, 1);
  return $path;
}

function fileValidName($fname) {
  return ereg_replace("[^a-z0-9._]", "", ereg_replace(" ", "_", ereg_replace("%20", "_", strtolower($fname))));
}

function create_watermarked( $source_file_path, $watermark ){

  $opacity = 15; // %
  $quality = 5; // Compression level: from 0 (no compression) to 9;

  $site_id = $GLOBALS['site_id'];
  if (!$site_id) return '1';

  $name = basename ($source_file_path);
  $dir = dirname($source_file_path);
  $new_file_name = explode(".", $name);
  $new_file_name[count($new_file_name)-2] .= "_watermarked";
  $new_file_name[count($new_file_name)-2] = md5($new_file_name[count($new_file_name)-2]); // not readable
  $new_file_name[count($new_file_name)-1] = "png"; // output will be png
  $new_file_name = implode(".", $new_file_name);
  $new_file_path = $dir."/".$new_file_name;

  $watermark = getFilePathFromLink($site_id, $watermark);
  $path_to_image = getFilePathFromLink($site_id, $source_file_path);
  $path_to_new_image = getFilePathFromLink($site_id, $new_file_path);

  list( $source_width, $source_height, $source_type ) = getimagesize( $path_to_image ); // original sizes
  list( $o_watermark_width, $o_watermark_height, $watermark_type ) = getimagesize( $watermark ); // original sizes

  if(is_file($path_to_new_image)){ return $new_file_path;  }

  if ( $source_type === NULL ){ return '2';}

  switch ( $source_type ){
    case IMAGETYPE_JPEG: $source_gd_image = imagecreatefromjpeg( $path_to_image ); break;
    case IMAGETYPE_PNG: $source_gd_image = imagecreatefrompng( $path_to_image ); break;
    default: return '3';
  }

  switch ( $watermark_type ){
    case IMAGETYPE_JPEG: $watermark_gd_image = imagecreatefromjpeg( $watermark ); break;
    case IMAGETYPE_PNG: $watermark_gd_image = imagecreatefrompng( $watermark ); break;
    default: return '4';
  }

	$ratio = false;
  if($o_watermark_width > $source_width && $o_watermark_height > $source_height){  // if watermark is wider and taller we need to resize watermark
  	$ratio = max(array($o_watermark_width / $source_width, $o_watermark_height / $source_height));
  }elseif($o_watermark_height > $source_height){  // if watermark is taller we need to resize watermark
    $ratio = $o_watermark_height / $source_height;
  }elseif($o_watermark_height > $source_height){  // if watermark is wider we need to resize watermark
    $ratio = $o_watermark_width / $source_width;
  }

  if($ratio){
  	$watermark_width = $o_watermark_width / $ratio;
    $watermark_height = $o_watermark_height / $ratio;
  }

  if($o_watermark_width / $source_width < 0.5){ // if watermark is too small
    $ratio = $o_watermark_width /($source_width * 0.5);
    $watermark_width = $o_watermark_width / $ratio;
    $watermark_height = $o_watermark_height / $ratio;
  }

  $watermark_width = ($watermark_width)? floor($watermark_width) : $o_watermark_width;
  $watermark_height = ($watermark_height)? floor($watermark_height) : $o_watermark_height;

  force_truecolor_image($watermark_gd_image);
  force_truecolor_image($source_gd_image);

  $waterm = imagecreatetruecolor($watermark_width, $watermark_height);
  imagealphablending($waterm, false);
  imagefill($waterm, 0, 0, imagecolortransparent($waterm, imagecolorallocatealpha($waterm, 0, 0, 0, 127))); // better do not try to understand this
  imagesavealpha($waterm, true);
  imagecopyresampled($waterm, $watermark_gd_image, 0, 0, 0, 0, $watermark_width, $watermark_height, $o_watermark_width, $o_watermark_height );
  filter_opacity($waterm, $opacity);

  // add watermark
  //imagealphablending($source_gd_image, false);
  imagesavealpha($source_gd_image, true);

  imagecopy($source_gd_image, $waterm, 0, $source_height-$watermark_height, 0, 0, imagesx($waterm), imagesy($waterm));
  imagepng( $source_gd_image, $path_to_new_image, $quality );

  imagedestroy( $source_gd_image );
  imagedestroy( $waterm );
  imagedestroy( $watermark_gd_image );

  return $new_file_path;

}

function filter_opacity( &$img, $opacity ){

  $opacity /= 100;

  //get image width and height
  $w = imagesx( $img );
  $h = imagesy( $img );

  //turn alpha blending off
  imagealphablending( $img, false );

  //find the most opaque pixel in the image (the one with the smallest alpha value)
  $minalpha = 127;
  for( $x = 0; $x < $w; $x++ )
      for( $y = 0; $y < $h; $y++ )
          {
              $alpha = ( imagecolorat( $img, $x, $y ) >> 24 ) & 0xFF;
              if( $alpha < $minalpha )
                  { $minalpha = $alpha; }
          }

  //loop through image pixels and modify alpha for each
  for( $x = 0; $x < $w; $x++ )
      {
          for( $y = 0; $y < $h; $y++ )
              {
                  //get current alpha value (represents the TANSPARENCY!)
                  $colorxy = imagecolorat( $img, $x, $y );
                  $alpha = ( $colorxy >> 24 ) & 0xFF;
                  //calculate new alpha
                  if( $minalpha !== 127 )
                      { $alpha = 127 + 127 * $opacity * ( $alpha - 127 ) / ( 127 - $minalpha ); }
                  else
                      { $alpha += 127 * $opacity; }
                  //get the color index with new alpha
                  $alphacolorxy = imagecolorallocatealpha( $img, ( $colorxy >> 16 ) & 0xFF, ( $colorxy >> 8 ) & 0xFF, $colorxy & 0xFF, $alpha );
                  //set pixel with the new color + opacity
                  if( !imagesetpixel( $img, $x, $y, $alphacolorxy ) )
                      { return false; }
              }
      }
  return true;
}

function DeleteFile($directory, $relpath='') {
  if ($_GET['file'] != "" ) {
    $files = explode(";",$_GET['file']);
    foreach ($files as $key=>$file)
      {
        $fname = fileAddBackslash($directory).$file;
        fileDelete($fname, true);
        Add_Log("Deleted file '$fname'", $GLOBALS['site_id']);
        //if($relpath)
        if(($relpath)and(($GLOBALS['index_htm_files'])or($GLOBALS['index_doc_files'])))
        {
          require_once ($GLOBALS['cfgDirRoot']."library/".'func.search.php');
          IndexFileForSearch($GLOBALS['site_id'], fileDropStartBackslash(fileAddBackslash($relpath)).$file);
          sqlQuery(" DELETE FROM ".$GLOBALS['site_id']."_filesearch_object WHERE filename='".fileDropStartBackslash(fileAddBackslash($relpath)).$file."'");
          sqlQuery(" DELETE FROM ".$GLOBALS['site_id']."_filesearch_link WHERE filename='".fileDropStartBackslash(fileAddBackslash($relpath)).$file."'");
          sqlQuery(" DELETE FROM ".$GLOBALS['site_id']."_filesearch_keywordlink WHERE file_path ='".fileDropStartBackslash(fileAddBackslash($relpath)).$file."'");
        }
      }
  }
}

function RenameFile($directory, $oldfile, $newfile) {
  if ($newfile != "" ) {
    $oldfname = fileAddBackslash($directory).$oldfile;
    $newfname = fileAddBackslash($directory).$newfile;
    rename($oldfname, $newfname);
    Add_Log("Renamed file '$oldfname' to '$newfname'", $GLOBALS['site_id']);
  }
}

function CopyFile($fromfile, $tofile) {
    if (!copy($fromfile, $tofile)) {
        //print ("error while copying file $fromfname<br>\n");
        //Add_Log("Erorr while copying file from '$fromfile' to '$tofile'", $GLOBALS['site_id']);
    }
    else Add_Log("Copy file from '$fromfile' to '$tofile'", $GLOBALS['site_id']);
}

function MoveFile($fromfile, $tofile) {
    if ($fromfile!=$tofile)
        if (!copy($fromfile, $tofile)) {
            //print ("error while copying file $fromfile<br>\n");
            //Add_Log("Erorr while moving file from '$fromfile' to '$tofile'", $GLOBALS['site_id']);
        }
        else 
        {
            fileDelete($fromfile);
            Add_Log("Move file from '$fromfile' to '$tofile'", $GLOBALS['site_id']);
        }
}

function fileSizeStr($file){
  $file_size = filesize($file);
  if($file_size >= 1073741824) {
    $file_size = round($file_size / 1073741824 * 100) / 100 . "g";
  } elseif($file_size >= 1048576) {
    $file_size = round($file_size / 1048576 * 100) / 100 . "m";
  } elseif($file_size >= 1024) {
    $file_size = round($file_size / 1024 * 100) / 100 . "k";
  } else {
    $file_size = $file_size . "b";
  }
  return $file_size;
}

function fileDelete($name, $recursive=false)
{
  if(is_dir($name))
  {
    if ($recursive)
      recursive_remove_directory($name);
    else
      rmdir($name);

  } else {
    unlink($name);
  }
}

function fileUp($path) {
  $path = fileDropBackslash($path);
  $pos = strrpos($path, '/');
  if (!($pos === false)) $path = substr($path, 0, $pos); else $path = '';
  return($path);
}


//get full file path in filesystem from relative url
function getFilePathFromLink($site_id, $webpath)
{
  static $dirs = Array();

  if (!isset($dirs[$site_id]))
    $dirs[$site_id] = fileAddBackslash( sqlQueryValue("select dirroot from sites where site_id=".$site_id) );

  return $dirs[$site_id] . trim($webpath, '/');
}

//$file = elements no $_FILES masiiva
//$path = relatiivais celjsh, uz kuru augshupielaadeet, piemeeram, upload/iesutitie/
function uploadSiteFile($site_id, $file, $path)
{
      if(!$file['size'])
        return false;

      $file_name = basename($file['name']);
      $file_name = strtolower(str_replace(' ', '_', $file_name));
      $domain = sqlQueryValue("select domain from sites where site_id=".$site_id);
      $uploaddir = sqlQueryValue("select dirroot from sites where site_id=".$site_id);
      $uploaddir .= $path;
			
			if (!file_exists($uploaddir)) {
				mkdir($uploaddir, 0755, true);
			}
				
      $target_path = $uploaddir . $file_name;
      $target_path = str_replace("//", "/", $target_path);

      while(file_exists($target_path))
      {
          $timestamp = time();
          $file_name = rand(1,999999)."_".$file_name;
          $target_path = $uploaddir.$file_name;
      }

      $webpath = 'http://'.$domain.'/' . $path . $file_name;
      $shortwebpath = '/' . $path . $file_name;

      $shortwebpath = str_replace("//", "/", $shortwebpath);
      $webpath = str_replace("//", "/", $webpath);

      if(move_uploaded_file($file['tmp_name'], $target_path))
      {

          @chmod($target_path, 0755);
          $arr = array('fullurl' => $webpath, 'url' => $shortwebpath, 'filename' => $file_name, 'path' => $target_path);
          return $arr;
      }
      else
      {
        return false;
      }
}

//path = relatiivais celjsh (images/galleries/thumbs/)
function forceDirectories($site_id, $path)
{
  $uploaddir = getFilePathFromLink($site_id, '/');
  $path = explode('/', $path);
  foreach($path as $dir)
  {
    if($dir)
    {
      $uploaddir .= $dir;
      if (!is_dir($uploaddir))
      {
        mkdir($uploaddir);
        chmod($uploaddir, 0755);
      }
      $uploaddir .= '/';
    }
  }
}

//filename = pilnais celjsh
function fileDuplicate($site_id, $filename, $prefix = '', $destpath = '')
{
  $file_name = $prefix . basename($filename);
  $domain = sqlQueryValue("select domain from sites where site_id=".$site_id);
  $uploaddir = sqlQueryValue("select dirroot from sites where site_id=".$site_id);
  if($destpath)
  {
    $uploaddir .= $destpath;
    $path = $destpath;
  }else
  {
    $uploaddir2 = dirname($filename) . '/';
    if(substr($uploaddir2, 0, strlen($uploaddir)) == $uploaddir)
    {
      $path = substr($uploaddir2, strlen($uploaddir));
      $uploaddir = $uploaddir2;
    }else
    {
      $path = false;
    }
  }

  $target_path = $uploaddir . $file_name;

  while(file_exists($target_path))
  {
          $timestamp = time();
          $file_name = rand(1,999999)."_".$file_name;
          $target_path = $uploaddir.$file_name;
  }

  copy($filename, $target_path);
  @chmod($target_path, 0755);

  $webpath = 'http://'.$domain.'/' . $path . $file_name;
  $shortwebpath = '/' . $path . $file_name;
  $arr = array('fullurl' => $webpath, 'url' => $shortwebpath, 'filename' => $file_name, 'path' => $target_path);
  return $arr;
}

//will resize image file located in $filename, overwriting it
//$filename = full path in file system
//resize mode
// 0 - none
// 1 - resize by width
// 2 - resize by height
// 3 - resize by width if too big
// 4 - resize by height if too big
// 5 - resize by width and height to fit
// 6 - resize by width and height to fit if too big
// 7 - crop to fit in width and height exactly


function resizeImage($filename, $width, $height, $resizemode)
{
  $type = substr($filename, strrpos($filename, '.') + 1);
  if($resizemode > 0 && $resizemode < 11)
  {
    resizeKeepAspectUploadedImage($filename, $type, $width, $height, $filename, $resizemode);
  }else
  {
    resizeCropUploadedImage($filename, $type, $width, $height, $filename);
  }
  return true;
}


function resizeCropUploadedImage($srcfile, $type, $w, $h, $new_name, $resizemode)
{
    $uploadfile = $srcfile;
    $type = strtolower($type);
    if ($type == "gif")
        $abc = imagecreatefromGIF($uploadfile); //UPLOADED PIC FILE
    else if ($type == "jpg" || $type == "jpeg")
        $abc = imagecreatefromjpeg($uploadfile); //UPLOADED PIC FILE
    else if ($type == "png")
        $abc = imagecreatefrompng($uploadfile); //UPLOADED PIC FILE
    else
        return false;

    if (!$abc)
    {
      $abc = @imagecreatefromjpeg($uploadfile);
      if (!$abc) $abc = @imagecreatefrompng($uploadfile);
      if (!$abc)
      {
        $abc = @imagecreatefromgif($uploadfile);
      }
      if (!$abc) return false;
    }

    $thumbwidth = $w;
    $thumbheight = $h;

    //calc srcx, srcy, srcw and srch
    if($thumbheight / $thumbwidth < ImageSY($abc) / ImageSX($abc))
    {
      $srcw = ImageSX($abc);
      $srch = $thumbheight * (ImageSX($abc) / $thumbwidth);
    }else
    {
      $srcw = $thumbwidth * (ImageSY($abc) / $thumbheight);
      $srch = ImageSY($abc);
    }
    $srcx = round((ImageSX($abc) - $srcw) / 2);

    switch ($resizemode)
    {
      case 7:
        $srcy = round((ImageSY($abc) - $srch) / 2);
        break;
      case 8:
        $srcy = 0;
        break;
      case 9:
        $srcy = ImageSY($abc) - $srch;
        break;

    }

    $def = @imagecreatetruecolor($thumbwidth, $thumbheight) or die("Cannot Initialize new GD image stream");

    if ($type == 'png')
    {
      imagealphablending($def, false);
      imagesavealpha($def, true);
    }

    imagecopyresampled($def, $abc, 0, 0, $srcx, $srcy, $thumbwidth, $thumbheight, $srcw, $srch);

    ImageDestroy($abc);

    if ($type == 'gif')
      imagegif($def, $new_name);
    else if ($type == 'png')
      imagepng($def, $new_name, 9);
    else
      imagejpeg($def, $new_name, 90);  //new file from $def

    ImageDestroy($def);

}

function resizeKeepAspectUploadedImage($srcfile, $type, $w, $h, $new_name, $resizemode)
{
    $uploadfile = $srcfile;
    $type = strtolower($type);

    if ($type == "gif")
        $abc = imagecreatefromGIF($uploadfile); //UPLOADED PIC FILE
    else if ($type == "jpg" || $type == "jpeg")
        $abc = imagecreatefromjpeg($uploadfile); //UPLOADED PIC FILE
    else if ($type == "png")
        $abc = imagecreatefrompng($uploadfile); //UPLOADED PIC FILE

    $is_png = false;

    if (!$abc)
    {
      $abc = @imagecreatefromjpeg($uploadfile);
      if (!$abc)
      {
        $abc = @imagecreatefrompng($uploadfile);
        $is_png = true;
      }
      else if (!$abc)
      {
        $abc = @imagecreatefromgif($uploadfile);
      }
      if (!$abc) return false;
    }

    $thumbwidth = $w;
    $thumbheight = $h;

    //check if resizing is even required
    if(($resizemode != 3 || ImageSX($abc) > $w) && ($resizemode != 4 || ImageSY($abc) > $h) && ($resizemode != 6 || $resizemode != 10 || ImageSX($abc) > $w || ImageSY($abc) > $h))
    {
      //calculate required thumbwidth and thumbheight
      if($resizemode == 1 || $resizemode == 3)
      {
        //resize by width
        $thumbheight = round(ImageSY($abc) * ($thumbwidth/ImageSX($abc)));
      }else if($resizemode == 2 || $resizemode == 4)
      {
        //resize by height
        $thumbwidth = round(ImageSX($abc) * ($thumbheight/ImageSY($abc)));
      }else if($resizemode == 5 || $resizemode == 6)
      {
        //resize by width and height
        $new_w = round(ImageSX($abc) * ($thumbheight/ImageSY($abc)));
        if($new_w <= $thumbwidth)
        {
          $thumbwidth = $new_w;
        }else
        {
          $thumbheight = round(ImageSY($abc) * ($thumbwidth/ImageSX($abc)));
        }
      }

      $def = @imagecreatetruecolor($thumbwidth, $thumbheight) or die("Cannot Initialize new GD image stream");

      if ($is_png || $type == 'png')
      {
        imagealphablending($def, false);
        imagesavealpha($def, true);
      }

      imagecopyresampled($def, $abc, 0, 0, 0, 0, $thumbwidth, $thumbheight, ImageSX($abc), ImageSY($abc));

      ImageDestroy($abc);

      if ($type == 'gif')
        imagegif($def, $new_name);
      else if ($is_png || $type == 'png')
        imagepng($def, $new_name, 9);
      else
        imagejpeg($def, $new_name, 90);  //new file from $def

      ImageDestroy($def);
      return true;
    }else
    {
      //no resize required
      ImageDestroy($abc);
      return false;
    }
}

function getThumbURL($src_url, $width, $height, $resizemode, $site_id=0, $thumb_dir='th')
{
  if (!$src_url || !$width || !$height) return '';

  if (!$resizemode) return $src_url;

  if (!$site_id)
  {
    $site_id = $GLOBALS['site_id'];
    if (!$site_id) return '';
  }

  $thumb_dir = fileDropBackslash($thumb_dir);

  $src_path = fileDropBackslash(getFilePathFromLink($site_id, $src_url));

  if(!file_exists($src_path)){
    return false;
  }

  $dir = dirname($src_path);

  $th_conf = $thumb_dir.'/'.$width.'x'.$height.'_'.$resizemode;

  $thumb_path = getFilePathFromLink($site_id, dirname($src_url));

  $th_dir = fileAddBackslash($thumb_path).$th_conf;

  $basename = basename($src_url);
  $th_path = $th_dir.'/'.$basename;
  $th_url_path = fileAddBackslash(dirname($src_url)).$th_conf;
  $th_url = $th_url_path.'/'.$basename;

  $src_time = file_exists($src_path) ? filemtime($src_path) : false;
  $th_time = file_exists($th_path) ? filemtime($th_path) : false;

  if (file_exists($src_path) && file_exists($th_path) && $src_time && $src_time == $th_time) return $th_url;

  forceDirectories($site_id, $th_url_path);

  $type = substr($basename, strrpos($basename, '.') + 1);
  if(in_array($resizemode, array(7, 8, 9)))
  {
    resizeCropUploadedImage($src_path, $type, $width, $height, $th_path, $resizemode);

  }else
  {
    if (!resizeKeepAspectUploadedImage($src_path, $type, $width, $height, $th_path, $resizemode))
      copy($src_path, $th_path);

  }

  touch($th_path, $src_time);

  return $th_url;
}


function deleteThumbnails($image_url, $site_id=0, $thumb_dir='th')
{
  if (!$site_id)
  {
    $site_id = $GLOBALS['site_id'];
    if (!$site_id) return '';
  }

  $path = getFilePathFromLink($site_id, $image_url);
  $dir = dirname($path);
  $filename = basename($path);
  foreach (glob($dir.'/'.$thumb_dir.'/*x*_*') as $dir)
  {
    foreach (glob($dir.'/'.$filename) as $f)
    {
      @unlink($f);
    }
    @rmdir($dir);
  }


}

function getFileExtension($file){

	preg_match("/(\.)([a-z0-9]{3,999})$/", $file, $matches);
	return $matches[0];

}

function getFormFile($key, $site_id=0, $uploaddir='upload/')
{
  if (!isset($_POST[$key.'_file_select_mode']) || !isset($_FILES[$key])) return '';
  if (!$site_id) $site_id = $GLOBALS['site_id'];

  if ($_POST[$key.'_file_select_mode'] == 'upload')
  {
    if ($uploaddir) forceDirectories($site_id, $uploaddir);
    $f = uploadSiteFile($site_id, $_FILES[$key], $uploaddir);
    $url = $f['url'];
  }
  else
    $url = $_POST[$key.'_link'];

  return $url;
}


function recursive_remove_directory($directory, $empty=FALSE)
 {
     // if the path has a slash at the end we remove it here
     if(substr($directory,-1) == '/')
     {
         $directory = substr($directory,0,-1);
     }

     // if the path is not valid or is not a directory ...
     if(!file_exists($directory) || !is_dir($directory))
     {
         // ... we return false and exit the function
         return FALSE;

     // ... if the path is not readable
     }elseif(!is_readable($directory))
     {
         // ... we return false and exit the function
         return FALSE;

     // ... else if the path is readable
     }else{

         // we open the directory
         $handle = opendir($directory);

         // and scan through the items inside
         while (FALSE !== ($item = readdir($handle)))
         {
            // if the filepointer is not the current directory
             // or the parent directory
             if($item != '.' && $item != '..')
             {
                 // we build the new path to delete
                 $path = $directory.'/'.$item;

                 // if the new path is a directory
                 if(is_dir($path))
                 {
                     // we call this function with the new path
                     recursive_remove_directory($path);

                 // if the new path is a file
                 }else{
                     // we remove the file
                     unlink($path);
                 }
             }
         }
         // close the directory
         closedir($handle);

         // if the option to empty is not set to true
         if($empty == FALSE)
         {
             // try to delete the now empty directory
             if(!rmdir($directory))
             {
                 // return false if not possible
                 return FALSE;
             }
         }
         // return success
         return TRUE;
     }
 }


function force_truecolor_image($img)
{
  $w = imagesx($img);
  $h = imagesy($img);

  if (!imageistruecolor($img))
  {
    $truecolor = imagecreatetruecolor($w, $h);
    imagealphablending($img, false);
    imagesavealpha($img, true);
    imagecopy($truecolor, $img, 0, 0, 0, 0, $w, $h);
    imagedestroy($img);
    $img = $truecolor;
  }
  return $img;
}

function watermark_image($img, $watermark, $wf, $hf, $alpha=15)
{
  $w_width = round(imagesx($img) * $wf);
  $w_height = round(imagesy($watermark) * ($w_width/imagesx($watermark)) );

  $max_h = imagesy($img) * $hf ;
  if ($w_height > $max_h)
  {
    $h = $max_h; //round(imagesy($img) * 0.3);
    $w_width = round($w_width * ($h/$w_height) );
    $w_height = $h;
  }
  $waterm = imagecreatetruecolor($w_width, $w_height);

  $x = (imagesx($img) - imagesx($waterm)) * 0.96;
  $y = (imagesy($img) - imagesy($waterm)) * 0.96;

//  imagealphablending($waterm, false);

  imagealphablending($waterm, false);
  $color = imagecolortransparent($waterm, imagecolorallocatealpha($waterm, 0, 0, 0, 127));
  imagefill($waterm, 0, 0, $color);
  imagesavealpha($waterm, true);

  imagecopyresampled($waterm, $watermark, 0,0,0,0, $w_width, $w_height, imagesx($watermark), imagesy($watermark) );

  imagecopy($img, $waterm, $x, $y, 0,0, imagesx($waterm), imagesy($waterm));
}

function watermark_file($f, $outfile, $watermark, $wf, $hf, $alpha=15)
{
  $ext = strtolower(pathinfo($f, PATHINFO_EXTENSION));

  getimagesize($f, $info);

  if ($info || $ext == 'jpeg' || $ext == 'jpg')
    $img = @imagecreatefromjpeg($f);
  else if ($ext == 'png')
    $img = @imagecreatefrompng($f);
  else if ($ext == 'gif')
    $img = @imagecreatefromgif($f);

  if (!$img)
  {
    $img = @imagecreatefromjpeg($f);
    if (!$img) $img = @imagecreatefrompng($f);
    if (!$img) $img = @imagecreatefromgif($f);
    if (!$img) return false;
  }

  $img = force_truecolor_image($img);

  watermark_image($img, $watermark, $wf, $hf, $alpha);

//  imagecopymerge($img, $waterm, $x, $y, 0,0, imagesx($waterm), imagesy($waterm), $alpha);

  if ($info || $ext == 'jpeg' || $ext == 'jpg')
    imagejpeg($img, $outfile, 90);
  else if ($ext == 'png')
    imagepng($img, $outfile);
  else if ($ext == 'gif')
    imagegif($img, $outfile);

  return true;
}

?>
