<?php

use Constructor\WebApp;

class OrderGenerator
{

    private $fields;
    private $cart;
    private $user;
    private $id;
    private $lang;

    public function __construct($fields, $cart, $user, $id, $order = [])
    {

        $this->fields = $fields;
        $this->cart = $cart;
        $this->user = $user;
        $this->id = $id;

        $this->init();

    }

    public function getHtml($lang, $invoice = false)
    {

        $months = explode(',', WebApp::l("janvāris,februāris,marts,aprīlis,maijs,jūnijs,jūlijs,augusts,septembris,oktobris,novembris,decembris", ['lang' => $lang]));

        if($order){

            $time = strtotime($order['datums']);
            $date = date('Y', $time).'. '.WebApp::l("gada", ['lang' => $lang]).' '.date('d', $time).'. '.$months[date('m', $time)-1];

        }else{
            $date = date('Y').'. '.WebApp::l("gada", ['lang' => $lang]).' '.date('d').'. '.$months[date('m')-1];
        }

        $doc_type = $invoice ? WebApp::l("Pavadzīmes nr.", ['lang' => $this->lang]) : WebApp::l("Pasūtījuma nr.", ['lang' => $this->lang]);
        $prefix = $invoice ? $this->invoice_prefix : $this->order_prefix;

        ob_start();
        ?>
        <table id="ordertable<?php echo $id; ?>" style="page-break-inside:avoid" width="100%"  border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td colspan="1" rowspan="2"><?php ($logo ? '<img src="'.$logo.'" alt="" style="max-height: 100px">' : '')?></td>
                <td colspan="4" align="right"><b><?php echo $doc_type.'.: '.$prefix.' '.$id?></b></td>
            </tr>
            <tr>
                <td colspan="4" align="right"><b><?php echo $date?></b></td>
            </tr>
            <tr>
                <td colspan="5"><hr /></td>
            </tr>
            <tr>
                <td><strong><span class="mazsTd"><?php echo WebApp::l("Precu izsniedzējs", ['lang' => $lang])?>:</span></strong></td>
                <td><strong><span class="mazsTd"><?php echo WebApp::l('"Firma SIA"', ['lang' => $lang])?></span></strong></td>
            </tr>
            <tr>
                <td><span class="mazsTd"><?php echo WebApp::l("Reģistrācijas numurs", ['lang' => $lang])?>:</span></td>
                <td><span class="mazsTd"><<?php echo WebApp::l("400000000123", ['lang' => $lang])?></span></td>
            </tr>
            <tr>
                <td><span class="mazsTd"><?php echo WebApp::l("Juridiskā adrese", ['lang' => $lang])?>:</span></td>
                <td><span class="mazsTd"><<?php echo WebApp::l("Kr. Barona iela 129, Rīga, LV-1012", ['lang' => $lang])?></span></td>
            </tr>
            <tr>
                <td></td>
            </tr>
            <tr>
                <td><span class="mazsTd"><?php echo WebApp::l("Bankas nosaukums", ['lang' => $lang])?>:</span></td>
                <td><span class="mazsTd"><<?php echo WebApp::l("Hansabanka", ['lang' => $lang])?></span></td>
            </tr>
            <tr>
                <td><span class="mazsTd"><?php echo WebApp::l("Bankas kods", ['lang' => $lang])?>:</span></td>
                <td><span class="mazsTd"><<?php echo WebApp::l("HABALV22", ['lang' => $lang])?></span></td>
            </tr>
            <tr>
                <td><span class="mazsTd"><?php echo WebApp::l("Bankas konts", ['lang' => $lang])?>:</span></td>
                <td><span class="mazsTd"><<?php echo WebApp::l("LV52HABA0551005111951", ['lang' => $lang])?></span></td>
            </tr>
            <tr>
                <td><span class="mazsTd"><?php echo WebApp::l("Tālrunis", ['lang' => $lang])?>:</span></td>
                <td><span class="mazsTd"><<?php echo WebApp::l("6777 0077", ['lang' => $lang])?></span></td>
            </tr>
            <tr>
                <td colspan="5"><hr /></td>
            </tr>
        </table>
        <?php
        $html = ob_get_clean();
        return $html;

    }

    private function init()
    {

        option('Shop\\OrderForm', '', 'Order form');
        $optcat = getOptionByPath('Shop\\OrderForm');
        $params = Array('parent_id' => $optcat['id']);

        $this->order_prefix = option('order_prefix', null, 'Order prefix', $params, 'I-NET');
        $this->invoice_prefix = option('order_prefix', null, 'Invoice prefix', $params, 'I-INVOICE');

    }

}