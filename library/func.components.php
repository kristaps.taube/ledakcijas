<?
  //Functions for processing components, parsing and extracting components from HTML code

  function OutputComponentPlain($name, $type, $page_id)
  {
    $filename = $GLOBALS['cfgDirRoot'] . "components/class.$type.php";
    if(file_exists($filename))
    {

        $GLOBALS['component_name'] = $name;
        $GLOBALS['page_id'] = $page_id;
        ob_start();
        include_once($filename);
        $obj = new $type($name);
        if(!$obj->getProperty('visible'))
            $obj->Output();

        $cont = ob_get_contents();
        $ret .= $cont;
        ob_end_clean();
        return $cont;
    }
  }

  function executeComponent(&$component, $type, $name){

    if($component !== false){
        $GLOBALS['component_name'] = $name;
        $GLOBALS['profiler_component'] = $name;
        $component->Execute();
        $GLOBALS['profiler_component'] = '';
    }

  }

  if(!function_exists('initializeComponent'))
  {
    function initializeComponent($type, $name)
    {
      $filename = getComponentFilePath($type);
      if(file_exists($filename))
      {
        $GLOBALS['component_name'] = $name;

        $GLOBALS['profiler_component'] = $name;
        profiler_add('Init component ' . $type . ' ' . $name, 3);

        include_once($filename);
        $obj = new $type($name);
        addDefaultProperties($obj);

        profiler_add('Init component ' . $type . ' ' . $name, 4);
        $GLOBALS['profiler_component'] = '';

        return $obj;
      }else
      {
        return false;
      }
    }
  }

  function createComponentInstance($name, $type, $page_id=0, $devmode=false)
  {
    $filename = getComponentFilePath($type);

    if (!file_exists($filename))
      return null;

    include_once($filename);

    if (!class_exists($type))
      return null;

    $obj = new $type($name);

    if ($page_id)
      $obj->initForPage($page_id, $devmode);

    return $obj;
  }

  function getDynamicComponentInfo($id, $site_id=0)
  {
    if (!$site_id) $site_id = $GLOBALS['site_id'];
    return sqlQueryRow('SELECT * FROM '.$site_id.'_dynamic_components WHERE id='.(int)$id);
  }

  function createDynamicComponentInstance($id, $page_id, $devmode=false)
  {
    $cmp = getDynamicComponentInfo($id);
    if (!$cmp) return null;

    $name = str_replace('_', '', $cmp['type']);
    $obj = createComponentInstance($name, $cmp['type'], $page_id, $devmode);
    if ($obj)
    {
      $obj->component_id = $id;
      $obj->dynamic_component_info = $cmp;
    }

    return $obj;
  }

  function componentGetDynamicComponents($name, $type, $page_id, $ind)
  {
    $components = array();
    $obj = createComponentInstance($name, $type, $page_id, true);
    if ($obj)
    {
      $cmp_ids = $obj->getDynamicComponentsIDs($ind);
      $types = array();
      foreach ($cmp_ids as $id)
      {
        $cmp = getDynamicComponentInfo($id);
        if (!isset($types[$cmp['type']]))
        {
          $types[$cmp['type']] = 1;
          $components[] = array(
            'type' => $cmp['type'],
            'name' => str_replace('_', '', $cmp['type']),
            'options' => '',
            'ind' => $ind,
            'tag' => ''
          );
        }

      }

    }

    return $components;
  }

  //Functions returns array of all components in $body
  function componentArray($body, $page_id, $ind = 0){

    $r = Array();
    $rd = array();
    preg_match_all ( '/{%component:([a-zA-Z0-9_]+):([a-zA-Z0-9]+)(:[a-zA-Z0-9% \\.\\,\\\"\\\'\\#\\!-]+)?%}/s', $body, $matches, PREG_SET_ORDER);
    foreach($matches as $key => $regs){
    	$all = $regs[0];
      $type = $regs[1];
      $name = $regs[2];
      $opts = $regs[3];
      $r[] = Array("type" => $type, "name" => $name, "options" => $opts, "ind" => $ind, "tag" => $all);
    }

    return $r;

  }

  function getComponentParams($s)
  {  //function extracts parameters from string
     //parameters are in form
     //:paramname1 "paramval" paramname2 "param""val"
     $isval = false;
     $name = '';
     $value = '';
     $a = Array();
     for($f = 1; $f < strlen($s); $f++)
     {
       $c = $s{$f};
       if($isval)
       {
         if($c == '"')
         {
           if($s{$f+1} == '"')
           {
             $f++;
             $value .= $c;
           }else
           {
             $isval = false;
             $name = trim($name);
             $a[$name] = $value;
             $name = '';
             $value = '';
           }
         }else
         {
           $value .= $c;
         }
       }else
       {
         if($c == '"')
         {
           $isval = true;
         }else
         {
           $name .= $c;

         }
       }
     }
     return($a);
  }


  function addDefaultProperties(&$obj = false)
  {
      //Add options visible/hidden to all components
      if(!$obj || !$obj->properties["visible"])
      {
        $obj->properties["visible"] = Array(
          "label"     => "Visibility:",
          "type"      => "list",
          "value"     => "0",
          "lookup"    => Array("0:Visible", "1:Hidden")
        );
      }
  }

  function getPropertiesTable($type)
  {
    if(file_exists($GLOBALS['cfgDirRoot'] . 'components/class.' . $type . '.php'))
    {
      //Obtain the array of properties for the component
      include_once($GLOBALS['cfgDirRoot'] . 'components/class.' . $type . '.php');
      $obj = new $type($name);
      addDefaultProperties($obj);

      return $obj->properties;
    }
    return Array();
  }

  function getComponentFilePath($type)
  {
    $path = '';
    $file = 'class.' . $type . '.php';
    if (strpos($type, '_') !== false)
    {
      $a = explode('_', $type);
      unset($a[count($a)-1]);
      $path = implode('/', $a).'/';
    }

    return $GLOBALS['cfgDirRoot'] . 'components/'.$path.$file;
  }

  //function has been rebuilt to work with pagesdev table
  function displayPropertiesTable($type, $name, $site_id, $page_id, &$form_data, $hide = false, $filterproperties = '')
  {
    $file = getComponentFilePath($type);

    if(file_exists($file))
    {
      //Obtain the array of properties for the component

      include_once($file);
      $obj = new $type($name);
      $obj->InitFormData();
      addDefaultProperties($obj);
      $properties = $obj->properties;

      $template_id = 0;
      $language_id = 0;

      if ($wap)
      {
          //if not template use wapcontentsdev table
          if ($page_id!=-1)
          {
            //Obtain the array of current values of properties
            $data = sqlQueryData("SELECT propertyname, propertyvalue FROM " . $site_id . "_wapcontentsdev WHERE wappagedev=$page_id AND componentname='$name'");
          }
          //else use wapcontents table
          else
          {
            //Obtain the array of current values of properties
            $data = sqlQueryData("SELECT propertyname, propertyvalue FROM " . $site_id . "_wapcontents WHERE wappage=$page_id AND componentname='$name'");
          }
      }
      else
      {
          if ($page_id>0)
          {
              //Obtain the array of current values of properties for page
              $data = sqlQueryData("SELECT propertyname, propertyvalue FROM " . $site_id . "_contentsdev WHERE pagedev=$page_id AND componentname='$name'");
              list($template_id, $language_id) = sqlQueryRow('SELECT template, language FROM ' . $site_id . '_pages WHERE page_id = ' . $page_id);
          }
          else
          {
              //Obtain the array of current values of properties for view
              $data = sqlQueryData("SELECT propertyname, propertyvalue FROM " . $site_id . "_contents WHERE page=$page_id AND componentname='$name'");
              list($template_id) = sqlQueryRow('SELECT template_id FROM ' . $site_id . '_views WHERE view_id = ' . Abs($page_id));
              $language_id = $_SESSION['currentlanguagenum'];
          }



      }
      $properties['name']['value'] = $name;
      //All values are default before we start
      foreach(array_keys($properties) as $property)
      {
        $properties[$property]['default'] = true;
      }
      //Find which values are not default, set them accordingly
      if($data)
      {
        foreach($data as $row)
        {
          if(in_array($row['propertyname'], array_keys($properties)))
          {
            $properties[$row['propertyname']]['value'] = $row['propertyvalue'];
            $properties[$row['propertyname']]['default'] = false;
          }
        }
      }
      $form_data['type_' . $name] = Array('value' => $type, 'type' => 'hidden');
      //Set final values and default values of properties
      $properties_keys = array_keys($properties);
      foreach($properties_keys as $property)
      {
        $uselanguageview = true;
        if($page_id != -1)
        {
          //if editing properties of page components
          if ($wap)
          {
              if($page_id > 0)
              {
                $copypage = sqlQueryValue("SELECT copypage FROM " . $site_id . "_wappagesdev WHERE wappagedev_id=$page_id");
              }
          }
          else
          {
              if($page_id > 0)
              {
                $copypage = sqlQueryValue("SELECT copypage FROM " . $site_id . "_pagesdev WHERE pagedev_id=$page_id");
              }else
              {
                $copypage = sqlQueryValue("SELECT -parent FROM " . $site_id . "_views WHERE view_id=" . Abs($page_id));
              }
          }
          $defaultProperty = null;
          $OldDefaultProperty = $properties[$property]['value'];
          if($copypage || $uselanguageview)
          {
            if ($wap)
            {
                //allow copypages to work in multiple levels
                while(($copypage)and($defaultProperty == null))
                {
                  $defaultProperty = sqlQueryValue("SELECT propertyvalue FROM  " . $site_id . "_wapcontentsdev WHERE wappagedev=$copypage AND componentname='" . $name . "' AND propertyname='" . $property . "'");
                }
            }
            else
            {
                //allow copypages to work in multiple levels
                while(($copypage)and($defaultProperty == null))
                {
                  $defaultProperty = sqlQueryValue("SELECT propertyvalue FROM  " . $site_id . "_contents WHERE page=$copypage AND componentname='" . $name . "' AND propertyname='" . $property . "'");
                  if($defaultProperty == null)
                  {
                      $copypage = sqlQueryValue("SELECT -parent FROM " . $site_id . "_views WHERE view_id=" . Abs($copypage));
                  }
                }
                if($defaultProperty == null)
                {
                  if($template_id > 0 && $language_id > 0 && $uselanguageview)
                  {
                    $copypage = sqlQueryValue('SELECT -view_id FROM ' . $site_id . '_views WHERE template_id = ' . $template_id . ' AND language_id = ' . $language_id);
                    if($copypage != $page_id && $copypage)
                    {
                      $defaultProperty = sqlQueryValue("SELECT propertyvalue FROM  " . $site_id . "_contents WHERE page=$copypage AND componentname='" . $name . "' AND propertyname='" . $property . "'");
                      $uselanguageview = false;
                    }else
                    {
                      $copypage = 0;
                    }
                  }
                }
            }
          }
          if ($wap)
          {
              if($defaultProperty==null)
              {
                $defaultProperty = sqlQueryValue("SELECT propertyvalue FROM  " . $site_id . "_wapcontents WHERE wappage=-1 AND componentname='" . $name . "' AND propertyname='" . $property . "'");
              }
          }
          else
          {
              if($defaultProperty==null)
              {
                $defaultProperty = sqlQueryValue("SELECT propertyvalue FROM  " . $site_id . "_contents WHERE page=-1 AND componentname='" . $name . "' AND propertyname='" . $property . "'");
              }
          }
          $properties[$property]['defval'] = parseForJScript($defaultProperty,true);
          if($properties[$property]['default'])
          {
            $properties[$property]['value'] = $defaultProperty;
          }
          if($properties[$property]['value'] == null)
            $properties[$property]['value'] = $OldDefaultProperty;
        }else
        {
          //If editing properties of template components
          $properties[$property]['defval'] = parseForJScript($properties[$property]['value'],true);
        }


        //$properties[$property]['label'] = $property . ": ";
        if($property == 'name')
        {
          $properties[$property]['type'] = 'title';
          $properties[$property]['label'] = $name . ", " . $type;
        }
        //Do not display some components in graphical mode
        if((!$hide)or(!$properties[$property]['hide']))
        {
          if(!$filterproperties)
          {
            $form_data["component_" . $name . "_" . $property] = $properties[$property];
          }else
          {
            $fp = explode(':', $filterproperties);
            if(in_array($property, $fp))
            {
              $form_data["component_" . $name . "_" . $property] = $properties[$property];
            }
          }
        }
      }
    }
  }

  function getProperty($property, $component_name, $type, $page, $site, $copypage=false, $dev=0){

  	static $cache = array();

    if ($dev){
    	$pageData = sqlQueryRow("SELECT propertyvalue FROM " . $site . "_contentsdev WHERE propertyname='" . $property . "' AND componentname='$component_name' AND pagedev=$page");
    }else{

    	if(empty($cache)){

      	$data = DB::GetTable("SELECT * FROM " . $site . "_contents");
				foreach($data as $row){
         	$cache[$row["propertyname"]][$row['componentname']][$row['page']] = $row['propertyvalue'];
				}

    	}

    	//$pageData = sqlQueryRow("SELECT propertyvalue FROM " . $site . "_contents WHERE propertyname='" . $property . "' AND componentname='$component_name' AND page=$page");

			$pageData = $cache[$property][$component_name][$page];

		}


		if(($pageData==null)and($page!=-1)){
      $data = null;

      if($copypage){
        while(($copypage)and($data == null)){

          if($cache[$property][$component_name][$copypage]){
         		$data = $cache[$property][$component_name][$copypage];
          }else{
         		$data = sqlQueryValue("SELECT propertyvalue FROM " . $site . "_contents WHERE propertyname='" . $property . "' AND componentname='$component_name' AND page=$copypage");
          }

					if($data == null){
            $copypage = sqlQueryValue("SELECT -parent FROM " . $site . "_views WHERE view_id=" . Abs($copypage));
          }

        }
      }

      if($data==null){
      	if($cache[$property][$component_name][-1]){
      		$data = $cache[$property][$component_name][-1];
      	}else{
        	$data = sqlQueryValue("SELECT propertyvalue FROM " . $site . "_contents WHERE propertyname='" . $property . "' AND componentname='$component_name' AND page=-1");
      	}
      }

    }else{
      $data = is_array($pageData) ? $pageData['propertyvalue'] : $pageData;
    }

    if($data==null){
      if($type){
        $properties = getPropertiesTable($type);
        $data = $properties[$property]['value'];
      }
    }

    return $data;

  }


  function setProperty($property, $value, $component_name, $page, $site, $dev)
  {
    if ($dev)
    {
        sqlQuery("DELETE FROM " . $site . "_contentsdev WHERE propertyname='" . $property . "' AND componentname='$component_name' AND pagedev=$page");
        sqlQuery("INSERT INTO " . $site . "_contentsdev (pagedev, componentname, propertyname, propertyvalue) VALUES ('$page', '$component_name', '$property', '$value')");
    }
    else
    {
        sqlQuery("DELETE FROM " . $site . "_contents WHERE propertyname='" . $property . "' AND componentname='$component_name' AND page=$page");
        sqlQuery("INSERT INTO " . $site . "_contents (page, componentname, propertyname, propertyvalue) VALUES ('$page', '$component_name', '$property', '$value')");
    }
  }


  function getPropertyMassive($property, $component_name, $type, $page, $site, $copypage, $dev=0)
  {
    if(isset($GLOBALS['cachedproperty'][$property][$component_name]))
    {
      return $GLOBALS['cachedproperty'][$property][$component_name];
    }
    else
    {
      if ($dev)
          $pageData = sqlQueryData("SELECT propertyvalue, componentname FROM " . $site . "_contentsdev WHERE propertyname='" . $property . "' AND pagedev=$page");
      else
          $pageData = sqlQueryData("SELECT propertyvalue, componentname FROM " . $site . "_contents WHERE propertyname='" . $property . "' AND page=$page");
      foreach($pageData as $row)
      {
        $GLOBALS['cachedproperty'][$property][$row['componentname']] = $row['propertyvalue'];
      }
      if($page!=-1)
      {
        $data = null;

        if($copypage)
        {
          while(($copypage))
          {
            //if ($dev)
            //    $data = sqlQueryValue("SELECT propertyvalue FROM " . $site . "_contentsdev WHERE propertyname='" . $property . "' AND componentname='$component_name' AND pagedev=$copypage");
            //else
            $data = sqlQueryData("SELECT propertyvalue, componentname FROM " . $site . "_contents WHERE propertyname='" . $property . "' AND page=$copypage");
            foreach($data as $row)
            {
              if(!isset($GLOBALS['cachedproperty'][$property][$row['componentname']]))
                $GLOBALS['cachedproperty'][$property][$row['componentname']] = $row['propertyvalue'];
            }
            $copypage = sqlQueryValue("SELECT -parent FROM " . $site . "_views WHERE view_id=" . Abs($copypage));

          }
        }

        //process language default
        $lang_id = $GLOBALS['language_id'];
        $template_id = $GLOBALS['template_id'];
        if($template_id && $lang_id)
        {
          if(!isset($GLOBALS['cachedlangview' . $template_id . '_' . $lang_id]))
          {
            $copypage = sqlQueryValue('SELECT -view_id FROM ' . $site . '_views WHERE template_id = ' . $template_id . ' AND language_id = ' . $lang_id);
            $GLOBALS['cachedlangview' . $template_id . '_' . $lang_id] = $copypage;
          }else
          {
            $copypage = $GLOBALS['cachedlangview' . $template_id . '_' . $lang_id];
          }
          if($copypage)
          {
            $data = sqlQueryData("SELECT propertyvalue, componentname FROM " . $site . "_contents WHERE propertyname='" . $property . "' AND page=$copypage");
            foreach($data as $row)
            {
              if(!isset($GLOBALS['cachedproperty'][$property][$row['componentname']]))
                $GLOBALS['cachedproperty'][$property][$row['componentname']] = $row['propertyvalue'];
            }
          }
        }

        $data = sqlQueryData("SELECT propertyvalue, componentname FROM " . $site . "_contents WHERE propertyname='" . $property . "' AND page=-1");
        foreach($data as $row)
        {
          if(!isset($GLOBALS['cachedproperty'][$property][$row['componentname']]))
            $GLOBALS['cachedproperty'][$property][$row['componentname']] = $row['propertyvalue'];
        }

      }

      $data = $GLOBALS['cachedproperty'][$property][$component_name];
      return $data;
    }
  }


  function setWapProperty($property, $value, $component_name, $page, $site, $dev)
  {
    if ($dev)
    {
        sqlQuery("DELETE FROM " . $site . "_wapcontentsdev WHERE propertyname='" . $property . "' AND componentname='$component_name' AND wappagedev=$page");
        sqlQuery("INSERT INTO " . $site . "_wapcontentsdev (wappagedev, componentname, propertyname, propertyvalue) VALUES ('$page', '$component_name', '$property', '$value')");
    }
    else
    {
        sqlQuery("DELETE FROM " . $site . "_wapcontents WHERE propertyname='" . $property . "' AND componentname='$component_name' AND wappage=$page");
        sqlQuery("INSERT INTO " . $site . "_wapcontents (wappage, componentname, propertyname, propertyvalue) VALUES ('$page', '$component_name', '$property', '$value')");
    }
  }


  function createComponentCombobox($site_id, $page_id, $selname, $template_editor=0)
  {
    if ($template_editor)
    {
      $id = $page_id;
      list($data, $copybody) = sqlQueryRow("SELECT body, copybody FROM " . $site_id . "_templates WHERE template_id=$id");
      while($copybody)
        list($data, $copybody) = sqlQueryRow("SELECT body, copybody FROM " . $site_id . "_templates WHERE template_id=$copybody");

      $data = attachSubTemplates($data, $site_id);

      $r = componentArray($data, 0);

      $html = "\n".'<select style="width: 100%;" name="selectcomponent" id="selectcomponent" size="1" onchange="changeComponentElement(\'div_\'+this.value,\'?module=pages&site_id=' .$site_id. '&page_id=' .$page_id. '&id=' .$page_id. '&action=updatecontrolsdef&noclose=1&template_editor=1\'+this.value)">';
    }
    else
    {
        list($template,$copypage) = sqlQueryRow("SELECT template,copypage FROM ". $site_id . "_pagesdev WHERE pagedev_id=$page_id");
        if($template)
        {
          list($data, $copybody) = sqlQueryRow("SELECT body, copybody FROM " . $site_id . "_templates WHERE template_id=$template");
          while($copybody)
            list($data, $copybody) = sqlQueryRow("SELECT body, copybody FROM " . $site_id . "_templates WHERE template_id=$copybody");

          $data = attachSubTemplates($data, $site_id);

        }
        $r = componentArray($data, $page_id);

        $id = sqlQueryValue("SELECT page_id FROM ".$site_id."_pagesdev WHERE pagedev_id=".$page_id);

        $html = "\n".'<select style="width: 100%;" name="selectcomponent" id="selectcomponent" size="1" onchange="changeComponentElement(\'div_\'+this.value,\'?module=pages&site_id=' .$site_id. '&pagedev_id=' .$page_id. '&id=' .$page_id. '&page_id='.$id.'&action=updatecontrols&noclose=1\'+this.value)">';
    }


    foreach($r as $row)
    {
      if (!$template_editor)
      {
          //get page_id of the original page from pages table
          $id = sqlQueryValue("SELECT page_id FROM ".$site_id."_pagesdev WHERE pagedev_id=$page_id");
          if(HasComponentPermissionPage($site_id, $id, $row['name']))
          {
            $hidden = getProperty("visible",$row['name'],$row['type'],$page_id,$site_id,$copypage);
            $opts = getComponentParams($row['options']);
            if($hidden)
            {
              $opt = "color:#999999;";
            }
            else
            {
              $opt = "color:#000000";
            }
            $html .= "\n".'<option';
            if($row['name'] == $selname)
            {
              $html .= ' selected="selected" ';
            }
            $html .= ' style="'.$opt.'" value="&component_type=' .$row['type']. '&component_name=' .$row['name'] .'">'.str_repeat("&nbsp;&nbsp;", $row['ind']).'('.$row['type'] .') ' . iif($opts['fullname'], $opts['fullname'], $row['name']) .'</option>';
          }
      }
      else
      {
          if(HasComponentPermission($site_id, $page_id, $row['name']))
          {
            $hidden = getProperty("visible",$row['name'],$row['type'],-1,$site_id,$copypage);
            $opts = getComponentParams($row['options']);
            if($hidden)
            {
              $opt = "color:#999999;";
            }
            else
            {
              $opt = "color:#000000";
            }
            $html .= "\n".'<option';
            if($row['name'] == $selname)
            {
              $html .= ' selected="selected" ';
            }
            $html .= ' style="'.$opt.'" value="&component_type=' .$row['type']. '&component_name=' .$row['name'] .'">'.str_repeat("&nbsp;&nbsp;", $row['ind']).'('.$row['type'] .') ' . iif($opts['fullname'], $opts['fullname'], $row['name']) .'</option>';
          }
      }
    }
    $html .= '</select>';

    return $html;
  }

  function borderDiv($name = '', $contentEditable=true)
  {
    if ($contentEditable==false) $a = " contenteditable=false";
    else $a = "";
    return '<div'.$a.' id="div_'.$name.'_borderdiv" name="div_'.$name.'_borderdiv" class="borderdiv" style="border: 1px dashed #808080; left: 0px; top: 0px; width: 100%; min-height: 100%; position: absolute; display: block; z-index: 1;" onmouseover="if(!cnst.isIE) this.style.display=\'none\';"><img src="/images/1x1.gif" width="1" /></div>';
  }



  function outputGraphicalComponent(&$hascontent, $type, $name, $page_id, $noDiv, $paramstr = '', $dontprocessinnercomponents = false, &$toolbarfunc, $skippermissioncheck = false, $pagedev_id, $canloaddynamically = false)
  {
    global $site_id;
    $filename = getComponentFilePath($type);
    $hascontent = false;
    if(file_exists($filename))
    {
      $GLOBALS['design_view']=true;
      profiler_add('Design component ' . $type . ' ' . $name, 5);
      include_once($filename);
      $obj = new $type($name);
      $obj->dev = true;
      $obj->visualedit = true;

      $obj->componentParamStr = $paramstr;
      $params = getComponentParams($paramstr);
      $obj->RefineParams($params);
      $obj->componentParams = $params;

      //get copypage for page_id
      $copypage = sqlQueryValue("SELECT copypage FROM ".$site_id."_pagesdev WHERE pagedev_id=$pagedev_id");

      $hastoolbar = $obj->toolbar($script, $toolbars, $btnids, $onclick);

      if($hastoolbar)
      {
        $scripts = "\n".'SelectComponent(document.getElementById("div_'.$name.'"),\''.$name.'\',\''.$type.'\',1);ClearAllToolbars(1);';
        $c = 0;
        if(is_array($toolbars)){
            foreach($toolbars as $t)
            {
              $c++;
              $scripts .= "\n".'SetToolbarHTML(' . $c . ',\''.parseForJScript($t,true).'\', 1);';
            }
        }
        if(is_array($btnids)){
            foreach($btnids as $btnid)
            {
              if (preg_match("/CMB$/",$btnid))
                {
                  $scripts .= "\n".'window.parent.window.document.all["'.$btnid.'"].onchange = window.'.$btnid.'_onchange;';
                }
              else
                {
                    $scripts .= "\n".'window.parent.window.document.all["'.$btnid.'"].onclick = window.'.$btnid.'_onclick;';
                }
            }
        }
        $scripts .= $onclick;

        $toolbarfunc = $script . "\n" . $scripts;

      }

      if($noDiv)
      {
        $ret = '';
      }else
      {
        if(!$skippermissioncheck)
        {
          //get permissions from original page
          if(!HasComponentPermissionPage($site_id, $page_id, $name))
          {
            $params['novisualedit'] = true;
          }
        }

        $visualhidden = $params['novisualvisible'];
        if((!$params['novisualedit'])and(!$visualhidden))
        {
          $hidden = getPropertyMassive('visible', $name, $type, $pagedev_id, $site_id, $copypage, $dev=1);
          if($hidden)
          {
            $background = '';
            $hiddenstyle = ' display: none;';
          }else
          {
            $background = '';
            $hiddenstyle = ' display: inline;';
            if($params['outborder'] && !$params['forceinline'])
              $hiddenstyle = ' display: block;';
          }
          if($params['outborder'] && !$params['noborder'])
          {
            $borderstyle = 'border: 1px dashed #808080;';
          }else
          {
            $borderstyle = '';
          }
          $domain = $GLOBALS['domain'];

          $class = iif($params['class'], '', 'graphicaleditdiv');

          $ret = '<div class="'.$class.'" id="div_'.$name.'" name="div_'.$name.'" style="'.$background.'cursor:default;width: '.iif($params['width'],$params['width'],'100%').'; min-height: '.iif($params['height'],$params['height'],'100%').';position:relative;'.$borderstyle.$hiddenstyle;
          $ret .= iif($params['class'], '" class="'.$params['class'].'', '');

          $ret .= '"';


          if(!$hastoolbar)
          {
            if(!$params['noproperties'])
            {
              $ret .= "\n".' onClick="javascript:SelectComponent(document.getElementById(\'div_'.$name.'\'),\''.$name.'\',\''.$type.'\',1);ClearAllToolbars(1);PropertiesWindow(\'http://' . $domain . '/edit.php?module=pages&site_id='.$site_id.'&id='.$pagedev_id.'&page_id='.$page_id.'&component_name='.$name.'&component_type='.$type.'\',\'&component_type=' .$type. '&component_name=' .$name .'\' , this, 400, 400, 1, 1);"';
            }else
            {
              $ret .= "\n".' onClick="javascript:SelectComponent(document.getElementById(\'div_'.$name.'\'),\''.$name.'\',\''.$type.'\',1);ClearAllToolbars(1);"';
            }
          }
          else
          {
            $ret .= "\n".' onClick="if(window.parent.SelectedComponent != this) eval(document.getElementById(\'div_'.$name.'\').set_tb_cnt);ComponentEditStart(document.getElementById(\'div_'.$name.'\'),\'{%langstr:after_you_have_finished%}\',\'{%langstr:click_here_save%}\');"';
          }
          $ret .= "\n".' onMouseOver="javascript:try{this'.(!$params['outborder']?'.firstChild':'').'.style.borderColor=\'red\'}catch(e){}" onMouseOut="javascript:try{this'.(!$params['outborder']?'.firstChild':'').'.style.borderColor=\'#808080\'}catch(c){}"';
          $ret .= ' >';
          if(!$params['noborder']&&!$params['outborder']){
            $ret .= BorderDiv($name);
          }

          if($hastoolbar && !$hidden && (!$canloaddynamically || !$obj->CanLoadDesignDynamically())){

            $ret .= '<script language="JavaScript" type="text/javascript">'."\n".'
                    document.getElementById("div_'.$name.'").set_tb_cnt = "'.ParseForJScript($toolbarfunc).'";
                    '.$script."\n".'
                  </script>';
          }

          $ret .= iif($params['spanclass'], '<span class="'.$params['spanclass'].'">', '');

        }

      }
      if(!$visualhidden)
      {
        $GLOBALS['component_name'] = $name;
        $GLOBALS['page_id'] = $page_id;
        $GLOBALS['pagedev_id'] = $pagedev_id;
        if(!$params['novisualedit'])
          $GLOBALS['design_view'] = true;
        ob_start();
        if($params['novisualedit'])
        {
          if(!$obj->getProperty('visible')){
              $obj->Output();
          }

        }else
        {
          if(!$hidden)
          {
            if($canloaddynamically && $obj->CanLoadDesignDynamically())
              echo '
              <script>
                if(!GlobalOnloadEval)
                {
                  var GlobalOnloadEval = \'\';
                  var GlobalsIndicatorImage = \'<img src="'.$GLOBALS['cfgWebRoot'].'gui/images/indicator.gif">\';
                }
                GlobalOnloadEval = GlobalOnloadEval + \'AjaxLoadComponent("div_'.$name.'", "'.$name.'", "'.$type.'", "'.parseForJScript(parseForJScript($paramstr, false)).'", '.$page_id.', '.$pagedev_id.', '.$site_id.', "'."&session_id=".session_id()."&session_name=".session_name().'");\';
              </script>';
            else
            {
              $obj->Design();
            }
          }
        }

        $cont = ob_get_contents();
        ob_end_clean();
        //unset($GLOBALS['pagedev_id']);

        /*if(!$dontprocessinnercomponents)
        {
          //process inner components as well
          $r = componentArray($cont, $page_id);
          foreach($r as $c)
          {
            $cont = str_replace($c['tag'], outputGraphicalComponent($hascontent, $c['type'], $c['name'], $page_id, false, $c['options']), $cont, $toolbarfunc);
          }
        }*/
        $ret .= $cont;
        $hascontent = ((strlen($cont))and(!$hidden));
        $GLOBALS['design_view'] = false;
      }
      profiler_add('Design component ' . $type . ' ' . $name, 6);
    }else
    {
        $ret .= '<b>' . $name . '</b> ' . $type ;
        $hascontent = (!$hidden);
    }
    if((!$noDiv)and(!$params['novisualedit'])and(!$visualhidden))
    {
      $ret .= iif($params['spanclass'], '</span>', '');

      $ret .= '</div>';
    }

    return $ret;
  }



//--------------------------------------

  function outputTemplateGraphicalComponent(&$hascontent, $type, $name, $template_id, $noDiv, $paramstr = '', $dontprocessinnercomponents = false, &$toolbarfunc, $skippermissioncheck = false)
  {
    global $site_id;
    $filename = $GLOBALS['cfgDirRoot'] . "components/class.$type.php";
    $hascontent = false;
    if(file_exists($filename))
    {
      $GLOBALS['design_view']=true;
      include_once($filename);
      $obj = new $type($name);
      $obj->visualedit = true;

      $params = Array();

      if($noDiv)
      {
        $ret = '';
      }else
      {
        $params = getComponentParams($paramstr);
        if(!$skippermissioncheck)
        {
          /*
          if(!HasComponentPermissionPage($site_id, $page_id, $name))
          {
            $params['novisualedit'] = true;
          }
          */
        }
        $visualhidden = $params['novisualvisible'];
        if((!$params['novisualedit'])and(!$visualhidden))
        {

          $hidden = getProperty('visible', $name, $type, -1, $site_id, $copypage);
          if($hidden)
          {
            $background = '';
            $hiddenstyle = ' display: none;';
          }else
          {
            $background = '';
            $hiddenstyle = ' display: inline;';
          }
          $domain = $GLOBALS['domain'];

          $ret = '<div contentEditable="false" class="graphicaleditdiv" id="div_'.$name.'" name="div_'.$name.'" style="'.$background.'cursor:default;width: '.iif($params['width'],$params['width'],'100%').'; min-height: '.iif($params['height'],$params['height'],'100%').';position:relative;'.$hiddenstyle;
          $ret .= iif($params['class'], '" class="'.$params['class'].'', '');

          $ret .= '"';

          $ret .= "\n".' onClick="javascript:PropertiesWindow(\'http://' . $domain . '/edit.php?module=pages&template_editor=1&site_id='.$site_id.'&id='.$template_id.'&component_name='.$name.'&component_type='.$type.'\',\'&component_type=' .$type. '&component_name=' .$name .'\' , this, 400, 400, 1, 1);"';

            /*
          if(!$hastoolbar)
          {
            if(!$params['noproperties'])
            {
              $ret .= "\n".' onClick="javascript:SelectComponent(div_'.$name.',\''.$name.'\',\''.$type.'\',1);ClearAllToolbars(1);PropertiesWindow(\'http://' . $domain . '/edit.php?module=pages&site_id='.$site_id.'&id='.$page_id.'&component_name='.$name.'&component_type='.$type.'\',\'&component_type=' .$type. '&component_name=' .$name .'\' , this, 400, 400, 1, 1);"';
            }else
            {
              $ret .= "\n".' onClick="javascript:SelectComponent(div_'.$name.',\''.$name.'\',\''.$type.'\',1);ClearAllToolbars(1);"';
            }
          }
          else
          {
            $ret .= "\n".' onClick="javascript:eval(div_'.$name.'.set_tb_cnt);ComponentEditStart(div_'.$name.');"';
          }*/
          $ret .= "\n".' onMouseEnter="javascript:try{this.firstChild.style.borderColor=\'red\'}catch(e){}" onMouseLeave="javascript:try{this.firstChild.style.borderColor=\'#808080\'}catch(c){}"';
          $ret .= ' >';
          if(!$params['noborder'])
          {
            $ret .= BorderDiv($name, false);
          }
          /*if($hastoolbar)
          {


            $ret .= '<script language="JavaScript" type="text/javascript">'."\n".'
                    div_'.$name.'.set_tb_cnt = "'.ParseForJScript($toolbarfunc).'";
                    '.$script."\n".'
                  </script>';
          }*/


    //      $notable = getProperty('notable', $name, $type, $page_id, $site_id);
          $notable = true;
          if(!$notable)
            $ret .= '<table cellpadding="0" cellspacing="0" width=100%><tr><td>';
          $ret .= iif($params['spanclass'], '<span class="'.$params['spanclass'].'">', '');
        }

      }
      if(!$visualhidden)
      {
        $GLOBALS['component_name'] = $name;
        $GLOBALS['page_id'] = -1;
        if(!$params['novisualedit'])
          $GLOBALS['design_view'] = true;
        ob_start();
        /*if($params['novisualedit'])
        {
          if(!$obj->getProperty('visible'))
            $obj->Output();
        }else*/
          $obj->DesignTemplateEx();

        $cont = ob_get_contents();
        ob_end_clean();
        /*
        if(!$dontprocessinnercomponents)
        {
          //process inner components as well
          $r = componentArray($cont, $page_id);
          foreach($r as $c)
          {
            $cont = str_replace($c['tag'], outputTemplateGraphicalComponent($hascontent, $c['type'], $c['name'], $page_id, false, $c['options']), $cont, $toolbarfunc);
          }
        }
        */
        $ret .= $cont;
        $hascontent = ((strlen($cont))and(!$hidden));
        $GLOBALS['design_view'] = false;
      }
    }else
    {
        $ret .= '<b>' . $name . '</b> ' . $type ;
        $hascontent = (!$hidden);
    }
    if((!$noDiv)and(!$params['novisualedit'])and(!$visualhidden))
    {
      $ret .= iif($params['spanclass'], '</span>', '');
      if(!$notable)
        $ret .= '</td></tr></table>';
      $ret .= '</div>';
    }


    return $ret;
  }

//--------------------------------------



  function EditCollection($collection)
  {
    $category_id = $collection->collection_id;
    $site_id = $collection->site_id;


    if(isset($_GET['editpage']) && intval($_GET['editpage']) > 0)
    {
      $_SESSION[$collection->name . '_coledit_active_page'] = intval($_GET['editpage']) - 1;
      $forced_page = true;
    }else
    {
      $forced_page = false;
    }

    if($_GET['ordercolumn'])
    {
      $ordercol = $_GET['ordercolumn'];
      if($_GET['desc'])
        $ordercol .= ' DESC';
    }else
    {
      $ordercol = '';
    }



    $recordposition = '';
    $data = $collection->GetEditData(intval($_SESSION[$collection->name . '_coledit_active_page']), $count, $recordposition, $ordercol);
    //if active page is outside data range, re-request data
    if($_SESSION[$collection->name . '_coledit_active_page'] > floor($count / $collection->GetEditPageSize()))
    {
      $_SESSION[$collection->name . '_coledit_active_page'] = 0;
      $data = $collection->GetEditData(intval($_SESSION[$collection->name . '_coledit_active_page']), $count, $recordposition, $ordercol);
    }

    $cols = $collection->GetDisplayColumns();

    foreach ($cols as $key=>$col)
    {
        if ($col['timestamp'])
        {
            $date_cols[] = $key;
        }
    }

    //legacy stuff, please never use date_cols
    if ($date_cols)
    {
        foreach ($data as $key=>$row)
        {
            foreach ($date_cols as $date_col)
                $data[$key][$date_col] = date("Y-m-d H:i:s", $row[$date_col]);
        }
    }

    require("class.datagrid.php");

    //make normal columns sortable
    foreach($cols as $key => $col)
    {
      if(!isset($col['sortable']))
      {
        $cols[$key]['sortable'] = true;
        if($_GET['ordercolumn'] == $key)
        {
          $cols[$key]['sorted'] = true;
        }
      }
    }

    $Table = new DataGrid();
    $Table->cols = $cols;
    $Table->data = $data;
    $Table->footer = false;
    $Table->rowformat = " id=\"tablerow{%item_id%}\" onClick=\"javascript:HighLightTR(this, {%item_id%},1);\" oncontextmenu=\"javascript:HighLightTR(this, {%item_id%},1);
                          showMenu(this, event); return false;\"";
    $Table->sortlinkformat = preg_replace('/&ordercolumn=[a-zA-Z0-9_]*/s', '', $_SERVER["REQUEST_URI"]) . '&ordercolumn=%s';
    $Table->sortlinkformat = preg_replace('/&desc=[a-zA-Z0-9_]*/s', '', $Table->sortlinkformat);
    if($count > 0)
    {
      $Table->pagecount = ceil($count / $collection->GetEditPageSize());
      $Table->pagelen = 1;
      $Table->linkformat = preg_replace('/&editpage=[0-9]*/s', '', $_SERVER["REQUEST_URI"]) . '&editpage=%d';
      $Table->pagenum = min($Table->pagecount, $_SESSION[$collection->name . '_coledit_active_page'] + 1);
    }

    echo $Table->output();

    echo '<script language="JavaScript" type="text/javascript">
          /*<![CDATA[*/
          che = new Array("edit", "up", "down", "del", "delc");
          che2 = new Array("add", "edit", "properties", "up", "down", "delc");

          DisableToolbarButtons(che);
          /*]]>*/
          </script>';

    if($_GET['selrow'])
    {
      echo '<script language="JavaScript">
        <!--
          var tablerow' . $_GET['selrow'] . ' = document.getElementById("tablerow' . $_GET['selrow'] . '");
          if(typeof(tablerow' . $_GET['selrow'] . ')!= "undefined" && tablerow' . $_GET['selrow'] . ')
          {
            preEl = tablerow' . $_GET['selrow'] . ';
            ChangeTextColor(tablerow' . $_GET['selrow'] . ',\'tableRowSel\');
            SelectedRowID = ' . $_GET['selrow'] . ';
          }
        <!---->
        </script>';
}

  }


function listPageComponents($site_id, $page_id){

  list($pagedev_id, $title, $copypage) = sqlQueryRow('SELECT pagedev_id, title, copypage FROM ' . $site_id . '_pagesdev WHERE page_id = ' . $page_id . ' ORDER BY lastmod DESC LIMIT 1');
  if($pagedev_id){
    $template = sqlQueryValue("SELECT template FROM ". $site_id . "_pagesdev WHERE pagedev_id=$pagedev_id");
  }

	if($template){
    list($data, $copybody) = sqlQueryRow("SELECT body, copybody FROM " . $site_id . "_templates WHERE template_id=".$template);
    while($copybody){
    	list($data, $copybody) = sqlQueryRow("SELECT body, copybody FROM " . $site_id . "_templates WHERE template_id=".$copybody);
    }

  }

  $data = attachSubTemplates($data, $site_id);
  return componentArray($data, $pagedev_id);
}

