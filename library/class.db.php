<?php
/**
* PDO library for PHP
*
* Version 1.6 (25-01-2018)
* @author Kristaps Taube <ktaube@datateks.lv>
*
* Usage:
* $data = DB::GetTable($query, $params);
* or
* $db = new DB($conf);
* $data = $db->GetTable($query, $params);
*
*
**/

class DB{

    private $connection;
    private $conf;
    static private $instance;
    static private $query_count;
    private $tables=array();

    public function __construct($conf = false)
    {

        $this->conf = $conf;

    }

    public static function GetInstance()
    {

        if(isset($this) && $this) return $this; // called on object, otherwise called staticly

        if(self::$instance) return self::$instance;

        self::$instance = new DB();

        return self::$instance;

    }

    public static function GetQueryCount()
    {
        return self::$query_count;
    }

    public function GetTablePK($table)
    {
        static $cache = array();

        if($cache[$table]) return $cache[$table];

        foreach(self::GetTable("SHOW INDEXES FROM ".$table) as $index){

            if($index['Key_name'] == 'PRIMARY'){
                $cache[$table] = $index['Column_name'];
            }

        }

        return ($cache[$table]) ? $cache[$table] : false;
    }

    private function GetConnection()
    {

        if(!$this->connection){

            if(!$this->conf){

                if(isset(Constructor\App::$app) && Constructor\App::$app && Constructor\App::$app->getConfigValue('database')){ // new config

                    $this->conf = Constructor\App::$app->getConfigValue('database');
                    $this->conf['database'] = Constructor\App::$app->getConfigValue('database/name');

                }else{ // old config
                    $this->conf = array(
                        "host" => DB_HOST,
                        "database" => DB_DATABASE,
                        "username" => DB_USERNAME,
                        "password" => DB_PASSWORD,
                    );
                    if(defined('DB_PORT')){
                        $this->conf['port'] = DB_PORT;
                    }
                }

            }

            try {
                $this->connection = $this->Connect($this->conf);
            } catch (PDOException $e) {
                die("PDO CONNECTION ERROR: " . $e->getMessage() . " <br/>");
            }

            $this->InitConnection();

        }

        return $this->connection;

    }

    private function Connect($conf)
    {
        return new PDO('mysql:dbname='.$conf['database'].';host='.$conf['host'].(isset($conf['port'])?';port='.$conf['port']:'').';', $conf['username'], $conf['password']);
    }

    private function InitConnection()
    {
        $this->GetConnection()->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->Query('SET NAMES utf8');
    }

    private function LogError($error, $query, $params)
    {

        $file = PDO_ERROR_LOG_FOLDER.date("Y")."/".date("m")."/".date("d").".txt";
        $text = "----- ".date("d.m.Y H:i:s")." -----" . PHP_EOL ."Error: ".$error . PHP_EOL . "Query: " . $query . PHP_EOL . "Params: " . print_r($params, true) . PHP_EOL;

        if(!file_exists($file)){
            file_force_contents($file, $text);
        }else{
            file_put_contents($file, $text, FILE_APPEND);
        }

    }

    private function ProcessError($e, $query, $params)
    {

        $this->LogError($e->getMessage(), $query, $params);

        if($GLOBALS['developmode']){
            echo "PDO ERROR: ".$e->getMessage()."<br />Query: ".$query."<br />Params: ".print_r($params, true)."<br />";
        }

    }

    function LogQuery($query, $time)
    {

        $t = "Query(".$GLOBALS['total_queries_executed'].".): " . $query . PHP_EOL . "Exec time: " . round($time, 4) . PHP_EOL . "---------------" . PHP_EOL;

        file_force_contents(PDO_QUERY_FILE, $t, true);

    }

    private function LogSlowQuery($query, $params, $time)
    {

        $t =  PHP_EOL . "----- ".date("d.m.Y H:i:s")." -----" . PHP_EOL."Slow query: " . $query . PHP_EOL . " Params: " . print_r($params, true) . PHP_EOL . "Exec time: " . round($time, 4);

        file_force_contents(SLOW_PDO_QUERY_FILE, $t, true);

    }

    private function Execute($query, $params = Array())
    {

        try{
            $sth = $this->GetConnection()->prepare($query);
        }catch (Exception $e) {
            $this->ProcessError($e, $query, $params);
        }

        if($sth){
            $start = microtime(true);

            try{
                $sth->execute($params);
                $GLOBALS['total_queries_executed']++;
            }catch (Exception $e) {
                $this->ProcessError($e, $query, $params);
            }

            $end = microtime(true);
            $dif = $end - $start;
            $GLOBALS['total_query_time'] += $dif;

            if(defined("LOG_SLOW_PDO_QUERIES") && LOG_SLOW_PDO_QUERIES && $dif > SLOW_PDO_QUERY_TIME_LIMIT){
                $this->LogSlowQuery($query, $params, $dif);
            }

            self::$query_count++;

        }

        if(LOG_QUERIES){
            $this->LogQuery($query, $dif);
        }

        return $sth;

    }

    public static function GetColumn($query, $params = array())
    {

        $return = array();
        foreach(self::GetTable($query, $params) as $row){
            foreach($row as $value){
                $return[] = $value;
            }
        }

        return $return;

    }

    public static function GetTable($query, $params = array(), $fetch = PDO::FETCH_ASSOC)
    {
        return self::GetInstance()->Execute($query, $params)->fetchAll($fetch);
    }

    public static function GetRow($query, $params = array(), $fetch = PDO::FETCH_ASSOC)
    {
        return self::GetInstance()->Execute($query, $params)->fetch($fetch);
    }

    public static function GetValue($query, $params = array())
    {
        return self::GetInstance()->Execute($query, $params)->fetchColumn();
    }

    public static function Query($query, $params = array())
    {
        return self::GetInstance()->Execute($query, $params);
    }

    private function BuildQuery($table, $data)
    {

        $sql = '';

        if($data['action'] == 'update'){

            $sql .= 'UPDATE `'.$table.'` SET ';
            $what = array();
            foreach($data["what"] as $key => $value){
                $what[] = "`".$key."` = :".$key;
            }
            $sql .= implode(", ", $what);

        }elseif($data['action'] == 'select'){

            $sql .= 'SELECT ';
            $sql .= !empty($data["what"])? implode(", ", $data["what"]) : "*";
            $sql .= ' FROM `'.$table.'` ';

        }elseif($data['action'] == 'insert'){

            $sql .= 'INSERT INTO `'.$table.'`';

            $where = $what = array();
            foreach($data["what"] as $key => $value){
                $where[] = $key;
                $what[] = ":".$key; // place holders
            }

            $sql .= "(`".implode("`, `", $where)."`) VALUES(".implode(', ', $what).")";

        }elseif($data['action'] == 'delete'){

            $sql .= 'DELETE FROM `'.$table.'` ';

        }else{

            die("Unknown QueryBuilder action: ".$data['action']);

        }

        $where = array();
        if(isset($data['where']) && is_array($data['where'])){
            foreach($data['where'] as $key => $value){
                if(is_numeric($key)){
                    $where[] = $value;
                }else{
                    $where[] = $key . " = :".$key;
                }
            }
        }

        $sql .= !empty($data['where'])? " WHERE ".implode(" AND ", $where) : "";
        $sql .= !empty($data["order"])? " ORDER BY ".$data['order'] : "";
        $sql .= !empty($data["limit"])? " LIMIT ".$data['order'] : "";

        return $sql;

    }

    public static function GetLastInsertId()
    {
        return self::GetInstance()->GetConnection()->lastInsertId();
    }

    public static function Truncate($table)
    {
        return self::Query("TRUNCATE TABLE `".$table."`");
    }

    public static function Delete($table, $cond)
    {

        if(!is_array($cond)){
            $cond = array(self::GetTablePK($table) => $cond);
        }

        $params = array();

        foreach($cond as $key => $value){
            if(!is_numeric($key)){
                $params[":".$key] = $value;
            }
        }

        $data['action'] = 'delete';
        $query = self::BuildQuery($table, array("where" => $cond, "action" => "delete"));
        return self::GetInstance()->Execute($query, $params)->rowCount();

    }

    public static function Update($table, $cond, $data)
    {

        $params = array();

        if(!is_array($cond)){
            $cond = array(self::GetTablePK($table) => $cond);
        }

        foreach($data as $key => $value){
            if(!is_numeric($key)){
                $params[":".$key] = $value;
            }
        }

        foreach($cond as $key => $value){
            if(!is_numeric($key)){
                $params[":".$key] = $value;
            }
        }

        $data['what'] = $data;
        $data['where'] = $cond;
        $data['action'] = 'update';
        $query = self::BuildQuery($table, $data);

        return self::GetInstance()->Execute($query, $params)->rowCount();

    }

    public static function Insert($table, $data)
    {
        $query = self::BuildQuery($table, array("what" => $data, "action" => "insert"));
        $params = array();
        foreach($data as $key => $value){
            $params[":".$key] = $value;
        }
        self::GetInstance()->Execute($query, $params);
        return self::GetInstance()->GetConnection()->lastInsertId();
    }

}