<?php

namespace UrlRules;

use DB;
use Constructor\WebApp;
use Constructor\UrlRule;
use Constructor\UrlRuleInterface;
use Constructor\Controller;

class FirstUrlRule extends UrlRule implements UrlRuleInterface
{

    private $page;
    private $page_id;

    public function createUrl($route, $params = [])
    {

        $result = false;

        if($route == '/'){

            if(isset($params['lang'])){
                $lang = $params['lang'];
                unset($params['lang']);
            }else{
                $lang = WebApp::$app->getLanguage();
            }

            $result = '/'.($lang == 'lv' ? '' : $lang);
        }

        if($result && $params){
            $result .= "?".http_build_query($params);
        }

        return $result;

    }

    public function parseUrl($url)
    {

        $result = false;

        $parts = array_values(array_filter(explode("/", $url['path'])));

        $part_count = count($parts);

        $params = $_GET;

        if($part_count == 0){
            $result = $this->getDefaultRoute();
        }

        if(!$result && $part_count == 1){ // maybe its a language start page?

            $langs = getLanguages();
            if(isset($langs[$parts[0]])){// Yes, it is!
                $lang = $langs[$parts[0]]['shortname'];
                $result = $this->getDefaultRoute();
                WebApp::$app->setLanguage($lang);
            }

        }

        if(!$result && $part_count > 0 && $part_count < 3){ // 1 or 2

            $controller_name = $parts[0];
            $action_name = isset($parts[1]) ? $parts[1] : 'index';

            $action = Controller::createActionName($action_name);
            $controller_class_name = Controller::createClassName($controller_name);

            if(class_exists($controller_class_name) && method_exists($controller_class_name, $action)){
                $result = [$controller_name, $action_name];
            }

        }

        return $result;

    }

    private function getDefaultRoute()
    {

        $parts = explode("/", WebApp::$app->getConfigValue('default_route'));
        $controller = $parts[0];
        $action = isset($parts[1]) ? $parts[1] : null;

        return [$controller, $action];

    }

}