<?php

namespace UrlRules;

use DB;
use Constructor\WebApp;
use Constructor\UrlRule;
use Constructor\UrlRuleInterface;

class LastUrlRule extends UrlRule implements UrlRuleInterface
{

    private $page;
    private $page_id;

    public function createUrl($route, $params = [])
    {

        $result = $route;

        if($params){
            $result .= "?".http_build_query($params);
        }

        return $result;

    }

    // nothing has worked so far, we are the last hope
    public function parseUrl($url)
    {

        $result = $this->get404Result();
        return $result;

    }

    private function get404Result()
    {

        $page = DB::GetRow("SELECT * FROM " . WebApp::$app->getSiteId() . "_pages WHERE name = '404'");
        return $page ? ['page', 'index', ['page' => $page]] : false;

    }

}