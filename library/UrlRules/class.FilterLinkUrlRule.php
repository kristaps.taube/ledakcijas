<?php

namespace UrlRules;

use DB;
use Constructor\WebApp;
use Constructor\Url;
use Constructor\UrlRule;
use Constructor\UrlRuleInterface;

class FilterLinkUrlRule extends UrlRule implements UrlRuleInterface
{

    public function createUrl($route, $params = [])
    {

        $result = false;

        if($route == 'filter_link' && isset($params['category']) && isset($params['link'])){

            $category = $params['category'];
            $link = $params['link'];

            $category_url = Url::get('category', ['category' => $category]);
            $result = $category_url.$link['url_'.WebApp::$app->getLanguage()];

        }

        return $result;

    }

    public function parseUrl($url)
    {

        $result = false;

        $parts = array_values(array_filter(explode("/", $url['path'])));
        $category = false;

        if(count($parts) > 1){

            $lang = $parts[0];
            $langs = getLanguages();

            unset($parts[0]);

            if(isset($langs[$lang])){ // we are in lang

                $categories = \shopcatcollection::getInstance();
                $link_collection = \LedAkcijasLinkCollection::getInstance();
                $parent = 0;

                foreach($parts as $i => $part){

                    $kids = $categories->getAllCatsByParent($parent);
                    $found = false;
                    foreach($kids as $kid){

                        if($kid['url_'.$lang] == $part && !$kid['disabled'] && !$kid['uncategorized_item_cat']){
                            $parent = $kid['item_id'];
                            $found = true;
                            $category = $kid;
                            break;
                        }

                    }

                    if(!$found && $i == count($parts) && $category){ // didnt find on last part

                        $link = $link_collection->getByUrlAndCategory($part, $category['item_id']);

                        if($link){

                            foreach(getLanguages() as $short => $lang){
                                if($link['url_'.$short] == $part){
                                    WebApp::$app->setLanguage($short);
                                    break;
                                }
                            }

                            $result = ['category', 'filter-link', ['category' => $category, 'link' => $link]];

                        }

                    }elseif(!$found){ // didnt find

                        break;

                    }

                }

            }

        }

        return $result;

    }

}