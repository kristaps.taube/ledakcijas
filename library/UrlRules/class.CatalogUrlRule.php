<?php

namespace UrlRules;

use Constructor\WebApp;
use Constructor\UrlRule;
use Constructor\URL;
use Constructor\UrlRuleInterface;
use shopcatcollection;
use shopprodcollection;

class CatalogUrlRule extends UrlRule implements UrlRuleInterface
{

    private $_category;
    private $_product;

    public function getCategory()
    {
        return $this->_category;
    }

    public function getProduct()
    {
        return $this->_product;
    }

    public function createUrl($route, $params = [])
    {

        if($route == 'product' && isset($params['product'])){
            return $this->createProductUrl($params);
        }elseif($route == 'category' && isset($params['category'])){
            return $this->createCategoryUrl($params);
        }

        return false;

    }

    public function parseUrl($url)
    {

        $parts = array_values(array_filter(explode("/", $url['path'])));

        $category = $product = false;

        if(count($parts) > 1){ // at least 2 parts - /lv/../../../..  refer to getCatalogURLPrefix method

            $lang = $parts[0];
            $langs = getLanguages();

            unset($parts[0]);

            if(isset($langs[$lang])){ // we are in lang

                $categories = new shopcatcollection;

                $parent = 0;

                foreach($parts as $i => $part){

                    $kids = $categories->getAllCatsByParent($parent);
                    $found = false;
                    foreach($kids as $kid){

                        if($kid['url_'.$lang] == $part && !$kid['disabled'] && !$kid['uncategorized_item_cat']){
                            $parent = $kid['item_id'];
                            $found = true;
                            $category = $kid;
                            break;
                        }

                    }

                    if(!$found && $i == count($parts)){ // we didnt found category at last part, could it be a product?

                        $products = shopprodcollection::getInstance();
                        $product = $products->getProductByURL($part, $lang);

                        while($product && $product['shortcut']){
                            $product = $products->GetById($product['shortcut']);
                        }

                        if($product && $product['disabled']){
                            $product = false;
                        }

                        if(!$product){
                            $category = false;
                        }

                    }elseif(!$found){
                        $category = $product = false;
                        break;
                    }

                }

            }

        }

        #var_dump($category);

        if($product){
            $result = ['product', 'index', ['product' => $product, 'category' => $category]];
            $this->_product = $product;
            $this->_category = $category;
            WebApp::$app->setLanguage($lang);
        }elseif($category){
            $result = ['category', 'index', ['category' => $category]];
            $this->_category = $category;
            WebApp::$app->setLanguage($lang);
        }else{
            $result = false;
        }

        return $result;

    }

    private function createProductUrl($params)
    {

        static $cache;

        $result = false;
        $product = $params['product'];
        if(isset($params['lang'])){
            $lang = $params['lang'];
            unset($params['lang']);
        }else{
            $lang = WebApp::$app->getLanguage();
        }

        unset($params['product']);

        $cache_key =  (is_array($product) ? $product['item_id'] : $product).'-'.$lang.print_r($params, true);
        $cache_key = sha1($cache_key);

        if(isset($cache[$cache_key])) return $cache[$cache_key];

        $product = is_array($product) ? $product : shopprodcollection::getInstance()->getItemByIdAssoc($product, true);

        if($product){

            $url = $this->getCategoryURL($product['category_id'], $lang);
            $url .= $product['url_'.$lang].(!empty($params) ? '?'.http_build_query($params) : '');
            $result = $url;

            $cache[$cache_key] = $result;

        }

        return $result;

    }

    private function getCatalogURLPrefix($lang){
        return '/'.$lang.'/';
    }

    private function getCategoryURL($cat_id, $lang = false)
    {

        static $urls = [];

        $lang = $lang ? $lang : WebApp::$app->getLanguage();

        $prefix = $this->getCatalogURLPrefix($lang);
        $url_id = $cat_id.':'.$lang;

        if (isset($urls[$url_id])){
            return $urls[$url_id];
        }

        $path = shopcatcollection::getInstance()->getCategoryPath($cat_id);
        if (!$path) return '';

        $url = '';
        foreach ($path as $cat)
        {
            $url .= ($cat['url_'.$lang] != '') ? $cat['url_'.$lang].'/' : 'c'.$cat['item_id'].'/';
        }

        $urls[$url_id] = $prefix.$url;
        return $urls[$url_id];
    }

    private function createCategoryUrl($params)
    {
        static $cache;
        $result = false;

        $category = is_array($params['category']) ? $params['category']['item_id'] : $params['category']; // keep id only
        if(isset($params['lang'])){
            $lang = isset($params['lang']);
            unset($params['lang']);
        }else{
            $lang = WebApp::$app->getLanguage();
        }
        unset($params['category']);

        $url_key = sha1($category.$lang.print_r($params, true));

        if(isset($cache[$url_key])){

            $result = $cache[$url_key];

        }else{

            $url = $this->getCategoryURL($category, $lang);
            $url .= (!empty($params) ? '?'.http_build_query($params) : '');
            $result = $cache[$url_key] = $url;

        }

        return $result;

    }


}