<?php

namespace UrlRules;

use Constructor\WebApp;
use Constructor\Controller;
use Constructor\UrlRule;
use Constructor\UrlRuleInterface;
use Constructor\SystemUrl;
use DB;


class DefaultUrlRule extends UrlRule implements UrlRuleInterface
{

    public function createUrl($route, $params = [])
    {

        $result = $route;

        if($params){
            $result .= "?".http_build_query($params);
        }

        return $result;

    }

    public function parseUrl($url)
    {

        $result = false;

        $parts = array_values(array_filter(explode("/", $url['path'])));

        $part_count = count($parts);

        $params = $_GET;

        if($part_count == 0){

            $parts = explode("/", WebApp::$app->getConfigValue('default_route'));
            $controller = $parts[0];
            $action = isset($parts[1]) ? $parts[1] : null;

            $result = [$controller, $action];

        }elseif($part_count > 0 && $part_count < 3){ // 1 or 2

            $controller_name = $parts[0];
            $action_name = isset($parts[1]) ? $parts[1] : 'index';

            $action = Controller::createActionName($action_name);
            $controller_class_name = Controller::createClassName($controller_name);

            if(class_exists($controller_class_name) && method_exists($controller_class_name, $action)){
                $result = [$controller_name, $action_name];
            }

        }

        if(!$result){ // no result found - 404 page
            $result = $this->get404Result($url);
        }else{
            $result[] = $params;
        }

        return $result;

    }

    private function get404Result($url)
    {

        $page = DB::GetRow("SELECT * FROM " . WebApp::$app->getSiteId() . "_pages WHERE name = '404'");
        return $page ? ['page', 'index', ['page' => $page]] : false;

    }

}