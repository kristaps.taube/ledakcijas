<?php

namespace UrlRules;

use DB;
use Constructor\WebApp;
use Constructor\UrlRule;
use Constructor\UrlRuleInterface;

class BlogUrlRule extends UrlRule implements UrlRuleInterface
{

    private $page;
    private $page_id;

    public function createUrl($route, $params = [])
    {

        if($route == 'blog_category' && isset($params['category'])){

            return '/'.$params['category']['url_'.WebApp::$app->getLanguage()];

        }elseif($route = 'blog_article' && $params['article']){

            return '/'.$params['article']['url'];

        }

        return false;

    }

    public function parseUrl($url)
    {

        $result = false;

        $parts = array_values(array_filter(explode('/', $url['path'])));

        if(count($parts) == 1){

            $url = $parts[0];

            # search in articles
            $col = \LedAkcijasArticleCollection::getInstance();

            $article = $col->getByUrl($url);

            if($article){

                WebApp::$app->setLanguage($article['lang']);
                $result = ['blog', 'view', ['article' => $article]];

            }

            if(!$result){ # search in categories

                $cat_col = \LedAkcijasArticleCategoryCollection::getInstance();

                $category = $cat_col->getByUrl($url);

                if($category){

                    foreach(getLanguages() as $short => $lang){
                        if($category['url_'.$short] == $url){

                            WebApp::$app->setLanguage($short);
                            $result = ['blog', 'category', ['category' => $category]];
                            break;

                        }
                    }

                }

            }

        }

        return $result;

    }

}