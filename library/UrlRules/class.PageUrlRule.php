<?php

namespace UrlRules;

use DB;
use Constructor\WebApp;
use Constructor\UrlRule;
use Constructor\UrlRuleInterface;

class PageUrlRule extends UrlRule implements UrlRuleInterface
{

    private $page;
    private $page_id;

    public function createUrl($route, $params = [])
    {

        $result = false;

        if($route == 'page' && isset($params['page'])){

            $result = '/'.PagePathById($params['page']['page_id']);

        }

        return $result;

    }

    public function parseUrl($url)
    {

        $site_id = WebApp::$app->getSiteId();

        //Obtain page_id
        $path = trim($url['path'], '/');
        $path = array_filter(explode("/", $path));

        if(!empty($path)){
            $page_id = '0';
            for($i = 0; $i < count($path); $i++){
                // there is so much room for improvments...
                $page = DB::GetRow("SELECT * FROM " . $site_id . "_pages WHERE parent = :page_id AND name = :name AND enabled = 1 AND in_trash = 0", [':page_id' => $page_id, ':name' => $path[$i]]);
                $page_id = $page['page_id'];
                if(!$page){
                    $page = $page_id = false;
                    break;
                }
            }

        }

        return $page ? ['page', 'index', ['page' => $page]] : false;

    }

    public function getPage()
    {
        return $this->page;
    }

    public function getPageId()
    {
        return $this->page_id;
    }

}