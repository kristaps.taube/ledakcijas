<?php

namespace UrlRules;

use Constructor\WebApp;
use Constructor\UrlRule;
use Constructor\UrlRuleInterface;
use Constructor\SystemUrl;

class SystemUrlRule extends UrlRule implements UrlRuleInterface
{

    public function createUrl($route, $params = [])
    {

        $instance = SystemUrl::getInstance();
        $definitions = $instance->getDefinitions();

        $result = false;

        if(isset($definitions[$route])){

            if(isset($params['lang'])){
                $lang = $params['lang'];
                unset($params['lang']);
            }else{
                $lang = WebApp::$app->getLanguage();
            }
            $result = SystemUrl::get($route, $lang);
            $result = $result ? $result.($params ? "?".http_build_query($params) : '') : false;
        }

        return $result;

    }

    public function parseUrl($url)
    {

        $instance = SystemUrl::getInstance();

        $urls = $instance->getCache();
        $definitions = $instance->getDefinitions();

        $result = false;

        foreach($urls as $key => $langs){
            foreach($langs as $lang => $value){

                if(substr($value,0,1) != '/'){ // is this a good idea?
                    $value = '/'.$value;
                }

                if($value == $url['path']){

                    WebApp::$app->setLanguage($lang);
                    $definition = $definitions[$key];

                    $parts = array_filter(explode("/", $definition['route']));
                    $action = isset($parts[1]) ? $parts[1] : 'index';
                    $result = [$parts[0], $action];

                }

            }

        }

        return $result;

    }

}