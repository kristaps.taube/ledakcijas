<?php

namespace UrlRules;

use DB;
use Constructor\WebApp;
use Constructor\UrlRule;
use Constructor\UrlRuleInterface;

class NewsUrlRule extends UrlRule implements UrlRuleInterface
{

    private $page;
    private $page_id;

    public function createUrl($route, $params = [])
    {

        if($route == 'news/view' && isset($params['entry'])){

            return '/'.$params['entry']['url'];

        }

        return false;

    }

    public function parseUrl($url)
    {

        $result = false;

        foreach(getLanguages() as $short => $lang)
        {

            $col = \newnewscollection::getInstance('news_'.$short);

            $path = substr($url['path'], 1); // remove starting '/'

            $entry = $col->GetRow(['where' => 'url = :url', 'params' => [':url' => $path]]);
            if($entry){

                WebApp::$app->setLanguage($short);
                $result = ['news', 'view', ['entry' => $entry]];
                break;

            }

        }

        return $result;

    }

}