<?


function loadOptions($force = false, $site_id = 0){

  if (!$site_id) $site_id = $GLOBALS['site_id'];
  if(!$GLOBALS['options'] || $force){
    $GLOBALS['options'] = sqlQueryData("SELECT * FROM `".$site_id."_options`");
  }

}

function getOptionValueByParams($key, $params, $site_id = 0){

  $row = getOptionRowByParams($params, $site_id);

  return $row[$key];

}

function getOptionRowByParams($params, $site_id = 0){

  loadOptions(false, $site_id);

  foreach($GLOBALS['options'] as $option){

    $this_is_it = true;

    foreach($params as $key => $value){

      if($option[$key] != $value){
        $this_is_it = false;
        break;
      }

    }

    if($this_is_it){
      return $option;
    }

  }

}

function getOptionByPath($path, $parent_id=0, $site_id=0){
  if (!$site_id) $site_id = $GLOBALS['site_id'];

  $id = $parent_id;
  if (is_string($path)) $path = explode('\\', $path);

  $row = Array();
  for ($i=0; $i < count($path); $i++){
    $p = $path[$i];
    if ($p == '') continue;

    $pre_id = $id;
    if ($i == (count($path)-1)){ //
      $row = getOptionRowByParams(array("parent" => $id, "name" => $p), $site_id);
      $id = $row['id'];
    }else{
      $id = getOptionValueByParams("id", array("parent" => $id, "name" => $p), $site_id);
    }

    if (!$id){ // option not found, returning parent_id.. for sake of satan
      return Array($pre_id, array_slice($path, $i));
    }

  }

	return $row;

}

function optionPageCatPath($page_id, $path, $site_id=0){

  static $_pages_initialized = array();

  if (!isset($_pages_initialized[$site_id])){
    if (!$site_id) $site_id = $GLOBALS['site_id'];
    option('_pages', null, '_pages', array('is_advanced' => 1), $site_id);
    $_pages_initialized[$site_id] = true;
  }
  return '_pages\\'.(int)$page_id.'\\'.$path;
}

/*
 * params: value_on_create, parent_id, type, user, componenttype, componentname, page_id, is_advanced, use_pagecat, show_pageprop, fieldparams
*/
function option($path, $value=null, $label=null, $params=null, $value_on_create=null, $site_id=0){

  if (!$site_id) $site_id = $GLOBALS['site_id'];
  if ($params === null) $params = Array();

  if ($value_on_create !== null) $params['value_on_create'] = $value_on_create;

  if ($params['use_pagecat'] && !empty($params['page_id'])){
    $path = optionPageCatPath((int)$params['page_id'], $path, $site_id);
  }

  if (isset($params['fieldparams'])) $params['fieldparams'] = serialize($params['fieldparams']);

  $row = getOptionByPath($path, (int)$params['parent_id'], $site_id);

  if (!isset($row['id'])){ // new
    if ($params['_justread']) return null;

    if ($value === null && $params['value_on_create'] !== null)
      $value = $params['value_on_create'];

    list($parent_id, $path2) = $row;

    for ($i=0; $i<count($path2); $i++){
      $p = $path2[$i];
      if ($p == '') continue;

      $fields = array();
      if ($i == (count($path2)-1)){

        $fields = array(
        	"user" => strval($params['user']),
        	"type" => strval($params['type']),
        	"componenttype" => strval($params['componenttype']),
        	"componentname" => strval($params['componentname']),
        	"page_id" => intval($params['page_id']),
        	"label" => strval($label),
        	"is_advanced" => (int)(bool)$params['is_advanced'],
        	"use_pagecat" => (int)(bool)$params['use_pagecat'],
        	"show_pageprop" => (int)(bool)$params['show_pageprop'],
        	"fieldparams" => strval($params['fieldparams']),
				);

      }

			sqlQuery("UPDATE `".$site_id."_options` SET is_cat=1 WHERE id=".$parent_id);

			$id = DB::Insert($site_id."_options", array_merge($fields, array(
      	"parent" => intval($parent_id),
      	"name" => $p,
      	"value" => $value ? $value : "",
      	"is_cat" => 0,
			)));

      loadOptions(true, $site_id);
    }

    return $value;

  }

  $newrow = array();
  if ($value !== null && $value != $row['value']) $newrow['value'] = $value;
  if (isset($params['page_id'])) $params['page_id'] = (int)$params['page_id'];
  if ($label !== null) $params['label'] = $label;

  $paramkeys = Array('type', 'user', 'componenttype', 'componentname', 'page_id', 'label', 'is_advanced', 'use_pagecat', 'show_pageprop', 'fieldparams');
  foreach ($paramkeys as $key){
    if (isset($params[$key]) && $params[$key] != $row[$key])
      $newrow[$key] = $params[$key];
  }

  if ($newrow){

    $a = Array();
    foreach ($newrow as $key => $val) $a[$key] = $val;

		DB::Update($site_id."_options", array("id" => $row['id']), $a);

  }

  return isset($newrow['value']) ? $newrow['value'] : $row['value'];
}

function readOption($path, $params=null, $site_id=0)
{
  loadOptions(false, $site_id);
  if ($params === null) $params = array();
  $params['_justread'] = 1;
  return option($path, null, null, $params, $site_id);
}


function deleteOption($path, $parent_id=0, $site_id=0)
{
  if (!$site_id) $site_id = $GLOBALS['site_id'];
  if (is_int($path))
  {
    $id = $path;
  }
  else
  {
    $row = getOptionByPath($path, $parent_id, $site_id);
    if (!isset($row['id'])) return false;
    $id = $row['id'];
  }

  deleteOptionSubcategories($id, $site_id);
  return sqlQuery("DELETE FROM `".$site_id."_options` WHERE id=".$id) > 0;
}

function deleteOptionSubcategories($id, $site_id=0)
{
  if (!$site_id) $site_id = $GLOBALS['site_id'];

  $data = sqlQueryData("SELECT id FROM `".$site_id."_options` WHERE parent=".$id);
  foreach ($data as $row)
  {
    deleteOptionSubcategories($row['id'], $site_id);
  }
  sqlQuery("DELETE FROM `".$site_id."_options` WHERE parent=".$id);
}

function getOptionPathById($id, $site_id=0)
{
  if (!$id) return '';
  if (!$site_id) $site_id = $GLOBALS['site_id'];
  $a = Array();
  while ($id)
  {
    $row = sqlQueryRow("SELECT parent, name FROM `".$site_id."_options` WHERE id=".$id);
    $a[] = $row['name'];
    $id = $row['parent'];
  }
  return array_reverse($a);
}

function optionExists($path, $parent_id=0, $site_id=0, $pagecat_id=0)
{
  if (empty($path)) return false;

  if ($pagecat_id)
    $path = optionPageCatPath($pagecat_id, $path, $site_id);

  $row = getOptionByPath($path, $parent_id, $site_id);
  return isset($row['id']);
}

// backend functions:

function optionInitComponent($type, $name, $site_id, $page_id=0)
{
  static $pages = Array();

  $filename = getComponentFilePath($type);
  if (!class_exists($type) && !file_exists($filename)) return false;

  $GLOBALS['site_id'] = $site_id;
  $GLOBALS['page_id'] = $page_id;
  if ($page_id)
  {
    $ckey = $site_id.'_'.$page_id;
    if (isset($pages[$ckey]))
      $pageData = $pages[$ckey];
    else
    {
      $pageData = sqlQueryRow("SELECT * FROM " . $site_id . "_pages LEFT JOIN " . $site_id . "_languages ON " . $site_id . "_languages.language_id=" . $site_id . "_pages.language WHERE " . $site_id . "_pages.page_id='".$page_id."'");
      $pages[$ckey] = $pageData;
    }

    echo 'her!?';

    $GLOBALS['pageData'] = $pageData;
    $GLOBALS['template_id'] = $pageData['template'];
    $GLOBALS['language_id'] = $pageData['language'];
    $GLOBALS['language'] = $pageData['shortname'];
    $GLOBALS['copypage'] = $pageData['copypage'];
  }

  $GLOBALS['component_name'] = $name;

  if (!class_exists($type))
  {
    include_once($filename);
    if (!class_exists($type)) return false;
  }

  return true;
}

function getOptionFields($site_id, $parent_id, $advanced)
{
  $where = !$advanced ? " AND is_advanced=0" : '';
  return sqlQueryDataAssoc("SELECT * FROM `".$site_id."_options` WHERE parent=".$parent_id." AND is_cat=0".$where." ORDER BY id");
}

function prepareOptionForm(&$form_data, $opt_id, $parent_id, $site_id, $advanced=false, $pagepropsmode=false, $title=''){

  $parent_path = implode('\\', getOptionPathById($parent_id, $site_id));
  $parent_opt = sqlQueryRow("SELECT * FROM `".$site_id."_options` WHERE id=".$parent_id);

  if ($_POST){
    $data = getOptionFields($site_id, $parent_id, $advanced);

    foreach ($data as $row)
    {
      $key = 'i'.$row['id'];
      $val = isset($_POST[$key]) ? $_POST[$key] : '';

      $opt = getOptionByPath($row['name'], $parent_id);
      if (!isset($opt['id'])) continue;
      if ($pagepropsmode && !$opt['show_pageprop']) continue;

      if ($opt['componenttype'] && optionInitComponent($opt['componenttype'], $opt['componentname'], $site_id, $opt['page_id']) )
      {
        $path = $parent_path.'\\'.$key;
        if ($opt['componentname'])
        {
          $obj = new $opt['componenttype']($opt['componentname']);
          $val = $obj->processOptionBeforeSave($path, $opt, $val);
        }
        else
        {
          $val = call_user_func_array(Array($opt['componenttype'], 'processOptionBeforeSave'), Array($path, $opt, $val));
        }

      }

      DB::Update($site_id."_options", array("id" => $row['id']), array("value" => $val));

    }

  }

  if (empty($title))
  {
    if ($parent_opt)
      $title = ($parent_opt['label'] != '') ? $parent_opt['label'] : $parent_opt['name'];
    else
      $title = '{%langstr:menu_options%}';
  }

  $form_data['title'.$parent_id] = array(
    'type' => 'title',
    'label' => $title
  );
  $form_data['_f'.$parent_id] = array(
    'type' => 'hidden',
    'value' => 1
  );

  $data = getOptionFields($site_id, $parent_id, $advanced);
  foreach ($data as $row)
  {
    if ($pagepropsmode && !$row['show_pageprop']) continue;

    $key = 'i'.$row['id'];
    $label = $row['label'] != '' ? $row['label'] : $row['name'];

    $fieldparams = empty($row['fieldparams']) ? array() : unserialize($row['fieldparams']);
    $form_data[$key] = $fieldparams + array(
      'type' => $row['type'],
      'label' => $label,
      'value' => $row['value']
    );

    if (!$pagepropsmode && $_SESSION['userdata']['username'] == 'Admin')
    {
      $g = $_GET;
      $g['delopt'] = $row['id'];
      $delurl = '?'.http_build_query($g);
      $form_data[$key]['extrahtml'] = ' <a href="'.$delurl.'" onclick="return confirm(\'{%langstr:options_confirm_del%} &quot;'.$label.'&quot; ?\')"><img src="gui/images/del.gif"/></a>';
    }

    if ($row['componenttype'] && optionInitComponent($row['componenttype'], $row['componentname'], $site_id, $row['page_id']) )
    {
      $path = $parent_path.'\\'.$row['name'];
      if ($row['componentname'])
      {
        $obj = new $row['componenttype']($row['componentname']);
        $obj->processOptionBeforeDisplay($path, $row, $form_data[$key]);
      }
      else
      {
        call_user_func_array(Array($row['componenttype'], 'processOptionBeforeDisplay'), Array($path, $row, &$form_data[$key]) );
      }

    }

    if($row['type'] == 'wysiwyg'){

			if(!$fieldparams['dialog']){
      	$form_data[$key]['dialog'] = '?module=wysiwyg&site_id='.$site_id;
			}

    }

  }

}

function getOptionCatsCount($opt_id, $site_id)
{
  return sqlQueryValue("SELECT COUNT(*) FROM `".$site_id."_options` WHERE parent=".$opt_id." AND is_cat=1");
}

function getOptionFieldsCount($opt_id, $site_id)
{
  return sqlQueryValue("SELECT COUNT(*) FROM `".$site_id."_options` WHERE parent=".$opt_id." AND is_cat=0");
}

function getOptionCatFieldsCount($opt_id, $site_id, $pagepropsmode=false)
{
  $where = '';
  if ($pagepropsmode) $where = ' AND show_pageprop=1';
  return sqlQueryValue("SELECT SUM(( SELECT COUNT(*) FROM `".$site_id."_options` WHERE is_cat=0".$where." AND parent IN (SELECT id FROM `".$site_id."_options` WHERE parent=".$opt_id." AND is_cat=1) ))");
}


function displayAndProcessOptionForms($opt_id, $site_id, $pagepropsmode=false, $title='')
{
  if ($_SESSION['userdata']['username'] == 'Admin')
  {
    if ($_GET['delopt'])
    {
      deleteOption(intval($_GET['delopt']));
    }
  }

  $advanced = ($_SESSION['userdata']['username'] == 'Admin' && $_SESSION['options_show_advanced']);

  $form_data = array();

  $fields = getOptionFieldsCount($opt_id, $site_id);

  if ($fields)
    prepareOptionForm($form_data, $opt_id, $opt_id, $site_id, $advanced, $pagepropsmode, $title);

  if (!$pagepropsmode)
  {
    $subcats = getOptionCatsCount($opt_id, $site_id);
    if ($subcats)
    {
      $where = '';
      if ($pagepropsmode) $where = ' AND show_pageprop=1';

      $data = sqlQueryData("SELECT id, (SELECT COUNT(*) FROM `".$site_id."_options` WHERE parent=o.id AND is_cat=0".$where.") AS count FROM `".$site_id."_options` AS o WHERE parent=".$opt_id." AND is_cat=1");
      foreach ($data as $row)
      {
        if (!$row['count']) continue;
        prepareOptionForm($form_data, $opt_id, $row['id'], $site_id, $advanced, $pagepropsmode);
      }
    }
  }
//echo '<pre>'; var_dump($form_data); echo '</pre>';

  $FormGen = new FormGen();

  $g = $_GET;
  unset($g['delopt']);
  $g['opt_parent_id'] = $opt_id;

  $FormGen->action = '?'.http_build_query($g);
  $FormGen->cancel = "back";
  $FormGen->hidereset = true;
  $FormGen->properties = $form_data;
  $FormGen->buttons = true;
  $FormGen->name = 'form'.$parent_id;

  return $FormGen->output();
}

