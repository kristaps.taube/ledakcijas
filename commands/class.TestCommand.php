<?php

namespace commands;

use Constructor\Command;
use Constructor\ConsoleApp;

class TestCommand extends Command
{

    public function actionIndex($params = [])
    {

        $app = ConsoleApp::$app;

        $col = \CircleKCollection::getInstance();
        $order_form = \OrderForm::getInstance();

        $test1 = isset($params['test1']) ? $params['test1'] : 1;
        
        file_put_contents($app->getConfigValue('log/directory').'crontab.log', date("d.m.Y H:i:s")." ".$test1.PHP_EOL, FILE_APPEND);
        if($test1 <= 10){
            Command::run('test', ['test1' => $test1 + 1]);
        }

    }

    public function actionTest($params = [])
    {

        echo 'test';

    }

}