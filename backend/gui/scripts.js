/*
Copyright © 2002 Karlis Blumentals. All Rights Reserved.
e-mail: kblums@latnet.lv
*/

// usage: openDlg(url, obj, {w:800, h:600}, function(val){ console.log(val); });
function openDlg(theURL, args, params, callback){

	var w = (typeof params.w == 'undefined') ? 400 : params.w;
	var h = (typeof params.h == 'undefined') ? 450 : params.h;
	var resize = (typeof params.resize == 'undefined') ? 0 : params.resize;
	var scrollable = (typeof params.scrollable == 'undefined') ? 0 : params.scrollable;
	var toolbar = (typeof params.toolbar == 'undefined') ? 0 : params.toolbar;
	var dialogleft = (typeof params.dialogleft == 'undefined') ? "" : " dialogLeft:"+l+"px;";
	var dialogtop = (typeof params.dialogtop == 'undefined') ? "" : " dialogTop:"+t+"px;";

  kid = openDialog(theURL, w, h, resize, scrollable, toolbar);
  kid.dialogArguments = args;
  if(typeof callback == 'function'){
  	kid.opener_callback_function = callback;
  }

}

function openModalWin(theURL,args,w,h,resize,l,t) {
  if (w == null) w = 400;
  if (h == null) h = 450;
  if (resize == null) resize = 0;
  dialogleft = '';
  dialogtop = '';
  if (l != null) dialogleft = " dialogLeft:"+l+"px;";
  if (t != null) dialogtop = " dialogTop:"+t+"px;";

  var arr = showModalDialog(theURL, args, "font-family:Verdana; font-size:12; dialogWidth:"+w+"px; dialogHeight:"+h+"px; help:0; status:0; scroll: 0; resizable:"+resize+";"+ dialogleft + dialogtop);
  if (arr != null) {
    return arr;
  } else {
    return "";
  }
}

function openDlgWin(theURL,args,w,h) {
  if (w == null) w = 400;
  if (h == null) h = 450;
  try
  {
    var arr = showModelessDialog(theURL, args, "font-family:Verdana; font-size:12; dialogWidth:"+w+"px; dialogHeight:"+h+"px; help:0; status:0; scroll: 0; resizable:1");
    if (arr != null) {
      return arr;
    } else {
      return "";
    }
  }
  catch(e)
  {
    win = openDialog(theURL, w, h, true, false, false);
    win.dialogArguments = args;
    return win;
  }

}

function openDialog(theURL, w, h, sizeable, scrollable, toolbar) {
  if (w == null) w = 500;
  if (h == null) h = 450;
  if(toolbar == null) toolbar = 0;
  var aw = screen.availWidth;
  var ah = screen.availHeight;
  if(ah - h < 50)
    var t = 0;
  else
    var t = (ah - h) / 2;
  if(aw - w < 50)
    var l = 0;
  else
    var l = (aw - w) / 2;
  return window.open(theURL,"","top="+t+",left="+l+",toolbar="+toolbar+",location=0,directories=0,status=0,menubar=0,scrollbars="+scrollable+",resizable="+sizeable+",width="+w+",height="+h+",");
}


function openNewWindow(theURL) {
  return window.open(theURL);
}


function retVal(form, property, thevalue, url) {
  if (parent) {
    eval('var ok = parent.document.'+form+';');
    if (ok) eval('var ok = parent.document.'+form+'.'+property+';');
    if (ok) eval('parent.document.'+form+'.'+property+'.value = thevalue');
  }
  if (url) location = url;
}


function appendQueryString(str)
{
  var date = new Date();
  var name = "" + date.getTime();
  if((str == null) || (str.length == 0))  str = '?';
  else
  {
    if(str.charAt(0) != '?')  str = '?' + str;
    if(str.length != 1)  str += '&';
  }
  str += name;
  return str;
}


function DefaultProperty(el,str,textbox,prevval)
{
  try
  {
    //var el = event.srcElement;
    if (el.checked) {
      textbox.value = str;
      textbox.style.backgroundColor = '#F0F0F0';
    }else{
      textbox.value = prevval;
      textbox.style.backgroundColor = '#FFFFFF';
    }
  }catch(e)
  {}
}

function UnDefaultControl(control)
{
  control.style.backgroundColor = '';
}

var PWin = null;
var PWinOpened = false;
function PropertiesWindow(theUrl, theTypeName, theDiv, w, h, sizeable, scrollable, setWindow)
{
  if(!PWinOpened)
  {
    try
    {
      PWin.iframe.document.write('Loading...');
      PWin.iframe.location.href = theUrl + '&action=component_frame';
      PWin.dialogArguments[1] = theDiv;
      PWin.selectcomponent.value = theTypeName;
    }
    catch(e)
    {
      try
      {
      if(!PWin.closed)
        PWin.close();
      }catch(e){}
      var a = new Array(2);
      if(!setWindow)
        a[0] = window;
      else
        a[0] = setWindow;
      a[1] = theDiv;
      PWin = openDlgWin(theUrl + '&action=component_form',a , w + 50, h, sizeable, scrollable);
      //PWin = openDialog(theUrl + '&action=component_form', w, h, sizeable, scrollable);
    }
    PWinOpened = true;
    setTimeout("PWinOpened = false;", 200);
  }
}

function SubmitPageVisibleCheckbox(el, pageID,siteID)
{
  if(el.checked)
  {
    window.location.href = "?module=pages&site_id=" + siteID + "&id=" + pageID + "&action=setpagevisible" + "&selrow=" + pageID;
  }else
  {
    window.location.href = "?module=pages&site_id=" + siteID + "&id=" + pageID + "&action=unsetpagevisible" + "&selrow=" + pageID;
  }
}

function SubmitPageEnabledCheckbox(el, pageID,siteID)
{
  if(el.checked)
  {
    window.location.href = "?module=pages&site_id=" + siteID + "&id=" + pageID + "&action=setpageenabled" + "&selrow=" + pageID;
  }else
  {
    window.location.href = "?module=pages&site_id=" + siteID + "&id=" + pageID + "&action=unsetpageenabled" + "&selrow=" + pageID;
  }
}

function SubmitDocPublicCheckbox(el, docID,siteID,parentDir)
{
  if(el.checked)
  {
    window.location.href = "?module=docs&site_id=" + siteID + "&id=" + docID + "&action=setdocpublic&parent_dir=" + parentDir + "&selrow=" + docID;
  }else
  {
    window.location.href = "?module=docs&site_id=" + siteID + "&id=" + docID + "&action=unsetdocpublic&parent_dir=" + parentDir + "&selrow=" + docID;
  }
}

function SubmitWapPageVisibleCheckbox(el, pageID,siteID)
{
  if(el.checked)
  {
    window.location.href = "?module=wap&site_id=" + siteID + "&id=" + pageID + "&action=setpagevisible" + "&selrow=" + pageID;
  }else
  {
    window.location.href = "?module=wap&site_id=" + siteID + "&id=" + pageID + "&action=unsetpagevisible" + "&selrow=" + pageID;
  }
}

function SubmitWapPageEnabledCheckbox(el, pageID,siteID)
{
  if(el.checked)
  {
    window.location.href = "?module=wap&site_id=" + siteID + "&id=" + pageID + "&action=setpageenabled" + "&selrow=" + pageID;
  }else
  {
    window.location.href = "?module=wap&site_id=" + siteID + "&id=" + pageID + "&action=unsetpageenabled" + "&selrow=" + pageID;
  }
}

function SubmitComponentEnabledCheckbox(el, componentID)
{
  if(el.checked)
  {
    window.location.href = "?module=components&component_id=" + componentID + "&action=setcomponentenabled";
  }else
  {
    window.location.href = "?module=components&component_id=" + componentID + "&action=unsetcomponentenabled";
  }
}

function SubmitComponentEnabledCheckboxFile(el, componentFile)
{
  window.location.href = "?module=components&component_file=" + componentFile + "&action=setcomponentenabledfile";
}

function AddText(text)
{

  if (document.Form.bodytext.createTextRange && document.Form.bodytext.caretPos) {

    var caretPos = document.Form.bodytext.caretPos;
    caretPos.text = text;
  } else {
    document.Form.bodytext.value += text;
  }
  document.Form.bodytext.focus();

}

function AddTextEditArea(text)
{

  editAreaLoader.setSelectedText('template_body', text);

}

function FindAvailableComponentName(s, componenttype, pageid)
{
  //{%component:\'+combocomponents.value+\':\'+s+\'%}
  St = new String(s);
  f = 1;
  while(St.indexOf("{%component:"+componenttype+":"+componenttype+pageid+"n"+f+"%}")>-1)
  {
    f++
  }
  return componenttype+pageid+"n"+f;
}

function AddTextToArea(text, bodytext)
{

  bodytext.value += '\n';
  bodytext.value += text;
  bodytext.focus();
}

function storeCaret(ftext) {
        if (ftext.createTextRange) {
                ftext.caretPos = document.selection.createRange().duplicate();
        }
}

function changeComponentElement(newname, newlocation)
{
  try
  {
    var win = window.dialogArguments[0];
    var component = newname.substr(newname.indexOf('&component_name=')+16,newname.length);

    window.dialogArguments[1] = win.document.getElementById("div_" + component);
  }catch(e){}
  window.iframe.document.write('Loading...');
  window.iframe.location.search = newlocation;
}

function URLEncode(plaintext)
{
        // The Javascript escape and unescape functions do not correspond
        // with what browsers actually do...
        var SAFECHARS = "0123456789" +                                  // Numeric
                                        "ABCDEFGHIJKLMNOPQRSTUVWXYZ" +  // Alphabetic
                                        "abcdefghijklmnopqrstuvwxyz" +
                                        "-_.!~*'()";                                    // RFC2396 Mark characters
        var HEX = "0123456789ABCDEF";

        var encoded = "";
        for (var i = 0; i < plaintext.length; i++ ) {
                var ch = plaintext.charAt(i);
            if (ch == " ") {
                    encoded += "+";                             // x-www-urlencoded, rather than %20
                } else if (SAFECHARS.indexOf(ch) != -1) {
                    encoded += ch;
                } else {
                    var charCode = ch.charCodeAt(0);
                        if (charCode > 255) {
                                encoded += escape(ch);
                        } else {
                                encoded += "%";
                                encoded += HEX.charAt((charCode >> 4) & 0xF);
                                encoded += HEX.charAt(charCode & 0xF);
                        }
                }
        } // for


        return encoded;
};

function EnableToolbarButtons(a)
{
  for(f=0;f<a.length;f++)
  {
    var re = new RegExp ('/navigation_disabled/') ;
    try
    {
      document.getElementById("toolbarimage_" + a[f]).style.backgroundImage = document.getElementById("toolbarimage_" + a[f]).style.backgroundImage.replace(re, '/navigation/');
    }
    catch(e){}
  }
}

function DisableToolbarButtons(a)
{
  for(f=0;f<a.length;f++)
  {
    var re = new RegExp ('/navigation/') ;
    //eval("alert(toolbarimage_" + a[f] + ".src.replace(re, '/toolbar/disabled/'));");
    try
    {
      document.getElementById("toolbarimage_" + a[f]).style.backgroundImage = document.getElementById("toolbarimage_" + a[f]).style.backgroundImage.replace(re, '/navigation_disabled/');
    }
    catch(e){}
  }
}

function UnExpandCollectionBox(type)
{
  document.getElementById(type+'_expanded').style.display = 'inline';
  document.getElementById(type+'_collapsed').style.display = 'none';
}

function ExpandCollectionBox(type)
{
  document.getElementById(type+'_expanded').style.display = 'none';
  document.getElementById(type+'_collapsed').style.display = 'inline';
}

function AjaxLoadComponent(divname, name, type, paramstr, page_id, pagedev_id, site_id, session)
{

  if(GlobalsIndicatorImage)
    document.getElementById(divname).innerHTML = GlobalsIndicatorImage;
  else
    document.getElementById(divname).innerHTML = 'Loading...';


  var url = '?module=components&action=ajaxdesign&component_name='+name+'&component_type='+type+'&paramstr='+paramstr+'&page_id='+page_id+'&pagedev_id='+pagedev_id+'&site_id='+site_id+session;

  $.get(url, function(data)
  {
    $('#'+divname).replaceWith(data);
  });

}

function includeJsOnce(file) {
   if (document.createElement && document.getElementsByTagName) {
     var head = document.getElementsByTagName('head')[0];

     var scripts = head.getElementsByTagName('script');
     if(scripts && scripts.length > 0)
     {
       for(var f = 0; f < scripts.length; f++)
       {
         var scr = scripts[f].getAttribute('src');
         if(scr == file)
           return false;
       }
     }

     var script = document.createElement('script');
     script.setAttribute('type', 'text/javascript');
     script.setAttribute('src', file);

     head.appendChild(script);
     return true;
   } else {
     return false;
   }
 }


function getCurrentStyle(elem, name) {
    // J/S Pro Techniques p136
    if (elem.style && elem.style[name]) {
        return elem.style[name];
    } else if (elem.currentStyle) {
        return elem.currentStyle[name];
    }
    else if (document.defaultView && document.defaultView.getComputedStyle) {
        name = name.replace(/([A-Z])/g, "-$1");
        name = name.toLowerCase();
        s = document.defaultView.getComputedStyle(elem, "");
        return s && s.getPropertyValue(name);
    } else {
        return null;
    }
}

//firefox compatibility stuff....
/*if (typeof(HTMLElement) != "undefined")
{
  if(HTMLElement.__defineGetter__)
  {
       if (HTMLElement) {
              var element = HTMLElement.prototype;
              if (element.__defineGetter__) {
                     element.__defineGetter__("outerHTML",
                           function () {
                                  var parent = this.parentNode;
                                  var el = document.createElement(parent.tagName);
                                  var el2 = this.cloneNode(true);
                                  el.appendChild(el2);
                                  var shtml = el.innerHTML;
                                  el2.removeNode();
                                  return shtml;
                           }
                     );
              }
       }

       HTMLElement.prototype.__defineSetter__("outerHTML", function (sHTML) {
         var r = this.ownerDocument.createRange();
         r.setStartBefore(this);
         var df = r.createContextualFragment(sHTML);
         this.parentNode.replaceChild(df, this);
      });
  }
}  */


if ( window.Node && typeof(Node.prototype) != 'undefined' )
{
  if(!Node.removeNode)
  {
    Node.prototype.removeNode = function( removeChildren )
    {
    	var self = this;
    	if ( Boolean( removeChildren ) )
    	{
    		return this.parentNode.removeChild( self );
    	}
    	else
    	{
    		var range = document.createRange();
    		range.selectNodeContents( self );
    		return this.parentNode.replaceChild( range.extractContents(), self );
    	}
    }
  }

  if(!Node.replaceNode)
  {
    Node.prototype.replaceNode = function( newNode )
    {
  		return this.parentNode.replaceChild( newNode, this );
    }
  }
}



var cnst = {
  isFF: (navigator.userAgent.match("Firefox") || navigator.userAgent.match("chrome") || navigator.userAgent.match("shiretoko")),
  isIE: navigator.userAgent.match("MSIE"),
  isCH: navigator.userAgent.match("chrome")
}

if(typeof(console) == 'undefined')
{
  console = {
    log: function(s)
    {
      //plug for firebug
    }
  }
}

/**
 * Generates SHA-256 hash of string.
 *
 * @param   {string} msg - String to be hashed
 * @returns {string} Hash of msg as hex character string
 */
var Sha256 = {};   

Sha256.hash = function(msg) {
    // convert string to UTF-8, as SHA only deals with byte-streams
    msg = msg.utf8Encode();

    // constants [§4.2.2]
    var K = [
        0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5, 0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
        0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3, 0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
        0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc, 0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
        0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7, 0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
        0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13, 0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
        0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3, 0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
        0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5, 0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
        0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208, 0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2 ];
    // initial hash value [§5.3.1]
    var H = [
        0x6a09e667, 0xbb67ae85, 0x3c6ef372, 0xa54ff53a, 0x510e527f, 0x9b05688c, 0x1f83d9ab, 0x5be0cd19 ];

    // PREPROCESSING

    msg += String.fromCharCode(0x80);  // add trailing '1' bit (+ 0's padding) to string [§5.1.1]

    // convert string msg into 512-bit/16-integer blocks arrays of ints [§5.2.1]
    var l = msg.length/4 + 2; // length (in 32-bit integers) of msg + ‘1’ + appended length
    var N = Math.ceil(l/16);  // number of 16-integer-blocks required to hold 'l' ints
    var M = new Array(N);

    for (var i=0; i<N; i++) {
        M[i] = new Array(16);
        for (var j=0; j<16; j++) {  // encode 4 chars per integer, big-endian encoding
            M[i][j] = (msg.charCodeAt(i*64+j*4)<<24) | (msg.charCodeAt(i*64+j*4+1)<<16) |
                      (msg.charCodeAt(i*64+j*4+2)<<8) | (msg.charCodeAt(i*64+j*4+3));
        } // note running off the end of msg is ok 'cos bitwise ops on NaN return 0
    }
    // add length (in bits) into final pair of 32-bit integers (big-endian) [§5.1.1]
    // note: most significant word would be (len-1)*8 >>> 32, but since JS converts
    // bitwise-op args to 32 bits, we need to simulate this by arithmetic operators
    M[N-1][14] = ((msg.length-1)*8) / Math.pow(2, 32); M[N-1][14] = Math.floor(M[N-1][14]);
    M[N-1][15] = ((msg.length-1)*8) & 0xffffffff;


    // HASH COMPUTATION [§6.1.2]

    var W = new Array(64); var a, b, c, d, e, f, g, h;
    for (var i=0; i<N; i++) {

        // 1 - prepare message schedule 'W'
        for (var t=0;  t<16; t++) W[t] = M[i][t];
        for (var t=16; t<64; t++) W[t] = (Sha256.σ1(W[t-2]) + W[t-7] + Sha256.σ0(W[t-15]) + W[t-16]) & 0xffffffff;

        // 2 - initialise working variables a, b, c, d, e, f, g, h with previous hash value
        a = H[0]; b = H[1]; c = H[2]; d = H[3]; e = H[4]; f = H[5]; g = H[6]; h = H[7];

        // 3 - main loop (note 'addition modulo 2^32')
        for (var t=0; t<64; t++) {
            var T1 = h + Sha256.Σ1(e) + Sha256.Ch(e, f, g) + K[t] + W[t];
            var T2 =     Sha256.Σ0(a) + Sha256.Maj(a, b, c);
            h = g;
            g = f;
            f = e;
            e = (d + T1) & 0xffffffff;
            d = c;
            c = b;
            b = a;
            a = (T1 + T2) & 0xffffffff;
        }
         // 4 - compute the new intermediate hash value (note 'addition modulo 2^32')
        H[0] = (H[0]+a) & 0xffffffff;
        H[1] = (H[1]+b) & 0xffffffff;
        H[2] = (H[2]+c) & 0xffffffff;
        H[3] = (H[3]+d) & 0xffffffff;
        H[4] = (H[4]+e) & 0xffffffff;
        H[5] = (H[5]+f) & 0xffffffff;
        H[6] = (H[6]+g) & 0xffffffff;
        H[7] = (H[7]+h) & 0xffffffff;
    }

    return Sha256.toHexStr(H[0]) + Sha256.toHexStr(H[1]) + Sha256.toHexStr(H[2]) + Sha256.toHexStr(H[3]) +
           Sha256.toHexStr(H[4]) + Sha256.toHexStr(H[5]) + Sha256.toHexStr(H[6]) + Sha256.toHexStr(H[7]);
};


/**
 * Rotates right (circular right shift) value x by n positions [§3.2.4].
 * @private
 */
Sha256.ROTR = function(n, x) {
    return (x >>> n) | (x << (32-n));
};

/**
 * Logical functions [§4.1.2].
 * @private
 */
Sha256.Σ0  = function(x) { return Sha256.ROTR(2,  x) ^ Sha256.ROTR(13, x) ^ Sha256.ROTR(22, x); };
Sha256.Σ1  = function(x) { return Sha256.ROTR(6,  x) ^ Sha256.ROTR(11, x) ^ Sha256.ROTR(25, x); };
Sha256.σ0  = function(x) { return Sha256.ROTR(7,  x) ^ Sha256.ROTR(18, x) ^ (x>>>3);  };
Sha256.σ1  = function(x) { return Sha256.ROTR(17, x) ^ Sha256.ROTR(19, x) ^ (x>>>10); };
Sha256.Ch  = function(x, y, z) { return (x & y) ^ (~x & z); };
Sha256.Maj = function(x, y, z) { return (x & y) ^ (x & z) ^ (y & z); };


/**
 * Hexadecimal representation of a number.
 * @private
 */
Sha256.toHexStr = function(n) {
    // note can't use toString(16) as it is implementation-dependant,
    // and in IE returns signed numbers when used on full words
    var s="", v;
    for (var i=7; i>=0; i--) { v = (n>>>(i*4)) & 0xf; s += v.toString(16); }
    return s;
};


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */


/** Extend String object with method to encode multi-byte string to utf8
 *  - monsur.hossa.in/2012/07/20/utf-8-in-javascript.html */
if (typeof String.prototype.utf8Encode == 'undefined') {
    String.prototype.utf8Encode = function() {
        return unescape( encodeURIComponent( this ) );
    };
}

/** Extend String object with method to decode utf8 string to multi-byte */
if (typeof String.prototype.utf8Decode == 'undefined') {
    String.prototype.utf8Decode = function() {
        try {
            return decodeURIComponent( escape( this ) );
        } catch (e) {
            return this; // invalid UTF-8? return as-is
        }
    };
}