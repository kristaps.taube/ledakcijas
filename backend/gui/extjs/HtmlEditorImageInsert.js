Ext.namespace('Ext.ux', 'Ext.ux.plugins');

Ext.ux.plugins.HtmlEditorImageInsert = function(config) {

    config = config || {};

    Ext.apply(this, config);

    this.init = function(htmlEditor) {
        this.editor = htmlEditor;
        this.editor.on('render', onRender, this);
    };

    this.imageInsertConfig = {
        popTitle: config.popTitle || 'Image URL',
        popMsg: config.popMsg || 'Please select the URL of the image you want to insert:',
        popWidth: config.popWidth || 350,
        popValue: config.popValue || '',
        site_id: config.site_id || '0'
    }

    this.bigEditor = function(){

        this.editor.syncValue();
        //var val=openModalWin('?module=wysiwyg&site_id=' + this.imageInsertConfig.site_id, this.editor.getRawValue(), 720, 450, 0);
        var me = this;


        openDlg('?module=wysiwyg&site_id=' + this.imageInsertConfig.site_id, this.editor.getRawValue(), {w: 720, h: 450}, function(val){

        	if (val) {
            me.editor.setRawValue(val);
            me.editor.pushValue();
        	}

        });

    }

    this.imageInsert = function(){
        /*Ext.MessageBox.show({
            title: this.imageInsertConfig.popTitle,
            msg: this.imageInsertConfig.popMsg,
            width: this.imageInsertConfig.popWidth,
            buttons: Ext.MessageBox.OKCANCEL,
            prompt: true,
            value: this.imageInsertConfig.popValue,
            scope: this,
            fn: function(btn, text){
                if ( btn == 'ok' )
                    this.editor.relayCmd('insertimage', text);
            }
       });*/

       var me = this;


			 var args = new Array();
       openDlg("?module=wysiwyg&action=image&site_id=" + this.imageInsertConfig.site_id + "&view=thumbs&", args, {w: 530, h: 450}, function(arr){

	       if (arr != null){
	         var s = "<img src=\"" + arr["src"] + "\"";
	         if (arr["vspace"] != "") s = s + " vspace=\"" + arr["vspace"] + "\"";
	         if (arr["hspace"] != "") s = s + " hspace=\"" + arr["hspace"] + "\"";
	         if (arr["align"] != "") s = s + " align=\"" + arr["align"] + "\"";
	         s = s + " border=\"0\" />";

	         if (Ext.isIE) {
	             // get selected text/range
	             var selection = me.editor.doc.selection;
	             var range = selection.createRange();

	             // insert the image over the selected text/range
	             range.pasteHTML(s);

	         } else
	         {
	             me.editor.relayCmd('inserthtml', s);
	         }
	       }

       });

       //var arr = showModalDialog( "?module=wysiwyg&action=image&site_id=" + this.imageInsertConfig.site_id + "&view=thumbs&", args, "font-family:Verdana; font-size:12; dialogWidth:530px; dialogHeight:450px; help:0; status:0");

    }

    function onRender() {
        if (!Ext.isSafari) {
            this.editor.tb.addSeparator();
            this.editor.tb.insertButton(17, {
                itemId : 'htmlEditorImage',
                cls : 'x-btn-icon x-edit-insertimage',
                enableToggle: false,
                scope: this,
                handler:function(){ this.imageInsert(); },
                clickEvent:'mousedown',
                tooltip : {
                		title: 'Insert Image',
                		text: 'Display image selection dialog',
                		cls: 'x-html-editor-tip'
                },
                tabIndex:-1
            });
            this.editor.tb.addFill();
            this.editor.tb.add({
                itemId : 'htmlEditorBigEdit',
                cls : 'x-btn-icon x-edit-bigedit',
                enableToggle: false,
                scope: this,
                handler:function(){ this.bigEditor(); },
                clickEvent:'mousedown',
                tooltip : {
                		title: 'Open Editor',
                		text: 'Open text in WYSIWYG editor',
                		cls: 'x-html-editor-tip'
                },
                tabIndex:-1
            });
        }
    }

}