function initFormatedtext0(conf)
{

    id = conf.id;

    if(!$("#top_toolbar").length){
        $("body").append("<div style=position:fixed;top:0;z-index:100000;width:100%; id='top_toolbar'></div>");
    }

    var wrap = $("#"+id);
    if(!wrap.length){
        console.log('Formatedtext error: no ' + id + ' element ');
        return;
    }

    $(wrap).click(function(){
        createCKEditor(wrap, id);
    });

    function createCKEditor(wrap, id)
    {

        var content = wrap.html();
        var textarea_id = 'text_' + id;
        var form_id = 'form_' + id;
        wrap.html('<form id="' + form_id + '"><textarea id="' + textarea_id +'"></textarea></form>');
        var textarea = $("#"+textarea_id);
        textarea.val(content);

        var CK = CKEDITOR.replace(textarea_id, {
            allowedContent: true,
            on: {
                save: function(evt){
                    save(wrap, textarea_id);
                    return false;
                }
            },
            sharedSpaces :{
                top : "top_toolbar"
            }
        });

    }

    function save(wrap, textarea_id)
    {

        var editor = CKEDITOR.instances[textarea_id];
        var content = editor.getData();
        editor.destroy();
        addLoader(wrap);
        $.ajax({
            type: "POST",
            url: conf.save_url,
            data: { page_id: conf.page_id, name: conf.name, content: content },
            success: function(response) {
                wrap.html(content);
            }
        });

    }

}

function addLoader(where, opts){

    // add loader
    var loader_id = "loader"+Math.random();
    where.html("<div id='" + loader_id + "' class='loading spinner'></div>");

    var def_opts = {
        lines: 8, // The number of lines to draw
        length: 5, // The length of each line
        width: 3, // The line thickness
        radius: 3, // The radius of the inner circle
        color: '#000000', // #rgb or #rrggbb
        speed: 1.5, // Rounds per second
        trail: 50, // Afterglow percentage
        shadow: false // Whether to render a shadow
    };

    opts = (typeof opts !== "undefined")? opts : def_opts;

    var target = document.getElementById(loader_id);
    var spinner = new Spinner(opts).spin(target);

    return loader_id;

}