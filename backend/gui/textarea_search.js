var search_textarea = {

  'text_obj' : null,
  'search_controls' : null,
  'search_button' : null,
  'find_next_button' : null,
  'input_box_form' : null,
  'last_offset' : 0,
  'range' : null,

  'LANG_not_found' : 'Search string not found!',
  'LANG_searh' : 'Search',
  'LANG_find_next' : 'Find next',
  'LANG_go' : 'Go!',

  'init' : function(text_element, search_controls) {

    if(cnst.isIE)
    {

      var self = this;

      this.search_controls  = document.getElementById(search_controls);
      this.text_obj = document.getElementById(text_element);

      this.search_button = document.createElement('input');
      this.search_button.type = 'button';
      this.search_button.value = this.LANG_searh;

      this.search_controls.appendChild(this.search_button);

      this.search_button.onclick = function() {

        if (self.input_box_form == null) {
          self.add_input_box();
        } else {
          self.show_input_box();
        }

      }
    }

  },

  'start_search' : function(search_str) {

    if (search_str != '') {

      this.text_obj.focus();

      this.range = this.text_obj.createTextRange();

      this.range.expand("textedit");

      var text = this.text_obj.value;

      var found_at_offset = text.indexOf(search_str);

      if (found_at_offset > -1) {

        this.last_offset = found_at_offset;

        this.highlight_text(search_str);

        if (this.find_next_button == null) {
          this.add_find_next_button(search_str);
        } else {
          this.update_find_next_button(search_str);
        }

        if (!this.has_no_next_occurances(search_str, found_at_offset)) {
          this.text_obj.blur();
          this.disable_find_next_button();
        } else {
          this.find_next_button.focus();
        }

      } else {
        alert(this.LANG_not_found);
      }

    }

  },

  'find_next' : function(search_str) {

    this.text_obj.focus();

    var text = this.text_obj.value;

    var text = text.substring(this.last_offset + search_str.length);

    var relative_offset = text.indexOf(search_str);

    var next_offset
        = this.last_offset + search_str.length + relative_offset;

    if (relative_offset > -1) {

      this.highlight_text(search_str);

      if (!this.has_no_next_occurances(search_str, next_offset)) {
        this.disable_find_next_button();
        this.text_obj.blur();
      } else {
        this.find_next_button.focus();
      }

    }

    this.last_offset = next_offset;

  },

  'highlight_text' : function(search_str) {

    this.text_obj.focus();

    this.range.findText(search_str);

    this.range.select();

    this.range.moveEnd('textedit');

    this.range.moveStart('character', search_str.length);

  },

  'add_input_box' : function() {

    var self = this;

    var input_box_form = document.createElement('form');;
    input_box_form.action = '#';
    input_box_form.method = 'get';
    input_box_form.style.display = 'inline';

    var input_box = document.createElement('input');
    input_box.type = 'text';

    input_box_form.appendChild(input_box);

    var input_submit = document.createElement('input');
    input_submit.type = 'submit';
    input_submit.value = this.LANG_go;

    input_box_form.appendChild(input_submit);

    this.search_button.parentNode.insertBefore(
        input_box_form, this.search_button.nextSibling
    );

    input_box.focus();

    input_box_form.onsubmit = function() {
      var search_str = this.getElementsByTagName('input')[0].value;
      self.start_search(search_str);
      return false;
    }

    this.input_box_form = input_box_form;

  },

  'show_input_box' : function() {
    this.hide_find_next();
    this.input_box_form.style.display = 'inline';
    this.input_box_form.getElementsByTagName('input')[0].focus();
  },

  'hide_input_box' : function() {
    this.input_box_form.style.display = 'none';
  },

  'add_find_next_button' : function(search_str) {

    var self = this;

    this.hide_input_box();

    var find_next_button = document.createElement('input');
    find_next_button.type = 'button';
    find_next_button.value = this.LANG_find_next+' "'+search_str+'"';

    this.search_button.parentNode.insertBefore(
        find_next_button, this.search_button.nextSibling
    );

    find_next_button.onclick = function() {
      self.find_next(search_str);
    }

    this.find_next_button = find_next_button;

  },

  'update_find_next_button' : function(search_str) {

    this.hide_input_box();
    this.show_find_next();

    var self = this;

    if (this.find_next_button.disabled == true) {
      this.find_next_button.disabled = false;
    }

    this.find_next_button.value = this.LANG_find_next+' "'+search_str+'"';

    this.find_next_button.onclick = function() {
      self.find_next(search_str);
    }

  },

  'disable_find_next_button' : function() {
    this.find_next_button.disabled = true;
  },

  'show_find_next' : function() {
    this.find_next_button.style.display = '';
  },

  'hide_find_next' : function() {
    this.find_next_button.style.display = 'none';
  },

  'has_no_next_occurances' : function(search_str, next_offset) {

    this.text_obj.focus();

    var text = this.text_obj.value;

    var text = text.substring(next_offset + search_str.length);

    var relative_offset = text.indexOf(search_str);

    if (relative_offset > -1) {
      return true;
    }

    return false;

  }

}
