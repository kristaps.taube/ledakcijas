<? require('../../config.php');   
$GLOBALS['site_id'] = (int)$_GET['site_id'];

$cmsparams = array(
  'site_id' => (int)$_GET['site_id'],
  'page_id' => (int)$_GET['page_id'],
  'component_page_id' => (int)$_GET['component_page_id'],
  'web_root' => $GLOBALS['cfgWebRoot'],
  'component_name' => $_GET['component_name']
);

?>
FCKConfig.CMSParams = <?=json_encode($cmsparams) ?>;

FCKConfig.Plugins.Add( 'googlemaps', 'lv,ru,en' ) ;
FCKConfig.Plugins.Add( 'youtube', 'lv,ru,en' ) ;
FCKConfig.Plugins.Add( 'flvPlayer', 'lv,ru,en' ) ;
FCKConfig.Plugins.Add( 'constructor_additional', 'lv,ru,en' ) ;

FCKConfig.CMSComponents = {
  dynamic_form: {
    dialog: {
      width: 600,
      height: 550
    }
  },
  dynamic_gallery: {
    dialog: {
      width: 800,
      height: 700
    }
  }
};

FCKConfig.GoogleMaps_Key = '<?=option('googlemaps_key', null, 'Google maps API key'); ?>';

FCKConfig.ToolbarSets["Default"] = [
['Save','NewPage','Preview','-','Templates'],
['Cut','Copy','Paste','PasteText','PasteWord','-','Print'],
['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
['Source'],
'/',
['Bold','Italic','Underline','StrikeThrough','-','Subscript','Superscript'],
['OrderedList','UnorderedList','-','Outdent','Indent','Blockquote'],
['JustifyLeft','JustifyCenter','JustifyRight','JustifyFull'],
['Link','Unlink','Anchor'],
['Image','Flash','YouTube','flvPlayer','googlemaps',<?=$_GET['add_component'] ? "'AddComponent'," : '' ?>'Table','Rule','SpecialChar'],
'/',
[/*'Style',*/'FontFormat','FontName','FontSize'],
['TextColor','BGColor'],
['FitWindow','ShowBlocks']
] ;

FCKConfig.FontSizes = '50%/mini;85%/small;100%/normal;150%/large;200%/huge';

FCKConfig.ToolbarSets["Basic"] = [
['Bold','Italic','Underline','-','Link','Unlink','Image','-','Source']
] ;

FCKConfig.MaxUndoLevels = 150 ;
FCKConfig.BrowserContextMenu = false; //<?=$GLOBALS['developmode'] ? 'true' : 'false' ?>;


FCKConfig.AutoDetectLanguage	= false ;
FCKConfig.DefaultLanguage		= '<?

if ($_GET['site_id'])
{
  $GLOBALS['site_id'] = $_GET['site_id'];
  $GLOBALS['cfgLanguage'] = option('CMS\\language', 'Latvian', 'Language', Array('value_on_create' => 1, 'type' => '  list', 'componenttype' => 'cmsconfig'));
}

session_start();

if (isset($_SESSION['cms_language']))
  $GLOBALS['cfgLanguage'] = $_SESSION['cms_language'];

$langs = array('' => 'en', 'Latvian' => 'lv', 'Russian' => 'ru');

$lang = $langs[$GLOBALS['cfgLanguage']];
if (!$lang) $lang = 'en';

echo $lang;

?>';


FCKConfig.SkinPath = '<?= $GLOBALS['cfgWebRoot']; ?>gui/fckeditor/editor/skins/silk/' ;

FCKConfig.ImageBrowserURL = '<?= $GLOBALS['cfgWebRoot']; ?>?module=dialogs&action=filex&site_id=<?= $_GET['site_id']; ?>&view=thumbs' ;
FCKConfig.ImageUploadURL = FCKConfig.BasePath + 'filemanager/connectors/' + _QuickUploadLanguage + '/upload.' + _QuickUploadExtension + '?site_id=<?= $_GET['site_id']; ?>' ;
FCKConfig.LinkBrowserURL = '<?= $GLOBALS['cfgWebRoot']; ?>?module=dialogs&action=links&site_id=<?= $_GET['site_id']; ?>' ;
FCKConfig.LinkUploadURL = FCKConfig.BasePath + 'filemanager/connectors/' + _QuickUploadLanguage + '/upload.' + _QuickUploadExtension + '?site_id=<?= $_GET['site_id']; ?>' ;
FCKConfig.FlashBrowserURL = '<?= $GLOBALS['cfgWebRoot']; ?>?module=dialogs&action=filex&site_id=<?= $_GET['site_id']; ?>&view=thumbs' ;
FCKConfig.FlashUploadURL = FCKConfig.BasePath + 'filemanager/connectors/' + _QuickUploadLanguage + '/upload.' + _QuickUploadExtension + '?site_id=<?= $_GET['site_id']; ?>' ;

FCKConfig.MediaBrowserURL   = '<?= $GLOBALS['cfgWebRoot']; ?>?module=dialogs&action=filex&site_id=<?= $_GET['site_id']; ?>&view=thumbs' ;
FCKConfig.MediaBrowserWindowWidth = 1000;
FCKConfig.MediaBrowserWindowHeight = 800;

<?
if($_GET['autogrow'])
{
?>
FCKConfig.Plugins.Add( 'autogrow' ) ;
FCKConfig.AutoGrowMax = 10000;
<?
}
?>

