CKEDITOR.dialog.add( 'paypalDialog', function( editor ) {
    return {
        title: 'Paypal Properties',
        minWidth: 400,
        minHeight: 200,

        contents: [
            {
                id:         'tab1',
                label:      'First Tab',
                title:      'First Tab Title',
                accessKey:  'Q',
                elements: [
																					{
                        type:           'text',
                        label:          'Konts',
                        id:             'konts',
                								validate: CKEDITOR.dialog.validate.notEmpty( "Jānorāda konts." ),
																								setup: function( element ) {
																            this.setValue( element.getAttribute('data-business') );
																        },
																								commit: function( element ) {
																            element.setAttribute('data-business', this.getValue() );
																        }
																				},
																					{
                        type:           'text',
                        label:          'Prece',
                        id:             'prece',
                        'default':      '',
                								validate: CKEDITOR.dialog.validate.notEmpty( "Jānorāda preces nosaukums." ),
																								setup: function( element ) {
																            this.setValue( element.getAttribute('data-item_name') );
																        },
																								commit: function( element ) {
																            element.setAttribute('data-item_name', this.getValue() );
																        }
																				},
																				{
                        type:           'text',
                        label:          'Summa',
                        id:             'sum',
                        'default':      '',
                								validate: CKEDITOR.dialog.validate.notEmpty( "Jānorāda apmaksas summa." ),
                								validate: CKEDITOR.dialog.validate.integer( "Summa jānorāda kā skaitlis." ),
																								setup: function( element ) {
																            this.setValue( element.getAttribute('data-amount') );
																        },
																								commit: function( element ) {
																            element.setAttribute('data-amount', this.getValue() );
																        }
                    },
                    {
                        type:           'text',
                        label:          'Pogas teksts',
                        id:             'text1',
                        'default':      '',
                								validate: CKEDITOR.dialog.validate.notEmpty( "Jānorāda pogas teksts." ),
																								setup: function( element ) {
																            this.setValue( element.getText() );
																        },
																								commit: function( element ) {
																            element.setText(this.getValue() );
																        }
																				},
																				{
                        type:           'text',
                        label:          'Pogas platums',
                        id:             'width',
                        'default':      '175',
                								validate: CKEDITOR.dialog.validate.integer( "Platums jānorāda kā skaitlis." ),
																								setup: function( element ) {
																            this.setValue( element.getStyle("width").split('p')[0] );
																        },
																								commit: function( element ) {
																            element.setStyle("width", this.getValue()+"px" );
																        }
                    },
																				{
                        type:           'text',
                        label:          'Pogas augstums',
                        id:             'height',
                        'default':      '25',
                								validate: CKEDITOR.dialog.validate.integer( "Augstums jānorāda kā skaitlis." ),
																									setup: function( element ) {
																            this.setValue( element.getStyle("height").split('p')[0] );
																        },
																								commit: function( element ) {
																            element.setStyle("height", this.getValue()+"px" );
																        }
                    },
                ]
            }
        ],
								onShow: function() {
          var selection = editor.getSelection();
										var element = selection.getStartElement();

										if ( element )
										    element = element.getAscendant( 'button', true );

										if ( !element || element.getName() != 'button' ) {
										    element = editor.document.createElement( 'button' );
										    this.insertMode = true;
										}
										else
										    this.insertMode = false;

										this.element = element;

										if ( !this.insertMode )
   							this.setupContent( element );

								},
								onOk: function() {
          var dialog = this;
          var buttons = $('.ClassyPaypal-button').length;

          var paypal = dialog.element;

										dialog.commitContent(paypal);

										paypal.setAttribute('class', 'ClassyPaypal-button');
										paypal.setAttribute('data-currency_code', 'EUR');
										paypal.setAttribute('data-button','buynow');
										paypal.setAttribute('async', 'async');
                                                      
									if(dialog.insertMode) {
	          editor.insertElement( paypal );
          }

      }
    };
});