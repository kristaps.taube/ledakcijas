CKEDITOR.plugins.add( 'paypal', {
    icons: 'paypal',
    init: function( editor ) {
        editor.addCommand( 'paypal', new CKEDITOR.dialogCommand( 'paypalDialog' ) );

								editor.ui.addButton( 'Paypal', {
							    label: 'Paypal',
							    command: 'paypal',
							    toolbar: 'insert'
							});
							CKEDITOR.dialog.add( 'paypalDialog', this.path + 'dialogs/paypal.js' );
							if ( editor.contextMenu ) {
						    editor.addMenuGroup( 'paypalGroup' );
						    editor.addMenuItem( 'paypalItem', {
						        label: 'Edit Paypal',
						        icon: this.path + 'icons/paypal.png',
						        command: 'paypal',
						        group: 'paypalGroup'

						    });
					    editor.contextMenu.addListener( function( element ) {
					        if ( element.getAscendant( 'button', true ) ) {
					            return { paypalItem: CKEDITOR.TRISTATE_OFF };
					        }
					    });

						}
    }
});
