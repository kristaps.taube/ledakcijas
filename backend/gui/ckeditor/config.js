/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	config.allowedcontent = true;
	config.extraPlugins = 'youtube,googlemaps,paypal';
	config.width = '100%';
	config.toolbarCanCollapse = true;
};
