/*
Copyright � 2002 Karlis Blumentals. All Rights Reserved.
e-mail: kblums@latnet.lv
*/

function toolbarOver(hint) {
  //bar.className= 'toolbarButtonOver';
  window.status = hint;
  return true;
}

function toolbarOut() {
  //bar.className= 'toolbarButton';
  window.status= '';
  return true;
}

function toolbarDown(bar) {
  bar.className= 'toolbarButtonDown';
}

function toolbarUp(bar) {
  bar.className= 'toolbarButtonOver';
}


//for the small toolbar
function toolbarOverSmall(bar, hint) {
  bar.className= 'toolbarButtonOverSmall';
  window.status= hint;
  return true;
}

function toolbarOutSmall(bar) {
  if (!bar) return false;
  bar.className= 'toolbarButtonSmall';
  window.status= '';
  return true;
}

function toolbarDownSmall(bar) {
  bar.className= 'toolbarButtonDownSmall';
}

function toolbarUpSmall(bar) {
  bar.className= 'toolbarButtonOverSmall';
}



var preEl ;
var orgBColor;
var orgTColor;
var SelectedRowID;
var SelectedRowIDs=new Array;
var SelectedRowIDsEmpty=true;

function ChangeTextColor(a_obj,a_class){
var i;
 for (i=0;i<a_obj.cells.length;i++)
 {
  a_obj.cells[i].className = a_class;
 }
}

function checkEmptyRowArr(){
   var t=true;
   for ( keyVar in SelectedRowIDs ) {
     if(typeof(SelectedRowIDs[keyVar])!='undefined'&&SelectedRowIDs[keyVar]!=0) t=false;
   }
   SelectedRowIDsEmpty=t;
}

function HighLightTR(el, ID,multiple,butt){
  if(typeof(multiple)=='undefined') multiple=0;
  if(typeof(che)=='undefined') che="";
  if(typeof(che2)=='undefined') che2="";

  if(typeof(butt)=='undefined') butt=0; //butt=0 ja kreisaa poga butt=1 ja labaa poga
  //var el = event.srcElement;
  //el = el.parentElement;
  //if(!el.cells){
  //  el = el.parentElement;
  //}
  if(multiple!=0)
  {
    if(typeof(event) != 'undefined' && event && event.ctrlKey&& butt==0)
    {
        if(SelectedRowID!=-1){
          EnableToolbarButtons(che);
          DisableToolbarButtons(che2);
          SelectedRowIDs[SelectedRowID]=SelectedRowID;
          SelectedRowID=-1;
        }
        if(typeof(SelectedRowIDs[ID])=='undefined')
        {
          SelectedRowIDs[ID]=ID;
          try{ChangeTextColor(el,'tableRowSel');}catch(e){;}
        }
        else
        {
          if(SelectedRowIDs[ID]!=0)
          {
            SelectedRowIDs[ID]=0;
            try{ChangeTextColor(el,'tableRow');}catch(e){;}
          }
          else
          {
            SelectedRowIDs[ID]=ID;
            try{ChangeTextColor(el,'tableRowSel');}catch(e){;}
          }
        }
    }
    else
    {
      if(butt==1&&SelectedRowID==-1)
      {

      }
      else
      {
        for ( keyVar in SelectedRowIDs ) {
          SelectedRowIDs[keyVar]=0;
          try{ChangeTextColor(document.getElementById('tableRow'+keyVar),'tableRow');}catch(e){;}
        }
        if(ID)
          EnableToolbarButtons(che);
        else
          DisableToolbarButtons(che);
        SelectedRowID = ID;
        if(typeof(preEl)!='undefined') {
          if (orgBColor!=undefined)
            preEl.bgColor=orgBColor;
          try{ChangeTextColor(preEl,'tableRow');}catch(e){;}
        }
        try{ChangeTextColor(el,'tableRowSel');}catch(e){;}
        preEl = el;
      }
    }
    checkEmptyRowArr();
    if(SelectedRowIDsEmpty&&SelectedRowID==-1)
    {
      DisableToolbarButtons(che);
    } else if(SelectedRowID==-1){
      EnableToolbarButtons(che);
      DisableToolbarButtons(che2);
    }
  }
  else
  {
    SelectedRowID = ID;
    if(typeof(preEl)!='undefined') {
      if (orgBColor!=undefined)
        preEl.bgColor=orgBColor;
      try{ChangeTextColor(preEl,'tableRow');}catch(e){;}
    }
    try{ChangeTextColor(el,'tableRowSel');}catch(e){;}
    preEl = el;
  }
}
