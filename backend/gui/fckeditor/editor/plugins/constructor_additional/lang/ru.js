﻿
FCKLang['CMSAddComponentTip']			= 'Добавить / редактировать компонент';
FCKLang['CMSdlgAddComponentTitle']			= 'Добавить / редактировать компонент';
FCKLang['CMSdlgTabInfo']			= 'Основные параметры';
FCKLang['CMSdlgComponentType']			= 'Тип компонента';
FCKLang['CMSdlgComponentWidth']			= 'Ширина';

FCKLang['CMSdlgComponentAlign']			= 'Выровнять';
FCKLang['CMSdlgComponentAlignLeft']			= 'Слева';
FCKLang['CMSdlgComponentAlignCenter']			= 'Центр';
FCKLang['CMSdlgComponentAlignRight']			= 'Право';

FCKLang['CMSdlgComponentActions']			= 'Действия';
FCKLang['CMSdlgComponentActionsPaste']			= 'Вставить';
FCKLang['CMSdlgComponentActionsCut']			= 'Сократить';
FCKLang['CMSdlgComponentActionsCopy']			= 'Копировать';

FCKLang['CMSMenuCutComponent']			= 'Сократить компонент';
FCKLang['CMSMenuCopyComponent']			= 'Копировать компонент';
FCKLang['CMSMenuPasteComponent']			= 'Вставить компонент';
FCKLang['CMSMenuEditComponent']			= 'Изменить компонента';

FCKLang['CMSComponentType_dynamic_gallery']			= 'Галерея';
FCKLang['CMSComponentType_dynamic_form']			= 'Форма';

