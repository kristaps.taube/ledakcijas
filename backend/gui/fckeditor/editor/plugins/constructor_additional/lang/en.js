﻿
FCKLang['CMSAddComponentTip']			= 'Add/edit component';
FCKLang['CMSdlgAddComponentTitle']			= 'Add/edit component';

FCKLang['CMSdlgTabInfo']			= 'Basic options';
FCKLang['CMSdlgComponentType']			= 'Component type';
FCKLang['CMSdlgComponentWidth']			= 'Width';

FCKLang['CMSdlgComponentAlign']			= 'Align';
FCKLang['CMSdlgComponentAlignLeft']			= 'Left';
FCKLang['CMSdlgComponentAlignCenter']			= 'Center';
FCKLang['CMSdlgComponentAlignRight']			= 'Right';

FCKLang['CMSdlgComponentActions']			= 'Actions';
FCKLang['CMSdlgComponentActionsPaste']			= 'Paste';
FCKLang['CMSdlgComponentActionsCut']			= 'Cut';
FCKLang['CMSdlgComponentActionsCopy']			= 'Copy';

FCKLang['CMSMenuCutComponent']			= 'Cut component';
FCKLang['CMSMenuCopyComponent']			= 'Copy component';
FCKLang['CMSMenuPasteComponent']			= 'Paste component';
FCKLang['CMSMenuEditComponent']			= 'Edit component';

FCKLang['CMSComponentType_dynamic_gallery']			= 'Gallery';
FCKLang['CMSComponentType_dynamic_form']			= 'Form';

