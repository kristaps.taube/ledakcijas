﻿
FCKLang['CMSAddComponentTip']			= 'Ievietot/labot komponenti';
FCKLang['CMSdlgTabInfo']			= 'Pamatiespējas';
FCKLang['CMSdlgAddComponentTitle']			= 'Ievietot/labot komponenti';
FCKLang['CMSdlgComponentType']			= 'Komponenta veids';
FCKLang['CMSdlgComponentWidth']			= 'Platums';

FCKLang['CMSdlgComponentAlign']			= 'Kārtojums';
FCKLang['CMSdlgComponentAlignLeft']			= 'Pa kreisi';
FCKLang['CMSdlgComponentAlignCenter']			= 'Centrā';
FCKLang['CMSdlgComponentAlignRight']			= 'Pa labi';

FCKLang['CMSdlgComponentActions']			= 'Darbības';
FCKLang['CMSdlgComponentActionsPaste']			= 'Ielīmēt';
FCKLang['CMSdlgComponentActionsCut']			= 'Izgriezt';
FCKLang['CMSdlgComponentActionsCopy']			= 'Kopēt';

FCKLang['CMSMenuCutComponent']			= 'Izgriezt komponenti';
FCKLang['CMSMenuCopyComponent']			= 'Kopēt komponenti';
FCKLang['CMSMenuPasteComponent']			= 'Ielīmēt komponenti';
FCKLang['CMSMenuEditComponent']			= 'Labot komponenti';

FCKLang['CMSComponentType_dynamic_gallery']			= 'Galerija';
FCKLang['CMSComponentType_dynamic_form']			= 'Forma';

