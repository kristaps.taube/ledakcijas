﻿

var onready = [];

onready.push(function()
{
  includeJs('/cms/backend/gui/jquery.cookie.js');

})

function jqueryLoaded()
{
  $(document).ready(function(){
    for (var i=0; i<onready.length; i++)
    {
      onready[i]();
    }
  });
}

function includeJs(src)
{
  var headID = document.getElementsByTagName("head")[0];
  var newScript = document.createElement('script');
  newScript.type = 'text/javascript';
//    newScript.onload = jqueryLoaded;
  newScript.src = src;
  headID.appendChild(newScript);
}

if (window['jQuery'])
  jqueryLoaded();
else
{
  includeJs('/cms/backend/gui/jquery.min.js');
  tryReady(0);
}


function tryReady (time_elapsed) {
  // Continually polls to see if jQuery is loaded.
  if (typeof $ == "undefined") { // if jQuery isn't loaded yet...
    if (time_elapsed <= 5000) { // and we havn't given up trying...
      setTimeout("tryReady(" + (time_elapsed + 200) + ")", 200); // set a timer to check again in 200 ms.
    } else {
//      alert("Timed out while loading jQuery.")
    }
  } else {
    jqueryLoaded();
  }
}


FCKToolbarItems.RegisterItem( 'Save'	, new FCKToolbarButton( 'Save'		, FCKLang.Save, null, FCK_TOOLBARITEM_ICONTEXT, true, null, 3 ) ) ;


var AddComponentCommand = new FCKDialogCommand( FCKLang['CMSdlgAddComponentTitle'], FCKLang['CMSdlgAddComponentTitle'], FCKConfig.PluginsPath + 'constructor_additional/add_component.html', 450, 250 );

FCKCommands.RegisterCommand( 'AddComponent', AddComponentCommand) ;

var oAddComponent		= new FCKToolbarButton( 'AddComponent', FCKLang['CMSAddComponentTip'] ) ;
oAddComponent.IconPath	= '/cms/backend/gui/tbimages/addcomp.gif';

FCKToolbarItems.RegisterItem( 'AddComponent', oAddComponent ) ;

function OnDoubleClick ( span )
{
  if ( span.tagName == 'IMG' && span.getAttribute('_fckcomponent') != null )
    AddComponentCommand.Execute() ;

}
FCK.RegisterDoubleClickHandler( OnDoubleClick, 'IMG' ) ;

FCKDocumentProcessor.AppendNew().ProcessDocument = function( document )
{
	var aComponents = document.getElementsByTagName('COMPONENT') ;

	var oComponent ;
	var i = aComponents.length - 1 ;
	while ( i >= 0 && ( oComponent = aComponents[i--] ) )
	{
  	var oImg = FCKDocumentProcessor_CreateFakeImage( 'FCK__'+oComponent.getAttribute('type'), oComponent.cloneNode(true) ) ;

    $(oImg).attr('_fckcomponent', 1);

  	oComponent.parentNode.insertBefore( oImg, oComponent ) ;
  	oComponent.parentNode.removeChild( oComponent ) ;
  }
}

var CMS = {};

CMS.SelectNode = function( node )
{
  FCK.Focus() ;
  FCK.EditorDocument.selection.empty() ;
  var oRange ;
  try
  {
  // Try to select the node as a control.
  oRange = FCK.EditorDocument.body.createControlRange() ;
  oRange.addElement( node ) ;
  }
  catch(e)
  {
  // If failed, select it as a text range.
  //	 oRange = FCK.EditorDocument.selection.createRange() ;
  oRange = FCK.EditorDocument.body.createTextRange();
  oRange.moveToElementText( node ) ;
  }

  oRange.select() ;
}

CMS.AjaxCall = function(action, params, postvars, callback, type)
{
  var cmsp = FCKConfig.CMSParams;

  $.post(cmsp.web_root+'?module=dynamiccomponents&action='+action+'&site_id='+cmsp.site_id+'&page_id='+cmsp.page_id+'&'+params, postvars, callback, type);

}

CMS.PasteComponent = function(on_load)
{
  var post = {
    action: $.cookie('cms_clipboard_action', undefined, { path: '/' }),
    id: $.cookie('cms_clipboard_component_id', undefined, { path: '/' }),
    holder: FCKConfig.CMSParams.component_name,
    component_page_id: FCKConfig.CMSParams.component_page_id
  }

  CMS.AjaxCall('paste', '', post, function(data)
  {
    try {
      var values = $.parseJSON(data);

      var id = parseInt(values.id);
      if (!(id > 0)) return;
    }
    catch (e)
    {
      alert("Paste error: "+e+"\nData: "+data);

      if (on_load)
        on_load(false);

      return;
    }

    CMS.InsertComponentImage(id, values.type, values.width, values.align);

    if (on_load)
      on_load(true, values);

  });

}

CMS.CutComponent = function(oFakeImage, oComponent, on_load)
{
  if (oFakeImage == null || oComponent == null) return;

  $.cookie('cms_clipboard_action', 'cut', { expires: 30.5, path: '/' });
  $.cookie('cms_clipboard_component_id', $(oComponent).attr('id'), { expires: 30.5, path: '/' });

  var post = {
    id: $.cookie('cms_clipboard_component_id', undefined, { path: '/' })
  };

  CMS.AjaxCall('cut', '', post, function(data)
  {
    if (data != '1')
    {
      alert("Cut error. Data:\n"+data)
      return;
    }

    FCKUndo.SaveUndoStep() ;

    $(oFakeImage).remove();
    $(oComponent).remove();

    if (on_load)
      on_load();

  })

}

CMS.CopyComponent = function(oComponent)
{
  $.cookie('cms_clipboard_action', 'copy', { expires: 30.5, path: '/' });
  $.cookie('cms_clipboard_component_id', $(oComponent).attr('id'), { expires: 30.5, path: '/' });
}


CMS.InsertComponentImage = function(id, type, width, align)
{
  FCKUndo.SaveUndoStep() ;

  var oFakeImage = null;
  var oComponent = false;

  if (!oComponent)
	{
		oComponent		= FCK.EditorDocument.createElement( 'COMPONENT' ) ;
		oFakeImage  = null ;
	}

  $(oComponent).attr('type', type);
  $(oComponent).attr('id', id);
  $(oComponent).attr('width', width);
  $(oComponent).attr('align', align);

  fake_class = 'FCK__' + type;

	if (!oFakeImage)
	{
		oFakeImage	= FCKDocumentProcessor_CreateFakeImage(fake_class, oComponent) ;
//      oFakeImage.setAttribute('style', 'display:block');

/*
    var br = document.createElement('BR');
    br.setAttribute( '_fckfakelement', 'true', 0 ) ;
    FCK.InsertElement( br );
*/
    $(oFakeImage).attr('_fckcomponent', 1);

		oFakeImage	= FCK.InsertElement( oFakeImage ) ;
    FCK.InsertElement( oFakeImage );

/*
    var br = document.createElement('BR');
    br.setAttribute( '_fckfakelement', 'true', 0 ) ;
    FCK.InsertElement( br );
  	oFakeImage.parentNode.insertBefore( br, oFakeImage.nextSibling ) ;
*/


	}
  else
    oFakeImage.className = fake_class;

  return oFakeImage;
}


function PasteComponentCommand(name)
{
  this.Name = name;
}
PasteComponentCommand.prototype.Execute = function(itemText, itemLabel)
{
  CMS.PasteComponent();
}
PasteComponentCommand.prototype.GetState = function()
{
	return;
}
FCKCommands.RegisterCommand('PasteComponent', new PasteComponentCommand()) ;


function CutComponentCommand(name)
{
  this.Name = name;
}
CutComponentCommand.prototype.Execute = function(itemText, itemLabel)
{
  var oFakeImage = FCK.Selection.GetSelectedElement() ;
  var oComponent ;

  if (oFakeImage)
    oComponent = FCK.GetRealElement(oFakeImage);

  CMS.CutComponent(oFakeImage, oComponent);
}
CutComponentCommand.prototype.GetState = function()
{
	return;
}
FCKCommands.RegisterCommand('CutComponent', new CutComponentCommand()) ;


function CopyComponentCommand(name)
{
  this.Name = name;
}
CopyComponentCommand.prototype.Execute = function(itemText, itemLabel)
{
  var oFakeImage = FCK.Selection.GetSelectedElement() ;
  var oComponent ;

  if (oFakeImage)
    oComponent = FCK.GetRealElement(oFakeImage);

  CMS.CopyComponent(oComponent);
}
CopyComponentCommand.prototype.GetState = function()
{
	return;
}
FCKCommands.RegisterCommand('CopyComponent', new CopyComponentCommand()) ;




FCK.ContextMenu.RegisterListener({
  AddItems : function( menu, tag, tagName )
  {
    menu.AddSeparator() ;

    if ( tagName == 'IMG' && tag.getAttribute('_fckcomponent') == 1 )
    {
      menu.AddItem( 'AddComponent', FCKLang.CMSMenuEditComponent, oAddComponent.IconPath);

      var disabled = !($.cookie('cms_clipboard_component_id', undefined, { path: '/' }) > 0);
      menu.AddItem( 'PasteComponent', FCKLang.CMSMenuPasteComponent, 9,  disabled) ;

      menu.AddItem( 'CutComponent', FCKLang.CMSMenuCutComponent, 7);
      menu.AddItem( 'CopyComponent', FCKLang.CMSMenuCopyComponent, 8);
    }
    else
    {
      var disabled = !($.cookie('cms_clipboard_component_id', undefined, { path: '/' }) > 0);
      menu.AddItem( 'PasteComponent', FCKLang.CMSMenuPasteComponent, 9,  disabled) ;
    }
  }
});

CMS.onInsertComponent = function(id, oFakeImage)
{
  FCKSelection.SelectNode(oFakeImage);
  FCK.Commands.GetCommand('AddComponent').Execute();
}

CMS.insertComponent = function(values, on_load)
{
  values.component_page_id = FCKConfig.CMSParams.component_page_id;

  CMS.AjaxCall('insert', '', values, function(data)
  {
    $('#ajaxload').toggle(false);

    try {
      var obj = $.parseJSON(data);

      var id = parseInt(obj.id);
      if (!(id > 0)) throw 'invalid id';
    }
    catch (e)
    {
      alert("Insert error: "+e+"\nData: "+data);
      dialog.CloseDialog();
      return;
    }

    oFakeImage = CMS.InsertComponentImage(id, values.type, values.width, values.align);

    if (on_load)
      on_load(id, oFakeImage);

  });

}

CMS.updateComponent = function(values, oComponent, on_load)
{
  values.id = oComponent.getAttribute('id');
  values.component_page_id = FCKConfig.CMSParams.component_page_id;

  CMS.AjaxCall('update', '', values, function(data)
  {
    $('#ajaxload').toggle(false);
    if (data != '1')
    {
      alert('Update error! Data: '+data);
      dialog.CloseDialog();
      return;
    }

    $(oComponent).attr('type', values.type);
    $(oComponent).attr('id', values.id);
    $(oComponent).attr('width', values.width);
    $(oComponent).attr('align', values.align);

    if (on_load)
      on_load();

  });

}
