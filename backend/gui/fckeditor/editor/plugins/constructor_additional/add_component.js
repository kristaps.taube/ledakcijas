
var dialog		= window.parent ;
var oEditor		= dialog.InnerDialogLoaded() ;
var FCK			= oEditor.FCK ;
var FCKLang		= oEditor.FCKLang ;
var FCKConfig	= oEditor.FCKConfig ;
var FCKTools	= oEditor.FCKTools ;
var FCKBrowserInfo = oEditor.FCKBrowserInfo;
var FCKDomTools = oEditor.FCKDomTools;
var CMS = oEditor.CMS;

var oFakeImage = FCK.Selection.GetSelectedElement() ;
var oComponent ;

if (oFakeImage)
  oComponent = FCK.GetRealElement(oFakeImage);

dialog.AddTab( 'Info', FCKLang.CMSdlgTabInfo ) ;

if (oComponent)
{
  dialog.AddTab( 'Design', FCKLang['CMSComponentType_'+oComponent.getAttribute('type')]) ;
}


function OnDialogTabChange( tabCode )
{
	ShowE('divInfo'		, ( tabCode == 'Info' ) ) ;
	ShowE('divDesign'		, ( tabCode == 'Design' ) ) ;
}

function FCKResizeDialog(iframe, width, height)
{
  var topWindow = parent.parent;
  var viewSize = FCKTools.GetViewPaneSize( topWindow ) ;
	var scrollPosition = { 'X' : 0, 'Y' : 0 } ;
	var useAbsolutePosition = FCKBrowserInfo.IsIE && ( !FCKBrowserInfo.IsIE7 || !FCKTools.IsStrictMode( topWindow.document ) ) ;
	if ( useAbsolutePosition )
		scrollPosition = FCKTools.GetScrollPosition( topWindow ) ;
	var iTop  = Math.max( scrollPosition.Y + ( viewSize.Height - height - 20 ) / 2, 0 ) ;
	var iLeft = Math.max( scrollPosition.X + ( viewSize.Width - width - 20 )  / 2, 0 ) ;

	FCKDomTools.SetElementStyles( iframe,
			{
				'top'		: iTop + 'px',
				'left'		: iLeft + 'px',
				'width'		: width + 'px',
				'height'	: height + 'px'
			} ) ;

}


window.onload = function()
{

	oEditor.FCKLanguageManager.TranslatePage(document) ;

	dialog.SetAutoSize( true ) ;
	dialog.SetOkButton( true ) ;

  var type_select = GetE('type');

  for (var type in FCKConfig.CMSComponents)
  {
//    var config = FCKConfig.CMSComponents[type];
    var opt = new Option(FCKLang['CMSComponentType_'+type], type);
    type_select.options[type_select.options.length] = opt;

    if (oComponent && oComponent.getAttribute('type') == type)
      opt.selected = true;

  }



	SelectField( 'type' ) ;

  var form = $('#divInfo > form')[0];
  var align = '';

  if (oComponent)
  {
    $("#actions .paste").attr('disabled', true);

    var config = FCKConfig.CMSComponents[oComponent.getAttribute('type')];
    if (config.dialog)
    {
      var width = config.dialog.width > 0 ? config.dialog.width : 450;
      var height = config.dialog.height > 0 ? config.dialog.height : 250;

//      dialog.Sizer.ResizeDialog(width, height);

      var iframes = parent.parent.document.getElementsByTagName('iframe');
      var iframe = iframes[iframes.length-1];
      FCKResizeDialog(iframe, width, height);

    }

    loadComponent(oComponent.getAttribute('id'));

    $('#actions .copy').css('display', '');
    $('#actions .cut').css('display', '');

    $('#actions .copy').click(function()
    {
      CMS.CopyComponent(oComponent);

      updateClipboardButtons();
      dialog.CloseDialog();

      return false;
    });

    $('#actions .cut').click(function()
    {
      CMS.CutComponent(oFakeImage, oComponent, function()
      {
        updateClipboardButtons(true);
        dialog.CloseDialog();
      });

      return false;
    });

    dialog.SetSelectedTab('Design');

    form.width.value = oComponent.getAttribute('width');
    align = oComponent.getAttribute('align');
  }
  else
    $("#actions .paste").attr('disabled', false);


  $('#actions .paste').click(function()
  {
    $('#ajaxload').toggle(true);

    CMS.PasteComponent(function(success, values)
    {
      $('#ajaxload').toggle(false);
      dialog.CloseDialog();
    });

    return false;
  });

  var opts = '<option value=""'+(align == '' ? ' selected="selected"' : '')+'></option>'+
    '<option value="left"'+(align == 'left' ? ' selected="selected"' : '')+'>'+FCKLang.CMSdlgComponentAlignLeft+'</option>'+
    '<option value="right"'+(align == 'right' ? ' selected="selected"' : '')+'>'+FCKLang.CMSdlgComponentAlignRight+'</option>';

  $(form.align).html(opts);

  form.holder.value = FCKConfig.CMSParams.component_name;

  updateClipboardButtons();
}

function updateClipboardButtons(cut)
{
  var action = $.cookie('cms_clipboard_action', undefined, { path: '/' });
  var id = $.cookie('cms_clipboard_component_id', undefined, { path: '/' });
  var active = (action != undefined && id > 0);

//  $("#actions .paste").attr('disabled', !active);
  $('#actions .copy').attr('disabled', cut);
  $('#actions .cut').attr('disabled', cut);
}

var ok_clicked = false;

function Ok()
{
  if (ok_clicked) return;
  ok_clicked = true;

  $('#ajaxload').toggle(true);


  var values = getFormValues('#divInfo > form');

  if (!oComponent)
  {
    CMS.insertComponent(values, CMS.onInsertComponent)
    return true;
  }

  CMS.updateComponent(values, oComponent);

  return true;
}

function getFormValues(formsel)
{
  var values = {};
  var a = $(formsel).serializeArray();
  for (var k in a)
  {
    if (!a[k]['name']) continue;
    values[a[k]['name']] = a[k]['value'];
  }

  return values;
}

function loadComponent(id)
{
  CMS.AjaxCall('load', 'id='+id, {}, function(data)
  {
    $('#divDesign').html(data);
  });
}

