﻿/*
 * == BEGIN LICENSE ==
 *
 * Licensed under the terms of any of the following licenses at your
 * choice:
 *
 *  - GNU General Public License Version 2 or later (the "GPL")
 *    http://www.gnu.org/licenses/gpl.html
 *
 *  - GNU Lesser General Public License Version 2.1 or later (the "LGPL")
 *    http://www.gnu.org/licenses/lgpl.html
 *
 *  - Mozilla Public License Version 1.1 or later (the "MPL")
 *    http://www.mozilla.org/MPL/MPL-1.1.html
 *
 * == END LICENSE ==
 *
 * Google Maps for FCKeditor, Latvian language file.
 */
FCKLang.DlgGMapsTitle = 'Google Karte' ;
FCKLang.GMapsBtn = 'Google Karte' ;
FCKLang.GMapsBtnTooltip	= 'Ievietot/Labot Google Karti' ;
FCKLang.GMapsMap			= 'Karte' ;
FCKLang.GMapsZoomLevel	= 'Attālums' ;
FCKLang.txtLatitude	  = 'Platums' ;
FCKLang.txtLongitude  = 'Garums' ;
FCKLang.GMapsMarker		= 'Iezīme' ;
FCKLang.GMapsSearchLabel	= 'Vieta:' ;
FCKLang.GMapsSearch		= 'Meklēt' ;
FCKLang.GMapsNotFound = '%s netika atrasts.' ;
FCKLang.GMapsMarkerText	= 'Teksts' ;
FCKLang.GMapsMarkerDefaultText	= 'Raksti pats savu tekstu' ;
FCKLang.GMapsLine = 'Līnija' ;
FCKLang.GMapsLineInstructions = 'Lai veidotu līniju, atzīmē secīgi punktu pēc punkta.' ;
FCKLang.GMapsHelpFile	= 'install.html' ;
FCKLang.GMapsUserHelpFile = 'users.html' ;
FCKLang.Help = 'Palīdzība' ;
FCKLang.GMapsClickToAddMarker = 'Spied uz kartes, lai pievienotu iezīmi' ;
FCKLang.GMapsDeleteMarker = 'Izdzēst iezīmi' ;
FCKLang.GMapsAddMarker = 'Pievienot iezīmi' ;
FCKLang.GMaps_MissingKey = 'Jāievada Google Kartes atslēga, lai lietotu Google Karti.' ;