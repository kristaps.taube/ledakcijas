﻿/*
 * == BEGIN LICENSE ==
 *
 * Licensed under the terms of any of the following licenses at your
 * choice:
 *
 *  - GNU General Public License Version 2 or later (the "GPL")
 *    http://www.gnu.org/licenses/gpl.html
 *
 *  - GNU Lesser General Public License Version 2.1 or later (the "LGPL")
 *    http://www.gnu.org/licenses/lgpl.html
 *
 *  - Mozilla Public License Version 1.1 or later (the "MPL")
 *    http://www.mozilla.org/MPL/MPL-1.1.html
 *
 * == END LICENSE ==
 *
 * Google Maps for FCKeditor, English language file.
 */

FCKLang.DlgGMapsTitle = 'Google Карта' ;
FCKLang.GMapsBtn		  = 'Google Карта' ;
FCKLang.GMapsBtnTooltip	= 'Вставить/Редактировать' ;
FCKLang.GMapsMap			= 'Карта' ;
FCKLang.GMapsZoomLevel	= 'Расстояние' ;
FCKLang.txtLatitude	  = 'Долгота' ;
FCKLang.txtLongitude  = 'Широта' ;
FCKLang.GMapsMarker		= 'Маркер' ;
FCKLang.GMapsSearchLabel	= 'Место:' ;
FCKLang.GMapsSearch		= 'Поиск' ;
FCKLang.GMapsNotFound = '%s не был найден.' ;
FCKLang.GMapsMarkerText	= 'Текст' ;
FCKLang.GMapsMarkerDefaultText	= 'Ваш текст' ;
FCKLang.GMapsLine = 'Линия' ;
FCKLang.GMapsLineInstructions = 'Нажмите снова и снова, чтобы сделать линию.' ;

FCKLang.GMapsHelpFile	= 'install.html' ;
FCKLang.GMapsUserHelpFile = 'users.html' ;
FCKLang.Help = 'Помощь' ;

FCKLang.GMapsClickToAddMarker = 'Нажмите на карту, чтобы добавить маркер' ;
FCKLang.GMapsDeleteMarker = 'Удалить маркер' ;
FCKLang.GMapsAddMarker = 'Добавить маркер' ;

FCKLang.GMaps_MissingKey = 'Необходимость Google API ключ' ;