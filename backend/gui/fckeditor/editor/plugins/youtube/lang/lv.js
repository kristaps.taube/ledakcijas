﻿/*
 * For FCKeditor 2.3
 * 
 * 
 * File Name: en.js
 * 	English language file for the youtube plugin.
 * 
 * File Authors:
 * 		Uprush (uprushworld@yahoo.co.jp) 2007/10/30
 */

FCKLang['YouTubeTip']			= 'Ievietot/Labot Youtube video' ;
FCKLang['DlgYouTubeTitle']		= 'YouTube uzstādījumi' ;
FCKLang['DlgYouTubeCode']		= '"Lūdzu ievadiet vēlamā YouTube video adresi."' ;
FCKLang['DlgYouTubeSecurity']	= 'Kļūdaina adrese.' ;
FCKLang['DlgYouTubeURL']	    = 'Adrese' ;
FCKLang['DlgYouTubeWidth']	    = 'Platums' ;
FCKLang['DlgYouTubeHeight']	    = 'Augstums' ;
FCKLang['DlgYouTubeQuality']    = 'Kvalitāte' ;
FCKLang['DlgYouTubeLow']	    = 'Zema' ;
FCKLang['DlgYouTubeHigh']	    = 'Augsta (ja iespējams)' ;
