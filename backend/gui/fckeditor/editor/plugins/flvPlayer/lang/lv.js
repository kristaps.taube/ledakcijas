﻿// FLV Player Dialog
FCKLang["DlgFLVPlayerTitle"]          = "Ievietot/labot FLV Atskaņotāju";
FCKLang["DlgFLVPlayerURL"]            = "Video/Audio fails" ;
FCKLang["DlgFLVPlayerBtnBrowse"]      = "Meklēt..." ;
FCKLang["DlgFLVPlayerImgURL"]         = "Video pirmsskats" ;
FCKLang["DlgFLVPlayerWidth"]          = "Platums (px)" ;
FCKLang["DlgFLVPlayerHeight"]         = "Augstums (px)" ;
FCKLang["DlgFLVPlayerAlign"]          = "Novietojums" ;
FCKLang["DlgFLVPlayerAlignLeft"]      = "Kreisajā pusē" ;
FCKLang["DlgFLVPlayerAlignRight"]     = "Labajā pusē" ;
FCKLang["DlgFLVPlayerAlignCenter"]    = "Vidū" ;
FCKLang["DlgFLVPlayerHSpace"]         = "Horizontālā telpa" ;
FCKLang["DlgFLVPlayerVSpace"]         = "Vertikālā telpa" ;
FCKLang["DlgFLVPlayerBgColor"]        = "Fona krāsa"
FCKLang["DlgFLVPlayerTBColor"]        = "Rīkjoslas krāsa"
FCKLang["DlgFLVPlayerTBTColor"]       = "Teksta krāsa"
FCKLang["DlgFLVPlayerTBTRColor"]      = "Aktīvā teksta krāsa"
FCKLang["DlgFLVPlayerBtnSelect"]      = "Izvēlēties..."
FCKLang["DlgFLVPlayerAutoplay"]       = "Automātiska atskaņošana" ;
FCKLang["DlgFLVPlayerLoop"]           = "Spēlēt cikliski" ;
FCKLang["DlgFLVPlayerDownload"]       = "Lejupielādējams" ;
FCKLang["DlgFLVPlayerFullscreen"]     = "Atļaut pilnekrānu" ;
FCKLang["DlgFLVPlayerAlertUrl"]       = "Lūdzu ievadiet adresi"
FCKLang["DlgFLVPlayerAlertPlaylist"]  = "Lūdzu ievadiet adresi"
FCKLang["DlgFLVPlayerAlertWidth"]     = "Lūdzu ievadiet platumu"
FCKLang["DlgFLVPlayerAlertHeight"]    = "Lūdzu ievadiet augstumu"
FCKLang["DlgFLVPlayerPlayerAttrs"]    = "Atskaņotāja rekvizīti"
FCKLang["DlgFLVPlayerMovieAttrs"]     = "Video/Audio rekvizīti"
FCKLang["DlgFLVPlayerShowNavigation"] = "Rādīt navigāciju"
FCKLang["DlgFLVPlayerShowDigits"]     = "Rādīt laiku"
FCKLang["DlgFLVPlayerWatermarkURL"]   = "Izbalināt bildi"
FCKLang["DlgFLVPlayerFileType"]		  = "Faila tips"
FCKLang["DlgFLVPlayerSingleFile"]     = "Video fails"
FCKLang["DlgFLVPlayerPlaylistFile"]   = "Playliste"
FCKLang["DlgFLVPlayerPlaylistURL"]    = "Playlistes adrese"
FCKLang["DlgFLVPlayerDispPlaylist"]   = "Rādīt playlisti"
FCKLang["DlgFLVPlayerDispPLNone"]     = "Nerādīt"
FCKLang["DlgFLVPlayerDispPLRight"]    = "Rādīt labajā pusē"
FCKLang["DlgFLVPlayerDispPLBelow"]    = "Rādīt apakšā"
FCKLang["DlgFLVPlayerRecommendURL"]   = "Ieteikumu saraksts"
FCKLang["DlgFLVPlaylistWidth"]        = "Playlistes platums (px)"
FCKLang["DlgFLVPlaylistHeight"]       = "Playlistes augstums (px)"
FCKLang["DlgFLVPlaylistDimText"]      = "Playlistes izmērs"
FCKLang["DlgFLVPlayerPLThumbs"]       = "Rādīt sīktēlus playlistē"

