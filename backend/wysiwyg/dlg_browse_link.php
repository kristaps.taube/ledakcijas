<?

require($GLOBALS['cfgDirRoot']."config.php");

// Utilities

function GetCategoryTree($id, $indent) {
  global $CMS_SESSION;
  $data = sqlQueryData("SELECT id, CONCAT('$indent', title) as title FROM categories WHERE parent_id = '".$id."' AND id <> '".$rid."' AND language_id = '".$CMS_SESSION['LANGUAGE']."' ORDER BY ind");
  foreach($data as $value) {
    $result[] = $value;
    if ($get = GetCategoryTree($value['id'], $indent.'&nbsp;&nbsp;&nbsp;')) $result = array_merge($result, $get);
  }
  return $result;
}

// generate selector
require($GLOBALS['cfgDirRoot']."library/"."class.propertylist.php");
$propertyControl = new propertyControl();
$propertyControl->property['type'] = 'action_list';
$propertyControl->property['action'] = 'document.LinkPick.location = this.options[this.selectedIndex].value';
$lookup = GetCategoryTree(0, '');
$propertyControl->property['lookup'][] = '../_pages.php:'.$strSelector;
foreach($lookup as $row) $propertyControl->property['lookup'][] = '../_pages.php?cat='.$row['id'].':'.$row['title'];
$propertyControl->name = 'select';
$category_select = $propertyControl->output();


?>

<html>
<head>
<title>Insert Link</title>
<link rel=stylesheet href="../dialog.css" type="text/css">
<? global $CMS_SESSION; ?>
<meta http-equiv="Content-Type" content="text/html; charset=<?=$CMS_SESSION['encoding'];?>">
<script language=javascript for=window event=onload>
<!--
  for ( elem in window.dialogArguments )
  {
    switch( elem )
    {
    case "LinkUrl":
      document.Form.LinkUrl.value=window.dialogArguments["LinkUrl"];
      break;
    case "LinkTarget":
      document.Form.LinkTarget.value=window.dialogArguments["LinkTarget"];
      break;
    }
  }
// -->
</script>

<script language=javascript for=ok event=onclick>
<!--
  var arr = new Array();
  if (document.Form.LinkUrl.value ==''){
    alert('You did not select a link. Existing link will be retained.');
  }
  arr["LinkUrl"] =  document.Form.LinkUrl.value;
  if (document.Form.LinkTarget.selectedIndex!=-1)
    arr["LinkTarget"] = document.Form.LinkTarget[document.Form.LinkTarget.selectedIndex].value;
  else
    arr["LinkTarget"] = "";
  window.returnValue = arr;
  window.close();
// -->
</script>
</head>
<body>

<form name="Form">
  <table cellspacing="10">
    <tr>
      <td valign="middle" align="left" colspan="2" nowrap>
        Category: <?=$category_select;?>
      </td>
    </tr>
    <tr>
      <td valign="top" align="left" colspan="2"><iframe width="400" height="240" name="LinkPick" src="../_pages.php" style="z-index:1" scrolling="yes"></iframe></td>
    </tr>
    <tr>
      <td valign="middle" align="left" colspan="2" nowrap>
      URL: <input name="LinkUrl" type="text" size="35">
      &nbsp;{%langstr:target%}: <select name="LinkTarget">
      <option value="_self">_self</option>
      <option value="_top">_top</option>
      <option value="_blank">_blank</option>
      </select> </td>
    </tr>
    <tr>
      <td valign="middle" align="left" nowrap></td>
      <td valign="middle" align="left" nowrap></td>
    </tr>
    <tr>
      <td valign="top" align="left"><input type="button" id="files" value="&nbsp;Files&nbsp;" OnClick="document.LinkPick.location='../_files.php';"></td>
      <td valign="top" align="right"><input type="button" id="ok" value="&nbsp;&nbsp;&nbsp;OK&nbsp;&nbsp;&nbsp;">&nbsp;<input type="button" onclick="window.close();" value="Cancel"></td>
    </tr>
  </table>
</form>

</body>
</html>
