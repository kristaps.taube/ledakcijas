<?

die;

require('../../config.php');
require($GLOBALS['cfgDirRoot'].'library/class.excel_xml.php');

if ($_GET['export'] == 1)
{
  mysql_query("SET NAMES 'utf-8'");

  $xc = new Excel_XML();
  $pnames = Array();
  $data = sqlQueryDataAssoc('SELECT phrasename FROM phrases WHERE lang="'.db_escape($_GET['lang']).'" AND phrase=""');
  foreach ($data as $row)
  {
    $pnames[] = $row['phrasename'];
  }

  if ($pnames)
  {
    $data = sqlQueryDataAssoc('SELECT p.*, pn.name FROM phrases AS p LEFT JOIN phrasenames AS pn ON p.phrasename=pn.phrasename_id WHERE phrasename IN ('.implode(',', $pnames).')');
    $xc->addArray(Array(Array('phrasename', 'lang', 'phrase')));
    foreach ($data as $row)
    {
      $xc->addArray(Array(Array(
        html_entity_decode($row['phrasename'], ENT_NOQUOTES, 'UTF-8'),
        html_entity_decode($row['lang'], ENT_NOQUOTES, 'UTF-8'),
        html_entity_decode($row['phrase'], ENT_NOQUOTES, 'UTF-8')
      )));
    }
  }

  header("Content-Type: application/octet-stream");
  header("Content-Disposition: inline; filename=\"phrases.xml\"");
//  header("Content-Type: application/xml");
  echo $xc->generateXML();
  die;
}

if ($_GET['import'] == 1 && isset($_FILES['file']))
{
  $filename = $_FILES['file']['tmp_name'];
  require_once($GLOBALS['cfgDirRoot'].'library/Excel/reader.php');

  if (!is_readable($filename)) die("can't read file");

  $xl = new Spreadsheet_Excel_Reader();
  $xl->setOutputEncoding('utf-8');
  $xl->setUTFEncoder();

  $xl->read($filename);

  $data = Array();
  for ($i=0; $i<count($xl->sheets); $i++)
  {
    $sheet = $xl->sheets[$i];

    for ($y=1; $y<=$sheet['numRows']; $y++)
    {
      $row = Array();
      for ($x=1; $x<=$sheet['numCols']; $x++)
      {
        $row[] = $sheet['cells'][$y][$x];
      }
      $data[] = $row;
      $n++;
    }
  }
// mysql_query("SET NAMES 'latin1'");
header('Content-type: text/html; charset=utf-8');
  $n = 0;
  for ($i=1; $i<count($data); $i++)
  {
    $row = $data[$i];
    $lang = trim($row[1]);
    if ($lang == 'Russian')
    {
      echo mb_convert_encoding($row[2], 'HTML-ENTITIES', 'UTF-8').'<br/>';
      sqlQuery('UPDATE phrases SET phrase="'.db_escape(mb_convert_encoding($row[2], 'HTML-ENTITIES', 'UTF-8')).'" WHERE phrasename='.$row[0].' AND lang="'.$lang.'"');
      $n++;
    }
  }
  var_dump($n);
//  header('Location: ?done='.$n);
  exit();
}

if (isset($_GET['done']))
{
  echo 'Done! Imported rows: '.$_GET['done'].'<br/>';
}

?>
<h3>Export empty phrases:</h3>
<a href="?export=1&lang=Latvian" target="_blank">Export LV</a>
<a href="?export=1&lang=Russian" target="_blank">Export RU</a>
<a href="?export=1&lang=" target="_blank">Export EN</a>
<br/><br/>
<form name="form" action="?import=1" method="post" enctype="multipart/form-data">
  Import: <input type="file" name="file" onchange="document.form.submit()"/>
</form>