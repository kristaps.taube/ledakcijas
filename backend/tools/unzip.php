<?

error_reporting(E_ALL);
ini_set('display_errors', 1);

//require_once('pclzip.lib.php');

function unzipFile($zipfile, $path)
{
  exec('unzip -o '.$zipfile.' -d '.$path);
  return '';

  $zip = new PclZip($zipfile);
  $list = $zip->listContent();
  if (!$list)
  {
    return 'Not a zip archive!<br/><br/>';
  }

  $zip_dir = $path != '' ? $path : '.';
  @mkdir($zip_dir, 0755);
  @chmod($zip_dir, 0755);

  if (!is_dir($zip_dir))
    return 'Can\'t create zip directory: '.$zip_dir;

  if ($zip->extract(PCLZIP_OPT_PATH, $zip_dir, PCLZIP_OPT_REPLACE_NEWER) < 0)
  {
    return 'Can\'t unpack ZIP archive!';
  }

  return '';
}

$files = array();
foreach (glob('*.zip') as $f)
{
  $files[] = $f;
}

$result = array();
if ($_POST)
{
  for ($i=0; $i<count($files); $i++)
  {
    if ($_POST['f'.$i])
    {
      $msg = unzipFile($files[$i], $_POST['path'.$i]);
      $result[] = array('file' => basename($files[$i]), 'error' => ($msg != ''), 'msg' => $msg ? $msg : 'done!');
    }
  }
}

$files = array();
foreach (glob('*.zip') as $f)
{
  $files[] = $f;
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title>Unzip</title>
  <style type="text/css">

    table {
      width: 400px;
    }
    th {
      text-align: left;
    }

    .err {
      color: red;
    }
    .ok {
      color: green;
    }


  </style>
</head>
<body>
  <ul>
<? foreach ($result as $r) { ?>
  <li><?=$r['file'] ?> - <span class="<?=$r['error'] ? 'err' : 'ok' ?>"><?=$r['msg'] ?></span></li>
<? } ?>
  </ul>
  <form action="" method="post">
  <table>
    <tr>
      <th>File</th>
      <th>Extract to</th>
      <th> </th>
    </tr>
<? $i=0; foreach ($files as $f) { ?>
    <tr>
      <td><label><input type="checkbox" name="f<?=$i ?>"/> <?=basename($f) ?></label></td>
      <td><input type="text" name="path<?=$i ?>" value="." /></td>
    </tr>
<? $i++; } ?>
  </table>
    <br/>
    <input type="submit" value="Unzip file(s)"/>
  </form>
</body>
</html>
