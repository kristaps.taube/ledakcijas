<?

die;

require('../../config.php');

session_start();

if (!isset($_SESSION['difmatch'])) $_SESSION['difmatch'] = array();

$update_db1 = true;
$update_db2 = false;

$db1 = $GLOBALS['DBH'];

$db2 = mysql_connect('localhost', 'demo', 'demo2003', true) or die("can't connect to dbserver 2");
mysql_select_db('demo_datateks_lv_-_main1', $db2) or die("can't select database");

$phrasenames = Array();

function getPhrasenames()
{
  global $phrasenames;

  $data = sqlQueryDataAssoc('SELECT * FROM phrasenames');
  foreach ($data as $row)
  {
    if (!in_array($row['name'], $phrasenames))
      $phrasenames[] = $row['name'];
  }
}

$GLOBALS['DBH'] = $db1;
getPhrasenames();

$GLOBALS['DBH'] = $db2;
getPhrasenames();

echo 'phrasenames='.count($phrasenames).'<br/>';

$pnames = array();
foreach ($phrasenames as $name)
{
  $pnames[$name] = count($pnames) + 1;
}

$phrases = array();
$difphrases = array();

function getPhrases()
{
  global $pnames, $phrases, $difphrases;
  $data = sqlQueryDataAssoc('SELECT p.*, pn.name FROM phrases AS p LEFT JOIN phrasenames AS pn ON p.phrasename=pn.phrasename_id');
  foreach ($data as $row)
  {
    if (!isset($pnames[$row['name']])) continue;

    if (!isset($phrases[$row['name']]))
      $phrases[$row['name']] = Array('phrasename_id' => $pnames[$row['name']], 'langs' => Array() );

    if (isset($phrases[$row['name']]['langs'][$row['lang']]) && $phrases[$row['name']]['langs'][$row['lang']] != $row['phrase'])
      $difphrases[] = array('name' => $row['name'], 'lang' => $row['lang'], 'value' => $row['phrase']);
    else
      $phrases[$row['name']]['langs'][$row['lang']] = $row['phrase'];
  }
}

$GLOBALS['DBH'] = $db1;
getPhrases();


$GLOBALS['DBH'] = $db2;
getPhrases();


if ($difphrases && count($difphrases) != count($_SESSION['difmatch']))
{
  if ($_POST)
  {
    for ($i=0; $i<count($difphrases); $i++)
    {
      if ($_POST['dif'.$i]) $_SESSION['difmatch'][$i] = $_POST['dif'.$i];
    }
  }


?>
  <html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Phrases merger</title>
    <style type="text/css">

    table {
      width: 100%;
      border-collapse: collapse;
    }

    td {
      border: 1px solid black;
      padding: 2px;
      padding-left: 6px;
      padding-right: 6px;
    }


    </style>
  </head>
  <body>
    <h3>Atšķirīgās frāzes: <?=count($difphrases) ?></h3>
    <form action="" method="post">
    <table>
      <tr>
        <th>Nosaukums</th>
        <th>Valoda</th>
        <th>Frāze 1</th>
        <th>Frāze 2</th>
      </tr>
<? $i = 0; foreach ($difphrases as $dif) { if ($_SESSION['difmatch'][$i]) { $i++; continue; } ?>
      <tr>
        <td><?=$dif['name'] ?></td>
        <td><?=$dif['lang'] ? $dif['lang'] : '(default)' ?></td>
        <td><label><input type="radio" name="dif<?=$i ?>" value="1"/> <?=htmlspecialchars(mb_convert_encoding($phrases[$dif['name']]['langs'][$dif['lang']], 'UTF-8', 'HTML-ENTITIES' )) ?></label></td>
        <td><label><input type="radio" name="dif<?=$i ?>" value="2"/> <?=htmlspecialchars(mb_convert_encoding($dif['value'], 'UTF-8', 'HTML-ENTITIES' )) ?></label></td>
      </tr>
<? $i++; } ?>
    </table>
    <br/>
    <input type="submit" value="Atjaunot" />
    </form>
  </body>
  </html>
<?
  die;
}

if ($difphrases && $_SESSION['difmatch'])
{
  for ($i=0; $i<count($difphrases); $i++)
  {
    $p = $difphrases[$i];
    $m = $_SESSION['difmatch'][$i];
    if ($m == 2) $phrases[$p['name']]['langs'][$p['lang']] = $p['value'];
  }
}

function insertPhrasenames()
{
  global $phrasenames, $pnames;
//  $pnames = Array();
  sqlQuery('DROP TABLE IF EXISTS phrasenames_tmp');
  sqlQuery('CREATE TABLE phrasenames_tmp LIKE phrasenames');
  foreach ($phrasenames as $name)
  {
    sqlQuery('INSERT INTO phrasenames_tmp SET name="'.db_escape($name).'"');
//    $pnames[$name] = sqlLastID();
  }
}

if ($update_db1)
{
  $GLOBALS['DBH'] = $db1;
  insertPhrasenames();
}

if ($update_db2)
{
  $GLOBALS['DBH'] = $db2;
  insertPhrasenames();
}


function insertPhrases()
{
  global $phrases;
  sqlQuery('DROP TABLE IF EXISTS phrases_tmp');
  sqlQuery('CREATE TABLE phrases_tmp LIKE phrases');
  foreach ($phrases as $p)
  {
    foreach ($p['langs'] as $lang => $phrase)
    {
      sqlQuery('INSERT INTO phrases_tmp SET
        lang="'.db_escape($lang).'",
        phrasename='.$p['phrasename_id'].',
        phrase="'.db_escape($phrase).'"
        '
      );
    }
  }
}

if ($update_db1)
{
  $GLOBALS['DBH'] = $db1;
  insertPhrases();
}

if ($update_db2)
{
  $GLOBALS['DBH'] = $db2;
  insertPhrases();
}

function renameTables()
{
  sqlQuery('DROP TABLE phrasenames');
  sqlQuery('RENAME TABLE phrasenames_tmp TO phrasenames');
  sqlQuery('DROP TABLE phrases');
  sqlQuery('RENAME TABLE phrases_tmp TO phrases');
}

if ($update_db1)
{
  $GLOBALS['DBH'] = $db1;
  renameTables();
}

if ($update_db2)
{
  $GLOBALS['DBH'] = $db2;
  renameTables();
}


echo '<pre>'; var_dump($phrases); echo '</pre>';

?>