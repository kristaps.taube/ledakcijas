<?
$perm_managesites = $GLOBALS['perm_managesites'];

$cols = Array(

  "title"        => Array(
    "width"     => "30%",
    "title"     => "{%langstr:title%}",
    "format"    => "<a href=\"#\" OnClick=\"javascript:window.location='?module={%mod%}&site_id={%site_id%}{%cmodparam%}'; return false;\">{%title%}</a> ({%license%})"

  ),

  "domain"    => Array(
    "width"     => "25%",
    "title"     => "{%langstr:domain%}"
  ),

  "created"     => Array(
    "width"     => "15%",
    "title"     => "{%langstr:col_created%}",
    "format"    => "<font color=\"#999999\">{%created%}</font>"
  ),

  "lastmod"    => Array(
    "width"     => "15%",
    "title"     => "{%langstr:col_modified%}",
    "format"    => "<font color=\"#999999\">{%lastmod%}</font>"
  ),

  "modby"       => Array(
    "width"     => "15%",
    "title"     => "{%langstr:modify_by%}"
  )

);

if(!$perm_managesites)
{
  unset($cols['title']['format']);
}


$data = sqlQueryData("SELECT a.site_id as site_id,a.title as title,a.domain as domain,FROM_UNIXTIME(a.created) as created,FROM_UNIXTIME(a.lastmod) as lastmod,wapsite,b.username as modby FROM sites as a, users as b WHERE a.modby=b.user_id");
foreach($data as $key => $row)
{
  CheckLicense($row['domain']);
  $data[$key]['license'] = $GLOBALS['licensetext'];

  if ($GLOBALS['custommodule'][$row['domain']])
  {
    $data[$key]['mod'] = $GLOBALS['custommodule'][$row['domain']];
    $data[$key]['cmodparam'] = "&cmodule=1";
  }
  else
  {
    if ($row['wapsite']==0)
    {
        $data[$key]['mod'] = "pages";
    }
    else
    {
        $data[$key]['mod'] = "wap";
    }
    $data[$key]['cmodparam'] = "";
  }
}

echo '
<script language="JavaScript">
  var selrow = ' . (int) ($data[0][0]) . ';
</script>';

require($GLOBALS['cfgDirRoot']."library/"."class.datagrid.php");

$Table = new DataGrid();
$Table->cols = $cols;
$Table->data = $data;
$Table->footer = false;
$Table->rowformat = " onClick=\"javascript:HighLightTR(this, {%site_id%});if(SelectedRowID){ EnableToolbarButtons(che); }else { DisableToolbarButtons(che); }\"";
echo $Table->output();

echo '<script language="JavaScript" type="text/javascript">
/*<![CDATA[*/
che = new Array("props", "perms", "edit", "del");
DisableToolbarButtons(che);
/*]]>*/
</script>';


?>
