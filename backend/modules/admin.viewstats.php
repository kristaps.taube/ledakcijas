<?

if(!$GLOBALS['perm_accessadmin'])
  Die('Access Forbidden');

if (!isset($_GET['period']))
    $_SESSION['period'] = 0;
else $_SESSION['period'] = $_GET['period'];

$t = mktime(0, 0, 0, date("m")  , date("d"), date("Y"));
$y = mktime(0, 0, 0, date("m")  , date("d")-1, date("Y"));
$w = mktime(date("H"), date("i"), date("s"), date("m")  , date("d")-7, date("Y"));
$m = mktime(date("H"), date("i"), date("s"), date("m")-1  , date("d"), date("Y"));
$m3 = mktime(date("H"), date("i"), date("s"), date("m")-3  , date("d"), date("Y"));
$m6 = mktime(date("H"), date("i"), date("s"), date("m")-6  , date("d"), date("Y"));
$yr = mktime(date("H"), date("i"), date("s"), date("m")  , date("d"), date("Y")-1);

switch($_SESSION['period'])
{
    case 0: $condition = " timestamp>".$t;
            break;
    case 1: $condition = " timestamp>".$y." and timestamp<".$t;
            break;
    case 2: $condition = " timestamp>".$w;
            break;
    case 3: $condition = " timestamp>".$m;
            break;
    case 4: $condition = " timestamp>".$m3;
            break;
    case 5: $condition = " timestamp>".$m6;
            break;
    case 6: $condition = " timestamp>".$yr;
            break;
}
//echo $condition;
//echo $_SESSION['period'];
if (!isset($_GET['page']))
    $page = 0;
else $page = $_GET['page'];

require_once($GLOBALS['cfgDirRoot']."library/"."func.files.php");

$site_id = $_GET['site_id'];

switch ($page)
{
    case 0:
        $cols = Array(
          "page"        => Array(
            "width"     => "50%",
            "title"     => "Pages"
          ),
          "hits"    => Array(
            "width"     => "15%",
            "title"     => "Hits"
          ),
          "unique"     => Array(
            "width"     => "20%",
            "title"     => "Unique hosts"
          )
        );
        break;
    case 1:
        $cols = Array(
          "image"       => Array(
            "width"     => "1%",
            "title"     => ""
          ),
          "referrers"        => Array(
            "width"     => "50%",
            "title"     => "Referrers"
          ),
          "quantity"    => Array(
            "width"     => "15%",
            "title"     => "Quantity"
          )
        );
        break;
    case 2:
        $cols = Array(
          "languages"        => Array(
            "width"     => "50%",
            "title"     => "Languages"
          ),
          "count"    => Array(
            "width"     => "15%",
            "title"     => "Count"
          ),
          "rate"     => Array(
            "width"     => "20%",
            "title"     => "%"
          )
        );
        break;
    case 3:
        $cols = Array(
          "agent"        => Array(
            "width"     => "50%",
            "title"     => "User Agent"
          ),
          "count"    => Array(
            "width"     => "15%",
            "title"     => "Count"
          ),
          "rate"     => Array(
            "width"     => "20%",
            "title"     => "%"
          )
        );
        break;
    case 4:
        $cols = Array(
          "os"        => Array(
            "width"     => "50%",
            "title"     => "O/S"
          ),
          "count"    => Array(
            "width"     => "15%",
            "title"     => "Count"
          ),
          "rate"     => Array(
            "width"     => "20%",
            "title"     => "%"
          )
        );
        break;
    case 5:
        $cols = Array(
          "browsers"        => Array(
            "width"     => "50%",
            "title"     => "Browsers"
          ),
          "count"    => Array(
            "width"     => "15%",
            "title"     => "Count"
          ),
          "rate"     => Array(
            "width"     => "20%",
            "title"     => "%"
          )
        );
        break;
    case 6:
        $cols = Array(
          "proxies"        => Array(
            "width"     => "50%",
            "title"     => "Proxies"
          ),
          "count"    => Array(
            "width"     => "15%",
            "title"     => "Count"
          )
        );
        break;
    case 7:
        $cols = Array(
          "ip"        => Array(
            "width"     => "15%",
            "title"     => "IP"
          ),
          "last"    => Array(
            "width"     => "20%",
            "title"     => "Last visited page"
          ),
          "lasttime"    => Array(
            "width"     => "15%",
            "title"     => "Time"
          ),
          "ref"    => Array(
            "width"     => "15%",
            "title"     => "Referrer"
          ),
          "total"    => Array(
            "width"     => "15%",
            "title"     => "Total hits"
          ),
          "time"    => Array(
            "width"     => "30%",
            "title"     => "Session time"
          )
        );
        break;
}

function bubble($a, $ind, $dif=0)
{
    $n = count($a)-$dif;
    for ($i=0; $i<$n-1; $i++) {
      for ($j=0; $j<$n-1-$i; $j++)
      {
        if ($a[$j+1][$ind] > $a[$j][$ind]) {  /* compare the two neighbors */
          $tmp = $a[$j];         /* swap a[j] and a[j+1]      */
          $a[$j] = $a[$j+1];
          $a[$j+1] = $tmp;

        }
      }
    }
    return $a;
}

/**
 * @name implodeAssoc($glue,$arr)
 * @description makes a string from an assiciative array
 * @parameter glue: the string to glue the parts of the array with
 * @parameter arr: array to implode
 */
function implodeAssoc($glue,$arr)
{ 
   $keys=array_keys($arr); 
   $values=array_values($arr); 

   return(implode($glue,$keys).$glue.implode($glue,$values)); 
}; 

/** 
 * @name explodeAssoc($glue,$arr) 
 * @description makes an assiciative array from a string 
 * @parameter glue: the string to glue the parts of the array with 
 * @parameter arr: array to explode 
 */
function explodeAssoc($glue,$str) 
{ 
   $arr=explode($glue,$str); 

   $size=count($arr); 

   for ($i=0; $i < $size/2; $i++) 
       $out[$arr[$i]]=$arr[$i+($size/2)]; 

   return($out); 
}; 




function pages($domain, $condition) {
  /*$data_all = sqlQueryDataAssoc("
  SELECT ".$_GET['site_id']."_stats.page_id,ip
  FROM ".$_GET['site_id']."_stats
  INNER JOIN ".$_GET['site_id']."_pages ON ".$_GET['site_id']."_pages.page_id=".$_GET['site_id']."_stats.page_id
  WHERE ".$condition);*/

  $data_hits_query = "
  SELECT ".$_GET['site_id']."_stats.page_id, count(ip) as 'hits'
  FROM ".$_GET['site_id']."_stats
  INNER JOIN ".$_GET['site_id']."_pages ON ".$_GET['site_id']."_pages.page_id=".$_GET['site_id']."_stats.page_id
  WHERE ".$condition." group by ".$_GET['site_id']."_stats.page_id order by hits desc";

  $total_hits = 0;
  $total_unique = 0;

  
  if ($qid = mysql_query($data_hits_query, $GLOBALS['DBH']))
  {

    while ($row = db_fetch_array($qid))
    {
        unset($unique);
        /*
        foreach ($data_all as $key2=>$row2)
        {
            //echo $row2['page_id']." ".$row2['ip']."<br>";
            if ($row['page_id']==$row2['page_id'] && !in_array($row2['ip'] ,$unique))
                $unique[]=$row2['ip'];
        }
        */

        $data[] = array('page' => "<a href='http://".$domain."/".PagePathById($row['page_id'], $_GET['site_id'])."' target='_blank'>http://".$domain."/".PagePathById($row['page_id'], $_GET['site_id'])."</a>",
                        'hits' => $row['hits'],
                        //'unique' => count($unique)
                        'unique' =>sqlQueryValue("SELECT count(distinct ip) FROM ".$_GET['site_id']."_stats INNER JOIN ".$_GET['site_id']."_pages ON ".$_GET['site_id']."_pages.page_id=".$_GET['site_id']."_stats.page_id WHERE ".$_GET['site_id']."_stats.page_id=".$row['page_id']." AND ".$condition)
        );
        $total_hits += $row['hits'];
        $unique_hits += count($unique);
    }
  }

  $unique_hits = sqlQueryValue("select count(distinct ip) from ".$_GET['site_id']."_stats WHERE ".$condition);

  $data = bubble($data, "unique");

  $data[] = array('page' => "<b>Total</b>",
                        'hits' => "<b>".$total_hits."</b>",
                        'unique' => "<b>".$unique_hits."</b>"
        );

  return $data;
}

function referrers($condition)
{
    global $page;

    $data = sqlQueryDataAssoc("SELECT referrer as 'referrers', count(ip) as 'quantity' FROM ".$_GET['site_id']."_stats WHERE referrer<>\"\" AND ".$condition." group by referrer order by quantity desc");

    $data_2 = "";
    foreach ($data as $key=>$val)
    {
        $url_data = parse_url($val['referrers']);
        if ($url_data['host'] == "") $url_data['host'] = "Unknown";
        $exists = 0;

        foreach ($data_2 as $key2=>$val2)
        {
            if ($val2['referrers'] == $url_data['host'])
            {
                $exists = 1;
                $k = $key2;
            }
        }
        if (!$exists)
        {
            if ($_GET['open']==$url_data['host'] && $_GET['act']==0)
            {
                $image = "gui/images/minus.gif";
                $act = 1;
            }
            else
            {
                $image = "gui/images/plus.gif";
                $act = 0;
            }

            $data_2[] = array(
            'image'=>'<a name="'.$url_data['host'].'"></a><a href="'.$_SERVER['PHP_SELF'].'?module=admin&site_id='.$_GET['site_id']
                .'&action=stats&period='
                .$_SESSION['period'].'&page='.$page.'&open='
                .$url_data['host'].'&act='.$act.'#'.$url_data['host'].'"><IMG height=11 src="'.$image.'" width=11 align=absMiddle border=0></a>',
            'referrers'=>$url_data['host'],
            'quantity'=>$val['quantity']);

            if ($_GET['open']==$url_data['host'] && $_GET['act']==0)
            {
                    $parent_key = count($data_2) - 1;
                    foreach($data as $key3=>$val3)
                        {
                            $sub = parse_url($val3['referrers']);
                            if ($sub['host'] == $url_data['host'])
                                $data_sub[] = array(
                                    'image'=>'',
                                    'referrers'=>'<a href="'.$val3['referrers'].'" target="_blank">'.$val3['referrers'].'</a>',
                                    'quantity'=>$val3['quantity']);
                        }
            }
        }
        else $data_2[$k]['quantity'] += $val['quantity'];
    }

    foreach ($data_2 as $key=>$val)
    {
        //echo $val['referrers'];
        if (!preg_match("/^<a href=/i",$val['referrers']))
            $data_2[$key]['referrers'] = '<a href="http://'.$val['referrers'].'" target="_blank">'.$val['referrers'].'</a>';
    }

    //$data_2 = bubble($data_2, "quantity");
    $ind = 'quantity';
    $n = count($data_2);
    for ($i=0; $i<$n-1; $i++) {
      for ($j=0; $j<$n-1-$i; $j++)
      {
        if ($data_2[$j+1][$ind] > $data_2[$j][$ind]) {  /* compare the two neighbors */
          $tmp = $data_2[$j];         /* swap a[j] and a[j+1]      */
          if ($parent_key == $j)
          {
            $parent_key++;
          }else if ($parent_key == $j+1)
          {
            $parent_key--;
          }
          $data_2[$j] = $data_2[$j+1];
          $data_2[$j+1] = $tmp;

        }
      }
    }

    if ($data_sub)
    {
        $data_2_bottom = array_slice($data_2, $parent_key + 1);
        $data_2_top = array_slice($data_2,0,$parent_key + 1);
        $data_2 = $data_2_top;;
        foreach ($data_sub as $key=>$row)
            $data_2[] = $row;

        foreach ($data_2_bottom as $key=>$row)
            $data_2[] = $row;
    }

    $no_ref = sqlQueryValue("select count(ip) from ".$_GET['site_id']."_stats WHERE referrer=\"\" AND ".$condition);

    $data_2[] = array(
        'image'=>'',
        'referrers'=>'No referrer',
        'quantity'=>$no_ref
    );

    //change rows with quantity=1
    foreach ($data_2 as $key=>$row)
    {
        if ($row['image']!='' && $row['quantity']==1)
        {
            $ref = preg_replace("/<a href=\"http\:\/\//","",$row['referrers']);
            $ref = preg_replace("/\"(\w|\W)+/","",$ref);
            //seek for whole link
            $new_ref = sqlQueryValue("select referrer from ".$_GET['site_id']."_stats WHERE referrer like '%".$ref."%'");
            $data_2[$key]['image'] = '<IMG height=11 src="gui/images/minus.gif" width=11 align=absMiddle border=0>';
            $data_2[$key]['referrers'] = '<a href="'.$new_ref.'" target="_blank">'.$new_ref.'</a>';
        }
    }

    return $data_2;
}

function languages($condition)
{
    //get unique ip and languages for them
    $data_all = sqlQueryDataAssoc("SELECT distinct ip,lang FROM `".$_GET['site_id']."_stats` WHERE ".$condition." order by ip desc");
    $total = 0;
    foreach ($data_all as $key=>$row)
    {
        if ($row['lang']=="") $row['lang']="Unknown";
        $total++;
        $k = -1;
        foreach ($data as $key2=>$row2)
            if ($row2['languages'] == $row['lang']) $k = $key2;
        if ($k==-1)
            $data[] = array('languages' => $row['lang'],
                            'count' => 1,
                            'rate' => 1
            );
        else
            $data[$k]['count'] = $data[$k]['count'] + 1;
    }
    foreach ($data as $key=>$row)
        $data[$key]['rate'] = round( ($row['count'] / $total)*100, 0);
    $data = bubble($data, "count");

    foreach ($data as $key=>$val)
        if ($val['languages']=="Unknown") $k1 = $key;
    
    if ($k1)
    {
        $tmp = $data[$k1];
        $data[$k1] = $data[count($data)-1];
        $data[count($data)-1] = $tmp;
    }

    return $data;
}

function agents($condition)
{
    //get unique ip and user agents for them
    //$data_all = sqlQueryDataAssoc("SELECT distinct ip,user_agent FROM `".$_GET['site_id']."_stats` WHERE ".$condition." order by ip desc");
    $data_all = sqlQueryData("SELECT user_agent, count(distinct ip) as 'c' FROM `".$_GET['site_id']."_stats` WHERE ".$condition." GROUP BY user_agent ORDER BY c desc");
    $total = 0;
    foreach ($data_all as $key=>$row)
    {
        //$total++;
        $total += $row['c'];
        //$k = -1;
        //foreach ($data as $key2=>$row2)
            //if ($row2['agent'] == $row['user_agent']) $k = $key2;
        //if ($k==-1)
        $data[] = array('agent' => $row['user_agent'],
                        'count' => $row['c'],
                        'rate' => 1
            );
        //else
            //$data[$k]['count'] = $data[$k]['count'] + 1;
    }
    foreach ($data as $key=>$row)
        $data[$key]['rate'] = round( ($row['count'] / $total)*100, 0);
    $data = bubble($data, "count");
    return $data;
//SELECT * FROM `1_stats` WHERE user_agent='Mozilla/4.0 (compatible; MSIE 5.01; Windows 98)'
}

function os($condition)
{
    require($GLOBALS['cfgDirRoot']."library/"."func.useragentinfo.php");

    //get unique ip and user agents for them
    $data_all = sqlQueryDataAssoc("SELECT distinct ip,user_agent FROM `".$_GET['site_id']."_stats` WHERE ".$condition." order by ip desc");
    $total = 0;
    foreach ($data_all as $key=>$row)
    {
        $os = browser_detection("os",$row['user_agent']);
        if ($os=="win") $os = "Windows";
        if ($os=="mac") $os = "Mac OS";
        $os_number = browser_detection("os_number",$row['user_agent']);
        //echo $os." ".$os_number;

        $total++;
        $k = -1;
        foreach ($data as $key2=>$row2)
            if ($row2['os'] == $os." ".$os_number) $k = $key2;
        if ($k==-1)
            $data[] = array('os' => $os." ".$os_number,
                            'count' => 1,
                            'rate' => 1
            );
        else
            $data[$k]['count'] = $data[$k]['count'] + 1;
    }
    foreach ($data as $key=>$row)
        $data[$key]['rate'] = round( ($row['count'] / $total)*100, 0);
    
    foreach ($data as $key=>$val)
    {
        if ($val['os']==" " || $val['os']=="") 
        {
            $k1 = $key;
            $data[$key]['os'] = "Unknown";
        }
    }

    if ($k1)
    {
        $tmp = $data[$k1];
        $data[$k1] = $data[count($data)-1];
        $data[count($data)-1] = $tmp;
    }

    $data = bubble($data, "count", 1);

    return $data;
}

function browsers($condition)
{
    require($GLOBALS['cfgDirRoot']."library/"."func.useragentinfo.php");

    //get unique ip and user agents for them
    $data_all = sqlQueryDataAssoc("SELECT distinct ip,user_agent FROM `".$_GET['site_id']."_stats` WHERE ".$condition);
    $total = 0;
    foreach ($data_all as $key=>$row)
    {
        $browser = browser_detection("browser",$row['user_agent']);
        $browser_number = browser_detection("number",$row['user_agent']);

        $total++;
        $k = -1;
        foreach ($data as $key2=>$row2)
            if ($row2['browsers'] == $browser." ".$browser_number) $k = $key2;
        if ($k==-1)
            $data[] = array('browsers' => $browser." ".$browser_number,
                            'count' => 1,
                            'rate' => 1
            );
        else
            $data[$k]['count'] = $data[$k]['count'] + 1;
    }
    foreach ($data as $key=>$row)
        $data[$key]['rate'] = round( ($row['count'] / $total)*100, 0);

    foreach ($data as $key=>$val)
    {
        if ($val['browsers']==" " || $val['browsers']=="") 
        {
            $k1 = $key;
            $data[$key]['browsers'] = "Unknown";
        }
    }

    if ($k1)
    {
        $tmp = $data[$k1];
        $data[$k1] = $data[count($data)-1];
        $data[count($data)-1] = $tmp;
    }

    $data = bubble($data, "count", 1);

    return $data;
}

function proxies($condition)
{
    $data = sqlQueryDataAssoc("SELECT distinct ip as 'proxies', count(page_id) as 'count' FROM `".$_GET['site_id']."_stats` WHERE is_proxy=1 AND ".$condition." GROUP BY ip order by count desc");
    return $data;
}

function visitors($domain)
{
    $time = time()-60*30;
    //$data = sqlQueryDataAssoc("select ip, page_id, timestamp, referrer from `".$_GET['site_id']."_stats` where timestamp>".$time." group by ip");

    $data_all = sqlQueryDataAssoc("select * from `".$_GET['site_id']."_stats` where timestamp>".$time." order by timestamp desc");
    
    foreach ($data_all as $key=>$row)
    {
        $exists = 0;
        foreach ($data_1 as $key1=>$row1)
        {
            if ($row1['ip'] == $row['IP'] && $row1['ua']==$row['user_agent'])
            {
                $exists = 1;
                $k = $key2;
            }
        }
        if (!$exists)
        {
            $min = "";
            $dif = time() - $row['timestamp'];
            if (round($dif / 60, 0)>0) $min = round($dif / 60, 0).' min ago';
            else $min .=  (time() - $row['timestamp']). ' sec ago';

            if ($row['referrer']=="") $ref = 'No referrer';
            else $ref = $row['referrer'];

            $total = sqlQueryValue("select count(*) from `".$_GET['site_id']."_stats` where ip='".$row['IP']."' and user_agent='".$row['user_agent']."' and timestamp>".$time);

            $begin = sqlQueryValue("select min(timestamp) from `".$_GET['site_id']."_stats` where ip='".$row['IP']."' and user_agent='".$row['user_agent']."' and timestamp>".$time);

            $end = sqlQueryValue("select max(timestamp) from `".$_GET['site_id']."_stats` where ip='".$row['IP']."' and user_agent='".$row['user_agent']."' and timestamp>".$time);
            
            $session = $end - $begin;
            $sess_time = round($session / 60, 0) . " min, ";
            $sess_time .= ($session%60) . " sec";

            $data_1[]=array(
                    'ip'=>$row['IP'],
                    'last'=>"<a target=\"_blank\" href='http://".$domain."/".PagePathById($row['page_id'],$_GET['site_id'])."'>http://".$domain."/".PagePathById($row['page_id'],$_GET['site_id'])."</a>",
                    'lasttime'=> $min,
                    'ref' => $ref,
                    'total' => $total,
                    'time' => $sess_time,
                    'ua' => $row['user_agent'],
                    'id' =>$row['ID']
                );
        }
    }

    foreach ($data_1 as $key=>$row)
    {
        $data_1[$key]['ip'] = '<a href="#" OnClick="openDialog(\'?module=admin&action=user_stats&site_id='.$site_id.'&site_id='.$GLOBALS['site_id'].'&id='.$row['id'].'\', 600, 600,0,1);">'.$row['ip'].'</a>';
    }

    return $data_1;
}


$domain = sqlQueryValue("SELECT domain FROM sites WHERE site_id=" . $_GET['site_id']);
switch ($page)
{
    case 0:
        $data = pages($domain, $condition);
        break;
    case 1:
        $data = referrers($condition);
        break;
    case 2:
        $data = languages($condition);
        break;
    case 3:
        $data = agents($condition);
        break;
    case 4:
        $data = os($condition);
        break;
    case 5:
        $data = browsers($condition);
        break;
    case 6:
        $data = proxies($condition);
        break;
    case 7:
        $data = visitors($domain);
        break;        
}

echo '
<script language="JavaScript">
  var selrow = ' . (int) ($data[0][0]) . ';
  function SelectPeriod()
  {                   document.location.href="http://localhost/constructor/backend/index.php?module=admin&site_id='.$_GET['site_id'].'&action=stats&period="+document.getElementById(\'period\').selectedIndex+"&page='.$page.'";
  }
</script>';

require($GLOBALS['cfgDirRoot']."library/"."class.datagrid.php");

$Table = new DataGrid();
$Table->cols = $cols;
$Table->data = $data;
$Table->footer = false;
$Table->padding = "2";
$Table->rowformat = " onClick=\"javascript:HighLightTR(this, '{%title%}');if(SelectedRowID){ EnableToolbarButtons(che); }else { DisableToolbarButtons(che); }\"";

echo '<table width="100%">';
echo '<tr><td width="180" valign="top">';
echo '<p align="center">'
     .'Time period:&nbsp;&nbsp;&nbsp;<select name="period" id="period" onChange="SelectPeriod()">'
     .'<option value="0"'; if ($_SESSION['period']==0) echo ' selected'; echo '>Today</option>'
     .'<option value="1"'; if ($_SESSION['period']==1) echo ' selected'; echo '>Yesterday</option>'
     .'<option value="2"'; if ($_SESSION['period']==2) echo ' selected'; echo '>Last 7 days</option>';

     if ($GLOBALS['cfgStatiscticsPeriod']==1 || $GLOBALS['cfgStatiscticsPeriod']==2)
     {
     echo '<option value="3"'; if ($_SESSION['period']==3) echo ' selected'; echo '>Last month</option>';
     echo '<option value="31"'; if ($_SESSION['period']==4) echo ' selected'; echo '>Last 3 months</option>';
     echo '<option value="32"'; if ($_SESSION['period']==5) echo ' selected'; echo '>Last 6 months</option>';
     }

     if ($GLOBALS['cfgStatiscticsPeriod']==2)
     echo '<option value="4"'; if ($_SESSION['period']==6) echo ' selected'; echo '>Last year</option>';
     
echo '</select></p>';
echo '
    <ul>
        <li><a href="'.$_SERVER['PHP_SELF'].'?module=admin&site_id='.$_GET['site_id'].'&action=stats&period='
        .$_SESSION['period'].'&page=0"'; if ($page==0) echo ' style="font-weight:bold;"'; echo '>Pages</a>
        <li><a href="'.$_SERVER['PHP_SELF'].'?module=admin&site_id='.$_GET['site_id'].'&action=stats&period='
        .$_SESSION['period'].'&page=1"'; if ($page==1) echo ' style="font-weight:bold;"'; echo '>Referrers</a>
        <li><a href="'.$_SERVER['PHP_SELF'].'?module=admin&site_id='.$_GET['site_id'].'&action=stats&period='
        .$_SESSION['period'].'&page=2"'; if ($page==2) echo ' style="font-weight:bold;"'; echo '>Languages</a>
        <li><a href="'.$_SERVER['PHP_SELF'].'?module=admin&site_id='.$_GET['site_id'].'&action=stats&period='
        .$_SESSION['period'].'&page=3"'; if ($page==3) echo ' style="font-weight:bold;"'; echo '>User Agent</a>
        <li><a href="'.$_SERVER['PHP_SELF'].'?module=admin&site_id='.$_GET['site_id'].'&action=stats&period='
        .$_SESSION['period'].'&page=4"'; if ($page==4) echo ' style="font-weight:bold;"'; echo '>O/S</a>
        <li><a href="'.$_SERVER['PHP_SELF'].'?module=admin&site_id='.$_GET['site_id'].'&action=stats&period='
        .$_SESSION['period'].'&page=5"'; if ($page==5) echo ' style="font-weight:bold;"'; echo '>Browsers</a>
        <li><a href="'.$_SERVER['PHP_SELF'].'?module=admin&site_id='.$_GET['site_id'].'&action=stats&period='
        .$_SESSION['period'].'&page=6"'; if ($page==6) echo ' style="font-weight:bold;"'; echo '>Proxies</a>
        <li><a href="'.$_SERVER['PHP_SELF'].'?module=admin&site_id='.$_GET['site_id'].'&action=stats&period='
        .$_SESSION['period'].'&page=7"'; if ($page==7) echo ' style="font-weight:bold;"'; echo '>Visitors</a>
        <li><a href="'.$_SERVER['PHP_SELF'].'?module=admin&site_id='.$_GET['site_id'].'&action=stats&period='
        .$_SESSION['period'].'&page=8"'; if ($page==8) echo ' style="font-weight:bold;"'; echo '>Graphs</a>
    </ul>
';

echo '</td><td valign="top">';
if ($page!=8) echo $Table->output();
if ($page==8)
{

//get 5 most popular pages and total hits count
$data_p = sqlQueryData("select page_id, count(ip) as 'c' from `".$_GET['site_id']."_stats` where ".$condition." group by page_id order by c desc limit 5");


foreach ($data_p as $key=>$val)
{
   $qid = mysql_query("select timestamp from ".$_GET['site_id']."_stats where ".$condition." and page_id=".$val['page_id'], $GLOBALS['DBH']);

   //$time = sqlQueryData("select timestamp from ".$_GET['site_id']."_stats where ".$condition." and page_id=".$val['page_id']);
//echo $_SESSION['period'];
switch ($_SESSION['period'])
{
   case 0:
       //create array for today
       //foreach ($time as $k=>$v)
       while ($v = mysql_fetch_array($qid))
       {
           $h = date("G", $v['timestamp']);
           $page_data[$key][$h]++;
       }
       break;
   case 1:
       //create array for yesterday
       while ($v = db_fetch_array($qid))
       {
           $h = date("G", $v['timestamp']);
           $page_data[$key][$h]++;
       }
       break;
   case 2:
       //create array for a week
       while ($v = mysql_fetch_array($qid))
       {
           //$h = date("G", $v['timestamp']);
           $ind = 7 - (mktime(0,0,0,date("m"),date("d")+1,date("Y")) - $v['timestamp'])/60/60/24 + 1;
           $page_data[$key][$ind]++;
       }
       break;
   case 3:
       //create array for a month
       if (date("d")<date("d", mktime(0,0,0,date("m"),date("d")+1,date("Y"))))
           $days = date("t",mktime(0,0,0,date("m")-1,date("d"),date("Y")));
       else
           $days = date("t");

       while ($v = db_fetch_array($qid))
       {
           $ind = $days - (mktime(0,0,0,date("m"),date("d")+1,date("Y")) - $v['timestamp'])/60/60/24 + 1;
           $page_data[$key][$ind]++;
       }
       break;
   case 4:
   case 5:
   case 6:
       //create array for a year
       while ($v = mysql_fetch_array($qid))
       {
           $dif = 12 - date("n");
           $ind = (date("n",$v['timestamp'])+$dif) % 12;
           if ($ind==0 && date("y",$v['timestamp'])==date("y")) $ind = 12;
           //if ($ind==0 && date("y",$v['timestamp'])<date("y")) $ind = 1;
           $page_data[$key][$ind]++;
           unset($v);
           unset($ind);
           unset($dif);
       }
       break;
}
}

/*
   foreach ($page_data as $key=>$row)
    {
       print_r($row);
        echo '<br>';
    }
*/

   $sep = 0;
   for ($n=0; $n<=4; $n++)
    {
        if ($sep) $packed .= ';';
        $packed .= implodeAssoc(',',$page_data[$n]);
        $sep = 1;
    }

   switch ($_SESSION['period'])
   {
        case 0:
            echo '<div>'.date("H:i:s - d.m.Y",$t). " - ".date("H:i:s - d.m.Y",mktime(0, 0, 0, date("m"),date("d")+1,date("Y"))).'</div><br>';
            break;
        case 1:
            echo '<div>'.date("H:i:s - d.m.Y",$y). " - ".date("H:i:s - d.m.Y",$t).'</div><br>';
            break;
        case 2:
            echo '<div>'.date("H:i:s - d.m.Y",$w). " - ".date("H:i:s - d.m.Y",time()).'</div><br>';
            break;
        case 3:
            echo '<div>'.date("H:i:s - d.m.Y",$m). " - ".date("H:i:s - d.m.Y",time()).'</div><br>';
            break;
        case 4:
        case 5:
        case 6:
            echo '<div>'.date("H:i:s - d.m.Y",$yr). " - ".date("H:i:s - d.m.Y",time()).'</div><br>'; 
            break;
   }

    echo '<img src="?module=admin&action=graph&perdiod='.$_SESSOION['period'].'&site_id='.$_GET['site_id'].'&data='.$packed.'">';

    $colors=array(0=>'FF0000',1=>'00FF00',2=>'0000FF',3=>'FFCC00',4=>'FF00FF');

    echo '<table>';
    echo '<tr><td></td><td></td><td>Page</td><td style="padding-left:10px;">Hits</td></tr>';
    $i = 0;
    foreach ($data_p as $key=>$row)
    {
        echo '<tr><td bgcolor="#'.$colors[$i].'"></td><td width="10"></td><td>http://'.$domain.'/'.PagePathById($row['page_id'], $_GET['site_id']).'</td><td style="padding-left:10px;">'.$row['c'].'</td></tr>';
        $i++;
    }
    echo '</table>';
}
echo '</td></tr>';
echo '</table>';

echo '<script language="JavaScript" type="text/javascript">
/*<![CDATA[*/
che = new Array("ren");
DisableToolbarButtons(che);
/*]]>*/
</script>';

?>
