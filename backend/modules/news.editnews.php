<?


$site_id = $_GET['site_id'];
$page_id = $_GET['page_id'];
$GLOBALS['PageEncoding'] = PageEncoding($page_id, $site_id);
$GLOBALS['maincharset']  = PageEncoding($page_id, $site_id, false);

$form_data = Array(

  "news_id"     => Array(
    "label"     => "ID:",
    "type"      => "hidden"
  ),

  "title"    => Array(
    "label"     => "Title:",
    "size"      => "35",
    "type"      => "str"
  ),
  
  "text" => Array(
    "label"     => "Text:",
    "type"      => "customtext",
    "rows"      => "7",
    "cols"      => "50",
    "dialog"    => "?module=wysiwyg&site_id=".$GLOBALS['site_id']."&page_id=".$page_id,
    "dialogw"   => "720",
    "dialogh"   => "450",
    "dlg_res"   => true
  ),
  
  "date"    => Array(
    "label"     => "Date:",
    "type"      => "str",
    "size"      => "30"
  )

);

$action = $_GET['action'];
$category_id = $_GET['category_id'];
$create = ($action == 'newnews');
$id = $_GET['id'];

if(!$create)
{
  $data = sqlQueryRow("SELECT news_id, text, title, date FROM ".$site_id."_news WHERE news_id=$id");
  $form_data['title']['value'] = $data['title'];
  $form_data['text']['value'] = $data['text'];
  $form_data['date']['value'] = $data['date'];
  $form_data['news_id']['value'] = $data['news_id'];
}else
{
  $form_data['date']['value'] = date("Y-m-d H:i:s", time());
}


require($GLOBALS['cfgDirRoot']."library/"."class.formgen.php");
$FormGen = new FormGen();
if(!$create)
  {
    $FormGen->action = "?module=".$GLOBALS['module']."&site_id=".$site_id."&page_id=$page_id&action=modnews&category_id=$category_id";
  }else{
    $FormGen->action = "?module=".$GLOBALS['module']."&site_id=".$site_id."&page_id=$page_id&action=addnews&category_id=$category_id";
  }
$FormGen->title = "News Item Properties";
$FormGen->cancel =  'close';
$FormGen->properties = $form_data;
$FormGen->buttons = true;
echo $FormGen->output();


?>
