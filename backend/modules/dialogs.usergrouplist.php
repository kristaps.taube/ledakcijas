<?

function array_search_recursive( $needle, $haystack )
{
   $path = NULL;
   $keys = array_keys($haystack);
   while (!$path && (list($toss,$k)=each($keys))) {
     $v = $haystack[$k];
     if (is_scalar($v)) {
         if ($v===$needle) {
           $path = array($k);
         }
     } elseif (is_array($v)) {
         if ($path=array_search_recursive( $needle, $v )) {
           array_unshift($path,$k);
         }
     }
   }
   return $path;
}

function InitCollection(&$coltype, &$colname, &$categoryID, &$collection)
{
    //initialize collection class
    include_once($GLOBALS['cfgDirRoot'] . "collections/class." . $coltype . ".php");
    $colname = '';
    $collection = new $coltype($colname,$categoryID);
}

  
  $coltype = $_GET['coltype'];
  $site_id = $_GET['site_id'];  

  if ($_GET['categoryid']) 
  {
      $categoryid = $_GET['categoryid'];
      //echo $coltype." ".$categoryid;
      InitCollection($coltype, $colname, $categoryid, $collection);

      //-----------------------------------------------------------
      //collection actions
      if ($_GET['save'])
      {
        $a = Array();
        foreach($collection->properties AS $key=>$val)
        {
          if (in_array(substr($key,2),$date_cols)) $a[] = strtotime($_POST[$key]);
          else $a[] = $_POST[$key];
        }

        $i = $collection->AddNewItem();
        $collection->ChangeItem($i,$a);
      }
      if ($_GET['add'])
      {
        $form_data = $collection->properties;
        require($GLOBALS['cfgDirRoot']."library/"."class.formgen.php");
        $FormGen = new FormGen();
        $FormGen->action = "?module=dialogs&action=usergrouplistframe&site_id=" . $_GET['site_id'] . "&coltype=".$coltype."&categoryid=".$categoryid."&useid=".$_GET['useid'] . "&columnname=" . $_GET['columnname'] . "&idcolumnname=" . $_GET['idcolumnname']."&save=1";
        $FormGen->title = "Item Properties";
        $FormGen->cancel =  "?module=dialogs&action=usergrouplistframe&site_id=" . $_GET['site_id'] . "&coltype=".$coltype."&categoryid=".$categoryid."&useid=".$_GET['useid'] . "&columnname=" . $_GET['columnname'] . "&idcolumnname=" . $_GET['idcolumnname'];
        $FormGen->properties = $form_data;
        $FormGen->buttons = true;
        echo $FormGen->output();
        $dontoutput = true;
      }else
      if ($_GET['del'])
      {
        $collection->DelItem($_GET['ind']);
      }
      //-----------------------------------------------------------
      
      $data = $collection->getDBData();
  }
 
  //print_r($_GET);
  if($_GET['columnname'])
    $columnname = $_GET['columnname'];
  else
    $columnname = 'col1';
  if($_GET['idcolumnname'])
    $idcolumnname = $_GET['idcolumnname'];
  else
    $idcolumnname = 'col0';


  if ($collection->properties['it1'])
  {
    $sql = ", `".$columnname."`";
    $showformat = "{%".$columnname."%}";
  }
  else
  {
    $sql = "";
    $showformat = "{%".$idcolumnname."%}";
  }

  if(!$_GET['useid'])
  {
    $valueformat = '{%'.$idcolumnname.'%}';
    $valuecolumn = $idcolumnname;
  }
  else
  {
    $valueformat = '{%item_id%}';
    $valuecolumn = 'item_id';
  }

  $sel = $_GET['sel'];
  if($sel == 'undefined') $sel = '';
  if($sel){
    $data = sqlQueryData("SELECT item_id, ind, ".$idcolumnname."".$sql.", ELT((FIND_IN_SET(".$valuecolumn.",'$sel')>0)+1,'','checked') as checked FROM `".$site_id."_coltable_".$collection->name."`");
  }else{
    $data = sqlQueryData("SELECT item_id, ind, ".$idcolumnname."".$sql.", '' as checked FROM `".$site_id."_coltable_".$collection->name."`");
  }

  $cols = Array(

  "group_id"    => Array(
    "width"     => "5%",
    "title"     => "#",
  ),

  "checkbox"    => Array(
    "width"     => "5%",
    "title"     => "",
    "format"    => "<input type=checkbox name=chbox value=\"".$valueformat."\" {%checked%} OnClick=\"checkBoxes();\">"
  ),

  "name"   => Array(
    "width"     => "80%",
    "title"     => "Value",
    "format"    => $showformat
  ),

  "del"     => Array(
    "width"     => "5%",
    "title"     => "",
    "format"    => "<a href='?module=dialogs&action=usergrouplistframe&site_id=" . $_GET['site_id'] . "&coltype=".$coltype."&categoryid=".$categoryid."&useid=".$_GET['useid'] . "&columnname=" . $_GET['columnname'] . "&idcolumnname=" . $_GET['idcolumnname']."&del=1&ind={%ind%}'><img src=\"".$GLOBALS['cfgWebRoot']."gui/images/del.gif\" border=\"0\"></a>"
  )

);

if(!$dontoutput)
{

  require($GLOBALS['cfgDirRoot']."library/"."class.datagrid.php");

  $Table = new DataGrid();
  $Table->cols = $cols;
  $Table->data = $data;
  $Table->header = false;
  $Table->footer = false;
  echo $Table->output();

  echo '<input type="button" value="Add" OnClick="document.location=\'?module=dialogs&action=usergrouplistframe&site_id=' . $_GET['site_id'] . '&coltype='.$coltype.'&categoryid='.$categoryid."&useid=".$_GET['useid'] . "&columnname=" . $_GET['columnname'] . "&idcolumnname=" . $_GET['idcolumnname'].'&add=1\'">';

}
?>
