<?

$action = $_GET["action"];
$GLOBALS['site_id'] = intval($_GET["site_id"]);

if (isset($_GET['path'])) { $_SESSION['path_img'] = $_GET['path']; }
if ($_SESSION['path_img'] != "") $GLOBALS['path'] = $_SESSION['path_img'];
else $GLOBALS['path'] = $_GET["path"];

if($GLOBALS['cfgMini'] && $GLOBALS['cfgMiniDemo'] && strtolower($_SESSION["userdata"]['username']) != 'admin')
{
  //ensure that path starts with upload/userNNNNN
  $muststart_s = 'upload/user'.$_SESSION['demo_db_id'];
  if(substr($GLOBALS['path'], 0, strlen($muststart_s)) != $muststart_s)
  {
    $GLOBALS['path'] = $muststart_s;
  }
}

$GLOBALS['PageEncoding'] = PageEncoding(0, $site_id);
$GLOBALS['maincharset']  = PageEncoding(0, $site_id, false);


if (isset($_GET['parent_dir'])) { $_SESSION['parent_dir'] = $_GET['parent_dir']; }
else if (!isset($_SESSION['parent_dir'])) $_SESSION['parent_dir'] = 0;

//show root
if ((($_SESSION['root']!=1 && $_SESSION['root']!=-1) || $_GET['path']=="/" || $_GET['path']=="" && $action=="list") ) {
    $_SESSION['root'] = 0;
}

//show documents
if (isset($_GET['parent_dir'])) $_SESSION['root'] = -1;
//show files
if (isset($_GET['path']) && $_GET['path']!="/" && $_GET['path']!="") $_SESSION['root'] = 1;

$GLOBALS['close'] = $_GET["close"];
$GLOBALS['view'] = $_GET["view"];
$GLOBALS['tabs'] = 'site';
if (isset($_GET['backup'])) $GLOBALS['backup'] = $_GET['backup'];

unset($perm_modsite);
if(($site_id == $GLOBALS["currentUserSiteID"])or(!$GLOBALS["currentUserSiteID"]))
  $perm_modsite = CheckPermission(6, $site_id);
if(!$perm_modsite)
  AccessDenied(False);

unset($perm_managefiles);
if(($site_id == $GLOBALS["currentUserSiteID"])or(!$GLOBALS["currentUserSiteID"]))
  $perm_managefiles = CheckPermission(7, $site_id);

require_once($GLOBALS['cfgDirRoot']."library/"."func.files.php");

switch ($action) {

  case "newdir":
    $docTitle = "Make Directory";
    $docTemplate = "form.htm";
    if ($_SESSION['root']==-1 && $_GET['dialog']==1)
        $docScripts['body'] = "docs.newdir.php";
    else $docScripts['body'] = "files.newdir.php";
    break;

  case "upload":
    $docTitle = "{%langstr:upload_file%}";
    $docTemplate = "form.htm";
    if ($_SESSION['root']==-1 && $_GET['dialog']==1)
        $docScripts['body'] = "docs.upload.php";
    else $docScripts['body'] = "files.upload.php";
    break;

  case "rename":
    $docTitle = "Rename File";
    $docTemplate = "form.htm";
    $docScripts['body'] = "files.rename.php";
    break;

  case "mkdir":
    $docScripts['exec'] = "files.exec.php";
    if ($GLOBALS['close'] == 'no') {
      $docTemplate = "form.htm";
      if ($_GET['dironly']) $docScripts['body'] = "files.dirlist.php";
      else $docScripts['body'] = "files.list.php";
    } else {
      $docTemplate = "close.htm";
    }
    break;

  case "save"://print_r($_GET);die;
  case "dorename":
  case "savekeywordcat":
  case "savekeyword":
  case "dodelkeyword":
  case "setfilekeywords":
    $docScripts['exec'] = "files.exec.php";
    if ($GLOBALS['close'] == 'no') {
      $docTemplate = "form.htm";
      $docScripts['body'] = "files.list.php";
    } else {
      $docTemplate = "close.htm";
    }
    break;

  case "list":
    $docTemplate = "form.htm";
    $docScripts['body'] = "files.list.php";
    break;

  case "dirlist":
    $docTemplate = "form.htm";
    $docScripts['body'] = "files.dirlist.php";
    break;

  case "copy":
    $docTitle = "Rename File";
    $docTemplate = "form.htm";
    $docScripts['body'] = "files.copy.php";
    break;

  case "docopy":
    $docScripts['exec'] = "files.exec.php";
    if ($GLOBALS['close'] == 'no') {
      $docTemplate = "form.htm";
      $docScripts['body'] = "files.list.php";
    } else {
      $docTemplate = "close.htm";
    }
    break;

  case "edit":
        $docTitle = "{%langstr:edit_file%}";
        $docTemplate = "form.htm";
        $docScripts['body'] = "files.contents.php";
      break;

  case "unzip":
        $docTitle = "Atpakot ZIP";
        $docTemplate = "form.htm";
        $docScripts['body'] = "files.unzip.php";
      break;

  case "savecontents":
        $docScripts['exec'] = "files.exec.php";
        $docTemplate = "close.htm";
      break;

  case "keywords":
        $docTitle = "{%langstr:file_keywords%}";
        $docTemplate = "form.htm";
        $docScripts['body'] = "files.keywords.php";
      break;

  case "addkeywordcat":
        $docTitle = "Add Category";
        $docTemplate = "form.htm";
        $docScripts['body'] = "files.keywordcat.php";
      break;

  case "addkeyword":
        $docTitle = "Add Keyword";
        $docTemplate = "form.htm";
        $docScripts['body'] = "files.keyword.php";
      break;

  case "delkeyword":
        $docTitle = "Delete Keyword";
        $docTemplate = "form.htm";
        $docScripts['body'] = "files.delkeyword.php";
      break;

  case "trash_restore":
      $docScripts['exec'] = "files.exec.php";
      break;

  case "delete":
    $docScripts['exec'] = "files.exec.php";


    if ($GLOBALS['close'] == 'no') {
      $docTemplate = "form.htm";
      if ($_GET['dironly']) $docScripts['body'] = "files.dirlist.php";
      else $docScripts['body'] = "files.list.php";

      break;
    }else{
			// header('Location: ?module=files&site_id=' . $GLOBALS['site_id']);
    }

  default:


    if($perm_managefiles)
    {

      require($GLOBALS['cfgDirRoot']."library/"."class.toolbar2.php");
      $Toolbar = new Toolbar2();

      $Toolbar->AddSpacer();

      $Toolbar->AddButtonImage("mkdir", 'files_new_folder', "{%langstr:docs_make_directory%}", "", "openDialog('?module=files&site_id=".$GLOBALS['site_id']."&path=".$GLOBALS['path']."&action=newdir', 400, 120);", 31, "{%langstr:docs_make_new_directory%}");
      $Toolbar->AddButtonImage("upload", 'files_upload', "{%langstr:docs_upload%}", "", "openDialog('?module=files&site_id=".$GLOBALS['site_id']."&path=".$GLOBALS['path']."&action=upload', 500, 300);", 31, "{%langstr:files_upload_file_current_dir%}");
      $Toolbar->AddSeperator();
      $Toolbar->AddButtonImage("ren", 'files_rename', "{%langstr:docs_rename%}", "", "if (window.fff) openDialog('?module=files&site_id=".$GLOBALS['site_id']."&path=".$GLOBALS['path']."&action=rename&name='+window.fff, 500, 245); else openDialog('?module=files&site_id=".$GLOBALS['site_id']."&path=".$GLOBALS['path']."&action=rename&name='+escape(SelectedRowID), 500, 145);", 31, "{%langstr:files_rename_file%}");

      #$Toolbar->AddButtonImage("copy", 'files_copy', "{%langstr:toolbar_copy%}", "", "javascript:if (window.fff) openDialog('?module=dialogs&site_id=".$GLOBALS['site_id']."&path=".$GLOBALS['path']."&action=dirx&file='+window.fff, '', 'font-family:Verdana; font-size:12; dialogWidth:530px; dialogHeight:370px; help:0; status:0'); else if (SelectedRowID) openDialog('?module=dialogs&site_id=".$GLOBALS['site_id']."&path=".$GLOBALS['path']."&action=dirx&file='+escape(SelectedRowID), '', 'font-family:Verdana; font-size:12; dialogWidth:530px; dialogHeight:370px; help:0; status:0'); if (window.fff || SelectedRowID) document.location='?module=files&path=".$GLOBALS['path']."&site_id=".$GLOBALS['site_id']."'", 31, "{%langstr:toolbar_copy_files%}");
      $Toolbar->AddButtonImage("copy", 'files_copy', "{%langstr:toolbar_copy%}", "", "javascript:if (window.fff) openDlg('?module=dialogs&site_id=".$GLOBALS['site_id']."&path=".$GLOBALS['path']."&action=dirx&file='+window.fff, '', {}, function(){ if (window.fff || SelectedRowID) document.location='?module=files&path=".$GLOBALS['path']."&site_id=".$GLOBALS['site_id']."' }); else if (SelectedRowID) openDlg('?module=dialogs&site_id=".$GLOBALS['site_id']."&path=".$GLOBALS['path']."&action=dirx&file='+escape(SelectedRowID), '', {}, function(){ if (window.fff || SelectedRowID) document.location='?module=files&path=".$GLOBALS['path']."&site_id=".$GLOBALS['site_id']."' }); ", 31, "{%langstr:toolbar_copy_files%}");

			#$Toolbar->AddButtonImage("move", 'files_move', "{%langstr:toolbar_move%}", "", "javascript:if (window.fff) openDialog('?module=dialogs&action=dirx&site_id=".$GLOBALS['site_id']."&path=".$GLOBALS['path']."&move=1&file='+window.fff, '', 'font-family:Verdana; font-size:12; dialogWidth:530px; dialogHeight:370px; help:0; status:0'); else if (SelectedRowID) openDialog('?module=dialogs&site_id=".$GLOBALS['site_id']."&path=".$GLOBALS['path']."&action=dirx&move=1&file='+escape(SelectedRowID), '', 'font-family:Verdana; font-size:12; dialogWidth:530px; dialogHeight:370px; help:0; status:0'); if (window.fff || SelectedRowID) document.location='?module=files&path=".$GLOBALS['path']."&site_id=".$GLOBALS['site_id']."'", 31, "{%langstr:toolbar_move_files%}");
      $Toolbar->AddButtonImage("move", 'files_move', "{%langstr:toolbar_move%}", "", "javascript:if (window.fff) openDlg('?module=dialogs&site_id=".$GLOBALS['site_id']."&path=".$GLOBALS['path']."&action=dirx&move=1&file='+window.fff, '', {}, function(){ if (window.fff || SelectedRowID) document.location='?module=files&path=".$GLOBALS['path']."&site_id=".$GLOBALS['site_id']."' }); else if (SelectedRowID) openDlg('?module=dialogs&site_id=".$GLOBALS['site_id']."&path=".$GLOBALS['path']."&action=dirx&move=1&file='+escape(SelectedRowID), '', {}, function(){ if (window.fff || SelectedRowID) document.location='?module=files&path=".$GLOBALS['path']."&site_id=".$GLOBALS['site_id']."' }); ", 31, "{%langstr:toolbar_copy_files%}");

      $Toolbar->AddButtonImage("edit", 'files_edit', "{%langstr:toolbar_edit%}", "", "if (SelectedRowID) openDialog('?module=files&site_id=".$GLOBALS['site_id']."&path=".$GLOBALS['path']."&action=edit&file='+escape(SelectedRowID), 800, 600);", 31, "{%langstr:toolbar_edit_file%}");

      $Toolbar->AddButtonImage("unzip", 'files_unzip', "{%langstr:unzip%}", "", "if (SelectedRowID) openDialog('?module=files&site_id=".$GLOBALS['site_id']."&path=".$GLOBALS['path']."&action=unzip&file='+escape(SelectedRowID), 800, 600);", 31, "{%langstr:unzip%}");

      if($GLOBALS['use_file_keywords_search'])
      {
        $Toolbar->AddSeperator();
        $Toolbar->AddButtonImage("keywords", 'files_edit_keywords', "{%langstr:toolbar_keywords%}", "", "if (SelectedRowID) openDialog('?module=files&site_id=".$GLOBALS['site_id']."&path=".$GLOBALS['path']."&action=keywords&file='+escape(SelectedRowID), 500, 600);", 31, "{%langstr:toolbar_file_keywords_edit%}");
      }
      $Toolbar->AddSeperator();

      $p = explode('/', $path);
      if ($p[0] == 'trash')
        $Toolbar->AddButtonImage('restore', 'view-refresh.png', "{%langstr:toolbar_restore_files%}", "javascript:restoreClick();void(0);", "", 31, "{%langstr:toolbar_restore_pages%}", "");

      $Toolbar->AddButtonImage("del", 'delete', "{%langstr:col_del%}", "javascript:
      if (window.fff || SelectedRowID)
          if (confirm('{%langstr:toolbar_delete_selected_files%}'))
      if (window.fff) window.location='?module=files&action=delete&path=".$GLOBALS['path']."&file='+window.fff+'&site_id=".$GLOBALS['site_id']."';
      else if (SelectedRowID) window.location='?module=files&action=delete&path=".$GLOBALS['path']."&file='+SelectedRowID+'&site_id=".$GLOBALS['site_id']."';", '', 31, "{%langstr:toolbar_files_del%}");

      $Toolbar->AddSeperator();

      $trash_files = glob(getFilePathFromLink($GLOBALS['site_id'], '/trash').'/*');
      $Toolbar->AddButtonImage('trash', 'trashcan_'.($trash_files ? 'full' : 'empty').'.png', "{%langstr:toolbar_files_trash%}", "?module=files&site_id=" . $GLOBALS['site_id'] . "&path=trash", "", 31, "{%langstr:toolbar_files_trash%}", "");

      $Toolbar->AddButtonImage("help", 'help', "{%langstr:toolbar_help%}", $GLOBALS['cfgWebRoot']."help/help.htm?category=faili", "", 31, "","","_blank");

      $docStrings['toolbar'] =  $Toolbar->output();




    }
//document.location='?module=files&site_id=".$GLOBALS['site_id']."&action=docopy&close=no';
    $docTemplate = "main.htm";
    $docTitle = "{%langstr:file_explorer%}";
    $docScripts['body'] = "files.default.php";
    break;
//
//arr = showModalDialog( "?module=dialogs&action=filex&site_id='.$this->site_id.'", "", "font-family:Verdana; font-size:12; dialogWidth:530px; dialogHeight:370px; help:0; status:0");
}
