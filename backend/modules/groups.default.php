<?

$perm_managegroups = $GLOBALS['perm_managegroups'];
$perm_managesitegroups = $GLOBALS['perm_managesitegroups'];
$site_id = $GLOBALS['site_id'];

// Let's try table generator
$cols = Array(

  "groupname"   => Array(
    "width"     => "30%",
    "title"     => "{%langstr:col_group_name%}",
    "format"    => "<a href=\"#\" OnClick=\"javascript:openDialog('?module=groups&action=properties_form&site_id=$site_id&id={%group_id%}', 400, 500, 0, 1);\">{%groupname%}</a>"
  ),

  "description" => Array(
    "width"     => "65%",
    "title"     => "{%langstr:col_description%}"
  )

);

if((!$perm_managegroups)and(!$perm_managesitegroups))
{
  unset($cols['groupname']['format']);
}


if($site_id)
{
  $data = sqlQueryData("SELECT group_id,groupname,description FROM groups WHERE site_id='$site_id'");
}else
{
  $data = sqlQueryData("SELECT group_id,groupname,description FROM groups WHERE site_id=0");
}

require($GLOBALS['cfgDirRoot']."library/"."class.datagrid.php");

$Table = new DataGrid();
$Table->cols = $cols;
$Table->data = $data;
$Table->footer = false;
$Table->rowformat = " onClick=\"javascript:HighLightTR(this, {%group_id%});if(SelectedRowID){ EnableToolbarButtons(che); }else { DisableToolbarButtons(che); }\"";
echo $Table->output();

echo '<script language="JavaScript" type="text/javascript">
/*<![CDATA[*/
che = new Array("props", "del");
DisableToolbarButtons(che);
/*]]>*/
</script>';


?>
