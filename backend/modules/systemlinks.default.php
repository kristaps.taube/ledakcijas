<?php

use Constructor\SystemUrl;

$obj = SystemUrl::getInstance();
$links = SystemUrl::getAll();

if(isset($_POST['save_sys_links']) && isset($_POST['links'])){

    foreach($_POST['links'] as $key => $lang_links){
        foreach($lang_links as $lang => $link){

            if(isset($links[$key][$lang])){

                if($links[$key][$lang] != $link){
                    $links[$key][$lang] = $link;
                    $obj->updateLink($key, $lang, $link);
                }

            }

        }
    }

    echo "<div class='sys_links_saved'>Saglabāts</div>";

}


$definitions = $obj->getDefinitions();

$langs = getLanguages();



#echo "<pre>".print_r($links, true)."</pre>";

?>
<link rel="stylesheet" type="text/css" href="/cms/backend/gui/sys_links.css">
<form id='SystemLinkAdministration' action='' method='post'>
    <h1>Sistēmas linku administrēšana</h1>
    <table>
        <?php foreach($definitions as $key => $definition){ ?>
        <tbody>
            <tr>
                <td colspan='2' class='label'><strong><?php echo $definition['label']?></strong></td>
            </tr>
            <?php foreach($langs as $lang => $langdata){?>
            <tr>
                <td><?php echo strtoupper($lang)?>:</td>
                <td><input type='text' name='links[<?php echo $key; ?>][<?php echo $lang; ?>]' value='<?php echo htmlspecialchars(isset($links[$key][$lang]) ? $links[$key][$lang] : SystemUrl::getDefault($key, $lang))?>' /></td>
            </tr>
            <?php } ?>
        </tbody>
        <?php } ?>
    </table>
    <input type='submit' name='save_sys_links' value='Saglabāt' />
</form>