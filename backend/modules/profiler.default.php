<?
$perm_managesites = $GLOBALS['perm_managesites'];
if(isset($_GET['ordermode']))
  $_SESSION['profiler_ordermode'] = intval($_GET['ordermode']);
if(isset($_GET['filtermode']))
  $_SESSION['profiler_filtermode'] = intval($_GET['filtermode']);

$ordermode = $_SESSION['profiler_ordermode'];
$filtermode = $_SESSION['profiler_filtermode'];

if(!$_GET['profile'])
{

  $cols = Array(

    "title"        => Array(
      "width"     => "50%",
      "title"     => "Start time",
      "format"    => "<a href=\"#\" OnClick=\"javascript:window.location='?module=profiler&profile={%profilestart%}'\">{%starttime%}</a>"

    ),

    "entries"    => Array(
      "width"     => "25%",
      "title"     => "Entries"
    ),

    "maxtime"     => Array(
      "width"     => "25%",
      "title"     => "Max deltatime",
    ),

  );

  echo '<a href="?module=profiler&action=truncate">Delete all data</a>&nbsp;&nbsp;&nbsp;&nbsp;';
  echo '<br /><br />';


  $data = sqlQueryData("SELECT profilestart,FROM_UNIXTIME(profilestart) as starttime,COUNT(*) AS entries, MAX(deltatime) AS maxtime FROM profiler GROUP BY profilestart ORDER BY profilestart DESC LIMIT 50");

  require($GLOBALS['cfgDirRoot']."library/"."class.datagrid.php");

  $Table = new DataGrid();
  $Table->cols = $cols;
  $Table->data = $data;
  $Table->footer = false;
  //$Table->rowformat = " onClick=\"javascript:HighLightTR(this, 0);if(SelectedRowID){ EnableToolbarButtons(che); }else { DisableToolbarButtons(che); }\"";
  $Table->rowformat = " onClick=\"javascript:HighLightTR(this, {%site_id%});if(SelectedRowID){ EnableToolbarButtons(che); }else { DisableToolbarButtons(che); }\"";
  echo $Table->output();
}else
{
  $profile = $_GET['profile'];
  $cols = Array(

    "action"        => Array(
      "width"     => "70%",
      "title"     => "Action",

    ),

    "time"    => Array(
      "width"     => "15%",
      "title"     => "Time"
    ),

    "deltatime"     => Array(
      "width"     => "15%",
      "title"     => "Deltatime",
    ),

    "component"     => Array(
      "width"     => "25%",
      "title"     => "Component",
    ),

  );


  echo '<a href="?module=profiler">&lt;&lt; Back</a>&nbsp;&nbsp;&nbsp;&nbsp;';

  if(!$ordermode)
  {
    echo '<a href="?module=profiler&profile='.$profile.'&ordermode=1">Order by deltatime</a>&nbsp;&nbsp;&nbsp;&nbsp;';
    $order = '';
  }else
  {
    echo '<a href="?module=profiler&profile='.$profile.'&ordermode=0">Don\'t order</a>&nbsp;&nbsp;&nbsp;&nbsp;';
    $order = ' ORDER BY deltatime DESC';
  }

  if($filtermode != 0)
  {
    echo '<a href="?module=profiler&profile='.$profile.'&filtermode=0">Show all actions</a>&nbsp;&nbsp;&nbsp;&nbsp;';
  }else
    echo 'Show all actions&nbsp;&nbsp;&nbsp;&nbsp;';
  if($filtermode != 1)
  {
    echo '<a href="?module=profiler&profile='.$profile.'&filtermode=1">Show only queries</a>&nbsp;&nbsp;&nbsp;&nbsp;';
  }else
    echo 'Show only queries&nbsp;&nbsp;&nbsp;&nbsp;';
  if($filtermode != 2)
  {
    echo '<a href="?module=profiler&profile='.$profile.'&filtermode=2">Show only components</a>&nbsp;&nbsp;&nbsp;&nbsp;';
  }else
    echo 'Show only components&nbsp;&nbsp;&nbsp;&nbsp;';
  if($filtermode == 0)
    $filter = ' AND action_type IN (2, 4, 6, 8)';
  else if($filtermode == 1)
    $filter = ' AND action_type IN (2)';
  else if($filtermode == 2)
    $filter = ' AND action_type IN (4, 6)';

  $data = sqlQueryData("SELECT action, time, deltatime, component FROM profiler WHERE profilestart = '$profile'" . $filter . $order);
  foreach($data as $key => $row)
  {
    $data[$key]['action'] = wordwrap($data[$key]['action'], 160, ' ', 170);
  }


  echo '<br /><br />';

  require($GLOBALS['cfgDirRoot']."library/"."class.datagrid.php");

  $Table = new DataGrid();
  $Table->cols = $cols;
  $Table->data = $data;
  $Table->footer = false;
  $Table->rowformat = " onClick=\"javascript:HighLightTR(this, {%site_id%});if(SelectedRowID){ EnableToolbarButtons(che); }else { DisableToolbarButtons(che); }\"";
  echo $Table->output();
}


?>
