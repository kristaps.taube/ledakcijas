<?
  $perm_manageusers = $GLOBALS['perm_manageusers'];
  $perm_managesiteusers = $GLOBALS['perm_managesiteusers'];

  //Check if all IDs in the array a can be found in groups table
  function ValidateGroupIDs($a)
  {
    $s = '';
    for($f = 0; $f < count($a); $f++)
    {
      if($s) $s .= ',';
      $s .= "WHERE group_is=" . $a[$f];
    }
    if($s)
    {
      $count = sqlQueryValue("SELECT count(*) FROM groups " . $s);
    }
    else
    {
      $count = 0;
    }
    return ($count == count($a));
  }

  //Remove current user-group relations, add the new ones
  function AddUserToGroups($userID, $groupID)
  {
    if(!in_array(1, $groupID))
    {
      $groupID[] = 1;
    }
    sqlQuery("DELETE FROM users_groups WHERE user_id=$userID ");
    for ($f = 0; $f < count($groupID); $f++)
    {
      if($groupID[$f])
      {
        sqlQuery("INSERT INTO users_groups (user_id, group_id) VALUES ('$userID', '$groupID[$f]') ");
      }
    }
  }
  
  
  if((($action == 'adduser')and(($perm_manageusers)or($perm_managesiteusers)))or(($action == 'moduser')and(($perm_manageusers)or($perm_managesiteusers))))
  {
    $username = $_POST['username'];
    $addresses = $_POST['addresses'];
    $usertype = $_POST['usertype'];
    if($usertype){
      $enabled = 1;
    }else{
      $enabled = 0;
    }
    if($usertype == 1)
      $visualeditonly = 1;
    else
      $visualeditonly = 0;
    $fname = $_POST['fname'];
    $lname = $_POST['lname'];
    $email = $_POST['email'];
    $phone = $_POST['phone'];
    $notes = $_POST['notes'];
    $user_id = $_POST['user_id'];
    $site_id = $_GET['site_id'];
    if($_POST['password']) $password = $_POST['password'];
    if($_POST['password2']) $password2 = $_POST['password2'];
    $groups = $_POST['groups'];
    $groups_array = explode(',', $groups);
    //Check if field input is valid
    $invalid = Array();
    $jsmessage = '';
    if((IsEmpty($username))or($baduser = IsBadSymbols($username))or($existsuser = sqlQueryValue("SELECT user_id FROM users WHERE username='$username' AND user_id<>'$user_id'")))
    {
      $invalid[] = "username";
      if($baduser)
        $jsmessage .= 'Username ' . msgInvalidChars . '\n';
      if($existsuser)
        $jsmessage .= 'Username ' . msgUserExists . '\n';
    }
    if((IsEmpty($password))and($action=='adduser'))
    {
      $invalid[] = "password";
    }
    if(IsLtGtSymbols($fname))
    {
      $invalid[] = "fname";
      $jsmessage .= 'First name ' . msgNoLtGt . '\n';
    }
    if(IsLtGtSymbols($lname))
    {
      $invalid[] = "lname";
      $jsmessage .= 'Last name ' . msgNoLtGt . '\n';
    }
    if(IsLtGtSymbols($email))
    {
      $invalid[] = "email";
      $jsmessage .= 'E-mail ' . msgNoLtGt . '\n';
    }
    if($password != $password2)
    {
      $invalid[] = "password";
      $invalid[] = "password2";
      $jsmessage .= 'Passwords don\'t match' . '\n';
    }
    if(count($invalid))
    {
      if($action == 'adduser')
        $action = 'create';
      if($action == 'moduser')
        $action = 'properties_form';
      $invalid = join(':', $invalid);
      $username = urlencode(stripslashes($username));
      $addresses = urlencode(stripslashes($addresses));
      $enabled = $enabled;
      $fname = urlencode(stripslashes($fname));
      $lname = urlencode(stripslashes($lname));
      $email = urlencode(stripslashes($email));
      $phone = urlencode(stripslashes($phone));
      $notes = urlencode(stripslashes($notes));
      $groups = urlencode(stripslashes($groups));
      Redirect('?module='.$module.'&action='.$action.'&invalid=' . $invalid . '&formdata_username='.$username.'&formdata_addresses='.$addresses.'&formdata_usertype='.$usertype.'&formdata_fname='.$fname.'&formdata_lname='.$lname.'&formdata_email='.$email.'&formdata_phone='.$phone.'&formdata_notes='.$notes.'&formdata_groups='.$groups.'&jsmessage='.$jsmessage.'&site_id='.$site_id.'&id='.$user_id);
      Die();
    }
  }

	$password = encode_password($password);

  if(($action == 'adduser')and(($perm_manageusers)or($perm_managesiteusers)))
  {

    sqlQuery("INSERT INTO users (username,password,addresses,enabled,visualeditonly,fname,lname,email,phone,notes,site_id) VALUES ('$username','$password','$addresses','$enabled','$visualeditonly','$fname','$lname','$email','$phone','$notes','$site_id')");
    $id = sqlQueryValue("SELECT user_id FROM users WHERE username='$username'");
    AddUserToGroups($id, $groups_array);
    if($site_id)
    { //for site users allow them to modify site contents
      sqlQuery("INSERT INTO permissions (perm_num, isuser, id, data) VALUES (6, 1, $id, $site_id)");
    }
    Add_Log("New user '$username' ($id)", $site_id);
    Header("Location: ?module=permissions&action=permission_frame&mod=site_form&hideitems=1&close=1&site_id=$site_id&data=$site_id&switchuser=$id&data2=&must_resize=1");
    Die();

  }else if(($action == 'moduser')and(($perm_manageusers)or($perm_managesiteusers)))
  {
    sqlQuery("UPDATE users SET username='$username', addresses='$addresses', enabled='$enabled', visualeditonly='$visualeditonly', fname='$fname', lname='$lname', email='$email', phone='$phone', notes='$notes' WHERE user_id='$user_id'");
    if($password) sqlQuery("UPDATE users SET password='$password' WHERE user_id='$user_id'");
    AddUserToGroups($user_id, $groups_array);
    Add_Log("Updated user '$username' ($user_id)", $site_id);

  }else if(($action == 'deluser')and(($perm_manageusers)or($perm_managesiteusers)))
  {
    $user_id = $_GET['id'];
    list($cantdelete, $username) = sqlQueryRow("SELECT cantdelete, username FROM users WHERE user_id=$user_id");
    //don't allow to delete the main admin user
    if(($user_id>1)and(!$cantdelete))
    {
      sqlQuery("DELETE FROM users_groups WHERE user_id=$user_id");
      sqlQuery("DELETE FROM users WHERE user_id=$user_id");
      sqlQuery("DELETE FROM permissions WHERE id=$user_id AND isuser=1");
      $site_id = $_GET['site_id'];
      Add_Log("Deleted user '$username' ($user_id)", $site_id);
      
    }
  }
?>
