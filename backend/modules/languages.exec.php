<?

  $site_id = $GLOBALS['site_id'];

  if(($action == 'addlanguage')or($action == 'modlanguage'))
  {
    $shortname = $_POST['shortname'];
    $fullname = $_POST['fullname'];
    $encoding = 'utf-8';
    $language_id = $_POST['language_id'];
    $default = $_POST['default'] ? 1 : 0;
    $disabled = $_POST['disabled'] ? 1 : 0;
    //Check if field input is valid
    $invalid = Array();
    $jsmessage = '';
    if((IsEmpty($shortname))or(IsBadSymbols($shortname))or(sqlQueryValue("SELECT language_id FROM " . $site_id . "_languages WHERE shortname='$shortname' AND language_id<>'$language_id'")))
    {
      $invalid[] = "shortname";
      if(!IsEmpty($shortname))
        $jsmessage .= 'Short language name ' . msgValidCharsUnique . '\n';
    }
    if((IsEmpty($fullname))or(IsBadSymbols($fullname))or(sqlQueryValue("SELECT language_id FROM " . $site_id . "_languages WHERE fullname='$fullname' AND language_id<>'$language_id'")))
    {
      $invalid[] = "fullname";
      if(!IsEmpty($fullname))
        $jsmessage .= 'Language name ' . msgValidCharsUnique . '\n';
    }

    if(count($invalid))
    {
      if($action == 'addlanguage')
        $action = 'create';
      if($action == 'modlanguage')
        $action = 'properties_form';
      $invalid = join(':', $invalid);
      $shortname = urlencode(stripslashes($shortname));
      $fullname = urlencode(stripslashes($fullname));
      $encoding = urlencode($encoding);
      $disabled = urlencode($disabled);
      $default = urlencode($default);

      Redirect('?module='.$module.'&action='.$action.'&invalid=' . $invalid . '&formdata_shortname='.$shortname . '&formdata_fullname='.$fullname . '&formdata_encoding='.$encoding .'&formdata_disabled='.$disabled.'&formdata_default='.$default. '&jsmessage='.$jsmessage.'&id='.$language_id.'&site_id='.$site_id);
      Die();
    }
  }


  if(($action == 'addlanguage'))
  {
    sqlQuery("INSERT INTO " . $site_id . "_languages (shortname,fullname,encoding,disabled,`is_default`) VALUES ('$shortname','$fullname','$encoding','$disabled','$default')");
    $language_id = sqlQueryValue("SELECT language_id FROM " . $site_id . "_languages WHERE fullname='$fullname'");
    Add_Log("New language '$fullname' ($language_id)", $site_id);
    DeleteAllCompiledTemplates($site_id);

  }else if(($action == 'modlanguage'))
  {
    sqlQuery("UPDATE " . $site_id . "_languages SET shortname='$shortname', fullname='$fullname', encoding='$encoding',`is_default`=$default,disabled=$disabled WHERE language_id='$language_id'");
    Add_Log("Updated language '$fullname' ($language_id)", $site_id);
    DeleteAllCompiledTemplates($site_id);
    unset($_SESSION['currentlanguage']);
    unset($_SESSION['currentlanguagenum']);

  }else if(($action == 'dellanguage'))
  {
    $language_id = $_GET['id'];
    $used = sqlQueryValue("SELECT page_id FROM " . $site_id . "_pages WHERE language=$language_id");
    if($used==null)
    {
      $fullname = sqlQueryValue("SELECT fullname FROM " . $site_id . "_languages WHERE language_id=$language_id");
      sqlQuery("DELETE FROM " . $site_id . "_languages WHERE language_id=$language_id");
      Add_Log("Deleted language '$fullname' ($language_id)", $site_id);

      unset($_SESSION['currentlanguage']);
      unset($_SESSION['currentlanguagenum']);
    }
  }else if(($action == 'setdefault'))
  {
    $language_id = $_GET['id'];
    sqlQuery("UPDATE " . $site_id . "_languages SET is_default = 0");
    sqlQuery("UPDATE " . $site_id . "_languages SET is_default = 1 WHERE language_id=" . $language_id);
  }

