<?

  require_once ($GLOBALS['cfgDirRoot']."library/".'func.search.php');

   if($_GET['mustrefresh'])
   {

      $page_id = $_GET['page_id'];
      $site_id = $_GET['site_id'];

      $GLOBALS['PageEncoding'] = PageEncoding($page_id, $site_id);
      $GLOBALS['maincharset']  = PageEncoding($page_id, $site_id, false);

       echo '

       <form name="HiddenForm" id="HiddenForm" action="?module=dialogs&action=refreshsinglecomponent&page_id='.$_GET['page_id'].'&pagedev_id='.$_GET['pagedev_id'].'&site_id='.$_GET['site_id'].'&savemode='.$_GET['savemode'].'&paramstr='.urlencode(stripslashes($_GET['paramstr'])).'&afterrefreshmethod='.$_GET['afterrefreshmethod'].'" method="POST">
         <input type="hidden" name="newvalue" value="">
         <input type="hidden" name="component_type" value="'.$_GET['component_type'].'">
         <input type="hidden" name="component_name" value="'.$_GET['component_name'].'">
         <input type="hidden" name="newproperty" value="'.$_GET['newproperty'].'">
       </form>

    <script language="JavaScript">

<!--

  if(window.parent.dialogArguments[1])
  {
    var win = window.parent.dialogArguments[0];
    var el = window.parent.dialogArguments[1];
    newvalue = el.thevalue;
    document.getElementById("HiddenForm").newvalue.value = newvalue;
  }else
  {
    document.getElementById("HiddenForm").newvalue.value = window.parent.dialogArguments;
  }

  document.getElementById("HiddenForm").submit();

//-->

</script> ';

   }else
   {

    $component_type = $_POST['component_type'];
    $component_name = $_POST['component_name'];
    $page_id = $_GET['page_id'];
    $pagedev_id = $_GET['pagedev_id'];
    $GLOBALS['pagedev_id'] = $pagedev_id;
    $site_id = $_GET['site_id'];
    $savemode = intval($_GET['savemode']);

    if (!$pagedev_id)
    {
      $pagedev_id = sqlQueryValue("SELECT pagedev_id FROM ".$site_id."_pagesdev WHERE page_id=".$page_id." ORDER BY lastmod DESC LIMIT 1");
      $GLOBALS['pagedev_id'] = $pagedev_id;
    }

    $GLOBALS['PageEncoding'] = PageDevEncoding($pagedev_id, $site_id);
    $GLOBALS['maincharset']  = PageDevEncoding($pagedev_id, $site_id, false);


    if($_POST['newproperty'])
    {
//      $val = $_POST['newvalue'];
//      while(ereg('%u0([[:alnum:]]{3})', $val, $regs))
//      {
//        $val = str_replace($regs[0], code2utf(intval($regs[1])), $val);
//      }
      //$val = ereg_replace('%u0([[:alnum:]]{3})', '&#x\1;',$_POST['newvalue']);
      $val = $_POST['newvalue'];

      if(!$savemode)
      {
        setProperty($_POST['newproperty'], $val, $component_name, $pagedev_id, $site_id, 1);

        //update modification date and user
        $modby = $GLOBALS['currentUserID'];
        sqlQuery("UPDATE ".$site_id."_pagesdev SET lastmod='".time()."', modby=$modby WHERE pagedev_id=$pagedev_id");
        if (CheckPermission(31, $site_id, $page_id) || CheckPermission(32, $site_id))
        {
            pageautopublish($site_id, $pagedev_id);
        }
      }else if($savemode == 2)
      {
        setProperty($_POST['newproperty'], $val, $component_name, -1, $site_id, 0);
        DeleteAllCompiledTemplates($site_id);
      }else if($savemode == 1)
      {
        //save to language
        $language_id = intval($_SESSION['currentlanguagenum']);
        $template_id = sqlQueryValue("SELECT template FROM ".$site_id."_pagesdev WHERE pagedev_id=".$pagedev_id." ORDER BY lastmod DESC LIMIT 1");
        if($language_id)
        {
          //does view exist?
          $row = sqlQueryRow('SELECT * FROM ' . $site_id . '_views WHERE template_id = ' . $template_id . ' AND language_id = ' . $language_id);
          if(!$row)
          {
            //create new autogenerated view
            $maxid = sqlQueryValue("SELECT max(view_id) FROM ".$site_id."_views");
            if($maxid)
              $newid = '';
            else
              $newid = '2';
            sqlQuery("INSERT INTO `".$site_id."_views` (`view_id`, `ind` , `template_id` , `name` , `parent` , `description` , `levels` , `parents` , `languages` , `language_id` )
                      VALUES (
                      '".$newid."', '0', '".$template_id."', 'autolanguage_".$template_id."_".$language_id."', '0', '', '', '', '', '".$language_id."'
                      )");
            $row = sqlQueryRow('SELECT * FROM ' . $site_id . '_views WHERE template_id = ' . $template_id . ' AND language_id = ' . $language_id);
          }
        }
        setProperty($_POST['newproperty'], $val, $component_name, -$row['view_id'], $site_id, 0);
        DeleteAllCompiledTemplates($site_id);
      }

    }


    if(!$_GET['afterrefreshmethod'])
    {
      $newDiv = outputGraphicalComponent($hascontent, $component_type, $component_name, $page_id, 1, stripslashes($_GET['paramstr']), false, $toolbarfunc, false, $pagedev_id);
//      $newDiv = borderDiv() . $newDiv;
    }else
    {
      $aftermethod = $_GET['afterrefreshmethod'];
      ob_start();
      include_once($GLOBALS['cfgDirRoot'] . 'components/class.' . $component_type . '.php');
      $obj = new $component_type($component_name);
      $obj->componentParamStr = stripslashes($_GET['paramstr']);
      $obj->dev = true;
      $obj->$aftermethod();
      $newDiv = ob_get_contents();
      ob_end_clean();
    }
    $newDiv = parseForJScript($newDiv);

    $obj = new $component_type($component_name);
    addDefaultProperties($obj);
    if($obj->getProperty('visible') == 0)
    {
      $displaymode = 'block';
      $optcol = '800000';
    }else
    {
      $displaymode = 'none';
      $optcol = '0000FF';
    }

  echo '
    <script language="JavaScript" src="'.$GLOBALS['cfgWebRoot'].'gui/jquery.min.js"></script>
    <script language="JavaScript">
<!--

  try
  {
    var win = window.parent.dialogArguments[0];
    var el = window.parent.dialogArguments[1];
    el.thevalue = "' . $newDiv . '";
//    el.innerHTML = el.thevalue;
    $(el).html(el.thevalue);
    el.set_tb_cnt = "' . IIF($toolbarfunc, ParseForJScript($toolbarfunc), 'null') . '";
    el.style.display = "' . $displaymode . '";

//    console.log(el.firstChild.tagName + " " + el.firstChild.id);
//    console.log(el.firstChild);
//    console.log(el.firstChild.parentNode);

  }catch(e)
  {
  }

  window.parent.close();

//-->

</script>';
 }

?>

