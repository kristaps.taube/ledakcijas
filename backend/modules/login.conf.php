<?
//Login.Conf
//Aivars Irmejs

/*
if ($_GET['t'])
{
  $data = sqlQueryData("show databases like 'demo_datateks_lv_-_mainmini_%'");
  foreach ($data as $row)
  {
    sqlQuery('DROP DATABASE `'.$row[0].'`');
    echo $row[0].'<br/>';
  }

}
*/

$action = $_GET["action"];

$admin_mode = ($_GET['mode'] == 'admin');

$GLOBALS['admin_mode'] = $admin_mode ? 1 : 0;
$GLOBALS['admin_param'] = $admin_mode ? '&mode=admin' : '';
$GLOBALS['password_attribs'] = $admin_mode ? ' style="display:none"' : '';
$GLOBALS['login_username'] = htmlspecialchars(stripslashes($_POST[$admin_mode ? 'url' : 'username']));
$GLOBALS['username_label'] = $admin_mode ? 'URL' : '{%langstr:username%}';

$docStrings['langselect'] = '';

  $docStrings['langselect'] = '
    <div class="demolangsel">
      <a href="?module=login&set_language=lv'.$GLOBALS['admin_param'].'"><img src="'.$GLOBALS['cfgWebRoot'].'gui/images/flags/lv.gif" alt="" /> Latviski</a>
      <a href="?module=login&set_language=ru'.$GLOBALS['admin_param'].'"><img src="'.$GLOBALS['cfgWebRoot'].'gui/images/flags/ru.gif" alt="" /> Русский</a>
      <a href="?module=login&set_language=en'.$GLOBALS['admin_param'].'"><img src="'.$GLOBALS['cfgWebRoot'].'gui/images/flags/gb.gif" alt="" /> English</a>
    </div>
  ';

function login_redirectToCMS()
{
  global $redirectUrl, $domain, $site_id;

  $redirectUrl = "index.php?module=" . defaultModule;

  //in case it comes from pages
  if(($GLOBALS['site_id']) and (!$GLOBALS['domain']))
  {
    $GLOBALS['domain'] = sqlQueryValue("select domain from sites where site_id=".$_SESSION['userdata']['site_id']);
  }

  if ($GLOBALS['site_id'])
    {
      $domain = $GLOBALS['domain'];
      $site_id = $GLOBALS['site_id'];
    }
  else if ($_SESSION['userdata']['site_id'])
    {
      $domain = sqlQueryValue("select domain from sites where site_id=".$_SESSION['userdata']['site_id']);
      $site_id = $_SESSION['userdata']['site_id'];
      $GLOBALS['domain'] = $domain;
      $GLOBALS['site_id'] = $site_id;
    }
  if($GLOBALS['currentUserSiteID'][$domain])
      if ($GLOBALS['custommodule']!="")
          $redirectUrl = "index.php?module=" . $GLOBALS['custommodule'][$domain]."&site_id=".$site_id."&cmodule=1";
      else
        $redirectUrl = "index.php?site_id=" . $GLOBALS['currentUserSiteID'] . "&module=" . defaultSiteModule;

  header("location:".$redirectUrl);
	die();

}

require_once $GLOBALS['cfgDirRoot']."library/Auth/OpenID/Consumer.php";
require_once $GLOBALS['cfgDirRoot']."library/Auth/OpenID/FileStore.php";

function login_getConsumer()
{
  $dir1 = $GLOBALS['cfgDirRoot'].'openid';
  $dir2 = $GLOBALS['cfgDirRoot'].'openid/store';

  if (!file_exists($dir1))
  {
    @mkdir($dir1, 0755);
    @chmod($dir1, 0755);
  }
  if (!file_exists($dir2))
  {
    @mkdir($dir2, 0755);
    @chmod($dir2, 0755);
  }

  $store_path = $dir2;

  if (!file_exists($store_path))
    return null;

  $store = new Auth_OpenID_FileStore($store_path);

  return new Auth_OpenID_Consumer($store);
}

$openid_trust_root = 'http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
$openid_return_to = $GLOBALS['cfgWebRoot'].'?module=login&action=login&mode=admin&openid_finish=1';


function login_tryAuth($consumer, $openid)
{
  global $openid_trust_root, $openid_return_to;

  if (!$consumer)
    return 'OpenID configuration error (file store not available?)';

  $auth_request = $consumer->begin($openid);

  // No auth request means we can't begin OpenID.
  if (!$auth_request)
    return "Authentication error; not a valid OpenID.";

  $auth_request->endpoint->server_url .= '?openid_user_group='.$GLOBALS['openid_user_group'];

  if ($auth_request->shouldSendRedirect()) {
      $redirect_url = $auth_request->redirectURL($openid_trust_root, $openid_return_to);

      // If the redirect URL can't be built, display an error
      // message.
      if (Auth_OpenID::isFailure($redirect_url)) {
          return "Could not redirect to server: " . $redirect_url->message;
      } else {
          // Send redirect.
          header("Location: ".$redirect_url);
      }
  } else {
      // Generate form markup and render it.
      $form_id = 'openid_message';
      $form_html = $auth_request->htmlMarkup($openid_trust_root, $openid_return_to,
                                             false, array('id' => $form_id));

      // Display an error if the form markup couldn't be generated;
      // otherwise, render the HTML.
      if (Auth_OpenID::isFailure($form_html)) {
          return "Could not redirect to server: " . $form_html->message;
      } else {
          print $form_html;
      }
  }

  return null;
}

switch ($action) {

  // site properties dialog box
  case "login":

    if ($_GET['mode'] == 'admin')
    {
      $docTemplate = "login.htm";

      if ($_POST["url"])
      {
        if(count(explode(".",$_POST['url'])) == 1){
          $_POST['url'] .= ".".$GLOBALS['openid_server'];
        }

        $username = trim($_POST['url']);
        if (substr($username, 0, 4) != 'http')
          $username = 'http://'.$username;

        $suffix = '.id.serveris.lv';
//        $suffix = '.myopenid.com';
        $host_suffix = substr(parse_url($username, PHP_URL_HOST), -strlen($suffix));

        $fail = true;
        $err = 'Invalid URL';
        if ($host_suffix == $suffix)
        {
          $consumer = login_getConsumer();

          $e = login_tryAuth($consumer, $username);
          if ($e)
            $err = $e;
          else
          {
            $fail = false;
            die;
          }

        }

        if ($fail)
          $docStrings['message'] = $err;

      }
      else if($_GET['openid_mode'] == 'id_res')
      {

        $consumer = login_getConsumer();
        $response = $consumer->complete($openid_return_to);
        $valid = false;

        if ($response)
        {
          $openid = $response->getDisplayIdentifier();

          // Check the response status.
          if ($response->status == Auth_OpenID_CANCEL) {
              // This means the authentication was cancelled.

          } else if ($response->status == Auth_OpenID_FAILURE) {
              // Authentication failed; display the error message.
              $docStrings['message'] = "OpenID authentication failed: " . $response->message;
          } else if ($response->status == Auth_OpenID_SUCCESS) {
              // This means the authentication succeeded; extract the
              // identity URL and Simple Registration data (if it was
              // returned).
              $valid = true;
          }

        }

        if ($valid)
        {
          login_successful('Admin', '', $openid);
          $_SESSION['admin_logged_in'] = true;
          login_redirectToCMS();
        }
        else
        {
          login_failed('Admin', $openid);
        }

      }

    }else if(log_in($_POST["username"], $_POST["password"], $_SERVER['REMOTE_ADDR'])){

      $site_id = $GLOBALS['site_id'] ? $GLOBALS['site_id'] : $_SESSION['userdata']['site_id'];

      if($GLOBALS['cfgMini'] && $GLOBALS['cfgMiniDemo'] && strtolower($_POST["username"]) != 'admin')
      {
        //delete old databases
        $data = sqlQueryData(
          "SELECT * FROM dbcopy
           WHERE `created` < " . (time() - 4*60*60) . "
             AND `deleted` = 0");

        foreach($data as $row)
        {
          sqlQuery("DROP DATABASE IF EXISTS `" . $row['name'] . "`");
          sqlQuery("UPDATE dbcopy SET `deleted` = 1 WHERE id = " . $row['id']);
          $dir = getFilePathFromLink($site_id, 'upload/user'.$row['db_id']);
          recursive_remove_directory($dir);
        }

        $handle = popen('/usr/bin/sudo /usr/bin/dbcp', 'r');
        $dbname = fread($handle, 2096);
        pclose($handle);

        $dbname = trim($dbname);
        $_SESSION['demo_db_name'] = $dbname;

        $a = explode('_', $dbname);
        $_SESSION['demo_db_id'] = $a[count($a)-1];

        //copy DB
        sqlQuery("CREATE TABLE IF NOT EXISTS `dbcopy` (
                    `id` int(11) NOT NULL auto_increment,
                    `name` varchar(255) NOT NULL default '',
                    `created` int(11) NOT NULL default '0',
                    `ip` varchar(20) NOT NULL default '',
                    `deleted` tinyint(1) NOT NULL default '0',
                    `db_id` INT(10) NOT NULL,
                    PRIMARY KEY  (`id`)
                  )");
        sqlQuery("INSERT INTO dbcopy (`name`, `created`, `ip`, `db_id`)
                    VALUES ('". $dbname . "', '" . time() . "',
                    '" . $_SERVER['REMOTE_ADDR'] . "', ".$_SESSION['demo_db_id'].")");

        $dir = getFilePathFromLink($site_id, 'upload/user'.$_SESSION['demo_db_id']);
        mkdir($dir);

        if (!is_dir($dir))
        {
          die('Can\'t create directory: '.$dir);
        }
        chmod($dir, 0755);
      }

      if($_SESSION['userdata']['visualeditonly'])
      {  //user can only access visual editor
        $site_id = $GLOBALS["currentUserSiteID"];
        $docTemplate = "login.htm";
        $docStrings['message'] = "
        <script language=\"JavaScript\" type=\"text/javascript\">
        /*<![CDATA[*/
          openDialog('?module=pages&site_id=" . $site_id . "&action=components_visual_frame&id=0', screen.availWidth, screen.availHeight-25, 1 , 1);
        /*]]>*/
        </script>
        ";
        $redirectUrl = "";
      }else
      {  //user can log in constructor
        login_redirectToCMS();
      }
    }else{
      $docTemplate = "login.htm";
      $docStrings['message'] = $GLOBALS['loginfailmessage'];
      //echo md5($_POST['password']);
    }
    break;

  case "logout":
    log_out();
    $docTemplate = "login.htm";
    $docStrings['message'] = 'Logged out';
    break;

  default:
    $docTemplate = "login.htm";
    $docStrings['message'] = '';
    break;

}

?>
