<?

$perm_managefiles = $GLOBALS['perm_managefiles'];

$cols = Array(

  "name"        => Array(
    "width"     => "45%",
    "title"     => "{%langstr:col_name%}"
  ),

  "size"    => Array(
    "width"     => "10%",
    "title"     => "{%langstr:col_size%}"
  ),

  "date"     => Array(
    "width"     => "35%",
    "title"     => "{%langstr:col_modified%}"
  ),

  "del"    => Array(
    "width"     => "5%",
    "title"     => "{%langstr:col_del%}",
    "align"     => "center",
    "format"    => '<a href="?module=files&action=delete&path='.$GLOBALS['path'].'&file={%title%}&site_id='.$GLOBALS['site_id'].'&close=no&view=thumbs" OnClick="return confirm(\'{%langstr:sure_to_delete_item%}\')"><img src="'.$GLOBALS['cfgWebRoot'].'gui/images/ico_del.gif" width="16" height="16" border="0" alt="{%langstr:col_del%}"></a>'
  ),

  "edit"    => Array(
    "width"     => "5%",
    "title"     => "{%langstr:col_edit%}",
    "align"     => "center"
  )
);

if(!$perm_managefiles)
{
  unset($cols['del']);
  unset($cols['edit']);
}
  
function read_files($webroot, $dir, $path) {
  global $cfgCmsDirRoot, $cfgWebRoot;

  function outputFilename($filepath, $webpath, $webdir, $filename) {
    $ext = strtolower(substr($filename,-4,4));
    if ($GLOBALS['view']=='thumbs' || $_GET['view'] == 'thumbs'){
      if ($ext==".gif" || $ext==".jpg" || $ext==".png") {
        $image = image_preview_out($filepath, $webpath, 100, 100);
      } else {
        $image = '<img src="'.$GLOBALS['cfgWebRoot'].'gui/images/ico_file.gif" width="16" height="16" align="absmiddle" border="0">';
      }
      $html = '<center><a href="javascript:;" OnClick="retVal(\'Form\', \'Input\', \'/'.$webdir.'\');">'.$image.'<br><br>'.$filename.'</a></center>';
    } else {
      $html = '<a href="javascript:;" OnClick="retVal(\'Form\', \'Input\', \'/'.$webdir.'\');"><img src="'.$GLOBALS['cfgWebRoot'].'gui/images/ico_file.gif" width="16" height="16" align="absmiddle" border="0"> '.$filename.'</a>';
    }
    return $html;
  }

      if (is_dir($dir)) chdir($dir);

      $handle = opendir($dir);
      while ($file = readdir($handle)) {
        if (is_dir($file) && $file != ".")
          $dirlist[] = $file;
        elseif (is_file($file))
          $filelist[] = $file;
      }
      closedir($handle);

  if ($path != "") $_SESSION['root'] = 1;


  $i = 0;

//directories
if ($_SESSION['root']==1)
{
  asort($dirlist);
  foreach($dirlist as $file) {
    if (!($file == '.htaccess' && $file == '..' && $path == '')) {
      if ($file == '..') $url = fileUp($path); elseif ($path == '') $url = $file; else $url = $path.'/'.$file;
      $thisfile['name'] = '<img src="'.$GLOBALS['cfgWebRoot'].'gui/images/ico_folder.gif" width="16" height="16" align="absmiddle"> <a href="?module=files&action=list&path='.$url.'&site_id='.$GLOBALS['site_id'].'&view='.$GLOBALS['view'].'">'.$file.'</a>';
      $thisfile['title'] = $file;
      $thisfile['size'] = fileSizeStr($file);
      $thisfile['date'] = date("d-m-Y H:i:s", filectime($file));
      //if ($file != '..') $thisfile['del'] = 'x';
      $data[$i++] = $thisfile;
    }
  }
}


//secure dirs
if ($_SESSION['root']==0)
{
    $docroot = sqlQueryValue("SELECT docroot FROM sites WHERE site_id=".$GLOBALS['site_id']);
    //if docroot not set do not show secure dir in dialogs
    if ($docroot)
    {
    $newsecuredir = array("name" => '<img src="'.$GLOBALS['cfgWebRoot'].'gui/images/ico_folder_secure.gif" width="16" height="16" align="absmiddle"> <a href="?module=files&action=list&parent_dir=0&site_id='.$GLOBALS['site_id'].'">Documents</a>',
                        "size" =>fileSizeStr($docroot),
                        "date" => date("d-m-Y H:i:s", $sdir['creation_date']),
                        "title" => "dir:Documents",
                        );
    $data[$i++] = $newsecuredir;
    }
  
      asort($dirlist);
      foreach($dirlist as $file) {
        if (!($file == '..' && $path == '')) {
          if ($file == '..') $url = fileUp($path); elseif ($path == '') $url = $file; else $url = $path.'/'.$file;
          $thisfile['name'] = '<img src="'.$GLOBALS['cfgWebRoot'].'gui/images/ico_folder.gif" width="16" height="16" align="absmiddle"> <a href="?module=files&action=list&path='.$url.'&site_id='.$GLOBALS['site_id'].'&view='.$GLOBALS['view'].'">'.$file.'</a>';
          $thisfile['title'] = $file;
          $thisfile['size'] = fileSizeStr($file);
          $thisfile['date'] = date("d-m-Y H:i:s", filectime($file));
          //if ($file != '..') $thisfile['del'] = 'x';
          $data[$i++] = $thisfile;
        }
      }
}

if ($_SESSION['root']==-1)
{
    $docroot = sqlQueryValue("SELECT docroot FROM sites WHERE site_id=".$GLOBALS['site_id']);
    $dirlist = sqlQueryData("SELECT * FROM ".$GLOBALS['site_id']."_documentdir WHERE parent_directory=".$_SESSION['parent_dir']." ORDER BY ind");

    $parent = sqlQueryValue("SELECT parent_directory FROM ".$GLOBALS['site_id']."_documentdir WHERE dir_id=".$_SESSION['parent_dir']);
    
    if ($_SESSION['parent_dir'])
    {
        $data[$i++] = array("name" => '<img src="'.$GLOBALS['cfgWebRoot'].'gui/images/ico_folder.gif" width="16" height="16" align="absmiddle"> <a href="?module=files&action=list&parent_dir='.$parent.'&site_id='.$GLOBALS['site_id'].'">..</a>',
                        "size" => "",
                        "date" => date("d-m-Y H:i:s", $dir['creation_date']),
                        "title" => ".."
                        );
    }
    else
    {
          $thisfile = array("name" => '<img src="'.$GLOBALS['cfgWebRoot'].'gui/images/ico_folder.gif" width="16" height="16" align="absmiddle"> <a href="?module=files&action=list&path=/&site_id='.$GLOBALS['site_id'].'">..</a>',
                                "size" =>fileSizeStr($docroot),
                                "date" => date("d-m-Y H:i:s", $sdir['creation_date']),
                                "title" => "dir:Documents"
                                );
          $data[$i++] = $thisfile;
    }

    //secure directories
    foreach ($dirlist as $sdir)
    {
        $newsecuredir = array("name" => '<img src="'.$GLOBALS['cfgWebRoot'].'gui/images/ico_folder_secure.gif" width="16" height="16" align="absmiddle"> <a href="?module=files&action=list&parent_dir='.$sdir['dir_id'].'&site_id='.$GLOBALS['site_id'].'">'. $sdir['dir_name'].'</a>',
                        "size" =>fileSizeStr($docroot.$sdir['real_name']),
                        "date" => date("d-m-Y H:i:s", $sdir['creation_date']),
                        "title" => "dir:".$sdir['dir_id'],
                        );
        $data[$i++] = $newsecuredir;
    }
}

if ($_SESSION['root']==0 || $_SESSION['root']==1)
{
  if($filelist)
  {
    asort($filelist);
    foreach($filelist as $file) {
      $ext = strtolower(substr($file,-4,4));
      /*if ($GLOBALS['view']!='thumbs' || ($ext==".gif" || $ext==".jpg" || $ext==".png")) */
      {
        if ($path != "") $path = fileAddBackslash($path);
        $url = $path.$file;
        $thisfile['name'] = outputFilename(fileAddBackslash($dir).$file, fileAddBackslash($webroot).$url, $url, $file);
        $thisfile['title'] = $file;
        $thisfile['size'] = fileSizeStr($file);
        $thisfile['date'] = date("d-m-Y H:i:s", filectime($file));
        if ($ext==".gif" || $ext==".jpg" || $ext==".png" || $ext=="jpeg")
          $thisfile['edit'] = '<a href="javascript:void(0);" OnClick="openDialog(\'?module=wysiwyg&action=editimage&path='.$GLOBALS['path'].'&img='.$url.'&site_id='.$GLOBALS['site_id'].'\');"><img src="'.$GLOBALS['cfgWebRoot'].'gui/images/edit.gif" width="16" height="16" border="0" alt="Edit"></a>';
        $data[$i++] = $thisfile;
        unset($thisfile);
      }
    }
  }
}

//secure files
if ($_SESSION['root']==-1)
{
    $filelist = sqlQueryData("SELECT * FROM ".$GLOBALS['site_id']."_documents WHERE parent_directory=".$_SESSION['parent_dir']." AND deleted=0 ORDER BY ind");

    foreach ($filelist as $file)
    {
        $data[] = array("name" => '<img src="'.$GLOBALS['cfgWebRoot'].'gui/images/ico_file_secure.gif" width="16" height="16" align="absmiddle"> <a href="javascript:;" OnClick="retVal(\'Form\', \'Input\', \'{%documentfile:'.$file['file_id'].'%}\');">'.$file['file_name'].'</a>',
                        "size" => fileSizeStr($docroot.$file['real_name']),
                        "date" => date("d-m-Y H:i:s", $file['creation_date']),
                        "title" => "file:".$file['file_id']
                        );
    }
}

  if (!is_dir($dir)) 
    {
      $thisfile['name'] = '<img src="'.$GLOBALS['cfgWebRoot'].'gui/images/ico_folder.gif" width="16" height="16" align="absmiddle"> <a href="?module=files&action=list&path=/&site_id='.$GLOBALS['site_id'].'&view='.$GLOBALS['view'].'">back to root</a>';
      $thisfile['title'] = '';
      $thisfile['size'] = '';
      $thisfile['date'] = '';
      //if ($file != '..') $thisfile['del'] = 'x';
      $data[$i++] = $thisfile;
    }

  //chdir($cfgCmsDirRoot);  //this cfg doesn't exist?

  return $data;
}

$directory = sqlQueryValue("SELECT dirroot FROM sites WHERE site_id = '".$GLOBALS['site_id']."'");
$webroot = sqlQueryValue("SELECT domain FROM sites WHERE site_id = '".$GLOBALS['site_id']."'");
//$path = str_replace("..", "", $GLOBALS['path']);
$path = str_replace("..", "", $_SESSION['path_img']);
if ($path == "/") $path = "";


if($GLOBALS['cfgMini'] && $GLOBALS['cfgMiniDemo'] && strtolower($_SESSION["userdata"]['username']) != 'admin')
{
  //ensure that path starts with upload/userNNNNN
  $muststart_s = 'upload/user'.$_SESSION['demo_db_id'];
  if(substr($path, 0, strlen($muststart_s)) != $muststart_s)
  {
    $path = $muststart_s;
  }
}


//echo $_SESSION['root'];
//print_r($_GET);

$directory = fileAddBackslash($directory).$path;
$data = read_files('http://'.$webroot.'/', $directory, $path);

echo '
<script language="JavaScript">
  var selrow = ' . (int) ($data[0][0]) . ';
  if(!window.parent.Form)
    window.parent.Form = window.parent.document.getElementById("Form");
  window.parent.Form.Ok.disabled = false;
</script>';

echo("<script>\n  retVal('Form', 'Path', '".$path."');\n</script>\n");

require($GLOBALS['cfgDirRoot']."library/"."class.datagrid.php");

if ($_SESSION['root']==-1) 
{
    $cols['del']['format'] = '<a href="?module=docs&action=delete&path='.$GLOBALS['path'].'&file={%title%}&site_id='.$GLOBALS['site_id'].'&close=no" OnClick="return confirm(\'Are you sure you wish to delete this item?\')"><img src="'.$GLOBALS['cfgWebRoot'].'gui/images/ico_del.gif" width="16" height="16" border="0" alt="Delete"></a>';
}

$Table = new DataGrid();
$Table->cols = $cols;
$Table->data = $data;
$Table->footer = false;
$Table->filter = true;
$Table->table_id = 'filelist';
$Table->padding = "2";
$Table->rowformat = " [if {%site_id%}]onClick=\"javascript:HighLightTR(this, {%site_id%});\"[/if]";
echo("\n<br /><p>{%langstr:docs_current_dir%}: /".$path."</p>\n");
echo $Table->output();


?>
