<?


// Define form
$site_id = $GLOBALS['site_id'];
$id = intval($_GET["id"]);

function page_level($page_id, $site_id)
{
  $page_id = intval($page_id);
  $l = 0;
  while($page_id)
  {
    $l++;
    $page_id = sqlQueryValue("SELECT parent FROM " . $site_id . "_pages WHERE page_id=$page_id");
  }
  return $l;
}


$templates = sqlQueryData("SELECT template_id, name FROM " . $site_id . "_templates ORDER BY ind");
foreach ($templates as $row) {
  $combotemplates [] = $row['template_id'] . ":" . $row['name'];
  //add views to template list
  $views = sqlQueryData("SELECT -view_id AS id, name FROM " . $site_id . "_views WHERE template_id=".$row['template_id']." AND language_id = 0");
  $allviews = sqlQueryData("SELECT view_id AS page_id, name, parent,CONCAT('(view) ',name) AS title FROM " . $site_id . "_views WHERE template_id=".$row['template_id']." AND language_id = 0 ORDER BY ind");
  $comboviews = Array();
  ListNodes($allviews, $comboviews, 0, 0, "-");
  $comboviews = array_slice($comboviews, 1, count($comboviews) - 1);
  foreach($comboviews as $view)
  {
    $combotemplates [] = $view;//['id'] . ":&nbsp;&nbsp;&nbsp;&nbsp;(view)&nbsp;" . $view['name'];
  }
}

if($templates == null)
  Redirect("?module=pages&action=notemplates&site_id=" . $site_id);



$languages = sqlQueryData("SELECT language_id, fullname FROM " . $site_id . "_languages");
$combolanguages [] = "0:{%langstr:none%}";
if($languages == null)
{

}else
{
  foreach ($languages as $row) {
    $combolanguages [] = $row['language_id'] . ":" . $row['fullname'];
  }
}


$pages = sqlQueryData("SELECT page_id,name,parent,title FROM " . $site_id . "_pages WHERE page_id <> '$id' and in_trash=0 ORDER BY ind");
ListNodes($pages, $combopages, 0, 0);
$allpages = sqlQueryData("SELECT page_id,name,parent,title FROM " . $site_id . "_pages WHERE in_trash=0 ORDER BY ind");
ListNodes($allpages, $combopages2, 0, 0);
unset($combopages2[0]);
$combopages2 = array_merge(array('0:None', '-1:First child'), $combopages2);


$form_data = Array(

  "pagedev_id"     => Array(
    "label"     => "IDs:",
    "type"      => "hidden"
  ),


  "ind"         => Array(
    "noshow"    => true
  ),

  "sibiling"    => Array(
    "type"      => "hidden"
  ),

  "title"        => Array(
    "label"     => "{%langstr:title%}:",
    "type"      => "str",
    "hint"      => "{%langstrjs:formhint_page_title%}"
  ),

  "name"        => Array(
    "label"     => "{%langstr:col_name%}:",
    "type"      => "str",
    "hint"      => "{%langstrjs:formhint_page_name%}"
  ),

  "oldname"     => Array(
    "type"      => "hidden"
  ),

  "description" => Array(
    "label"     => "{%langstr:col_description%}:",
    "type"      => "html",
    "rows"      => "4",
    "cols"      => "30",
    "hint"      => "{%langstrjs:formhint_page_description%}"
  ),

  "template"    => Array(
    "label"     => "{%langstr:template%}:",
    "type"      => "list",
    "lookup"    => $combotemplates,
    "hint"      => "{%langstrjs:formhint_page_template%}"

  ),

  "language"    => Array(
    "label"     => "{%langstr:admin_language%}:",
    "type"      => "list",
    "lookup"    => $combolanguages,
    "hint"      => "{%langstrjs:formhint_page_language%}"

  ),

  "enabled"     => Array(
    "label"     => "{%langstr:enabled%}:",
    "type"      => "check",
    "value"     => "1",
    "hint"      => "{%langstrjs:formhint_page_enabled%}"
  ),

  "visible"     => Array(
    "label"     => "{%langstr:visible%}:",
    "type"      => "check",
    "value"     => "1",
    "hint"      => "{%langstrjs:formhint_page_visible%}"
  ),

  "passprotect"     => Array(
    "label"     => "{%langstr:protected%}:",
    "type"      => "check",
    "hint"      => "{%langstrjs:formhint_page_protected%}"
  ),

  "parent"      => Array(
    "label"     => "{%langstr:parent%}:",
    "type"      => "list",
    "lookup"    => $combopages,
    "hint"      => "{%langstrjs:formhint_page_parent%}"
  ),

  "redirect"    => Array(
    "label"     => "{%langstr:redirect%}:",
    "type"      => "list",
    "lookup"    => $combopages2,
    "hint"      => "{%langstrjs:formhint_page_redirect%}"
  ),

  "redirect_url"    => Array(
    "label"     => "{%langstr:redirect_url%}:",
    "type"      => "str",
    "hint"      => "{%langstrjs:formhint_page_redirect%}"
  ),

/*  "copypage"    => Array(
    "label"     => "View:",
    "type"      => "list",
    "lookup"    => $combopages2
  ), */

  "created"     => Array(
    "label"     => "{%langstr:col_created%}:",
    "type"      => "str",
    "size"      => "30",
    "disabled"  => true
  ),

  "lastmod"    => Array(
    "label"     => "{%langstr:last_modified%}:",
    "type"      => "str",
    "size"      => "30",
    "disabled"  => true
  ),

  "modby"    => Array(
    "label"     => "{%langstr:modify_by%}:",
    "type"      => "str",
    "size"      => "30",
    "disabled"  => true
  )

);

// Fill some values
  $action = $_GET['action'];
  $create = ($action == 'create');



  if(!$create){
    $row = sqlQueryRow("SELECT " . $site_id . "_pagesdev.*, users.username as modby FROM " . $site_id . "_pagesdev LEFT JOIN users ON " . $site_id . "_pagesdev.modby=users.user_id WHERE pagedev_id=$id ");
    $form_data['pagedev_id']['value'] = $id;
    $form_data['name']['value'] = $row['name'];
    $form_data['oldname']['value'] = $row['name'];
    $form_data['title']['value'] = $row['title'];
    $form_data['description']['value']  =$row['description'];
    $form_data['enabled']['value'] = $row['enabled'];
    $form_data['visible']['value'] = $row['visible'];
        $form_data['passprotect']['value'] = $row['passprotect'];
    $form_data['created']['value'] = date("Y-m-d H:i:s", $row['created']);
    $form_data['lastmod']['value'] = date("Y-m-d H:i:s", $row['lastmod']);
    $form_data['modby']['value'] = $row['modby'];
    $form_data['template']['value'] = $row['template'];
    $form_data['language']['value'] = $row['language'];
    $form_data['redirect']['value'] = $row['redirect'];
    $form_data['redirect_url']['value'] = $row['redirect_url'];
    if($row['copypage'])
    {
      $form_data['template']['value'] = $row['copypage'];
    }
    $form_data['parent']['value'] = $row['parent'];
  }
  else
  {
    $parent = intval($_GET['parent']);
    if ($_GET['changeparent']) $parent = sqlQueryValue("SELECT page_id FROM ".$site_id."_pagesdev WHERE pagedev_id=".$parent);
    if(($_GET['sibiling'])and($_GET['sibiling']<>'undefined'))
    {
      $form_data['sibiling']['value'] = $_GET['sibiling'];
      $parent = sqlQueryValue("SELECT parent FROM " . $site_id . "_pagesdev WHERE pagedev_id=" . intval($_GET['sibiling']));
    }

    if($parent<>'undefined')
    {
      $form_data['parent']['value'] = $parent;
    }
    list($parentlang,$parentcopypage,$parentpassprotect) = sqlQueryRow("SELECT language, copypage, passprotect FROM " . $site_id . "_pages WHERE page_id='$parent'");
    if(!$parentlang)
    {
      $parentlang = 0;
    }
    if(!$parentcopypage)
    {
      $parentcopypage = 0;
    }
    if($parentpassprotect)
    {
      $form_data['passprotect']['value'] = True;
    }
    $form_data['language']['value'] = $GLOBALS['currentlanguagenum'];
    if($parentcopypage)
    {
      $form_data['template']['value'] = $parentcopypage;
    }

    //select default view for new page
    $views = sqlQueryData("SELECT * FROM " . $site_id . "_views AS v, " . $site_id . "_templates AS t WHERE t.template_id = v.template_id AND language_id = 0 ORDER BY t.ind DESC, v.ind");
    $tparent = $parent;
    $tparents = array();
    while($tparent)
    {
      $tparents[] = $tparent;
      $tparent = sqlQueryValue("SELECT parent FROM " . $site_id . "_pages WHERE page_id=" . intval($tparent));
    }

    $form_data['template']['value'] = '';
    foreach($views as $view)
    {
      $levels = explode(":", $view['levels']);
      $parents = explode(":", $view['parents']);
      foreach($parents as $key=>$row)
      {
        $parents[$key] = explode("|", $parents[$key]);
      }
      $languages = explode(":", $view['languages']);
      $goodlevel = (($levels[0]=='') or (in_array(page_level($parent, $site_id)+1, $levels)));
      $goodparent = (($parents[0][0]==''));
      foreach($parents as $p)
      {
        if($p[1] == 1)
        {  //immediate parent
          if($parent == $p[0])
          {
            $goodparent = true;
          }
        }else
        { //any parent
          if(in_array($p[0], $tparents))
          {
            $goodparent = true;
          }
        }
      }
      $goodlanguage = ((($languages[0]=='')) or (in_array($_SESSION['currentlanguagenum'], $languages)));
      if(($goodlanguage)and($goodparent)and($goodlevel))
      {
        $form_data['template']['value'] = -$view['view_id'];
      }
    }
  }


  $invalid = explode(':', $_GET['invalid']);
  foreach($invalid as $i)
  {
    if($i)
      $form_data[$i]['error'] = true;
  }

  foreach(array_keys($_GET) as $key)
  {
    $s = explode('_', $key, 2);
    if($s[0] == 'formdata')
      $form_data[$s[1]]['value'] = stripslashes($_GET[$key]);
  }

  if($_GET['jsmessage'])
  {
    echo '<script language="JavaScript">
          <!--
          alert("' . stripslashes($_GET['jsmessage']) . '");
          <!---->
          </script>';
  }



// Use FormGen to bring this out

  require($GLOBALS['cfgDirRoot']."library/"."class.formgen.php");
  $FormGen = new FormGen();
  if(!$create)
  {
    $FormGen->action = "?module=".$GLOBALS['module']."&site_id=".$site_id."&action=modpage";
  }else{
    $FormGen->action = "?module=".$GLOBALS['module']."&site_id=".$site_id."&action=addpage";
  }
  $FormGen->cancel = "close";
  if(!$create)
  {
    $FormGen->title = "{%langstr:page_properties%} : " . ShortenString($row['title'],40);
  }else{
    $FormGen->title = "{%langstr:create_new_page%}";
  }
  $FormGen->properties = $form_data;
  $FormGen->buttons = true;
  echo $FormGen->output();

?>
