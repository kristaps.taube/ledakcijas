<?

// Define form
$site_id = $GLOBALS['site_id'];
$id = $_GET["id"];

$file_name = sqlQueryValue("SELECT file_name FROM ".$site_id."_documents WHERE file_id=".$id);

$cols = Array(


  "user"        => Array(
    "width"     => "30%",
    "title"     => "User",
  ),

  "times"        => Array(
    "width"     => "30%",
    "title"     => "Times accessed",
  ),

  "lastaccess"        => Array(
    "width"     => "40%",
    "title"     => "Last access date",
  )

);

$data = sqlQueryData("SELECT u.username as 'user', count(d.user_id) as 'times', FROM_UNIXTIME(max(d.time)) as 'lastaccess' FROM ".$site_id."_documentstats as d LEFT JOIN users as u ON d.user_id=u.user_id  WHERE document_id=".$id." GROUP BY d.user_id");



require($GLOBALS['cfgDirRoot']."library/"."class.datagrid.php");
$Table = new DataGrid();
$Table->cols = $cols;
$Table->data = $data;
$Table->footer = false;
echo '<br><b>File: '.$file_name.'</b><br><br>';
echo $Table->output();

?>
