<?

// Define form

$form_data = Array(

  "string_id" => Array(
    "label"     => "ID:",
    "type"      => "hidden"
  ),

  "ind"         => Array(
    "noshow"    => true
  ),

  "stringname"  => Array(
    "label"     => "{%langstr:string_name%}:",
    "type"      => "str"
  ),

  "stringvalue" => Array(
    "label"     => "{%langstr:col_value%}:",
    "type"      => "html",
    "rows"      => "6",
    "cols"      => "30"
  )

);

// Fill some values
  $id = $_GET["id"];
  $action = $_GET['action'];
  $create = ($action == 'create');
  $site_id = $GLOBALS['site_id'];

  if(!$create){
    $row = sqlQueryRow("SELECT * FROM " . $site_id . "_strings WHERE string_id=$id");
    $form_data['string_id']['value'] = $id;
    $form_data['stringname']['value'] = $row['stringname'];
    $form_data['stringvalue']['value'] = $row['stringvalue'];
  }

  $invalid = split(':', $_GET['invalid']);
  foreach($invalid as $i)
  {
    if($i)
      $form_data[$i]['error'] = true;
  }

  foreach(array_keys($_GET) as $key)
  {
    $s = Split('_', $key, 2);
    if($s[0] == 'formdata')
      $form_data[$s[1]]['value'] = stripslashes($_GET[$key]);
  }

  if($_GET['jsmessage'])
  {
    echo '<script language="JavaScript">
          <!--
          alert("' . stripslashes($_GET['jsmessage']) . '");
          <!---->
          </script>';
  }

// Use FormGen to bring this out

  require($GLOBALS['cfgDirRoot']."library/"."class.formgen.php");
  $FormGen = new FormGen();
  if(!$create)
  {
    $FormGen->action = "?module=".$GLOBALS['module']."&site_id=".$site_id."&action=modstring";
  }else{
    $FormGen->action = "?module=".$GLOBALS['module']."&site_id=".$site_id."&action=addstring";
  }
  $FormGen->cancel = "close";
  if(!$create)
  {
    $FormGen->title = "{%langstr:string_properties%} : " . $row['title'];
  }else{
    $FormGen->title = "{%langstr:add_new_string%}";
  }
  $FormGen->properties = $form_data;
  $FormGen->buttons = true;
  echo $FormGen->output();

?>
