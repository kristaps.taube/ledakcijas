<?

// Define form
$site_id = $GLOBALS['site_id'];
$id = $_GET["id"];

$wappage_id = sqlQueryValue("select wappage_id from ".$site_id."_wappagesdev where wappagedev_id=".$id);

$cols = Array(

  "preview"     => Array(
    "width"     => "5%",
    "align"     => "center",
    "title"     => "{%langstr:col_prev%}",
  ),

  "title"        => Array(
    "width"     => "30%",
    "title"     => "{%langstr:title%}",
  ),

  "lastmod"        => Array(
    "width"     => "30%",
    "title"     => "{%langstr:col_modified%}",
  ),

  "modby"        => Array(
    "width"     => "30%",
    "title"     => "{%langstr:modify_by%}",
  ),
/*
  "description" => Array(
    "width"     => "30%",
    "title"     => "Description"
  ),
*/
  "restore"     => Array(
    "width"     => "15%",
    "title"     => ""
  )

);

$h_pages = sqlQueryData("SELECT w.wappagedev_id, w.title, w.lastmod, w.description, users.username FROM ".$site_id."_wappagesdev as w LEFT JOIN users ON users.user_id=w.modby WHERE wappage_id=".$wappage_id."  ORDER BY lastmod DESC");

//published page
$page = sqlQueryRow("SELECT w.wappage_id, w.title, w.lastmod, w.description, users.username FROM ".$site_id."_wappages as w LEFT JOIN users ON users.user_id=w.modby WHERE wappage_id=".$wappage_id."  ORDER BY lastmod DESC");

//?module=wap&amp;site_id=11&amp;action=page_redirect&amp;id=16
$data[] = array("preview"=>"<a href=\"?module=wap&site_id=$site_id&action=page_redirect&id=".$page['wappage_id']."\"  target=\"_blank\"><img src=\"gui/images/ico_view_wap.gif\" width=\"16\" height=\"16\" border=\"0\"></a>",
                    "title"       => $page['title'].' <i>(published)</i>',
                    "description" => $page['description'],
                    "lastmod"     => date("Y-m-d H:i:s",$page['lastmod']),
                    "modby"       => $page['username'],
                    "restore"     => "<a href=\"?module=wap&action=restore_history&wappage_id=".$page['wappage_id']."&site_id=$site_id&published=1\">{%langstr:restore%}</a>"
                    );

foreach ($h_pages as $h_page)
{
    $data[] = array("preview"=>"<a href=\"?module=wap&site_id=$site_id&action=page_redirect&viewpagehistory=".$h_page['wappagedev_id']."&id=".$wappage_id."\"  target=\"_blank\"><img src=\"gui/images/ico_view_wap.gif\" width=\"16\" height=\"16\" border=\"0\"></a>",
                    "title"       => $h_page['title'],
                    "description" => $h_page['description'],
                    "lastmod"     => date("Y-m-d H:i:s",$h_page['lastmod']),
                    "modby"       => $h_page['username'],
                    "restore"     => "<a href=\"?module=wap&action=restore_history&wappagedev_id=".$h_page['wappagedev_id']."&id=".$wappage_id."&site_id=$site_id\">{%langstr:restore%}</a>"
                    );
}

require($GLOBALS['cfgDirRoot']."library/"."class.datagrid.php");
echo '<div style="width:500px;padding-right:12px;">';
$Table = new DataGrid();
$Table->cols = $cols;
$Table->data = $data;
$Table->footer = false;
$Table->rowformat = " id=\"tablerow{%wappagedev_id%}\" ";
echo $Table->output();
echo '</div>';

?>
