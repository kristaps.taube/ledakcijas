<?

$perm_managedocuments = $GLOBALS['perm_managedocuments'];
if(!$perm_managedocuments)
  AccessDenied(True);

$form_data = Array(

  "userfile"    => Array(
    "label"     => "Filename:",
    "size"      => "35",
    "type"      => "file",
    "uploadonly" => true
  ),

  "description" => Array(
    "label"     => "Description:",
    "type"      => "html",
    "rows"      => "4",
    "cols"      => "30"
  )

);

require($GLOBALS['cfgDirRoot']."library/"."class.formgen.php");
$FormGen = new FormGen();
$FormGen->action = "?module=docs&action=save&site_id=".$GLOBALS['site_id']."&parent_dir=".$_SESSION['parent_dir']."&view=".$GLOBALS['view'].($GLOBALS['close'] ? "&close=".$GLOBALS['close'] : "");
$FormGen->title = "Upload File";
$FormGen->cancel = ($GLOBALS['close'] ? "?module=docs&action=list&site_id=".$GLOBALS['site_id']."&path=".$GLOBALS['path']."&view=".$GLOBALS['view'] : 'close');
$FormGen->properties = $form_data;
$FormGen->buttons = true;
echo $FormGen->output();

echo '
    <script language="JavaScript">
        if (window.parent.frames.length) window.parent.Form.Ok.disabled = true;
    </script>
';

?>
