<?

// checkbox supports
if ($_POST)
{
  // atlasam komponentus, par kuriem ir info ieksh POST
  $cmp = Array();
  foreach(array_keys($_POST) as $key)
  {
    $s = explode('_', $key, 2);
    if($s[0] == 'type')
    {
      $cmp[$s[1]] = $_POST[$key];
    }
  }

  // uzstadam tuksho checkbokshu vertibu kaa nulli
  foreach ($cmp as $name => $type)
  {
    $props = getPropertiesTable($type);
    foreach ($props as $key => $p)
    {
      if ($p['type'] == 'check')
      {
        $k = 'component_'.$name.'_'.$key;
        if (!isset($_POST[$k]))
          $_POST[$k] = '0';

      }
    }
  }
}

  require_once ($GLOBALS['cfgDirRoot']."library/".'func.search.php');

  $site_id = $GLOBALS['site_id'];

  $perm_managepages = $GLOBALS['perm_managepages'];
  $perm_managecontents = $GLOBALS['perm_managecontents'];

  if(((($action == 'addpage')and($perm_managepages)))or((($action == 'modpage')and($perm_managepages))))
  {
    $name = $_POST['name'];
    $oldname = $_POST['oldname'];
    $title = $_POST['title'];
    $description = $_POST['description'];
    $template = $_POST['template'];
    $language = $_POST['language'];
    $lastmod = time();
    $modby = $GLOBALS['currentUserID'];
    if($_POST['enabled']){
      $enabled = 1;
    }else{
      $enabled = 0;
    }
    if($_POST['visible']){
      $visible = 1;
    }else{
      $visible = 0;
    }
    if($_POST['passprotect']){
      $passprotect = 1;
    }else{
      $passprotect = 0;
    }
    $parent = $_POST['parent'];
    $pagedev_id = intval($_POST['pagedev_id']);
    $redirect = intval($_POST['redirect']);
    $copypage = intval($_POST['copypage']);
    $redirect_url = $_POST['redirect_url'];

    //Check if field input is valid
    $invalid = Array();
    $jsmessage = '';

    //get original page_id to check the name
    if ($pagedev_id) $page_id = sqlQueryValue("select page_id from ".$site_id."_pagesdev where pagedev_id=".$pagedev_id);
    if(IsBadSymbols($name) or sqlQueryValue("SELECT page_id FROM " . $site_id . "_pages WHERE name='$name' AND page_id<>'$page_id' AND parent='$parent'"))
    {
      $invalid[] = "name";
      if(!IsEmpty($name))
        $jsmessage .= 'Page name' . msgValidCharsUnique . '\n';
    }
    if (($oldname=="404")and($name!=$oldname))
    {
      $invalid[] = "name";
      $jsmessage .= 'Page 404 can not be renamed\n';
    }
    if(IsLtGtSymbols($title))
    {
      $invalid[] = "title";
      $jsmessage .= 'Title ' . msgNoLtGt . '\n';
    }

    if(IsLtGtSymbols($description))
    {
      $invalid[] = "description";
      $jsmessage .= 'Description ' . msgNoLtGt . '\n';
    }
  //  if(IsBadSymbols($title, " "))
  //  {
  //    $invalid[] = "title";
  //  }
    if(count($invalid))
    {
      if($action == 'addpage')
        $action = 'create';
      if($action == 'modpage')
        $action = 'properties_form';
      $invalid = join(':', $invalid);
      $name = urlencode(stripslashes($name));
      $title = urlencode(stripslashes($title));
      $description = urlencode(stripslashes($description));
      $jsmessage = urlencode($jsmessage);

      Redirect('?module='.$module.'&action='.$action.'&invalid=' . $invalid . '&formdata_name='.$name . '&formdata_title='.$title.'&formdata_description='.$description . '&formdata_template='.$template . '&formdata_enabled='.$enabled . '&formdata_visible='.$visible . '&formdata_passprotect='.$passprotect . '&formdata_parent='.$parent . '&formdata_language='.$language . '&formdata_redirect='.$redirect . '&formdata_copypage='.$copypage.'&formdata_redirect_url='.$redirect_url.'&jsmessage='.$jsmessage.'&id='.$pagedev_id.'&site_id='.$site_id);
      Die();
    }

  }

  $copypage = 0;
  if($template < 0)
  {
    $copypage = sqlQueryValue("SELECT -view_id FROM " . $site_id . "_views WHERE view_id=" . abs($template));
    if($copypage)
    {
      $template = sqlQueryValue("SELECT template_id FROM " . $site_id . "_views WHERE view_id=".abs($copypage));
    }
  }

  if(IsEmpty($name))
  {
    //auto generate name from title
    $n = $title;

    $n = trim($n);
    $n = transliterate($n);

    $n = preg_replace('/\s+/', '-', $n);
    $n = preg_replace('/[^a-zA-Z0-9\-]/', '', $n);

    if(!$n) $n = 'index';
    $n = strtolower(substr($n, 0, 30));
    $i = 0;
    $name = $n;
    while(sqlQueryValue("SELECT pagedev_id FROM " . $site_id . "_pagesdev WHERE name='$name' AND pagedev_id<>'$pagedev_id' AND parent='$parent'"))
    {
      $i++;
      $name = $n . $i;
    }

  }

  if ($GLOBALS['maincharset'] == 'utf-8' && function_exists('mb_strlen') && function_exists('mb_substring'))
  {

    if (mb_strlen($title, 'utf-8')>200)
    {
      $title = mb_substr($title, 0, 200, 'utf-8').' ...';
    }

  } else {

    if (strlen($title)>200)
    {
      $title = substr($title,0,200).' ...';
    }

  }

  if ($action == 'delpage' && !$_GET['id'])
  {
    return;
  }


  if(($action == 'addpage')and($perm_managepages))
  {
    $parent = intval($parent);
    $created = time();
    if($_POST['sibiling'])
    {
      $sibiling = $_POST['sibiling'];
    }
    else
    {
      $sibiling = intval(sqlQueryValue("SELECT max(ind) FROM " . $site_id . "_pages WHERE parent=$parent"));
    }


    sqlQuery("UPDATE " . $site_id . "_pages SET ind=ind+1 WHERE ind>$sibiling AND parent=$parent");
    sqlQuery("INSERT INTO " . $site_id . "_pages (name,title,description,template,language,lastmod,created,modby,enabled,visible,passprotect,parent,ind,redirect,copypage,redirect_url) VALUES ('$name','$title','$description','$template','$language','$lastmod','$created','$modby','$enabled','$visible','$passprotect',$parent,$sibiling+1,$redirect,$copypage,'$redirect_url')");



    $page_id = sqlQueryValue("SELECT page_id FROM " . $site_id . "_pages WHERE name='$name' AND parent='$parent'");

    //page dev copy
    sqlQuery("UPDATE " . $site_id . "_pagesdev SET ind=ind+1 WHERE ind>$sibiling AND parent=$parent");
    sqlQuery("INSERT INTO " . $site_id . "_pagesdev (page_id,name,title,description,template,language,lastmod,created,modby,enabled,visible,passprotect,parent,ind,redirect,copypage,redirect_url) VALUES ('$page_id','$name','$title','$description','$template','$language','$lastmod','$created','$modby','$enabled','$visible','$passprotect',$parent,$sibiling+1,$redirect,$copypage,'$redirect_url')");

    $lid = sqlQueryValue("select pagedev_id from " . $site_id . "_pagesdev  where created = '".$created."' AND page_id=".$page_id);

    //change parents children count
    $c = sqlQueryValue('SELECT COUNT(*) FROM ' . $site_id . '_pages WHERE parent = ' . $parent);
    sqlQuery('UPDATE ' . $site_id . '_pagesdev SET childrencount = ' . $c .  ' WHERE page_id = ' . $parent);

    $_SESSION['expanded' . $parent] = true;
    $docStrings['urlend'] = "&selrow=" . $lid;


    //$page_id = sqlQueryValue("SELECT page_id FROM " . $site_id . "_pages WHERE name='$name' AND parent='$parent'");
    //$_SESSION['expanded' . $parent] = true;
    //$docStrings['urlend'] = "&selrow=" . $page_id;
    //Add access to the newly created page for Everyone
    AddGroupPermission(1, 11, $site_id, $page_id);
    //Add access to delete the newly created page for Everyone
    AddGroupPermission(1, 13, $site_id, $page_id);
    //Add access to the newly created page for Administrators
    AddGroupPermission(2, 11, $site_id, $page_id);
    //Add access to delete the newly created page for Administrators
    AddGroupPermission(2, 13, $site_id, $page_id);
    Add_Log("New page '$name' ($page_id)", $site_id);

    IndexPageForSearch($site_id, $page_id);
    DeleteAllCompiledTemplates($site_id);

  }else if(($action == 'modpage')and($perm_managepages))
  {
    $parent = intval($parent);

    //Check out if it's changing parent and act
    $curpage = sqlQueryRow("SELECT page_id, parent,ind FROM " . $site_id . "_pagesdev WHERE pagedev_id=$pagedev_id");

    if($parent <> $curpage['parent'])
    {

      sqlQuery("UPDATE " . $site_id . "_pages SET ind=ind-1 WHERE ind>" . $curpage['ind'] . " AND parent=" . $curpage['parent']);
      sqlQuery("UPDATE " . $site_id . "_pages SET ind=ind+1 WHERE parent=$parent");
      sqlQuery("UPDATE " . $site_id . "_pages SET ind=1 WHERE page_id='".$curpage['page_id']."'");
      sqlQuery("UPDATE " . $site_id . "_pages SET parent='$parent' WHERE page_id=".$curpage['page_id']);

      sqlQuery("UPDATE " . $site_id . "_pagesdev SET ind=ind-1 WHERE ind>" . $curpage['ind'] . " AND parent=" . $curpage['parent']);
      sqlQuery("UPDATE " . $site_id . "_pagesdev SET ind=ind+1 WHERE parent=$parent");
      sqlQuery("UPDATE " . $site_id . "_pagesdev SET ind=1 WHERE page_id='".$curpage['page_id']."'");
      sqlQuery("UPDATE " . $site_id . "_pagesdev SET parent='$parent' WHERE page_id=".$curpage['page_id']);

      //change parents children count for old and new parent
      $c = sqlQueryValue('SELECT COUNT(*) FROM ' . $site_id . '_pages WHERE parent = ' . $curpage['parent']);
      sqlQuery('UPDATE ' . $site_id . '_pagesdev SET childrencount = ' . $c .  ' WHERE page_id = ' . $curpage['parent']);

      $c = sqlQueryValue('SELECT COUNT(*) FROM ' . $site_id . '_pages WHERE parent = ' . $parent);
      sqlQuery('UPDATE ' . $site_id . '_pagesdev SET childrencount = ' . $c .  ' WHERE page_id = ' . $parent);
    }
    if ($name <> $curpage['name'])
    {
        sqlQuery("UPDATE " . $site_id . "_pages SET name='$name' WHERE page_id=".$curpage['page_id']);
        sqlQuery("UPDATE " . $site_id . "_pagesdev SET name='$name' WHERE page_id=".$curpage['page_id']);
    }

    sqlQuery("UPDATE " . $site_id . "_pagesdev SET name='$name', title='$title', description='$description', template='$template', language='$language', lastmod='$lastmod', modby='$modby', enabled='$enabled', visible='$visible', passprotect='$passprotect', redirect='$redirect', copypage='$copypage', redirect_url='$redirect_url' WHERE pagedev_id='$pagedev_id'");
    Add_Log("Updated page '$name' (" . $curpage['page_id'] . ")", $site_id);


    IndexPageForSearch($site_id, $curpage['page_id']);
    DeleteAllCompiledTemplates($site_id);

    if (CheckPermission(31, $site_id, $curpage['page_id']) || CheckPermission(32, $site_id))
    {
        pageautopublish($site_id, $pagedev_id);
    }

  } else if(($action == 'docopy')and($perm_managepages))
  {

    //$page_id - page to copy
    //$destination - parent page of copy
    function copypage($page_id, $destination, $destlanguage, $new_ind=0)
    {
        //active dev page id of currently processed page
        $site_id = intval($_GET['site_id']);
        $page_id = intval($page_id);
        $parent = intval($parent);
        $pagedev_id = sqlQueryValue("SELECT pagedev_id, lastmod FROM ".$site_id."_pagesdev WHERE page_id=".$page_id." ORDER BY lastmod DESC LIMIT 1");

        $created = time();
        $parent = $destination;

        /*if($_POST['sibiling'])
        {
          $sibiling = $_POST['sibiling'];
        }
        else
        {*/
          $sibiling = intval(sqlQueryValue("SELECT max(ind) FROM " . $site_id . "_pages WHERE parent=$parent"));

          if ($new_ind)
            $sibiling = $new_ind;

        //}
        $page = sqlQueryRow("SELECT * FROM ".$site_id."_pages WHERE page_id=".$page_id);
        $name = $page['name'];
        $title = $page['title'];
        $description = $page['description'];
        $template = $page['template'];
        $language = $page['language'];
        $lastmod = $page['lastmod'];
        $modby = $page['modby'];
        $enabled = $page['enabled'];
        $visible = $page['visible'];
        $passprotect = $page['passprotect'];
        $redirect = $page['redirect'];
        $copypage = $page['copypage'];
        $redirect_url = $page['redirect_url'];

        //update language
        if($destlanguage != 0)
        {
          $language = intval($destlanguage);
        }

        //check all names if copy to same level
        if ($page['parent']==$destination) $cond = "";
        else $cond = "page_id<>'$page_id' AND ";

        if (sqlQueryValue("SELECT page_id FROM " . $site_id . "_pages WHERE name='$name' AND ".$cond."parent='$parent'"))
        {
            //auto generate new name
            $n = $name;
            $i = 0;
            $name = $n.$i;
            while(sqlQueryValue("SELECT page_id FROM " . $site_id . "_pages WHERE name='$name' AND page_id<>'$page_id' AND parent='$parent'"))
            {
              $i++;
              $name = $n.$i;
            }
        }

        $contents = sqlQueryData("SELECT * FROM ".$site_id."_contents WHERE page=".$page_id);
        $contentsdev = sqlQueryData("SELECT * FROM ".$site_id."_contentsdev WHERE pagedev=".$pagedev_id);
        $strings = sqlQueryData("SELECT * FROM ".$site_id."_strings WHERE page=".$page_id);
        $stringsdev = sqlQueryData("SELECT * FROM ".$site_id."_stringsdev WHERE pagedev=".$pagedev_id);
        $permissions = sqlQueryData("SELECT * FROM permissions WHERE data=$site_id AND data2=$page_id AND (perm_num=11 OR perm_num=13 OR perm_num=16 OR perm_num=31)");

        //update indexes of destination page children and insert a copy of original page
        sqlQuery("UPDATE " . $site_id . "_pages SET ind=ind+1 WHERE ind>$sibiling AND parent=$parent");
        sqlQuery("INSERT INTO " . $site_id . "_pages (name,title,description,template,language,lastmod,created,modby,enabled,visible,passprotect,parent,ind,redirect,copypage,redirect_url) VALUES ('$name','$title','$description','$template','$language','$lastmod','$created','$modby','$enabled','$visible','$passprotect',$parent,$sibiling+1,$redirect,$copypage,'$redirect_url')");

        //get page_id of inserted page
        $ipage_id = sqlQueryValue("SELECT page_id FROM " . $site_id . "_pages WHERE name='$name' AND parent='$parent'");
        //put contents of original page to copy
        foreach ($contents as $content)
        {
            sqlQuery("INSERT INTO ".$site_id."_contents (page, componentname, propertyname, propertyvalue) VALUES (".$ipage_id.",'".$content['componentname']."','".$content['propertyname']."','".addslashes($content['propertyvalue'])."')");
        }
        foreach ($strings as $string)
        {
            sqlQuery("INSERT INTO ".$site_id."_strings (page, stringname, stringvalue) VALUES (".$ipage_id.",'".$string['stringname']."','".addslashes($string['stringvalue'])."')");
        }
        //copy permissions of original page to copy
        foreach ($permissions as $p)
        {
            sqlQuery("INSERT INTO permissions (perm_num, isuser, id, data, data2, extradata) VALUES (".$p['perm_num'].",'".$p['isuser']."',".$p['id'].",'".$p['data']."','".$ipage_id."', '".$p['extradata']."')");
        }

        //page dev copy
        sqlQuery("UPDATE " . $site_id . "_pagesdev SET ind=ind+1 WHERE ind>$sibiling AND parent=$parent");
        sqlQuery("INSERT INTO " . $site_id . "_pagesdev (page_id,name,title,description,template,language,lastmod,created,modby,enabled,visible,passprotect,parent,ind,redirect,copypage,redirect_url) VALUES ('$ipage_id','$name','$title','$description','$template','$language','$lastmod','$created','$modby','$enabled','$visible','$passprotect',$parent,$sibiling+1,$redirect,$copypage,'$redirect_url')");

        //get pagedev_id of inserted page
        $ipagedev_id = sqlQueryValue("select pagedev_id from " . $site_id . "_pagesdev  where created = '".$created."' AND page_id=".$ipage_id);
        //put contentsdev of original pagedev to copy
        foreach ($contentsdev as $contentdev)
        {
            sqlQuery("INSERT INTO ".$site_id."_contentsdev (pagedev, componentname, propertyname, propertyvalue) VALUES (".$ipagedev_id.",'".$contentdev['componentname']."','".$contentdev['propertyname']."','".addslashes($contentdev['propertyvalue'])."')");
        }
        foreach ($stringsdev as $stringdev)
        {
            sqlQuery("INSERT INTO ".$site_id."_stringsdev (pagedev, stringname, stringvalue) VALUES (".$ipagedev_id.",'".$stringdev['stringname']."','".addslashes($stringdev['stringvalue'])."')");
        }


        //check if children exists
        $subpages = sqlQueryData("SELECT page_id, ind FROM ".$site_id."_pages WHERE parent=".$page_id);
        //copy each child; destination is just created page
        foreach ($subpages as $subpage)
        {
            copypage($subpage['page_id'],$ipage_id, $destlanguage, $subpage['ind']);
        }
    }

    $from = $_POST['from'];
    $to = $_POST['to'];
    $language = $_POST['language'];

    copypage($from, $to, $language);

    //recalculate all children
    $pages = sqlQueryData('SELECT * FROM ' . $site_id . '_pagesdev');
    foreach($pages as $p)
    {
       $c = sqlQueryValue('SELECT COUNT(*) FROM ' . $site_id . '_pages WHERE parent = ' . $p['page_id']);
       //echo $p['title'] . $c . '<br />';
       sqlQuery('UPDATE ' . $site_id . '_pagesdev SET childrencount = ' . $c .  ' WHERE page_id = ' . $p['page_id']);
    }

    DeleteAllCompiledTemplates($site_id);


  } else if(($action == 'docopy2')and($perm_managepages))
  {
    //Copy several pages selected with Ctrl!!
    //$page_id - page to copy
    //$destination - parent page of copy
    function copypage($page_id, $destination, $new_ind=0)
    {
        //active dev page id of currently processed page
        $site_id = intval($_GET['site_id']);
        $page_id = intval($page_id);
        $destination = intval($destination);
        $pagedev_id = sqlQueryValue("SELECT pagedev_id, lastmod FROM ".$site_id."_pagesdev WHERE page_id=".$page_id." ORDER BY lastmod DESC LIMIT 1");

        $created = time();
        $parent = $destination;

        /*if($_POST['sibiling'])
        {
          $sibiling = $_POST['sibiling'];
        }
        else
        {*/
          $sibiling = intval(sqlQueryValue("SELECT max(ind) FROM " . $site_id . "_pages WHERE parent=$parent"));

          if ($new_ind)
            $sibiling = $new_ind;

        //}
        $page = sqlQueryRow("SELECT * FROM ".$site_id."_pages WHERE page_id=".$page_id);
        $name = $page['name'];
        $title = $page['title'];
        $description = $page['description'];
        $template = $page['template'];
        $language = $page['language'];
        $lastmod = $page['lastmod'];
        $modby = $page['modby'];
        $enabled = $page['enabled'];
        $visible = $page['visible'];
        $passprotect = $page['passprotect'];
        $redirect = $page['redirect'];
        $copypage = $page['copypage'];
        $redirect_url = $page['redirect_url'];

        //check all names if copy to same level
        if ($page['parent']==$destination) $cond = "";
        else $cond = "page_id<>'$page_id' AND ";

        if (sqlQueryValue("SELECT page_id FROM " . $site_id . "_pages WHERE name='$name' AND ".$cond."parent='$parent'"))
        {
            //auto generate new name
            $n = $name;
            $i = 0;
            $name = $n.$i;
            while(sqlQueryValue("SELECT page_id FROM " . $site_id . "_pages WHERE name='$name' AND page_id<>'$page_id' AND parent='$parent'"))
            {
              $i++;
              $name = $n.$i;
            }
        }

        $contents = sqlQueryData("SELECT * FROM ".$site_id."_contents WHERE page=".$page_id);
        $contentsdev = sqlQueryData("SELECT * FROM ".$site_id."_contentsdev WHERE pagedev=".$pagedev_id);
        $strings = sqlQueryData("SELECT * FROM ".$site_id."_strings WHERE page=".$page_id);
        $stringsdev = sqlQueryData("SELECT * FROM ".$site_id."_stringsdev WHERE pagedev=".$pagedev_id);
        $permissions = sqlQueryData("SELECT * FROM permissions WHERE data=$site_id AND data2=$page_id AND (perm_num=11 OR perm_num=13 OR perm_num=16 OR perm_num=31)");

        //update indexes of destination page children and insert a copy of original page
        sqlQuery("UPDATE " . $site_id . "_pages SET ind=ind+1 WHERE ind>$sibiling AND parent=$parent");
        sqlQuery("INSERT INTO " . $site_id . "_pages (name,title,description,template,language,lastmod,created,modby,enabled,visible,passprotect,parent,ind,redirect,copypage,redirect_url) VALUES ('$name','$title','$description','$template','$language','$lastmod','$created','$modby','$enabled','$visible','$passprotect',$parent,$sibiling+1,$redirect,$copypage,'$redirect_url')");

        //get page_id of inserted page
        $ipage_id = sqlQueryValue("SELECT page_id FROM " . $site_id . "_pages WHERE name='$name' AND parent='$parent'");
        //put contents of original page to copy
        foreach ($contents as $content)
        {
            sqlQuery("INSERT INTO ".$site_id."_contents (page, componentname, propertyname, propertyvalue) VALUES (".$ipage_id.",'".$content['componentname']."','".$content['propertyname']."','".$content['propertyvalue']."')");
        }
        foreach ($strings as $string)
        {
            sqlQuery("INSERT INTO ".$site_id."_strings (page, stringname, stringvalue) VALUES (".$ipage_id.",'".$string['stringname']."','".$string['stringvalue']."')");
        }
        //copy permissions of original page to copy
        foreach ($permissions as $p)
        {
            sqlQuery("INSERT INTO permissions (perm_num, isuser, id, data, data2, extradata) VALUES (".$p['perm_num'].",'".$p['isuser']."',".$p['id'].",'".$p['data']."','".$ipage_id."', '".$p['extradata']."')");
        }

        //page dev copy
        sqlQuery("UPDATE " . $site_id . "_pagesdev SET ind=ind+1 WHERE ind>$sibiling AND parent=$parent");
        sqlQuery("INSERT INTO " . $site_id . "_pagesdev (page_id,name,title,description,template,language,lastmod,created,modby,enabled,visible,passprotect,parent,ind,redirect,copypage,redirect_url) VALUES ('$ipage_id','$name','$title','$description','$template','$language','$lastmod','$created','$modby','$enabled','$visible','$passprotect',$parent,$sibiling+1,$redirect,$copypage,'$redirect_url')");

        //get pagedev_id of inserted page
        $ipagedev_id = sqlQueryValue("select pagedev_id from " . $site_id . "_pagesdev  where created = '".$created."' AND page_id=".$ipage_id);
        //put contentsdev of original pagedev to copy
        foreach ($contentsdev as $contentdev)
        {
            sqlQuery("INSERT INTO ".$site_id."_contentsdev (pagedev, componentname, propertyname, propertyvalue) VALUES (".$ipagedev_id.",'".$contentdev['componentname']."','".$contentdev['propertyname']."','".$contentdev['propertyvalue']."')");
        }
        foreach ($stringsdev as $stringdev)
        {
            sqlQuery("INSERT INTO ".$site_id."_stringsdev (pagedev, stringname, stringvalue) VALUES (".$ipagedev_id.",'".$stringdev['stringname']."','".$stringdev['stringvalue']."')");
        }


        //check if children exists
        $subpages = sqlQueryData("SELECT page_id, ind FROM ".$site_id."_pages WHERE parent=".$page_id);
        //copy each child; destination is just created page
        foreach ($subpages as $subpage)
        {
            copypage($subpage['page_id'], $ipage_id, $subpage['ind']);
        }
    }

    $from_all = preg_split("/[\s,]+/", $_POST['from'],-1,PREG_SPLIT_NO_EMPTY);
    $to = $_POST['to'];

    foreach($from_all as $from)
    {
      copypage($from, $to);
      DeleteAllCompiledTemplates($site_id);
    }
    $pages = sqlQueryData('SELECT * FROM ' . $site_id . '_pagesdev');
    foreach($pages as $p)
    {
       $c = sqlQueryValue('SELECT COUNT(*) FROM ' . $site_id . '_pages WHERE parent = ' . $p['page_id']);
       //echo $p['title'] . $c . '<br />';
       sqlQuery('UPDATE ' . $site_id . '_pagesdev SET childrencount = ' . $c .  ' WHERE page_id = ' . $p['page_id']);
    }


  } else if(($action == 'delpage')and($perm_managepages))
  {
    function DeletePageAndChildren($site_id, $page_id)
    {
      $page_id = intval($page_id);
      $data = sqlQueryData("SELECT page_id FROM " . $site_id . "_pages WHERE parent=$page_id");
      $pagedev_id_array = sqlQueryData("select pagedev_id from ".$site_id."_pagesdev where page_id=".$page_id);
      foreach($data as $row)
      {
        DeletePageAndChildren($site_id, $row['page_id']);
      }
      $row = sqlQueryRow("SELECT parent,ind FROM " . $site_id . "_pages WHERE page_id=$page_id");
      sqlQuery("DELETE FROM " . $site_id . "_pages WHERE page_id=$page_id");
      sqlQuery("DELETE FROM " . $site_id . "_pagesdev WHERE page_id=$page_id");
      sqlQuery("UPDATE " . $site_id . "_pages SET ind=ind-1 WHERE parent=" . $row['parent'] . " AND ind>" . $row['ind']);
      sqlQuery("UPDATE " . $site_id . "_pagesdev SET ind=ind-1 WHERE parent=" . $row['parent'] . " AND ind>" . $row['ind']);

      //delete permissions from all related dev pages
      /*foreach ($wappagedev_id_array as $row)
      {
          sqlQuery("DELETE FROM permissions WHERE perm_num=11 AND data=$site_id AND data2='".$row['pagedev_id']."'");
      }*/
      //sqlQuery("DELETE FROM permissions WHERE perm_num=11 AND data=$site_id AND data2=$page_id");
      //delete compiled templates
      sqlQuery("DELETE FROM " . $site_id . "_ctemplates WHERE page_id=" . $page_id);
      //delete search objects and stuff
      IndexPageForSearch($site_id, $page_id);
      DeleteAllCompiledTemplates($site_id);

    }

    function TrashPageAndChildren($site_id, $page_id)
    {
      $page_id = intval($page_id);
      $data = sqlQueryData("SELECT page_id FROM " . $site_id . "_pages WHERE parent=$page_id");
      $pagedev_id_array = sqlQueryData("select pagedev_id from ".$site_id."_pagesdev where page_id=".$page_id);
      foreach($data as $row)
      {
        TrashPageAndChildren($site_id, $row['page_id']);
      }

      $row = sqlQueryRow("SELECT parent,ind FROM " . $site_id . "_pages WHERE page_id=$page_id");
      sqlQuery("UPDATE " . $site_id . "_pages SET in_trash=1 WHERE page_id=$page_id");
      sqlQuery("UPDATE " . $site_id . "_pagesdev SET in_trash=1 WHERE page_id=$page_id");
    }

    $pagedev_id = intval($_GET['id']);
    list($name, $parent) = sqlQueryRow("SELECT name, parent FROM " . $site_id . "_pagesdev WHERE pagedev_id=$pagedev_id");
    $page_id = sqlQueryValue("SELECT page_id FROM " . $site_id . "_pagesdev WHERE pagedev_id=$pagedev_id");
    $perm_delpage = CheckPermission(13, $site_id, $page_id);
    if($perm_delpage)
    {
      TrashPageAndChildren($site_id, $page_id);

/*
      DeletePageAndChildren($site_id, $page_id);
      $c = sqlQueryValue('SELECT COUNT(*) FROM ' . $site_id . '_pages WHERE parent = ' . $parent);
      sqlQuery('UPDATE ' . $site_id . '_pagesdev SET childrencount = ' . $c .  ' WHERE page_id = ' . $parent);
*/
      Add_Log("Trashed page '$name' ($page_id)", $site_id);
      Header("Location: ?module=".$_GET['module']."&site_id=" . $GLOBALS['site_id'] . '&refrframe=1&selrow=' . $_GET['selrow']);
    }else
    {
      Add_Log("Permission denied to delete page '$name' ($page_id)", $site_id);
      Header("Location: ?module=".$_GET['module']."&site_id=" . $GLOBALS['site_id'] . '&selrow=' . $page_id . '&accessdenied=1');
    }

  } else if(($action == 'delpage2')and($perm_managepages))
  {
    //Dzēst vairākus ierakstus
    function TrashPageAndChildren($site_id, $page_id)
    {
      $page_id = intval($page_id);
      $data = sqlQueryData("SELECT page_id FROM " . $site_id . "_pages WHERE parent=$page_id");
      $pagedev_id_array = sqlQueryData("select pagedev_id from ".$site_id."_pagesdev where page_id=".$page_id);
      foreach($data as $row)
      {
        TrashPageAndChildren($site_id, $row['page_id']);
      }

      $row = sqlQueryRow("SELECT parent,ind FROM " . $site_id . "_pages WHERE page_id=$page_id");
      sqlQuery("UPDATE " . $site_id . "_pages SET in_trash=1 WHERE page_id=$page_id");
      sqlQuery("UPDATE " . $site_id . "_pagesdev SET in_trash=1 WHERE page_id=$page_id");
    }


    $pagedev_id = preg_split("/[\s,]+/", $_GET['id'],-1,PREG_SPLIT_NO_EMPTY);
    foreach ($pagedev_id as $pid)
    {
      $pid = intval($pid);
      if($pid)
      {
        $nameparent = sqlQueryRow("SELECT name, parent FROM " . $site_id . "_pagesdev WHERE pagedev_id=$pid");
        if($nameparent)
        {
          list($name, $parent) = $nameparent;
          $page_id = sqlQueryValue("SELECT page_id FROM " . $site_id . "_pagesdev WHERE pagedev_id=$pid");
          $perm_delpage = CheckPermission(13, $site_id, $page_id);
          if($perm_delpage)
          {
            TrashPageAndChildren($site_id, $page_id);
            $c = sqlQueryValue('SELECT COUNT(*) FROM ' . $site_id . '_pages WHERE parent = ' . $parent);
            sqlQuery('UPDATE ' . $site_id . '_pagesdev SET childrencount = ' . $c .  ' WHERE page_id = ' . $parent);
            Add_Log("Trashed page '$name' ($page_id)", $site_id);
            Header("Location: ?module=".$_GET['module']."&site_id=" . $GLOBALS['site_id'] . '&refrframe=1&selrow=' . $_GET['selrow']);
          }else
          {
            Add_Log("Permission denied to trash page '$name' ($page_id)", $site_id);
            Header("Location: ?module=".$_GET['module']."&site_id=" . $GLOBALS['site_id'] . '&selrow=' . $page_id . '&accessdenied=1');
          }
        }
      }
    }

  }else if(($action == 'tbarupdatecontrols')and($perm_managecontents))
  {
    $page_id = intval($_GET['id']);
    $pagedev_id = intval($_GET['pagedev_id']);
    $updated = false;
    $copypage = sqlQueryValue("SELECT copypage FROM " . $site_id . "_pagesdev WHERE pagedev_id=$pagedev_id");

    //echo '<pre>';print_r($_POST);echo '</pre>';
    foreach ($_POST['component'] as $key=>$component)
      {
        //noskaidro bijusho ID veertiibai
        $oldcontID = sqlQueryValue("SELECT contentdev_id FROM " . $site_id . "_contentsdev WHERE pagedev=$pagedev_id AND componentname='" . $component . "' AND propertyname='visible'");
        $contID = $oldcontID;

        $default = sqlQueryValue("SELECT propertyvalue FROM ".$site_id."_contents WHERE page=-1 AND componentname='" . $component . "' AND propertyname='visible'");

        //izdzeesh visas vecaas veertiibas
        sqlQuery("DELETE FROM " . $site_id . "_contentsdev WHERE pagedev=$pagedev_id AND componentname='" . $component . "' AND propertyname='visible'");

        //ieliek jauno visible vertibu t.u.t.t.,ja taa atshkjiras no nokluseetas
        if ($_POST[$component] && $default)
          {
            //echo "on ".$component."<br>";

            //ieliek jauno veertiibu un noskaidro ID
            sqlQuery("INSERT " . $site_id . "_contentsdev SET pagedev=$pagedev_id, componentname='" . $component . "', propertyname='visible', propertyvalue='0'");
            $updated = true;
            $contID = sqlQueryValue("SELECT contentdev_id FROM " . $site_id . "_contentsdev WHERE pagedev=$pagedev_id AND componentname='" . $component . "' AND propertyname='visible'");
          }
        else if (!$_POST[$component] && !$default)
          {
            //echo "off ".$component."<br>";

            //ieliek jauno veertiibu un noskaidro ID
            sqlQuery("INSERT " . $site_id . "_contentsdev SET pagedev=$pagedev_id, componentname='" . $component . "', propertyname='visible', propertyvalue='1'");
            $updated = true;
            $contID = sqlQueryValue("SELECT contentdev_id FROM " . $site_id . "_contentsdev WHERE pagedev=$pagedev_id AND componentname='" . $component . "' AND propertyname='visible'");
          }
      }

    $name = sqlQueryValue("SELECT name FROM " . $site_id . "_pages WHERE page_id=$page_id");
    if($updated)
    {
      //update modification date and user_id
      $modby = $GLOBALS['currentUserID'];
      sqlQuery("UPDATE ".$site_id."_pagesdev SET lastmod='".time()."', modby=$modby WHERE pagedev_id=$pagedev_id");

      if (CheckPermission(31, $site_id, $page_id) || CheckPermission(32, $site_id))
      {
        pageautopublish($site_id, $pagedev_id);
      }

      Add_Log("Updated contents of dev page '$name' ($pagedev_id)", $site_id);
    }

    //Stop the window from closing
    if($_GET['noclose'])
    {

      $docTemplate = "form.htm";
      $docScripts['body'] = "pages.component.php";

    }

    IndexPageForSearch($site_id, $page_id);
    DeleteAllCompiledTemplates($site_id);

    if ($_GET['redirect'])
     Redirect("?module=pages&site_id=".$site_id."&action=components_form_frame&id=".$page_id);


  }else if(($action == 'updatecontrols')and($perm_managecontents))
  {
    $pagedev_id = intval($_GET['pagedev_id']);
    if ($pagedev_id>0)
    {
        $updated = false;
        $copypage = sqlQueryValue("SELECT copypage FROM " . $site_id . "_pagesdev WHERE pagedev_id=$pagedev_id");

        foreach(array_keys($_POST) as $key)
        {
          $s = explode('_', $key, 3);
          if($s[0] == 'component')      {
            //noskaidro bijusho ID veertiibai
            $oldcontID = sqlQueryValue("SELECT contentdev_id FROM " . $site_id . "_contentsdev WHERE pagedev=$pagedev_id AND componentname='" . $s[1] . "' AND propertyname='" . $s[2] . "'");
            $contID = $oldcontID;
            //izdzeesh visas vecaas veertiibas
            sqlQuery("DELETE FROM " . $site_id . "_contentsdev WHERE pagedev=$pagedev_id AND componentname='" . $s[1] . "' AND propertyname='" . $s[2] . "'");
            //ja NAV iekjekseets, ka jaanjem defaultaas veertiibas
            if(!$_POST['check_component_' . $s[1] . '_' . $s[2]])
            {
              //ieliek jauno veertiibu un noskaidro ID
              sqlQuery("INSERT " . $site_id . "_contentsdev SET pagedev=$pagedev_id, componentname='" . $s[1] . "', propertyname='" . $s[2] . "', propertyvalue='" . $_POST[$key] . "'");
              $updated = true;
              $contID = sqlQueryValue("SELECT contentdev_id FROM " . $site_id . "_contentsdev WHERE pagedev=$pagedev_id AND componentname='" . $s[1] . "' AND propertyname='" . $s[2] . "'");
            }else
            {
              if($oldcontID) //ja tiek nomainiita lapas veertiiba uz defaulto
              {
                $updated = true;
              }
            }

          }
        }
        $name = sqlQueryValue("SELECT name FROM " . $site_id . "_pagesdev WHERE pagedev_id=$pagedev_id");
        if($updated)
        {
          //update modification date and user
          $modby = $GLOBALS['currentUserID'];
          sqlQuery("UPDATE ".$site_id."_pagesdev SET lastmod='".time()."', modby=$modby WHERE pagedev_id=$pagedev_id");

          if (CheckPermission(31, $site_id, intval($_GET['page_id'])) || CheckPermission(32, $site_id))
          {
              pageautopublish($site_id, $pagedev_id);
          }

          Add_Log("Updated contents of page '$name' ($pagedev_id)", $site_id);
        }

        //Stop the window from closing
        if($_GET['noclose'])
        {

          $docTemplate = "form.htm";
          $docScripts['body'] = "pages.component.php";

        }

        IndexPageForSearch($site_id, $page_id);
        DeleteAllCompiledTemplates($site_id);

        if ($_GET['redirect'])
            Redirect("?module=pages&site_id=".$site_id."&action=components_form_frame&pagedev_id=".$pagedev_id);
    }
    else
    {

        //save view contents
        $updated = false;
        $page_id = $pagedev_id;
        $copypage = sqlQueryValue("SELECT copypage FROM " . $site_id . "_pages WHERE page_id=$page_id");
        foreach(array_keys($_POST) as $key)
        {
          $s = explode('_', $key, 3);
          if($s[0] == 'component')      {
            //noskaidro bijusho ID veertiibai
            $oldcontID = sqlQueryValue("SELECT content_id FROM " . $site_id . "_contents WHERE page=$page_id AND componentname='" . $s[1] . "' AND propertyname='" . $s[2] . "'");
            $contID = $oldcontID;
            //izdzeesh visas vecaas veertiibas
            sqlQuery("DELETE FROM " . $site_id . "_contents WHERE page=$page_id AND componentname='" . $s[1] . "' AND propertyname='" . $s[2] . "'");
            //ja NAV iekjekseets, ka jaanjem defaultaas veertiibas
            if(!$_POST['check_component_' . $s[1] . '_' . $s[2]])
            {
              //ieliek jauno veertiibu un noskaidro ID
              sqlQuery("INSERT " . $site_id . "_contents SET page=$page_id, componentname='" . $s[1] . "', propertyname='" . $s[2] . "', propertyvalue='" . $_POST[$key] . "'");
              $updated = true;
              $contID = sqlQueryValue("SELECT content_id FROM " . $site_id . "_contents WHERE page=$page_id AND componentname='" . $s[1] . "' AND propertyname='" . $s[2] . "'");
            }
          }
        }

        //Stop the window from closing
        if($_GET['noclose'])
        {
          $docTemplate = "form.htm";
          $docScripts['body'] = "pages.component.php";
        }

        DeleteAllCompiledTemplates($site_id);

        if ($_GET['redirect'])
            Redirect("?module=pages&site_id=".$site_id."&action=components_form_frame&pagedev_id=".$pagedev_id);
    }

  }else if(($action == 'updatecontrolsdef')and($perm_managecontents))
  {
    $page_id = -1;
    $updated = false;

    foreach(array_keys($_POST) as $key)
    {
      $s = explode('_', $key, 3);
      if($s[0] == 'component')      {
        //noskaidro bijusho ID veertiibai
        $oldcontID = sqlQueryValue("SELECT content_id FROM " . $site_id . "_contents WHERE page=-1 AND componentname='" . $s[1] . "' AND propertyname='" . $s[2] . "'");
        $contID = $oldcontID;
        //izdzeesh visas vecaas veertiibas
        sqlQuery("DELETE FROM " . $site_id . "_contents WHERE page=-1 AND componentname='" . $s[1] . "' AND propertyname='" . $s[2] . "'");
        //ja NAV iekjekseets, ka jaanjem defaultaas veertiibas

        //ieliek jauno veertiibu un noskaidro ID
        sqlQuery("INSERT " . $site_id . "_contents SET page=-1, componentname='" . $s[1] . "', propertyname='" . $s[2] . "', propertyvalue='" . $_POST[$key] . "'");
        $updated = $s[1];
        $contID = sqlQueryValue("SELECT content_id FROM " . $site_id . "_contents WHERE page=-1 AND componentname='" . $s[1] . "' AND propertyname='" . $s[2] . "'");

      }
    }
    if($updated)
    {
      Add_Log("Updated default values of component '$updated'", $site_id);
    }

    //Stop the window from closing
    if($_GET['noclose'])
    {

      $docTemplate = "form.htm";
      $docScripts['body'] = "pages.component.php";

    }

    DeleteAllCompiledTemplates($site_id);

  }else if(($action == 'clearcashe')and($perm_managepages))
  {
    DeleteAllCompiledTemplates($site_id);
  }else if(($action == 'setpageenabled')and($perm_managepages))
  {
    $pagedev_id = intval($_GET['id']);
    $modby = $GLOBALS['currentUserID'];
    $page_id = sqlQueryValue("SELECT page_id FROM ".$site_id."_pagesdev WHERE pagedev_id=".$pagedev_id);
    sqlQuery("UPDATE " . $site_id . "_pagesdev SET enabled='1', lastmod='".time()."', modby=$modby WHERE pagedev_id='$pagedev_id'");
    if (CheckPermission(31, $site_id, $page_id) || CheckPermission(32, $site_id))
    {
        pageautopublish($site_id, $pagedev_id);
    }
    DeleteAllCompiledTemplates($site_id);

  }else if(($action == 'unsetpageenabled')and($perm_managepages))
  {
    $pagedev_id = intval($_GET['id']);
    $modby = $GLOBALS['currentUserID'];
    $page_id = sqlQueryValue("SELECT page_id FROM ".$site_id."_pagesdev WHERE pagedev_id=".$pagedev_id);
    sqlQuery("UPDATE " . $site_id . "_pagesdev SET enabled='0', lastmod='".time()."', modby=$modby WHERE pagedev_id='$pagedev_id'");
    if (CheckPermission(31, $site_id, $page_id) || CheckPermission(32, $site_id))
    {
        pageautopublish($site_id, $pagedev_id);
    }
    DeleteAllCompiledTemplates($site_id);

  }else if(($action == 'setpagevisible')and($perm_managepages))
  {
    $pagedev_id = intval($_GET['id']);
    $modby = $GLOBALS['currentUserID'];
    $page_id = sqlQueryValue("SELECT page_id FROM ".$site_id."_pagesdev WHERE pagedev_id=".$pagedev_id);
    sqlQuery("UPDATE " . $site_id . "_pagesdev SET visible='1', lastmod='".time()."', modby=$modby WHERE pagedev_id='$pagedev_id'");
    if (CheckPermission(31, $site_id, $page_id) || CheckPermission(32, $site_id))
    {
        pageautopublish($site_id, $pagedev_id);
    }
    DeleteAllCompiledTemplates($site_id);

  }else if(($action == 'unsetpagevisible')and($perm_managepages))
  {
    $pagedev_id = intval($_GET['id']);
    $modby = $GLOBALS['currentUserID'];
    $page_id = sqlQueryValue("SELECT page_id FROM ".$site_id."_pagesdev WHERE pagedev_id=".$pagedev_id);
    sqlQuery("UPDATE " . $site_id . "_pagesdev SET visible='0', lastmod='".time()."', modby=$modby WHERE pagedev_id='$pagedev_id'");
    if (CheckPermission(31, $site_id, $page_id) || CheckPermission(32, $site_id))
    {
        pageautopublish($site_id, $pagedev_id);
    }
    DeleteAllCompiledTemplates($site_id);

  }else if(($action == 'moveup')and($perm_managepages))
  {
    $pagedev_id = intval($_GET['id']);
    $page_id = sqlQueryValue("SELECT page_id FROM ".$site_id."_pagesdev WHERE pagedev_id=".$pagedev_id);
    $row = sqlQueryRow("SELECT parent, ind, language FROM " . $site_id . "_pages WHERE page_id=$page_id");

    if($row != null)
    {
      //Obtain the page_id above
      $aboverow = sqlQueryRow("SELECT ind, page_id FROM " . $site_id . "_pages WHERE parent=" . $row['parent'] . " AND language=".intval($row['language'])." AND in_trash=0 AND ind<" . $row['ind'] . " ORDER BY ind DESC LIMIT 1");

			if($aboverow != null)
      {
        sqlQuery("UPDATE " . $site_id . "_pages SET ind=" . $aboverow['ind'] . " WHERE page_id=$page_id");
        sqlQuery("UPDATE " . $site_id . "_pages SET ind=" . $row['ind'] . " WHERE page_id=" . $aboverow['page_id']);

        //set this ind to all dev pages of this page
        sqlQuery("UPDATE " . $site_id . "_pagesdev SET ind=" . $aboverow['ind'] . " WHERE page_id=$page_id");
        sqlQuery("UPDATE " . $site_id . "_pagesdev SET ind=" . $row['ind'] . " WHERE page_id=" . $aboverow['page_id']);
      }
    }
    if (CheckPermission(31, $site_id, $page_id) || CheckPermission(32, $site_id))
    {
        pageautopublish($site_id, $pagedev_id);
    }
    DeleteAllCompiledTemplates($site_id);
  }else if(($action == 'movedown')and($perm_managepages))
  {
    $pagedev_id = intval($_GET['id']);
    $page_id = sqlQueryValue("SELECT page_id FROM ".$site_id."_pagesdev WHERE pagedev_id=".$pagedev_id);
    $row = sqlQueryRow("SELECT parent, ind, language FROM " . $site_id . "_pages WHERE page_id=$page_id");
    if($row != null)
    {
      //Obtain the page_id below

      $belowrow = sqlQueryRow("SELECT ind, page_id FROM " . $site_id . "_pages WHERE parent=" . $row['parent'] . " AND language=".intval($row['language'])." AND in_trash=0 AND ind>" . $row['ind'] . " ORDER BY ind LIMIT 1");

      if($belowrow != null)
      {
        sqlQuery("UPDATE " . $site_id . "_pages SET ind=" . $belowrow['ind'] . " WHERE page_id=$page_id");
        sqlQuery("UPDATE " . $site_id . "_pages SET ind=" . $row['ind'] . " WHERE page_id=" . $belowrow['page_id']);

        //same for pagesdev
        sqlQuery("UPDATE " . $site_id . "_pagesdev SET ind=" . $belowrow['ind'] . " WHERE page_id=$page_id");
        sqlQuery("UPDATE " . $site_id . "_pagesdev SET ind=" . $row['ind'] . " WHERE page_id=" . $belowrow['page_id']);
      }
    }
    if (CheckPermission(31, $site_id, $page_id) || CheckPermission(32, $site_id))
    {
        pageautopublish($site_id, $pagedev_id);
    }
    DeleteAllCompiledTemplates($site_id);
  }else if($action == "page_redirect")
  {
    $page_id = intval($_GET['id']);
    $pagedev_id = intval($_GET['pagedev_id']);
    //Obtain domain
    $domain = $GLOBALS['domain'];

    //preview dev page
    if ($_GET['showdevpage']==1)
    {
        $param = "?showdevpage=".$pagedev_id;
    }
    else if ($_GET['pagehistory_id'])
    {
        $param = "?pagehistory_id=".$_GET['pagehistory_id'];
    }
    else
        $param = "";

    $path = PagePathById($page_id, $site_id);
    Redirect('http://' . $domain . '/' . $path . $param);
    DeleteAllCompiledTemplates($site_id);
  } else if($action == "publish")
  {
    //publish page if 'Can Publish All Pages' permission is set for current user
    $perm_publish = CheckPermission(33, $site_id);
    if($perm_publish)
    {
        //current dev page data
        $row = sqlQueryRow("select * from ".$site_id."_pagesdev where pagedev_id='".$_GET['pagedev_id']."'");

        //update normal page using dev page data
        sqlQuery("UPDATE " . $site_id . "_pages SET
            name='".$row['name']."',
            title='".$row['title']."',
            description='".$row['description']."',
            template='".$row['template']."',
            language='".$row['language']."',
            lastmod='".$row['lastmod']."',
            modby='".$row['modby']."',
            enabled='".$row['enabled']."',
            visible='".$row['visible']."',
            passprotect='".$row['passprotect']."',
            ind='".$row['ind']."',
            parent='".$row['parent']."',
            redirect='".$row['redirect']."',
            copypage='".$row['copypage']."',
            redirect_url='".$row['redirect_url']."'
            WHERE page_id='".$row['page_id']."'");

        //delete old contents of the original page
        sqlQuery("DElETE FROM ".$site_id."_contents WHERE page=".$row['page_id']);
        //delete old strings of the original page
        sqlQuery("DElETE FROM ".$site_id."_strings WHERE page=".$row['page_id']);

        //insert new contents for the original page from dev page
        sqlQuery("INSERT INTO ".$site_id."_contents (page,componentname,propertyname,propertyvalue) SELECT ".$row['page_id']." as page, componentname, propertyname, propertyvalue FROM ".$site_id."_contentsdev WHERE pagedev=".intval($_GET['pagedev_id']));
        /*$cdata = sqlQueryData("SELECT ".$row['page_id']." as page, componentname, propertyname, propertyvalue FROM ".$site_id."_contentsdev WHERE pagedev=".$_GET['pagedev_id']);
        foreach ($cdata as $ckey=>$crow)
            sqlQuery("INSERT INTO ".$site_id."_contents (page,componentname,propertyname,propertyvalue) VALUES (".$crow['page'].",'".$crow['componentname']."','".$crow['propertyname']."','".$crow['propertyvalue']."')");*/


        //insert new strings for the original page from dev page
        sqlQuery("INSERT INTO ".$site_id."_strings (page,stringname,stringvalue) SELECT ".$row['page_id']." as page, stringname, stringvalue FROM ".$site_id."_stringsdev WHERE pagedev=".intval($_GET['pagedev_id']));
        /*$sdata = sqlQueryData("SELECT ".$row['page_id']." as page, stringname, stringvalue FROM ".$site_id."_stringsdev WHERE pagedev=".$_GET['pagedev_id']);
        foreach ($sdata as $skey=>$srow)
            sqlQuery("INSERT INTO ".$site_id."_strings (page,stringname,stringvalue) VALUES (".$srow['page'].",'".$srow['stringname']."','".$srow['stringvalue']."')");*/

        //dev page count
        $pagevercount = sqlQueryValue("select count(pagedev_id) from ".$site_id."_pagesdev where page_id='".$row['page_id']."'");

        //max dev page count defined in config.php
        if (isset($GLOBALS['DevPageNum']) && intval($GLOBALS['DevPageNum'])>0)
            $maxdev = intval($GLOBALS['DevPageNum']);
        else
            $maxdev = 5; //default value

        //delete the oldest
        if ($pagevercount>=$maxdev)
        {
            $oldest = sqlQueryValue("select pagedev_id, min(lastmod) from ".$site_id."_pagesdev where page_id=".$row['page_id']." group by pagedev_id");
            sqlQuery("delete from ".$site_id."_pagesdev where pagedev_id=".$oldest);
            sqlQuery("delete from ".$site_id."_contentsdev where pagedev=".$oldest);
            sqlQuery("delete from ".$site_id."_stringsdev where pagedev=".$oldest);
        }

        //page dev copy
        sqlQuery("INSERT INTO " . $site_id . "_pagesdev (page_id,name,title,description,template,language,lastmod,created,modby,enabled,visible,passprotect,parent,ind,redirect,copypage,redirect_url) VALUES (".$row['page_id'].",'".$row['name']."','".$row['title']."','".$row['description']."','".$row['template']."','".$row['language']."','".$row['lastmod']."','".$row['created']."','".$row['modby']."','".$row['enabled']."','".$row['visible']."','".$row['passprotect']."',".$row['parent'].",".$row['ind'].",".$row['redirect'].",".$row['copypage'].",".$row['redirect_url'].")");

        $newdev = sqlLastID();

        //copy contents for new page dev copy
        //sqlQuery("INSERT INTO ".$site_id."_contentsdev (pagedev,componentname,propertyname,propertyvalue) select ".$newdev." as 'pagedev',componentname,propertyname,propertyvalue from ".$site_id."_contentsdev where pagedev=".$row['pagedev_id']);
        $cdata = sqlQueryData("SELECT ".$newdev." AS 'pagedev',componentname,propertyname,propertyvalue FROM ".$site_id."_contentsdev WHERE pagedev=".$row['pagedev_id']);
        foreach ($cdata as $ckey=>$crow)
            sqlQuery("INSERT INTO ".$site_id."_contentsdev (pagedev,componentname,propertyname,propertyvalue) VALUES (".$crow['pagedev'].",'".$crow['componentname']."','".$crow['propertyname']."','".$crow['propertyvalue']."')");

        //copy strings for new page dev copy
        //sqlQuery("INSERT INTO ".$site_id."_stringsdev (pagedev,stringname,stringvalue) select ".$newdev." as 'pagedev',stringname,stringvalue from ".$site_id."_stringsdev where pagedev=".$row['pagedev_id']);
        $sdata = sqlQueryData("SELECT ".$newdev." AS 'pagedev',stringname,stringvalue FROM ".$site_id."_stringsdev WHERE pagedev=".$row['pagedev_id']);
        foreach ($sdata as $skey=>$srow)
            sqlQuery("INSERT INTO ".$site_id."_stringsdev (pagedev,stringname,stringvalue) VALUES (".$srow['pagedev'].",'".$srow['stringname']."','".$srow['stringvalue']."')");

        if ($GLOBALS['StorePageHistory']) PageHistory($site_id, $row['page_id'], $row['title']);
        IndexPageForSearch($site_id, $row['page_id']);
        DeleteAllCompiledTemplates($site_id);

        Add_Log("Published page '".$row['name']."' (".$row['page_id'].")", $site_id);
    }else
    {
        Add_Log("Permission denied to publish page '$name' ($page_id)", $site_id);
        Header("Location: ?module=".$_GET['module']."&site_id=" . $GLOBALS['site_id'] . '&selrow=' . $page_id . '&accessdenied=1');
    }
  }
else if($action == "restore_history")
  {
    //restore page if 'Can Publish All Pages' permission is set for current user
    $perm_publish = CheckPermission(33, $site_id);
    if($perm_publish)
    {
     if (!isset($_GET['published']))
     {
        //restore contents from dev page to active dev page

        $pagedev_id = intval($_GET['pagedev_id']);
        $page_id = intval($_GET['id']);

        //active dev page id
        $pagedev_id_a = sqlQueryValue("SELECT pagedev_id, lastmod FROM ".$site_id."_pagesdev WHERE page_id=".$page_id." ORDER BY lastmod DESC LIMIT 1");

        //get data from source dev page
        $row = sqlQueryRow("select * from ".$site_id."_pagesdev where pagedev_id='".intval($_GET['pagedev_id'])."'");

        //update active dev page
        sqlQuery("UPDATE " . $site_id . "_pagesdev SET
            name='".$row['name']."',
            title='".$row['title']."',
            description='".$row['description']."',
            template='".$row['template']."',
            language='".$row['language']."',
            modby='".$row['modby']."',
            enabled='".$row['enabled']."',
            visible='".$row['visible']."',
            passprotect='".$row['passprotect']."',
            parent='".$row['parent']."',
            redirect='".$row['redirect']."',
            copypage='".$row['copypage']."',
            redirect_url='".$row['redirect_url']."
            WHERE pagedev_id=".$pagedev_id_a);

        //delete old contents of the active dev page
        sqlQuery("DELETE FROM ".$site_id."_contentsdev WHERE pagedev=".$pagedev_id_a);
        //delete old strings of the active dev page
        sqlQuery("DELETE FROM ".$site_id."_stringsdev WHERE pagedev=".$pagedev_id_a);

        //insert new contents for the active dev page from source dev page
        //sqlQuery("INSERT INTO ".$site_id."_contentsdev (pagedev,componentname,propertyname,propertyvalue) SELECT pagedev, componentname, propertyname, propertyvalue FROM ".$site_id."_contentsdev WHERE pagedev=".$pagedev_id_a);

        $contents = sqlQueryData("SELECT pagedev, componentname, propertyname, propertyvalue FROM ".$site_id."_contentsdev WHERE pagedev=".$pagedev_id);
        foreach ($contents as $c)
         {
            sqlQuery("INSERT INTO ".$site_id."_contentsdev (pagedev,componentname,propertyname,propertyvalue) VALUES (".$pagedev_id_a.",'".$c['componentname']."','".$c['propertyname']."','".$c['propertyvalue']."')");
         }
        $strings = sqlQueryData("SELECT pagedev, stringname, stringvalue FROM ".$site_id."_stringsdev WHERE pagedev=".$pagedev_id);
        foreach ($strings as $s)
         {
            sqlQuery("INSERT INTO ".$site_id."_stringsdev (pagedev,stringname,stringvalue) VALUES (".$pagedev_id_a.",'".$s['stringname']."','".$s['stringvalue']."')");
         }
     }
     else
     {
         //restoring from published page

         $page_id = intval($_GET['page_id']);

         //source page data
         $row = sqlQueryRow("SELECT * FROM ".$site_id."_pages WHERE page_id='".$page_id."'");

         //active dev page id
         $pagedev_id_a = sqlQueryValue("SELECT pagedev_id, lastmod FROM ".$site_id."_pagesdev WHERE page_id=".$page_id." ORDER BY lastmod DESC LIMIT 1");

         //update active dev page using published page data
         sqlQuery("UPDATE " . $site_id . "_pagesdev SET
            name='".$row['name']."',
            title='".$row['title']."',
            description='".$row['description']."',
            template='".$row['template']."',
            language='".$row['language']."',
            modby='".$row['modby']."',
            enabled='".$row['enabled']."',
            visible='".$row['visible']."',
            passprotect='".$row['passprotect']."',
            parent='".$row['parent']."',
            redirect='".$row['redirect']."',
            copypage='".$row['copypage']."',
            redirect_url='".$row['redirect_url']."'
            WHERE pagedev_id=".$pagedev_id_a);

         //delete old contents of the active dev page
         sqlQuery("DELETE FROM ".$site_id."_contentsdev WHERE pagedev=".$pagedev_id_a);
         //delete old strings of the active dev page
         sqlQuery("DELETE FROM ".$site_id."_stringsdev WHERE pagedev=".$pagedev_id_a);

         //restore contents
         sqlQuery("INSERT INTO ".$site_id."_contentsdev (pagedev,componentname,propertyname,propertyvalue) SELECT ".$pagedev_id_a." as pagedev, componentname, propertyname, propertyvalue FROM ".$site_id."_contents WHERE page=".$page_id);
         //restore strings
         sqlQuery("INSERT INTO ".$site_id."_stringsdev (pagedev,stringname,stringvalue) SELECT ".$pagedev_id_a." as pagedev, stringname, stringvalue FROM ".$site_id."_strings WHERE page=".$page_id);

     }
     if ($_GET['publish']==1)
     {
       //active dev page id
       $pagedev_id_a = sqlQueryValue("SELECT pagedev_id, lastmod FROM ".$site_id."_pagesdev WHERE page_id=".$page_id." ORDER BY lastmod DESC LIMIT 1");
       pageautopublish($site_id, $pagedev_id_a);
     }
     else
     {
       //update modification date and user_id
       $modby = $GLOBALS['currentUserID'];
       sqlQuery("UPDATE ".$site_id."_pagesdev SET lastmod='".time()."', modby=$modby WHERE pagedev_id=$pagedev_id");
     }
     Add_Log("Restored page '".$row['name']."' ($page_id)", $site_id);
    }else
    {
     Add_Log("Permission denied to restore page '".$row['name']."' ($page_id)", $site_id);
     Header("Location: ?module=".$_GET['module']."&site_id=" . $GLOBALS['site_id'] . '&selrow=' . $page_id . '&accessdenied=1');
    }

  }
  else if($action == "addcomponentsave")
  {
        //print_R($_GET);
        //print_R($_POST);
        $pagedev_id = intval($_GET['pagedev_id']);
        $page_id = intval($_GET['page_id']);
        $site_id = intval($_GET['site_id']);

        if($page_id>0)
        {   //real page
          $template = sqlQueryValue("SELECT template FROM ". $site_id . "_pagesdev WHERE pagedev_id=$pagedev_id");
        }

        if($template)
        {
          list($data, $copybody) = sqlQueryRow("SELECT body, copybody FROM " . $site_id . "_templates WHERE template_id=$template");
          while($copybody)
            list($data, $copybody) = sqlQueryRow("SELECT body, copybody FROM " . $site_id . "_templates WHERE template_id=$copybody");
        }

        //$data = attachSubTemplates($data, $site_id);

        if (ereg("{%component:".$_GET['component_type'].":".$_GET['component_name']."(:[a-zA-Z0-9% \\.\\,\\\"\\\'\\#\\!-]+)?%}", $data, $regs))
        {
            //echo $regs[0];
            $params = "";

            if (!$_POST['visualedit'])
                $params = 'novisualedit "1" ';
            if ($_POST['height'])
                $params .= 'height "'.intval($_POST['height']).'" ';
            if ($_POST['width'])
                $params .= 'width "'.intval($_POST['width']).'" ';

            if ($params) $params = ':'.$params;

            if ($_POST['position']=="above")
                $data = str_replace($regs[0],"{%component:".$_POST['combocomponents'].":".$_POST['componentname'].$params."%}"."\r\n".$regs[0], $data);
            else if ($_POST['position']=="below")
                $data = str_replace($regs[0], $regs[0]."\r\n"."{%component:".$_POST['combocomponents'].":".$_POST['componentname'].$params."%}", $data);
        }

        sqlQuery("UPDATE ".$site_id."_templates SET body='".$data."' WHERE template_id=".$template);

        if ($_POST['pagevisibility']=="this")
        {
            sqlQuery("INSERT INTO ".$site_id."_contents (page,componentname,propertyname,propertyvalue) VALUES (-1,'".$_POST['componentname']."','visible','1')");
            sqlQuery("INSERT INTO ".$site_id."_contentsdev (pagedev,componentname,propertyname,propertyvalue) VALUES (".$pagedev_id.",'".$_POST['componentname']."','visible','0')");
        }

  }
  else if($action == "trash_restore")
  {
    function RestorePageAndChildren($site_id, $page_id)
    {
      $page_id = intval($page_id);
      $data = sqlQueryData("SELECT page_id FROM " . $site_id . "_pages WHERE parent=$page_id");
      $pagedev_id_array = sqlQueryData("select pagedev_id from ".$site_id."_pagesdev where page_id=".$page_id);
      foreach($data as $row)
      {
        RestorePageAndChildren($site_id, $row['page_id']);
      }

      $row = sqlQueryRow("SELECT parent,ind FROM " . $site_id . "_pages WHERE page_id=$page_id");
      sqlQuery("UPDATE " . $site_id . "_pages SET in_trash=0 WHERE page_id=$page_id");
      sqlQuery("UPDATE " . $site_id . "_pagesdev SET in_trash=0 WHERE page_id=$page_id");
    }

    $ids = explode(',', $_GET['ids']);

    foreach ($ids as $pagedev_id)
    {
      list($name, $parent) = sqlQueryRow("SELECT name, parent FROM " . $site_id . "_pagesdev WHERE pagedev_id=$pagedev_id");
      $page_id = sqlQueryValue("SELECT page_id FROM " . $site_id . "_pagesdev WHERE pagedev_id=$pagedev_id");
      RestorePageAndChildren($site_id, $page_id);

      Add_Log("Restored page from trash '$name' ($page_id)", $site_id);
    }

    header("Location: ?module=".$_GET['module']."&action=trash&site_id=" . $GLOBALS['site_id'] . '&refrframe=1');
  }
  else if($action == "trash_delete")
  {
    function DeletePageAndChildren($site_id, $page_id)
    {
      $page_id = intval($page_id);
      $data = sqlQueryData("SELECT page_id FROM " . $site_id . "_pages WHERE parent=$page_id");
      $pagedev_id_array = sqlQueryData("select pagedev_id from ".$site_id."_pagesdev where page_id=".$page_id);
      foreach($data as $row)
      {
        DeletePageAndChildren($site_id, $row['page_id']);
      }
      $row = sqlQueryRow("SELECT parent,ind FROM " . $site_id . "_pages WHERE page_id=$page_id");
      sqlQuery("DELETE FROM " . $site_id . "_pages WHERE page_id=$page_id");
      sqlQuery("DELETE FROM " . $site_id . "_pagesdev WHERE page_id=$page_id");
      sqlQuery("UPDATE " . $site_id . "_pages SET ind=ind-1 WHERE parent=" . $row['parent'] . " AND ind>" . $row['ind']);
      sqlQuery("UPDATE " . $site_id . "_pagesdev SET ind=ind-1 WHERE parent=" . $row['parent'] . " AND ind>" . $row['ind']);

      //delete permissions from all related dev pages
      /*foreach ($wappagedev_id_array as $row)
      {
          sqlQuery("DELETE FROM permissions WHERE perm_num=11 AND data=$site_id AND data2='".$row['pagedev_id']."'");
      }*/
      //sqlQuery("DELETE FROM permissions WHERE perm_num=11 AND data=$site_id AND data2=$page_id");
      //delete compiled templates
      sqlQuery("DELETE FROM " . $site_id . "_ctemplates WHERE page_id=" . $page_id);
      //delete search objects and stuff
      IndexPageForSearch($site_id, $page_id);
      DeleteAllCompiledTemplates($site_id);

    }

    $pagedev_id = explode(',', $_GET['ids']);
    foreach ($pagedev_id as $pid)
    {
      $pid = intval($pid);
      if($pid)
      {
        $nameparent = sqlQueryRow("SELECT name, parent FROM " . $site_id . "_pagesdev WHERE pagedev_id=$pid");
        if($nameparent)
        {
          list($name, $parent) = $nameparent;
          $page_id = sqlQueryValue("SELECT page_id FROM " . $site_id . "_pagesdev WHERE pagedev_id=$pid");
          $perm_delpage = CheckPermission(13, $site_id, $page_id);
          if($perm_delpage)
          {
            DeletePageAndChildren($site_id, $page_id);
            $c = sqlQueryValue('SELECT COUNT(*) FROM ' . $site_id . '_pages WHERE parent = ' . $parent);
            sqlQuery('UPDATE ' . $site_id . '_pagesdev SET childrencount = ' . $c .  ' WHERE page_id = ' . $parent);
            Add_Log("Trashed page '$name' ($page_id)", $site_id);
            Header("Location: ?module=".$_GET['module']."&site_id=" . $GLOBALS['site_id'] . '&refrframe=1&selrow=' . $_GET['selrow']);
          }else
          {
            Add_Log("Permission denied to trash page '$name' ($page_id)", $site_id);
            Header("Location: ?module=".$_GET['module']."&site_id=" . $GLOBALS['site_id'] . '&selrow=' . $page_id . '&accessdenied=1');
          }
        }
      }
    }


    header("Location: ?module=".$_GET['module']."&action=trash&site_id=" . $GLOBALS['site_id'] . '&refrframe=1');
  }

?>
