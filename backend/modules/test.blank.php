<a href="#" OnClick="javascript:openDlgWin('?module=test&action=form', 0);">Test Form</a><br><br>
<a href="#" OnClick="javascript:openDlgWin('?module=test&action=form', 0, 400, 410);">Test Form 2</a><br><br>
<a href="#" OnClick="javascript:openDlgWin('?module=test&action=ceditor', 400, 410);">Content Editor</a><br><br>
<?

// Let's try table generator

$cols = Array(

  "title"        => Array(
    "width"     => "30%",
    "title"     => "Title",
    "format"    => "<a href=\"#\" OnClick=\"javascript:openDlgWin('?module=test&action=form&id={%id%}', 400, 420);\">{%title%}</a>"
  ),

  "domain"    => Array(
    "width"     => "25%",
    "title"     => "Domain"
  ),

  "created"     => Array(
    "width"     => "15%",
    "title"     => "Created"
  ),

  "modified"    => Array(
    "width"     => "15%",
    "title"     => "Modified"
  ),

  "modby"       => Array(
    "width"     => "15%",
    "title"     => "Modified By"
  )

);

$data[0]['id'] = 1;
$data[0]['title'] = 'Super Cool Website';
$data[0]['domain'] = 'www.supercool.com';
$data[0]['created'] = '11/11/2002 12:34';
$data[0]['modified'] = '11/11/2002 12:34';
$data[0]['modby'] = 'admin';

$data[1]['id'] = 2;
$data[1]['title'] = 'Another Cool Web';
$data[1]['domain'] = 'www.anothercoolweb.com';
$data[1]['created'] = '11/11/2002 12:34';
$data[1]['modified'] = '11/11/2002 12:34';
$data[1]['modby'] = 'sexyboy';

require($GLOBALS['cfgDirRoot']."library/"."class.datagrid.php");

$Table = new DataGrid();
$Table->cols = $cols;
$Table->data = $data;
$Table->footer = false;
echo $Table->output();


?>
