<?
$action = $_GET["action"];
$GLOBALS['site_id'] = $_GET["site_id"];
$site_id = $_GET["site_id"];

if(!$site_id)
  die("No site specified");

$GLOBALS['tabs'] = 'site';
//check site license
$domain = $GLOBALS['domain'];
if(!CheckLicense($domain))
{
  RedirectNoLicense();
}
unset($perm_modsite);
if(($site_id == $GLOBALS["currentUserSiteID"])or(!$GLOBALS["currentUserSiteID"]))
  $perm_modsite = CheckPermission(6, $site_id);
if((!$perm_modsite)and($action!="simple"))
  AccessDenied(False);

unset($perm_managepages);
if(($site_id == $GLOBALS["currentUserSiteID"])or(!$GLOBALS["currentUserSiteID"]))
  $perm_managepages = CheckPermission(8, $site_id);

if ($action=="simple" || $action=="" || $action=="trash") {
  if (isset($_GET["collapsebranch"])) $_SESSION['expanded' . $_GET['collapsebranch']] = 0;
  if (isset($_GET["expandbranch"])) $_SESSION['expanded' . $_GET['expandbranch']] = 1;
}
if($action!='copytree2'&&$action!='delpage2')
{
  $_GET['id'] = intval($_GET['id']);
  $GLOBALS['PageEncoding'] = PageEncoding($_GET['id'], $site_id);
  $GLOBALS['maincharset']  = PageEncoding($_GET['id'], $site_id, false);
}
$id = $_GET['id'];

$docScripts['sellanguage'] = "sellanguage.default.php";
include("sellanguage.setlanguage.php");
$GLOBALS['havelanguages'] = HaveLanguages($site_id);
$GLOBALS['PageEncoding'] = FullEncoding($GLOBALS['currentlanguage']);
$GLOBALS['maincharset']  = $GLOBALS['currentlanguage'];

switch ($action) {

  case "create":
    $docTemplate = "form.htm";
    $GLOBALS['resizeauto'] = 1;
    $docTitle = "{%langstr:create_page%}";
    $docScripts['body'] = "pages.properties.php";
    break;

  // site properties form
  case "properties_form":
    $docTemplate = "form.htm";
    $GLOBALS['resizeauto'] = 1;
    $docScripts['body'] = "pages.properties.php";

    $opt = getOptionByPath('_pages\\'.intval($_GET['id']), 0, $site_id);
    $docScripts['body'] = Array(
      "{%langstr:page_mainprops%}" => "pages.properties.php",
      "{%langstr:page_extraprops%}" => "pages.options.php",
    );

    if (getLanguages())
      $docScripts['body']["{%langstr:page_langpages%}"] = "pages.langpages.php";

    break;

  case "search_form":
    $docTemplate = "form.htm";
    $GLOBALS['resizeauto'] = 1;
    $docScripts['body'] = "pages.searchform.php";
    break;

  case "copytree":
    $docTemplate = "form.htm";
    $docTitle = "{%langstr:copy_tree_with_pages%}";
    $GLOBALS['resizeauto'] = 1;
    $docScripts['body'] = "pages.copyform.php";
    break;

  case "copytree2":
    $docTemplate = "form.htm";
    $docTitle = "{%langstr:copy_tree_with_pages%}";
    $GLOBALS['resizeauto'] = 1;
    $docScripts['body'] = "pages.copyform2.php";
    break;

  case "docopy":
    $docTemplate = "close.htm";
    $docScripts['exec'] = "pages.exec.php";
    break;

  case "docopy2":
    $docTemplate = "close.htm";
    $docScripts['exec'] = "pages.exec.php";
    break;

  //for displaying all components in the page
  case "components_form_frame":
    $docTemplate = "form.htm";
    $GLOBALS['resizeauto'] = 0;
    if ($_GET['component'])
        $docScripts['body'] = "pages.component_contenteditor.php";
    else
        $docScripts['body'] = "pages.components.php";
    //check if permitted to edit page
    if(intval($_GET['id'])>0)
      $perm_managecontents = CheckPermission(11, $site_id, intval($_GET['id']));
    else
      $perm_managecontents = CheckPermission(9, $site_id);
    break;

  //for displaying all components in the page
/*  case "components_form":
          $docTemplate = "lefttree_components.html";
          $docScripts['body'] = "pages.tree_components.php";
    //$docTemplate = "form.htm";
    $GLOBALS['resizeauto'] = 0;
    //$docScripts['body'] = "pages.components.php";
    //check if permitted to edit page
    if($_GET['id']>0)
      $perm_managecontents = CheckPermission(11, $site_id, $_GET['id']);
    else
      $perm_managecontents = CheckPermission(9, $site_id);
    break;
*/

  case "components_form":
    $docTemplate = "frameset2.htm";
    $domain = $GLOBALS['domain'];
    $url = parse_url($REQUEST_URI);
    //$url['query'] = str_replace("components_visual_frame", "components_graph", $url['query']);
    $docStrings['rightframesrc'] = "?module=pages&site_id=" . $GLOBALS['site_id'] . "&pagedev_id=" . $_GET['id'] ."&action=components_form_frame";
    $docStrings['leftframesrc'] = "?module=pageframe_components&site_id=" . $GLOBALS['site_id'] . "&pagedev_id=" . $_GET['id'] . "&page_id=".$_GET['page_id'];
    break;


  //for displaying a single component
  case "component_form":
    $docTitle = "{%langstr:content_editor%}";
    $docTemplate = "dlg_cform.htm";
    if ($_GET['template_editor']) $param = "&template_editor=1";
    else $param = "";
    $docStrings['frame'] = '?module=pages&site_id='.$_GET['site_id'].'&id='.$_GET['id'].'&action=component_frame&component_name='.$_GET['component_name'].'&component_type='.$_GET['component_type'].$param;
    $docStrings['celement_selector'] = createComponentCombobox($_GET['site_id'],$_GET['id'],$_GET['component_name'],$_GET['template_editor']);
    break;

  case "component_frame":
    $docTemplate = "form.htm";
    $docScripts['body'] = "pages.component.php";
    if($_GET['id']>0)
      if ($_GET['template_editor'])
        $perm_managecontents = CheckPermission(12, $site_id, $_GET['id']);
      else
      {
        //"id" parameter is now pagedev_id for pages
        $perm_managecontents = CheckPermission(11, $site_id, $_GET['page_id']);
      }
    else
      $perm_managecontents = CheckPermission(9, $site_id);
    $perm_managecontents = 1;
    break;

  case "notemplates":
    $docTemplate = "form.htm";
    $docScripts['body'] = "pages.notemplates.php";
    break;

  case "page_redirect":
    $docScripts['exec'] = "pages.exec.php";
    break;

  case "close":
    $docTemplate = "close.htm";
    break;

  case "updatecontrols":
    $docTemplate = "close.htm";
    $docScripts['exec'] = "pages.exec.php";
    if($_POST['pagedev_id'])
      $docStrings['urlend'] = "&selrow=" . $_POST['pagedev_id'];
    if(($action == "updatecontrols")or($action == "updatecontrolsdef"))
    {
      if($_GET['id']>0)
      {
        //"id" parameter is now pagedev_id for pages
        $perm_managecontents = CheckPermission(11, $site_id, $_GET['page_id']);
      }
      else
        $perm_managecontents = CheckPermission(9, $site_id);
    }
    $GLOBALS['perm_managecontents'] = $perm_managecontents;
    break;
  case "componentvisibility":
    $docTitle = "{%langstr:set_component_visibility%}";
    $docTemplate = "form.htm";
    $docScripts['body'] = "pages.cvisibility.php";
    $perm_managecontents = CheckPermission(11, $site_id, $_GET['id']);
    break;
  case "addcomponent":
    $docTitle = "{%langstr:add_component%}";
    $docTemplate = "form.htm";
    $docScripts['body'] = "pages.addcomponent.php";
    $perm_managecontents = CheckPermission(11, $site_id, $_GET['page_id']);
    break;
  case "addcomponentsave":
    $docTemplate = "close.htm";
    $docScripts['exec'] = "pages.exec.php";
    break;
  case "modpage":
  case "addpage":
  case "tbarupdatecontrols":
  case "updatecontrolsdef":
    $docTemplate = "close.htm";
    $docScripts['exec'] = "pages.exec.php";
    if($_POST['page_id'])
      $docStrings['urlend'] = "&selrow=" . $_POST['page_id'];
    if(($action == "updatecontrols")or($action == "updatecontrolsdef")or($action == "tbarupdatecontrols"))
    {
      if ($_GET['id']>0) $page_id = $_GET['id'];
      else if ($_GET['page_id']>0) $page_id = $_GET['page_id'];

      if (!$_GET['template_editor'])
      {
          if($page_id>0)
            $perm_managecontents = CheckPermission(11, $site_id, $_GET['page_id']);
          else
            $perm_managecontents = CheckPermission(9, $site_id);
      }
      else
      {
         //permissions uses IDs from pages table
         $perm_managecontents = CheckPermission(12, $site_id, $_GET['id']);
      }
    }
    break;

  case "frame_page_tree":
    $docTemplate = "lefttree.html";
    $docScripts['body'] = "pages.tree.php";
    break;
  case "inline_frame_page":
    $docTemplate = "editor.htm";
    $docStrings['oldhost'] = $GLOBALS['cfgWebRoot'];
    $docScripts['body'] = "pages.graphical.php";
    $_GET['page_id'] = intval($_GET['page_id']);
    if($_GET['page_id']>0)
    {
      $perm_managecontents = CheckPermission(11, $site_id, $_GET['page_id']);
    }else
      $perm_managecontents = CheckPermission(9, $site_id);
    break;
  case "frame_page_tree_components":
    $docTemplate = "lefttree_components.html";
    $docScripts['body'] = "pages.tree_components.php";
    break;
  case "components_visual_frame":
    $docTemplate = "frameset.htm";
    $domain = $_SERVER['SERVER_NAME']; //$GLOBALS['domain'];
    $url = parse_url($_SERVER['REQUEST_URI']);
    $url['query'] = str_replace("components_visual_frame", "components_graph", $url['query']);
    if ($_GET['nopageid'])
    {
        $p = sqlQueryValue("SELECT page_id FROM ".$_GET['site_id']."_pagesdev WHERE pagedev_id=".$_GET['id']);
        $url['query'] .= "&page_id=".$p;
        $_GET['page_id'] = $p;
    }
    $docStrings['rightframesrc'] = "http://" . $domain . "/edit.php?" . $url['query'] . "&session_id=" . session_id() . "&session_name=" . session_name();

    //$docStrings['controlframesrc'] = "http://" . $domain . "/edit.php?" . $url['query'] . "&session_id=" . session_id() . "&session_name=" . session_name();

    $docStrings['leftframesrc'] = "?module=pageframe&site_id=" . $GLOBALS['site_id'] . "&pagedev_id=" . $_GET['id'] . "&page_id=".$_GET['page_id']."&action=frame_page_tree";
    break;


/*  case "components_graph":
    $docTemplate = "editor.htm";
    $docStrings['oldhost'] = $GLOBALS['cfgWebRoot'];
    $docScripts['body'] = "pages.graphical.php";
    if($_GET['id']>0)
      $perm_managecontents = CheckPermission(11, $site_id, $_GET['id']);
    else
      $perm_managecontents = CheckPermission(9, $site_id);
    break; */

  case "components_graph":
    $docTemplate = "iframe.html";
    $docTitle = "{%langstr:edit_page%}";
    //$docScripts['body'] = "pages.graphical.php";
    $docStrings['editorframesrc'] = "?module=pages&site_id=" . $GLOBALS['site_id'] . "&pagedev_id=" . $_GET['id'] . "&page_id=".$_GET['page_id']."&action=inline_frame_page" . "&session_id=" . session_id() . "&session_name=" . session_name();
    $docStrings['oldhost'] = $GLOBALS['cfgWebRoot'];
    $docStrings['pagedev_id'] = $_GET['id'];
    $docStrings['page_id'] = $_GET['page_id'];
    $docStrings['site_id'] = $GLOBALS['site_id'];
    $docStrings['domain'] = $GLOBALS['domain'];

    break;

  case "redirectto_graph":
    $domain = $GLOBALS['domain'];
    $url = parse_url($_SERVER['REQUEST_URI']);
    $url['query'] = str_replace("redirectto_graph", "components_graph", $url['query']);
    Redirect("http://" . $domain . "/edit.php?" . $url['query'] . "&session_id=" . session_id() . "&session_name=" . session_name());
    break;

  case "delpage":
    $docScripts['exec'] = "pages.exec.php";
    break;

  case "delpage2":
    $docScripts['exec'] = "pages.exec.php";
    break;

  case "moveup":
  case "movedown":
  case "unsetpageenabled":
  case "setpageenabled":
  case "unsetpagevisible":
  case "setpagevisible":
  case "clearcashe":
    $docScripts['exec'] = "pages.exec.php";
    header("location: ?module=pages&site_id=" . $GLOBALS['site_id'] . '&selrow=' . $_GET['selrow']);
    break;

  case "simple":
    $docTemplate = "form.htm";
    $GLOBALS['resizeauto'] = 0;
    $docTitle = "Pages";
    $docScripts['body'] = "pages.simple.php";
    break;

  case "cssparser":
    $docTemplate = "form.htm";
    $GLOBALS['resizeauto'] = 0;
    $docTitle = "CSS Parser";
    $docScripts['body'] = "dialogs.cssparser.php";
    break;

  case "publish":
    $docScripts['exec'] = "pages.exec.php";
    Header("Location: ?module=pages&site_id=" . $GLOBALS['site_id'] . '&selrow=' . $_GET['selrow']);
    break;

  case "history_form":
    $docTemplate = "form.htm";
    $GLOBALS['resizeauto'] = 1;
    $docScripts['body'] = "pages.history.php";
    break;

  case "overallhistory_form":
    $docTemplate = "form.htm";
    $GLOBALS['resizeauto'] = 1;
    $docScripts['body'] = "pages.overallhistory.php";
    break;

  case "restore_history":
    $docTemplate = "close.htm";
    $docScripts['exec'] = "pages.exec.php";
    break;

  case "trash":
    $docTemplate = "main.htm";
    $docScripts['body'] = "pages.trash.php";

    require($GLOBALS['cfgDirRoot']."library/"."class.toolbar2.php");
    $Toolbar2 = new Toolbar2();

    $Toolbar2->AddSpacer();

    $Toolbar2->AddButtonImage('restore', 'view-refresh.png', "{%langstr:toolbar_restore_pages%}", "javascript:restoreClick();void(0);", "", 31, "{%langstr:toolbar_restore_pages%}", "");

    $Toolbar2->AddButtonImage('del', 'delete', "{%langstr:toolbar_delete%}", "javascript:deleteClick();void(0);", "", 31, "{%langstr:toolbar_hint_page_delete%}", "{%langstr:toolbar_ask_pages_delete%}");

    $docStrings['toolbar'] = $Toolbar2->output();

    break;

  case "trash_restore":
  case "trash_delete":
    $docScripts['exec'] = "pages.exec.php";
    break;

  case "search":
  default:

    if($perm_managepages)
    {
      $havestrings = $data = sqlQueryValue("SELECT Count(1) FROM " . $site_id . "_strings WHERE page=-1");
      /*require($GLOBALS['cfgDirRoot']."library/"."class.toolbar.php");
      $Toolbar = new Toolbar();
      $Toolbar->AddButton("add", "New Page", "add_page1.gif", "javascript:openDialog('?module=pages&site_id=" . $GLOBALS['site_id'] . "&action=create&sibiling='+SelectedRowID, 400, 550, 1, 0);", "Add new page to the selected branch");
      $Toolbar->AddButton("add2", "New Subpage", "add_page2.gif", "javascript:if(SelectedRowID)openDialog('?module=pages&site_id=" . $GLOBALS['site_id'] . "&action=create&parent='+SelectedRowID+'&changeparent=1', 400, 550, 1, 0);", "Add new page as subbranch to the selected branch");
      $Toolbar->AddButton("properties", "Properties", "edit_properties.gif", "javascript:if(SelectedRowID)openDialog('?module=pages&site_id=" . $GLOBALS['site_id'] . "&action=properties_form&id='+SelectedRowID, 400, 550, 1, 0);", "Change page properties");
      $Toolbar->AddButton("perms", "Permissions", "edit_permissions.gif", "javascript:if(SelectedRowID)openDialog('?module=permissions&action=permission_frame&mod=page_form&close=1&site_id=$site_id&data=$site_id&data2='+SelectedRowID+'&single_page=1', 800, 600, 0, 1);", "Edit selected page access permissions");

      $Toolbar->AddButton("sedit", "Standard Edit", "edit_page1.gif", "javascript:if(SelectedRowID)openDialog('?module=pages&site_id=" . $GLOBALS['site_id'] . "&action=components_form&id='+SelectedRowID, 800, 600, 1, 1);", "Edit page in standard editor");


      $Toolbar->AddLink("visedit", "Visual Edit", "edit_page2.gif", "javascript:if(SelectedRowID)openDialog('?module=pages&site_id=" . $GLOBALS['site_id'] . "&action=components_visual_frame&id='+SelectedRowID+'&nopageid=1', screen.availWidth, screen.availHeight-25, 1 , 1);", "Edit page in visual page editor");
      if($havestrings)
        $Toolbar->AddButton("strings", "Strings", "edit_properties.gif", "javascript:if(SelectedRowID)openDialog('?module=strings&site_id=" . $GLOBALS['site_id'] . "&action=page_strings&id='+SelectedRowID, 400, 520, 1, 0);", "Edit strings for selected page");

      $Toolbar->AddLink("up", "Up", "move_up.gif", "javascript:if(SelectedRowID)window.location='?module=pages&site_id=" . $GLOBALS['site_id'] . "&action=moveup&selrow='+SelectedRowID+'&id='+SelectedRowID;", "");
      $Toolbar->AddLink("down", "Down", "move_down.gif", "javascript:if(SelectedRowID)window.location='?module=pages&site_id=" . $GLOBALS['site_id'] . "&action=movedown&selrow='+SelectedRowID+'&id='+SelectedRowID;", "");
      $Toolbar->AddLink("del", "Delete", "delete.gif", "javascript:if(SelectedRowID)window.location='?module=pages&site_id=" . $GLOBALS['site_id'] . "&action=delpage&id='+SelectedRowID;", "Delete selected page", "Are you sure you want to delete selected page?");

      $Toolbar->AddButton("copy", "Copy pages", "copy_files.gif", "javascript:if(SelectedRowID)openDialog('?module=pages&site_id=" . $GLOBALS['site_id'] . "&action=copytree&id='+SelectedRowID, 400, 300, 1, 1);", "Copy page tree");

      if (CheckPermission(33, $GLOBALS['site_id']) && $GLOBALS['devhistory'])
          $Toolbar->AddButton("history", "History", "edit_properties.gif", "javascript:if(SelectedRowID)openDialog('?module=pages&site_id=" . $GLOBALS['site_id'] . "&action=history_form&id='+SelectedRowID, 400, 550, 1, 0);", "Dev pages");
      if ($GLOBALS['StorePageHistory'])
          $Toolbar->AddButton("history2", "Overall History", "edit_properties.gif", "javascript:if(SelectedRowID)openDialog('?module=pages&site_id=" . $GLOBALS['site_id'] . "&action=overallhistory_form&id='+SelectedRowID, 400, 550, 1, 0);", "View page history");
      if ($GLOBALS['clearcache']) $Toolbar->AddButton("clearcashe", "<span style='color:#FFFF00;'>Clear Cache<span>", "edit_properties.gif", "javascript:window.location='?module=pages&site_id=" . $GLOBALS['site_id'] . "&action=clearcashe&selrow='+SelectedRowID;", "Clear cache");
      if ($GLOBALS['searchincms']) $Toolbar->AddButton("searchincms", "Search", "view_log.gif", "javascript:openDialog('?module=pages&site_id=" . $GLOBALS['site_id'] . "&action=search_form', 400, 250, 1, 0);", "Search in CMS");
      $docStrings['toolbar'] = $Toolbar->output(); */


      require($GLOBALS['cfgDirRoot']."library/"."class.toolbar2.php");
      $Toolbar2 = new Toolbar2();

      $Toolbar2->AddSpacer();

      $Toolbar2->AddButtonImageText('add', 'sitemap_new_page', '{%langstr:toolbar_new_page%}', "", "javascript:openDialog('?module=pages&site_id=" . $GLOBALS['site_id'] . "&action=create&sibiling='+SelectedRowID, 400, 550, 1, 0);", 85, "{%langstr:toolbar_hint_new_page%}");
      $Toolbar2->AddButtonImageText('add2', 'sitemap_new_subpage', '{%langstr:toolbar_new_subpage%}', "", "javascript:if(SelectedRowID&&SelectedRowID!=-1)openDialog('?module=pages&site_id=" . $GLOBALS['site_id'] . "&action=create&parent='+SelectedRowID+'&changeparent=1', 400, 550, 1, 0);", 100, "{%langstr:toolbar_hint_new_subpage%}");
      $Toolbar2->AddButtonImage('properties', 'sitemap_properties', "{%langstr:toolbar_properties%}", "", "javascript:if(SelectedRowID&&SelectedRowID!=-1)openDialog('?module=pages&site_id=" . $GLOBALS['site_id'] . "&action=properties_form&id='+SelectedRowID, 400, 550, 1, 0);", 31, "{%langstr:toolbar_hint_page_properties%}");

      $Toolbar2->AddSeperator();

      $Toolbar2->AddButtonImage('visedit', 'sitemap_visual_edit', "{%langstr:toolbar_visual_edit%}", "", "javascript:if(SelectedRowID&&SelectedRowID!=-1)openDialog('?module=pages&site_id=" . $GLOBALS['site_id'] . "&action=components_visual_frame&id='+SelectedRowID+'&nopageid=1', screen.availWidth, screen.availHeight-25, 1 , 1);", 31, "{%langstr:toolbar_hint_visual_edit%}");
      if(!$GLOBALS['cfgMini'] || $_SESSION['userdata']['username'] == 'Admin')
        $Toolbar2->AddButtonImage('sedit', 'sitemap_standart_edit', "{%langstr:toolbar_standart_edit%}", "", "javascript:if(SelectedRowID&&SelectedRowID!=-1)openDialog('?module=pages&site_id=" . $GLOBALS['site_id'] . "&action=components_form&id='+SelectedRowID, 800, 600, 1, 1);", 31, "{%langstr:toolbar_hint_standart_edit%}");
      if($havestrings && (!$GLOBALS['cfgMini'] || $_SESSION['userdata']['username'] == 'Admin'))
        $Toolbar2->AddButtonImage('strings', 'sitemap_strings', "{%langstr:toolbar_edit_strings%}", "", "javascript:if(SelectedRowID&&SelectedRowID!=-1)openDialog('?module=strings&site_id=" . $GLOBALS['site_id'] . "&action=page_strings&id='+SelectedRowID, 400, 520, 1, 0);", 31, "{%langstr:toolbar_hint_edit_strings%}");

      $Toolbar2->AddSeperator();

      if(!$GLOBALS['cfgMini'] || $_SESSION['userdata']['username'] == 'Admin')
      {
        $Toolbar2->AddButtonImage('perms', 'sitemap_permissions', "{%langstr:toolbar_permissions%}", "", "javascript:if(SelectedRowID&&SelectedRowID!=-1)openDialog('?module=permissions&action=permission_frame&mod=page_form&close=1&site_id=$site_id&data=$site_id&data2='+SelectedRowID+'&single_page=1', 800, 600, 0, 1);", 31, "{%langstr:toolbar_hint_page_permissions%}");
        $Toolbar2->AddButtonImage('copy', 'sitemap_copy', "{%langstr:toolbar_copy_pages%}", "", "javascript:if(SelectedRowID&&SelectedRowID!=-1)openDialog('?module=pages&site_id=" . $GLOBALS['site_id'] . "&action=copytree&id='+SelectedRowID, 400, 300, 1, 1); else if(!SelectedRowIDsEmpty)openDialog('?module=pages&site_id=" . $GLOBALS['site_id'] . "&action=copytree2&id='+SelectedRowIDs, 400, 300, 1, 1);", 31, "{%langstr:toolbar_hint_copy_pages%}");
        $Toolbar2->AddButtonImage('history', 'sitemap_history', "{%langstr:toolbar_history%}", "", "javascript:if(SelectedRowID&&SelectedRowID!=-1)openDialog('?module=pages&site_id=" . $GLOBALS['site_id'] . "&action=history_form&id='+SelectedRowID, 400, 550, 1, 0);else alert('No page selected');", 31, "{%langstr:toolbar_hint_page_history%}");

        $Toolbar2->AddSeperator();
      }

      $Toolbar2->AddButtonImage('up', 'up', "{%langstr:toolbar_up%}", "javascript:if(SelectedRowID&&SelectedRowID!=-1)window.location='?module=pages&site_id=" . $GLOBALS['site_id'] . "&action=moveup&selrow='+SelectedRowID+'&id='+SelectedRowID;", "", 31, "{%langstr:toolbar_hint_page_up%}");
      $Toolbar2->AddButtonImage('down', 'down', "{%langstr:toolbar_down%}", "javascript:if(SelectedRowID&&SelectedRowID!=-1)window.location='?module=pages&site_id=" . $GLOBALS['site_id'] . "&action=movedown&selrow='+SelectedRowID+'&id='+SelectedRowID;", "", 31, "{%langstr:toolbar_hint_page_down%}");
      $Toolbar2->AddButtonImage('del', 'delete', "{%langstr:toolbar_delete%}", "javascript:if(SelectedRowID&&SelectedRowID!=-1)window.location='?module=pages&site_id=" . $GLOBALS['site_id'] . "&action=delpage&id='+SelectedRowID; else if(!SelectedRowIDsEmpty) window.location='?module=pages&site_id=" . $GLOBALS['site_id'] . "&action=delpage2&id='+SelectedRowIDs;", "", 31, "{%langstr:toolbar_hint_page_delete%}", "{%langstr:toolbar_ask_pages_delete%}");

      $Toolbar2->AddSeperator();

          $trash_count = sqlQueryValue('SELECT COUNT(*) FROM '.$GLOBALS['site_id'].'_pagesdev WHERE in_trash=1 AND language='.(int)$GLOBALS['currentlanguagenum']);

      $Toolbar2->AddButtonImage('trash', 'trashcan_'.($trash_count ? 'full' : 'empty').'.png', "{%langstr:toolbar_pages_trash%}", "?module=pages&site_id=" . $GLOBALS['site_id'] . "&action=trash", "", 31, "{%langstr:toolbar_pages_trash%}", "");

      $Toolbar2->AddButtonImage("help", 'help', "{%langstr:toolbar_help%}", $GLOBALS['cfgWebRoot']."help/help.htm?category=lapas", "", 31, "","","_blank");


      $docStrings['toolbar'] = $Toolbar2->output();
    }

    $docTemplate = "main.htm";
    $docTitle = "Pages";
    if($action == 'search')
    {
      $docScripts['body'] = "pages.search.php";
    }else
    {
      $docScripts['body'] = "pages.default.php";
    }
    break;

}

