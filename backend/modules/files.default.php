<?

require($GLOBALS['cfgDirRoot']."library/"."class.rightmenu.php");
$rightmenu = new rightmenu;

$rightmenu->addItem('{%langstr:docs_make_directory%}',"openDialog('?module=files&site_id=".$GLOBALS['site_id']."&path=".$GLOBALS['path']."&action=newdir', 400, 120);",false,false,'files_new_folder');
$rightmenu->addItem("{%langstr:docs_upload%}","openDialog('?module=files&site_id=".$GLOBALS['site_id']."&path=".$GLOBALS['path']."&action=upload', 500, 300);",false,false,"files_upload");
$rightmenu->addSpacer();
$rightmenu->addItem("{%langstr:docs_rename%}","if (window.fff) openDialog('?module=files&site_id=".$GLOBALS['site_id']."&path=".$GLOBALS['path']."&action=rename&name='+window.fff, 500, 245); else openDialog('?module=files&site_id=".$GLOBALS['site_id']."&path=".$GLOBALS['path']."&action=rename&name='+escape(SelectedRowID), 500, 145);",false,false,"files_rename");
$rightmenu->addItem("{%langstr:toolbar_copy%}","javascript:if (window.fff) openDialog('?module=dialogs&site_id=".$GLOBALS['site_id']."&path=".$GLOBALS['path']."&action=dirx&file='+window.fff, '', 'font-family:Verdana; font-size:12; dialogWidth:530px; dialogHeight:370px; help:0; status:0'); else if (SelectedRowID) showModalDialog('?module=dialogs&site_id=".$GLOBALS['site_id']."&path=".$GLOBALS['path']."&action=dirx&file='+escape(SelectedRowID), '', 'font-family:Verdana; font-size:12; dialogWidth:530px; dialogHeight:370px; help:0; status:0'); if (window.fff || SelectedRowID) document.location='?module=files&path=".$GLOBALS['path']."&site_id=".$GLOBALS['site_id']."'",false,false,"files_copy");
$rightmenu->addItem("{%langstr:toolbar_move%}","javascript:if (window.fff) openDialog('?module=dialogs&action=dirx&site_id=".$GLOBALS['site_id']."&path=".$GLOBALS['path']."&move=1&file='+window.fff, '', 'font-family:Verdana; font-size:12; dialogWidth:530px; dialogHeight:370px; help:0; status:0'); else if (SelectedRowID) showModalDialog('?module=dialogs&site_id=".$GLOBALS['site_id']."&path=".$GLOBALS['path']."&action=dirx&move=1&file='+escape(SelectedRowID), '', 'font-family:Verdana; font-size:12; dialogWidth:530px; dialogHeight:370px; help:0; status:0'); if (window.fff || SelectedRowID) document.location='?module=files&path=".$GLOBALS['path']."&site_id=".$GLOBALS['site_id']."'",false,false,"files_move");
$rightmenu->addItem("{%langstr:toolbar_edit%}","if (SelectedRowID) openDialog('?module=files&site_id=".$GLOBALS['site_id']."&path=".$GLOBALS['path']."&action=edit&file='+escape(SelectedRowID), 800, 600);",false,false,"files_edit");
$rightmenu->addSpacer();
$rightmenu->addItem("{%langstr:col_del%}","if (window.fff || SelectedRowID) if (confirm('Are you sure to delete?')) if (window.fff) window.location='?module=files&action=delete&path=".$GLOBALS['path']."&file='+window.fff+'&site_id=".$GLOBALS['site_id']."'; else if (SelectedRowID) window.location='?module=files&action=delete&path=".$GLOBALS['path']."&file='+SelectedRowID+'&site_id=".$GLOBALS['site_id']."';",false,false,"delete");
if($GLOBALS['use_file_keywords_search'])
{
  $rightmenu->addSpacer();
  $rightmenu->addItem("{%langstr:toolbar_keywords%}","if (SelectedRowID) openDialog('?module=files&site_id=".$GLOBALS['site_id']."&path=".$GLOBALS['path']."&action=keywords&file='+escape(SelectedRowID), 500, 600);",false,false,"files_edit_keywords");
}
$rightmenu->addSpacer();
$rightmenu->addItem('{%langstr:toolbar_help%}',$GLOBALS['cfgWebRoot'].'help/help.htm?category=faili',true,true,'help');


echo $rightmenu->output();

$perm_managefiles = $GLOBALS['perm_managefiles'];

$cols = Array(
//50%
  "name"        => Array(
    "width"     => "75%",
    "title"     => "{%langstr:col_name%}"
  ),

  "edit"    => Array(
    "width"     => "5%",
    "title"     => "{%langstr:col_edit%}",
    "align"     => "center"
  ),

  "size"    => Array(
    "width"     => "5%",
    "title"     => "{%langstr:col_size%}"
  ),
//35%
  "date"     => Array(
    "width"     => "15%",
    "title"     => "{%langstr:col_modified%}"
  ),
/*
  "del"    => Array(
    "width"     => "5%",
    "title"     => "Del",
    "align"     => "center",
    "format"    => '<a href="?module=files&action=delete&path='.$GLOBALS['path'].'&file={%title%}&site_id='.$GLOBALS['site_id'].'" OnClick="return confirm(\'Are you sure you wish to delete this item?\')"><img src="gui/images/ico_del.gif" width="16" height="16" border="0" alt="Delete"></a>'
  )

*/
);

if($GLOBALS['index_htm_files'])
{
  $cols["indexsearch"] =
      Array(
        "width"     => "5%",
        "title"     => "{%langstr:col_index%}"
      );
}

//Disable file deleting
if(!$perm_managefiles)
  unset($cols['del']);

$GLOBALS['view']='thumbs';
$domain = sqlQueryValue("SELECT domain FROM sites WHERE site_id=".$_GET['site_id']);

function read_files($webroot, $dir, $path) {
  global $cfgCmsDirRoot, $cfgWebRoot;

  function outputFilename($filepath, $webpath, $webdir, $filename) {
    global $domain;
    $ext = strtolower(substr($filename,-4,4));
    if ($GLOBALS['view']=='thumbs')/*(eregi("^images", $webdir) || eregi("^/images", $webdir))*/ {
      if ($ext==".gif" || $ext==".jpg" || $ext==".png") {
        $image = image_preview_out($filepath, $webpath, 200, 120);
      } else {
        $image = '<img src="'.$GLOBALS['cfgWebRoot'].'gui/images/ico_file.gif" width="16" height="16" align="absmiddle" border="0">';
      }
      $html = '<center><a href="http://'.$domain."/".$webdir.'" target="_blank">'.$image.'<br><br>'.$filename.'</a></center>';
    } else {
      $html = '<img src="'.$GLOBALS['cfgWebRoot'].'gui/images/ico_file.gif" width="16" height="16" align="absmiddle"> <a href="javascript:;" OnClick="retVal(\'Form\', \'Input\', \'/'.$webdir.'\');">'.$filename.'</a>';
    }
    return $html;
  }

  chdir($dir);

  $handle = opendir($dir);
  while ($file = readdir($handle)) {
    if (is_dir($file) && $file != ".")
      $dirlist[] = $file;
    elseif (is_file($file))
      $filelist[] = $file;
  }
  closedir($handle);

  $i = 0;         
  asort($dirlist);
  foreach($dirlist as $file) {
    if (!($file == '..' && $path == '')) {
      if ($file == '..') $url = fileUp($path); elseif ($path == '') $url = $file; else $url = $path.'/'.$file;
      $thisfile['name'] = '<a href="?module=files&path='.$url.'&site_id='.$GLOBALS['site_id'].'"><img src="gui/images/ico_folder.gif" width="16" height="16" align="absmiddle" border="0"> '.$file.'</a>';
      $thisfile['title'] = $file;
      $thisfile['size'] = fileSizeStr($file);
      $thisfile['date'] = date("d-m-Y H:i:s", filectime($file));
      //if ($file != '..') $thisfile['del'] = 'x';
      if($file != '..')
        $thisfile['indexsearch'] = "<a href=\"javascript:void(0);\" onclick=\"javascript:openDialog('?module=admin&action=reindex&close=0&site_id=".$_GET['site_id']."&page_id=0&file=1&indexdir=1&reldir='+escape('".$url."')+'&fulldir='+escape('".$dir.$file."'), 400, 100);\">{%langstr:col_index%}</a>";
      $data[$i++] = $thisfile;
    }
  }
  asort($filelist);
  foreach($filelist as $file) {
    if(in_array($file, array('.htaccess', '.gitignore'))) continue; # dont show us
    if ($path != "") $path = fileAddBackslash($path);
    $url = $path.$file;
    //$thisfile['chk'] = '<input type="checkbox" name>';
    $thisfile['name'] = '<input style="position:absolute;" type="checkbox" id="filecheck" name="'.$file.'" onClick="fileparam(\''.$file.'\')">'.outputFilename(fileAddBackslash($dir).$file, fileAddBackslash($webroot).$url, $url, $file);
    //'<img src="gui/images/ico_file.gif" width="16" height="16" align="absmiddle"> <a href="'.$url.'" target="_blank">'.$file.'</a>';
    $thisfile['title'] = $file;
    $thisfile['size'] = fileSizeStr($file);
    $thisfile['date'] = date("d-m-Y H:i:s", filectime($file));
    $isindexed = false;
    $thisfile['indexsearch'] = '';
    if((($GLOBALS['index_htm_files'])and(preg_match("/\.htm$|\.html$/i", $file)))or(($GLOBALS['index_doc_files'])and(preg_match("/\.doc$/i", $file))))
    {
      $isindexed = sqlQueryValue("SELECT id FROM ".$GLOBALS['site_id']."_filesearch_object WHERE filename='$url'");
      $thisfile['indexsearch'] = "<a href=\"javascript:void(0);\" onclick=\"javascript:openDialog('?module=admin&action=reindex&close=0&site_id=".$_GET['site_id']."&page_id=0&file='+escape('$url'), 400, 100);\">".IIF($isindexed,"Re-index", "Index")."</a>";
    }
    $ext = strtolower(substr($file,-4,4));
    if ($ext==".gif" || $ext==".jpg" || $ext==".png" || $ext=="jpeg")
        $thisfile['edit'] = '<a href="javascript:void(0);" OnClick="openDialog(\'?module=wysiwyg&action=editimage&path='.$GLOBALS['path'].'&img='.$url.'&site_id='.$GLOBALS['site_id'].'\');"><img src="'.$GLOBALS['cfgWebRoot'].'gui/images/edit.gif" width="16" height="16" border="0" alt="Edit"></a>';
    $data[$i++] = $thisfile;
    unset($thisfile);
  }

  chdir($cfgCmsDirRoot);

  return $data;
}

$directory = sqlQueryValue("SELECT dirroot FROM sites WHERE site_id = '".$GLOBALS['site_id']."'");
$webroot = sqlQueryValue("SELECT domain FROM sites WHERE site_id = '".$GLOBALS['site_id']."'");
$path = str_replace("..", "", $GLOBALS['path']);
if ($path == "/") $path = "";
$directory = str_replace("\\", "/", $directory);
  //"

$directory = fileAddBackslash($directory).$path;

$data = read_files('http://'.$webroot.'/', $directory, $path);

if($path)
  $disppath = '/' . $path;
else
  $disppath = '';
$disppath = explode('/', $disppath);
$pathhref = '';
foreach($disppath as $key => $row)
{
  if($pathhref)
    $pathhref .= '/';
  $pathhref .= $row;
  if(!$row)
    $row = '(..)';
  $disppath[$key] = '<a href="?module=files&path='.$pathhref.'&site_id='.$GLOBALS['site_id'].'">' . $row . '</a>';
}
$disppath = implode('/', $disppath);

echo '
<script language="JavaScript">
  var selrow = ' . (int) ($data[0][0]) . ';
  window.fff = "";
  function fileparam(fname)
  {
    var s = fff.indexOf(fname);
    var sub;
    if (s>=0)
      {
        if (s==0) sub = window.fff.substr(fname.length+1);
        else sub = window.fff.substr(0,s-1) + window.fff.substr(s+fname.length);
        window.fff = sub
      }
    else
      {
        if (window.fff.length>0) window.fff = window.fff + ";" + fname;
        else window.fff = window.fff + fname;
      }

  }
</script>';

require($GLOBALS['cfgDirRoot']."library/"."class.datagrid.php");

$Table = new DataGrid();
$Table->cols = $cols;
$Table->data = $data;
$Table->footer = false;
$Table->padding = "2";
$Table->rowformat = " onClick=\"javascript:HighLightTR(this, '{%title%}',1);if(SelectedRowID){ EnableToolbarButtons(che); actions_enabled = true; }else { DisableToolbarButtons(che); }\" oncontextmenu=\"javascript:HighLightTR(this, '{%title%}',1); showMenu(this, event); return false;\" ";
echo("\n<br /><p><span class=\"gray\">{%langstr:docs_current_dir%}</span> /".$disppath."</p>\n");
echo $Table->output();
echo '<input type="checkbox" id="select_all" name="select_all" onClick="javascript:
  var filecheck = document.getElementsByTagName(\'input\');
  var lastname = \'\';
  if(filecheck)
  {
    if(filecheck.length)
    {
      for(f=0; f < filecheck.length; f++)
      {
        if(filecheck[f].id == \'filecheck\')
        {
          lastname = filecheck[f].name;
          if(filecheck[f].checked != this.checked)
          {
            filecheck[f].checked = this.checked;
            fileparam(filecheck[f].name);
          }
        }
      }
      EnableToolbarButtons(che);
      actions_enabled = true;
      HighLightTR(this, lastname);
    }
  }
"> <label for="select_all">{%langstr:select_all%}</label>';

echo '<script language="JavaScript" type="text/javascript">
/*<![CDATA[*/
che = new Array("ren","del","copy","move","edit","keywords","restore","unzip");
DisableToolbarButtons(che);
/*]]>*/
</script>';


?>
  <script type="text/javascript">

  var actions_enabled = false;

  function getSelectedRowIDs()
  {
    if (SelectedRowID != -1)
      return [SelectedRowID];


    var ids = [];
    for (var i=0; i<SelectedRowIDs.length; i++)
    {
      if (SelectedRowIDs[i] == undefined || SelectedRowIDs[i] == 0) continue;

      ids.push(SelectedRowIDs[i]);
    }

    return ids;
  }

  function restoreClick()
  {
    if (!actions_enabled) return false;

    var files = getSelectedRowIDs();
    if (files.length == 0) return false;

    for (var k=0; k<files.length; k++)
    {
      files[k] = encodeURIComponent(files[k]);
    }

    window.location = '?module=files&site_id=<?=$GLOBALS["site_id"] ?>&action=trash_restore&file='+files[0];
    return false;
  }

  </script>
