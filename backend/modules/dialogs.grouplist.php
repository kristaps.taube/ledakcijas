<?

  function InitCollection(&$coltype, &$colname, &$categoryID, &$collection)
  {
    //initialize collection class
    include_once($GLOBALS['cfgDirRoot'] . "collections/class." . $coltype . ".php");
    $colname = '';
    $collection = new $coltype($colname,$categoryID);
  }

  $coltype = "lbbusergroupscollection";
  $site_id = $_GET['site_id'];  

  $categoryid = $_GET['categoryid'];
  //echo $coltype." ".$categoryid;
  InitCollection($coltype, $colname, $categoryid, $collection);

  $sel = $_GET['sel'];
  if($sel == 'undefined') $sel = '';
  if($sel){
    $data = sqlQueryData("SELECT item_id, ind, col10, ELT((FIND_IN_SET(ind,'$sel')>0)+1,'','checked') as checked FROM `".$site_id."_coltable_".strtolower($collection->name)."`");
  }else{
    $data = sqlQueryData("SELECT item_id, ind, col10, '' as checked FROM `".$site_id."_coltable_".strtolower($collection->name)."`");
  }

  $cols = Array(

  "group_id"    => Array(
    "width"     => "5%",
    "title"     => "#",
  ),

  "checkbox"    => Array(
    "width"     => "5%",
    "title"     => "",
    "format"    => "<input type=checkbox name=chbox value={%ind%} {%checked%} OnClick=\"checkBoxes();\">"
  ),

  "name"   => Array(
    "width"     => "80%",
    "title"     => "Value",
    "format"    => "{%col10%}"
  )

  );
/*
  $sel = $_GET['sel'];
  if($sel == 'undefined') $sel = '';
  if($sel){
    $data = sqlQueryData("SELECT group_id, name_en, ELT((FIND_IN_SET(group_id,'$sel')>0)+1,'','checked') as checked FROM ".$site_id."_lbbgroups");
  }else{
    $data = sqlQueryData("SELECT group_id, name_en, '' as checked FROM ".$site_id."_lbbgroups");
  }
  
  $cols = Array(

  "group_id"    => Array(
    "width"     => "5%",
    "title"     => "#",
  ),

  "checkbox"    => Array(
    "width"     => "5%",
    "title"     => "",
    "format"    => "<input type=checkbox name=chbox value={%group_id%} {%checked%} OnClick=\"checkBoxes();\">"

  ),

  "name_en"   => Array(
    "width"     => "90%",
    "title"     => "Groups"
  )

);
*/

require($GLOBALS['cfgDirRoot']."library/"."class.datagrid.php");

$Table = new DataGrid();
$Table->cols = $cols;
$Table->data = $data;
$Table->header = false;
$Table->footer = false;
echo $Table->output();

  
?>
