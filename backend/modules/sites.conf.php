<?

$action = $_GET["action"];
if($GLOBALS['currentUserSiteID'])
{
  Header("Location: index.php?site_id=" . $GLOBALS['currentUserSiteID'] . "&module=" . defaultSiteModule);
  Die;
}
$perm_managesites = CheckPermission(1);

switch ($action) {

  // site properties dialog box
  case "properties":
    $docTemplate = "dlg_form.htm";
    $docTitle = "{%langstr:site_properties%}";
    $docStrings['frame'] = "?module=sites&action=properties_form&id=" . $_GET['id'];
    break;

  case "create":
    $docTemplate = "form.htm";
    $GLOBALS['resizeauto'] = 1;
    $docTitle = "{%langstr:create_site%}";
    $docScripts['body'] = "sites.properties.php";
    break;

  // site properties form
  case "properties_form":
    $docTemplate = "form.htm";
    $GLOBALS['resizeauto'] = 1;
    $docScripts['body'] = "sites.properties.php";
    break;

  case "close":
    $docTemplate = "close.htm";
    break;
    
  case "ask_delete":
    $docTemplate = "nothing.htm";
    $docScripts['body'] = "sites.askdelete.php";
    $docTitle = "Delete Site";
    break;

  case "modsite":
  case "addsite":
    $docTemplate = "close.htm";
    $docScripts['exec'] = "sites.exec.php";
    break;

  case "delsite":
    $docScripts['exec'] = "sites.exec.php";
    Header("Location: ?module=sites");
    break;

  default:

    if($perm_managesites)
    {
      /*require($GLOBALS['cfgDirRoot']."library/"."class.toolbar.php");
      $Toolbar = new Toolbar();
      $Toolbar->AddButton("add", "New Site", "new_site.gif", "javascript:openDialog('?module=sites&action=create', 400, 500);", "Create new web site");
      $Toolbar->AddButton("props", "Properties", "edit_properties.gif", "javascript:if(SelectedRowID)openDialog('?module=sites&action=properties_form&id='+SelectedRowID, 400, 500);", "Edit selected web site properties");
      $Toolbar->AddButton("perms", "Site Permissions", "edit_permissions.gif", "javascript:if(SelectedRowID)openDialog('?module=permissions&action=permission_frame&mod=site_form&hideitems=1&close=1&site_id='+SelectedRowID+'&data='+SelectedRowID, 800, 600, 0, 1);", "Edit selected web site access permissions");
      $Toolbar->AddLink("edit", "Edit Contents", "edit_contents.gif", "javascript:if(SelectedRowID)window.location='?module=pages&site_id='+SelectedRowID;", "Access selected web site content management system");
      $Toolbar->AddLink("del", "Delete", "delete_site.gif", "javascript:if(SelectedRowID)openDialog('?module=sites&action=ask_delete&id='+SelectedRowID, 400, 395);", "Delete selected web site", "Warning!\\n\\nAll pages, templates and other content of the selected web site will be permanently deleted with no possibility to undo the action.\\n\\nAre you really sure you want to delete selected web site?");
      $docStrings['toolbar'] = $Toolbar->output();*/

      require($GLOBALS['cfgDirRoot']."library/"."class.toolbar2.php");
      $Toolbar = new Toolbar2();

      $Toolbar->AddSpacer();

      $Toolbar->AddButtonImage("add", 'site_new', "{%langstr:toolbar_new_site%}", "", "javascript:openDialog('?module=sites&action=create', 400, 500, 1, 0);", 31, "{%langstr:hint_new_site%}");
      $Toolbar->AddButtonImage("props", 'site_properties', "{%langstr:toolbar_properties%}", "", "javascript:if(SelectedRowID)openDialog('?module=sites&action=properties_form&id='+SelectedRowID, 400, 500, 1, 0);", 31, "{%langstr:hint_edit_site_properties%}");
      $Toolbar->AddSeperator();

      $Toolbar->AddButtonImage("edit", 'site_edit', "{%langstr:toolbar_edit_contents%}", "javascript:if(SelectedRowID)window.location='?module=pages&site_id='+SelectedRowID;", "", 31, "{%langstr:hint_edit_contents%}");
      $Toolbar->AddSeperator();
      $Toolbar->AddButtonImage("perms", 'site_permissions', "{%langstr:toolbar_site_permissions%}", "", "javascript:if(SelectedRowID)openDialog('?module=permissions&action=permission_frame&mod=site_form&hideitems=1&close=1&site_id='+SelectedRowID+'&data='+SelectedRowID, 800, 600, 0, 1);", 31, "{%langstr:edit_selected_web_site_access%}");
      $Toolbar->AddSeperator();
      $Toolbar->AddButtonImage("del", 'delete', "{%langstr:col_del%}", "javascript:if(SelectedRowID)openDialog('?module=sites&action=ask_delete&id='+SelectedRowID, 400, 395);", '', 31, "{%langstr:hint_delete_web_site%}", "{%langstr:warning%}\\n\\n{%langstr:ask_delete_web_site%}\\n\\n{%langstr:ask_really_delete_site%}");

      $Toolbar->AddSeperator();

      $docStrings['toolbar'] =  $Toolbar->output();

    }

    $docTemplate = "main.htm";
    $docTitle = "Sites";
    $docScripts['body'] = "sites.default.php";
    $docStrings['test'] = "idusgiusdibusdvbuivs";
    break;

}

?>
