<?


function InitCollection(&$coltype, &$colname, &$categoryID, &$collection)
{
    //initialize collection class
    include_once($GLOBALS['cfgDirRoot'] . "collections/class." . $coltype . ".php");
    $colname = '';
    $collection = new $coltype($colname,$categoryID);
}

  
  $coltype = $_GET['coltype'];
  $site_id = $_GET['site_id'];  

  if ($_GET['categoryid']) 
  {
      $categoryid = $_GET['categoryid'];
      //echo $coltype." ".$categoryid;
      InitCollection($coltype, $colname, $categoryid, $collection);

      //-----------------------------------------------------------
      //collection actions
      if ($_GET['save'])
      {
        $a = Array();
        foreach($collection->properties AS $key=>$val)
        {
          if (in_array(substr($key,2),$date_cols)) $a[] = strtotime($_POST[$key]);
          else $a[] = $_POST[$key];
        }

        $i = $collection->AddNewItem();
        $collection->ChangeItem($i,$a);
      }
      if ($_GET['add'])
      {
        $form_data = $collection->properties;
        require($GLOBALS['cfgDirRoot']."library/"."class.formgen.php");
        $FormGen = new FormGen();
        $FormGen->action = "?module=dialogs&action=spsproductframe&site_id=" . $_GET['site_id'] . "&coltype=".$coltype."&categoryid=".$categoryid."&save=1";
        $FormGen->title = "Item Properties";
        $FormGen->cancel =  "?module=dialogs&action=spsproductframe&site_id=" . $_GET['site_id'] . "&coltype=".$coltype."&categoryid=".$categoryid;
        $FormGen->properties = $form_data;
        $FormGen->buttons = true;
        echo $FormGen->output();
        die;
      }
      if ($_GET['del'])
      {
        $collection->DelItem($_GET['ind']);
      }
      //-----------------------------------------------------------
      
      $data = $collection->getDBData();
  }
 

  $sel = $_GET['sel'];
  if($sel == 'undefined') $sel = '';
  if($sel){
    $data = sqlQueryData("SELECT item_id, ind, col0, ELT((FIND_IN_SET(col0,'$sel')>0)+1,'','checked') as checked FROM `".$site_id."_coltable_".$collection->name."`");
  }else{
    $data = sqlQueryData("SELECT item_id, ind, col0, '' as checked FROM `".$site_id."_coltable_".$collection->name."`");
  }

  $cols = Array(

  "item_id"    => Array(
    "width"     => "5%",
    "title"     => "#",
  ),

  "checkbox"    => Array(
    "width"     => "5%",
    "title"     => "",
    "format"    => "<input type=checkbox name=chbox value={%col0%} {%checked%} OnClick=\"checkBoxes();\">"
  ),

  "name"   => Array(
    "width"     => "80%",
    "title"     => "Value",
    "format"    => $showformat
  ),

  "del"     => Array(
    "width"     => "5%",
    "title"     => "",
    "format"    => "<a href='?module=dialogs&action=spsproductframe&site_id=" . $_GET['site_id'] . "&coltype=".$coltype."&categoryid=".$categoryid."&del=1&ind={%ind%}' style='color:red;text-decoration:none;font-weight:bold;'>X</a>"
  )

);


require($GLOBALS['cfgDirRoot']."library/"."class.datagrid.php");

$Table = new DataGrid();
$Table->cols = $cols;
$Table->data = $data;
$Table->header = false;
$Table->footer = false;
echo $Table->output();

echo '<input type="button" value="Add" OnClick="document.location=\'?module=dialogs&action=spsproductframe&site_id=' . $_GET['site_id'] . '&coltype='.$coltype.'&categoryid='.$categoryid.'&add=1\'">';
  
?>
