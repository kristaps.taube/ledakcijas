<?
$collection= $GLOBALS['collection'];

$category_id = $collection->collection_id;

$cols = $collection->columns;


$site_id = $_GET['site_id'];
$page_id = $_GET['page_id'];

$GLOBALS['PageEncoding'] = PageEncoding($page_id, $site_id);
$GLOBALS['maincharset']  = PageEncoding($page_id, $site_id, false);

$form_data = $collection->GetDBProperties();

//eleminate undesired fields, convert tricky fields and add empty selections to fixedlist fields
$new_data = Array();
foreach($form_data as $key => $field)
{
  $field['value'] = '';
  $field['validate'] = '';
  if($field['type'] == 'collection' || $field['type'] == 'button' || $field['type'] == 'file' || $field['type'] == 'hidden' || $field['type'] == 'check' || $field['type'] == 'action_list' ||  $field['type'] == 'template' || $field['type'] == 'code')
  {
    unset($field);
  }else if($field['type'] == 'wysiwyg' || $field['type'] == 'html')
  {
     $field['type'] = 'string';
  }else if($field['type'] == 'list')
  {
    $field['lookup'] = array_merge(Array(':'), $field['lookup']);
  }

  //convert it to col
  if($field['column_name'])
  {
    $newkey = $key;
  }else
  {
    $newkey = str_replace('it', 'col', $key);
  }
  if($field)
    $new_data[$newkey] = $field;
}
$form_data = $new_data;


$action = $_GET['action'];
$category_id = $_GET['category_id'];
$create = ($action == 'newcolitem');
$id = $_GET['id'];


require($GLOBALS['cfgDirRoot']."library/"."class.formgen.php");
$FormGen = new FormGen();
if(!$create)
{
  $FormGen->action = "?module=".$GLOBALS['module']."&site_id=".$site_id."&page_id=$page_id&action=filtercollectionsave&category_id=$category_id&coltype=".$_GET['coltype'].'&justedit='.$_GET['justedit']."&id=".$id;
}else{
  $FormGen->action = "?module=".$GLOBALS['module']."&site_id=".$site_id."&page_id=$page_id&action=filtercollectionsave&category_id=$category_id&coltype=".$_GET['coltype'].'&justedit='.$_GET['justedit'];
}
$FormGen->title = "Item Properties";
$FormGen->cancel =  'close';
$FormGen->properties = $form_data;
$FormGen->buttons = true;
echo $FormGen->output();


?>