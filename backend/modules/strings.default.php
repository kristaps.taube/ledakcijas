<?

$site_id = $GLOBALS['site_id'];
$perm_managetemplates = $GLOBALS['perm_managetemplates'];

// Let's try table generator

$cols = Array(

  "stringname"  => Array(
    "width"     => "50%",
    "title"     => "{%langstr:string_name%}",
    "format"    => "<a href=\"#\" OnClick=\"javascript:openDialog('?module=strings&site_id=" . $site_id . "&action=properties_form&id={%string_id%}', 400, 220);\">{%stringname%}</a>"
  ),

  "stringvalue" => Array(
    "width"     => "50%",
    "title"     => "{%langstr:col_value%}"
  )

);

if(!$perm_managetemplates)
  unset($cols['stringname']['format']);


$data = sqlQueryData("SELECT string_id, stringname, stringvalue FROM " . $site_id . "_strings WHERE page=-1");

foreach($data as $key => $row)
{
  $data[$key]['stringvalue'] = htmlspecialchars($data[$key]['stringvalue']);
}

require($GLOBALS['cfgDirRoot']."library/"."class.datagrid.php");

$Table = new DataGrid();
$Table->cols = $cols;
$Table->data = $data;
$Table->footer = false;
$Table->rowformat = " onClick=\"javascript:HighLightTR(this, {%string_id%});if(SelectedRowID){ EnableToolbarButtons(che); }else { DisableToolbarButtons(che); }\"";
echo $Table->output();

echo '<script language="JavaScript" type="text/javascript">
/*<![CDATA[*/
che = new Array("props", "del");
DisableToolbarButtons(che);
/*]]>*/
</script>';


?>
