<div style="padding-left: 20px; padding-top: 0px">

<p>
<span style="font-size: 10pt; font-weight: bold; color: #597EC5">{%langstr:version%} 4.0</span>
</p>

<p>
{%langstr:copyright%}
</p>

<p>
<span style="color: #083085"><b>{%langstr:contacts%}</b></span><br>
<span style="color: #999999">{%langstr:feedback_and_support%}</span>: <a href="mailto:info@serveris.lv">info@serveris.lv</a>, <a href="mailto:constructor@datateks.lv">constructor@datateks.lv</a>
</p>

</div>
