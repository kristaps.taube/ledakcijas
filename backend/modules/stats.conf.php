<?

$GLOBALS['tabs'] = 'site';
$site_id = $_GET['site_id'];

/*if($GLOBALS['currentUserSiteID'])
{
  Header("Location: index.php?site_id=" . $GLOBALS['currentUserSiteID'] . "&module=" . defaultSiteModule);
  Die;
} */


$action = $_GET['action'];
$GLOBALS['perm_accessstats'] = (CheckPermission(34) || CheckPermission(18));

switch ($action) {

  case "user_stats":
    $docTemplate = "nothing.htm";
    $docScripts['body'] = "stats.userstats.php";
    $docTitle = "User statistics";
    break;

  case "graph":
    $docTemplate = "blank.html";
    $docScripts['body'] = "stats.graph.php";
    $docTitle = "Restore Site";
    break;

  case "conv":
    $docTemplate = "main.htm";
    $docTitle = "Statistics Convertation";
    $docScripts['body'] = "stats.conv.php";
    break;

  default :
    $docTemplate = "main.htm";
    $docTitle = "Site Statistics";
    $docScripts['body'] = "stats.viewstats.php";
    break;

}

?>
