<?

  $perm_managecontents = $GLOBALS['perm_managecontents'];
  if(!$perm_managecontents)
    AccessDenied(True);

  require_once ($GLOBALS['cfgDirRoot']."library/".'class.formgen.php');

  $id = $_GET["id"];
  if (!$id) $id = $_GET['wappagedev_id'];
  $GLOBALS['wappagedev_id'] = $id;
  $wappagedev_id = $id;
  $site_id = $GLOBALS['site_id'];

  if($wappagedev_id>0)
  {   //real page
    $template = sqlQueryValue("SELECT template FROM ". $site_id . "_wappagesdev WHERE wappagedev_id=$id");
  }/*else
  { //template view
    $template = sqlQueryValue("SELECT template_id  FROM ". $site_id . "_views WHERE view_id=".-$id);
  }*/
  if($template)
  {
    list($data, $copybody) = sqlQueryRow("SELECT body, copybody FROM " . $site_id . "_waptemplates WHERE template_id=$template");
    while($copybody)
      list($data, $copybody) = sqlQueryRow("SELECT body, copybody FROM " . $site_id . "_waptemplates WHERE template_id=$copybody");
  }
  $r = componentArray($data, $wappagedev_id);
  if(!count($r))
  {
    //echo "No components in this page";
    /*$r[] = Array ( "type" => formatedtext0, 
                   "name" => formatedtext043n1, 
                   "options" => "",
                   "ind" => 0,
                   "tag" => "{%component:formatedtext0:formatedtext043n1%}"
            );
    for($f=0; $f<count($r); $f++)
    {
      displayPropertiesTable($r[$f]["type"], $r[$f]["name"], $site_id, $id, $form_data);
    }
    print_r($form_data);*/
      $value = sqlQueryValue("SELECT propertyvalue FROM " . $site_id . "_wapcontentsdev WHERE wappagedev=$wappagedev_id AND componentname='stdcontents'");
    
      $form_data["type_wapcontents"] = Array ( "value" => "stdcontents", "type" => "hidden" ); 
      $form_data["component_stdcontents_name"] = Array ( 
                    "label" => "{%langstr:wap_page%}", 
                    "type" => "title", 
                    "value" => "wapcontents", 
                    "default" => 1,
                    "defval" => ''
      );
      $form_data["component_stdcontents_stdtext"] = Array(
            "label"     => "{%langstr:contents%}:",
            "type"      => "waptext",
            "rows"      => "6",
            "cols"      => "30",
            "search"    => true,
            "value"     => $value
            );
     /*
$form_data["component_stdcontents_stdtext"] = Array(
            "label"     => "Contents:",
            "type"      => "wysiwyg",
            "width"     => "250",
            "height"    => "120",
            "value"     => $value,
            "dialog"    => "?module=wysiwyg&canpreview='+((window.parent.dialogArguments)?1:0)+'&site_id=".$GLOBALS['site_id']."&page_id=".$this->page_id,
            "dialogw"   => "720",
            "dialogh"   => "450",
            "dlg_res"   => true
            );      
      */

      $Inspector = new Inspector();
      
      $Inspector->action = '?module=' .$GLOBALS['module']. '&site_id=' .$site_id. '&wappagedev_id=' .$id. '&id=' .$id. '&action=updatecontrols&redirect=1';
      
      $Inspector->properties = $form_data;
      $Inspector->opened = true;
      $Inspector->buttons =true;

      $Inspector->cancel ="closeparent";
      $Inspector->name = "Form";
      echo $Inspector->output();

  }
  else
  {
 //$type, $name, $site_id, $page_id, &$form_data, $hide = false, $filterproperties = '',$wap = false)
    $form_data = Array();
    for($f=0; $f<count($r); $f++)
    {
      displayPropertiesTable($r[$f]["type"], $r[$f]["name"], $site_id, $id, $form_data, false, '', true);
    }
    //displayPropertiesTable($type, $name, $site_id, $page_id, &$form_data, $hide = false, $filterproperties = '')

    $Inspector = new Inspector();
    $Inspector->properties = $form_data;
    $Inspector->opened = (count($r) == 1);
    $Inspector->buttons =true;

    $Inspector->cancel ="closeparent";
    $Inspector->name = "Form";
    $Inspector->action = '?module=' .$GLOBALS['module']. '&site_id=' .$site_id. '&wappagedev_id=' .$id. '&id=' .$id. '&action=updatecontrols';
    if($wappagedev_id > 0)
      $Inspector->action .= '&redirect=1';
    echo $Inspector->output();

  }
  
  
?>
