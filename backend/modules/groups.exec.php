<?
  $perm_managegroups = $GLOBALS['perm_managegroups'];
  $perm_managesitegroups = $GLOBALS['perm_managesitegroups'];

  function AddGroupPermissions($group_id, $site_id)
  {
    include_once($GLOBALS['cfgDirRoot']."backend/modules/"."permissions.texts.php");
    foreach($_POST as $key => $value)
    {
      if(substr($key, 0, 11) == "permission_")
      {
        $num = intval(substr($key, 11, strlen($key)));
        if($permissions_list[$num]['type'] == 0)
        {
          sqlQuery("INSERT INTO permissions (perm_num, isuser, id) VALUES ($num, 0, $group_id)");
        }else if(($permissions_list[$num]['type'] == 1)and($site_id))
        {
          sqlQuery("INSERT INTO permissions (perm_num, isuser, id, data) VALUES ($num, 0, $group_id, $site_id)");
        }
      }
    }
  }

  if((($action == 'addgroup')and(($perm_managegroups)or($perm_managesitegroups)))or(($action == 'modgroup')and(($perm_managegroups)or($perm_managesitegroups))))
  {
    $site_id = $_GET['site_id'];
    $groupname = $_POST['groupname'];
    $description = $_POST['description'];
    $group_id = $_POST['group_id'];
    //Check if field input is valid
    $invalid = Array();
    $jsmessage = '';
    if((IsEmpty($groupname))or($badgroup = IsBadSymbols($groupname))or($existsgroup = sqlQueryValue("SELECT group_id FROM groups WHERE groupname='$groupname' AND group_id<>'$group_id'")))
    {
      $invalid[] = "groupname";
      if($badgroup)
        $jsmessage .= 'Group name ' . msgInvalidChars . '\n';
      if($existsgroup)
        $jsmessage .= 'Group name ' . msgUserExists . '\n';
    }
    if(IsLtGtSymbols($description))
    {
      $invalid[] = "description";
      $jsmessage .= 'Description ' . msgNoLtGt . '\n';
    }
    if(count($invalid))
    {
      if($action == 'addgroup')
        $action = 'create';
      if($action == 'modgroup')
        $action = 'properties_form';
      $invalid = join(':', $invalid);
      $groupname = urlencode(stripslashes($groupname));
      $description = urlencode(stripslashes($description));
      Redirect('?module='.$module.'&action='.$action.'&invalid=' . $invalid . '&formdata_groupname='.$groupname.'&formdata_description='.$description.'&jsmessage='.$jsmessage.'&site_id='.$site_id.'&id='.$group_id);
      Die();
    }
  }

  if(($action == 'addgroup')and(($perm_managegroups)or($perm_managesitegroups)))
  {
    sqlQuery("INSERT INTO groups (groupname, description, site_id) VALUES ('$groupname', '$description', '$site_id')");
    $group_id = sqlQueryValue("SELECT group_id FROM groups WHERE groupname='$groupname'");
    AddGroupPermissions($group_id, $site_id);
    Add_Log("New group '$groupname' ($group_id)", $site_id);
  }
  else if(($action == 'modgroup')and(($perm_managegroups)or($perm_managesitegroups)))
  {
    sqlQuery("UPDATE groups SET groupname='$groupname', description='$description' WHERE group_id='$group_id'");
    sqlQuery("DELETE FROM permissions WHERE id=$group_id AND isuser=0");
    AddGroupPermissions($group_id, $site_id);
    Add_Log("Updated group '$groupname' ($group_id)", $site_id);
  }
  else if(($action == 'delgroup')and(($perm_managegroups)or($perm_managesitegroups)))
  {
    $group_id = $_GET['id'];
    list($cantdelete, $groupname) = sqlQueryRow("SELECT cantdelete, groupname FROM groups WHERE group_id=$group_id");
    if(!$cantdelete)
    {
      sqlQuery("DELETE FROM groups WHERE group_id=$group_id");
      sqlQuery("DELETE FROM users_groups WHERE group_id=$group_id");
      sqlQuery("DELETE FROM permissions WHERE id=$group_id AND isuser=0");
      $site_id = $_GET['site_id'];
      Add_Log("Deleted group '$groupname' ($group_id)", $site_id);
    }
  }
?>
