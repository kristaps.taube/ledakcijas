<?


$perm_managesite = CheckPermission(7, $_GET["id"]);
$perm_managesites = $GLOBALS['perm_managesites'];
if((!$perm_managesites)and(!$perm_managesite))
{
  AccessDenied(True);
}

// Define form
$form_data = Array(

  "site_id"     => Array(
    "label"     => "ID:",
    "type"      => "hidden"
  ),

  "ind"         => Array(
    "noshow"    => true
  ),

  "title"        => Array(
    "label"     => "{%langstr:title%}:",
    "type"      => "str"
  ),
  
  "description" => Array(
    "label"     => "{%langstr:col_description%}:",
    "type"      => "html",
    "rows"      => "4",
    "cols"      => "30"
  ),

  "domain"       => Array(
    "label"     => "{%langstr:domain_name%}:",
    "type"      => "str",
    "size"      => "30",
    "maxlength" => "100"
  ),

  "alias"       => Array(
    "label"     => "{%langstr:domain_aliases%}:",
    "type"      => "str",
    "size"      => "30",
  ),

  "wwwroot"       => Array(
    "label"     => "{%langstr:web_root%}:",
    "type"      => "str",
    "size"      => "30",
    "maxlength" => "250"
  ),
  
  "dirroot"       => Array(
    "label"     => "{%langstr:directory_root%}:",
    "type"      => "str",
    "size"      => "30",
    "maxlength" => "250"
  ),

  "docroot"       => Array(
    "label"     => "{%langstr:document_root%}:",
    "type"      => "str",
    "size"      => "30",
    "maxlength" => "250"
  ),

  "created"     => Array(
    "label"     => "{%langstr:col_created%}:",
    "type"      => "str",
    "size"      => "30",
    "disabled"  => true
  ),

  "lastmod"    => Array(
    "label"     => "{%langstr:last_modified%}:",
    "type"      => "str",
    "size"      => "30",
    "disabled"  => true
  ),
  
  "modby"    => Array(
    "label"     => "{%langstr:modify_by%}:",
    "type"      => "str",
    "size"      => "30",
    "disabled"  => true
  ),

  "wapsite"     => Array(
    "label"     => "{%langstr:wap_site%}:",
    "type"      => "check",
        "value"         => "0"
  )
  
);

// Fill some values
  $id = $_GET["id"];
  $action = $_GET['action'];
  $create = ($action == 'create');
  



  if(!$create){
    $row = sqlQueryRow("SELECT sites.*, users.username as modby FROM sites LEFT JOIN users ON sites.modby=users.user_id WHERE sites.site_id=$id");
    $form_data['site_id']['value'] = $id;
    $form_data['domain']['value'] = $row['domain'];
    $form_data['alias']['value'] = $row['alias'];
    $form_data['title']['value'] = $row['title'];
    $form_data['description']['value']  =$row['description'];
    $form_data['wwwroot']['value'] = $row['wwwroot'];
    $form_data['dirroot']['value'] = $row['dirroot'];
    $form_data['docroot']['value'] = $row['docroot'];
    $form_data['created']['value'] = date("Y-m-d H:i:s", $row['created']);
    $form_data['lastmod']['value'] = date("Y-m-d H:i:s", $row['lastmod']);
    $form_data['modby']['value'] = $row['modby'];
    $form_data['wapsite']['value'] = $row['wapsite'];
  }
  
  $invalid = explode(':', $_GET['invalid']);
  foreach($invalid as $i)
  {
    if($i)
      $form_data[$i]['error'] = true;
  }
  
  foreach(array_keys($_GET) as $key)
  {
    $s = explode('_', $key, 2);
    if($s[0] == 'formdata')
      $form_data[$s[1]]['value'] = stripslashes($_GET[$key]);
  }
  
  if($_GET['jsmessage'])
  {
    echo '<script language="JavaScript">
          <!--
          alert("' . stripslashes($_GET['jsmessage']) . '");
          <!---->
          </script>';
  }

// Use FormGen to bring this out

  require($GLOBALS['cfgDirRoot']."library/"."class.formgen.php");
  $FormGen = new FormGen();
  if(!$create)
  {
    $FormGen->action = "?module=".$GLOBALS['module']."&action=modsite";
  }else{
    $FormGen->action = "?module=".$GLOBALS['module']."&action=addsite";
  }
  $FormGen->cancel = "close";
  if(!$create)
  {
    $FormGen->title = "{%langstr:site_properties%} : " . $row['title'];
  }else{
    $FormGen->title = "{%langstr:create_new_site%}";
  }
  $FormGen->properties = $form_data;
  $FormGen->buttons = true;
  echo $FormGen->output();

?>
