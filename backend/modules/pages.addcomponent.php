<?

  $perm_managecontents = $GLOBALS['perm_managecontents'];
  if(!$perm_managecontents)
    AccessDenied(True);


  $id = $_GET['page_id'];
  $GLOBALS['page_id'] = $id;
  $page_id = $id;
  $site_id = $GLOBALS['site_id'];
  $pagedev_id = $_GET['pagedev_id'];

  if($page_id>0)
  {   //real page
    $template = sqlQueryValue("SELECT template FROM ". $site_id . "_pagesdev WHERE pagedev_id=$pagedev_id");
  }
  if (!CheckPermission(12, $site_id, $template))
  {
     AccessDenied();
  }

  echo '
  <script>
  window.advanced=false;
  function toggle(advanced)
  {
    if (advanced) 
    {
        document.all["a1"].style.display = "none";
        document.all["a2"].style.display = "none";
        document.all["a3"].style.display = "none";
        window.advanced = false;
    }
    else 
    {
        document.all["a1"].style.display = "block";
        document.all["a2"].style.display = "block";
        document.all["a3"].style.display = "block";
        window.advanced = true;
    }
  }

  function Validate()
  {
    var filter_1  = /^[a-zA-Z0-9]+$/;

    if(document.Form.componentname.value != "" && !filter_1.test(document.Form.componentname.value)) {
        alert("Component name can contain only letters and digits!");
        return(false);
    }
    else if (document.Form.componentname.value == "") {
        alert("Please eneter component name!");
        return(false);    
    }
    else return true;
  }
  </script>
  ';

  /*if($page_id>0)
  {   //real page
    $template = sqlQueryValue("SELECT template FROM ". $site_id . "_pagesdev WHERE pagedev_id=$pagedev_id");
  }
  if($template)
  {
    list($data, $copybody) = sqlQueryRow("SELECT body, copybody FROM " . $site_id . "_templates WHERE template_id=$template");
    while($copybody)
      list($data, $copybody) = sqlQueryRow("SELECT body, copybody FROM " . $site_id . "_templates WHERE template_id=$copybody");
  }

  $data = attachSubTemplates($data, $site_id);*/
//print_R($data);
//print_R($_GET);
  /*$r = componentArray($data, $page_id);
  if(!count($r))
  {
    echo "No components in this page";
  }
  else
  {
    $form_data = Array();
    for($f=0; $f<count($r); $f++)
    {
      displayPropertiesTable($r[$f]["type"], $r[$f]["name"], $site_id, $pagedev_id, $form_data, false, 'visible:name');
    }
    $i=0;*/
//print_R($_GET);
    echo '<form name="Form" action="?module=' .$GLOBALS['module']. '&site_id=' .$site_id. '&pagedev_id=' .$pagedev_id. '&page_id=' .$page_id. '&action=addcomponentsave&component_name='.$_GET['component_name'].'&component_type='.$_GET['component_type'].'" method="post" OnSubmit="return Validate()">';
    echo '<table cellpadding="5" cellspacing="0" width="100%">';
    echo '<tr>';
    echo '<td class="formTitle">{%langstr:new_component%}</td><td class="formTitle"></td>';
    echo '</tr>';

    $components = sqlQueryColumn("SELECT type FROM components WHERE wap=0 AND enabled=1 ORDER BY type");

    $combo = '<select name="combocomponents" size="1" style="margin-left:10px;">';
    foreach($components as $componenttype)
    {
      $combo .= '  <option value="' . $componenttype . '" selected="selected">' . $componenttype . '</option>'."\n";
    }
    $combo .= '</select>';

    echo '<tr><td class=formLabel colspan="2">{%langstr:add_component_form%}<br/><input type="radio" name="position" value="above"> {%langstr:above%} '.str_repeat("&nbsp;",7).'<input type="radio" name="position" value="below" checked> {%langstr:below%}<br/>
        {%langstr:selected_form%} ('.$_GET['component_name'].')
        </td>
        </tr>';

    echo '<tr><td class=formLabel colspan="2">{%langstr:component%}: '.$combo.'</td></tr>';
    echo '<tr><td class=formLabel colspan="2">{%langstr:col_name%}: <input type="text" style="width:230px;margin-left:37px;" class=formEdit name="componentname"></td></tr>';

    echo '<tr><td class=formLabel><input type="radio" name="pagevisibility" value="all" checked> {%langstr:add_to_all_pages%}
        <br/><input type="radio" name="pagevisibility" value="this"> {%langstr:add_to_this_page%}</td>
        <td></td></tr>';

    echo '<tr><td class=formLabel><input type="button" name="advanced" value="{%langstr:advanced%}" onClick="toggle(window.advanced);"></td><td></td></tr>';


    echo '<tr id="a1" style="display:none;"><td class=formLabel><input type="checkbox" name="visualedit" checked=true> {%langstr:allow_visual_edit%}</td><td></td></tr>';
    echo '<tr id="a2" style="display:none;"><td class=formLabel colspan="2">{%langstr:width%}: <input style="width:230px;margin-left:37px;" type="text" class=formEdit name="width"></td></tr>';
    echo '<tr id="a3" style="display:none;"><td class=formLabel colspan="2">{%langstr:height%}: <input style="width:230px;margin-left:33px;" type="text" class=formEdit name="height"></td></tr>';

    echo '<tr>';
    echo '<td colspan="2" align="right"><input class=formButton type="submit" value="{%langstr:save%}"> <input class=formButton type="button" value="{%langstr:cancel%}" onClick="jacascript:window.close();"></td>';
    echo '</tr>';
    echo '</table>';
    echo '</form>';

  //}


?>
