<?
$GLOBALS['site_id'] = $_GET["site_id"];
$site_id = $_GET["site_id"];
$page_id = $_GET['page_id'];

$action = $_GET["action"];
$GLOBALS['tabs'] = 'site';


//echo $GLOBALS['REQUEST_URI'];


unset($perm_modsite);
if(($site_id == $GLOBALS["currentUserSiteID"])or(!$GLOBALS["currentUserSiteID"]))
  $perm_modsite = CheckPermission(6, $site_id);
  //$perm_modsite = ( CheckPermission(6, $site_id) && CheckPermission(17, $site_id) );

if(!$perm_modsite)
{
  echo "Permission Denied!";
  AccessDenied(True);
}

$docScripts['sellanguage'] = "sellanguage.default.php";
include($GLOBALS['cfgDirRoot']."backend/modules/"."sellanguage.setlanguage.php");
$GLOBALS['havelanguages'] = HaveLanguages($site_id);
$GLOBALS['PageEncoding'] = FullEncoding($GLOBALS['currentlanguage']);
$GLOBALS['maincharset']  = $GLOBALS['currentlanguage'];


function LS_getAllStrings($site_id, $activelanguage, $devmode = false)
{
  //obtain list of all templates and all components
  $tdata = sqlQueryData("SELECT body FROM " . $site_id . "_templates ORDER BY ind");
  $compstrings = Array();
  $compdefs = Array();
  $compdefvals = Array();
  $all_components = array();
  foreach($tdata as $tmp)
  {
    $body = $tmp['body'];

    //global strings from templates
    while (preg_match("/{%ls:([a-zA-Z0-9_]+)(:(([^%])*))?%}/", $body, $regs)){
			$key = transliterateText($regs[3]);
      $row = DB::GetRow("SELECT * FROM ".$site_id."_languagestrings WHERE component='_template' AND name=:name AND language = :lang", array(":name" => $key, ":lang" => $activelanguage));

			$value = $row ? $row['value'] : $regs[3];

      $body = str_replace($regs[0], $value, $body);

      $compstrings['_template'][$key] = $value;
			$compdefs['_template'][$row['name']] = $value == $row['default'];
      $compdefvals['_template'][$key] = $row['default'];
    }

    $r = componentArray($body, 0);
    $all_components = array_merge($all_components, $r);
    $GLOBALS['page_id'] = -1;
    foreach($r as $component)
    {
      $type = $component['type'];
      $name = $component['name'];
      if(file_exists($GLOBALS['cfgDirRoot'] . 'components/class.' . $type . '.php'))
      {
        //Obtain the array of properties for the component

        include_once($GLOBALS['cfgDirRoot'] . 'components/class.' . $type . '.php');
        $obj = new $type($name);
        $obj->activelanguage = $activelanguage;

        $ls = $obj->LS[0];

        if($ls && count($ls))
        {
          if (!isset($compstrings[$type])) $compstrings[$type] = Array();
          foreach($ls as $key => $s)
          {
            if (isset($compstrings['_template'][$key]) || isset($compstrings[$type][$key])) continue;

            if(substr($key, 0, 1) != '_'){

              $t = $obj->LS($key, $devmode);

              $compstrings[$type][$key] = $obj->LS($key, $devmode);
              $compdefs[$type][$key] = $obj->LS_def[$obj->activelanguage][$key];
              $compdefvals[$type][$key] = $s;
            }else
            {
              $compstrings['_template'][$key] = $obj->LS($key, $devmode);
              $compdefs['_template'][$key] = $obj->LS_def[$obj->activelanguage][$key];
              $compdefvals['_template'][$key] = $s;
            }
          }
        }
      }
    }
  }

  // adding new l generated strings
  foreach($all_components as $component){
    $type = $component['type'];
    $name = $component['name'];

    // new l method generated language strings
    $new_ls = DB::GetTable("
			SELECT ls.*
			FROM ".$site_id."_languagestrings AS ls
			LEFT JOIN ".$site_id."_languagestrings_info AS i ON i.name = ls.name AND i.component = ls.component
			WHERE
				ls.component = :type AND
				ls.language = :language AND
				i.is_used = 1
		", array(
    	":type" => $type,
      ":language" => $activelanguage
		));

    if($new_ls){
      foreach($new_ls as $l){

        if(!isset($compstrings[$type][$l['name']])){

          $compstrings[$type][$l['name']] = $l['value'];
          $compdefs[$type][$l['name']] = $l['value'] == $l['default'];
          $compdefvals[$type][$l['name']] = $l['default'];

        }

      }
    }


  }

  ksort($compstrings);

  return array(
    'compstrings' => $compstrings,
    'compdefvals' => $compdefvals,
    'compdefs' => $compdefs
  );
}



$action = $_GET["action"];
require($GLOBALS['cfgDirRoot']."library/"."class.toolbar.php");

switch ($action) {

  case 'export_xls':
  case 'savestrings':
  case 'getstrings':
    $docTemplate = "main.htm";
    $docTitle = "Language Strings";
    $docScripts['body'] = "languagestrings.exec.php";
    break;

  default:
    $docTemplate = "main.htm";
    $docTitle = "Language Strings";
    $docScripts['body'] = "languagestrings.default.php";
    break;

}
