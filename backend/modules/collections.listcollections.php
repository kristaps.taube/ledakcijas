<?

$site_id = $GLOBALS['site_id'];


$cols = Array(

  "coldesc" => Array(
    "width"     => "100%",
    "title"     => "Value",
    "format"    => "<a href=\"#\" OnClick=\"javascript:window.location='?module=collections&site_id=" . $site_id . "&action=editcollection&id={%collection_id%}'\">{%coldesc%}</a> ({%c%})"
  )

);

$coltype = AddSlashes($_GET['coltype']);
if(files_exists($GLOBALS['cfgDirRoot'] . "collections/class." . $coltype . ".php"))
{
  include_once($GLOBALS['cfgDirRoot'] . "collections/class." . $coltype . ".php");

  $rawdata = sqlQueryData("SELECT collection_id, type, name FROM " . $site_id . "_collections WHERE type='$coltype'");

  $data = array();
  foreach($rawdata as $key => $row)
  {
    $colname = $row['name'];
    $collection_id = $row['collection_id'];
    $collection = new $coltype($colname,$collection_id);
    if($collection->IsEditableOutside())
    {
      $row['coldesc'] = $collection->longname;
      $row['c'] = $collection->ItemCount();
      $data[] = $row;
    }
  }

  require($GLOBALS['cfgDirRoot']."library/"."class.datagrid.php");

  $Table = new DataGrid();
  $Table->cols = $cols;
  $Table->data = $data;
  $Table->footer = false;
  $Table->rowformat = " onClick=\"javascript:HighLightTR(this, {%collection_id%});\"";
  echo $Table->output();
}

?>
