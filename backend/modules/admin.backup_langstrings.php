<?
if(!$GLOBALS['perm_accessadmin'])
  Die('Access Forbidden');

$action = $_GET['action'];
if($action == 'database')
{
  echo '
  <form action="?module=admin&action=backupdatabase&site_id='.$_GET['site_id'].'" method="POST">
  <input type="checkbox" name="backupdatabase" checked="checked" />{%langstr:backup_backup_data%}<br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="includesearch" />{%langstr:backup_include_cache_search%}<br />
  <br />
  <input type="checkbox" name="backupfiles" checked="checked" />{%langstr:backup_backup_files%}<br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="newfiles" />
  {%langstr:backup_files_newer%} 
  <INPUT maxLength=2 size=2 name=day';
  echo ' value='.date("d", mktime(0, 0, 0, date("m")-1, date("d"),  date("Y")));
  echo '>';

  $mon=date("M", mktime(0, 0, 0, date("m")-1, date("d"),  date("Y")));

  $month=array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
  //print_r($month);
  echo'<SELECT name=month>';
    foreach ($month as $val)
	{
      echo '<OPTION value='.$val; 
      if ($mon == $val) echo ' selected';
      echo '>'.$val.'</OPTION>';
	}
  echo '</SELECT>
  <INPUT maxLength=4 size=4 name=year';
  echo ' value='.date("Y", mktime(0, 0, 0, date("m")-1, date("d"),  date("Y")));
  echo '>
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="smallfiles" />
  {%langstr:backup_only_files_smaller%}
  <INPUT maxLength=8 size=8 name=size value=100>
  <SELECT name=sizetype>
    <OPTION value=B>{%langstr:bytes%}</OPTION>
    <OPTION value=KB selected>{%langstr:kilobytes%}</OPTION>
	<OPTION value=MB>{%langstr:megabytes%}</OPTION>
    <OPTION value=GB>{%langstr:gigabytes%}</OPTION>
  </SELECT><br />
  <input type="checkbox" name="backupdocs" checked="checked" />{%langstr:backup_backup_documents%}<br />
  <br />
  <br />
  <input type="submit" name="submit" value="{%langstr:backup_backup_database%}">
  </form>';

}

?>
