<?

$GLOBALS['tabs'] = 'site';
$site_id = $_GET['site_id'];

if(!empty($_POST))
{

  if(isset($_POST['accept']))
  {

    if(!empty($_POST['email']))
    {

      $title = explode("@", $_POST['email']);
      $title = $title[0];

    }

    $protocol = strpos(strtolower($_SERVER['SERVER_PROTOCOL']),'https') === FALSE ? 'http' : 'https';
    $host     = $_SERVER['HTTP_HOST'];
    $script   = $_SERVER['SCRIPT_NAME'];
    $params   = $_SERVER['QUERY_STRING'];
    $url = $protocol . '://' . $host . $script . '?' . $params;

    $raw_url= parse_url($url);
    preg_match ("/\.([^\/]+)/", $raw_url['host'], $domain_only);
    $domain = $domain_only[1];
    $country_code = 'lv';
    // seo.datateks.lv
    list($GLOBALS["success"], $GLOBALS["response"]) = XMLRPC_request(
      'seo.datateks.lv',
      "/xmlrpc/",
      "Seo.RegisterSelfserviceClient",
      array(
        XMLRPC_prepare("seoportal_eu"),
        XMLRPC_prepare("k2PAce2EC2BRUra"),
        XMLRPC_prepare(
          array(
            "email" => $_POST['email'],
            "title" => $title,
            "type" => "individual",
            "from_constructor" => true,
            "domain" => $domain,
            "country_code" => $country_code
          )
        )
      )
    );

    if($GLOBALS["success"]){

      option("seo\\api_access_key", $GLOBALS["response"]["api_access_key"], "SEO API access key", $params = array("is_advanced" => 1));

    }
  }else{
    $GLOBALS['no_accept'] = true;
  }
}

$docStrings['test'] = "idusgiusdibusdvbuivs";
$docTemplate = "main.htm";
$docTitle = "Seo system";
$docScripts['body'] = "seo.default.php";

?>
