<?

$site_id = $GLOBALS['site_id'];


$cols = Array(

  "coldesc" => Array(
    "width"     => "100%",
    "title"     => "{%langstr:col_value%}",
    "format"    => "<a href=\"#\" OnClick=\"javascript:window.location='?module=collections&site_id=" . $site_id . "&action=editcollection&id={%collection_id%}'\">{%coldesc%}</a> ({%c%})"
  )

);


$id = $_GET['id'];
$row = sqlQueryRow("SELECT collection_id, type, name FROM " . $site_id . "_collections WHERE collection_id='$id'");

$coltype = $row['type'];
$colname = $row['name'];
if($coltype)
{
  $collection_id = $row['collection_id'];
  $classname = pdocollection::typeToClassname($coltype);
  $collection = new $classname($colname,$collection_id);

  if($_GET['setfiltervalue'] && $_GET['setfilterfield'])
  {
    $_SESSION['collections_viewfilter_' . $collection->name] = Array();
    $_SESSION['collections_viewfilter_' . $collection->name][$_GET['setfilterfield']] = $_GET['setfiltervalue'];
    //redirect to get rid of these GET parameters
    $url = '?';
    foreach($_GET as $key => $val)
    {
      if($key != 'setfiltervalue' && $key != 'setfilterfield')
        $url .= $key . '=' . urlencode($val) . '&';
    }
    Redirect($url);
  }
//  else if ($_GET['resetfilters'])
//    $_SESSION['collections_viewfilter_' . $collection->name] = Array();

  //if($collection->IsEditableOutside())
  {

    $collection->Edit();
    $GLOBALS['docStrings']['toolbar'] = $collection->EditToolbar();
  }
}else
{
  header('Location: ?module='.$_GET['module'].'&site_id='.$_GET['site_id']);
  Die();
}

?>
