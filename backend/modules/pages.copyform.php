<?

// Define form
$site_id = $GLOBALS['site_id'];
if (isset($_GET['id'])) 
{
    $pagedev_id = $_GET['id'];
    $row = sqlQueryRow("SELECT * FROM ".$site_id."_pagesdev WHERE pagedev_id=$pagedev_id");
    $page_id = $row['page_id'];
    $title = $row['title'];
}


function getTree($page_id, &$data)
{
    $childs = sqlQueryData("select page_id from ".$_GET['site_id']."_pages where parent=$page_id");
    foreach ($childs as $child)
    {
        $data[] = $child['page_id'];
        getTree($child['page_id'], $data);
    }
}
//get array of subpages for current page
$data[] = $row['page_id'];
getTree($row['page_id'], $data);


function page_level($page_id, $site_id)
{
  $l = 0;
  while($page_id)
  {
    $l++;
    $page_id = sqlQueryValue("SELECT parent FROM " . $site_id . "_pages WHERE page_id=$page_id");
  }
  return $l;
}


$pages = sqlQueryData("SELECT page_id,name,parent,title FROM " . $site_id . "_pages ORDER BY ind");
ListNodes($pages, $combopages, 0, 0);

foreach ($combopages as $key=>$row)
{
    $p = explode(":",$row);
    if (in_array($p[0],$data))
        unset($combopages[$key]);
}

$languages = sqlQueryData("SELECT language_id, fullname FROM " . $site_id . "_languages");
$langarr = Array("0:Do not change language");
foreach($languages as $lang)
{
  $langarr[] = $lang['language_id'] . ':Change to ' . $lang['fullname'];
}

$form_data = Array(

  "from"      => Array(
    "type"      => "hidden"
  ),

  "fromname"      => Array(
    "label"     => "{%langstr:pages_from%}:",
    "disabled"  => true,
    "value"     => "",
    "type"      => "str"
  ),

  "to"      => Array(
    "label"     => "{%langstr:pages_to%}:",
    "type"      => "list",
    "lookup"    => $combopages
  ),

  "language"      => Array(
    "label"     => "Language:",
    "type"      => "list",
    "lookup"    => $langarr
  ),

);

// Fill some values
  $action = $_GET['action'];
  $create = ($action == 'create');

  $form_data['from']['value'] = $page_id;
  $form_data['fromname']['value'] = $title;

  $invalid = explode(':', $_GET['invalid']);
  foreach($invalid as $i)
  {
    if($i)
      $form_data[$i]['error'] = true;
  }

/*
  foreach(array_keys($_GET) as $key)
  {
    $s = Split('_', $key, 2);
    if($s[0] == 'formdata')
      $form_data[$s[1]]['value'] = stripslashes($_GET[$key]);
  }
*/
  if($_GET['jsmessage'])
  {
    echo '<script language="JavaScript">
          <!--
          alert("' . stripslashes($_GET['jsmessage']) . '");
          <!---->
          </script>';
  }



// Use FormGen to bring this out

  require($GLOBALS['cfgDirRoot']."library/"."class.formgen.php");
  $FormGen = new FormGen();

  $FormGen->action = "?module=".$GLOBALS['module']."&site_id=".$site_id."&action=docopy";

  $FormGen->cancel = "close";
  $FormGen->title = "{%langstr:copy_tree%}";
  
  $FormGen->properties = $form_data;
  $FormGen->buttons = true;
  echo $FormGen->output();

?>
