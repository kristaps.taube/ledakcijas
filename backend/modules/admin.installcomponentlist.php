<?
$dir = $GLOBALS['cfgDirRoot'] . 'components/';
chdir($dir);

  $handle = opendir($dir);
  while ($file = readdir($handle)) {
    if (is_dir($file) && $file != ".")
      $dirlist[] = $file;
    elseif (is_file($file))
      $filelist[] = $file;
  }
  closedir($handle);
  asort($filelist);

//kategorijas
$cat_ar = sqlQueryData('SELECT category_id, category_name FROM component_categories ORDER BY category_name');
$cat_opt = '';
foreach($cat_ar as $cat)
{
  $cat_opt .= '<option value="'.$cat['category_id'].'">' . $cat['category_name'] . '</option>';
}

//regjistreetie komponenti
$data = sqlQueryData("SELECT component_id, enabled, type, category_id FROM components ORDER BY  type");
$components = Array();
foreach($data as $comp)
{
  $components[$comp['type']]['enabled'] = $comp['enabled'];
  $components[$comp['type']]['category_id'] = $comp['category_id'];
}

echo '<table width="100%" border="0">';
echo '<form name="Form" method="POST" action="?module=components&action=saveinstallcomponents">';
foreach($filelist as $file)
{
  if(preg_match('/^class[.](.*)[.]php$/', $file, $matches) && !$components[$matches[1]]['enabled'])
  {
    echo '<tr><td>';
    echo '<input type="checkbox" name="cb_' . $matches[1] . '" '.($components[$matches[1]]['enabled'] ? 'checked="checked" ':'').'/>';
    echo '</td><td>';
    echo $matches[1];
    echo '</td><td>';
    echo '<select name="' . $matches[1] . '_cat">' . $cat_opt . '</select>';
    if($components[$matches[1]]['category_id'])
    {
      echo '<script>Form.' . $matches[1] . '_cat.value = "'.$components[$matches[1]]['category_id'].'";</script>';
    }
    echo '</td></tr>';
  }
}
echo '<tr><td colspan="3" align="right"><input type="submit" class="formButton" value="&nbsp;&nbsp;OK&nbsp;&nbsp;"></td></tr>';
echo '</form>';
echo '</table>';

?>
