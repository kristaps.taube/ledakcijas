<?

$site_id = $GLOBALS['site_id'];


$cols = Array(

  "coldesc" => Array(
    "width"     => "100%",
    "title"     => "Value",
    "format"    => "<a href=\"#\" OnClick=\"javascript:window.location='?module=collections&site_id=" . $site_id . "&action=listcollections&coltype={%type%}'\">{%coldesc%}</a> ({%c%})"
  )

);


function OutputTableHead($row, $expanded)
{
  echo '
  <table width="100%" border="0" cellspacing="0" cellpadding="4" style="border: 1px solid #C0C0C0;">
  <tr>
    <td width="90%" class="tableHead">
      <a href="#" style="color: #9CB4E4" onclick="javascript:'.
      iif(!$expanded,'Un','').'ExpandCollectionBox(\''.$row['type'].'\');">'.$row['coldesc'].' ('.$row['c'].')</a>
    </td>
    <td width="10%" class="tableHead" align="right">';
    
    if(!$expanded)
    {
      echo '<a href="#" style="color: #9CB4E4" onclick="javascript:UnExpandCollectionBox(\''.$row['type'].'\');
                                                                   "><img border="0" src="'.$GLOBALS['cfgWebRoot'].'gui/images/ico_expand.gif"></a>';
    }else
    {
      echo '<a href="#" style="color: #9CB4E4" onclick="javascript:ExpandCollectionBox(\''.$row['type'].'\');
                                                                   "><img border="0" src="'.$GLOBALS['cfgWebRoot'].'gui/images/ico_collapse.gif"></a>';
    }
    echo '</td>
  </tr>';
}


function OutputCollectionsBox($row)
{
  global $site_id;
    $coltype = $row['type'];

    $rawdata = sqlQueryData("SELECT collection_id, type, name FROM " . $site_id . "_collections WHERE type='$coltype'");

    $data = array();
    foreach($rawdata as $key => $r)
    {
      $colname = $r['name'];
      $collection_id = $r['collection_id'];
      $classname = pdocollection::typeToClassname($coltype);
      $collection = new $classname($colname,$collection_id);
      if($collection->IsEditableOutside() || $_GET['showallcollections'])
      {
        $r['coldesc'] = $collection->longname;
        $r['c'] = $collection->ItemCount();
        $r['edit'] = $collection->IsEditableOutside();
        $r['islisted'] = sqlQueryValue('SELECT collection_id FROM lists WHERE site_id='.intval($site_id).' AND collection_id='.intval($collection_id));
        $data[] = $r;
      }
    }

  $expanded = ($row[c] <= 3);

  echo '<div id="'.$coltype.'_expanded" style="display: '.iif($expanded, 'inline', 'none').'">';
  OutputTableHead($row, true);
  foreach($data as $r)
  {
    echo'
    <tr bgcolor="#FFFFFF"">
    <td width="100%" class="tableRow">
      <a href="#" '.($r['edit'] ? '' : ' style="color: red;"').' OnClick="javascript:window.location=\'?module=collections&site_id='.$site_id.'&action=editcollection&id='.$r['collection_id'].'&justedit=1&resetfilters=0\'">'.$r['coldesc'].' ('.$r['c'].')</a> ';
    echo '<img src="'.$GLOBALS['cfgWebRoot'].'gui/images/'.($r['islisted']?'graycross':'pluss').'.gif" style="vertical-align: middle;" alt="Add to Quick Edit and Menu" onclick="javascript:openDialog(\'?module=lists&site_id=' . $GLOBALS['site_id'] . '&action=new_list_collection&collection_id='.$r['collection_id'].'\', 400, 550, 1, 0);" />';
    echo '
    </td>
    </tr>  ';
  }
  echo '
  </table><br>
    ';
  echo '</div>';
  echo '<div id="'.$coltype.'_collapsed" style="display: '.iif($expanded, 'none', 'inline').'">';
  OutputTableHead($row, false);
  echo '
  </table><br>
    ';
  echo '</div>';
}


$rawdata = sqlQueryData("SELECT collection_id, type, name FROM " . $site_id . "_collections");

$data = array();
foreach($rawdata as $key => $row)
{
    $coltype = pdocollection::typeToClassname($row['type']);

    $colname = $row['name'];
    $collection_id = $row['collection_id'];

    $collection = new $coltype($colname, $collection_id);
    if($collection->IsEditableOutside() || $_GET['showallcollections'])
    {
        $row['coldesc'] = $collection->description;
        $data[] = $row;
    }

}

$col = Array(Array());
$f = 0;
foreach($data as $row)
{
  $f++;
  $col[$f][] = $row;
  if($f == 3)
    $f = 0;
}

////////////////////////////////
echo '
<table border="0" width="100%" cellspacing="4" cellpadding="4">
  <tr>';
for($f = 1; $f <= 3; $f++)
{
  echo '    <td width="33%" valign="top">';;
  foreach($col[$f] as $row)
  {
    OutputCollectionsBox($row);
  }

  echo '</td>';

}

echo '</tr>
</table>';

///////////////////////////////



?>
