<?

  function HaveLanguages($site_id)
  {
    $data = sqlQueryData("SELECT language_id, shortname, fullname, encoding FROM " . $site_id . "_languages ORDER BY is_default DESC");
    return ($data != null);
  }

  if(!isset($_SESSION['currentlanguagenum']))
  {
    $_GET['setlanguage'] = 0;
  }else if($_SESSION['currentlanguagenum'])
  {
    //test if unexisting language has been selected
    $l = sqlQueryValue("SELECT language_id FROM " . $site_id . "_languages WHERE language_id = " . intval($_SESSION['currentlanguagenum']));
    if(!$l)
      $_GET['setlanguage'] = 0;
  }

  if(!$_SESSION['currentlanguagenum'])
  {
    $nolanguagepages = sqlQueryValue("SELECT count(1) FROM " . $site_id . "_pages WHERE language=0");
    if(!$nolanguagepages)
      $_GET['setlanguage'] = sqlQueryValue("SELECT language_id FROM " . $site_id . "_languages WHERE language_id <> 0  ORDER BY is_default DESC");
  }

  if($_SESSION['currentlanguage'])
  {
    $GLOBALS['currentlanguage'] = $_SESSION['currentlanguage'];
    $GLOBALS['currentlanguagenum'] = $_SESSION['currentlanguagenum'];
  }

  if(isset($_GET['setlanguage'])){
    list($_SESSION['currentlanguage'], $_SESSION['currentlanguagenum']) = sqlQueryRow("SELECT encoding, language_id FROM " . $site_id . "_languages WHERE language_id = " . intval($_GET['setlanguage']));
    if(!$_SESSION['currentlanguage']){
      $_SESSION['currentlanguage'] = 'UTF-8';
      $_SESSION['currentlanguagenum'] = 0;
    }
    $GLOBALS['currentlanguage'] = $_SESSION['currentlanguage'];
    $GLOBALS['currentlanguagenum'] = $_SESSION['currentlanguagenum'];
		$params = $_GET;
		unset($params['setlanguage']);
		$url = "?".http_build_query($params);
		header("location:".$url);
		die();
  }


