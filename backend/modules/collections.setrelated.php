<?
$site_id = $_GET['site_id'];
$page_id = $_GET['page_id'];
if($page_id)
{
  $GLOBALS['PageEncoding'] = PageEncoding($page_id, $site_id);
  $GLOBALS['maincharset']  = PageEncoding($page_id, $site_id, false);
}



$form_data = Array(
  "rel_col"        => Array(
    "label"     => "Collection:",
    "type"        => "collection",
    "collectiontype" => $_GET['relatedcoltype'],
    "lookup"      => GetCollections($site_id, $_GET['relatedcoltype']),
    "dialog"      => "?module=dialogs&action=collection&site_id=".$site_id."&page_id=".$page_id."&category_id=",
    "dialogw"     => "600",
    "value"       => $_GET['relatedcolid'],  
  ),
);



require($GLOBALS['cfgDirRoot']."library/"."class.formgen.php");
$FormGen = new FormGen();
$FormGen->action = "?module=".$GLOBALS['module']."&site_id=".$site_id."&action=setrelatedcollectionsave&category_id=" . $_GET['sourcecol'] . "&coltype=" . $_GET['sourcecoltype'] . "&fieldname=" . $_GET['fieldname'];
$FormGen->title = "Select collection";
$FormGen->cancel =  'close';
$FormGen->properties = $form_data;
$FormGen->buttons = true;
echo $FormGen->output();


?>
