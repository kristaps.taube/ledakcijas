<?

$perm_managefiles = $GLOBALS['perm_managefiles'];
if(!$perm_managefiles)
  AccessDenied(True);

function stdupload()
{
  $form_data = Array(

    "userfile1"    => Array(
      "label"     => "{%langstr:files_filename%}:",
      "size"      => "35",
      "type"      => "file",
      "uploadonly" => true,
    ),

    "userfile2"    => Array(
      "label"     => "{%langstr:files_filename%}:",
      "size"      => "35",
      "type"      => "file",
      "uploadonly" => true,
    ),

    "userfile3"    => Array(
      "label"     => "{%langstr:files_filename%}:",
      "size"      => "35",
      "type"      => "file",
      "uploadonly" => true,
    ),

    "userfile4"    => Array(
      "label"     => "{%langstr:files_filename%}:",
      "size"      => "35",
      "type"      => "file",
      "uploadonly" => true,
    ),

    "userfile5"    => Array(
      "label"     => "{%langstr:files_filename%}:",
      "size"      => "35",
      "type"      => "file",
      "uploadonly" => true,
    ),

    "over"        => Array(
      "label"     => "{%langstr:files_if_exists%}:",
      "value"     => 3,
      "lookup"    => Array('3:{%langstr:do_nothing%}', '1:{%langstr:overwrite%}', '2:{%langstr:auto_rename%}'),
      "type"      => "list"
    )

  );

  require_once($GLOBALS['cfgDirRoot']."library/"."class.formgen.php");
  $FormGen = new FormGen();
  $FormGen->action = "?module=files&action=save&site_id=".$GLOBALS['site_id']."&backup=".$GLOBALS['backup']."&path=".$GLOBALS['path']."&view=thumbs".($GLOBALS['close'] ? "&close=".$GLOBALS['close'] : "");
  $FormGen->title = "{%langstr:files_upload_file%}";
  $FormGen->cancel = ($GLOBALS['close'] ? "?module=files&action=list&site_id=".$GLOBALS['site_id']."&path=".$GLOBALS['path']."&view=thumbs" : 'close');
  $FormGen->properties = $form_data;
  $FormGen->buttons = true;
  echo $FormGen->output();
}


function flashupload()
{
  $upload_url = "/cms/backend/?module=files&action=save&site_id=".$GLOBALS['site_id']."&backup=".$GLOBALS['backup']."&path=".$GLOBALS['path']."&view=thumbs".($GLOBALS['close'] ? "&close=".$GLOBALS['close'] : "")."&session_id=".session_id();

  require_once($GLOBALS['cfgDirRoot']."library/"."class.formgen.php");
  $FormGen = new FormGen();

  ob_start();
?>
  <script type="text/javascript" src="/cms/backend/gui/swfupload/swfupload.js"></script>
  <script type="text/javascript" src="/cms/backend/gui/swfupload/swfupload.swfobject.js"></script>

  <script type="text/javascript">

  var swfu;

  function fileDialogComplete(numFilesSelected, numFilesQueued) {
    swfu.setUploadURL("http://<?=$GLOBALS['cfgDomain'].$upload_url ?>" + "&over=" + document.getElementById("over2").value);
  	try {
  		if (numFilesQueued > 0) {
  			this.startUpload();
  		}
  	} catch (ex) {
  		this.debug(ex);
  	}
  }

  function uploadProgress(file, bytesLoaded)
  {
    var percent = Math.ceil((bytesLoaded / file.size) * 100);
    document.getElementById(file.id).innerHTML = file.name + ": " + percent + "%";
  }

  function uploadError(file, code, msg)
  {
//    document.getElementById(file.id).innerHTML = file.name + ": ERROR: ("+msg+")";
    alert(file.name + ": ERROR: ("+msg+")");
  }

  function uploadSuccess(file, serverData)
  {
    if(serverData.substring(0, 6) == "ERROR:")
      alert(serverData);

    document.getElementById("filelist").removeChild(document.getElementById(file.id));
  }

  function uploadComplete(file)
  {
    if (this.getStats().files_queued > 0) {
    	this.startUpload();
    }
    else {
      //document.flashform.submit();

      if (window.opener){
      	window.opener.location = window.opener.location;
      }

      $("#OKButton").click();

      window.close();

		}
  }

  function queueFile(file)
  {
    var e = document.createElement("div");
    e.id = file.id;
    e.innerHTML = file.name;
    document.getElementById("filelist").appendChild(e);
  }

  function swfuploadLoadFailed()
  {
    document.getElementById("stdcontent").style.display = "block";
    document.getElementById("flashcontent").style.display = "none";
  }

  function swfuploadPreLoad()
  {
  }

  SWFUpload.onload = function()
  {
    swfu = new SWFUpload({
      minimum_flash_version: "8.0.0",
      swfupload_load_failed_handler: swfuploadLoadFailed,
      swfupload_pre_load_handler: swfuploadPreLoad,

      upload_url : "",
      flash_url : <?=json_encode($GLOBALS['cfgWebRoot'].'gui/swfupload/swfupload.swf') ?>,

      post_params: {},

      // File Upload Settings
      file_size_limit : "0",
      file_types : "*.*",
      file_types_description : "All files",
      file_upload_limit : "0",

      file_queue_error_handler : uploadError,
      file_dialog_complete_handler : fileDialogComplete,
      upload_progress_handler : uploadProgress,
      upload_error_handler : uploadError,
      upload_success_handler : uploadSuccess,
      upload_complete_handler : uploadComplete,
      file_queued_handler : queueFile,

      button_placeholder_id: "swfUploadButton",
      button_image_url: "<?=$GLOBALS['cfgWebRoot'].'gui/swfupload/xpbutton.png' ?>",
      button_width: 130,
      button_height: 22,
      button_text : "<span class=\'btext\'>Pievienot failus...</span>",
      button_text_style : ".btext { font-family: Arial, Tahoma, Verdana; font: 10px; }",
      button_text_left_padding : 6,
      button_text_top_padding : 2,
      button_action : SWFUpload.BUTTON_ACTION.SELECT_FILES,
      button_disabled : false,
      button_cursor : SWFUpload.CURSOR.HAND,
      button_window_mode : SWFUpload.WINDOW_MODE.TRANSPARENT
    });

  }

  </script>

  <span id="swfUploadButton"></span>
  <div id="filelist">
  </div>
<?
  $html = ob_get_contents();
  ob_end_clean();


  $form_data = Array(
    "over2"        => Array(
      "label"     => "{%langstr:files_if_exists%}:",
      "value"     => 3,
      "lookup"    => Array('3:{%langstr:do_nothing%}', '1:{%langstr:overwrite%}', '2:{%langstr:auto_rename%}'),
      "type"      => "list"
    ),


    Array(
      'type' => 'code',
      'value' => $html
));

  $FormGen->name = 'flashform';
  $FormGen->action = "?module=files&action=save&site_id=".$GLOBALS['site_id']."&backup=".$GLOBALS['backup']."&path=".$GLOBALS['path']."&view=thumbs".($GLOBALS['close'] ? "&close=".$GLOBALS['close'] : "");

  $FormGen->title = "{%langstr:files_upload_file%}";
  $FormGen->cancel = ($GLOBALS['close'] ? "?module=files&action=list&site_id=".$GLOBALS['site_id']."&path=".$GLOBALS['path']."&view=thumbs" : 'close');
  $FormGen->properties = $form_data;
  $FormGen->buttons = true;
  echo $FormGen->output();

}

echo '<div id="stdcontent" style="display: none">';
stdupload();
echo '</div>';


echo '<div id="flashcontent">';
flashupload();
echo '</div>';



echo '
    <script language="JavaScript">
        if (window.parent.frames.length) window.parent.Form.Ok.disabled = true;
    </script>
';

?>
