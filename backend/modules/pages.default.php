<?php
$havestrings = sqlQueryValue("SELECT Count(1) FROM " . $GLOBALS['site_id'] . "_strings WHERE page=-1");
require($GLOBALS['cfgDirRoot']."library/"."class.rightmenu.php");
$rightmenu = new rightmenu;

$rightmenu->addItem('{%langstr:toolbar_new_page%}','javascript:openDialog(\'?module=pages&site_id='.$GLOBALS['site_id'].'&action=create&sibiling=\'+SelectedRowID, 400, 550, 1, 0);',false,false,'sitemap_new_page');
$rightmenu->addItem('{%langstr:toolbar_new_subpage%}','javascript:if(SelectedRowID)openDialog(\'?module=pages&site_id='.$GLOBALS['site_id'].'&action=create&parent=\'+SelectedRowID+\'&changeparent=1\', 400, 550, 1, 0);',false,false,'sitemap_new_subpage');
$rightmenu->addItem('{%langstr:toolbar_properties%}','javascript:if(SelectedRowID)openDialog(\'?module=pages&site_id='.$GLOBALS['site_id'].'&action=properties_form&id=\'+SelectedRowID, 400, 550, 1, 0);',false,false,'sitemap_properties');
$rightmenu->addSpacer();

$rightmenu->addItem('{%langstr:toolbar_visual_edit%}','javascript:if(SelectedRowID)openDialog(\'?module=pages&site_id='.$GLOBALS['site_id'].'&action=components_visual_frame&id=\'+SelectedRowID+\'&nopageid=1\', screen.availWidth, screen.availHeight-25, 1 , 1);',false,false,'sitemap_visual_edit');
if(!$GLOBALS['cfgMini'])
  $rightmenu->addItem('{%langstr:toolbar_standart_edit%}','javascript:if(SelectedRowID)openDialog(\'?module=pages&site_id='.$GLOBALS['site_id'].'&action=components_form&id=\'+SelectedRowID, 800, 600, 1, 1);',false,false,'sitemap_standart_edit');
if(!$GLOBALS['cfgMini'])
{
  if($havestrings)
  {
    $rightmenu->addItem('{%langstr:toolbar_edit_strings%}',"javascript:if(SelectedRowID&&SelectedRowID!=-1)openDialog('?module=strings&site_id=" . $GLOBALS['site_id'] . "&action=page_strings&id='+SelectedRowID, 400, 520, 1, 0);",false,false,'sitemap_strings');
  }
}
$rightmenu->addSpacer();

if(!$GLOBALS['cfgMini'] || $_SESSION['userdata']['username'] == 'Admin')
{
  $rightmenu->addItem('{%langstr:toolbar_permissions%}',"javascript:if(SelectedRowID&&SelectedRowID!=-1)openDialog('?module=permissions&action=permission_frame&mod=page_form&close=1&site_id=".$GLOBALS['site_id']."&data=".$GLOBALS['site_id']."&data2='+SelectedRowID+'&single_page=1', 800, 600, 0, 1);",false,false,'sitemap_permissions');
  $rightmenu->addItem('{%langstr:toolbar_copy_pages%}',"javascript:if(SelectedRowID&&SelectedRowID!=-1)openDialog('?module=pages&site_id=" . $GLOBALS['site_id'] . "&action=copytree&id='+SelectedRowID, 400, 300, 1, 1); else if(!SelectedRowIDsEmpty)openDialog('?module=pages&site_id=" . $GLOBALS['site_id'] . "&action=copytree2&id='+SelectedRowIDs, 400, 300, 1, 1);",false,false,'sitemap_copy');
  $rightmenu->addItem('{%langstr:toolbar_history%}',"javascript:if(SelectedRowID&&SelectedRowID!=-1)openDialog('?module=pages&site_id=" . $GLOBALS['site_id'] . "&action=history_form&id='+SelectedRowID, 400, 550, 1, 0);else alert('No page selected');",false,false,'sitemap_history');
  $rightmenu->addSpacer();
}

$rightmenu->addItem('{%langstr:toolbar_up%}','javascript:if(SelectedRowID)window.location=\'?module=pages&site_id='.$GLOBALS['site_id'].'&action=moveup&selrow=\'+SelectedRowID+\'&id=\'+SelectedRowID;',false,false,'up');
$rightmenu->addItem('{%langstr:toolbar_down%}',' javascript:if(SelectedRowID)window.location=\'?module=pages&site_id='.$GLOBALS['site_id'].'&action=movedown&selrow=\'+SelectedRowID+\'&id=\'+SelectedRowID;',false,false,'down');
$rightmenu->addItem('{%langstr:toolbar_delete%}','var agree = confirm(\'{%langstrjs:toolbar_ask_page_delete%}\'); if (agree){ javascript:if(SelectedRowID)window.location=\'?module=pages&site_id='.$GLOBALS['site_id'].'&action=delpage&id=\'+SelectedRowID;}',false,false,'delete');
$rightmenu->addSpacer();
$rightmenu->addItem('{%langstr:toolbar_help%}',$GLOBALS['cfgWebRoot'].'help/help.htm?category=lapas',true,true,'help');

echo $rightmenu->output();


$perm_managepages = $GLOBALS['perm_managepages'];

$nodes_path = array();

//function for sorting data in tree structure
function Nodes($data, &$newdata, $parent, $level, $justcount, &$goodl, $perms, $canpublish)
{
  global $nodes_path;

  if (is_array($nodes_path) && in_array($parent, $nodes_path)) return 0;
  $nodes_path[] = $parent;

  $goodlanguage = false;
  $nodecount = 0;
  foreach($data as $key => $row)
  {
    if($parent == $row['parent']){
      $nodecount++;

        $row['prefix'] = str_repeat('<img src="gui/images/1x1.gif" width="16" height="16" border="0" align="absmiddle">', $level) . '<img src="gui/images/1x1.gif" width="16" height="16" border="0" align="absmiddle">&nbsp;';
        $newdata[] = $row;
        $c = count($newdata) - 1;
        $childs = $row['childrencount'];
        Nodes($data, $newdata, $row['page_id'], $level + 1, 0, $g, $perms, $canpublish);
        $goodlanguage = ($g or $goodlanguage);
        if(!$_SESSION['expanded' . $row['page_id']])
        {
          $newdata = array_slice($newdata, 0, $c+1);
        }

        if($row['language'] != intval($GLOBALS['currentlanguagenum']))
        {
          //$newdata[$c]['page_id'] = 0;
          if(!$g)
          {
            if($c)
              $newdata = array_slice($newdata, 0, $c);
            else
              $newdata = array();
            $nodecount--;
          }else
          {
            $newdata[$c]['clicklink'] = "Page in " . sqlQueryValue("SELECT fullname FROM " . $GLOBALS['site_id'] . "_languages WHERE language_id = " . intval($row['language'])) . " (" . $row['name'] . ")";
            $newdata[$c]['page_id'] = 0;
            $newdata[$c]['pagedev_id'] = 0;
          }
        }else
        {
          if ($GLOBALS['perm_managepages'] || in_array($row['page_id'],$perms))
          {
            $newdata[$c]['clicklink'] = "<a href=\"#\" OnClick=\"javascript:openDialog('?module=pages&site_id=" . $GLOBALS['site_id'] . "&action=components_visual_frame&id=".$row['pagedev_id']."&page_id=".$row['page_id']."', screen.availWidth, screen.availHeight-25, 1 , 1);\">".ShortenString($row['title'],40)."</a>";

            //publish page if 'Can Publish All Pages' permission is set for current user
            if ($canpublish) $newdata[$c]['publish'] = '<a href="?module=pages&site_id='.$_GET['site_id'].'&action=publish&pagedev_id='.$row['pagedev_id'].'&page_id='.$row['page_id'].'">{%langstr:publish%}</a>';
            else $newdata[$c]['publish'] = '{%langstr:publish%}';
          }
          else
            $newdata[$c]['clicklink'] = ShortenString($row['title'],40);
          $goodlanguage = true;
        }
        if(($childs)and(isset($newdata[$c]))){
          if($_SESSION['expanded' . $row['page_id']])
          {
            $newdata[$c]['prefix'] = '&nbsp;'.str_repeat('<img src="gui/images/1x1.gif" width="16" height="16" border="0" style="vertical-align:middle">', $level) . '<a href="?module=pages&site_id=' . $GLOBALS['site_id'] . '&collapsebranch=' . $newdata[$c]['page_id'] . '"><img src="gui/images/minus.gif" width="11" height="11" border="0" align="absmiddle" style="vertical-align: middle;"></a>&nbsp; ';
          }else
          {
            $newdata[$c]['prefix'] = '&nbsp;'.str_repeat('<img src="gui/images/1x1.gif" width="16" height="16" border="0" style="vertical-align:middle">', $level) . '<a href="?module=pages&site_id=' . $GLOBALS['site_id'] . '&expandbranch=' . $newdata[$c]['page_id'] . '"><img src="gui/images/plus.gif" width="11" height="11" border="0" align="absmiddle" style="vertical-align: middle;"></a>&nbsp; ';
          }
        }

    }

  }
  $goodl = $goodlanguage;
  return $nodecount;
}


$site_id = $GLOBALS['site_id'];

// Let's try table generator

$cols = Array(

  "select"    => Array(
    "width"     => "1%",
    "title"     => "",
    "format"    => '<input type="radio" name="selectpage" value="{%pagedev_id%}" id="selectpage_{%pagedev_id%}" />'
  ),

  "preview"     => Array(
    "width"     => "5%",
    "align"     => "center",
    "title"     => "{%langstr:col_prev%}",
    "format"    => "<a href=\"?module=pages&site_id=$site_id&action=page_redirect&id={%page_id%}\"  target=\"_blank\"><img src=\"gui/images/ico_view.gif\" width=\"16\" height=\"16\" border=\"0\"></a>"
  ),

  "visible"       => Array(
    "width"     => "5%",
    "align"     => "center",
    "title"     => "{%langstr:col_vis%}"
  ),

  "enabled"     => Array(
    "width"     => "5%",
    "align"     => "center",
    "title"     => "{%langstr:col_enab%}"
  ),

  "title"        => Array(
    "width"     => "30%",
    "title"     => "{%langstr:title%}",
    "format"    => "{%prefix%}{%clicklink%}{%scut%}{%ishome%}{%islock%}"
  ),

  "description" => Array(
    "width"     => "40%",
    "title"     => "{%langstr:col_description%}"
  ),

  "modby" => Array(
    "width"     => "15%",
    "title"     => "{%langstr:modify_by%}"
  ),

  "publish"     => Array(
    "width"     => "5%",
    "align"     => "center",
    "title"     => "{%langstr:col_pub%}"
  ),

  "previewdev"     => Array(
    "width"     => "5%",
    "align"     => "center",
    "title"     => ""
  ),

);

if(CheckPermission(32, $site_id) && CheckPermission(33, $site_id)) //can publish and autopublish
{
  unset($cols['publish']);
  unset($cols['previewdev']);
}

if(!$perm_managepages)
{
  //unset($cols['title']['format']);
}


$javascriptv = ' onClick="javascript:SubmitPageVisibleCheckbox(this, \',pagedev_id,\',' . $site_id . ')"';
$javascripte = ' onClick="javascript:SubmitPageEnabledCheckbox(this, \',pagedev_id,\',' . $site_id . ')"';

if(!$perm_managepages)
{
  $javascriptv = '';
  $javascripte = '';
}
$checkstylev = 'CONCAT(\'<input type="checkbox" name="cccb"' . $javascriptv . ' checked>\')';
$uncheckstylev = 'CONCAT(\'<input type="checkbox" name="cccb"' . $javascriptv . '>\')';
$checkstylee = 'CONCAT(\'<input type="checkbox" name="cccb"' . $javascripte . ' checked>\')';
$uncheckstylee = 'CONCAT(\'<input type="checkbox" name="cccb"' . $javascripte . '>\')';

$shortcutimage = '<img src="gui/images/shortcut.gif" width="11" height="11" border="0" /> ';
$homeimage = '<img src="gui/images/home-11x11.gif" width="11" height="11" border="0" id="homeiconimg" /> ';
$lockimage = '<img src="gui/images/lock-11x11.gif" width="11" height="11" border="0" /> ';
$openedbr = '';
foreach($_SESSION as $key => $val)
{
  if(substr($key, 0, 8) == 'expanded')
  {
    $openedbr .= ',' . intval(substr($key, 8));
  }
}
$unsorteddata = sqlQueryData("SELECT in_trash, pagedev_id, page_id, IF(redirect!=0 or redirect_url!='', CONCAT('&nbsp;', '$shortcutimage'), '') AS scut, IF(name='index', CONCAT('&nbsp;', '$homeimage'), '') AS ishome, IF(passprotect>0, CONCAT('&nbsp;', '$lockimage'), '') AS islock, name, title, description, ELT(visible+1,$uncheckstylev,$checkstylev) AS visible, ELT(p.enabled+1,$uncheckstylee,$checkstylee) AS enabled, parent, p.language, lastmod, username as modby, childrencount FROM " . $site_id . "_pagesdev as p LEFT JOIN users as u ON modby = u.user_id WHERE parent IN (0".$openedbr.") ORDER BY ind");

//$unsorteddata = sqlQueryData("SELECT wappagedev_id, IF(redirect>0, CONCAT('$shortcutimage', '&nbsp;'), '') AS scut, name, IF(is_card>0,CONCAT('$cardimage', '&nbsp;'), '') as link, title, description, lastmod, ELT(visible+1,$uncheckstylev,$checkstylev) AS visible, ELT(w.enabled+1,$uncheckstylee,$checkstylee) AS enabled, parent, wappage_id, username as modby FROM " . $site_id . "_wappagesdev as w, users as u WHERE modby = u.user_id ORDER BY ind");

//get page_id array
$ids = sqlQueryData("select distinct page_id, ind from ".$site_id."_pagesdev ORDER BY ind");

//get keys of last modified dev pages
if (count($ids))
{
    $keys = array();
    for ($i=0; $i<=count($ids)-1; $i++)
    {
        $max = 0;
        foreach ($unsorteddata as $key=>$row)
        {
            //echo $row['page_id']." ".$ids[$i]['page_id']."<br>";
            if ($row['page_id']==$ids[$i]['page_id'])
            {
                if($row['lastmod']>$max)
                {
                    $max = $row['lastmod'];
                    $keys[$i] = $key;
                    $lastmoddev[$row['page_id']]['lastmod'] = $max;
                }
            }
        }
    }
}

//drop unnecessary rows
foreach ($unsorteddata as $key=>$row)
    if (!in_array($key,$keys))
        unset($unsorteddata[$key]);
$lastmod_data = sqlQueryData("SELECT page_id,lastmod FROM ".$site_id."_pages");
foreach ($lastmod_data as $row)
{
    $lastmod[$row['page_id']]['lastmod'] = $row['lastmod'];
}

$groups = sqlQueryDataAssoc("SELECT group_id from users_groups where user_id=".$GLOBALS['currentUserID']);
foreach ($groups as $group)
    $where .= " OR (perm_num=11 AND  isuser=0 AND id=".$group['group_id']." AND data=".$site_id." AND data2<>'')";

$perm_data = sqlQueryDataAssoc("SELECT data2 FROM permissions WHERE (perm_num=11 AND  isuser=1 AND id=".$GLOBALS['currentUserID']." AND data=".$site_id." AND data2<>'') ".$where);
//echo "SELECT data2 FROM permissions WHERE (perm_num=11 AND  isuser=1 AND id=".$GLOBALS['currentUserID']." AND data=".$site_id." AND data2<>'') ".$where;
if($perm_data && $perms)
{
    foreach ($perm_data as $row)
        if (!in_array($row['data2'],$perms))
            $perms[] = $row['data2'];
}
//print_r($perms);

$g = false;
Nodes($unsorteddata, $data, 0, 0, 0, $g, $perms, CheckPermission(33, $site_id));

//check which pages to publish
foreach ($data as $key=>$row)
{
    if ($row['lastmod']==$lastmod[$row['page_id']]['lastmod'])
    {
        unset($data[$key]['publish']);
    }
    else
    {
        $data[$key]['previewdev'] = "<a href=\"?module=pages&site_id=$site_id&action=page_redirect&id=".$row['page_id']."&pagedev_id=".$row['pagedev_id']."&showdevpage=1\"  target=\"_blank\"><img src=\"gui/images/ico_view.gif\" width=\"16\" height=\"16\" border=\"0\"></a>";
    }
}

foreach ($data as $key => $row)
{
  if ($row['in_trash'])
    unset($data[$key]);

}

require($GLOBALS['cfgDirRoot']."library/"."class.datagrid.php");

$Table = new DataGrid();
$Table->cols = $cols;
$Table->data = $data;
$Table->footer = false;
$Table->rowformat = " id=\"tablerow{%pagedev_id%}\" onClick=\"javascript:HighLightTR(this, {%pagedev_id%},1);
                         if(SelectedRowID){ document.getElementById('selectpage_{%pagedev_id%}').checked = true; }else {  }
                         \" oncontextmenu=\"javascript:HighLightTR(this, {%pagedev_id%},1);
                         if(SelectedRowID){ document.getElementById('selectpage_{%pagedev_id%}').checked = true; }else {  }
                          showMenu(this, event); return false;\" ";
echo $Table->output();

if($_GET['accessdenied'])
{
  AccessDenied(false, true);
  echo '
  <script language="JavaScript" type="text/javascript">
  /*<![CDATA[*/
  window.document.location.href = "?module=pages&site_id=' . $GLOBALS['site_id'] . '&selrow=' . $_GET['selrow'] . '"
  /*]]>*/
  </script>
  ';

}

echo '<script language="JavaScript" type="text/javascript">
/*<![CDATA[*/
che = new Array("add2", "properties", "perms", "sedit", "visedit", "strings", "up", "down", "del", "copy", "history");
che2 = new Array("add","add2", "properties", "perms", "sedit", "visedit", "strings", "up", "down", "history");

DisableToolbarButtons(che);
/*]]>*/
</script>';

if($_GET['selrow'])
{
  echo '<script language="JavaScript">
        <!--
          var tablerow' . $_GET['selrow'] . ' = document.getElementById("tablerow' . $_GET['selrow'] . '");
          if(typeof(tablerow' . $_GET['selrow'] . ')!= "undefined" && tablerow' . $_GET['selrow'] . ')
          {
            preEl = tablerow' . $_GET['selrow'] . ';
            ChangeTextColor(tablerow' . $_GET['selrow'] . ',\'tableRowSel\');
            SelectedRowID = ' . $_GET['selrow'] . ';
            if(SelectedRowID){ document.getElementById(\'selectpage_' . $_GET['selrow'] . '\').checked = true; EnableToolbarButtons(che); }else { DisableToolbarButtons(che); }
          }
        <!---->
        </script>';
}


if($GLOBALS['cfgMiniDemo'] && !$_SESSION['hasSeenSitemapArrowHints'])
{

  $_SESSION['hasSeenSitemapArrowHints'] = true;

?>

<table id="floaterdiv" style="display: none; background: #FFFFFF; width: 260px; border: 1px solid black; border-collapse: collapse;">
<tr>
  <td rowspan="2" style="padding: 5px;"><img src="<?= $GLOBALS['cfgWebRoot']; ?>gui/images/bigarrowleft.png" /></td>
  <td style="text-align: right; padding: 0;"><a href="javascript:close_float_div('');" style="color: black; text-decoration: none;">&nbsp;x&nbsp;</a></td>
</tr>
<tr>
  <td valign="top" style="padding-left: 5px; padding-right: 5px; padding-bottom: 5px; padding-top: 0px; font-size: 130%;">
  {%langstr:demohint_click_title%}
  </td>
</tr>
</table>

<table id="floaterdiv2" style="display: none; background: #FFFFFF; width: 140px; border: 1px solid black; border-collapse: collapse;">
<tr>
  <td style="text-align: right; padding: 0;"><a href="javascript:close_float_div('2');" style="color: black; text-decoration: none;">&nbsp;x&nbsp;</a></td>
</tr>
<tr>
  <td style="padding: 5px; text-align: center;"><img src="<?= $GLOBALS['cfgWebRoot']; ?>gui/images/bigarrowup.png" /></td>
</tr>
<tr>
  <td style="padding-left: 5px; padding-right: 5px; padding-bottom: 5px; padding-top: 0px; font-size: 130%; text-align: center;">
  {%langstr:demohint_video%}
  </td>
</tr>
</table>

<script>

function close_float_div(a)
{
  var el = Ext.get("floaterdiv" + a);
  Ext.getDom(el).style.display = "none";
  if(a = "2")
  {
    var el2 = Ext.get("floaterdiv2");
    el2.alignTo("video_ol", "t-b", [0, 5]);
  }
}

function float_div()
{
  try {
    var el = Ext.get("floaterdiv");
    document.body.appendChild(Ext.getDom(el));
    el.setOpacity(0);
    Ext.getDom(el).style.display = "block";
    el.alignTo("homeiconimg", "l-r", [5, 0]);
    el.animate(
      // animation control object
      {
          opacity: {to: 1, from: 0}
      },
      0.65,      // animation duration
      null,      // callback
      'easeOut', // easing method
      'run'      // animation type ('run','color','motion','scroll')

    );

    var el2 = Ext.get("floaterdiv2");
    document.body.appendChild(Ext.getDom(el2));
    el2.setOpacity(0);
    Ext.getDom(el2).style.display = "block";
    el2.alignTo("video_ol", "t-b", [0, 5]);
    el2.animate(
      // animation control object
      {
          opacity: {to: 1, from: 0}
      },
      0.65,      // animation duration
      null,      // callback
      'easeOut', // easing method
      'run'      // animation type ('run','color','motion','scroll')

    );


    //blink home link
    window.fadeCount = 0;
    fadeUrlOut();
  } catch(e) {}
}

function fadeUrlOut()
{
  window.fadeCount++;
  if(window.fadeCount > 4)
    return false;

  var el = Ext.get("homeiconimg").prev('a');

  el.animate(
    {
        backgroundColor: { to: '#FFFFFF', from: '#FF9966' }
    },
    0.65,      // animation duration
    fadeUrlOut,      // callback
    'easeBoth', // easing method
    'color'    // animation type ('run','color','motion','scroll')
  );
}

window.onload = float_div;

</script>

<?
}
?>