<?

// checkbox supports
if ($_POST)
{
  // atlasam komponentus, par kuriem ir info ieksh POST
  $cmp = Array();
  foreach(array_keys($_POST) as $key)
  {
    $s = split('_', $key, 2);
    if($s[0] == 'type')
    {
      $cmp[$s[1]] = $_POST[$key];
    }
  }

  // uzstadam tuksho checkbokshu vertibu kaa nulli
  foreach ($cmp as $name => $type)
  {
    $props = getPropertiesTable($type);
    foreach ($props as $key => $p)
    {
      if ($p['type'] == 'check')
      {
        $k = 'component_'.$name.'_'.$key;
        if (!isset($_POST[$k]))
          $_POST[$k] = '0';

      }
    }
  }
}

  require_once ($GLOBALS['cfgDirRoot']."library/".'func.search.php');

  $site_id = $GLOBALS['site_id'];

  $perm_managewappages = $GLOBALS['perm_managewappages'];
  $perm_managecontents = $GLOBALS['perm_managecontents'];

  if(((($action == 'addpage')and($perm_managewappages)))or((($action == 'modpage')and($perm_managewappages))))
  {
    $name = $_POST['name'];
    $oldname = $_POST['oldname'];
    $title = $_POST['title'];
    $description = $_POST['description'];
    $template = $_POST['template'];
    //$language = $_POST['language'];
    $lastmod = time();
    $modby = $GLOBALS['currentUserID'];
    if($_POST['enabled']){
      $enabled = 1;
    }else{
      $enabled = 0;
    }
    if($_POST['visible']){
      $visible = 1;
    }else{
      $visible = 0;
    }
    if($_POST['iscard']){
      $is_card = 1;
    }else{
      $is_card = 0;
    }
    if($_POST['isroot']){
      $is_root = 1;
    }else{
      $is_root = 0;
    }
    /*
    if($_POST['passprotect']){
      $passprotect = 1;
    }else{
      $passprotect = 0;
    }
    */
    $parent = $_POST['parent'];
    $wappagedev_id = $_POST['wappagedev_id'];
    $redirect = $_POST['redirect'];
    $copypage = $_POST['copypage'];

    //Check if field input is valid
    $invalid = Array();
    $jsmessage = '';
    if ($wappagedev_id) $wappage_id = sqlQueryValue("select wappage_id from ".$site_id."_wappagesdev where wappagedev_id=".$wappagedev_id);

    if(/*(IsEmpty($name))or*/(IsBadSymbols($name))or(sqlQueryValue("SELECT wappage_id FROM " . $site_id . "_wappages WHERE name='$name' AND wappage_id<>'$wappage_id' AND parent='$parent'")))
    {
      $invalid[] = "name";
      if(!IsEmpty($name))
        $jsmessage .= 'Page name' . msgValidCharsUnique . '\n';
    }
    if (($oldname=="404")and($name!=$oldname))
    {
      $invalid[] = "name";
      $jsmessage .= 'Page 404 can not be renamed\n';
    }
    if(IsLtGtSymbols($title))
    {
      $invalid[] = "title";
      $jsmessage .= 'Title ' . msgNoLtGt . '\n';
    }

    if(IsLtGtSymbols($description))
    {
      $invalid[] = "description";
      $jsmessage .= 'Description ' . msgNoLtGt . '\n';
    }
  //  if(IsBadSymbols($title, " "))
  //  {
  //    $invalid[] = "title";
  //  }
    if(count($invalid))
    {
      if($action == 'addpage')
        $action = 'create';
      if($action == 'modpage')
        $action = 'properties_form';
      $invalid = join(':', $invalid);
      $name = urlencode(stripslashes($name));
      $title = urlencode(stripslashes($title));
      $description = urlencode(stripslashes($description));
      $jsmessage = urlencode($jsmessage);

      Redirect('?module='.$module.'&action='.$action.'&invalid=' . $invalid . '&formdata_name='.$name . '&formdata_title='.$title.'&formdata_description='.$description . '&formdata_template='.$template . '&formdata_enabled='.$enabled . '&formdata_visible='.$visible . '&formdata_parent='.$parent . '&formdata_enabled='.$enabled . '&formdata_isroot='.$is_root .'&formdata_iscard='.$is_card . '&formdata_copypage='.$copypage.'&jsmessage='.$jsmessage.'&id='.$wappagedev_id.'&site_id='.$site_id);
      Die();
    }

  }

  $copypage = 0;
  if($template < 0)
  {
    $copypage = sqlQueryValue("SELECT -view_id FROM " . $site_id . "_views WHERE view_id=" . abs($template));
    if($copypage)
    {
      $template = sqlQueryValue("SELECT template_id FROM " . $site_id . "_views WHERE view_id=".abs($copypage));
    }
  }

  if(IsEmpty($name))
  {
    //auto generate name from title
    $n = $title;
    $n = ereg_replace('[^a-zA-Z0-9]', '', $n);
    if(!$n) $n = 'index';
    $n = strtolower(substr($n, 0, 12));
    $i = 0;
    $name = $n;
    while(sqlQueryValue("SELECT wappage_id FROM " . $site_id . "_wappages WHERE name='$name' AND wappage_id<>'$wappage_id' AND parent='$parent'"))
    {
      $i++;
      $name = $n . $i;
    }

  }


  if(($action == 'addpage')and($perm_managewappages))
  {
    $created = time();
    if($_POST['sibiling'])
    {
      $sibiling = $_POST['sibiling'];
    }
    else
    {
      $sibiling = intval(sqlQueryValue("SELECT max(ind) FROM " . $site_id . "_wappages WHERE parent=$parent"));
    }


    sqlQuery("UPDATE " . $site_id . "_wappages SET ind=ind+1 WHERE ind>$sibiling AND parent=$parent");
    sqlQuery("INSERT INTO " . $site_id . "_wappages (name,title,description,template,lastmod,created,modby,enabled,visible,parent,ind,redirect,copypage,is_card,is_root) VALUES ('$name','$title','$description','$template','$lastmod','$created','$modby','$enabled','$visible',$parent,$sibiling+1,$redirect,$copypage,$is_card,$is_root)");

    $wappage_id = sqlQueryValue("SELECT wappage_id FROM " . $site_id . "_wappages WHERE name='$name' AND parent='$parent'");

    //wappage dev copy
    sqlQuery("UPDATE " . $site_id . "_wappagesdev SET ind=ind+1 WHERE ind>$sibiling AND parent=$parent");
    sqlQuery("INSERT INTO " . $site_id . "_wappagesdev (name,title,description,template,lastmod,created,modby,enabled,visible,parent,ind,redirect,copypage,wappage_id,is_card,is_root) VALUES ('$name','$title','$description','$template','$lastmod','$created','$modby','$enabled','$visible',$parent,$sibiling+1,$redirect,$copypage,$wappage_id,$is_card,$is_root)");

    $lid = sqlQueryValue("select wappagedev_id from " . $site_id . "_wappagesdev  where created = '".$created."' AND wappage_id=".$wappage_id);

    $_SESSION['wapexpanded' . $parent] = true;
    $docStrings['urlend'] = "&selrow=" . $page_id;
    
    //Add access to the newly created page for Everyone
    AddGroupPermission(1, 20, $site_id, $lid);
    //Add access to delete the newly created page for Everyone
    AddGroupPermission(1, 21, $site_id, $lid);
    //Add access to the newly created page for Administrators
    AddGroupPermission(2, 20, $site_id, $lid);
    //Add access to delete the newly created page for Administrators
    AddGroupPermission(2, 21, $site_id, $lid);
    Add_Log("New WAP page '$name' ($wappage_id)", $site_id);

    IndexPageForSearch($site_id, $page_id);
    DeleteAllCompiledWapTemplates($site_id);

  }else if(($action == 'modpage')and($perm_managewappages))
  {
    //Check out if it's changing parent and act
    $curpage = sqlQueryRow("SELECT wappage_id, parent,ind,name FROM " . $site_id . "_wappagesdev WHERE wappagedev_id=$wappagedev_id");
    if($parent <> $curpage['parent'])
    {

      sqlQuery("UPDATE " . $site_id . "_wappages SET ind=ind-1 WHERE ind>" . $curpage['ind'] . " AND parent=" . $curpage['parent']);
      sqlQuery("UPDATE " . $site_id . "_wappages SET ind=ind+1 WHERE parent=$parent");
      sqlQuery("UPDATE " . $site_id . "_wappages SET ind=1 WHERE wappage_id=".$curpage['wappage_id']);
      sqlQuery("UPDATE " . $site_id . "_wappages SET parent='$parent' WHERE wappage_id=".$curpage['wappage_id']);

      //same for wappagesdev table
      sqlQuery("UPDATE " . $site_id . "_wappagesdev SET ind=ind-1 WHERE ind>" . $curpage['ind'] . " AND parent=" . $curpage['parent']);
      sqlQuery("UPDATE " . $site_id . "_wappagesdev SET ind=ind+1 WHERE parent=$parent");
      sqlQuery("UPDATE " . $site_id . "_wappagesdev SET ind=1 WHERE wappage_id=".$curpage['wappage_id']);
      sqlQuery("UPDATE " . $site_id . "_wappagesdev SET parent='$parent' WHERE wappage_id=".$curpage['wappage_id']);
    }
    if ($name <> $curpage['name'])
    {
        sqlQuery("UPDATE " . $site_id . "_wappages SET name='$name' WHERE wappage_id=".$curpage['wappage_id']);
    }

    //other stuff update only for wappagedev
    sqlQuery("UPDATE " . $site_id . "_wappagesdev SET name='$name', title='$title', description='$description', template='$template', lastmod='$lastmod', modby='$modby', enabled='$enabled', visible='$visible', redirect='$redirect', copypage='$copypage', is_card='$is_card', is_root='$is_root' WHERE wappagedev_id='$wappagedev_id'");
    //Add_Log("Updated page '$name' (" . $curpage['page_id'] . ")", $site_id);

    //IndexPageForSearch($site_id, $page_id);
    DeleteAllCompiledWapTemplates($site_id);

    if (CheckPermission(27, $site_id, $wappagedev_id) || CheckPermission(32, $site_id))
    {
        autopublish($site_id, $wappagedev_id);
    }

  }else if(($action == 'delpage')and($perm_managewappages))
  {
    function DeletePageAndChildren($site_id, $wappage_id)
    {
      $data = sqlQueryData("SELECT wappage_id FROM " . $site_id . "_wappages WHERE parent=$wappage_id");
      $wappagedev_id_array = sqlQueryData("select wappagedev_id from ".$site_id."_wappagesdev where wappage_id=".$wappage_id);
      foreach($data as $row)
      {
        DeletePageAndChildren($site_id, $row['wappage_id']);
      }
      $row = sqlQueryRow("SELECT parent,ind FROM " . $site_id . "_wappages WHERE wappage_id=$wappage_id");
      sqlQuery("DELETE FROM " . $site_id . "_wappages WHERE wappage_id=$wappage_id");
      sqlQuery("DELETE FROM " . $site_id . "_wappagesdev WHERE wappage_id=$wappage_id");
      sqlQuery("UPDATE " . $site_id . "_wappages SET ind=ind-1 WHERE parent=" . $row['parent'] . " AND ind>" . $row['ind']);
      sqlQuery("UPDATE " . $site_id . "_wappagesdev SET ind=ind-1 WHERE parent=" . $row['parent'] . " AND ind>" . $row['ind']);
      
      //delete permissions from all related dev pages
      foreach ($wappagedev_id_array as $row)
      {
          sqlQuery("DELETE FROM permissions WHERE perm_num=20 AND data=$site_id AND data2='".$row['wappagedev_id']."'");
          sqlQuery("DELETE FROM permissions WHERE perm_num=21 AND data=$site_id AND data2='".$row['wappagedev_id']."'");
      }
      //delete compiled templates
      sqlQuery("DELETE FROM " . $site_id . "_wapctemplates WHERE wappage_id=" . $wappage_id);
      //delete search objects and stuff
      //IndexPageForSearch($site_id, $page_id);
      DeleteAllCompiledWapTemplates($site_id);

    }
    $wappagedev_id = $_GET['id'];
    $name = sqlQueryValue("SELECT name FROM " . $site_id . "_wappagesdev WHERE wappagedev_id=$wappagedev_id");
    $wappage_id = sqlQueryValue("SELECT wappage_id FROM " . $site_id . "_wappagesdev WHERE wappagedev_id=$wappagedev_id");
    
    $perm_delpage = CheckPermission(21, $site_id, $wappagedev_id);
    if($perm_delpage)
    {
      DeletePageAndChildren($site_id, $wappage_id);
      Add_Log("Deleted WAP page '$name' ($wappage_id)", $site_id);
      Header("Location: ?module=".$_GET['module']."&site_id=" . $GLOBALS['site_id'] . '&selrow=' . $_GET['selrow']);
    }else
    {
      Add_Log("Permission denied to deleted page '$name' ($wappage_id)", $site_id); 
      Header("Location: ?module=".$_GET['module']."&site_id=" . $GLOBALS['site_id'] . "&selrow=" . $wappagedev_id . "&accessdenied=1");
    }

  }else if(($action == 'tbarupdatecontrols')and($perm_managecontents))
  {
    $page_id = $_GET['page_id'];
    $updated = false;
    $copypage = sqlQueryValue("SELECT copypage FROM " . $site_id . "_pages WHERE page_id=$page_id");

    //print_r($_POST['component']);
    //echo "<hr>";
    //print_r($_POST);
    foreach ($_POST['component'] as $key=>$component)
      {
        //noskaidro bijusho ID veertiibai
        $oldcontID = sqlQueryValue("SELECT content_id FROM " . $site_id . "_contents WHERE page=$page_id AND componentname='" . $component . "' AND propertyname='visible'");
        $contID = $oldcontID;
        //izdzeesh visas vecaas veertiibas
        sqlQuery("DELETE FROM " . $site_id . "_contents WHERE page=$page_id AND componentname='" . $component . "' AND propertyname='visible'");

        //echo "component = " . $component . "<br>";
        if ($_POST[$component])
          {
            //echo "on ".$component."<br>";

            //ieliek jauno veertiibu un noskaidro ID
            sqlQuery("INSERT " . $site_id . "_contents SET page=$page_id, componentname='" . $component . "', propertyname='visible', propertyvalue='0'");
            $updated = true;
            $contID = sqlQueryValue("SELECT content_id FROM " . $site_id . "_contents WHERE page=$page_id AND componentname='" . $component . "' AND propertyname='visible'");
          }
        else
          {
            //echo "off ".$component."<br>";

            //ieliek jauno veertiibu un noskaidro ID
            sqlQuery("INSERT " . $site_id . "_contents SET page=$page_id, componentname='" . $component . "', propertyname='visible', propertyvalue='1'");
            $updated = true;
            $contID = sqlQueryValue("SELECT content_id FROM " . $site_id . "_contents WHERE page=$page_id AND componentname='" . $component . "' AND propertyname='visible'");
          }
      }

    /* ----- OLD STUFF -----
    foreach(array_keys($_POST) as $key)
    {
      //print_r($_POST); echo "<br>";
      $s = split('_', $key, 3);
      if($s[0] == 'component')      {
        //noskaidro bijusho ID veertiibai
        $oldcontID = sqlQueryValue("SELECT content_id FROM " . $site_id . "_contents WHERE page=$page_id AND componentname='" . $s[1] . "' AND propertyname='" . $s[2] . "'");
        $contID = $oldcontID;
        //izdzeesh visas vecaas veertiibas
        sqlQuery("DELETE FROM " . $site_id . "_contents WHERE page=$page_id AND componentname='" . $s[1] . "' AND propertyname='" . $s[2] . "'");
        //ja NAV iekjekseets, ka jaanjem defaultaas veertiibas
        if(!$_POST['check_component_' . $s[1] . '_' . $s[2]])
        {
          //ieliek jauno veertiibu un noskaidro ID
          sqlQuery("INSERT " . $site_id . "_contents SET page=$page_id, componentname='" . $s[1] . "', propertyname='" . $s[2] . "', propertyvalue='" . $_POST[$key] . "'");
          $updated = true;
          $contID = sqlQueryValue("SELECT content_id FROM " . $site_id . "_contents WHERE page=$page_id AND componentname='" . $s[1] . "' AND propertyname='" . $s[2] . "'");
        }

      }
    }
    */
             
    $name = sqlQueryValue("SELECT name FROM " . $site_id . "_pages WHERE page_id=$page_id");
    if($updated)
    {
      Add_Log("Updated contents of page '$name' ($page_id)", $site_id);
    }

    //Stop the window from closing
    if($_GET['noclose'])
    {

      $docTemplate = "form.htm";
      $docScripts['body'] = "pages.component.php";

    }

    IndexPageForSearch($site_id, $page_id);
    DeleteAllCompiledWapTemplates($site_id);

    if ($_GET['redirect'])
     Redirect("?module=wap&site_id=".$site_id."&action=components_form_frame&id=".$page_id);
  

  }else if(($action == 'updatecontrols')and($perm_managecontents))
  {
    $wappagedev_id = $_GET['wappagedev_id'];
    $updated = false;
    $copypage = sqlQueryValue("SELECT copypage FROM " . $site_id . "_wappagesdev WHERE wappagedev_id=$wappagedev_id");
    //print_r($_POST);
    foreach(array_keys($_POST) as $key)
    {
      $s = split('_', $key, 3);
      if($s[0] == 'component')      {
        //noskaidro bijusho ID veertiibai
        $oldcontID = sqlQueryValue("SELECT wapcontentdev_id FROM " . $site_id . "_wapcontentsdev WHERE wappagedev=$wappagedev_id AND componentname='" . $s[1] . "' AND propertyname='" . $s[2] . "'");
        $contID = $oldcontID;
        //izdzeesh visas vecaas veertiibas
        sqlQuery("DELETE FROM " . $site_id . "_wapcontentsdev WHERE wappagedev=$wappagedev_id AND componentname='" . $s[1] . "' AND propertyname='" . $s[2] . "'");
        
        //ja lapai nav neviena komponenta
        if ($_POST['stdcontents'])
        {
            //ieliek jauno veertiibu un noskaidro ID
            sqlQuery("INSERT " . $site_id . "_wapcontentsdev SET wappagedev=$wappagedev_id, componentname='" . $s[1] . "', propertyname='" . $s[2] . "', propertyvalue='" . $_POST[$key] . "'");
            $updated = true;
            $contID = sqlQueryValue("SELECT wapcontentdev_id FROM " . $site_id . "_wapcontentsdev WHERE wappagedev=$wappagedev_id AND componentname='" . $s[1] . "' AND propertyname='" . $s[2] . "'");
        }
        //ja NAV iekjekseets, ka jaanjem defaultaas veertiibas
        else //if(!$_POST['check_component_' . $s[1] . '_' . $s[2]])
        {
          //ieliek jauno veertiibu un noskaidro ID
          sqlQuery("INSERT " . $site_id . "_wapcontentsdev SET wappagedev=$wappagedev_id, componentname='" . $s[1] . "', propertyname='" . $s[2] . "', propertyvalue='" . $_POST[$key] . "'");
          $updated = true;
          $contID = sqlQueryValue("SELECT wapcontentdev_id FROM " . $site_id . "_wapcontentsdev WHERE wappagedev=$wappagedev_id AND componentname='" . $s[1] . "' AND propertyname='" . $s[2] . "'");
        }

      }
    }
    $name = sqlQueryValue("SELECT name FROM " . $site_id . "_wappagesdev WHERE wappagedev_id=$wappagedev_id");
    if($updated)
    {
      //update modification date
      sqlQuery("update ".$site_id."_wappagesdev set lastmod='".time()."' where wappagedev_id=$wappagedev_id");

      if (CheckPermission(27, $site_id, $wappagedev_id) || CheckPermission(32, $site_id))
      {
          autopublish($site_id, $wappagedev_id);
      }

      Add_Log("Updated contents of WAP page '$name' ($wappagedev_id)", $site_id);
    }

    //Stop the window from closing
    
    if($_GET['noclose'])
    {

      $docTemplate = "form.htm";
      $docScripts['body'] = "wap.component.php";

    }
    

    //IndexPageForSearch($site_id, $page_id);
    DeleteAllCompiledWapTemplates($site_id);

    if ($_GET['redirect'])
    {        Redirect("?module=wap&site_id=".$site_id."&action=components_form_frame&id=".$wappagedev_id."&wappagedev_id".$wappagedev_id);
    }



  }else if(($action == 'updatecontrolsdef')and($perm_managecontents))
  {
    $page_id = -1;
    $updated = false;
    foreach(array_keys($_POST) as $key)
    {
      $s = split('_', $key, 3);
      if($s[0] == 'component')      {
        //noskaidro bijusho ID veertiibai
        $oldcontID = sqlQueryValue("SELECT content_id FROM " . $site_id . "_wapcontents WHERE wappage=-1 AND componentname='" . $s[1] . "' AND propertyname='" . $s[2] . "'");
        $contID = $oldcontID;
        //izdzeesh visas vecaas veertiibas
        sqlQuery("DELETE FROM " . $site_id . "_wapcontents WHERE wappage=-1 AND componentname='" . $s[1] . "' AND propertyname='" . $s[2] . "'");
        //ja NAV iekjekseets, ka jaanjem defaultaas veertiibas

        //ieliek jauno veertiibu un noskaidro ID
        sqlQuery("INSERT " . $site_id . "_wapcontents SET wappage=-1, componentname='" . $s[1] . "', propertyname='" . $s[2] . "', propertyvalue='" . $_POST[$key] . "'");
        $updated = $s[1];
        $contID = sqlQueryValue("SELECT content_id FROM " . $site_id . "_wapcontents WHERE wappage=-1 AND componentname='" . $s[1] . "' AND propertyname='" . $s[2] . "'");

      }
    }
    if($updated)
    {
      Add_Log("Updated default values of component '$updated'", $site_id);
    }

    //Stop the window from closing
    if($_GET['noclose'])
    {

      $docTemplate = "form.htm";
      $docScripts['body'] = "pages.component.php";

    }

    DeleteAllCompiledWapTemplates($site_id);

  }else if(($action == 'setpageenabled')and($perm_managewappages))
  {
    $wappagedev_id = $_GET['id'];
    sqlQuery("UPDATE " . $site_id . "_wappagesdev SET enabled='1' WHERE wappagedev_id='$wappagedev_id'");
    DeleteAllCompiledWapTemplates($site_id);
    if (CheckPermission(27, $site_id, $wappagedev_id) || CheckPermission(32, $site_id))
    {
        autopublish($site_id, $wappagedev_id);
    }

  }else if(($action == 'unsetpageenabled')and($perm_managewappages))
  {
    $wappagedev_id = $_GET['id'];
    sqlQuery("UPDATE " . $site_id . "_wappagesdev SET enabled='0' WHERE wappagedev_id='$wappagedev_id'");
    DeleteAllCompiledWapTemplates($site_id);
    if (CheckPermission(27, $site_id, $wappagedev_id) || CheckPermission(32, $site_id))
    {
        autopublish($site_id, $wappagedev_id);
    }

  }else if(($action == 'setpagevisible')and($perm_managewappages))
  {
    $wappagedev_id = $_GET['id'];
    sqlQuery("UPDATE " . $site_id . "_wappagesdev SET visible='1' WHERE wappagedev_id='$wappagedev_id'");
    DeleteAllCompiledWapTemplates($site_id);
    if (CheckPermission(27, $site_id, $wappagedev_id) || CheckPermission(32, $site_id))
    {
        autopublish($site_id, $wappagedev_id);
    }

  }else if(($action == 'unsetpagevisible')and($perm_managewappages))
  {
    $wappagedev_id = $_GET['id'];
    sqlQuery("UPDATE " . $site_id . "_wappagesdev SET visible='0' WHERE wappagedev_id='$wappagedev_id'");
    DeleteAllCompiledWapTemplates($site_id);
    if (CheckPermission(27, $site_id, $wappagedev_id) || CheckPermission(32, $site_id))
    {
        autopublish($site_id, $wappagedev_id);
    }

  }else if(($action == 'moveup')and($perm_managewappages))
  {
    $wappagedev_id = $_GET['id'];
    $wappage_id = sqlQueryValue("select wappage_id from ".$site_id."_wappagesdev where wappagedev_id=".$wappagedev_id);
    $row = sqlQueryRow("SELECT parent,ind FROM " . $site_id . "_wappages WHERE wappage_id=$wappage_id");
    if($row != null)
    {
      //Obtain the page_id above
      $aboverow = sqlQueryRow("SELECT ind, wappage_id FROM " . $site_id . "_wappages WHERE parent=" . $row['parent'] . " AND ind<" . $row['ind'] . " ORDER BY ind DESC LIMIT 1");
      if($aboverow != null)
      {
        sqlQueryValue("UPDATE " . $site_id . "_wappages SET ind=" . $aboverow['ind'] . " WHERE wappage_id=$wappage_id");
        sqlQueryValue("UPDATE " . $site_id . "_wappages SET ind=" . $row['ind'] . " WHERE wappage_id=" . $aboverow['wappage_id']);

        //same for wappagesdev
        sqlQueryValue("UPDATE " . $site_id . "_wappagesdev SET ind=" . $aboverow['ind'] . " WHERE wappage_id=$wappage_id");
        sqlQueryValue("UPDATE " . $site_id . "_wappagesdev SET ind=" . $row['ind'] . " WHERE wappage_id=" . $aboverow['wappage_id']);
//        Header("Location: ?module=pages&site_id=" . $site_id . "&selrow=" . $page_id);
      }
    }
    DeleteAllCompiledWapTemplates($site_id);
  }else if(($action == 'movedown')and($perm_managewappages))
  {
    $wappagedev_id = $_GET['id'];
    $wappage_id = sqlQueryValue("select wappage_id from ".$site_id."_wappagesdev where wappagedev_id=".$wappagedev_id);
    $row = sqlQueryRow("SELECT parent, ind FROM " . $site_id . "_wappages WHERE wappage_id=$wappage_id");
    if($row != null)
    {
      //Obtain the page_id below
      $belowrow = sqlQueryRow("SELECT ind, wappage_id FROM " . $site_id . "_wappages WHERE parent=" . $row['parent'] . " AND ind>" . $row['ind'] . " ORDER BY ind LIMIT 1");
      if($belowrow != null)
      {
        sqlQueryValue("UPDATE " . $site_id . "_wappages SET ind=" . $belowrow['ind'] . " WHERE wappage_id=$wappage_id");
        sqlQueryValue("UPDATE " . $site_id . "_wappages SET ind=" . $row['ind'] . " WHERE wappage_id=" . $belowrow['wappage_id']);
        //same for wappagesdev
        sqlQueryValue("UPDATE " . $site_id . "_wappagesdev SET ind=" . $belowrow['ind'] . " WHERE wappage_id=$wappage_id");
        sqlQueryValue("UPDATE " . $site_id . "_wappagesdev SET ind=" . $row['ind'] . " WHERE wappage_id=" . $belowrow['wappage_id']);
//        Header("Location: ?module=pages&site_id=" . $site_id . "&selrow=" . $page_id);
      }
    }
    DeleteAllCompiledWapTemplates($site_id);
  }else if(($action == 'publish')and($perm_managewappages))
  {
        //if (CheckPermission(27, $site_id, $wappagedev_id))
        //{
        //    autopublish($site_id, $wappagedev_id);
        //}

        //current dev page data
        $row = sqlQueryRow("select * from ".$site_id."_wappagesdev where wappagedev_id='".$_GET['wappagedev_id']."'");

        //update normal page using dev page data
        sqlQuery("UPDATE " . $site_id . "_wappages SET name='".$row['name']."', title='".$row['title']."', description='".$row['description']."', template='".$row['template']."', lastmod='".$row['lastmod']."', modby='".$row['modby']."', enabled='".$row['enabled']."', visible='".$row['visible']."', redirect='".$row['redirect']."', copypage='".$row['copypage']."', is_card='".$row['is_card']."', is_root='".$row['is_root']."' WHERE wappage_id='".$row['wappage_id']."'");

        //delete old
        sqlQuery("DELETE FROM ".$site_id."_wapcontents WHERE wappage=".$row['wappage_id']);

        //copy contents from wapcontentsdev to wapcontents
        if ($row['template']==0)
        {
            //get page contents if no template
            $row_contents = sqlQueryRow("select * from ".$site_id."_wapcontentsdev where wappagedev=".$_GET['wappagedev_id']." AND componentname='stdcontents'");

            /*if (sqlQueryValue("select * from ".$site_id."_wapcontents where wappage=".$row['wappage_id']))
            {
                sqlQuery("update ".$site_id."_wapcontents SET propertyvalue='".addslashes($row_contents['propertyvalue'])."',propertyname='".$row_contents['propertyname']."',componentname='".$row_contents['componentname']."' where wappage=".$row['wappage_id']);
            }
            else
            {*/
                sqlQuery("insert into ".$site_id."_wapcontents (wappage,componentname,propertyname,propertyvalue) values (".$row['wappage_id'].",'".$row_contents['componentname']."','".$row_contents['propertyname']."','".addslashes($row_contents['propertyvalue'])."')");
            //}
        }
        else
        {
            $data_contents = sqlQueryData("select * from ".$site_id."_wapcontentsdev where wappagedev=".$_GET['wappagedev_id']);
            foreach ($data_contents as $row_contents)
            {
                /*if (sqlQueryValue("select * from ".$site_id."_wapcontents where wappage=".$row['wappage_id']." AND componentname='".$row_contents['componentname']."' AND propertyname='".$row_contents['propertyname']."'"))
                {
                    sqlQuery("update ".$site_id."_wapcontents SET propertyvalue='".addslashes($row_contents['propertyvalue'])."' WHERE wappage=".$row['wappage_id']." AND componentname='".$row_contents['componentname']."' AND propertyname='".$row_contents['propertyname']."'");
                }
                else
                {*/
                    sqlQuery("insert into ".$site_id."_wapcontents (wappage,componentname,propertyname,propertyvalue) values (".$row['wappage_id'].",'".$row_contents['componentname']."','".$row_contents['propertyname']."','".addslashes($row_contents['propertyvalue'])."')");
                //}                
            }
        }
        
        //dev page count
        $pagevercount = sqlQueryValue("select count(wappagedev_id) from ".$site_id."_wappagesdev where wappage_id='".$row['wappage_id']."'");

        //delete the oldest
        if ($pagevercount>=5)
        {
            $oldest = sqlQueryValue("select wappagedev_id, min(lastmod) from ".$site_id."_wappagesdev where wappage_id=".$row['wappage_id']." group by wappagedev_id");
            sqlQuery("delete from ".$site_id."_wappagesdev where wappagedev_id=".$oldest);
            sqlQuery("delete from ".$site_id."_wapcontentsdev where wappagedev=".$oldest);
        }

        //wappage dev copy
        sqlQuery("INSERT INTO " . $site_id . "_wappagesdev (name,title,description,template,lastmod,created,modby,enabled,visible,parent,ind,redirect,copypage,wappage_id) VALUES ('".$row['name']."','".$row['title']."','".$row['description']."','".$row['template']."','".$row['lastmod']."','".$row['created']."','".$row['modby']."','".$row['enabled']."','".$row['visible']."',".$row['parent'].",".$row['ind'].",".$row['redirect'].",".$row['copypage'].",".$row['wappage_id'].")");

        $newdev = sqlLastID();

        //copy contents for new wappage dev copy
        sqlQuery("INSERT INTO ".$site_id."_wapcontentsdev (wappagedev,componentname,propertyname,propertyvalue) select ".$newdev." as 'wappagedev',componentname,propertyname,propertyvalue from ".$site_id."_wapcontentsdev where wappagedev=".$row['wappagedev_id']);
            
        //Add access to the newly created page for Everyone
        AddGroupPermission(1, 20, $site_id, $newdev);
        //Add access to delete the newly created page for Everyone
        AddGroupPermission(1, 21, $site_id, $newdev);
        //Add access to the newly created page for Administrators
        AddGroupPermission(2, 20, $site_id, $newdev);
        //Add access to delete the newly created page for Administrators
        AddGroupPermission(2, 21, $site_id, $newdev);

        DeleteAllCompiledWapTemplates($site_id);

  }else if($action == "page_redirect")
  {
    $page_id = $_GET['id'];
    //Obtain domain
    $domain = sqlQueryValue("SELECT domain FROM sites WHERE site_id=" . $_GET['site_id']);
    
    $param = '';
    if (isset($_GET['viewpagehistory']))
    {
        $param = "?viewpagehistory=".$_GET['viewpagehistory'];
    }
    if ($_GET['inbrowser']==1)
    {
        if ($param!='') $param .= '&inbrowser=1';
        else $param = '?inbrowser=1';
    }
    if ($_GET['inbrowser']==2)
    {
        if ($param!='') $param .= '&inbrowser=2&wappagedev_id='.$_GET['wappagedev_id'];
        else $param = '?inbrowser=2&wappagedev_id='.$_GET['wappagedev_id'];
    }


    $path = WapPagePathById($page_id, $site_id);
    
    /*
    if ($param=='')
    {
        if (sqlQueryValue("SELECT is_card FROM ".$site_id."_wappages WHERE wappage_id=".$page_id))
        {
            //$path = substr($path, 0, strlen($path)-1);
            $param = '#page'.$page_id;
        }
    }
    */

    $wapsite = sqlQueryValue("SELECT wapsite FROM sites WHERE site_id=" . $_GET['site_id']);
    if ($wapsite) Redirect('http://' . $domain . '/' . $path. $param);
    else Redirect('http://' . $domain . '/wap/' . $path. $param);
  }
else if($action == "restore_history")
  {
     if (!isset($_GET['published']))
     {
         $wappagedev_id = $_GET['wappagedev_id'];
         $wappage_id = $_GET['id'];

         //source dev page data
         $row = sqlQueryRow("SELECT * FROM ".$site_id."_wappagesdev WHERE wappagedev_id='".$wappagedev_id."'");
         
         $active = sqlQueryValue("SELECT wappagedev_id, lastmod FROM ".$site_id."_wappagesdev WHERE wappage_id=".$wappage_id." ORDER BY lastmod DESC LIMIT 1");

         //update active dev page using hist. dev page data
         sqlQuery("UPDATE " . $site_id . "_wappagesdev SET name='".$row['name']."', title='".$row['title']."', description='".$row['description']."', template='".$row['template']."', lastmod='".time()."', modby='".$row['modby']."', enabled='".$row['enabled']."', visible='".$row['visible']."', redirect='".$row['redirect']."', copypage='".$row['copypage']."', is_card='".$row['is_card']."', is_root='".$row['is_root']."' WHERE wappagedev_id='".$active."'");

         //delete old
         sqlQuery("DELETE FROM ".$site_id."_wapcontentsdev WHERE wappagedev=".$active);

         //restore contents
         if ($row['template']==0)
         {
            //get page contents if no template
            $row_contents = sqlQueryRow("select * from ".$site_id."_wapcontentsdev where wappagedev=".$wappagedev_id." AND componentname='stdcontents'");

            /*if (sqlQueryValue("select * from ".$site_id."_wapcontentsdev where wappagedev=".$active))
            {
                sqlQuery("update ".$site_id."_wapcontentsdev SET propertyvalue='".addslashes($row_contents['propertyvalue'])."',propertyname='".$row_contents['propertyname']."',componentname='".$row_contents['componentname']."' where wappagedev=".$active);
            }
            else
            {*/
                sqlQuery("insert into ".$site_id."_wapcontentsdev (wappagedev,componentname,propertyname,propertyvalue) values (".$active.",'".$row_contents['componentname']."','".$row_contents['propertyname']."','".addslashes($row_contents['propertyvalue'])."')");
            //}
         }
         else
         {
            $data_contents = sqlQueryData("select * from ".$site_id."_wapcontentsdev where wappagedev=".$wappagedev_id);
            foreach ($data_contents as $row_contents)
            {
                /*if (sqlQueryValue("select * from ".$site_id."_wapcontentsdev where wappagedev=".$active." AND componentname='".$row_contents['componentname']."' AND propertyname='".$row_contents['propertyname']."'"))
                {
                    sqlQuery("update ".$site_id."_wapcontentsdev SET propertyvalue='".addslashes($row_contents['propertyvalue'])."' WHERE wappagedev=".$active." AND componentname='".$row_contents['componentname']."' AND propertyname='".$row_contents['propertyname']."'");
                }
                else
                {*/
                    sqlQuery("insert into ".$site_id."_wapcontentsdev (wappagedev,componentname,propertyname,propertyvalue) values (".$active.",'".$row_contents['componentname']."','".$row_contents['propertyname']."','".addslashes($row_contents['propertyvalue'])."')");
                //}                
            }
         }
     }
     else
     {
         //restoring from published page


         $wappage_id = $_GET['wappage_id'];
         //$wappage_id = $_GET['id'];

         //source page data
         $row = sqlQueryRow("SELECT * FROM ".$site_id."_wappages WHERE wappage_id='".$wappage_id."'");
         
         $active = sqlQueryValue("SELECT wappagedev_id, lastmod FROM ".$site_id."_wappagesdev WHERE wappage_id=".$wappage_id." ORDER BY lastmod DESC LIMIT 1");

         //update active dev page using published page data
         sqlQuery("UPDATE " . $site_id . "_wappagesdev SET name='".$row['name']."', title='".$row['title']."', description='".$row['description']."', template='".$row['template']."', lastmod='".time()."', modby='".$row['modby']."', enabled='".$row['enabled']."', visible='".$row['visible']."', redirect='".$row['redirect']."', copypage='".$row['copypage']."', is_card='".$row['is_card']."', is_root='".$row['is_root']."' WHERE wappagedev_id='".$active."'");

         //delete old
         sqlQuery("DELETE FROM ".$site_id."_wapcontentsdev WHERE wappagedev=".$active);

         //restore contents
         if ($row['template']==0)
         {
            //get page contents if no template
            $row_contents = sqlQueryRow("select * from ".$site_id."_wapcontents where wappage=".$wappage_id." AND componentname='stdcontents'");

            /*if (sqlQueryValue("select * from ".$site_id."_wapcontentsdev where wappagedev=".$active))
            {
                sqlQuery("update ".$site_id."_wapcontentsdev SET propertyvalue='".addslashes($row_contents['propertyvalue'])."',propertyname='".$row_contents['propertyname']."',componentname='".$row_contents['componentname']."' where wappagedev=".$active);
            }
            else
            {*/
                sqlQuery("insert into ".$site_id."_wapcontentsdev (wappagedev,componentname,propertyname,propertyvalue) values (".$active.",'".$row_contents['componentname']."','".$row_contents['propertyname']."','".addslashes($row_contents['propertyvalue'])."')");
            //}
         }
         else
         {
            $data_contents = sqlQueryData("select * from ".$site_id."_wapcontents where wappage=".$wappage_id);
            foreach ($data_contents as $row_contents)
            {
                /*if (sqlQueryValue("select * from ".$site_id."_wapcontentsdev where wappagedev=".$active." AND componentname='".$row_contents['componentname']."' AND propertyname='".$row_contents['propertyname']."'"))
                {
                    sqlQuery("update ".$site_id."_wapcontentsdev SET propertyvalue='".addslashes($row_contents['propertyvalue'])."' WHERE wappagedev=".$active." AND componentname='".$row_contents['componentname']."' AND propertyname='".$row_contents['propertyname']."'");
                }
                else
                {*/
                    sqlQuery("insert into ".$site_id."_wapcontentsdev (wappagedev,componentname,propertyname,propertyvalue) values (".$active.",'".$row_contents['componentname']."','".$row_contents['propertyname']."','".addslashes($row_contents['propertyvalue'])."')");
                //}                
            }
         }

     }

  }
?>
