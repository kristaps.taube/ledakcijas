<?

if(!$GLOBALS['perm_accessadmin'])
  Die('Access Forbidden');

$cols = Array(

  "enabled"     => Array(
    "width"     => "5%",
    "align"     => "center",
    "title"     => "{%langstr:col_enab%}"
  ),

  "type" => Array(
    "width"     => "45%",
    "title"     => "{%langstr:component_name%}"
  )
);

  global $docStrings;

  /*require($GLOBALS['cfgDirRoot']."library/"."class.toolbar.php");
  $Toolbar2 = new Toolbar();
  $Toolbar2->AddButton("add", "Add New", "add_component.gif", "openDialog('?module=components&action=uploadcomponent', 400, 120, 1);", "Upload and install a new component");
  $Toolbar2->AddLink("del", "Delete", "delete_component.gif", "if(SelectedRowID)window.location='?module=components&action=delcomponent&id='+SelectedRowID;", "Deletes selected component", "Are you sure you want to delete selected component?");
  $docStrings['toolbar'] = $Toolbar2->output();*/

  require($GLOBALS['cfgDirRoot']."library/"."class.toolbar2.php");
  $Toolbar = new Toolbar2();

  $Toolbar->AddSpacer();

  $Toolbar->AddButtonImage("add", 'components_new', "{%langstr:add_new%}", "", "openDialog('?module=components&action=uploadcomponent', 400, 120, 1);", 31, "{%langstr:hint_new_component%}");

  $Toolbar->AddButtonImage("add2", 'components_new', "{%langstr:install_existing%}", "", "openDialog('?module=components&action=installcomponentlist', 400, 600, 1, 1);", 31, "{%langstr:hint_install_existing%}");

  $Toolbar->AddButtonImage("add_cat", 'collection_new', "{%langstr:add_components_category%}", "", "openDialog('?module=components&action=addcategory', 400, 120, 1);", 31, "{%langstr:hint_add_components_category%}");

  $Toolbar->AddButtonImage("change_component", 'strings_properties', "{%langstr:change_category%}", "", "if(SelectedRowID)openDialog('?module=components&action=changecomponent&component_id='+SelectedRowID+'', 400, 120, 1);", 31, "{%langstr:hint_change_category%}");

  $Toolbar->AddButtonImage("del", 'delete', "{%langstr:col_del%}", "if(SelectedRowID)window.location='?module=components&action=delcomponent&id='+SelectedRowID;", '', 31, "{%langstr:hint_delete_component%}", "{%langstr:ask_delete_component%}");
  

  $Toolbar->AddSeperator();

  $docStrings['toolbar'] =  $Toolbar->output();



  $javascriptee = ' onClick="javascript:SubmitComponentEnabledCheckbox(this, \',component_id,\')"';

  $checkstylee = 'CONCAT(\'<input type="checkbox" name="cccb"' . $javascriptee . ' checked>\')';
  $checkstyle = '<input type="checkbox" name="cccb"' . $javascriptee . ' checked>';
  $uncheckstylee = 'CONCAT(\'<input type="checkbox" name="cccb"' . $javascriptee . '>\')';

  require($GLOBALS['cfgDirRoot']."library/"."class.datagrid.php");

  $Table = new DataGrid();
  $Table->cols = $cols;
  
  $Table->footer = false;
  $Table->rowformat = " onClick=\"javascript:HighLightTR(this, {%component_id%});if(SelectedRowID){ EnableToolbarButtons(che); }else { DisableToolbarButtons(che); }\"";

  # Standart components
  $standart_components = sqlQueryData("SELECT component_id, ELT(enabled+1,$uncheckstylee,$checkstylee) AS enabled, type FROM components  WHERE category_id = 2 ORDER BY  type");
  echo '<p style="font-size: 15px; font-weight: bold; margin: 3px 0 3px 0;">Standart</p>';
  $Table->data = $standart_components;
  echo $Table->output();

  # Default components
  $standart_components = sqlQueryData("SELECT component_id, ELT(enabled+1,$uncheckstylee,$checkstylee) AS enabled, type FROM components  WHERE category_id = 1 ORDER BY  type");
  echo '<p style="font-size: 15px; font-weight: bold; margin: 3px 0 3px 0;">Default</p>';
  $Table->data = $standart_components;
  echo $Table->output();


  $categories = sqlQueryData('SELECT category_id, category_name FROM component_categories  WHERE category_id != 1 AND category_id != 2 ORDER BY category_name');

  foreach ($categories as $id => $value)
  {
    echo '<p style="font-size: 15px; font-weight: bold; margin: 3px 0 3px 0;">'.$value['category_name'].'&nbsp;&nbsp;&nbsp;
          <a href="" title="{%langstr:col_del%}" href="#" onclick="answer(\'{%langstr:ask_delete_component%}\','.$value['category_id'].'); return false;"><img style="border: 0" src="/cms/backend/gui/images/del.gif" /></a></p>';
    $data = sqlQueryData("SELECT comp.component_id, comp.category_id, comp.type, wap, ELT(enabled+1,$uncheckstylee,$checkstylee) AS enabled FROM components comp WHERE comp.category_id = ".$value['category_id']." ORDER BY type");
    $Table->data = $data;
    echo $Table->output();
  }

echo '<script language="JavaScript" type="text/javascript">
/*<![CDATA[*/
che = new Array("del");
DisableToolbarButtons(che);
/*]]>*/

</script>';

?>

<script type="text/javascript">

  function answer(str,category_id)
  {
    var answer = confirm (str);
    if (answer)
    openDialog('?module=components&action=delcategory&category_id='+category_id+'', 400, 120, 0);
  }
</script>
