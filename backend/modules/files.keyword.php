<?

$perm_managefiles = $GLOBALS['perm_managefiles'];
if(!$perm_managefiles)
  AccessDenied(True);

$site_id = $GLOBALS['site_id'];

$data = sqlQueryData("SELECT * FROM " . $site_id . "_file_keywords WHERE category_id=0");
$lookup = Array();
foreach($data as $row)
{
  $lookup[] = $row['keyword_id'] . ':' . $row['word'];
}

$form_data = Array(

  "category"        => Array(
    "label"     => "{%langstr:category%}:",
    "lookup"    => $lookup,
    "type"      => "list"
  ),

  "title"    => Array(
    "label"     => "{%langstr:title%}:",
    "size"      => "35",
    "type"      => "text"
  )

);

require($GLOBALS['cfgDirRoot']."library/"."class.formgen.php");
$FormGen = new FormGen();
$FormGen->action = "?module=files&action=savekeyword&site_id=".$_GET['site_id'];
$FormGen->title = "{%langstr:add_keyword%}";
$FormGen->cancel = 'close';
$FormGen->properties = $form_data;
$FormGen->buttons = true;
echo $FormGen->output();

?>
