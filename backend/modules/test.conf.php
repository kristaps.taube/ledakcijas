<?

$action = $HTTP_GET_VARS['action'];

switch ($action) {

  // test form dialog box content
  case "form":
    $docTemplate = "dlg_form.htm";
    $docTitle = "Test Form";
    $docStrings['frame'] = "?module=test&action=form_content";
    break;

  // test form dialog box iframe content
  case "form_content":
    $docTemplate = "nothing.htm";
    $docScripts['body'] = "test.form.php";
    break;
    
  // content editor
  case "ceditor":
    $docTemplate = "dlg_ceditor.htm";
    $docTitle = "Content Editor";
    $docScripts['celement_selector'] = "test.celements.php";
    $docStrings['frame'] = "?module=test&action=ceditor_content";
    break;

  // fcontent editor iframe content
  case "ceditor_content":
    $docTemplate = "nothing.htm";
    $docScripts['body'] = "test.dummyform.php";
    break;
    
  // modal dialog
  case "modal":
    $docTemplate = "dlg_simple.htm";
    $docTitle = "Sample Modal Dialog";
    $docStrings['label'] = "Sample Data:";
    $docStrings['frame'] = "?module=test&action=modal_content";
    break;

  // modal dialog iframe content
  case "modal_content":
    $docTemplate = "nothing.htm";
    $docScripts['body'] = "test.form.php";
    break;
    
  case "close":
    $docTemplate = "close.htm";
    break;
    
  default:
    $docTemplate = "main.htm";
    $docTitle = "Test Table";
    $docScripts['body'] = "test.blank.php";
    break;
}

?>
