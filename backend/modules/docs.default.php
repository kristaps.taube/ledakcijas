<?

$perm_managefiles = $GLOBALS['perm_managefiles'];

$cols = Array(

  "history"     => Array(
    "width"     => "3%",
    "align"     => "center",
    "title"     => ""
  ),

  "move"     => Array(
    "width"     => "3%",
    "align"     => "center",
    "title"     => ""
  ),

  "public"     => Array(
    "width"     => "5%",
    "align"     => "center",
    "title"     => "{%langstr:col_pub%}"
  ),

  "name"        => Array(
    "width"     => "30%",
    "title"     => "{%langstr:col_name%}"
  ),

  "description"        => Array(
    "width"     => "35%",
    "title"     => "{%langstr:col_description%}"
  ),

  "size"    => Array(
    "width"     => "10%",
    "title"     => "{%langstr:col_size%}"
  ),
//35%
  "date"     => Array(
    "width"     => "15%",
    "title"     => "{%langstr:col_created%}"
  ),
/*
  "del"    => Array(
    "width"     => "5%",
    "title"     => "Del",
    "align"     => "center",
    "format"    => '<a href="?module=files&action=delete&path='.$GLOBALS['path'].'&file={%title%}&site_id='.$GLOBALS['site_id'].'" OnClick="return confirm(\'Are you sure you wish to delete this item?\')"><img src="gui/images/ico_del.gif" width="16" height="16" border="0" alt="Delete"></a>'
  )
*/
);

//----------------------------------------------------------------


$docroot = sqlQueryValue("SELECT docroot FROM sites WHERE site_id=".$GLOBALS['site_id']);

$dirlist = sqlQueryData("SELECT * FROM ".$GLOBALS['site_id']."_documentdir WHERE parent_directory=".$_SESSION['parent_dir']." ORDER BY ind");
$filelist = sqlQueryData("SELECT * FROM ".$GLOBALS['site_id']."_documents WHERE parent_directory=".$_SESSION['parent_dir']." AND deleted=0 ORDER BY ind");

if ($_SESSION['parent_dir']!=0)
{
    $parent = sqlQueryValue("SELECT parent_directory FROM ".$GLOBALS['site_id']."_documentdir WHERE dir_id=".$_SESSION['parent_dir']);
    $data[] = array("name" => '<img src="gui/images/ico_folder.gif" width="16" height="16" align="absmiddle"> <a href="?module=docs&parent_dir='.$parent.'&site_id='.$GLOBALS['site_id'].'">..</a>',
                    "size" => "",
                    "date" => date("d-m-Y H:i:s", $dir['creation_date']),
                    "title" => ".."
                    );
}

$current = $_SESSION['parent_dir'];
while ($current!=0)
{
    $row = sqlQueryRow("SELECT dir_name,parent_directory FROM ".$GLOBALS['site_id']."_documentdir WHERE dir_id=".$current);
    $pathArray[] = $row['dir_name'];
    $current = $row['parent_directory'];
}

$pathArray = array_reverse($pathArray);
$path = implode("/", $pathArray);

foreach ($dirlist as $dir)
{
    $data[] = array("name" => '<img src="gui/images/ico_folder.gif" width="16" height="16" align="absmiddle"> <a href="?module=docs&parent_dir='.$dir['dir_id'].'&site_id='.$GLOBALS['site_id'].'">'. $dir['dir_name'].'</a>',
                    "description" => $dir['dir_description'],
                    "size" =>fileSizeStr($docroot.$dir['real_name']),
                    "date" => date("d-m-Y H:i:s", $dir['creation_date']),
                    "title" => "dir:".$dir['dir_id'],
                    "move" => "<a href=\"?module=docs&site_id=".$GLOBALS['site_id']."&action=moveupdir&id=".$dir['dir_id']."\"><img src=\"gui/images/up.gif\" width=\"16\" height=\"16\" border=\"0\"></a><a href=\"?module=docs&site_id=".$GLOBALS['site_id']."&action=movedowndir&id=".$dir['dir_id']."\"><img src=\"gui/images/down.gif\" width=\"16\" height=\"16\" border=\"0\"></a>"
                    );
}

$javascripte = ' onClick="javascript:SubmitDocPublicCheckbox(this, \',page_id,\',' . $site_id . ')"';
//<a href="#"><img src="gui/images/hist.gif" height="16" width="16" border="0"></a>
foreach ($filelist as $file)
{
    $data[] = array("name" => '<img src="gui/images/ico_file.gif" width="16" height="16" align="absmiddle"> '.$file['file_name'],
                    "history" => '<a href="#" OnClick="openDialog(\'?module=docs&action=history&id='.$file['file_id'].'&site_id='.$GLOBALS['site_id'].'\',400, 150, 0, 1);"><img src="gui/images/hist.gif" height="16" width="16" border="0"></a>',
                    "public" => '<input type="checkbox" name="filee" '.($file['public']==1?'checked ':''). 'onClick="javascript:SubmitDocPublicCheckbox(this, \''.$file['file_id'].'\',' . $GLOBALS['site_id'] . ', '.$_SESSION['parent_dir'].')">',
                    "description" => $file['file_description'],
                    "size" => fileSizeStr($docroot.$file['real_name']),
                    "date" => date("d-m-Y H:i:s", $file['creation_date']),
                    "title" => "file:".$file['file_id'],
                    "move" => "<a href=\"?module=docs&site_id=".$GLOBALS['site_id']."&action=moveupfile&id=".$file['file_id']."\"><img src=\"gui/images/up.gif\" width=\"16\" height=\"16\" border=\"0\"></a><a href=\"?module=docs&site_id=".$GLOBALS['site_id']."&action=movedownfile&id=".$file['file_id']."\"><img src=\"gui/images/down.gif\" width=\"16\" height=\"16\" border=\"0\"></a>"
                    );
}

//----------------------------------------------------------------

//$data = read_files('http://'.$webroot.'/', $directory, $path);

echo '
<script language="JavaScript">
  var selrow = ' . (int) ($data[0][0]) . ';
  window.fff = "";
  function fileparam(fname)
  {
    var s = fff.indexOf(fname);
    var sub;
    if (s>=0) 
      {
        if (s==0) sub = window.fff.substr(fname.length+1);
        else sub = window.fff.substr(0,s-1) + window.fff.substr(s+fname.length);
        window.fff = sub
      }
    else
      {
        if (window.fff.length>0) window.fff = window.fff + ";" + fname;
        else window.fff = window.fff + fname;
      }
    
  }
</script>';

if ($docroot)
{
    require($GLOBALS['cfgDirRoot']."library/"."class.datagrid.php");

    $Table = new DataGrid();
    $Table->cols = $cols;
    $Table->data = $data;
    $Table->footer = false;
    $Table->padding = "2";
    $Table->rowformat = " onClick=\"javascript:HighLightTR(this, '{%title%}');if(SelectedRowID){ EnableToolbarButtons(che); }else { DisableToolbarButtons(che); }\"";
    echo("\n<br /><p><span class=\"gray\">{%langstr:docs_current_dir%}</span> /".$path."</p>\n");
    echo $Table->output();
}
else
{
    echo 'Document root is not specified for this site';
}

if($_GET['accessdenied'])
{
  AccessDenied(false, true);
  echo '
  <script language="JavaScript" type="text/javascript">
  /*<![CDATA[*/
  window.document.location.href = "?module=docs&site_id=' . $GLOBALS['site_id'] . '&selrow=' . $_GET['selrow'] . '&parent_directory='.$_GET['parent_directory'].'" 
  /*]]>*/
  </script>
  ';

}

echo '<script language="JavaScript" type="text/javascript">
/*<![CDATA[*/
che = new Array("ren","del","copy","move");
DisableToolbarButtons(che);
/*]]>*/
</script>';


?>
