<?

$site_id = $GLOBALS['site_id'];
$perm_managetemplates = $GLOBALS['perm_managetemplates'];

$cols = Array(

  "name"        => Array(
    "width"     => "30%",
    "title"     => "{%langstr:col_name%}",
    "format"    => "<a href=\"#\" OnClick=\"javascript:if({%template_id%}>0)openDialog('?module=templates&site_id=" . $site_id . "&action=contents_form&id={%template_id%}', 620, 540, 1, 1);
                                            else openDialog('?module=templates&site_id=" . $GLOBALS['site_id'] . "&action=editview&view_id={%template_id%}', 400, 390, 1, 1);\">{%name%}</a>"
  ),

  "description" => Array(
    "width"     => "60%",
    "title"     => "{%langstr:col_description%}"
  ),

);

function SortViewNodes($vdata, &$newdata, $parent, $level)
{
  if($vdata)
  {
    foreach($vdata as $row)
    {
      if($row['parent'] == $parent)
      {
        $row['level'] = $level;
        $newdata[] = $row;
        SortViewNodes($vdata, $newdata, -$row['template_id'], $level+1);
      }
    }
  }
}

if(!$perm_managetemplates)
  unset($cols['name']['format']);


$tdata = sqlQueryData("SELECT template_id, name, description FROM " . $site_id . "_templates ORDER BY ind");
//get template views in data
$data = array();
foreach($tdata as $row)
{
  $data[] = $row;
  $vdata = sqlQueryData("SELECT -view_id as template_id, name, description, parent FROM " . $site_id . "_views WHERE template_id=" . $row['template_id'] . " AND language_id = 0 ORDER BY ind");
  $newdata = Array();
  SortViewNodes($vdata, $newdata, 0, 0);
  foreach($newdata as $vrow)
  {
    $vrow['name'] = str_repeat('<img src="gui/images/1x1.gif" width="16" height="16" border="0" align="absmiddle">', $vrow['level']) .
                    "&nbsp;&nbsp;&nbsp;&nbsp;(view)&nbsp;" . $vrow['name'];
    $data[] = $vrow;
  }
}

require($GLOBALS['cfgDirRoot']."library/"."class.datagrid.php");

$Table = new DataGrid();
$Table->cols = $cols;
$Table->data = $data;
$Table->footer = false;
$Table->rowformat = " onClick=\"javascript:
                                var Toolbar2Div = document.getElementById('Toolbar2Div');
                                var Toolbar1Div = document.getElementById('Toolbar1Div');
                                HighLightTR(this, {%template_id%});if(SelectedRowID){ EnableToolbarButtons(che); }else { DisableToolbarButtons(che); }

                                try{
                                if({%template_id%}<0){
                                  Toolbar2Div.style.display='inline'; Toolbar1Div.style.display='none';}
                                else {
                                  Toolbar2Div.style.display='none'; Toolbar1Div.style.display='inline';}
                                }catch(e) {}\"";
echo $Table->output();

if($_GET['accessdenied'])
{
  AccessDenied(false, true);
  echo '
  <script language="JavaScript" type="text/javascript">
  /*<![CDATA[*/
  window.document.location.href = "?module=templates&site_id=' . $GLOBALS['site_id'] . '&selrow=' . $_GET['selrow'] . '"
  /*]]>*/
  </script>
  ';

}

echo '<script language="JavaScript" type="text/javascript">
/*<![CDATA[*/
che = new Array("edit", "addv", "defval", "langdefval", "langdefstrings", "perms", "del", "up", "down", "visedit");
DisableToolbarButtons(che);
/*]]>*/
</script>';


