<?

$perm_managedocuments = $GLOBALS['perm_managedocuments'];
$site_id = $_GET['site_id'];

$docroot = sqlQueryValue("SELECT docroot FROM sites WHERE site_id=".$GLOBALS['site_id']);

function SaveFile($directory, $filename2="", $id) {
  global $docroot;
  require_once($GLOBALS['cfgDirRoot']."library/"."class.fileupload.php");
  $UPLOADPATH = fileAddBackslash($directory);
  $FILENAME = "userfile";
  if ($filename2) $FILENAME = $filename2; 
  $EXTENSION = "";
  $SAVE_MODE = 1;
  $upload = new uploader;
  $upload->max_filesize(100000000);
  if($upload->upload("$FILENAME", "", "$EXTENSION")) {
    /*
    if (eregi($upload->file["extention"], '.php .phtml .cgi .pl')) {
      $upload->file["extention"] = ".dat";
      $upload->file["name"] = $upload->file["name"] . ".dat";
    }
    */
    $upload->file["extention"] = "";
    $upload->file["name"] = $id;
    if($upload->save_file($UPLOADPATH, $SAVE_MODE)) {
      Add_Log("Uploaded document '" . $upload->file["name"], $GLOBALS['site_id']);
    }
  }
  foreach($upload->errors as $error) $errors .= $error."<br>";
  unset($upload);

  //if upload was unsuccessful, then delete inserted record from DB
  if (!is_file($docroot.$id))
  {
      sqlQuery("DELETE FROM ".$GLOBALS['site_id']."_documents WHERE file_id=".$id);
  }
  else
  {
     Header("Location: ?module=docs&action=doc_permissions&site_id=".$GLOBALS['site_id']."&file_id=".$id."&name=".$_FILES['userfile']['name']);
     Die();
  }
}


switch ($GLOBALS["action"]) {

  case "save":
    if($perm_managedocuments)
    {
        $sibiling = intval(sqlQueryValue("SELECT max(ind) FROM " . $GLOBALS['site_id'] . "_documents WHERE parent_directory=".$_GET['parent_dir']));

        sqlQuery("INSERT INTO ".$GLOBALS['site_id']."_documents (file_name, file_description, parent_directory, creation_date, ind) VALUES ('".$_FILES['userfile']['name']."', '".$_POST['description']."', ".$_GET['parent_dir'].", '".time()."', '".($sibiling+1)."')");
        
        $id = sqlLastID();

        sqlQuery("UPDATE ".$GLOBALS['site_id']."_documents SET real_name='".$id."' WHERE file_id=".$id);
        
        if ($_FILES['userfile']['name']) SaveFile($docroot, 'userfile', $id);
    }
    break;

  case "doc_permissionssave":
        $keys = array_keys($_POST);
        //print_R($keys);
        if ($_POST['public']==0)
        {
            sqlQuery("UPDATE ".$GLOBALS['site_id']."_documents SET public=0");
        }
            foreach ($keys as $key)
            {
                if ($key!="public")
                {
                    //echo $key;
                    $perm_data = split(":", $key);
                    //print_R($perm_data);echo '<br>';
                    if ($perm_data[0]=="g")
                    {
                        sqlQuery("INSERT INTO permissions (perm_num, isuser, id, data, data2) VALUES (".$perm_data[2].", 0, ".$perm_data[1].", ".$GLOBALS['site_id'].",".$_GET['file_id'].")");
                    }
                    else
                    {
                        sqlQuery("INSERT INTO permissions (perm_num, isuser, id, data, data2) VALUES (".$perm_data[2].", 1, ".$perm_data[1].", ".$GLOBALS['site_id'].",".$_GET['file_id'].")");
                    }
                }
            }
      break;

  case "movedowndir":
        $dir_id = $_GET['id'];
        $row = sqlQueryRow("SELECT parent_directory, ind FROM " . $site_id . "_documentdir WHERE dir_id=$dir_id");
        if($row != null)
        {
          //Obtain the dir_id below
          $belowrow = sqlQueryRow("SELECT ind, dir_id FROM " . $site_id . "_documentdir WHERE parent_directory=" . $row['parent_parentdirectory'] . " AND ind>" . $row['ind'] . " ORDER BY ind LIMIT 1");
          if($belowrow != null)
          {
            sqlQueryValue("UPDATE " . $site_id . "_documentdir SET ind=" . $belowrow['ind'] . " WHERE dir_id=$dir_id");
            sqlQueryValue("UPDATE " . $site_id . "_documentdir SET ind=" . $row['ind'] . " WHERE dir_id=" . $belowrow['dir_id']);
          }
        }
      break;

  case "moveupdir":
        $dir_id = $_GET['id'];
        $row = sqlQueryRow("SELECT parent_directory, ind FROM " . $site_id . "_documentdir WHERE dir_id=$dir_id");
        if($row != null)
        {
          //Obtain the dir_id above
          $aboverow = sqlQueryRow("SELECT ind, dir_id FROM " . $site_id . "_documentdir WHERE parent_directory=" . $row['parent_directory'] . " AND ind<" . $row['ind'] . " ORDER BY ind DESC LIMIT 1");
          if($aboverow != null)
          {
            sqlQueryValue("UPDATE " . $site_id . "_documentdir SET ind=" . $aboverow['ind'] . " WHERE dir_id=$dir_id");
            sqlQueryValue("UPDATE " . $site_id . "_documentdir SET ind=" . $row['ind'] . " WHERE dir_id=" . $aboverow['dir_id']);
          }
        }
      break;

  case "movedownfile":
        $file_id = $_GET['id'];
        $row = sqlQueryRow("SELECT parent_directory, ind FROM " . $site_id . "_documents WHERE file_id=$file_id");
        if($row != null)
        {
          //Obtain the file_id below
          $belowrow = sqlQueryRow("SELECT ind, file_id FROM " . $site_id . "_documents WHERE parent_directory=" . $row['parent_parentdirectory'] . " AND ind>" . $row['ind'] . " ORDER BY ind LIMIT 1");
          if($belowrow != null)
          {
            sqlQueryValue("UPDATE " . $site_id . "_documents SET ind=" . $belowrow['ind'] . " WHERE file_id=$file_id");
            sqlQueryValue("UPDATE " . $site_id . "_documents SET ind=" . $row['ind'] . " WHERE file_id=" . $belowrow['file_id']);
          }
        }
      break;

  case "moveupfile":
        $file_id = $_GET['id'];
        $row = sqlQueryRow("SELECT parent_directory, ind FROM " . $site_id . "_documents WHERE file_id=$file_id");
        if($row != null)
        {
          //Obtain the dir_id above
          $aboverow = sqlQueryRow("SELECT ind, file_id FROM " . $site_id . "_documents WHERE parent_directory=" . $row['parent_directory'] . " AND ind<" . $row['ind'] . " ORDER BY ind DESC LIMIT 1");
          if($aboverow != null)
          {
            sqlQueryValue("UPDATE " . $site_id . "_documents SET ind=" . $aboverow['ind'] . " WHERE file_id=$file_id");
            sqlQueryValue("UPDATE " . $site_id . "_documents SET ind=" . $row['ind'] . " WHERE file_id=" . $aboverow['file_id']);
          }
        }
      break;

  case "delete":
    if($perm_managedocuments)
    {
      if ($_GET['file']!="..")
      {
        $file = explode(":",$_GET['file']);
        if ($file[0]=="file")
        {
            $perm_deldocument = CheckPermission(30, $GLOBALS['site_id'], $file[1]);
            if ($perm_deldocument)
            {
                fileDelete($docroot.$file[1]);
                sqlQuery("UPDATE ".$GLOBALS['site_id']."_documents SET deleted=1 WHERE file_id=".$file[1]);
                $name = sqlQueryValue("SELECT file_name FROM " . $site_id . "_documents WHERE file_id=".$file[1]);
                Add_Log("Deleted document '$name' (".$file[1].")", $site_id);
            }
            else
            {
                $name = sqlQueryValue("SELECT file_name FROM " . $site_id . "_documents WHERE file_id=".$file[1]);
                Add_Log("Permission denied to deleted document '$name' (".$file[1].")", $site_id); 
                Header("Location: ?module=".$_GET['module']."&parent_dir=".$_SESSION['parent_dir']."&site_id=" . $GLOBALS['site_id'] . "&selrow=" . $file[1] . "&accessdenied=1");
            }
        }
        else
        {
            function deleteDirectoryFiles($dir_id)
            {
                global $docroot;
                $files = sqlQueryData("SELECT * FROM ".$GLOBALS['site_id']."_documents WHERE parent_directory=".$dir_id);
                foreach ($files as $file)
                {
                    $perm_deldocument = CheckPermission(30, $GLOBALS['site_id'], $file['file_id']);
                    if ($perm_deldocument)
                    {
                        fileDelete($docroot.$file['file_id']);
                        sqlQuery("UPDATE ".$GLOBALS['site_id']."_documents SET deleted=1 WHERE file_id=".$file['file_id']);
                        $name = sqlQueryValue("SELECT file_name FROM " . $site_id . "_documents WHERE file_id=".$file['file_id']);
                        Add_Log("Deleted document '$name' (".$file['file_id'].")", $site_id);
                    }
                    else
                    {
                        $name = sqlQueryValue("SELECT file_name FROM " . $GLOBALS['site_id'] . "_documents WHERE file_id=".$file['file_id']);
                        Add_Log("Permission denied to deleted document '$name' (".$file['file_id'].")", $site_id); 
                        return false;
                    }
                }

                $dirs = sqlQueryData("SELECT dir_id FROM ".$GLOBALS['site_id']."_documentdir WHERE parent_directory=".$dir_id);

                foreach ($dirs as $dir)
                {
                    deleteDirectoryFiles($dir['dir_id']);
                }

                $row = sqlQueryRow("SELECT * FROM ".$GLOBALS['site_id']."_documentdir WHERE dir_id=$dir_id");
                if ($row)
                {
                    sqlQuery("DELETE FROM ".$GLOBALS['site_id']."_documentdir WHERE dir_id=".$dir_id);
                    sqlQuery("UPDATE ".$GLOBALS['site_id']."_documentdir SET ind=ind-1 WHERE parent_directory=".$row['parent_directory']." AND ind>".$row['ind']);
                }

                return true;
            }

            if (deleteDirectoryFiles($file[1])==false)
            {
                Header("Location: ?module=".$_GET['module']."&parent_dir=".$_SESSION['parent_dir']."&site_id=" . $GLOBALS['site_id'] . "&selrow=" . $file[1] . "&accessdenied=1");
            }
            
            //delete dir in DB then check recursive
            //sqlQuery("UPDATE ".$GLOBALS['site_id']."_documents SET deleted=1 WHERE file_id=".$file[1]);
        }
      }
      
      //DeleteFile($directory);
    }
    break;

  case "unsetdocpublic":
      $file_id = $_GET['id'];
      sqlQuery("UPDATE " . $site_id . "_documents SET public='0' WHERE file_id='$file_id'");
      break;

  case "setdocpublic":
      $file_id = $_GET['id'];
      sqlQuery("UPDATE " . $site_id . "_documents SET public='1' WHERE file_id='$file_id'");
      break;

  case "dorename":
    /*$i=0;
    while ($_POST['oldfile'.$i])
    {
    if($perm_managedocuments)
      RenameFile($directory,$_POST['oldfile'.$i], $_POST['newfile'.$i]);
    $i++;
    }*/
    $type = $_POST['type'];
    $oldname = $_POST['oldfile'];
    $newname = $_POST['newfile'];
    $id = $_POST['id'];
    $description = $_POST['description'];
    if ($type=="dir")
    {
        $parent = sqlQueryValue("SELECT parent_directory FROM ".$_GET['site_id']."_documentdir WHERE dir_id=".$id);
        $names_data = sqlQueryData("SELECT dir_name FROM ".$_GET['site_id']."_documentdir WHERE parent_directory=".$parent. " AND dir_name<>'".$oldname."'");
        foreach ($names_data as $val)
            $names[] = $val[0];

        //check if there is such name on the same level
        if (!in_array($newname,$names))
        {
            sqlQuery("UPDATE ".$_GET['site_id']."_documentdir SET dir_name='".$newname."', dir_description='".$description."' WHERE dir_id=".$id);
        }
    }
    else
    {
        $parent = sqlQueryValue("SELECT parent_directory FROM ".$_GET['site_id']."_documents WHERE file_id=".$id);
        $names_data = sqlQueryData("SELECT file_name FROM ".$_GET['site_id']."_documents WHERE parent_directory=".$parent." AND file_name<>'".$oldname."'");
        foreach ($names_data as $val)
            $names[] = $val[0];

        //check if there is such name on the same level
        if (!in_array($newname,$names))
        {
            sqlQuery("UPDATE ".$_GET['site_id']."_documents SET file_name='".$newname."', file_description='".$description."' WHERE file_id=".$id);
        }
    }

    break;

  case "docopy":
      //echo $directory;
        
      if ($_GET['file'] != "" ) {
        $files = explode(";",$_GET['file']);
        $dir = $_GET['over'];
        if ($dir=="/") $dir = "";
        //echo fileAddBackslash($directory).$file. "      ". $rootdir.$dir."/".$file. "   ".$_GET['move'];
        $rootdir = sqlQueryValue("SELECT dirroot FROM sites WHERE site_id = '".intval($GLOBALS['site_id'])."'");
        foreach ($files as $key=>$file)
          {
            if($perm_managedocuments)
              if ($_GET['move']==1) MoveFile(fileAddBackslash($directory).$file, $rootdir.$dir."/".$file);
              else CopyFile(fileAddBackslash($directory).$file, $rootdir.$dir."/".$file);
          }
        }
    break;

  case "mkdir":
    if($perm_managedocuments)
      //mkdir(fileAddBackslash($directory).$_POST['name'], 0777);
      
      $sibiling = intval(sqlQueryValue("SELECT max(ind) FROM " . $GLOBALS['site_id'] . "_documentdir WHERE parent_directory=".$_GET['parent_dir']));

      sqlQuery("INSERT INTO ".$GLOBALS['site_id']."_documentdir (dir_name, dir_description, parent_directory, creation_date, ind) VALUES ('".$_POST['name']."', '".$_POST['description']."', ".$_GET['parent_dir'].", '".time()."', '".($sibiling+1)."')");
      
      Add_Log("New Document directory '" . $_POST['name'] . "'", $GLOBALS['site_id']);
    break;

}

?>
