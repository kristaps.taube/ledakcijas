<?

$perm_managepages = $GLOBALS['perm_managepages'];

//function for sorting data in tree structure
function Nodes($data, &$newdata, $parent, $level, $justcount)
{
  $nodecount = 0;
  foreach($data as $row)
  {
    if($parent == $row['parent']){
      $nodecount++;
      if(!$justcount)
      {
        $row['prefix'] = str_repeat('<img src="'.$GLOBALS['cfgWebRoot'].'gui/images/1x1.gif" width="16" height="16" border="0" align="absmiddle">', $level) . '<img src="'.$GLOBALS['cfgWebRoot'].'gui/images/1x1.gif" width="16" height="16" border="0" align="absmiddle">&nbsp;';
        $newdata[] = $row;
        $c = count($newdata) - 1;
        if($_SESSION['expanded' . $row['page_id']])
        {
          $childs = Nodes($data, $newdata, $row['page_id'], $level + 1, 0);
        }
        else
        {
          $childs = Nodes($data, $newdata, $row['page_id'], $level + 1,true);
        }
        if($childs){
          if($_SESSION['expanded' . $row['page_id']])
          {
            $newdata[$c]['prefix'] = str_repeat('<img src="'.$GLOBALS['cfgWebRoot'].'gui/images/1x1.gif" width="16" height="16" border="0" align="absmiddle">', $level) . '<a href="?module=pages&site_id=' . $GLOBALS['site_id'] . '&action=simple&collapsebranch=' . $newdata[$c]['page_id'] . '"><img src="'.$GLOBALS['cfgWebRoot'].'gui/images/minus.gif" width="11" height="11" border="0" align="absmiddle"></a>&nbsp;&nbsp;';
          }else
          {
            $newdata[$c]['prefix'] = str_repeat('<img src="'.$GLOBALS['cfgWebRoot'].'gui/images/1x1.gif" width="16" height="16" border="0" align="absmiddle">', $level) . '<a href="?module=pages&site_id=' . $GLOBALS['site_id'] . '&action=simple&expandbranch=' . $newdata[$c]['page_id'] . '"><img src="'.$GLOBALS['cfgWebRoot'].'gui/images/plus.gif" width="11" height="11" border="0" align="absmiddle"></a>&nbsp;&nbsp;';
          }
        }
      }
    }
  }
  return $nodecount;
}


$site_id = $GLOBALS['site_id'];

// Let's try table generator

$cols = Array(

  "title"        => Array(
    "width"     => "100%",
    "title"     => "Title",
    "format"    => "{%prefix%}<a href=\"javascript:;\" OnClick=\"javascript: retVal('Form', 'LinkUrl', '/{%path%}');\">{%title%}</a>"
  )

);



/*
$javascriptv = ' onClick="javascript:SubmitPageVisibleCheckbox(this, \',page_id,\',' . $site_id . ')"';
$javascripte = ' onClick="javascript:SubmitPageEnabledCheckbox(this, \',page_id,\',' . $site_id . ')"';

if(!$perm_managepages)
{
  $javascriptv = '';
  $javascripte = '';
}

/*$checkstylev = 'CONCAT(\'<input type="checkbox" name="cccb"' . $javascriptv . ' checked>\')';
$uncheckstylev = 'CONCAT(\'<input type="checkbox" name="cccb"' . $javascriptv . '>\')';
$checkstylee = 'CONCAT(\'<input type="checkbox" name="cccb"' . $javascripte . ' checked>\')';
$uncheckstylee = 'CONCAT(\'<input type="checkbox" name="cccb"' . $javascripte . '>\')';  */

$unsorteddata = sqlQueryData("SELECT page_id, title, parent FROM " . $site_id . "_pages WHERE in_trash = 0 ORDER BY ind");
Nodes($unsorteddata, $data, 0, 0, 0);
foreach (array_keys($data) as $key) $data[$key]['path'] = PagePathById($data[$key]['page_id'], $site_id);

require($GLOBALS['cfgDirRoot']."library/"."class.datagrid.php");

$Table = new DataGrid();
$Table->cols = $cols;
$Table->data = $data;
$Table->header = false;
$Table->footer = false;
$Table->rowformat = " id=\"tablerow{%page_id%}\" onClick=\"javascript:HighLightTR(this, {%page_id%});\"";
echo $Table->output();

if($_GET['selrow'])
{
  echo '<script language="JavaScript">
        <!--
          preEl = document.getElementById("tablerow' . $_GET['selrow'] . '");
          ChangeTextColor(tablerow' . $_GET['selrow'] . ',\'tableSelected\');
          SelectedRowID = ' . $_GET['selrow'] . ';
        <!---->
        </script>';
}

?>
