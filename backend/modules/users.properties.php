<?

$perm_manageusers = $GLOBALS['perm_manageusers'];
$perm_managesiteusers = $GLOBALS['perm_managesiteusers'];
if((!$perm_manageusers)and(!$perm_managesiteusers))
{
  AccessDenied(True);
}

$usertypecombo = Array("2:{%langstr:can_log_in_constructor%}");
if($_GET['site_id'])
  $usertypecombo[] = "1:{%langstr:can_log_in_visual_editor%}";
$usertypecombo[] = "0:{%langstr:cant_log_in_constructor%}";

// Define form
$form_data = Array(

  "user_id"     => Array(
    "label"     => "ID:",
    "type"      => "hidden"
  ),

  "ind"         => Array(
    "noshow"    => true
  ),

  "username"        => Array(
    "label"     => "{%langstr:username%}:",
    "type"      => "str",
    "size"      => "30",
    "maxlength" => "20"
  ),
  
  "password"        => Array(
    "label"     => "{%langstr:password%}:",
    "type"      => "pass",
    "size"      => "30",
    "maxlength" => "20"
  ),

  "password2"   => Array(
    "label"     => "{%langstr:password_again%}:",
    "type"      => "pass",
    "size"      => "30",
    "maxlength" => "20"
  ),
  
  "addresses" => Array(
    "label"     => "{%langstr:ip_addresses%}:",
    "type"      => "html",
    "rows"      => "4",
    "cols"      => "30"
  ),


  "usertype"    => Array(
        "label"     => "{%langstr:user_type%}:",
        "type"      => "list",
        "lookup"    => $usertypecombo
      ),

  "fname"       => Array(
    "label"     => "{%langstr:first_name%}:",
    "type"      => "str",
    "size"      => "30",
    "maxlength" => "50"
  ),
  
  "lname"       => Array(
    "label"     => "{%langstr:last_name%}:",
    "type"      => "str",
    "size"      => "30",
    "maxlength" => "50"
  ),
  
  "email"       => Array(
    "label"     => "{%langstr:e_mail%}:",
    "type"      => "str",
    "size"      => "30",
    "maxlength" => "50"
  ),
  
  "phone"       => Array(
    "label"     => "{%langstr:phone%}:",
    "type"      => "str",
    "size"      => "30",
    "maxlength" => "15"
  ),
  
  "groups"      => Array(
    "label"     => "{%langstr:groups%}:",
    "type"      => "dialog",
    "size"      => "30",
    "maxlength" => "50",
    "dialog"    => "?module=users&action=grouplist&site_id=" . $_GET['site_id']
  ),
  
  "notes" => Array(
    "label"     => "{%langstr:col_description%}:",
    "type"      => "html",
    "rows"      => "4",
    "cols"      => "30"
  )


  
);

// Fill some values
  $id = $_GET["id"];
  $action = $_GET['action'];
  $create = ($action == 'create');

  if(!$create)
  {
    $row = sqlQueryRow("SELECT * FROM users WHERE user_id=$id");
    $usertype = 0;
    if($row['enabled'])
      $usertype = 2;
    if($row['visualeditonly'])
      $usertype = 1;
    $form_data['user_id']['value'] = $id;
    $form_data['username']['value'] = $row['username'];
    $form_data['addresses']['value'] = $row['addresses'];
    //$form_data['enabled']['value']  =$row['enabled'];
    $form_data['usertype']['value'] = $usertype;
    $form_data['fname']['value'] = $row['fname'];
    $form_data['lname']['value'] = $row['lname'];
    $form_data['email']['value'] = $row['email'];
    $form_data['phone']['value'] = $row['phone'];
    $form_data['notes']['value'] = $row['notes'];
    $col = sqlQueryColumn("SELECT group_id FROM users_groups WHERE user_id=$id");
    $form_data['groups']['value'] = join(",", $col);
  }

  $invalid = explode(':', $_GET['invalid']);
  foreach($invalid as $i)
  {
    if($i)
      $form_data[$i]['error'] = true;
  }

  foreach(array_keys($_GET) as $key)
  {
    $s = explode('_', $key, 2);
    if($s[0] == 'formdata')
      $form_data[$s[1]]['value'] = stripslashes($_GET[$key]);
  }
  
  if($_GET['jsmessage'])
  {
    echo '<script language="JavaScript">
          <!--
          alert("' . stripslashes($_GET['jsmessage']) . '");
          <!---->
          </script>';
  }


// Use FormGen to bring this out

  require($GLOBALS['cfgDirRoot']."library/"."class.formgen.php");
  $FormGen = new FormGen();
  if(!$create)
  {
    $FormGen->action = "?module=".$GLOBALS['module']."&site_id=" . $_GET['site_id'] . "&action=moduser";
  }else{
    $FormGen->action = "?module=".$GLOBALS['module']."&site_id=" . $_GET['site_id'] . "&action=adduser";
  }
  $FormGen->cancel = "close";
  if(!$create)
  {
    $FormGen->title = "{%langstr:user_properties%} : " . $row['username'];
  }else{
    $FormGen->title = "{%langstr:create_new_user%}";
  }
  $FormGen->properties = $form_data;
  $FormGen->buttons = true;
  echo $FormGen->output();

?>
