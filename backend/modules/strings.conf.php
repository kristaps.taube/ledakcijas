<?
$action = $_GET["action"];
$GLOBALS['site_id'] = $_GET["site_id"];
$site_id = $_GET["site_id"];
$GLOBALS['tabs'] = 'site';


unset($perm_modsite);
if(($site_id == $GLOBALS["currentUserSiteID"])or(!$GLOBALS["currentUserSiteID"]))
  $perm_modsite = CheckPermission(6, $site_id);
if(!$perm_modsite)
  AccessDenied(False);

unset($perm_managetemplates);
if(($site_id == $GLOBALS["currentUserSiteID"])or(!$GLOBALS["currentUserSiteID"]))
  $perm_managetemplates = CheckPermission(9, $site_id);

unset($perm_managepages);
if(($site_id == $GLOBALS["currentUserSiteID"])or(!$GLOBALS["currentUserSiteID"]))
  $perm_managepages = CheckPermission(8, $site_id);


$GLOBALS['PageEncoding'] = PageEncoding(0, $site_id);
$GLOBALS['maincharset']  = PageEncoding(0, $site_id, false);

$docScripts['sellanguage'] = "sellanguage.default.php";
include($GLOBALS['cfgDirRoot']."backend/modules/"."sellanguage.setlanguage.php");
$GLOBALS['PageEncoding'] = FullEncoding($GLOBALS['currentlanguage']);
$GLOBALS['maincharset']  = $GLOBALS['currentlanguage'];


switch ($action) {

  case "create":
    $docTemplate = "form.htm";
    $GLOBALS['resizeauto'] = 1;
    $docTitle = "{%langstr:create_string%}";
    $docScripts['body'] = "strings.properties.php";
    break;

  // site properties form
  case "properties_form":
    $docTemplate = "form.htm";
    $GLOBALS['resizeauto'] = 1;
    $docTitle = "{%langstr:string_properties%}";
    $docScripts['body'] = "strings.properties.php";
    break;

  // edit strings per page
  case "page_strings":
    $docTemplate = "form.htm";
    $GLOBALS['resizeauto'] = 1;
    $docTitle = "{%langstr:edit_string%}";
    $docScripts['body'] = "strings.list.php";
    break;

  case "modstring":
  case "addstring":
    $docTemplate = "close.htm";
    $docScripts['exec'] = "strings.exec.php";
    break;

  case "delstring":
    $docScripts['exec'] = "strings.exec.php";
    Header("Location: ?module=strings&site_id=" . $site_id);

  case "updatestrings":
    $docTemplate = "close.htm";
    $docScripts['exec'] = "strings.exec.php";
    break;


  default:

    if($perm_managetemplates)
    {
      /*require($GLOBALS['cfgDirRoot']."library/"."class.toolbar.php");
      $Toolbar = new Toolbar();
      $Toolbar->AddButton("add", "New String", "new_template.gif", "javascript:openDialog('?module=strings&site_id=" . $GLOBALS['site_id'] . "&action=create', 400, 220, 0, 0);", "Add New String");
      $Toolbar->AddButton("props", "Properties", "edit_properties.gif", "javascript:if(SelectedRowID)openDialog('?module=strings&site_id=" . $GLOBALS['site_id'] . "&action=properties_form&id='+SelectedRowID, 400, 220, 0, 0);", "Edit String");
      $Toolbar->AddLink("del", "Delete", "delete_template.gif", "javascript:if(SelectedRowID)window.location='?module=strings&site_id=" . $GLOBALS['site_id'] . "&action=delstring&id='+SelectedRowID;", "Delete Selected String", "Are you sure you want to delete selected String?");
      $docStrings['toolbar'] = $Toolbar->output();*/

      require($GLOBALS['cfgDirRoot']."library/"."class.toolbar2.php");
      $Toolbar = new Toolbar2();

      $Toolbar->AddSpacer();
      $Toolbar->AddButtonImage('add', 'strings_new', '{%langstr:new_string%}', "", "javascript:openDialog('?module=strings&site_id=" . $GLOBALS['site_id'] . "&action=create', 400, 220, 0, 0);", 31, "{%langstr:add_new_string%}");
      $Toolbar->AddButtonImage('props', 'strings_properties', '{%langstr:edit_string%}', "", "javascript:if(SelectedRowID)openDialog('?module=strings&site_id=" . $GLOBALS['site_id'] . "&action=properties_form&id='+SelectedRowID, 400, 220, 0, 0);", 31, "{%langstr:edit_selected_string%}");
      $Toolbar->AddSeperator();
      $Toolbar->AddButtonImage('del', 'delete', "{%langstr:col_del%}", "javascript:if(SelectedRowID)window.location='?module=strings&site_id=" . $GLOBALS['site_id'] . "&action=delstring&id='+SelectedRowID;", "", 31, "{%langstr:delete_selected_string%}", "{%langstr:sure_delete_string%}");
      $Toolbar->AddSeperator();

      $docStrings['toolbar'] =  $Toolbar->output();
    }
    $docTemplate = "main.htm";
    $docTitle = "Strings";
    $docScripts['body'] = "strings.default.php";

    break;

}

?>
