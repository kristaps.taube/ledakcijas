<?
  $perm_managecontents = $GLOBALS['perm_managecontents'];
  if(!$perm_managecontents)
    AccessDenied(True);

  require_once ($GLOBALS['cfgDirRoot']."library/".'class.formgen.php');

  $GLOBALS['pagedev_id'] = $_GET['id'];
  $pagedev_id = $_GET['id'];
 
  $site_id = $GLOBALS['site_id'];
  $component_name = $_GET['component_name'];
  $component_type = $_GET['component_type'];
  $GLOBALS['PageEncoding'] = PageDevEncoding($pagedev_id, $site_id);
  $GLOBALS['maincharset']  = PageDevEncoding($pagedev_id, $site_id, false);

  list($page_id, $copypage) = sqlQueryRow("SELECT page_id, copypage FROM ".$site_id."_pagesdev WHERE pagedev_id=$pagedev_id");
  $GLOBALS['page_id'] = $page_id;
  $GLOBALS['copypage'] = $copypage;
  $pageData = sqlQueryRow("SELECT * FROM " . $site_id . "_pagesdev LEFT JOIN " . $site_id . "_languages ON " . $site_id . "_languages.language_id=" . $site_id . "_pagesdev.language WHERE " . $site_id . "_pagesdev.pagedev_id='$pagedev_id'");

  if(!$component_name)
  {
    echo "No components selected.";
  }
  if(($_GET['noclose'])and($component_name))
  {
    //refresh parent page

    $newDiv = outputGraphicalComponent($hascontent, $component_type, $component_name, $page_id, 1, stripslashes($_GET['paramstr']), false, $toolbarfunc, false, $pagedev_id);
    $newDiv = borderDiv() . $newDiv;
    $newDiv = parseForJScript($newDiv);


    $obj = new $component_type($component_name);
    addDefaultProperties($obj);
    $obj->dev = 1;
    if($obj->getProperty('visible') == 0)
    {
      $displaymode = 'block';
      $optcol = '#000000';
    }else
    {
      $displaymode = 'none';
      $optcol = '#999999';
    }



    echo '
    <script language="JavaScript">

<!--

  var win = window.parent.dialogArguments[0];
  var el = window.parent.dialogArguments[1];
  try{
    el.innerHTML = "' . $newDiv . '";
    el.set_tb_cnt = "' . IIF($toolbarfunc, ParseForJScript($toolbarfunc), 'null') . '";
    el.style.display = "' . $displaymode . '";
    window.parent.selectcomponent.options.item(window.parent.selectcomponent.selectedIndex).style.color = "'.$optcol.'";
  }catch(e){}

//-->

</script> ';
  }

?>
<script>
  function prop_editor_activate_tab(tab_num) {

    if (tab_num == 1)
    {

      poga1.style.backgroundColor = '#C8C8C8';
      poga2.style.backgroundColor = '#F0F0F0';
      poga3.style.backgroundColor = '#F0F0F0';
      pageprops.style.display = 'block';
      langdefprops.style.display = 'none';
      defprops.style.display = 'none';
      document.Form = Form1;

    }
    else if (tab_num == 2)
    {

      poga1.style.backgroundColor = '#F0F0F0';
      poga2.style.backgroundColor = '#C8C8C8';
      poga3.style.backgroundColor = '#F0F0F0';
      pageprops.style.display = 'none';
      langdefprops.style.display = 'none';
      defprops.style.display = 'block';
      document.Form = Form2;

    }
    else if (tab_num == 3)
    {

      poga1.style.backgroundColor = '#F0F0F0';
      poga2.style.backgroundColor = '#F0F0F0';
      poga3.style.backgroundColor = '#C8C8C8';
      pageprops.style.display = 'none';
      langdefprops.style.display = 'block';
      defprops.style.display = 'none';
      document.Form = Form3;

    }

  }

</script>
<?php

//  echo '<form name="Form" action="?module=' .$GLOBALS['module']. '&site_id=' .$site_id. '&page_id=' .$page_id. '&component_type=' .$component_type. '&component_name=' .$component_name .'&id=' .$page_id. '&action=updatecontrols&noclose=1" method=POST>
//          <input type="hidden" name="id" value="' . $page_id . '">';
//output default values buttons
if (!$_GET['template_editor'])
  echo '
  <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%" id="AutoNumber1" height="28">
  <tr>
    <td id="poga1" width="*" height="28" align="center" style="border-style: solid; border-width: 1; padding-left: 4; padding-right: 4; padding-top: 1; padding-bottom: 1; background-color: #C8C8C8">
    <a href="#" onclick="prop_editor_activate_tab(1);">{%langstr:page_values%}</a></td>
    <td width="8" height="28" align="center">&nbsp;</td>
    <td id="poga3" width="*" height="28" align="center" style="border-style: solid; border-width: 1; padding-left: 4; padding-right: 4; padding-top: 1; padding-bottom: 1; background-color: #F0F0F0">
    <a href="#" onclick="prop_editor_activate_tab(3);">{%langstr:default_language_values%}</a></td>
    <td width="8" height="28" align="center">&nbsp;</td>
    <td id="poga2" width="*" height="28" align="center" style="border-style: solid; border-width: 1; padding-left: 4; padding-right: 4; padding-top: 1; padding-bottom: 1; background-color: #F0F0F0">
    <a href="#" onclick="prop_editor_activate_tab(2);">{%langstr:default_values%}</a></td>

  </tr>
</table>';
//if ($_GET['action']=='updatecontrols')
//{
    echo '
    <script>
        function showimage()
        {
            document.all.prgbar.style.display="block";
        }
        //document.all.prgbar.style.display="none";
        //setTimeout("hideimage();",5000);
    </script>
    ';
    echo '<div id="prgbar" align="center" style="padding-top:3px;display:none;"><img src="'.$GLOBALS['cfgWebRoot'].'gui/images/progresbar.gif" width="95%" height="10"></div>';
//}
///
  $form_data = Array();
  if (!$_GET['template_editor'])
  {
      if(HasComponentPermissionPage($site_id, $page_id, $component_name))
      {
        displayPropertiesTable($component_type, $component_name, $site_id, $pagedev_id, $form_data, true);
      }
  }
  else
  {
      //HasComponentPermission($site_id, $template_id, $component_name)      
      if(HasComponentPermission($site_id, $page_id, $component_name))
      {
        displayPropertiesTable($component_type, $component_name, $site_id, -1, $form_data, true);
      }
  }
  if (!$_GET['template_editor'])
  {
      echo '<div id="pageprops">';
      $Inspector = new Inspector();
      $Inspector->allowwysiwyg = false;
      $Inspector->properties = $form_data;
      $Inspector->opened = true;
      $Inspector->buttons =false;
      $Inspector->cancel ="close";
      $Inspector->name = "Form1";
      $Inspector->action = '?module=' .$GLOBALS['module']. '&site_id=' .$site_id. '&pagedev_id=' .$pagedev_id. '&component_type=' .$component_type. '&component_name=' .$component_name .'&id=' .$pagedev_id. '&action=updatecontrols&page_id='.$page_id.'&active_tab=1&noclose=1';
      $Inspector->displayplus = False;
      $Inspector->displaytitle = False;
      echo $Inspector->output();
      echo '</div>';
      $form_param = "";
  }
  else
  {
      $form_param = "&template_editor=1";
  }
///
  echo '<div id="defprops">';
  $form_data = Array();
  displayPropertiesTable($component_type, $component_name, $site_id, -1, $form_data, true);
  $Inspector = new Inspector();
  $Inspector->allowwysiwyg = false;
  $Inspector->properties = $form_data;
  $Inspector->opened = true;
  $Inspector->buttons =false;
  $Inspector->cancel ="close";
  $Inspector->name = "Form2";
  $Inspector->hidecheckboxes = true;
  $Inspector->action = '?module=' .$GLOBALS['module']. '&site_id=' .$site_id. '&pagedev_id=' .$pagedev_id. '&component_type=' .$component_type. '&component_name=' .$component_name .'&id=' .($page_id>0?$pagedev_id:$page_id). '&page_id='.$page_id.'&action=updatecontrolsdef&active_tab=2&noclose=1'.$form_param;
  $Inspector->displayplus = False;
  $Inspector->displaytitle = False;
  echo $Inspector->output();
  echo '</div>';

  $copypage = sqlQueryValue('SELECT -view_id FROM ' . $site_id . '_views WHERE template_id = ' . $pageData['template'] . ' AND language_id = ' . $pageData['language']);
  echo '<div id="langdefprops">';
  $form_data = Array();
  if($copypage)
    displayPropertiesTable($component_type, $component_name, $site_id, $copypage, $form_data, true);
  $Inspector = new Inspector();
  $Inspector->allowwysiwyg = false;
  $Inspector->properties = $form_data;
  $Inspector->opened = true;
  $Inspector->buttons =false;
  $Inspector->cancel ="close";
  $Inspector->name = "Form3";
  $Inspector->action = '?module=' .$GLOBALS['module']. '&site_id=' .$site_id. '&pagedev_id=' .$copypage. '&component_type=' .$component_type. '&component_name=' .$component_name .'&id=' .($page_id>0?$pagedev_id:$page_id). '&page_id='.$page_id.'&action=updatecontrols&active_tab=3&noclose=1'.$form_param;
  $Inspector->displayplus = False;
  $Inspector->displaytitle = False;
  echo $Inspector->output();
  echo '</div>';

if (!$_GET['template_editor'])
{

    $active_tab = 1;
    if (!empty($_GET['active_tab']) && in_array($_GET['active_tab'], array(1,2,3)))
    {
      $active_tab = $_GET['active_tab'];
    }

    echo '<script language="JavaScript" type="text/javascript">
    /*<![CDATA[*/
    prop_editor_activate_tab('.$active_tab.');
    /*]]>*/
    </script>
    ';

}
else 
{
    echo '<script language="JavaScript" type="text/javascript">
    /*<![CDATA[*/
    document.Form = Form2;
    /*]]>*/
    </script>
    ';
}
?>
