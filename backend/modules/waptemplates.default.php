<?

$site_id = $GLOBALS['site_id'];
$perm_managetemplates = $GLOBALS['perm_managetemplates'];

$cols = Array(

  "name"        => Array(
    "width"     => "30%",
    "title"     => "{%langstr:col_name%}",
    "format"    => "<a href=\"#\" OnClick=\"javascript:if({%template_id%}>0)openDialog('?module=waptemplates&site_id=" . $site_id . "&action=contents_form&id={%template_id%}', 620, 540, 1, 1);
                                            else openDialog('?module=waptemplates&site_id=" . $GLOBALS['site_id'] . "&action=editview&view_id={%template_id%}', 400, 390, 1, 1);\">{%name%}</a>"
  ),

  "description" => Array(
    "width"     => "60%",
    "title"     => "{%langstr:col_description%}"
  ),

);

if(!$perm_managetemplates)
  unset($cols['name']['format']);


$data = sqlQueryData("SELECT template_id, name, description FROM " . $site_id . "_waptemplates ORDER BY ind");

require($GLOBALS['cfgDirRoot']."library/"."class.datagrid.php");

$Table = new DataGrid();
$Table->cols = $cols;
$Table->data = $data;
$Table->footer = false;
$Table->rowformat = " onClick=\"javascript:HighLightTR(this, {%template_id%});if(SelectedRowID){ EnableToolbarButtons(che); }else { DisableToolbarButtons(che); }
                                try{
                                if({%template_id%}<0){
                                  Toolbar2Div.style.display='inline'; Toolbar1Div.style.display='none';}
                                else {
                                  Toolbar2Div.style.display='none'; Toolbar1Div.style.display='inline';}
                                }catch(e) {}\"";
echo $Table->output();

if($_GET['accessdenied'])
{
  AccessDenied(false, true);
  echo '
  <script language="JavaScript" type="text/javascript">
  /*<![CDATA[*/
  window.document.location.href = "?module=waptemplates&site_id=' . $GLOBALS['site_id'] . '&selrow=' . $_GET['selrow'] . '" 
  /*]]>*/
  </script>
  ';

}

echo '<script language="JavaScript" type="text/javascript">
/*<![CDATA[*/
che = new Array("edit", "addv", "defval", "perms", "del", "up", "down");
DisableToolbarButtons(che);
/*]]>*/
</script>';


?>
