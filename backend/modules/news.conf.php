<?
$GLOBALS['site_id'] = $_GET["site_id"];
$site_id = $_GET["site_id"];
$page_id = $_GET['page_id'];



unset($perm_modsite);
if(($site_id == $GLOBALS["currentUserSiteID"])or(!$GLOBALS["currentUserSiteID"]))
  $perm_modsite = CheckPermission(6, $site_id);

if(!$perm_modsite)
{
  echo "Permission Denied!";
  AccessDenied(True);
}

sqlQuery('CREATE TABLE IF NOT EXISTS '.$site_id.'_news (news_id INT (32) not null AUTO_INCREMENT,
          category_id INT not null , title TEXT , text TEXT , date DATETIME ,
          PRIMARY KEY (news_id), INDEX (news_id))');
  

$action = $_GET["action"];
require($GLOBALS['cfgDirRoot']."library/"."class.toolbar.php");

switch ($action) {

  case "listcategories":
    $docTitle = "News Category List";
    $docTemplate = "form.htm";
    $docScripts['body'] = "news.categories.php";

    break;
    
  case "listnews":
    $docTitle = "Edit News";
    $docTemplate = "form.htm";
    $docScripts['body'] = "news.newslist.php";

    break;
    
  case "newcategory":
    $docTitle = "New Category";
    $docTemplate = "form.htm";
    $docScripts['body'] = "news.editcategory.php";
    break;
    
  case "editcategory":
    $docTitle = "Edit Category";
    $docTemplate = "form.htm";
    $docScripts['body'] = "news.editcategory.php";
    break;

  case "newnews":
    $docTitle = "New News Item";
    $docTemplate = "form.htm";
    $docScripts['body'] = "news.editnews.php";
    break;

  case "editnews":
    $docTitle = "Edit News Item";
    $docTemplate = "form.htm";
    $docScripts['body'] = "news.editnews.php";
    break;

  case "modcategory":
  case "addcategory":
  case "modnews":
  case "addnews":
    $docTemplate = "close.htm";
    $docScripts['exec'] = "news.exec.php";
    break;

  case "delcategory":
    $docScripts['exec'] = "news.exec.php";
    Header("Location: ?module=news&action=listcategories&site_id=" . $site_id . "&page_id=" . $page_id);
    break;
    
  case "delnews":
    $docScripts['exec'] = "news.exec.php";
    Header("Location: ?module=news&action=listnews&category_id=" . $_GET['category_id'] . "&site_id=" . $site_id . "&page_id=" . $page_id);
    break;

  default:



    break;

}
  
?>
