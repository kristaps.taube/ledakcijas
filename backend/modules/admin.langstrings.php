<?

if(!$GLOBALS['perm_accessadmin'])
  Die('Access Forbidden');

$site_id = $_GET['site_id'];

$cols = Array(

  "name"  => Array(
    "width"     => "30%",
    "title"     => "{%langstr:col_string_name%}",
    "format"    => "<a href=\"#\" OnClick=\"javascript:openDialog('?module=admin&site_id=" . $GLOBALS['site_id'] . "&action=phrase_properties_form&id={%phrasename_id%}', 400, 220, 0, 0);\">{%name%}</a>"
  ),

  "phrase" => Array(
    "width"     => "70%",
    "title"     => "{%langstr:col_value%}"
  )

);

if($_GET['setlang'])
{
  $_SESSION['editphraselang'] = $_GET['lang'];
}

$data = sqlQueryData("SELECT * FROM phrases LEFT JOIN phrasenames ON phrasename=phrasename_id WHERE lang='".$_SESSION['editphraselang']."' ORDER BY name");


//require($GLOBALS['cfgDirRoot']."library/"."class.toolbar2.php");
$Toolbar = new Toolbar2();

$Toolbar->AddSpacer();
$Toolbar->AddButtonImage('add', 'strings_new', '{%langstr:toolbar_new_phrase2%}', "", "javascript:openDialog('?module=admin&site_id=" . $GLOBALS['site_id'] . "&action=phrase_create', 400, 220, 0, 0);", 31, "{%langstr:toolbar_add_new_phrase%}");
$Toolbar->AddButtonImage('props', 'strings_properties', '{%langstr:toolbar_edit_phrase%}', "", "javascript:if(SelectedRowID)openDialog('?module=admin&site_id=" . $GLOBALS['site_id'] . "&action=phrase_properties_form&id='+SelectedRowID, 400, 220, 0, 0);", 31, "{%langstr:toolbar_edit_selected_phrase%}");
$Toolbar->AddSeperator();

  $Toolbar->AddButtonImage("backup", 'admin_backup', "{%langstr:toolbar_backup_langstrings%}", "javascript:window.location='?module=admin&site_id=".$_GET['site_id']."&action=backup_langstrings'", '', 31, "{%langstr:hint_backup_langstrings%}");
  $Toolbar->AddButtonImage("restore", 'admin_restore', "{%langstr:toolbar_restore_langstrings%}", "javascript:window.location='?module=admin&site_id=".$_GET['site_id']."&action=browslangstrebackups'", '', 31, "{%langstr:hint_restore_langstrings%}");
$Toolbar->AddSeperator();

$Toolbar->AddButtonImage('del', 'delete', "{%langstr:toolbar_delete%}", "javascript:if(SelectedRowID)window.location='?module=admin&site_id=" . $GLOBALS['site_id'] . "&action=delphrase&id='+SelectedRowID;", "", 31, "{%langstr:toolbar_delete_selected_phrase%}", "{%langstr:toolbar_ask_phrase_delete%}");
$Toolbar->AddSeperator();

$GLOBALS['docStrings']['toolbar'] =  $Toolbar->output();

$langs = sqlQueryColumn("SELECT lang FROM phrases WHERE lang<>'' GROUP BY lang");
echo '{%langstr:select_language%}: <select name="langselect" size="1" onchange="window.location.href=\'?module=admin&action=langstrings&site_id='.$site_id.'&setlang=1&lang=\' + this.options[this.selectedIndex].value">';
echo '<option value="">{%langstr:default_language%}</option>';
foreach($langs as $lan)
{
  echo '<option value="'.$lan.'"'.($_SESSION['editphraselang'] == $lan?' selected="selected"':'').'>'.$lan.'</option>';
}
echo '
</select><br /><br />';

require($GLOBALS['cfgDirRoot']."library/"."class.datagrid.php");
$Table = new DataGrid();
$Table->cols = $cols;
$Table->data = $data;
$Table->footer = false;
$Table->padding = "2";
$Table->rowformat = " onClick=\"javascript:HighLightTR(this, '{%phrasename_id%}');if(SelectedRowID){ EnableToolbarButtons(che); }else { DisableToolbarButtons(che); }\"";
echo $Table->output();

echo '<script language="JavaScript" type="text/javascript">
/*<![CDATA[*/
che = new Array("ren");
DisableToolbarButtons(che);
/*]]>*/
</script>';

?>
