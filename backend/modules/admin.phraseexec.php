<?

  $site_id = $GLOBALS['site_id'];

  if(($action == 'addphrase')or($action == 'modphrase'))
  {
    $name = $_POST['name'];
    $phrasename_id = $_POST['phrasename_id'];
    //Check if field input is valid
    $invalid = Array();
    $jsmessage = '';
    if((IsEmpty($name))or(IsBadSymbols($name))or(sqlQueryValue("SELECT phrasename_id FROM phrasenames WHERE name='$name' AND phrasename_id<>'$phrasename_id'")))
    {
      $invalid[] = "name";
      if(!IsEmpty($name))
        $jsmessage .= 'Phrase name ' . msgValidCharsUnique . '\n';
    }

    if(count($invalid))
    {
      if($action == 'addphrase')
        $action = 'phrase_create';
      if($action == 'modphrase')
        $action = 'phrase_properties_form';
      $invalid = join(':', $invalid);
      $name = urlencode(stripslashes($name));
      $phrase = urlencode(stripslashes($phrase));
      $lang = urlencode(stripslashes($lang));

      Redirect('?module='.$module.'&action='.$action.'&invalid=' . $invalid . '&formdata_name='.$name . '&jsmessage='.$jsmessage.'&id='.$phrasename_id.'&site_id='.$site_id);
      Die();
    }
  }

  if(($action == 'addphrase'))
  {
    print_r($_POST);
    sqlQuery("INSERT INTO phrasenames (name) VALUES ('$name')");
    $name_id = sqlQueryValue("SELECT phrasename_id FROM phrasenames WHERE name='$name'");
    $f = 1;
    while(isset($_POST['lang'. $f]))
    {
      //echo "INSERT INTO phrases (lang,phrasename,phrase) VALUES ('".$_POST['lang'. $f]."','$name_id','".mb_convert_encoding($_POST['phrase'. $f], 'HTML-ENTITIES', 'UTF-8')."')";
      sqlQuery("INSERT INTO phrases (lang,phrasename,phrase) VALUES ('".$_POST['lang'. $f]."','$name_id','".mb_convert_encoding($_POST['phrase'. $f], 'HTML-ENTITIES', 'UTF-8')."')");
      $f++;
    }
    Add_Log("New phrase '$name' ($phrasename_id)", $site_id);

  }else if(($action == 'modphrase'))
  {
    sqlQuery("UPDATE phrasenames SET name='$name' WHERE phrasename_id='$phrasename_id'");
    $f = 1;
    sqlQuery("DELETE FROM phrases WHERE phrasename=$phrasename_id");
    while(isset($_POST['lang'. $f]))
    {
      sqlQuery("INSERT INTO phrases (lang,phrasename,phrase) VALUES ('".$_POST['lang'. $f]."','$phrasename_id','".mb_convert_encoding($_POST['phrase'. $f], 'HTML-ENTITIES', 'UTF-8')."')");
      $f++;
    }

    Add_Log("Updated phrase '$name' ($phrase_id)", $site_id);

  }else if(($action == 'delphrase'))
  {
    $phrase_id = $_GET['id'];
    $name = sqlQueryValue("SELECT name FROM phrasenames WHERE phrasename_id=$phrasename_id");
    sqlQuery("DELETE FROM phrases WHERE phrasename='$phrase_id'");
    sqlQuery("DELETE FROM phrasenames WHERE name='$name'");
    Add_Log("Deleted phrase '$name' ($phrase_id)", $site_id);

  }
?>
