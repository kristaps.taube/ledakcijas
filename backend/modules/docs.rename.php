<?

$perm_managedocuments = $GLOBALS['perm_managedocuments'];
if(!$perm_managedocuments)
  AccessDenied(True);

//  print_r($_GET);
/*
$files = explode(";",$_GET['name']);
$i=0;
foreach ($files as $key=>$file)
{

$form_data["newfile".$i] = array(
    "label"     => "New name:",
    "size"      => "35",
    "type"      => "str",
    "value"	=> $file     
);

$form_data["oldfile".$i] = array(
    "type"      => "hidden",
    "value"	=> $file     
);
$i++;
*/

$file = explode(":",$_GET['name']);

if ($file[0]=="dir")
{
    $old_data = sqlQueryRow("SELECT dir_name, dir_description FROM ".$GLOBALS['site_id']."_documentdir WHERE dir_id=".$file[1]);
    $oldname = $old_data['dir_name'];
    $olddescription = $old_data['dir_description'];
}
else
{
    $old_data = sqlQueryRow("SELECT file_name, file_description FROM ".$GLOBALS['site_id']."_documents WHERE file_id=".$file[1]);
    $oldname = $old_data['file_name'];
    $olddescription = $old_data['file_description'];
}

$form_data = Array(

  "newfile"    => Array(
    "label"     => "New name:",
    "size"      => "35",
    "type"      => "str",
    "value"	=> $oldname
  ),

  "description" => Array(
    "label"     => "Description:",
    "type"      => "html",
    "rows"      => "4",
    "cols"      => "30",
    "value"	=> $olddescription
  ),

  "oldfile"     => Array(
    "type"      => "hidden",
    "value"	=> $oldname
  ),

  "type"     => Array(
    "type"      => "hidden",
    "value"	=> $file[0]
  ),

  "id"     => Array(
    "type"      => "hidden",
    "value"	=> $file[1]
  )
);

//}

require($GLOBALS['cfgDirRoot']."library/"."class.formgen.php");
$FormGen = new FormGen();
$FormGen->action = "?module=docs&action=dorename&site_id=".$GLOBALS['site_id']."&path=".$GLOBALS['path']."&view=".$GLOBALS['view'].($GLOBALS['close'] ? "&close=".$GLOBALS['close'] : "");
$FormGen->title = "Rename File";
$FormGen->cancel = ($GLOBALS['close'] ? "?module=files&action=list&site_id=".$GLOBALS['site_id']."&path=".$GLOBALS['path']."&view=".$GLOBALS['view'] : 'close');
$FormGen->properties = $form_data;
$FormGen->buttons = true;
echo $FormGen->output();


?>
