<?

function array_search_recursive( $needle, $haystack )
{
   $path = NULL;
   $keys = array_keys($haystack);
   while (!$path && (list($toss,$k)=each($keys))) {
     $v = $haystack[$k];
     if (is_scalar($v)) {
         if ($v===$needle && $k == 'item_id') {
           $path = array($k);
         }
     } elseif (is_array($v)) {
         if ($path=array_search_recursive( $needle, $v )) {
           array_unshift($path,$k);
         }
     }
   }
   return $path;
}

  function InitCollection($coltype, $colname, $categoryID)
  {
    //$coltype = $this->collectiontype;
    include_once($GLOBALS['cfgDirRoot'] . "collections/class." . $coltype . ".php");
    $colname = '';
    //$categoryID = $this->getProperty('collectionID');

    return new $coltype($colname,$categoryID);
  }


  if ($_GET['groupcategoryid']) 
  {
      $groupcategoryid = $_GET['groupcategoryid'];
      $collection = InitCollection("lbbproductsgroupscollection", "", $groupcategoryid);
      
      $groups = $collection->getDBData();
      /*echo '<pre>';
      print_R($groups);
      echo '</pre>';*/
  }

  $colname = $_GET['colname'];
  $site_id = $_GET['site_id'];  

  //$data = sqlQueryData("SELECT * FROM `".$site_id."_coltable_".$colname."` ORDER BY ind");
  //print_R($data);

  /*
  $sel = $_GET['sel'];
  if($sel == 'undefined') $sel = '';
  if($sel){
    $data = sqlQueryData("SELECT group_id, name_en, ELT((FIND_IN_SET(group_id,'$sel')>0)+1,'','checked') as checked FROM ".$site_id."_lbbgroups");
  }else{
    $data = sqlQueryData("SELECT group_id, name_en, '' as checked FROM ".$site_id."_lbbgroups");
  }
  */
  $sel = $_GET['sel'];
  if($sel == 'undefined') $sel = '';
  if($sel){
    $data = sqlQueryData("SELECT item_id, col1, ELT(col23+1, '', '(disabled)') AS status, ELT((FIND_IN_SET(item_id,'$sel')>0)+1,'','checked') as checked, col17 as 'group_id' FROM `".$site_id."_coltable_".$colname."` WHERE col18='0' OR col18='2'");
    $data_2 = sqlQueryData("SELECT item_id, col1, ELT(col23+1, '', '(disabled)') AS status, ELT((FIND_IN_SET(item_id,'$sel')>0)+1,'','checked') as checked, col17 as 'group_id' FROM `".$site_id."_coltable_".$colname."` WHERE col18='1'");
  }else{
    $data = sqlQueryData("SELECT item_id, col1, ELT(col23+1, '', '(disabled)') AS status, '' as checked, col17 as 'group_id' FROM `".$site_id."_coltable_".$colname."` WHERE col18='0' OR col18='2'");
    $data_2 = sqlQueryData("SELECT item_id, col1, ELT(col23+1, '', '(disabled)') AS status, '' as checked, col17 as 'group_id' FROM `".$site_id."_coltable_".$colname."` WHERE col18='1'");
  }

  $cols = Array(

  "group_id"    => Array(
    "width"     => "5%",
    "title"     => "#",
  ),

  "checkbox"    => Array(
    "width"     => "5%",
    "title"     => "",
    "format"    => "<input type=checkbox name=chbox value={%item_id%} {%checked%} OnClick=\"checkBoxes();\">"

  ),

  "col1"   => Array(
    "width"     => "90%",
    "title"     => "Products"
  )

);

//print_R($data);
foreach ($data as $key=>$row)
{
        $sorted_data_keys[$row['group_id']][] = $key;
}

foreach ($data_2 as $key=>$row)
{
        $sorted_data_2_keys[$row['group_id']][] = $key;
}

  echo '
  <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%" id="AutoNumber1" height="28">
  <tr>
    <td id="poga1" width="*" height="28" align="center" style="border-style: solid; border-width: 1; padding-left: 4; padding-right: 4; padding-top: 1; padding-bottom: 1; background-color: #C8C8C8; font-weight:bold;">
    <a href="#" style="text-decoration:none;" onclick="javascript:poga1.style.backgroundColor = \'#C8C8C8\'; poga2.style.backgroundColor = \'#F0F0F0\'; private.style.display=\'block\';corporate.style.display=\'none\';">Private products</a></td>
    <td width="8" height="28" align="center">&nbsp;</td>
    <td id="poga2" width="*" height="28" align="center" style="border-style: solid; border-width: 1; padding-left: 4; padding-right: 4; padding-top: 1; padding-bottom: 1; background-color: #F0F0F0">
    <a href="#" style="text-decoration:none;" onclick="javascript:poga2.style.backgroundColor = \'#C8C8C8\'; poga1.style.backgroundColor = \'#F0F0F0\'; private.style.display=\'none\';corporate.style.display=\'block\';">Corporate products</a></td>

  </tr>
</table>';


require($GLOBALS['cfgDirRoot']."library/"."class.datagrid.php");

echo '<div id="private" style="block:none;padding-left:5px;">';
/*
$Table = new DataGrid();
$Table->cols = $cols;
$Table->data = $data;
$Table->header = false;
$Table->footer = false;
echo $Table->output();
*/
//echo '<pre>';
//print_R($groups);
//echo '</pre>';
foreach ($sorted_data_keys as $group=>$products)
{
    //echo $group."<br>";
    $result = array_search_recursive(strval($group), $groups);
    echo '<image src="'.$GLOBALS['cfgWebRoot'].'gui/images/plus.gif" onclick="javascript:if(div'.$group.'.style.display==\'none\'){div'.$group.'.style.display=\'block\'; this.src=\''.$GLOBALS['cfgWebRoot'].'gui/images/minus.gif\'; }else{div'.$group.'.style.display=\'none\';this.src=\''.$GLOBALS['cfgWebRoot'].'gui/images/plus.gif\';}  " /> <span id="g'.$group.'">'.$groups[$result[0]]['col0'].'</span><br/>';
    echo '<div id="div'.$group.'" style="display:none;">';

    foreach ($products as $key)
    {
        if ($data[$key]['checked'])
        {
            echo '
            <script>
                if (g'.$group.'.style.fontWeight!="bold") g'.$group.'.style.fontWeight = "bold";
            </script>';
        }
        echo str_repeat('&nbsp;',5).'<input type=checkbox name=chbox value="'.$data[$key]['item_id'].'" '.$data[$key]['checked'].' OnClick="checkBoxes();"> '.$data[$key]['col1'].' '.$data[$key]['status'].'<br/>';
    }
    
    echo '</div>';
}
echo '</div>';


echo '<div id="corporate" style="display:none;padding-left:5px;">';
/*
$Table_2 = new DataGrid();
$Table_2->cols = $cols;
$Table_2->data = $data_2;
$Table_2->header = false;
$Table_2->footer = false;
echo $Table_2->output();
*/
//print_R($sorted_data_keys);
foreach ($sorted_data_2_keys as $group=>$products)
{
    //echo $products."<br>";
    $result = array_search_recursive(strval($group), $groups);
    echo '<image src="'.$GLOBALS['cfgWebRoot'].'gui/images/plus.gif" onclick="javascript:if(div_2'.$group.'.style.display==\'none\'){div_2'.$group.'.style.display=\'block\'; this.src=\''.$GLOBALS['cfgWebRoot'].'gui/images/minus.gif\'; }else{div_2'.$group.'.style.display=\'none\';this.src=\''.$GLOBALS['cfgWebRoot'].'gui/images/plus.gif\';}  " /> <span id="g_2'.$group.'">'.$groups[$result[0]]['col0'].'</span><br/>';
    echo '<div id="div_2'.$group.'" style="display:none;">';

    foreach ($products as $key)
    {
        if ($data_2[$key]['checked'])
        {
            echo '
            <script>
                if (g_2'.$group.'.style.fontWeight!="bold") g_2'.$group.'.style.fontWeight = "bold";
            </script>';
        }
        echo str_repeat('&nbsp;',5).'<input type=checkbox name=chbox value="'.$data_2[$key]['item_id'].'" '.$data_2[$key]['checked'].' OnClick="checkBoxes();"> '.$data_2[$key]['col1'].' '.$data_2[$key]['status'].'<br/>';
    }

    echo '</div>';
}
echo '</div>';
  
?>
