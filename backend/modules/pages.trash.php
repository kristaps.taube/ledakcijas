<?

$nodes_path = array();

//function for sorting data in tree structure
function Nodes($data, &$newdata, $parent, $level, $justcount, &$goodl, $perms, $canpublish)
{
  global $nodes_path;

  if (in_array($parent, $nodes_path)) return 0;
  $nodes_path[] = $parent;

  $goodlanguage = false;
  $nodecount = 0;
  foreach($data as $key => $row)
  {
    if($parent == $row['parent']){
      $nodecount++;

        $row['prefix'] = str_repeat('<img src="gui/images/1x1.gif" width="16" height="16" border="0" align="absmiddle">', $level) . '<img src="gui/images/1x1.gif" width="16" height="16" border="0" align="absmiddle">&nbsp;';
        $newdata[] = $row;
        $c = count($newdata) - 1;
        $childs = $row['childrencount'];
        Nodes($data, $newdata, $row['page_id'], $level + 1, 0, $g, $perms, $canpublish);
        $goodlanguage = ($g or $goodlanguage);
/*
        if(!$_SESSION['expanded' . $row['page_id']])
        {
          $newdata = array_slice($newdata, 0, $c+1);
        }
*/
        if($row['language'] != intval($GLOBALS['currentlanguagenum']))
        {
          //$newdata[$c]['page_id'] = 0;
          if(!$g)
          {
            if($c)
              $newdata = array_slice($newdata, 0, $c);
            else
              $newdata = array();
            $nodecount--;
          }else
          {
            $newdata[$c]['clicklink'] = "Page in " . sqlQueryValue("SELECT fullname FROM " . $GLOBALS['site_id'] . "_languages WHERE language_id = " . intval($row['language'])) . " (" . $row['name'] . ")";
            $newdata[$c]['page_id'] = 0;
            $newdata[$c]['pagedev_id'] = 0;
          }
        }else
        {
          if ($GLOBALS['perm_managepages'] || in_array($row['page_id'],$perms))
          {
            $newdata[$c]['clicklink'] = "<a href=\"#\" OnClick=\"javascript:openDialog('?module=pages&site_id=" . $GLOBALS['site_id'] . "&action=components_visual_frame&id=".$row['pagedev_id']."&page_id=".$row['page_id']."', screen.availWidth, screen.availHeight-25, 1 , 1);\">".ShortenString($row['title'],40)."</a>";

            //publish page if 'Can Publish All Pages' permission is set for current user
            if ($canpublish) $newdata[$c]['publish'] = '<a href="?module=pages&site_id='.$_GET['site_id'].'&action=publish&pagedev_id='.$row['pagedev_id'].'&page_id='.$row['page_id'].'">{%langstr:publish%}</a>';
            else $newdata[$c]['publish'] = '{%langstr:publish%}';
          }
          else
            $newdata[$c]['clicklink'] = ShortenString($row['title'],40);
          $goodlanguage = true;
        }
        if(($childs)and(isset($newdata[$c]))){
          if(1) //$_SESSION['expanded' . $row['page_id']])
          {
            $newdata[$c]['prefix'] = '&nbsp;'.str_repeat('<img src="gui/images/1x1.gif" width="16" height="16" border="0" style="vertical-align:middle">', $level) . '<a href="?module=pages&site_id=' . $GLOBALS['site_id'] . '&collapsebranch=' . $newdata[$c]['page_id'] . '&action=trash"><img src="gui/images/minus.gif" width="11" height="11" border="0" align="absmiddle" style="vertical-align: middle;"></a>&nbsp; ';
          }else
          {
            $newdata[$c]['prefix'] = '&nbsp;'.str_repeat('<img src="gui/images/1x1.gif" width="16" height="16" border="0" style="vertical-align:middle">', $level) . '<a href="?module=pages&site_id=' . $GLOBALS['site_id'] . '&expandbranch=' . $newdata[$c]['page_id'] . '&action=trash"><img src="gui/images/plus.gif" width="11" height="11" border="0" align="absmiddle" style="vertical-align: middle;"></a>&nbsp; ';
          }
        }

    }

  }
  $goodl = $goodlanguage;
  return $nodecount;
}


$site_id = $GLOBALS['site_id'];

// Let's try table generator

$cols = Array(

  "select"    => Array(
    "width"     => "1%",
    "title"     => "",
    "format"    => '<input type="radio" name="selectpage" value="{%pagedev_id%}" id="selectpage_{%pagedev_id%}" />'
  ),

  "title"        => Array(
    "width"     => "30%",
    "title"     => "{%langstr:title%}",
    "format"    => "{%prefix%}{%clicklink%}{%scut%}{%ishome%}{%islock%}"
  ),

  "description" => Array(
    "width"     => "40%",
    "title"     => "{%langstr:col_description%}"
  ),

  "modby" => Array(
    "width"     => "15%",
    "title"     => "{%langstr:modify_by%}"
  ),

  "publish"     => Array(
    "width"     => "5%",
    "align"     => "center",
    "title"     => "{%langstr:col_pub%}"
  ),

  "previewdev"     => Array(
    "width"     => "5%",
    "align"     => "center",
    "title"     => ""
  ),

);

if(CheckPermission(32, $site_id) && CheckPermission(33, $site_id)) //can publish and autopublish
{
  unset($cols['publish']);
  unset($cols['previewdev']);
}

if(!$perm_managepages)
{
  //unset($cols['title']['format']);
}


$javascriptv = ' onClick="javascript:SubmitPageVisibleCheckbox(this, \',pagedev_id,\',' . $site_id . ')"';
$javascripte = ' onClick="javascript:SubmitPageEnabledCheckbox(this, \',pagedev_id,\',' . $site_id . ')"';

if(!$perm_managepages)
{
  $javascriptv = '';
  $javascripte = '';
}
$checkstylev = 'CONCAT(\'<input type="checkbox" name="cccb"' . $javascriptv . ' checked>\')';
$uncheckstylev = 'CONCAT(\'<input type="checkbox" name="cccb"' . $javascriptv . '>\')';
$checkstylee = 'CONCAT(\'<input type="checkbox" name="cccb"' . $javascripte . ' checked>\')';
$uncheckstylee = 'CONCAT(\'<input type="checkbox" name="cccb"' . $javascripte . '>\')';

$shortcutimage = '<img src="gui/images/shortcut.gif" width="11" height="11" border="0" /> ';
$homeimage = '<img src="gui/images/home-11x11.gif" width="11" height="11" border="0" id="homeiconimg" /> ';
$lockimage = '<img src="gui/images/lock-11x11.gif" width="11" height="11" border="0" /> ';
$openedbr = '';
foreach($_SESSION as $key => $val)
{
  if(substr($key, 0, 8) == 'expanded')
  {
    $openedbr .= ',' . intval(substr($key, 8));
  }
}
$unsorteddata = sqlQueryData("
  SELECT
    in_trash, pagedev_id, page_id,
    IF(redirect>0 or redirect_url!='', CONCAT('&nbsp;', '$shortcutimage'), '') AS scut,
    IF(name='index', CONCAT('&nbsp;', '$homeimage'), '') AS ishome,
    IF(passprotect>0, CONCAT('&nbsp;', '$lockimage'), '') AS islock,
    name, title, description,
    ELT(visible+1,$uncheckstylev,$checkstylev) AS visible,
    ELT(p.enabled+1,$uncheckstylee,$checkstylee) AS enabled,
    parent, p.language, lastmod, username as modby, childrencount
  FROM " . $site_id . "_pagesdev as p
    LEFT JOIN users as u ON modby = u.user_id
  WHERE 1 ORDER BY ind
");
//   WHERE parent IN (0".$openedbr.") ORDER BY ind

//$unsorteddata = sqlQueryData("SELECT wappagedev_id, IF(redirect>0, CONCAT('$shortcutimage', '&nbsp;'), '') AS scut, name, IF(is_card>0,CONCAT('$cardimage', '&nbsp;'), '') as link, title, description, lastmod, ELT(visible+1,$uncheckstylev,$checkstylev) AS visible, ELT(w.enabled+1,$uncheckstylee,$checkstylee) AS enabled, parent, wappage_id, username as modby FROM " . $site_id . "_wappagesdev as w, users as u WHERE modby = u.user_id ORDER BY ind");

//get page_id array
$ids = sqlQueryData("select distinct page_id, ind from ".$site_id."_pagesdev ORDER BY ind");

//get keys of last modified dev pages
if (count($ids))
{
    $keys = array();
    for ($i=0; $i<=count($ids)-1; $i++)
    {
        $max = 0;
        foreach ($unsorteddata as $key=>$row)
        {
            //echo $row['page_id']." ".$ids[$i]['page_id']."<br>";
            if ($row['page_id']==$ids[$i]['page_id'])
            {
                if($row['lastmod']>$max)
                {
                    $max = $row['lastmod'];
                    $keys[$i] = $key;
                    $lastmoddev[$row['page_id']]['lastmod'] = $max;
                }
            }
        }
    }
}

//drop unnecessary rows
foreach ($unsorteddata as $key=>$row)
    if (!in_array($key,$keys))
        unset($unsorteddata[$key]);
$lastmod_data = sqlQueryData("SELECT page_id,lastmod FROM ".$site_id."_pages");
foreach ($lastmod_data as $row)
{
    $lastmod[$row['page_id']]['lastmod'] = $row['lastmod'];
}

$groups = sqlQueryDataAssoc("SELECT group_id from users_groups where user_id=".$GLOBALS['currentUserID']);
foreach ($groups as $group)
    $where .= " OR (perm_num=11 AND  isuser=0 AND id=".$group['group_id']." AND data=".$site_id." AND data2<>'')";

$perm_data = sqlQueryDataAssoc("SELECT data2 FROM permissions WHERE (perm_num=11 AND  isuser=1 AND id=".$GLOBALS['currentUserID']." AND data=".$site_id." AND data2<>'') ".$where);
//echo "SELECT data2 FROM permissions WHERE (perm_num=11 AND  isuser=1 AND id=".$GLOBALS['currentUserID']." AND data=".$site_id." AND data2<>'') ".$where;
if($perm_data && $perms)
{
    foreach ($perm_data as $row)
        if (!in_array($row['data2'],$perms))
            $perms[] = $row['data2'];
}
//print_r($perms);

$g = false;
Nodes($unsorteddata, $data, 0, 0, 0, $g, $perms, CheckPermission(33, $site_id));


$deleted_pages = array();

//check which pages to publish
foreach ($data as $key=>$row)
{
  if ($row['in_trash'])
  {
    $deleted_pages[] = $row['pagedev_id'];
  }


    if ($row['lastmod']==$lastmod[$row['page_id']]['lastmod'])
    {
        unset($data[$key]['publish']);
    }
    else
    {
        $data[$key]['previewdev'] = "<a href=\"?module=pages&site_id=$site_id&action=page_redirect&id=".$row['page_id']."&pagedev_id=".$row['pagedev_id']."&showdevpage=1\"  target=\"_blank\"><img src=\"gui/images/ico_view.gif\" width=\"16\" height=\"16\" border=\"0\"></a>";
    }
}

$pages = array();
foreach ($data as $key => $row)
{
  $pages[$row['page_id']] = &$data[$key];
}

foreach ($data as $key => $row)
{
  if ($row['in_trash'])
  {
    $data[$key]['show_in_tree'] = true;

    $parent = $row['parent'];

    if ($pages[$parent]['in_trash'])
    {
      $p = array_search($row['pagedev_id'], $deleted_pages);

      if ($p !== false)
        array_splice($deleted_pages, $p, 1);

    }

    while ($parent)
    {
      $pages[$parent]['show_in_tree'] = true;
      $parent = $pages[$parent]['parent'];
    }
  }

}

foreach ($data as $key => $row)
{
  if (!$row['show_in_tree'])
    unset($data[$key]);

}


require($GLOBALS['cfgDirRoot']."library/"."class.datagrid.php");

$Table = new DataGrid();
$Table->cols = $cols;
$Table->data = $data;
$Table->footer = false;
$Table->rowformat = " id=\"tablerow{%pagedev_id%}\" onClick=\"onRowClick(this, {%pagedev_id%})\" oncontextmenu=\"onRowContextMenu(this, {%pagedev_id%})\" style='background:#e8ffe8' ";
echo $Table->output();

if($_GET['accessdenied'])
{
  AccessDenied(false, true);
  echo '
  <script language="JavaScript" type="text/javascript">
  /*<![CDATA[*/
  window.document.location.href = "?module=pages&site_id=' . $GLOBALS['site_id'] . '&selrow=' . $_GET['selrow'] . '"
  /*]]>*/
  </script>
  ';

}

echo '<script language="JavaScript" type="text/javascript">
/*<![CDATA[*/
che = new Array("del", "restore");
che2 = new Array();

DisableToolbarButtons(che);
/*]]>*/
</script>';

if($_GET['selrow'])
{
  echo '<script language="JavaScript">
        <!--
          var tablerow' . $_GET['selrow'] . ' = document.getElementById("tablerow' . $_GET['selrow'] . '");
          if(typeof(tablerow' . $_GET['selrow'] . ')!= "undefined" && tablerow' . $_GET['selrow'] . ')
          {
            preEl = tablerow' . $_GET['selrow'] . ';
            ChangeTextColor(tablerow' . $_GET['selrow'] . ',\'tableRowSel\');
            SelectedRowID = ' . $_GET['selrow'] . ';
            if(SelectedRowID){ document.getElementById(\'selectpage_' . $_GET['selrow'] . '\').checked = true; EnableToolbarButtons(che); }else { DisableToolbarButtons(che); }
          }
        <!---->
        </script>';
}
?>
  <div id="debug"></div>
  <script type="text/javascript">

  var deleted_pages = <?=json_encode($deleted_pages) ?>;
  var actions_enabled = false;

  function toggleActions(enabled)
  {
    if (enabled)
      EnableToolbarButtons(che);
    else
      DisableToolbarButtons(che);


    actions_enabled = enabled;
  }

  $(function()
  {
    $.each(deleted_pages, function(i, id)
    {
      $('#tablerow'+id).css('backgroundColor', '#ffe8e8');
    });

  });

  function trashHighLightTR(el, ID,multiple,butt)
  {
    HighLightTR(el, ID, multiple, butt);
  }

  function getSelectedRowIDs()
  {
    if (SelectedRowID != -1)
      return [SelectedRowID];


    var ids = [];
    for (var i=0; i<SelectedRowIDs.length; i++)
    {
      if (SelectedRowIDs[i] == undefined || SelectedRowIDs[i] == 0) continue;

      ids.push(SelectedRowIDs[i]);
    }

    return ids;
  }

  function onRowClick(row, pagedev_id)
  {
    trashHighLightTR(row, pagedev_id, 1);

    var ids = getSelectedRowIDs();

    var enable = true;
    for (var i=0; i<ids.length; i++)
    {
      if (deleted_pages.indexOf(String(ids[i])) == -1)
      {
        enable = false;
        break;
      }
    }

    toggleActions(enable);


    if (SelectedRowID)
      document.getElementById('selectpage_'+pagedev_id).checked = true;

  }

  function onRowContextMenu(row, pagedev_id)
  {
    trashHighLightTR(row, pagedev_id, 1);
    if (SelectedRowID)
      document.getElementById('selectpage_'+pagedev_id).checked = true;

    return false;
  }

  function restoreClick()
  {
    if (!actions_enabled) return false;

    var ids = getSelectedRowIDs();
    if (ids.length == 0) return false;

    window.location = '?module=pages&site_id=<?=$GLOBALS["site_id"] ?>&action=trash_restore&ids='+ids.join(',');
    return false;
  }

  function deleteClick()
  {
    if (!actions_enabled) return false;

    var ids = getSelectedRowIDs();
    if (ids.length == 0) return false;

    window.location = '?module=pages&site_id=<?=$GLOBALS["site_id"] ?>&action=trash_delete&ids='+ids.join(',');
    return false;
  }

  </script>
<?

?>