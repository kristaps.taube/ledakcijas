<?
    $site_id = $_GET['site_id'];

    $uploaddir = sqlQueryValue("select dirroot from sites where site_id=".$site_id);
    $uploaddir .= "/images/";
    if (!is_dir($uploaddir)) mkdir($uploaddir);
    if (!is_dir($uploaddir."/big")) mkdir($uploaddir."/big");
    if (!is_dir($uploaddir."/small")) mkdir($uploaddir."/small");

    preg_match("/\.\w+$/",$_FILES['image']['name'],$ext);
    $name = basename($_FILES['image']['name'], $ext[0]);

    while(is_file($uploaddir."/big/".$name.$ext[0]) || is_file($uploaddir."/small/".$name.$ext[0])) {
        $name = basename($_FILES['image']['name'], $ext[0]) . rand();
    }
    $name .= $ext[0];

    $new_name = $name;
    $uploadfile1 = $uploaddir . "/big/" .$new_name;
    $uploadfile2 = $uploaddir . "/small/" .$new_name;

    if (move_uploaded_file($_FILES['image']['tmp_name'], $uploadfile1)) {

        //CREATE THUMB
        //--------------------------------------------------
        if ($_FILES['image']["type"] == "image/gif")
            $abc = imagecreatefromGIF($uploadfile1); //UPLOADED PIC FILE
        else if ($_FILES['image']["type"] == "image/jpeg" || $_FILES['image']["type"] == "image/pjpeg")
            $abc = imagecreatefromjpeg($uploadfile1); //UPLOADED PIC FILE
        else if ($_FILES['image']["type"] == "image/x-png")
            $abc = imagecreatefrompng($uploadfile1); //UPLOADED PIC FILE

        $thumbwidth = $_POST['thumbwidth'];
        if (!$thumbwidth) $thumbwidth = 350;

        $new_h = round(ImageSY($abc) * ($thumbwidth/ImageSX($abc)));

        $def = @imagecreatetruecolor($thumbwidth, $new_h) or die("Cannot Initialize new GD image stream");

        imagecopyresampled($def, $abc, 0, 0, 0, 0, $thumbwidth, $new_h, ImageSX($abc), ImageSY($abc));

        imagejpeg($def, $uploadfile2, 100);  //new file from $def

        ImageDestroy($def);
        ImageDestroy($abc);

        $GLOBALS['lightningsmall'] = $uploadfile2;

        //resize original image if maxwidth defined
        $maxwidth = $_POST['width'];
        if ($maxwidth)
        {
            if ($_FILES['image']["type"] == "image/gif")
                $abc = imagecreatefromGIF($uploadfile1); //UPLOADED PIC FILE
            else if ($_FILES['image']["type"] == "image/jpeg" || $_FILES['image']["type"] == "image/pjpeg")
                $abc = imagecreatefromjpeg($uploadfile1); //UPLOADED PIC FILE
            else if ($_FILES['image']["type"] == "image/x-png")
                $abc = imagecreatefrompng($uploadfile1); //UPLOADED PIC FILE

            if (ImageSX($abc)>$maxwidth)
            {
                $new_h = round(ImageSY($abc) * ($maxwidth/ImageSX($abc)));

                $def = @imagecreatetruecolor($maxwidth, $new_h) or die("Cannot Initialize new GD image stream");

                //imagecopyresized($def, $abc, 0, 0, 0, 0, $maxwidth, $new_h, ImageSX($abc), ImageSY($abc));
                imagecopyresampled($def, $abc, 0, 0, 0, 0, $maxwidth, $new_h, ImageSX($abc), ImageSY($abc));

                ImageDestroy($abc);
                imagejpeg($def, $uploadfile1, 100);  //new file from $def

                ImageDestroy($def);

                $GLOBALS['lightningbig'] = $uploadfile1;
            }else
            {
              ImageDestroy($abc);
            }
        }

        $GLOBALS['lightningname'] = $new_name;

        //--------------------------------------------------


        print "File is valid, and was successfully uploaded. ";
    }
?>