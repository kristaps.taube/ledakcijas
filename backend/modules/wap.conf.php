<?
$action = $_GET["action"];
$GLOBALS['site_id'] = $_GET["site_id"];
$site_id = $_GET["site_id"];
$GLOBALS['tabs'] = 'site';
//check site license
$domain = $GLOBALS['domain'];
if(!CheckLicense($domain))
{
  RedirectNoLicense();
}
unset($perm_modsite);
if(($site_id == $GLOBALS["currentUserSiteID"])or(!$GLOBALS["currentUserSiteID"]))
  $perm_modsite = CheckPermission(6, $site_id);
if((!$perm_modsite)and($action!="simple"))
  AccessDenied(False);

unset($perm_managewappages);
if(($site_id == $GLOBALS["currentUserSiteID"])or(!$GLOBALS["currentUserSiteID"]))
  $perm_managewappages = CheckPermission(19, $site_id);

if ($action=="simple" || $action=="") {
  if (isset($_GET["collapsebranch"])) $_SESSION['wapexpanded' . $_GET['collapsebranch']] = 0;
  if (isset($_GET["expandbranch"])) $_SESSION['wapexpanded' . $_GET['expandbranch']] = 1;
}

$GLOBALS['PageEncoding'] = PageEncoding($id, $site_id);
$GLOBALS['maincharset']  = PageEncoding($id, $site_id, false);

$docScripts['sellanguage'] = "sellanguage.default.php";
include("sellanguage.setlanguage.php");
$GLOBALS['havelanguages'] = HaveLanguages($site_id);
$GLOBALS['PageEncoding'] = FullEncoding($GLOBALS['currentlanguage']);
$GLOBALS['maincharset']  = $GLOBALS['currentlanguage'];


switch ($action) {

  case "create":
    $docTemplate = "form.htm";
    $GLOBALS['resizeauto'] = 1;
    $docTitle = "{%langstr:create_new_wap_page%}";
    $docScripts['body'] = "wap.properties.php";
    break;

  // site properties form
  case "properties_form":
    $docTemplate = "form.htm";
    $GLOBALS['resizeauto'] = 1;
    $docScripts['body'] = "wap.properties.php";
    break;

  // site properties form
  case "history_form":
    $docTemplate = "form.htm";
    $GLOBALS['resizeauto'] = 1;
    $docScripts['body'] = "wap.history.php";
    break;


  //for displaying all components in the page
  case "components_form_frame":
    $docTemplate = "form.htm";
    $GLOBALS['resizeauto'] = 0;
    if ($_GET['component'])
        $docScripts['body'] = "wap.component_contenteditor.php";
    else
        $docScripts['body'] = "wap.components.php";
    //check if permitted to edit page
    if($_GET['id']>0)
      $perm_managecontents = CheckPermission(20, $site_id, $_GET['id']);
    else
      $perm_managecontents = CheckPermission(19, $site_id);
    //$perm_managecontents = 1;
    break;

  //for displaying all components in the page
/*  case "components_form":
          $docTemplate = "lefttree_components.html";
          $docScripts['body'] = "pages.tree_components.php";
    //$docTemplate = "form.htm";
    $GLOBALS['resizeauto'] = 0;
    //$docScripts['body'] = "pages.components.php";
    //check if permitted to edit page
    if($_GET['id']>0)
      $perm_managecontents = CheckPermission(11, $site_id, $_GET['id']);
    else
      $perm_managecontents = CheckPermission(9, $site_id);
    break;
*/

  case "components_form":
    $docTemplate = "frameset2.htm";
    $domain = $GLOBALS['domain'];
    $url = parse_url($_SERVER['REQUEST_URI']);
    $url['query'] = str_replace("components_visual_frame", "components_graph", $url['query']);
    $docStrings['rightframesrc'] = "?module=wap&site_id=" . $GLOBALS['site_id'] . "&wappagedev_id=" . $_GET['id'] ."&action=components_form_frame&id=".$_GET['id'];
    $docStrings['leftframesrc'] = "?module=pageframe_components&site_id=" . $GLOBALS['site_id'] . "&page_id=" . $_GET['id'] . "&wap=1";
    break;


  //for displaying a single component
  case "component_form":
    $docTitle = "{%langstr:content_editor%}";
    $docTemplate = "dlg_cform.htm";
    $docStrings['frame'] = '?module=wap&site_id='.$_GET['site_id'].'&id='.$_GET['id'].'&action=component_frame&component_name='.$_GET['component_name'].'&component_type='.$_GET['component_type'];
    $docStrings['celement_selector'] = createComponentCombobox($_GET['site_id'],$_GET['id'],$_GET['component_name']);
    break;

  case "component_frame":
    $docTemplate = "form.htm";
    $docScripts['body'] = "pages.component.php";
    if($_GET['id']>0)
      $perm_managecontents = CheckPermission(20, $site_id, $_GET['id']);
    else
      $perm_managecontents = CheckPermission(19, $site_id);
    break;

  case "notemplates":
    $docTemplate = "form.htm";
    $docScripts['body'] = "wap.notemplates.php";
    break;
    
  case "page_redirect":
    $docScripts['exec'] = "wap.exec.php";
    break;

  case "close":
    $docTemplate = "close.htm";
    break;

  case "updatecontrols":
    $docTemplate = "close.htm";
    //$docTemplate = "form.htm";
    //print_r($_POST);
    $docScripts['exec'] = "wap.exec.php";
    if($_POST['wappagedev_id'])
      $docStrings['urlend'] = "&selrow=" . $_POST['wappagedev_id'];
    if(($action == "updatecontrols")or($action == "updatecontrolsdef"))
    {
      if($_GET['id']>0)
        $perm_managecontents = CheckPermission(20, $site_id, $_GET['id']);
      else
        $perm_managecontents = CheckPermission(19, $site_id);
    }
    //$perm_managecontents = 1;
    $GLOBALS['perm_managecontents'] = $perm_managecontents;
    break;
  case "componentvisibility":
    $docTitle = "Set Component Visibility";
    $docTemplate = "form.htm";
    $docScripts['body'] = "pages.cvisibility.php";
    $perm_managecontents = CheckPermission(20, $site_id, $_GET['id']);
    break;
  case "modpage":
  case "addpage":
  case "tbarupdatecontrols":
  case "updatecontrolsdef":
    $docTemplate = "close.htm";
    //$docTemplate = "form.htm";
    $docScripts['exec'] = "wap.exec.php";
    if($_POST['wappagedev_id'])
      $docStrings['urlend'] = "&selrow=" . $_POST['wappagedev_id'];
    if(($action == "updatecontrols")or($action == "updatecontrolsdef")or($action == "tbarupdatecontrols"))
    {
      if ($_GET['id']>0) $page_id = $_GET['id'];
      else if ($_GET['wappage_id']>0) $wappage_id = $_GET['wappage_id'];
      
      if($wappage_id>0)
        $perm_managecontents = CheckPermission(20, $site_id, $wappage_id);
      else
        $perm_managecontents = CheckPermission(19, $site_id);
      
    }
    break;

  

  case "delpage":
    $docScripts['exec'] = "wap.exec.php";
    break;

  case "moveup":
  case "movedown":
  case "unsetpageenabled":
  case "setpageenabled":
  case "unsetpagevisible":
  case "setpagevisible":
    $docScripts['exec'] = "wap.exec.php";
    Header("Location: ?module=wap&site_id=" . $GLOBALS['site_id'] . '&selrow=' . $_GET['selrow']);
    break;

  case "simple":
    $docTemplate = "form.htm";
    $GLOBALS['resizeauto'] = 0;
    $docTitle = "Pages";
    $docScripts['body'] = "wap.simple.php";
    break;

  case "publish":
    $docScripts['exec'] = "wap.exec.php";
    Header("Location: ?module=wap&site_id=" . $GLOBALS['site_id'] . '&selrow=' . $_GET['selrow']);
    break;

  case "restore_history":
    $docTemplate = "close.htm";
    $docScripts['exec'] = "wap.exec.php";
    break;


  default:

    if($perm_managewappages)
    {
      $havestrings = $data = sqlQueryValue("SELECT Count(1) FROM " . $site_id . "_strings WHERE page=-1");
      /*require($GLOBALS['cfgDirRoot']."library/"."class.toolbar.php");
      $Toolbar = new Toolbar();
      $Toolbar->AddButton("add", "New WAP Page", "add_page1.gif", "javascript:openDialog('?module=wap&site_id=" . $GLOBALS['site_id'] . "&action=create&sibiling='+SelectedRowID, 400, 550, 1, 0);", "Add new page to the selected branch");
      $Toolbar->AddButton("add2", "New WAP Subpage", "add_page2.gif", "javascript:if(SelectedRowID)openDialog('?module=wap&site_id=" . $GLOBALS['site_id'] . "&action=create&parent='+SelectedRowID, 400, 550, 1, 0);", "Add new page as subbranch to the selected branch");
      $Toolbar->AddButton("properties", "Properties", "edit_properties.gif", "javascript:if(SelectedRowID)openDialog('?module=wap&site_id=" . $GLOBALS['site_id'] . "&action=properties_form&id='+SelectedRowID, 400, 550, 1, 0);", "Change page properties");
      $Toolbar->AddButton("perms", "Permissions", "edit_permissions.gif", "javascript:if(SelectedRowID)openDialog('?module=permissions&action=permission_frame&mod=wappage_form&close=1&site_id=$site_id&data=$site_id&data2='+SelectedRowID, 800, 600, 0, 1);", "Edit selected page access permissions");

      $Toolbar->AddButton("sedit", "Standard Edit", "edit_page1.gif", "javascript:if(SelectedRowID)openDialog('?module=wap&site_id=" . $GLOBALS['site_id'] . "&action=components_form&id='+SelectedRowID, 800, 600, 1, 1);", "Edit page in standard editor");


      $Toolbar->AddLink("up", "Up", "move_up.gif", "javascript:if(SelectedRowID)window.location='?module=wap&site_id=" . $GLOBALS['site_id'] . "&action=moveup&selrow='+SelectedRowID+'&id='+SelectedRowID;", "");
      $Toolbar->AddLink("down", "Down", "move_down.gif", "javascript:if(SelectedRowID)window.location='?module=wap&site_id=" . $GLOBALS['site_id'] . "&action=movedown&selrow='+SelectedRowID+'&id='+SelectedRowID;", "");
      $Toolbar->AddLink("del", "Delete", "delete.gif", "javascript:if(SelectedRowID)window.location='?module=wap&site_id=" . $GLOBALS['site_id'] . "&action=delpage&id='+SelectedRowID;", "Delete selected page", "Are you sure you want to delete selected page?");

      $Toolbar->AddButton("history", "History", "edit_properties.gif", "javascript:if(SelectedRowID)openDialog('?module=wap&site_id=" . $GLOBALS['site_id'] . "&action=history_form&id='+SelectedRowID, 400, 550, 1, 0);", "View page history");

      $docStrings['toolbar'] = $Toolbar->output(); */

      require($GLOBALS['cfgDirRoot']."library/"."class.toolbar2.php");
      $Toolbar2 = new Toolbar2();

      $Toolbar2->AddSpacer();

      $Toolbar2->AddButtonImageText('add', 'wap_new_page', '{%langstr:toolbar_new_wap_page%}', "", "javascript:openDialog('?module=wap&site_id=" . $GLOBALS['site_id'] . "&action=create&sibiling='+SelectedRowID, 400, 550, 1, 0);", 115, "{%langstr:hint_new_wap_page%}");
      $Toolbar2->AddButtonImageText('add2', 'wap_new_subpage', '{%langstr:new_wap_subpage%}', "", "javascript:if(SelectedRowID)openDialog('?module=wap&site_id=" . $GLOBALS['site_id'] . "&action=create&parent='+SelectedRowID, 400, 550, 1, 0);", 130, "{%langstr:add_new_wap_page_subbranch%}");
      $Toolbar2->AddButtonImage('properties', 'wap_properties', "{%langstr:toolbar_properties%}", "", "javascript:if(SelectedRowID)openDialog('?module=wap&site_id=" . $GLOBALS['site_id'] . "&action=properties_form&id='+SelectedRowID, 400, 550, 1, 0);", 31, "{%langstr:hint_properties_wap%}");

      $Toolbar2->AddSeperator();

      $Toolbar2->AddButtonImageText('sedit', 'wap_standart_edit', "{%langstr:col_edit%}", "", "javascript:if(SelectedRowID)openDialog('?module=wap&site_id=" . $GLOBALS['site_id'] . "&action=components_form&id='+SelectedRowID, 800, 600, 1, 1);", 40, "{%langstr:hint_wap_edit%}");

      $Toolbar2->AddSeperator();

      $Toolbar2->AddButtonImage('perms', 'wap_permissions', "{%langstr:toolbar_permissions%}", "", "javascript:if(SelectedRowID)openDialog('?module=permissions&action=permission_frame&mod=wappage_form&close=1&site_id=$site_id&data=$site_id&data2='+SelectedRowID, 800, 600, 0, 1);", 31, "{%langstr:toolbar_hint_page_permissions%}");
      $Toolbar2->AddButtonImage('history', 'wap_history', "{%langstr:toolbar_history%}", "", "javascript:if(SelectedRowID)openDialog('?module=wap&site_id=" . $GLOBALS['site_id'] . "&action=history_form&id='+SelectedRowID, 400, 550, 1, 0);", 31, "{%langstr:hint_wap_page_history%}");

      $Toolbar2->AddSeperator();

      $Toolbar2->AddButtonImage('up', 'up', "{%langstr:toolbar_up%}", "javascript:if(SelectedRowID)window.location='?module=wap&site_id=" . $GLOBALS['site_id'] . "&action=moveup&selrow='+SelectedRowID+'&id='+SelectedRowID;", "", 31, "{%langstr:toolbar_hint_page_up%}");
      $Toolbar2->AddButtonImage('down', 'down', "{%langstr:toolbar_down%}", "javascript:if(SelectedRowID)window.location='?module=wap&site_id=" . $GLOBALS['site_id'] . "&action=movedown&selrow='+SelectedRowID+'&id='+SelectedRowID;", "", 31, "{%langstr:toolbar_hint_page_down%}");
      $Toolbar2->AddButtonImage('del', 'delete', "{%langstr:toolbar_delete%}", "javascript:if(SelectedRowID)window.location='?module=wap&site_id=" . $GLOBALS['site_id'] . "&action=delpage&id='+SelectedRowID;", "", 31, "{%langstr:toolbar_hint_page_delete%}", "{%langstr:toolbar_ask_page_delete%}");
      $Toolbar2->AddSeperator();

      $docStrings['toolbar'] = $Toolbar2->output();
    }

    $docTemplate = "main.htm";
    $docTitle = "{%langstr:stats_pages%}";
    $docScripts['body'] = "wap.default.php";
    break;

}

?>
