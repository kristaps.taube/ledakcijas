<?

$sitetabs = array();
if($GLOBALS['user_site_permissions_modifysite'])
{
  $row = sqlQueryRow("select domain,wapsite from sites where site_id=".$GLOBALS['site_id']);

  //if not wapsite, then show regular sitetab control

      if ($GLOBALS['custommodule'][$row['domain']]) $sitetabs[] = Array("Home", $GLOBALS['custommodule'][$row['domain']], "?module=".$GLOBALS['custommodule'][$row['domain']]."&site_id=".$GLOBALS['site_id']."&cmodule=1");

      if(!$row['wapsite'])
      {
        if($GLOBALS['user_site_permissions_modifysite'])
        {
          $sitetabs[] = Array("{%langstr:menu_sitemap%}", "pages", "?module=pages&site_id=".$GLOBALS['site_id'], array());
        }
      }
      if($GLOBALS['user_site_permissions_modifysite']) {
        if ($GLOBALS['cfgMini'])
        {
          $sitetabs[] = Array("{%langstr:menu_skins%}", "skins", "?module=skins&site_id=".$GLOBALS['site_id'], Array(
            Array("{%langstr:menu_skins_list%}", "list", "?module=skins&site_id=".$GLOBALS['site_id']."&action=list"),
            Array("{%langstr:menu_skins_download%}", "downloadlist", "?module=skins&site_id=".$GLOBALS['site_id']."&action=downloadlist")
          ));
        }
      }
      if($GLOBALS['user_site_permissions_managewappages']) $sitetabs[] = Array("{%langstr:menu_wap%}", "wap", "?module=wap&site_id=".$GLOBALS['site_id']);
      if (!$GLOBALS['cfgMini'])
        if($GLOBALS['user_site_permissions_modifysite']) $sitetabs[] = Array("{%langstr:menu_lists%}", "lists", "?module=lists&site_id=".$GLOBALS['site_id']);
      if($GLOBALS['module']== 'collections')
      {
        $menu_collections = Array();
        $visible_cols = sqlQueryData('SELECT * FROM `lists` WHERE collection_id > 0 AND show_in_menu = 1 AND site_id = ' . $GLOBALS['site_id']);
        if($visible_cols)
        {
          foreach($visible_cols as $item)
          {
            $menu_collections[] = Array($item['title'], Array('id', $item['collection_id']), '?module=collections&site_id='.$GLOBALS['site_id'].'&action=editcollection&id='.$item['collection_id'].'&justedit=1');
          }
        }
      }else
      {
        $menu_collections = Array();
      }
      if($GLOBALS['user_site_permissions_managecollections']) $sitetabs[] = Array("{%langstr:menu_collections%}", "collections", "?module=collections&site_id=".$GLOBALS['site_id'],
        $menu_collections
      );
      if(!$row['wapsite'])
        if($GLOBALS['user_site_permissions_managetemplates']) $sitetabs[] = Array("{%langstr:menu_templates%}", "templates", "?module=templates&site_id=".$GLOBALS['site_id']);

      if($GLOBALS['user_site_permissions_managewaptemplates']) $sitetabs[] = Array("{%langstr:menu_wap_templates%}", "waptemplates", "?module=waptemplates&site_id=".$GLOBALS['site_id']);

//      if($GLOBALS['user_site_permissions_managetemplates']) $sitetabs[] = Array("{%langstr:menu_strings%}", "strings", "?module=strings&site_id=".$GLOBALS['site_id']);

      if(optionExists("seo\\api_access_key") && readOption("seo\\api_access_key") != '')
      {
        ?>
        <form id='seo_form' action="http://admin.seoportal.eu/auth/" method="post" target="newWin" onsubmit="newWindow()">
          <input type='hidden' name='api_access_key' value="<?=Option("seo\\api_access_key")?>" />
        </form>
        <script type='text/javascript'>

          function newWindow()
          {
              window.open('', "newWin", "scrollbars=0,menubar=0,toolbar=0,location=0,status=0");
          }




          $(document).ready(function(){

            $("#seo, #seoactive").click(function(){

              $("#seo_form").trigger("submit");
              return false;

            });

          });
        </script>
        <?
      }

      $sitetabs[] = Array("{%langstr:system_links%}", "sys_links", "?module=systemlinks&site_id=".$GLOBALS['site_id']);
      $sitetabs[] = Array("{%langstr:seo_system%}", "seo", "?module=seo&site_id=".$GLOBALS['site_id']);

      if($GLOBALS['user_site_permissions_manageusers']) $sitetabs[] = Array("{%langstr:menu_users%}", "users", "?module=users&site_id=".$GLOBALS['site_id']);
      if($GLOBALS['user_site_permissions_manageusers']) $sitetabs[] = Array("{%langstr:menu_groups%}", "groups", "?module=groups&site_id=".$GLOBALS['site_id']);
      if($GLOBALS['user_site_permissions_managetemplates']) $sitetabs[] = Array("{%langstr:menu_languages%}", "languages", "?module=languages&site_id=".$GLOBALS['site_id']);
      if($GLOBALS['user_site_permissions_managefiles']) $sitetabs[] = Array("{%langstr:menu_files%}", "files", "?module=files&site_id=".$GLOBALS['site_id']);
      if($GLOBALS['user_site_permissions_modifysite'] && $GLOBALS['user_site_permissions_admin'])
      {
          $sitetabs[] = Array("{%langstr:menu_administration%}", "admin", "?module=admin&site_id=".$GLOBALS['site_id'], array(
            array("{%langstr:menu_view_site_activity_log%}", "log", "?module=admin&action=log&site_id=".$GLOBALS['site_id']),
//            array("{%langstr:menu_restore_site_from_backup%}", "browsebackups", "?module=admin&action=browsebackups&site_id=".$GLOBALS['site_id']),
/*            array("{%langstr:menu_create_site_backup%}", "database", "?module=admin&action=database&site_id=".$GLOBALS['site_id']), */
            array("{%langstr:menu_edit_constructor_phrases%}", "langstrings", "?module=admin&action=langstrings&site_id=".$GLOBALS['site_id']),
//            array("Index pages for site search", "reindex", "javascript:openDialog('?module=admin&amp;action=reindex&amp;close=0&amp;site_id=".$GLOBALS['site_id']."&amp;page_id=0', 400, 100);void(0);"),
//            array("Modify access permissions", "permission_frame", "javascript:openDialog('?module=permissions&amp;action=permission_frame&amp;mod=site_form&amp;hideitems=1&amp;close=1&amp;site_id=".$GLOBALS['site_id']."&amp;data=".$GLOBALS['site_id']."&amp;data2=', 800, 600, 0, 1);void(0);")
          )
          );
      }
      if($GLOBALS['user_site_permissions_modifysite'])
      {
        $sitetabs[] = Array("{%langstr:menu_translations%}", "languagestrings", "?module=languagestrings&site_id=".$GLOBALS['site_id']);

        if($GLOBALS['user_site_permissions_admin'])
          $sitetabs[] = Array("{%langstr:menu_options%}", "options", "?module=options&site_id=".$GLOBALS['site_id']);

      }

      if($GLOBALS['user_site_permissions_stats'] && $GLOBALS['user_site_permissions_modifysite'])
      {
          $submenu[] = array("{%langstr:menu_pages%}", "0", "?module=stats&site_id=".$GLOBALS['site_id']."&action=0&period=".$_SESSION['period']."&page=0");
          $submenu[] = array("{%langstr:menu_unvisited_pages%}", "9", "?module=stats&site_id=".$GLOBALS['site_id']."&action=9&period=".$_SESSION['period']."&page=9");
          $submenu[] = array("{%langstr:menu_referrers%}", "1", "?module=stats&site_id=".$GLOBALS['site_id']."&action=1&period=".$_SESSION['period']."&page=1");
          $submenu[] = array("{%langstr:menu_keywords%}", "12", "?module=stats&site_id=".$GLOBALS['site_id']."&action=12&period=".$_SESSION['period']."&page=12");
          $submenu[] = array("{%langstr:menu_languages%}", "2", "?module=stats&site_id=".$GLOBALS['site_id']."&action=2&period=".$_SESSION['period']."&page=2");
          $submenu[] = array("{%langstr:menu_user_agent%} ", "3", "?module=stats&site_id=".$GLOBALS['site_id']."&action=3&period=".$_SESSION['period']."&page=3");
          $submenu[] = array("{%langstr:menu_os%}", "4", "?module=stats&site_id=".$GLOBALS['site_id']."&action=4&period=".$_SESSION['period']."&page=4");
          $submenu[] = array("{%langstr:menu_browsers%}", "5", "?module=stats&site_id=".$GLOBALS['site_id']."&action=5&period=".$_SESSION['period']."&page=5");
          $submenu[] = array("{%langstr:menu_proxies%}", "6", "?module=stats&site_id=".$GLOBALS['site_id']."&action=6&period=".$_SESSION['period']."&page=6");
          $submenu[] = array("{%langstr:menu_visitors%}", "7", "?module=stats&site_id=".$GLOBALS['site_id']."&action=7&period=".$_SESSION['period']."&page=7");

          if (!$GLOBALS['cfgStatiscticsPaths'])
              $submenu[] = array("{%langstr:menu_paths%}", "10", "?module=stats&site_id=".$GLOBALS['site_id']."&action=10&period=".$_SESSION['period']."&page=10");

          $submenu[] = array("{%langstr:menu_graphs%}", "8", "?module=stats&site_id=".$GLOBALS['site_id']."&action=8&period=".$_SESSION['period']."&page=8");
//          $submenu[] = array("Flash Player Version", "11", "?module=stats&site_id=".$GLOBALS['site_id']."&action=11&period=".$_SESSION['period']."&page=11");

          if($GLOBALS['user_site_permissions_stats'] || $GLOBALS['user_site_permissions_modifysite']) $sitetabs[] = Array("{%langstr:menu_statistics%}", "stats", "?module=stats&site_id=".$GLOBALS['site_id'], $submenu);
      }

      if(!$row['wapsite'])
        if($GLOBALS['user_site_permissions_managedocuments']) $sitetabs[] = Array("{%langstr:menu_documents%}", "docs", "?module=docs&site_id=".$GLOBALS['site_id']);

      if($GLOBALS['cfgMini'])
      {
        $sitetabs[] = Array("{%langstr:menu_upgrade%}", "upgrade", "?module=upgrade&site_id=".$GLOBALS['site_id']);
      }
  }




for($f=0;$f<count($sitetabs);$f++)
{
  /*if($f == 0)
    echo '<td><img border="0" src="gui/images/tab_first'.IIF($GLOBALS['module']==$sitetabs[$f][1],'_sel','').'.gif" width="26" height="24"></td>';
  else
    echo '<td><img border="0" src="gui/images/tab_mid'.IIF($GLOBALS['module']==$sitetabs[$f-1][1],'_left','').''.IIF($GLOBALS['module']==$sitetabs[$f][1],'_right','').'.gif" width="26" height="24"></td>';
  echo '<td class="'.IIF($GLOBALS['module']==$sitetabs[$f][1],'tabcellsel','tabcell').'">';
  echo '  <a href="'.$sitetabs[$f][2].'" class="'.IIF($GLOBALS['module']==$sitetabs[$f][1],'tabtextsel','tabtext').'">'.$sitetabs[$f][0].'</a>';
  echo '</td>   ';
  if($f == count($sitetabs) - 1)
    echo '<td><img border="0" src="gui/images/tab_last'.IIF($GLOBALS['module']==$sitetabs[$f][1],'_sel','').'.gif" width="15" height="24"></td>';*/

  echo '<li><a id="'.$sitetabs[$f][1].($GLOBALS['module']==$sitetabs[$f][1]?'active':'').'" href="'.$sitetabs[$f][2].'">'.$sitetabs[$f][0].'</a></li>';

  //submenu
  if ($sitetabs[$f][3] && $GLOBALS['module']==$sitetabs[$f][1])
  {
    echo '<ul>';
    for($ff=0;$ff<count($sitetabs[$f][3]);$ff++)
    {
          $activemod = $sitetabs[$f][3][$ff][1];
          echo '<li><a '.(!is_array($activemod) ? ($_GET['action']==$activemod?'class="active"':'') : ($_GET[$activemod[0]]==$activemod[1]?'class="active"':'')).' href="'.$sitetabs[$f][3][$ff][2].'">'.$sitetabs[$f][3][$ff][0].'</a></li>';
    }
    echo '</ul>';
  }


}


?>

