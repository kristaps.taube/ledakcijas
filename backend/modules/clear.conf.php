<?

$GLOBALS['tabs'] = 'site';
$site_id = $_GET['site_id'];

if (!CheckPermission(18)) die("access denied!");

if (!$site_id) die("site id not set");

$action = $_GET['action'];

switch ($action) {

  case "clearsite":
    $docScripts['exec'] = "clear.exec.php";
    Header("Location: ?module=pages&site_id=" . $site_id);
    break;

  default :
    $docTemplate = "main.htm";
    $docTitle = "Clear site";
    $docScripts['body'] = "clear.default.php";
    break;

}

?>
