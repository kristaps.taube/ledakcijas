<?

if(!$GLOBALS['perm_accessadmin'])
  Die('Access Forbidden');

$cols = Array(

  "action"   => Array(
    "width"     => "40%",
    "title"     => "{%langstr:log_action%}",
  ),

  "user" => Array(
    "width"     => "15%",
    "title"     => "{%langstr:log_user%}"
  ),
  
  "site" => Array(
    "width"     => "15%",
    "title"     => "{%langstr:log_site%}"
  ),
  
  "IP" => Array(
    "width"     => "10%",
    "title"     => "{%langstr:log_ip%}"
  ),
  
  "time" => Array(
    "width"     => "20%",
    "title"     => "{%langstr:log_time%}"
  )

);

$queryconds = ' (1=1) ';
if((($_GET['site'] != "a")and(isset($_GET['site'])))or($GLOBALS['site_id']))
{
  if(!$GLOBALS['site_id'])
    $GLOBALS['site_id'] = $_GET['site'];
  $queryconds .= " AND log.site_id=" . $GLOBALS['site_id'];
}
if(($_GET['user'] != "a")and(isset($_GET['user'])))
{
  $queryconds .= " AND log.user_id=" . $_GET['user'];
}
if(($_GET['IP'])and(isset($_GET['IP'])))
{
  $queryconds .= " AND log.IP LIKE '%" . $_GET['IP'] . "%'";
}
if(($_GET['checkTime']))
{
  $time1 = mktime($_GET['hour1'], $_GET['minute1'], 0, $_GET['month1'], $_GET['day1'], $_GET['year1']);
  $time2 = mktime($_GET['hour2'], $_GET['minute2'], 59, $_GET['month2'], $_GET['day2'], $_GET['year2']);
  $queryconds .= " AND log.time BETWEEN $time1 AND $time2";
}


//Table paging stuff
if(isset($_GET['pagenum']))
{
  $_SESSION['logtable']['pagenum'] = $_GET['pagenum'];
}


$pagelen = 20;

  $count= sqlQueryValue("SELECT Count(1)
                         FROM log LEFT JOIN users ON log.user_id=users.user_id
                         LEFT JOIN sites ON log.site_id=sites.site_id
                         WHERE " . $queryconds . " ORDER BY time DESC");
$pagenum = 1;
if(isset($_SESSION['logtable']['pagenum']))
  $pagenum = $_SESSION['logtable']['pagenum'];
$firstrecord = ($pagenum-1)*$pagelen;
if($firstrecord > $count)
  $firstrecord = $count - $pagelen;
$firstrecord = max(0, $firstrecord);


$data = sqlQueryData("SELECT log_id, action, IFNULL(username,log.user_id) as user, IP, IFNULL(title,log.site_id) as site, FROM_UNIXTIME(time) as time FROM log LEFT JOIN users ON log.user_id=users.user_id LEFT JOIN sites ON log.site_id=sites.site_id WHERE " . $queryconds . " ORDER BY time DESC  LIMIT $firstrecord,$pagelen");

$sites = sqlQueryData("SELECT site_id, title FROM sites");
$users = sqlQueryData("SELECT user_id, username FROM users");

$today = getdate();

echo '
  <form action="" method="GET">';

if(!$GLOBALS['site_id'])
{
  echo '  <input type="hidden" name="module" value="log">
          <input type="hidden" name="action" value="log">
          {%langstr:log_site%}: <select name="site" size="1">
          <option value="a">{%langstr:log_all%}</option>
          <option value="0">(Global)</option>
          ' . ListBox($sites, $_GET['site']) . '
          </select>';
}else
{
  echo '  <input type="hidden" name="module" value="admin">
          <input type="hidden" name="action" value="log">
          <input type="hidden" name="site_id" value="'.$GLOBALS['site_id'].'">';
}
echo '
  {%langstr:log_user%}: <select name="user" size="1">
        <option value="a">{%langstr:log_all%}</option>
        ' . ListBox($users, $_GET['user']) . '
        </select>
  {%langstr:log_ip%}:   <input name="IP" value="' . $_GET['IP'] . '">     <br>
  {%langstr:log_time%}: <input type="checkbox" name="checkTime"' . IIF($_GET['checkTime'],'checked="checked"','') . '>
        <input name="year1" value="' . IIF($_GET['year1'],$_GET['year1'],$today['year']) . '" size=6>
        <input name="month1" value="' . AddZero(IIF($_GET['month1'],$_GET['month1'],$today['mon'])) . '" size=3>
        <input name="day1" value="' . AddZero(IIF($_GET['day1'],$_GET['day1'],$today['mday'])) . '" size=3>&nbsp;&nbsp;&nbsp;
        <input name="hour1" value="' . AddZero(IIF($_GET['hour1'],$_GET['hour1'],$today['hours'])) . '" size=3> :
        <input name="minute1" value="' . AddZero(IIF($_GET['minute1'],$_GET['minute1'],$today['minutes'])) . '" size=3> &nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;
        <input name="year2" value="' . IIF($_GET['year2'],$_GET['year2'],$today['year']) . '" size=6>
        <input name="month2" value="' . AddZero(IIF($_GET['month2'],$_GET['month2'],$today['mon'])) . '" size=3>
        <input name="day2" value="' . AddZero(IIF($_GET['day2'],$_GET['day2'],$today['mday'])) . '" size=3>&nbsp;&nbsp;&nbsp;
        <input name="hour2" value="' . AddZero(IIF($_GET['hour2'],$_GET['hour2'],$today['hours'])) . '" size=3> :
        <input name="minute2" value="' . AddZero(IIF($_GET['minute2'],$_GET['minute2'],$today['minutes'])) . '" size=3>
        <br>
        <input type="submit" name="submit" value="{%langstr:log_search%}">
  </form>
';


require($GLOBALS['cfgDirRoot']."library/"."class.datagrid.php");

$Table = new DataGrid();
$Table->cols = $cols;
$Table->data = $data;
$Table->footer = false;
$Table->linkformat = $_SERVER['REQUEST_URI'] . "&pagenum=%d";
if($count / $pagelen > 1)
{
  $Table->pagecount = $count / $pagelen;
  $Table->pagelen = 1;
  $Table->pagenum = $pagenum;
}
echo $Table->output();



?>
