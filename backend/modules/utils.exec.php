<?php

function isBuggyIe() {
    $ua = $_SERVER['HTTP_USER_AGENT'];
    // quick escape for non-IEs
    if (0 !== strpos($ua, 'Mozilla/4.0 (compatible; MSIE ')
        || false !== strpos($ua, 'Opera')) {
        return false;
    }
    // no regex = faaast
    $version = (float)substr($ua, 30);
    return (
        $version < 6
        || ($version == 6  && false === strpos($ua, 'SV1'))
    );
}


  if($action == 'compress')
  {
    $compressfiles = Array(
      'ext-all.js' => 'backend/gui/extjs/ext-all.js',
      'ext-all-debug.js' => 'backend/gui/extjs/ext-all-debug.js',
      'ext-core.js' => 'backend/gui/extjs/ext-core.js',
      'ext-all.css' => 'backend/gui/extjs/resources/css/ext-all.css',
      'ext-base.js' => 'backend/gui/extjs/adapter/ext/ext-base.js',
    );

    if( headers_sent() ){
        $encoding = false;
    }elseif( strpos($_SERVER['HTTP_ACCEPT_ENCODING'], 'x-gzip') !== false ){
        $encoding = 'x-gzip';
    }elseif( strpos($HTTP_ACCEPT_ENCODING,'gzip') !== false ){
        $encoding = $_SERVER['HTTP_ACCEPT_ENCODING'];
    }else{
        $encoding = false;
    }

//    echo $GLOBALS['cfgDirRoot'] . $compressfiles[$_GET['file']] . '<br />';
    $filename = $GLOBALS['cfgDirRoot'] . $compressfiles[$_GET['file']];
    $contents = file($filename);
    $contents = join('', $contents);

    if(!$contents)
    {
      header('HTTP/1.0 404 Not Found');
      exit;
    }

    if( $encoding && !isBuggyIe()){
      ob_start("ob_gzhandler") || ob_start();
    }else
    {
      ob_start();
    }

    $hash = md5($contents);
    $offset = 3600 * 24;
    $last_modified = filemtime($filename);
    $gmt_date = gmdate("D, d M Y H:i:s", $last_modified) . ' GMT';

    header("ETag: \"$hash\"");
    header('Cache-Control: private');
    header("Pragma: ");
    header('Expires: ');
    header("Last-Modified: " . $gmt_date);
    header('Content-type: application/javascript');

    if ((isset($_SERVER["If-None-Match"]))||(isset($_SERVER["HTTP_IF_NONE_MATCH"]))){
      if(((strpos($_SERVER["If-None-Match"], $hash) !== false))||((strpos($_SERVER["HTTP_IF_NONE_MATCH"], $hash) !== false))){
        header('HTTP/1.1 304 Not Modified');
        ob_end_clean();
        exit;
      }
    }

    print($contents);
    ob_end_flush();
    exit;

  }
?>