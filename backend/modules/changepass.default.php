<?


$error = 1;
if (isset($_POST['changepass']))
{
  $u = $_SESSION['userdata'];

  if (empty($_POST['currentpass']) || empty($_POST['newpass']) || empty($_POST['newpass2']))
  {
    $msg = '{%langstr:pass_nofields%}';
  }
  else if (md5($_POST['currentpass']) != $u['password'])
  {
    $msg = '{%langstr:pass_invalid_current%}';
  }
  else if ($_POST['newpass'] != $_POST['newpass2'])
  {
    $msg = '{%langstr:pass_different%}';
  }
  else
  {
    $p = md5($_POST['newpass']);
    sqlQuery("UPDATE users SET password='".$p."' WHERE user_id=".$u['user_id']);
    $_SESSION['session_pass'] = $_POST['newpass'];
    $_SESSION['userdata']['password'] = $p;

    $error = 0;
    $msg = '{%langstr:pass_changed%}';
  }

}

$msgstyle = $error ? 'font-weight:bold; color:red' : 'font-weight:bold; color:green';
?>
<div style="<?=$msgstyle ?>"><?=$msg ?></div>
<form action="" method="post">
<table style="width:auto" cellspacing="3" cellpadding="0">
  <tr>
    <td>{%langstr:pass_current%}:</td>
    <td><input type="password" name="currentpass" /></td>
  </tr>
  <tr>
    <td>{%langstr:pass_new%}:</td>
    <td><input type="password" name="newpass" /></td>
  </tr>
  <tr>
    <td>{%langstr:pass_new2%}:</td>
    <td><input type="password" name="newpass2" /></td>
  </tr>
  <tr>
    <td colspan="2"><br/><input type="submit" name="changepass" value="{%langstr:pass_change%}" /></td>
  </tr>
</table>
</form>