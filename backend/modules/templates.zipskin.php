<?

$site_id = $GLOBALS['site_id'];

if (empty($_GET['skinname'])) return;

header("Content-type: application/octet-stream");
header('Content-Disposition: attachment; filename="'.$_GET['skinname'].'.zip"');
header("Content-Transfer-Encoding: binary");

$tpls = Array();
$componentarr = Array();
$data = sqlQueryDataAssoc("SELECT * FROM `".$GLOBALS['site_id']."_templates` ORDER BY ind");

foreach ($data as $row)
{
  $tpls[$row['name']] = $row;

  $components = componentArray($row['body'], 0);
  foreach($components as $c)
  {
    if(!in_array($c['name']))
    {
      $componentarr[] = $c['name'];
    }
  }
}

$temp_dir = $GLOBALS['cfgDirRoot'] . 'temp/';

if (!is_dir($temp_dir))
  die('nonexisting temp dir');

define( 'PCLZIP_TEMPORARY_DIR', $temp_dir );
require_once($GLOBALS['cfgDirRoot'].'library/pclzip.lib.php');

$temp_file = $temp_dir . 'downskin.zip';
$zip = new PclZip($temp_file);

//========= add templates
foreach($tpls as $name => $template)
{
  file_put_contents($temp_dir.'/'.$name, $template['body']);

  $zip->add($temp_dir.'/' . $name,
    PCLZIP_OPT_REMOVE_ALL_PATH,
    PCLZIP_OPT_ADD_PATH, 'templates');

  unlink($temp_dir.'/'.$name);
}

//========== add files (from tpl only)
$templatepath = getFilePathFromLink($site_id, '/tpl/'.$_GET['skinname']);

$zip->add($templatepath,
  PCLZIP_OPT_REMOVE_ALL_PATH,
  PCLZIP_OPT_ADD_PATH, 'files');

//=========== add cms files (scripts)
//create and add skin installation script
$scriptname = $temp_dir.'/install.php';
$s = '<?
add_log("Skin installed: '.$_GET['skinname'].'", $GLOBALS[\'site_id\']);
?>';
file_put_contents($scriptname, $s);

$zip->add($scriptname,
  PCLZIP_OPT_REMOVE_ALL_PATH,
  PCLZIP_OPT_ADD_PATH, 'cms');

unlink($scriptname);

//create and add skin initialization script
$scriptname = $temp_dir.'/apply.php';
$s = '<?
add_log("Skin applied: '.$_GET['skinname'].'", $GLOBALS[\'site_id\']);
';

$componentlist = join('","', $componentarr);
$componentlist = '"' . $componentlist . '"';
$defcont = sqlQueryData("SELECT * FROM " . $site_id . "_contents WHERE componentname IN (" . $componentlist . ") AND page = -1");

foreach($defcont as $c)
{
  $s.= "\n" . 'setProperty(\'' . $c['propertyname'] . '\', \'' . db_escape($c['propertyvalue']) . '\', \'' . $c['componentname'] . '\', -1, $GLOBALS[\'site_id\'], false);';
}

$s .= '
?>';
file_put_contents($scriptname, $s);

$zip->add($scriptname,
  PCLZIP_OPT_REMOVE_ALL_PATH,
  PCLZIP_OPT_ADD_PATH, 'cms');

unlink($scriptname);

header('Content-Length: '.filesize($temp_file));

//================= dump zip contents =============
$f = fopen($temp_file, 'r');
$bufsize = 32768;
do {
  $s = fread($f, $bufsize);
  echo $s;
} while (strlen($s) == $bufsize);
fclose($f);

@unlink($temp_file);

die;

?>