<?

$perm_managegroups = $GLOBALS['perm_managegroups'];
$perm_managesitegroups = $GLOBALS['perm_managesitegroups'];

if((!$perm_managegroups)and(!$perm_managesitegroups))
{
  AccessDenied(True);
}

// Define form
$form_data = Array(

  "group_id"    => Array(
    "label"     => "{%langstr:id%}:",
    "type"      => "hidden"
  ),

  "ind"         => Array(
    "noshow"    => true
  ),

  "groupname"   => Array(
    "label"     => "{%langstr:col_name%}:",
    "type"      => "str"
  ),
  
  "description" => Array(
    "label"     => "{%langstr:col_description%}:",
    "type"      => "html",
    "rows"      => "4",
    "cols"      => "30"
  )

);


//Add checkboxes for group permissions
include_once($GLOBALS['cfgDirRoot']."backend/modules/"."permissions.texts.php");
foreach($permissions_list as $permnum => $perm)
{
  if(($perm['type'] == 0)or(($_GET['site_id'])and($perm['type'] == 1)))
  {
    $form_data['permission_' . $permnum] = Array(
      "label"  => $perm["text"],
      "type"   => "check"
    );

  }
}


// Fill some values
  $id = $_GET["id"];
  $action = $_GET['action'];
  $create = ($action == 'create');

  if(!$create){
    $row = sqlQueryRow("SELECT * FROM groups WHERE group_id=$id");
    $form_data['group_id']['value'] = $id;
    $form_data['groupname']['value'] = $row['groupname'];
    $form_data['description']['value']  =$row['description'];
    //get permissions list and set checkboxes
    $perms = sqlQueryData("SELECT * FROM permissions WHERE isuser=0 AND id='$id'");
    foreach($perms AS $perm)
    {
      if(isset($form_data['permission_' . $perm['perm_num']]))
      {
        $form_data['permission_' . $perm['perm_num']]['value'] = 1;
      }
    }
  }

  $invalid = explode(':', $_GET['invalid']);
  foreach($invalid as $i)
  {
    if($i)
      $form_data[$i]['error'] = true;
  }

  foreach(array_keys($_GET) as $key)
  {
    $s = Split('_', $key, 2);
    if($s[0] == 'formdata')
      $form_data[$s[1]]['value'] = stripslashes($_GET[$key]);
  }
  
  if($_GET['jsmessage'])
  {
    echo '<script language="JavaScript">
          <!--
          alert("' . stripslashes($_GET['jsmessage']) . '");
          <!---->
          </script>';
  }

// Use FormGen to bring this out

  require($GLOBALS['cfgDirRoot']."library/"."class.formgen.php");
  $FormGen = new FormGen();
  if(!$create)
  {
    $FormGen->action = "?module=".$GLOBALS['module']."&site_id=" . $_GET['site_id'] . "&action=modgroup";
  }else{
    $FormGen->action = "?module=".$GLOBALS['module']."&site_id=" . $_GET['site_id'] . "&action=addgroup";
  }
  $FormGen->cancel = "close";
  if(!$create)
  {
    $FormGen->title = "{%langstr:group_properties%} : " . $row['title'];
  }else{
    $FormGen->title = "{%langstr:create_new_group%}";
  }
  $FormGen->properties = $form_data;
  $FormGen->buttons = true;
  echo '<div style=" overflow: auto; height: 500px; margin-bottom: 0;">';
  echo $FormGen->output();
  echo '</div>';

?>
