<?

  $site_id = $_GET['site_id'];
  $perm_manageusers = $GLOBALS['perm_manageusers'];
  $perm_managesiteusers = $GLOBALS['perm_managesiteusers'];
  if((!$perm_manageusers)and(!$perm_managesiteusers))
  {
    AccessDenied(True);
  }
  
  
  $sel = $_GET['sel'];
  if($sel == 'undefined') $sel = '';
  if($sel){
    $data = sqlQueryData("SELECT group_id, groupname, ELT((FIND_IN_SET(group_id,'$sel')>0)+1,'','checked') as checked FROM groups WHERE group_id=1 OR site_id='$site_id'");
  }else{
    $data = sqlQueryData("SELECT group_id, groupname, '' as checked FROM groups WHERE group_id=1 OR site_id='$site_id'");
  }
  
  $cols = Array(

  "group_id"    => Array(
    "width"     => "5%",
    "title"     => "#",
  ),

  "checkbox"    => Array(
    "width"     => "5%",
    "title"     => "",
    "format"    => "<input type=checkbox name=chbox value={%group_id%} {%checked%} OnClick=\"checkBoxes();\">"

  ),

  "groupname"   => Array(
    "width"     => "90%",
    "title"     => "{%langstr:groups%}"
  )

);


require($GLOBALS['cfgDirRoot']."library/"."class.datagrid.php");

$Table = new DataGrid();
$Table->cols = $cols;
$Table->data = $data;
$Table->header = false;
$Table->footer = false;
echo $Table->output();

  
?>
