<?

$perm_managefiles = $GLOBALS['perm_managefiles'];
if(!$perm_managefiles)
  AccessDenied(True);

$files = explode(";",$_GET['name']);
$i=0;
foreach ($files as $key=>$file)
{

$form_data["newfile".$i] = array(
    "label"     => "{%langstr:files_new_name%}:",
    "size"      => "35",
    "type"      => "str",
    "value"	=> $file     
);

$form_data["oldfile".$i] = array(
    "type"      => "hidden",
    "value"	=> $file     
);
$i++;

/*
$form_data = Array(

  "newfile"    => Array(
    "label"     => "New name:",
    "size"      => "35",
    "type"      => "str",
    "value"	=> $_GET['name']
  ),

  "oldfile"     => Array(
    "type"      => "hidden",
    "value"	=> $_GET['name']
  )
);
*/
}

if($_GET['jsmessage'])
{
  echo '<script language="JavaScript">
        <!--
        alert("' . parseForJScript(stripslashes($_GET['jsmessage']), false) . '");
        <!---->
        </script>';
}

require($GLOBALS['cfgDirRoot']."library/"."class.formgen.php");
$FormGen = new FormGen();
$FormGen->action = "?module=files&action=dorename&site_id=".$GLOBALS['site_id']."&path=".$GLOBALS['path']."&view=".$GLOBALS['view']."&name=".$_GET['name'].($GLOBALS['close'] ? "&close=".$GLOBALS['close'] : "");
$FormGen->title = "{%langstr:files_rename_file_popup%}";
$FormGen->cancel = ($GLOBALS['close'] ? "?module=files&action=list&site_id=".$GLOBALS['site_id']."&path=".$GLOBALS['path']."&view=".$GLOBALS['view'] : 'close');
$FormGen->properties = $form_data;
$FormGen->buttons = true;
echo $FormGen->output();


