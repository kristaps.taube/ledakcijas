<?
$site_id = $_GET['site_id'];
$page_id = $_GET['page_id'];


$form_data = Array(

  "category_id"     => Array(
    "label"     => "ID:",
    "type"      => "hidden"
  ),

  "title"    => Array(
    "label"     => "Category Title:",
    "size"      => "35",
    "type"      => "str"
  )

);

$action = $_GET['action'];
$create = ($action == 'newcategory');
$id = $_GET['id'];

if(!$create)
{
  $data = sqlQueryRow("SELECT text, category_id FROM ".$site_id."_news WHERE title='{%category_name%}' AND category_id=$id");
  $form_data['title']['value'] = $data['text'];
  $form_data['category_id']['value'] = $data['category_id'];
}


require($GLOBALS['cfgDirRoot']."library/"."class.formgen.php");
$FormGen = new FormGen();
if(!$create)
  {
    $FormGen->action = "?module=".$GLOBALS['module']."&site_id=".$site_id."&page_id=$page_id&action=modcategory";
  }else{
    $FormGen->action = "?module=".$GLOBALS['module']."&site_id=".$site_id."&page_id=$page_id&action=addcategory";
  }
$FormGen->title = "News Category Properties";
$FormGen->cancel =  'close';
$FormGen->properties = $form_data;
$FormGen->buttons = true;
echo $FormGen->output();


?>
