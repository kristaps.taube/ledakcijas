<?

  require_once ($GLOBALS['cfgDirRoot']."library/".'func.search.php');

  $site_id = $GLOBALS['site_id'];

  if(($action == 'addstring')or($action == 'modstring'))
  {
    $stringname = $_POST['stringname'];
    $stringvalue = $_POST['stringvalue'];
    $string_id = $_POST['string_id'];
    //Check if field input is valid
    $invalid = Array();
    $jsmessage = '';
    if((IsEmpty($stringname))or(IsBadSymbols($stringname))or(sqlQueryValue("SELECT string_id FROM " . $site_id . "_strings WHERE stringname='$stringname' AND string_id<>'$string_id' AND page=-1")))
    {
      $invalid[] = "stringname";
      if(!IsEmpty($stringname))
        $jsmessage .= 'String name ' . msgValidCharsUnique . '\n';
    }

    if(count($invalid))
    {
      if($action == 'addstring')
        $action = 'create';
      if($action == 'modstring')
        $action = 'properties_form';
      $invalid = join(':', $invalid);
      $stringname = urlencode(stripslashes($stringname));
      $stringvalue = urlencode(stripslashes($stringvalue));

      Redirect('?module='.$module.'&action='.$action.'&invalid=' . $invalid . '&formdata_stringname='.$stringname . '&formdata_stringvalue='.$stringvalue . '&jsmessage='.$jsmessage.'&id='.$string_id.'&site_id='.$site_id);
      Die();
    }
  }else if(($action == 'updatestrings'))
  {
    $page_id = $_GET['page_id'];

    sqlQuery("DELETE FROM " . $site_id . "_stringsdev WHERE pagedev='$page_id'");

    foreach($_POST as $key => $pval)
    {
      if(substr($key, 0, 17) == 'component_string_')
      {
        $name = substr($key, 17, strlen($key));
        if(!$_POST['check_component_string_' . $name])
        {
          sqlQuery("INSERT INTO " . $site_id . "_stringsdev (pagedev, stringname, stringvalue) VALUES ('$page_id','$name','$pval')");
        }
      }
    }
    //update modification date and user_id
    $modby = $GLOBALS['currentUserID'];
    sqlQuery("UPDATE ".$site_id."_pagesdev SET lastmod='".time()."', modby=$modby WHERE pagedev_id=$page_id");
    //$page_id === pagedev_id
    //$p_id === page_id
    $p_id = sqlQueryValue("SELECT page_id FROM ".$site_id."_pagesdev WHERE pagedev_id=$page_id");
    if ($p_id>0 && (CheckPermission(31, $site_id, $p_id) || CheckPermission(32, $site_id)) )
    {
        pageautopublish($site_id, $page_id);
    }
    DeleteAllCompiledTemplates($site_id);
  }


  if(($action == 'addstring'))
  {
    sqlQuery("INSERT INTO " . $site_id . "_strings (page,stringname,stringvalue) VALUES ('-1','$stringname','$stringvalue')");
    $string_id = sqlQueryValue("SELECT string_id FROM " . $site_id . "_strings WHERE stringname='$stringname'");
    Add_Log("New string '$stringname' ($string_id)", $site_id);
    DeleteAllCompiledTemplates($site_id);

  }else if(($action == 'modstring'))
  {
    $oldname = sqlQueryValue("SELECT stringname FROM " . $site_id . "_strings WHERE string_id='$string_id'");
    if($oldname != $stringname)
    {
      sqlQuery("UPDATE " . $site_id . "_strings SET stringname='$stringname' WHERE stringname='$oldname'");
    }
    sqlQuery("UPDATE " . $site_id . "_strings SET stringname='$stringname', stringvalue='$stringvalue' WHERE string_id='$string_id'");
    Add_Log("Updated string '$stringname' ($string_id)", $site_id);
    DeleteAllCompiledTemplates($site_id);

  }else if(($action == 'delstring'))
  {
    $string_id = $_GET['id'];
    $stringname = sqlQueryValue("SELECT stringname FROM " . $site_id . "_strings WHERE string_id=$string_id");
    sqlQuery("DELETE FROM " . $site_id . "_strings WHERE stringname='$stringname'");
    sqlQuery("DELETE FROM " . $site_id . "_stringsdev WHERE stringname='$stringname'");
    Add_Log("Deleted string '$stringname' ($string_id)", $site_id);
    DeleteAllCompiledTemplates($site_id);

  }
?>
