<?

  if(!$GLOBALS['perm_accessadmin'])
    Die('Access Forbidden');
  
  
  $action = $GLOBALS['action'];
  
  function sqlAddslashes($a_string = '', $is_like = FALSE)
  {
      if ($is_like) {
          $a_string = str_replace('\\', '\\\\\\\\', $a_string);
      } else {
          $a_string = str_replace('\\', '\\\\', $a_string);
      }
      $a_string = str_replace('\'', '\\\'', $a_string);

      return $a_string;
  }

  
  function CreateTable($name)
  {
    list($tablename, $command) = sqlQueryRow("SHOW CREATE TABLE $name");
    $command = str_replace("\n", "\r\n", $command);
    return $command . ";  #{%query_delimiter}";
  }
  
  function TableContents($table)
  {
    $r = '';
    $search       = array("\x00", "\x0a", "\x0d", "\x1a"); //\x08\\x09, not required
    $replace      = array('\0', '\n', '\r', '\Z');
    $data = sqlQueryDataAndID("SELECT * FROM $table", $qid);
    $colnames = '';
    if(count($data))
    {
      $cols = mysql_num_fields($qid);
      for($f=0;$f<$cols;$f++)
      {
        if($colnames)
          $colnames .= ",";
        $colnames .= mysql_field_name($qid,$f);
      }
      foreach($data as $row)
      {
        $rowvals = '';
        for($f=0;$f<$cols;$f++)
        {
          $cell = $row[$f];
          if($rowvals)
            $rowvals .= ",";
          $rowvals .= "'" . str_replace($search, $replace, sqlAddslashes($cell)) . "'";
        }
        $r .= "INSERT INTO $table ($colnames) VALUES ($rowvals);  #{%query_delimiter}\r\n";
      }
    }
    return $r;
  }

function deldir($dir)
{
  $handle = opendir($dir);
  while (false!==($FolderOrFile = readdir($handle)))
  {
     if($FolderOrFile != "." && $FolderOrFile != "..") 
     {  
       if(is_dir("$dir/$FolderOrFile")) 
       { deldir("$dir/$FolderOrFile"); }  // recursive
       else
       { unlink("$dir/$FolderOrFile"); }
     }  
  }
  closedir($handle);
  if(rmdir($dir))
  { $success = true; }
  return $success;  
} 

  
  if($action=="delcomponent")
  {
    $id = $_GET['id'];
    $comptype = sqlQueryValue("SELECT type FROM components WHERE component_id=$id");

    $filename = "../components/class.$comptype.php";
    if(file_exists($filename))
    {
      rename($filename, "../components/class.$comptype.php.old");
      sqlQuery("DELETE FROM components WHERE component_id=$id");
    }

    
  }
  if($action=="restorebackup")
  {
	require_once($GLOBALS['cfgDirRoot']."library/"."func.tar.php");
	require_once($GLOBALS['cfgDirRoot']."library/"."func.db_backup.php");

    if(!$GLOBALS['cfgBackupRoot'])
      $cfgBackupRoot = $cfgDirRoot . "backup/";
    else
      $cfgBackupRoot = $GLOBALS['cfgBackupRoot'];
    $site_id = $_GET['site_id'];

	$filename = urldecode($_GET['filename']);
	tar_extract($cfgBackupRoot."site".$site_id."/".$filename, $cfgBackupRoot."site".$site_id);

    deldir($contents_dir);
    mkdir($contents_dir, 0755);
	chmod($contents_dir, 0755);

    if (is_file($cfgBackupRoot."site".$site_id."/files.tgz"))
    {
        //atjauno failus
        $contents_dir = sqlQueryValue("select dirroot from sites where site_id=".$site_id);
        unlink($contents_dir);
        tar_extract($cfgBackupRoot."site".$site_id."/files.tgz", $contents_dir);
        unlink($cfgBackupRoot."site".$site_id."/files.tgz");
    }

    if (is_file($cfgBackupRoot."site".$site_id."/docs.tgz"))
    {
        //atjauno dokumentus
        $docroot = sqlQueryValue("select docroot from sites where site_id=".$site_id);
        tar_extract($cfgBackupRoot."site".$site_id."/docs.tgz", $docroot);
        unlink($cfgBackupRoot."site".$site_id."/docs.tgz");
    }
	
	//atjauno DB
	tar_extract($cfgBackupRoot."site".$site_id."/dumps.tgz", $cfgBackupRoot."site".$site_id."/dumps/");
	mkdir($cfgBackupRoot . "site".$site_id."/dumps/", 0755);
    chmod($cfgBackupRoot . "site".$site_id."/dumps/", 0755);
	unlink($cfgBackupRoot."site".$site_id."/dumps.tgz");

    if (is_dir($cfgBackupRoot . "site".$site_id."/dumps/"))
    {
      if ($handle = opendir($cfgBackupRoot . "site".$site_id."/dumps/"))
      {
        while (false !== ($file = readdir($handle))) 
        { 
          if ($file != "." && $file != "..") 
		  {
            $table_array[] = $file;
          } 
        }
      }
      closedir($handle); 
    }
	
	foreach($table_array as $t_filename)
      {
        csv_to_mysql($cfgBackupRoot . "site".$site_id."/dumps/".$t_filename, $site_id . "_");
      }
	deldir($cfgBackupRoot . "site".$site_id."/dumps/");

	
  }

  if($action=="backupdatabase")
  {
    require_once($GLOBALS['cfgDirRoot']."library/"."func.tar.php");

    if(!$GLOBALS['cfgBackupRoot'])
      $cfgBackupRoot = $cfgDirRoot . "backup/";
    else
      $cfgBackupRoot = $GLOBALS['cfgBackupRoot'];
    $site_id = $_GET['site_id'];

	$make_backup=0;
    //first of all, create database table dumps
    if($_POST['backupdatabase'])
    {
      require_once($GLOBALS['cfgDirRoot']."library/"."func.db_backup.php");
      //dump standard tables
      $stdtables = Array("colitems", "collections", "contents", "languages", "pages",
                         "strings", "templates", "views", "wapcontents", "wapcontentsdev", "wappages", "wappagesdev", "waptemplates", "stats");
      $doctables = array("documentdir", "documents", "documentstats");
      if ($_POST['backupdocs'])
      {
          $stdtables = array_merge($stdtables, $doctables);
      }
      $addtables = Array("ctemplates", "search", "search_link", "search_object", "search_word", "wapctemplates");
	  if ($_POST['includesearch'])
      {
		  $stdtables = array_merge($stdtables, $addtables);
	  }

      $prefix_length = strlen(strval($site_id))+1;
      
      $alltables = sqlQueryData("SHOW TABLES");
      foreach ($alltables as $table)
      {
        if (preg_match("/^".$site_id."_/",$table[0]) && !in_array(substr($table[0],$prefix_length),$stdtables) && !in_array(substr($table[0],$prefix_length),$doctables) && !in_array(substr($table[0],$prefix_length),$addtables)) 
          {
            $stdtables[] = substr($table[0],$prefix_length);
          }
      }

      mkdir($cfgBackupRoot . "dumps".$site_id."/", 0755);
      chmod($cfgBackupRoot . "dumps".$site_id."/", 0755);
      foreach($stdtables as $tname)
      {
        mysql_to_csv($tname, $cfgBackupRoot . "dumps".$site_id."/", $site_id . "_");
		$db_files_to_compress[]= $tname.".csv";
      }
	  chdir($cfgBackupRoot."dumps".$site_id);
	  tar_archive($cfgBackupRoot . "dumps.tgz", $db_files_to_compress);
	  $make_backup = 1;

      //dir where the final backup will be stored
      mkdir($cfgBackupRoot . "site".$site_id."/", 0755);
      chmod($cfgBackupRoot . "site".$site_id."/", 0755);
    }

	if ($_POST['backupfiles'])
	{
	  $contents_dir = sqlQueryValue("select dirroot from sites where site_id=".$site_id);
      if ($handle = opendir($contents_dir)) 
	  {
		while (false !== ($file = readdir($handle))) 
		{ 
		  if ($file != "." && $file != "..") 
		  { 
            $contents_to_compress[]=$file;
          } 
		}
		if ($_POST['newfiles'])
		{
          $day = $_POST['day'];
		  $month = $_POST['month'];
		  $year = $_POST['year'];

          if (($after_this_date = strtotime($day." ".$month." ".$year)) === -1) {
            $after_this_date="";
          } 
		}
		if ($_POST['smallfiles'])
		{
          if ($max_size=intval($_POST['size'])) 
			{
			 $sizetype=$_POST['sizetype'];
			 if ($sizetype=="KB") $max_size *= 1024;
			 if ($sizetype=="MB") $max_size *= (1024*1024);
			 if ($sizetype=="GB") $max_size *= (1024*1024*1024);
			}
		  else $max_size="";
		}
	  }
	  closedir($handle);

      chdir($contents_dir);

      mkdir($cfgBackupRoot . "site".$site_id."/", 0755);
      chmod($cfgBackupRoot . "site".$site_id."/", 0755);
      tar_archive($cfgBackupRoot."files.tgz", $contents_to_compress, $after_this_date, $max_size);
	  $make_backup = 1;
	}


	if ($_POST['backupdocs'])
	{
      unset($contents_to_compress);
	  $docroot = sqlQueryValue("select docroot from sites where site_id=".$site_id);
      if ($handle = opendir($docroot)) 
	  {
		while (false !== ($file = readdir($handle))) 
		{ 
		  if ($file != "." && $file != "..") 
		  { 
            $contents_to_compress[]=$file;
          } 
		}
	  }
	  closedir($handle);

      chdir($docroot);

      mkdir($cfgBackupRoot . "site".$site_id."/", 0755);
      chmod($cfgBackupRoot . "site".$site_id."/", 0755);
      tar_archive($cfgBackupRoot."docs.tgz", $contents_to_compress);
	  $make_backup_docs = 1;
	}

	
	if ($_POST['backupdatabase']) $backup[] = "dumps.tgz";
	if ($_POST['backupfiles']) $backup[] = "files.tgz";
    if ($_POST['backupdocs']) $backup[] = "docs.tgz";
	chdir($cfgBackupRoot);
	if ($make_backup) tar_archive($cfgBackupRoot."site".$site_id."/".date("Y-m-d_H-i-s", time()).".tgz", $backup);

    //copy($cfgBackupRoot.date("Y-m-d_H-i-s", time()).".tgz", $cfgBackupRoot."site".$site_id."/".date("Y-m-d_H-i-s", time()).".tgz");
	

	//deleting temporary files
	if ($_POST['backupdatabase']) 
	  {
		foreach ($db_files_to_compress as $key=>$val)
		  {
            unlink($cfgBackupRoot."dumps".$site_id."/".$val);
		  }
        rmdir($cfgBackupRoot."dumps".$site_id."/");
        unlink($cfgBackupRoot."dumps.tgz");
	  }
    if ($_POST['backupfiles']) unlink($cfgBackupRoot."files.tgz");
    if ($_POST['backupdocs']) unlink($cfgBackupRoot."docs.tgz");

  }
  if($action == "deletebackup")
  {
    require_once($GLOBALS['cfgDirRoot']."library/"."func.files.php");
    if(!$GLOBALS['cfgBackupRoot'])
      $cfgBackupRoot = $cfgDirRoot . "backup/";
    else
      $cfgBackupRoot = $GLOBALS['cfgBackupRoot'];
	
    $GLOBALS['file'] = $_GET['file'];
    if($GLOBALS['perm_accessadmin'])
      DeleteFile($cfgBackupRoot."site".$_GET['site_id']."/");
  }
  if ($action == "setcomponentenabled")
  {
    sqlQuery("update components set enabled=1 where component_id='".$_GET['component_id']."'");
  }
  if ($action == "unsetcomponentenabled")
  {
    sqlQuery("update components set enabled=0 where component_id='".$_GET['component_id']."'");
  }

  if ($action == 'setcomponentenabledfile')
  {
    $component = $_GET['component_file'];
    $filename = $GLOBALS['cfgDirRoot'] . "components/class." . $component.".php";
    include_once($filename);
    $obj = new $component("");

    if($obj->Install())
    {
      if ($obj->IsWap == true)
      {
        @sqlQuery("INSERT INTO components (type,category_id,wap) VALUES ('" . $type . "','3','1')");
      }
      else
      {
        @sqlQuery("INSERT INTO components (type,category_id) VALUES ('" . $type . "','3')");
      }
    }
  }

  if ($action == 'savechangedcomponent')
  {
    if (!empty($_POST['category']))
    {
       sqlQuery('UPDATE components SET category_id = '.$_POST['category'].' WHERE component_id = '.$_GET['component_id']);
    }
  }

  # -------------------------------------------------- #
  if($action=="backup_langstrings")
  {
    $result = sqlQueryData('SELECT * FROM phrases');
    $result2 = sqlQueryData('SELECT * FROM phrasenames');

    $xml = '<?xml version="1.0" encoding="utf-8" ?><langstr><phrases>';
    foreach ($result as $id => $value) {
      $xml .= '<item>
          <phrase_id>'.$value['phrase_id'].'</phrase_id>
          <lang>'.$value['lang'].'</lang>
          <phrasename>'.$value['phrasename'].'</phrasename>
          <phrase>'.htmlentities($value['phrase'], ENT_QUOTES).'</phrase>
        </item>';
    }
    $xml .= '</phrases>';
    $xml .= '<phrasenames>';
    foreach ($result2 as $id => $value) {
      $xml .= '<item>
          <phrasename_id>'.$value['phrasename_id'].'</phrasename_id>
          <name>'.$value['name'].'</name>
        </item>';
    }
    $xml .= '</phrasenames></langstr>';

    if(!$GLOBALS['cfgBackupRoot'])
      $cfgBackupRoot = $cfgDirRoot . "backup/";
    else
      $cfgBackupRoot = $GLOBALS['cfgBackupRoot'];

    if (!is_dir($cfgBackupRoot.'langstrings/'))
    {
      mkdir($cfgBackupRoot . "langstrings/", 0755);
      chmod($cfgBackupRoot . "langstrings/", 0755);
    }
    $file = fopen ($cfgBackupRoot.'langstrings/'.date("Y-m-d_H-i-s", time()).'.xml','w');
    fwrite($file,$xml);
    
    
  }

  if($action == "deletelangstr")
  {
    require_once($GLOBALS['cfgDirRoot']."library/"."func.files.php");
    if(!$GLOBALS['cfgBackupRoot'])
      $cfgBackupRoot = $cfgDirRoot . "backup";
    else
      $cfgBackupRoot = $GLOBALS['cfgBackupRoot'];

    $GLOBALS['file'] = $_GET['file'];
    if($GLOBALS['perm_accessadmin'])
      DeleteFile($cfgBackupRoot."/langstrings/");
  }

  if($action=="restorelangstr")
  {
    $file = $_GET['filename'];
    if(!$GLOBALS['cfgBackupRoot'])
      $cfgBackupRoot = $cfgDirRoot . "backup";
    else
      $cfgBackupRoot = $GLOBALS['cfgBackupRoot'];
    $xml = file_get_contents($cfgBackupRoot.'/langstrings/'.$file);

    $xml_array = XMLtoArray($xml);
    $data = $xml_array['LANGSTR']['PHRASES']['ITEM'];
    sqlQueryData ('TRUNCATE TABLE `phrases`');
    foreach ($data as $id => $value)
    {
      sqlQueryData('INSERT INTO `phrases` ( `phrase_id` , `lang` , `phrasename` , `phrase` )
                    VALUES (\''.$value['PHRASE_ID'].'\', \''.$value['LANG'].'\', \''.$value['PHRASENAME'].'\', \''.$value['PHRASE'].'\');');
    }
    
    $data = $xml_array['LANGSTR']['PHRASENAMES']['ITEM'];
    foreach ($data as $id => $value)
    {
      sqlQueryData('INSERT INTO `phrasenames` ( `phrasename_id` , `name`)
                    VALUES (\''.$value['PHRASENAME_ID'].'\', \''.$value['NAME'].'\');');
    }
    
//print_r($data);
    //die();
  }



function XMLtoArray($XML)
{
   $xml_parser = xml_parser_create();
   xml_parse_into_struct($xml_parser, $XML, $vals);
   xml_parser_free($xml_parser);
   // wyznaczamy tablice z powtarzajacymi sie tagami na tym samym poziomie
   $_tmp='';
   foreach ($vals as $xml_elem)
   {
       $x_tag=$xml_elem['tag'];
       $x_level=$xml_elem['level'];
       $x_type=$xml_elem['type'];
       if ($x_level!=1 && $x_type == 'close')
       {
           if (isset($multi_key[$x_tag][$x_level]))
               $multi_key[$x_tag][$x_level]=1;
           else
               $multi_key[$x_tag][$x_level]=0;
       }
       if ($x_level!=1 && $x_type == 'complete')
       {
           if ($_tmp==$x_tag)
               $multi_key[$x_tag][$x_level]=1;
           $_tmp=$x_tag;
       }
   }
   // jedziemy po tablicy
   foreach ($vals as $xml_elem)
   {
       $x_tag=$xml_elem['tag'];
       $x_level=$xml_elem['level'];
       $x_type=$xml_elem['type'];
       if ($x_type == 'open')
           $level[$x_level] = $x_tag;
       $start_level = 1;
       $php_stmt = '$xml_array';
       if ($x_type=='close' && $x_level!=1)
           $multi_key[$x_tag][$x_level]++;
       while($start_level < $x_level)
       {
             $php_stmt .= '[$level['.$start_level.']]';
             if (isset($multi_key[$level[$start_level]][$start_level]) && $multi_key[$level[$start_level]][$start_level])
                 $php_stmt .= '['.($multi_key[$level[$start_level]][$start_level]-1).']';
             $start_level++;
       }
       $add='';
       if (isset($multi_key[$x_tag][$x_level]) && $multi_key[$x_tag][$x_level] && ($x_type=='open' || $x_type=='complete'))
       {
           if (!isset($multi_key2[$x_tag][$x_level]))
               $multi_key2[$x_tag][$x_level]=0;
           else
               $multi_key2[$x_tag][$x_level]++;
             $add='['.$multi_key2[$x_tag][$x_level].']';
       }
       if (isset($xml_elem['value']) && trim($xml_elem['value'])!='' && !array_key_exists('attributes',$xml_elem))
       {
           if ($x_type == 'open')
               $php_stmt_main=$php_stmt.'[$x_type]'.$add.'[\'content\'] = $xml_elem[\'value\'];';
           else
               $php_stmt_main=$php_stmt.'[$x_tag]'.$add.' = $xml_elem[\'value\'];';
           eval($php_stmt_main);
       }
       if (array_key_exists('attributes',$xml_elem))
       {
           if (isset($xml_elem['value']))
           {
               $php_stmt_main=$php_stmt.'[$x_tag]'.$add.'[\'content\'] = $xml_elem[\'value\'];';
               eval($php_stmt_main);
           }
           foreach ($xml_elem['attributes'] as $key=>$value)
           {
               $php_stmt_att=$php_stmt.'[$x_tag]'.$add.'[$key] = $value;';
               eval($php_stmt_att);
           }
       }
   }
     return $xml_array;
}




?>
