<?

  $perm_managecontents = $GLOBALS['perm_managecontents'];
  if(!$perm_managecontents) AccessDenied(True);

  require ($GLOBALS['cfgDirRoot']."library/".'class.formgen.php');

  $id = $_GET["id"];
  $site_id = $GLOBALS['site_id'];
  $template_id = $id;
  $GLOBALS['template_id'] = $id;
  $page_id = 0;
  $GLOBALS['page_id'] = 0;

  list($data, $copybody) = sqlQueryRow("SELECT body, copybody FROM " . $site_id . "_templates WHERE template_id=$id");
  while($copybody)
    list($data, $copybody) = sqlQueryRow("SELECT body, copybody FROM " . $site_id . "_templates WHERE template_id=$copybody");

  $data = attachSubTemplates($data, $site_id);

  $r = componentArray($data, 0);

  if(!count($r)){
    echo "No components in this template";
  }else{

    $form_data = Array();
    for($f=0; $f<count($r); $f++){

      if(HasComponentPermission($site_id, $id, $r[$f]["name"])){
        displayPropertiesTable($r[$f]["type"], $r[$f]["name"], $site_id, -1, $form_data);
      }

		}

    $Inspector = new Inspector();
    $Inspector->properties = $form_data;
    $Inspector->buttons =true;
    $Inspector->cancel ="close";
    $Inspector->name = "Form";
    $Inspector->hidecheckboxes = true;
    $Inspector->action = '?module=' .$GLOBALS['module']. '&site_id=' .$site_id. '&action=updatecontrols&id='.$id;
    echo $Inspector->output();

	}
