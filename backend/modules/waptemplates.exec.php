<?

// checkbox supports
if ($_POST)
{
  // atlasam komponentus, par kuriem ir info ieksh POST
  $cmp = Array();
  foreach(array_keys($_POST) as $key)
  {
    $s = split('_', $key, 2);
    if($s[0] == 'type')
    {
      $cmp[$s[1]] = $_POST[$key];
    }
  }

  // uzstadam tuksho checkbokshu vertibu kaa nulli
  foreach ($cmp as $name => $type)
  {
    $props = getPropertiesTable($type);
    foreach ($props as $key => $p)
    {
      if ($p['type'] == 'check')
      {
        $k = 'component_'.$name.'_'.$key;
        if (!isset($_POST[$k]))
          $_POST[$k] = '0';

      }
    }
  }
}

  require_once ($GLOBALS['cfgDirRoot']."library/".'func.search.php');

  $site_id = $GLOBALS['site_id'];
  $perm_managetemplates = $GLOBALS['perm_managetemplates'];
  $perm_managecontents = $GLOBALS['perm_managecontents'];

  function IndexAndCompileAllPages($template_id, $site_id)
  {
    $pages = sqlQueryColumn("SELECT page_id FROM " . $site_id . "_pages WHERE template=$template_id");
    foreach($pages as $page)
    {
      //IndexPageForSearch($site_id, $page);
      //CreateCompiledTemplate($site_id, $page);
    }
  }

  function DeleteAllCompiledWapPages($template_id, $site_id)
  {
    sqlQuery("DELETE FROM " . $site_id . "_wapctemplates WHERE template_id=$template_id");
  }

  if(((($action == 'addtemplate')and($perm_managetemplates)))or((($action == 'modtemplate')and($perm_managetemplates))))
  {
    $name = $_POST['name'];
    $description = $_POST['description'];
    $template_id = $_POST['template_id'];
    $copybody = $_POST['copybody'];
    //Check if field input is valid
    $invalid = Array();
    $jsmessage = '';
    if((IsEmpty($name))or(IsBadSymbols($name, ' '))or(sqlQueryValue("SELECT template_id FROM " . $site_id . "_waptemplates WHERE name='$name' AND template_id<>'$template_id'")))
    {
      $invalid[] = "name";
      if(!IsEmpty($name))
        $jsmessage .= 'Template name ' . msgValidCharsUnique . '\n';
    }
    if(IsLtGtSymbols($description))
    {
      $invalid[] = "description";
      $jsmessage .= 'Description ' . msgNoLtGt . '\n';
    }
    if(count($invalid))
    {
      if($action == 'addtemplate')
        $action = 'create';
      if($action == 'modtemplate')
        $action = 'properties_form';
      $invalid = join(':', $invalid);
      $name = urlencode(stripslashes($name));
      $description = urlencode(stripslashes($description));

      Redirect('?module='.$module.'&action='.$action.'&invalid=' . $invalid . '&formdata_name='.$name . '&formdata_description='.$description . '&jsmessage='.$jsmessage.'&id='.$template_id.'&site_id='.$site_id);
      Die();
    }
  }


  if(($action == 'addtemplate')and($perm_managetemplates))
  {
    $ind = sqlQueryValue("SELECT max(ind)+1 FROM " . $site_id . "_waptemplates");
    if (!$ind) $ind = 1;
    sqlQuery("INSERT INTO " . $site_id . "_waptemplates (name,description, ind) VALUES ('$name','$description',$ind)");
    $template_id = sqlQueryValue("SELECT template_id FROM " . $site_id . "_waptemplates WHERE name='$name'");

    //Add access to the newly created template for Everyone
    AddGroupPermission(1, 24, $site_id, $template_id);
    AddGroupPermission(1, 25, $site_id, $template_id);
    AddGroupPermission(2, 24, $site_id, $template_id);
    AddGroupPermission(2, 25, $site_id, $template_id);
    Add_Log("New template wap '$name' ($template_id)", $site_id);

  }else if(($action == 'modtemplate')and($perm_managetemplates))
  {
    sqlQuery("UPDATE " . $site_id . "_templates SET name='$name', description='$description', copybody='$copybody' WHERE template_id='$template_id'");
    Add_Log("Updated template '$name' ($template_id)", $site_id);

  }else if(($action== 'modcontents')and($perm_managecontents))
  {
    $body = $_POST['bodytext'];
    $name = $_POST['name'];
    $template_id = $_POST['template_id'];
    $copybody = $_POST['copybody'];

    //Check if all components have different names
    $oldbody = $body;
    $comptypes = Array();
    $compnames = Array();
    while (ereg("{%component:([a-zA-Z0-9]+):([a-zA-Z0-9]+)(:[a-zA-Z0-9% \\.\\,\\\"\\\'\\#\\!-]+)?%}", $body, $regs))
    {
//      echo $regs[0] . "<br>";
      $body = str_replace($regs[0], "", $body);
      $comptype = $regs[1];
      $compname = $regs[2];
      if(in_array($compname, $compnames)){
        $pos = array_search($compname, $compnames);
        if($comptype != $comptypes[$pos])
        {
          $oldbody = str_replace($regs[0], "{%component:$comptype:???".$regs[3]."%}", $oldbody);
        }
      }else
      {
        $comptypes[] = $comptype;
        $compnames[] = $compname;
      }

    }
    $body = $oldbody;

    if(IsLtGtSymbols($description))
    {
      $description = htmlspecialchars($description);
    }

    //IndexAndCompileAllPages($template_id, $site_id);
    DeleteAllCompiledTemplates($site_id);

    sqlQuery("UPDATE " . $site_id . "_waptemplates SET name='$name', body='$body', description='$description', copybody='$copybody' WHERE template_id='$template_id'");
    Add_Log("Changed contents of wap template '$name' ($template_id)", $site_id);

  }else if(($action == 'deltemplate')and($perm_managetemplates))
  {
    $template_id = $_GET['id'];

    $perm_deltemplate = CheckPermission(25, $site_id, $template_id);
    if(!$perm_deltemplate)
    {
      $name = sqlQueryValue("SELECT name FROM " . $site_id . "_waptemplates WHERE template_id=$template_id");
      Add_Log("Permission denied to deleted template '$name' ($template_id)", $site_id); 
      Header("Location: ?module=".$_GET['module']."&site_id=" . $GLOBALS['site_id'] . "&selrow=" . $wappagedev_id . "&accessdenied=1");
    }
    else
    {
        $used = sqlQueryValue("SELECT wappagedev_id FROM " . $site_id . "_wappagesdev WHERE template=$template_id");
        if($used==null)
        {
          $name = sqlQueryValue("SELECT name FROM " . $site_id . "_waptemplates WHERE template_id=$template_id");
          sqlQuery("DELETE FROM " . $site_id . "_waptemplates WHERE template_id=$template_id");
          sqlQuery("DELETE FROM permissions WHERE perm_num=24 AND data=$site_id AND data2=$template_id");
          sqlQuery("DELETE FROM permissions WHERE perm_num=25 AND data=$site_id AND data2=$template_id");
          Add_Log("Deleted wap template '$name' ($template_id)", $site_id);
        }
    }

    //IndexAndCompileAllPages($template_id, $site_id);
    DeleteAllCompiledWapPages($template_id, $site_id);

  }/*else if(($action == 'delview')and($perm_managetemplates))
  {
    $view_id = $_GET['id'];
    $used = sqlQueryValue("SELECT page_id FROM " . $site_id . "_pages WHERE copypage=-$view_id LIMIT 1");
    if($used!=null)
    {
      //does the view have any parents
      $parent = sqlQueryValue("SELECT parent FROM " . $site_id . "_views WHERE view_id=$view_id");
      if(!$parent)
        $parent = 0;
      sqlQuery("UPDATE " . $site_id . "_pages SET copypage=$parent WHERE copypage=$view_id");
    }

    $name = sqlQueryValue("SELECT name FROM " . $site_id . "_views WHERE view_id=$view_id");
    sqlQuery("DELETE FROM " . $site_id . "_views WHERE view_id=$view_id");
    Add_Log("Deleted view '$name' ($view_id)", $site_id);


    IndexAndCompileAllPages($template_id, $site_id);
    DeleteAllCompiledPages($template_id, $site_id);

  }*/else if($action == 'updatecontrols')
  {
    $template_id = $_GET['id'];
    $name = sqlQueryValue("SELECT name FROM " . $site_id . "_waptemplates WHERE template_id=$template_id");
    $updated = false;
    foreach(array_keys($_POST) as $key)
    {
      $s = split('_', $key, 3);
      if($s[0] == 'component')
      {

        $oldcontID = sqlQueryValue("SELECT wapcontent_id FROM " . $site_id . "_wapcontents WHERE wappage=-1 AND componentname='" . $s[1] . "' AND propertyname='" . $s[2] . "'");
        $contID = $oldcontID;
        sqlQuery("DELETE FROM " . $site_id . "_wapcontents WHERE wappage=-1 AND componentname='" . $s[1] . "' AND propertyname='" . $s[2] . "'");
        sqlQuery("INSERT " . $site_id . "_wapcontents SET wappage=-1, componentname='" . $s[1] . "', propertyname='" . $s[2] . "', propertyvalue='" . $_POST[$key] . "'");
        $updated = true;
        $contID = sqlQueryValue("SELECT wapcontent_id FROM " . $site_id . "_wapcontents WHERE wappage=-1 AND componentname='" . $s[1] . "' AND propertyname='" . $s[2] . "'");
        //Update search table
        $type = $_POST["type_" . $s[1]];
        /*$properties = getPropertiesTable($type);
        if($properties[$s[2]]['search'])
        {
          sqlQuery("DELETE FROM " . $site_id . "_search WHERE content_id='" . $oldcontID . "'");
          if(!$_POST['component_' . $s[1] . '_nosearch'])
          {
            sqlQuery("INSERT INTO " . $site_id . "_search SET content_id=$contID");
          }
        }*/
      }
    }
    if($updated)
    {
      IndexAndCompileAllPages($template_id, $site_id);
      DeleteAllCompiledTemplates($site_id);
      Add_Log("Changed default component values of wap template '$name' ($template_id)", $site_id);
    }
  }else if(($action == 'movetemplateup')and($perm_managetemplates))
  {
    $id = $_GET['id'];
    $row = sqlQueryRow("SELECT ind, template_id FROM " . $site_id . "_waptemplates WHERE template_id=$id");
    if($row != null)
    {
      //Obtain the template_id above
      $aboverow = sqlQueryRow("SELECT ind, template_id FROM " . $site_id . "_waptemplates WHERE ind<" . $row['ind'] . " ORDER BY ind DESC LIMIT 1");
      if($aboverow != null)
      {
        sqlQueryValue("UPDATE " . $site_id . "_waptemplates SET ind=" . $aboverow['ind'] . " WHERE template_id=$id");
        sqlQueryValue("UPDATE " . $site_id . "_waptemplates SET ind=" . $row['ind'] . " WHERE template_id=" . $aboverow['template_id']);
      }
    }
  }else if(($action == 'movetemplatedown')and($perm_managetemplates))
  {
    $id = $_GET['id'];
    $row = sqlQueryRow("SELECT ind, template_id FROM " . $site_id . "_waptemplates WHERE template_id=$id");
    if($row != null)
    {
      //Obtain the template_id below
      $belowrow = sqlQueryRow("SELECT ind, template_id FROM " . $site_id . "_waptemplates WHERE ind>" . $row['ind'] . " ORDER BY ind LIMIT 1");
      if($belowrow != null)
      {
        sqlQueryValue("UPDATE " . $site_id . "_waptemplates SET ind=" . $belowrow['ind'] . " WHERE template_id=$id");
        sqlQueryValue("UPDATE " . $site_id . "_waptemplates SET ind=" . $row['ind'] . " WHERE template_id=" . $belowrow['template_id']);
      }
    }
  }
?>
