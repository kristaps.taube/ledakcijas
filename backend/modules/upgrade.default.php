<div style="font-size: larger; font-family: Verdana">
  <p>Jūs šobrīd lietojiet Constructor free versiju. Constructor free versija nodrošina Jums piemērota bezmaksas dizaina izvēli, līdz divu līmeņu izvēlnes izveidi, brīvu informācijas ievietošanu izveidotajās lapās un vienas valodas atbalstu. Ja vēlaties lietot pilnu Constructor funkcionalitāti, <a href="#contactform" onclick="setaction('pieteikties_pilnajai');">spiediet šeit, lai pieteiktos pilnajai versijai.</a></p>
<p>Dažas no Constructor pilnās versijas priekšrocībām lietotājiem:</p>
<ul type="disc">
    <li>Grupu un <b>lietotāju      pārvaldīšanas</b> iespējas</li>
    <li>Pieeja <b>atskaitei</b>      (log failiem)</li>
    <li>Pieeja <b>vietnes      statistikai</b> (apmeklējums, no kurienes nāk apmeklētāji, ar kādiem      atslēgvārdiem atrod lapu meklētājos u.c.)</li>
    <li>Iespēja      pasūtīt lapai <b>papildus funkcionalitāti</b> un vairākas valodas.</li>
</ul>
<p>IT profesionāļiem pilnā versija ļaus</p>
<ul type="disc">
    <li>Veidot <b>atļaujas</b> lapām un lietotājiem;</li>
    <li><b>Modificēt lapu veidnes (template)</b>, kodu un izskatu</li>
    <li><b>Ievietot</b> veidnēs banneru un skaitītāju kodus u.c. <b>skriptus</b></li>
    <li>Veidot vietnes satura un lapu <b>rezerves kopijas</b> ar vienu klišķi</li>
</ul>
<p>Constructor ir SIA Datateks izstrādāta satura vadības sistēma un ir attīstīta jau kopš 2003. gada. Constructor nodrošina visu nepieciešamo funkcionalitāti ērtai mājas lapas administrēšanai &ndash; mājas lapas sadaļu pievienošanu, dzēšanu, pārvietošanu, teksta informācijas pievienošanu, daudzlietotāju tiesību administrēšanu, statistiku, u.c. funkcionalitāti. Uz Constructor bāzes ir izstrādāti jau vairāk kā 40 interneta veikali un 200 mājas lapas. Constructor strādā gan ar Interent Explorer, gan Mozilla Firefox pārlūkiem.</p>
<p>Piedāvājam:</p>
<ul type="disc">
    <li><a href="#contactform" onclick="setaction('veikt_izmainas');">Nomainīt dizainu pret Jūsu izstrādāto vai nopirkto veidni;</a></li>
    <li><a href="#contactform" onclick="setaction('veikt_izmainas');">Izveidot jaunu dizainu;</a></li>
    <li><a href="#contactform" onclick="setaction('veikt_izmainas');">Pievienot papildus funkcionalitāti;</a></li>
    <li><a href="#contactform" onclick="setaction('veikt_seo');">Veikt mājas lapas optimizēšanu meklētājservisiem (SEO);</a></li>
</ul>

<div style="width: 400px" id="contactform">

<script>
function setaction(what)
{
  document.getElementById('actn').value = what;
}
</script>
<?

if($_POST['actn'] && $_GET['action'] == 'sendform')
{

  if($_POST['email'])
  {
    $email = $_POST['email'];
    $sender = $_POST['email'];
  }
  else
  {
    $email = 'constructor@datateks.lv';
    $sender = 'Nav_noradits';
  }


  send_mail($sender, $email,
    'Constructor', 'constructor@datateks.lv',
    'utf-8', 'Constructor free pieteikums',
    'Darbība: ' . $_POST['actn'] . "\n" .
    'E-pasts: ' . $_POST['email'] . "\n" .
    'Domēns: ' . $_POST['domain'] . "\n" .
    'Komentārs: ' . $_POST['comment'] . "\n"
    );

  echo '<p><b>Paldies par ziņojumu!</b></p>';

}
else
{

  $form_data = Array(

    "actn"        => Array(
      "label"     => "Darbība:",
      "type"      => "list",
      "lookup"    => Array('uzdot_jautajumu:Vēlos uzdot jautājumu', 'pieteikties_pilnajai:Vēlos pieteikties pilnajai versijai', 'veikt_izmainas:Vēlos veikt izmaiņas lapā', 'veikt_seo:Vēlos veikt meklētāju optimizāciju (SEO)')
    ),

    "email"       => Array(
      "label"     => "Jūsu e-pasts:",
      "type"      => "str",
    ),

    "domain"      => Array(
      "label"     => "Domēns:",
      "type"      => "str",
      "value"     => $GLOBALS['cfgDomain']
    ),

    "comment" => Array(
      "label"     => "Komentārs:",
      "type"      => "html",
      "rows"      => "4",
      "cols"      => "30",
    )
  );


  require($GLOBALS['cfgDirRoot']."library/"."class.formgen.php");
  $FormGen = new FormGen();
  $FormGen->action = "?module=".$GLOBALS['module']."&site_id=".$GLOBALS['site_id']."&action=sendform";
  $FormGen->cancel = "";
  $FormGen->title = "Sazināties ar Datateks";
  $FormGen->properties = $form_data;
  $FormGen->buttons = true;
  $FormGen->hidereset = true;
  echo $FormGen->output();

}


?>
</div>
</div>