<?

  global $css;


  //Add a new propertystring to the list. The key represents under which tag/id/class/subclass to store the information.
  //The codestr is a string of css properties. Each property should be separated by a ;. Values should be separated from the propertynames by a :.
  function Add($key, $codestr) {
    global $css;
    $key = strtolower($key);
    $codestr = strtolower($codestr);
    if(!isset($css[$key])) {
      $css[$key] = array();
    }
    $codes = explode(";",$codestr);
    if(count($codes) > 0) {
      foreach($codes as $code) {
        $code = trim($code);
        list($codekey, $codevalue) = explode(":",$code);
        if(strlen($codekey) > 0) {
          $css[$key][trim($codekey)] = trim($codevalue);
        }
      }
    }
  }

  //Parse a textstring that contains css information.
  function ParseStr($str) {
    global $css;
    //$this->Clear();
    // Remove comments
    $str = preg_replace("/\/\*(.*)?\*\//Usi", "", $str);
    // Parse this damn csscode
    $parts = explode("}",$str);
    if(count($parts) > 0) {
      foreach($parts as $part) {
        list($keystr,$codestr) = explode("{",$part);
        //echo "key: ".$keystr." : code:".$codestr."<br><Br>";
        $keys = explode(",",trim($keystr));
        if(count($keys) > 0) {
          foreach($keys as $key) {
            if(strlen($key) > 0 && preg_match("/^\.[a-zA-Z0-9_]+$|\s\.[a-zA-Z0-9_]+$/i",$key)) {
              $key = str_replace("\n", "", $key);
              $key = str_replace("\\", "", $key);
              Add($key, trim($codestr));
            }
          }
        }
      }
    }
    //
    return (count($css) > 0);
  }

  //Parse a file that contains css information.
  function Parse($filename) {
    //$this->Clear();
    if(file_exists($filename)) {
      return ParseStr(implode('', file($filename)));
    } else {
      return false;
    }
  }

//----------------------------------------------------------
/*
$css_str = '
b, .test0 {
    font-weight: bold; 
    color: #777777;
  } 
.test1
{
    text-decoration: underline;
}
';
*/
//ParseStr($css_str);
$dirroot = sqlQueryValue("select dirroot from sites where site_id=".$GLOBALS[site_id]);

if ($_GET['cssfiles']!="") 
{
    $styles = explode(";",$_GET['cssfiles']);
    foreach ($styles as $style)
        parse($dirroot.$style);
}

$csslistJS = "";
echo '<table width="100%" border="0" cellpadding="0" cellspacing="0" >';
foreach ($css as $key=>$style)
{
    //echo $key;
    $html_style = "";
    foreach ($style as $prp=>$val)
    {
       $html_style .= $prp.":".$val.";";
    }
    echo '<tr><td><input type="checkbox" checked name="'.substr($key,1).'"></td>';
    echo '<td><div style="'.$html_style.'">'.$key.' class sample</div></td></tr>';
    if ($csslistJS) $csslistJS .= ",'".substr($key,1)."'";
    else $csslistJS .= "'".substr($key,1)."'";
}
echo '</table>';

echo '
<script>
    parent.window.csslist = new Array('.$csslistJS.');
</script>
';

//print_r($css);

?>
