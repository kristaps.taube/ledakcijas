<?

function unzip()
{
  $dirroot = sqlQueryValue("SELECT dirroot FROM sites WHERE site_id=".$_GET['site_id']);
  $path = $_GET['path'];
  $file = $_GET['file'];
  $site_id = $_GET['site_id'];
  $fullpath = $dirroot."/".$path."/".$file;

  if (!is_file($fullpath))
  {
    echo 'Not a file!';
    return;
  }

  $basename = pathinfo($file, PATHINFO_FILENAME);

  if (file_exists($dirroot."/".$path."/".$basename))
  {
    $n = 0;
    while (file_exists($dirroot."/".$path."/".$basename.$n))
    {
      $n++;
    }
    $basename .= $n;
  }

  require_once($GLOBALS['cfgDirRoot'].'library/pclzip.lib.php');

  $zip = new PclZip($fullpath);
  $list = $zip->listContent();
  if (!$list)
  {
    echo 'Not a zip archive!<br/><br/>';
    return;
  }

  $zip_dir = $dirroot."/".$path."/".$basename;
  @mkdir($zip_dir, 0755);
  @chmod($zip_dir, 0755);

  if (!is_dir($zip_dir))
  {
    echo 'Can\'t create zip directory: '.$zip_dir;
    return;
  }

  echo 'Extracting files to '.$basename.'<br/>';

  if ($zip->extract($zip_dir) < 0)
  {
    echo 'Can\'t unpack ZIP archive!<br/>';
  }
  else
  {
    echo 'Files count: '.count($list).'<br/><br/>';
    foreach ($list as &$item){

			if(!$item['folder'] && getFileExtension($item['stored_filename']) == '.php'){ // dont allow
				unlink($zip_dir."/".$item['stored_filename']);
        echo "Dropped: ".htmlspecialchars($item['stored_filename']).'<br/>';
				continue;
			}

      echo htmlspecialchars($item['stored_filename']).'<br/>';
    }
    echo '<br/>Done!';
  }
}

unzip();

echo '<form style="margin:0px;" method="post" name="Form" action="?module=files&action=dorename&site_id='.$site_id.'&path='.$path.'&file='.$file.'">
          <div align="right"><input type="submit" value="{%langstr:ok%}"><input type="button" value="{%langstr:cancel%}" onclick="window.close();"></div>
          </form>';


