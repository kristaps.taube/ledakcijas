<?

  profiler_add('Design ' . $_SERVER['REQUEST_URI'], 7);

  $site_id = intval($GLOBALS['site_id']);
  $pagedev_id = intval($_GET["pagedev_id"]);
  $page_id = intval($_GET['page_id']);
  $GLOBALS['pagedev_id'] = intval($_GET['pagedev_id']);
  $_SESSION['site_id'] = $site_id;

  if(!$pagedev_id)
  {
    //possibly old link used
    //active dev page id
    if ($page_id)
    {
        $pagedev_id = sqlQueryValue("SELECT pagedev_id, lastmod FROM ".$site_id."_pagesdev WHERE page_id=".$page_id." ORDER BY lastmod DESC LIMIT 1");
        $GLOBALS['pagedev_id'] = $pagedev_id;
    }
  }
  if (!$pagedev_id)
  {
    Die('No page selected');
  }

  $perm_managecontents = $GLOBALS['perm_managecontents'];

  if(!$perm_managecontents)
  {
    //check if page exists
    if($pagedev_id > 0)
      $pagedev_id = sqlQueryValue("SELECT pagedev_id FROM " . $site_id . "_pagesdev WHERE pagedev_id = " . $pagedev_id);
    if($pagedev_id)
    {
      AccessDenied(True);
    }else
    {
      Die("The page does not exist");
    }
  }

  $GLOBALS['PageEncoding'] = PageDevEncoding($pagedev_id, $site_id);
  $GLOBALS['maincharset']  = PageDevEncoding($pagedev_id, $site_id, false);

  if($pagedev_id<0)
  {
    $pageData = sqlQueryRow("SELECT template_id AS template FROM " . $site_id . "_views WHERE view_id='".(-$pagedev_id)."'");
  }
  else
  {
    $pageData = sqlQueryRow("SELECT * FROM " . $site_id . "_pagesdev LEFT JOIN " . $site_id . "_languages ON " . $site_id . "_languages.language_id=" . $site_id . "_pagesdev.language WHERE " . $site_id . "_pagesdev.pagedev_id='$pagedev_id'");
  }
  if($pageData == null)
    Die("The page does not exist");

  $GLOBALS['pageData'] = $pageData;
  if(!$GLOBALS['pageData']){
    Die ("Sorry, the page does not exist or database problems.");
  }


  $templateData = sqlQueryRow("SELECT * FROM " . $site_id . "_templates WHERE template_id=" . $pageData['template']);
  if(!$templateData){
    Die ("Sorry, database problems");
  }

  $template = DB::GetRow('SELECT * FROM '.$site_id.'_templates WHERE template_id = :id', [':id' => $pageData['template']]);

  $path = Constructor\App::$app->getConfigValue("template_folder").$template['name'].'.php';
  if(file_exists($path)){
    ob_start();
    require $path;
    $body = ob_get_clean();
  }else{
    die("Template `".$name."` not found");
  }

  $GLOBALS['template_id'] = $pageData['template'];
  $GLOBALS['language_id'] = $pageData['language'];
  $GLOBALS['language'] = $pageData['shortname'];
  $GLOBALS['copypage'] = $pageData['copypage'];

  $body = attachSubTemplates_new($body, $site_id);

  //process template language
  //remove comments
  $body = preg_replace('/{%\/\/(.*?)\/\/%}/s', '', $body);


  $startjs = '';
  if($GLOBALS['cfgMiniDemo'] && !$_SESSION['hasSeenVisualeditorArrowHints'])
  {
    $startjs = '<script language="JavaScript" src="'.$GLOBALS['docStrings']['oldhost'].'compressed/extall.php"></script>' . $startjs;
    $startjs = '<script language="JavaScript" src="'.$GLOBALS['docStrings']['oldhost'].'compressed/extbase.php"></script>' . $startjs;
  }
  $startjs = '<script language="JavaScript" src="'.$GLOBALS['docStrings']['oldhost'].'gui/componenttoolbar.js"></script>' . $startjs;
  $startjs = '<script language="JavaScript" src="'.$GLOBALS['docStrings']['oldhost'].'gui/scripts.js"></script>' . $startjs;
  $startjs = '<script language="JavaScript" src="'.$GLOBALS['cfgWebRoot'].'gui/jquery.min.js"></script>' . $startjs;

  if(strpos($body, '{%visualeditjs%}')===false && strpos($body, '{%header%}')===false)
  {
    $body = $startjs . $body;
    $defoutborder = false;
  }else
  {
    $body = str_replace(Array("{%visualeditjs%}", "{%header%}"), Array($startjs, $startjs), $body);
    $defoutborder = true;
  }


  //process ifvisible parts
  while(preg_match_all ( '/{%ifvisible([0-9]*?):([a-zA-Z0-9|]*?)%}(.*?){%fivisible\\1%}/s', $body, $matches, PREG_SET_ORDER))
  {
    foreach($matches as $key => $row)
    {
      $s = explode('{%elsevisible' . $row[1] . '%}', $row[3]);
      $components = explode('|', $row[2]);
      $hidden = true;
      foreach($components as $component)
      {
        $hid = getProperty('visible', $component, '', $pagedev_id, $site_id, $pageData['copypage']);
        if(!$hid)
          $hidden = false;
      }
  //          echo "row2 = " . $row[2];
      if(!$hidden)
        $body = str_replace($row[0], $s[0], $body);
      else
        $body = str_replace($row[0], $s[1], $body);
    }
  }

  //process ifdesign parts
  preg_match_all ( '/{%ifdesign([0-9]*?)%}(.*?){%fidesign\\1%}/s', $body, $matches, PREG_SET_ORDER);
  foreach($matches as $key => $row)
  {
    $s = explode('{%elsedesign' . $row[1] . '%}', $row[2]);
    $body = str_replace($row[0], $s[0], $body);
  }
  //process iflanguage parts
  preg_match_all ( '/{%iflanguage([0-9]*?):([a-zA-Z0-9|]*?)%}(.*?){%filanguage\\1%}/s', $body, $matches, PREG_SET_ORDER);
  foreach($matches as $key => $row)
  {
    $s = explode('{%elselanguage' . $row[1] . '%}', $row[3]);
    $languages = explode('|', $row[2]);
    $shortname = sqlQueryValue("SELECT shortname FROM " . $site_id . "_languages WHERE language_id=" . $pageData['language']);
    $hidden = true;

    foreach($languages as $l)
    {
      if($l == $shortname)
        $hidden = false;
    }
    if(!$hidden)
      $body = str_replace($row[0], $s[0], $body);
    else
      $body = str_replace($row[0], $s[1], $body);
  }


  $body = str_replace("{%title%}", $pageData['title'], $body);



  $mustdisplayproperties = true;
  while (preg_match("/{%component:([a-zA-Z0-9_-]+):([a-zA-Z0-9_-]+)(:[a-zA-Z0-9% \\.\\,\\\"\\\'\\#\\!-]+)?%}/s", $body, $regs))
  {
    $tags[] = Array($regs[0], $regs[1]);
    if($defoutborder)
      $regs[3] = ':outborder "1"' . trim($regs[3], ':');
    $body = str_replace($regs[0], outputGraphicalComponent($hascontent, $regs[1], $regs[2], $page_id, 0, $regs[3], false, $toolbarfunc, false, $pagedev_id, true), $body);
    if($hascontent)
      $mustdisplayproperties = false;
  }

  //process ifset parts
  while(preg_match_all ( '/{%ifset([0-9]*?):([a-zA-Z0-9_-]*?)(?::([a-zA-Z0-9_-]*?))?%}(.*?){%fiset\\1%}/s', $body, $matches, PREG_SET_ORDER))
  {
    foreach($matches as $key => $row)
    {
      $s = explode('{%elseset' . $row[1] . '%}', $row[4]);
      $flagname = $row[2];
      $componentname = $row[3];

      $hasflag = false;
      if($componentname)
      {
        if(isset($GLOBALS['component_flags'][$flagname][$componentname]))
          $hasflag = $GLOBALS['component_flags'][$flagname][$componentname];
      }
      else if($GLOBALS['component_flags'][$flagname])
      {
        foreach($GLOBALS['component_flags'][$flagname] as $flag)
          $hasflag = ($hasflag || $flag);
      }

      if($hasflag)
        $body = str_replace($row[0], $s[0], $body);
      else
        $body = str_replace($row[0], $s[1], $body);
    }
  }

  //global variables (after processing components)
  while (preg_match("/{%var:glo:([a-zA-Z0-9_]+)%}/s", $body, $regs))
  {
    $body = str_replace($regs[0], $GLOBALS[$regs[1]], $body);
  }

  //strings
  while (preg_match("/{%string:([a-zA-Z0-9_]+)%}/s", $body, $regs))
  {
    $body = str_replace($regs[0], GetPageString($site_id, $pagedev_id, $regs[1]), $body);
  }

  //languagestrings
  while (preg_match("/{%ls:([a-zA-Z0-9_]+)(:(([^%])*))?%}/s", $body, $regs))
  {
    $ls = GetLanguageString($site_id, $regs[1], '', $pageData['language']);
    if(!$ls)
      $ls = $regs[3];
    $body = str_replace($regs[0], $ls, $body);
  }

  //variables
  $body = str_replace("{%var:name%}", $pageData['name'], $body);
  $body = str_replace("{%var:site_id%}", $site_id, $body);
  $body = str_replace("{%var:page_id%}", $page_id, $body); //!!!!!!!!!!!!!!!!!!!!
  $body = str_replace("{%var:template_id%}", $template_id, $body);
  $body = str_replace("{%url:host%}", $url['host'], $body);
  $body = str_replace("{%url:path%}", $url['path'], $body);
  $body = str_replace("{%url:query%}", $url['query'], $body);

  $pos = strpos($body, '{%language:');

  if($pageData['language'])
  {
    list($shortname, $fullname, $encoding) = sqlQueryRow("SELECT shortname, fullname, encoding FROM " . $site_id . "_languages WHERE language_id=" . $pageData['language']);

  }else
  {
    $shortname = '';
    $fullname = '';
    $encoding = 'UTF-8';
  }

  if($pos)
  {
    $body = str_replace("{%language:shortname%}", $shortname, $body);
    $body = str_replace("{%language:fullname%}", $fullname, $body);
    $body = str_replace("{%language:encoding%}", $encoding, $body);
  }

  //gets and posts
  while (preg_match("/{%get:([a-zA-Z0-9]+)%}/s", $body, $regs))
  {
    $body = str_replace($regs[0], $_GET[$regs[1]], $body);
  }
  while (preg_match("/{%post:([a-zA-Z0-9]+)%}/s", $body, $regs))
  {
    $body = str_replace($regs[0], $_POST[$regs[1]], $body);
  }

  $body = str_replace("{%null%}", '', $body);

  //remove search indexer tags
  $body = str_replace("{%nosearch%}", '', $body);
  $body = str_replace("{%onsearch%}", '', $body);

  $body .= '<script language="JavaScript" type="text/javascript">
<!--
function SelectBranchOfTree()
{
  try
  {
    parent.parent.document.body.OnOpenFolderToNode('.$page_id.');
    parent.parent.document.body.selectedpage = '.$page_id.';
  } catch(e) {}
}

$(window).load(function()
{
  SelectBranchOfTree();

  if(window.GlobalOnloadEval != "undefined")
    eval(window.GlobalOnloadEval);
});

$(window).unload(function()
{
  if((window.parent.UnsavedComponents)&&(window.parent.UnsavedComponents.length))
  {
    if(confirm("{%langstrjs:ask_save_modifications%}"))
    {
      SaveComponents(true);
    }
  }
});

//-->
</script>';
  if($mustdisplayproperties)
  {
    $body .=  '<script language="JavaScript">
              <!--
                PropertiesWindow(\'?module=pages&site_id='.$site_id.'&id='.$pagedev_id.'&component_name='.'&component_type=' .''."&session_id=" . session_id() . "&session_name=" . session_name().'\' ,\'\', null, 400, 400, 1, 1);
              <!---->
              </script>';
  }


  $sitedata = sqlQueryRow("SELECT domain, wwwroot FROM sites WHERE site_id='".$site_id."'");
  $base = $sitedata['domain'] . $sitedata['wwwroot'];
  //echo '<base href="http://'.$base.'" />';

  echo $body;
  //echo '</base>';
  //echo "awaa";

  profiler_add('Design ' . $_SERVER['REQUEST_URI'], 8);


if($GLOBALS['cfgMiniDemo'] && !$_SESSION['hasSeenVisualeditorArrowHints'])
{

  $_SESSION['hasSeenVisualeditorArrowHints'] = true;



?>


<table id="floaterdiv" style="display: none; background: #FFFFFF; width: 260px; border: 1px solid black; border-collapse: collapse;">
<tr>
  <td rowspan="2" style="padding: 5px;"><img src="<?= $GLOBALS['cfgWebRoot']; ?>gui/images/bigarrowleft.png" /></td>
  <td style="text-align: right; padding: 0;"><a href="javascript:close_float_div('');" style="color: black; text-decoration: none;">&nbsp;x&nbsp;</a></td>
</tr>
<tr>
  <td valign="top" style="padding-left: 5px; padding-right: 5px; padding-bottom: 5px; padding-top: 0px; font-size: 130%;">
  {%langstr:demohint_click_to_edit%}
  </td>
</tr>
</table>

<script>

function close_float_div(a)
{
  var el = Ext.get("floaterdiv" + a);
  Ext.getDom(el).style.display = "none";
}

function float_div()
{
  try {
    var el = Ext.get("floaterdiv");
    document.body.appendChild(Ext.getDom(el));
    el.setOpacity(0);
    Ext.getDom(el).style.display = "block";
    el.alignTo("div_maincontents", "tl-tr", [5, 0]);
    el.animate(
      // animation control object
      {
          opacity: {to: 1, from: 0}
      },
      0.65,      // animation duration
      null,      // callback
      'easeOut', // easing method
      'run'      // animation type ('run','color','motion','scroll')

    );

    //blink maincontent
    window.fadeCount = 0;
    fadeDivOut();

  } catch (e) {}
}


function fadeDivOut()
{
  var el = Ext.get("div_maincontents");

  window.fadeCount++;
  if(window.fadeCount > 2)
  {
    el.dom.style.backgroundColor = "";
    return false;
  }

  el.animate(
    {
        backgroundColor: { to: '#FFFFFF', from: '#FF9966' }
    },
    0.65,      // animation duration
    fadeDivOut,      // callback
    'easeBoth', // easing method
    'color'    // animation type ('run','color','motion','scroll')
  );
}


window.onload_old = window.onload;
window.onload = function() {
    window.onload_old();
    float_div.defer(2000);
}

</script>

<?
}
?>