<?

$dirroot = sqlQueryValue("SELECT dirroot FROM sites WHERE site_id=".$_GET['site_id']);
$path = $_GET['path'];
$file = $_GET['file'];
if($file == '.htaccess') die();
$site_id = $_GET['site_id'];

if (is_file($dirroot."/".$path."/".$file))
{
    echo '<div class="FormTitle">{%langstr:file%}: '.$path."/".$file.'</div>';
    $contents = file_get_contents($dirroot."/".$path."/".$file);
}
else
{
    echo '<div class="FormLabel">{%langstr:files_can_not_open%}</div>';
}

if ($contents===false)
{
    echo '<div class="FormLabel">{%langstr:files_can_not_open_empty%}</div>';
}
else if ($contents)
{
    $ext = strtolower(substr(strrchr($file, '.'), 1));

    if($ext == 'htm' || $ext == 'html')
    {
      $ext = 'html';
    }
    elseif (!in_array($ext, array('css', 'js', 'php', 'sql', 'xml')))
    {
      unset($ext);
    }

    if (!empty($ext))
    {
      ?>
      <script language="javascript" type="text/javascript" src="<?php echo $GLOBALS['cfgWebRoot']; ?>gui/editarea/edit_area/edit_area_full.js"></script>
      <script language="javascript" type="text/javascript">
      window.onbeforeunload = function()
      {
        editAreaLoader.delete_instance('file_body');
      }

      editAreaLoader.init({
      	id : "file_body",
      	syntax: "<?php echo $ext; ?>",
      	start_highlight: true,
        toolbar: "search, go_to_line, |, undo, redo, |, select_font,|, change_smooth_selection, highlight, reset_highlight, |, help"
      });
      </script>
      <?php
    }

    echo '<form style="margin:0px;" method="post" name="Form" action="?module=files&action=savecontents&site_id='.$site_id.'&path='.$path.'&file='.$file.'">
          <textarea id="file_body" class="formEdit" style="width:800px;height:530px;font-family: \'Courier New\', Courier, monospace;" cols="97" rows="30" name="contents">'.$contents.'</textarea>
          <div align="right"><input type="submit" value="{%langstr:save%}"><input type="button" value="{%langstr:cancel%}" onclick="window.close();"></div>
          </form>
    ';
}
?>
