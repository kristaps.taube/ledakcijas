<?

$perm_managepages = $GLOBALS['perm_managepages'];
$site_id = $_GET['site_id'];

//function for sorting data in tree structure
function Nodes($data, &$newdata, $parent, $level, $justcount, &$goodl, $perms)
{
  $goodlanguage = false;
  $nodecount = 0;
  foreach($data as $key => $row)
  {
    if($parent == $row['parent']){
      $nodecount++;

        $row['prefix'] = str_repeat('<img src="gui/images/1x1.gif" width="16" height="16" border="0" align="absmiddle">', $level) . '<img src="gui/images/1x1.gif" width="16" height="16" border="0" align="absmiddle">&nbsp;';
        $newdata[] = $row;
        $c = count($newdata) - 1;
        $childs = Nodes($data, $newdata, $row['wappage_id'], $level + 1, 0, $g, $perms);
        $goodlanguage = ($g or $goodlanguage);

        if(!$_SESSION['wapexpanded' . $row['wappage_id']])
        {
            $newdata = array_slice($newdata, 0, $c+1);
        }

        /*if($row['language'] != intval($GLOBALS['currentlanguagenum']))
        {
          //$newdata[$c]['page_id'] = 0;
          $newdata[$c]['clicklink'] = "Page in " . sqlQueryValue("SELECT fullname FROM " . $GLOBALS['site_id'] . "_languages WHERE language_id = " . intval($row['language'])) . " (" . $row['name'] . ")";
          if(!$g)
          {
            if($c)
              $newdata = array_slice($newdata, 0, $c);
            else
              $newdata = array();
          }
        }else
        {*/
          /*if ($GLOBALS['perm_managepages'] || in_array($row['page_id'],$perms))
            $newdata[$c]['clicklink'] = "<a href=\"#\" OnClick=\"javascript:openDialog('?module=pages&site_id=" . $GLOBALS['site_id'] . "&action=components_visual_frame&id=".$row['page_id']."', screen.availWidth, screen.availHeight-25, 1 , 1);\">".ShortenString($row['title'],40)."</a>";
          else*/
            $newdata[$c]['clicklink'] = ShortenString($row['title'],40);
            //$newdata[$c]['publish'] = date("Y m d - H:i:s",$row['lastmod']);

            $newdata[$c]['publish'] = '<a href="?module=wap&site_id='.$_GET['site_id'].'&action=publish&wappagedev_id='.$row['wappagedev_id'].'&wappage_id='.$row['wappage_id'].'">Publish</a>';

          $goodlanguage = true;
        //}

        if(($childs)and(isset($newdata[$c]))){
          if($_SESSION['wapexpanded' . $row['wappage_id']])
          {
            $newdata[$c]['prefix'] = '&nbsp;'.str_repeat('<img src="gui/images/1x1.gif" width="16" height="16" border="0" align="absmiddle">', $level) . '<a href="?module=wap&site_id=' . $GLOBALS['site_id'] . '&collapsebranch=' . $newdata[$c]['wappage_id'] . '"><img src="gui/images/minus.gif" width="11" height="11" border="0" align="absmiddle"></a>&nbsp; ';
          }else
          {
            $newdata[$c]['prefix'] = '&nbsp;'.str_repeat('<img src="gui/images/1x1.gif" width="16" height="16" border="0" align="absmiddle">', $level) . '<a href="?module=wap&site_id=' . $GLOBALS['site_id'] . '&expandbranch=' . $newdata[$c]['wappage_id'] . '"><img src="gui/images/plus.gif" width="11" height="11" border="0" align="absmiddle"></a>&nbsp; ';
          }
        }

    }

  }
  $goodl = $goodlanguage;
  return $nodecount;
}


$site_id = $GLOBALS['site_id'];

// Let's try table generator

$cols = Array(

  "title"        => Array(
    "width"     => "100%",
    "title"     => "Title",
    "format"    => "{%prefix%}<a href=\"javascript:;\" OnClick=\"javascript: retVal('Form', 'LinkUrl', '/wap/{%path%}');\">{%title%}</a>"
  )

);



/*
$javascriptv = ' onClick="javascript:SubmitPageVisibleCheckbox(this, \',page_id,\',' . $site_id . ')"';
$javascripte = ' onClick="javascript:SubmitPageEnabledCheckbox(this, \',page_id,\',' . $site_id . ')"';

if(!$perm_managepages)
{
  $javascriptv = '';
  $javascripte = '';
}

/*$checkstylev = 'CONCAT(\'<input type="checkbox" name="cccb"' . $javascriptv . ' checked>\')';
$uncheckstylev = 'CONCAT(\'<input type="checkbox" name="cccb"' . $javascriptv . '>\')';
$checkstylee = 'CONCAT(\'<input type="checkbox" name="cccb"' . $javascripte . ' checked>\')';
$uncheckstylee = 'CONCAT(\'<input type="checkbox" name="cccb"' . $javascripte . '>\')';  */

    //get all data from wappagesdev
    $unsorteddata = sqlQueryData("SELECT wappagedev_id, name, title, description, lastmod, parent, wappage_id FROM " . $site_id . "_wappagesdev ORDER BY ind");

    //get wappage_id array
    $ids = sqlQueryData("select distinct wappage_id, ind from ".$site_id."_wappagesdev ORDER BY ind");
    
    //get keys of last modified dev pages
    if (count($ids))
    {
        $keys = array();
        for ($i=0; $i<=count($ids)-1; $i++)
        {
            $max = 0;
            foreach ($unsorteddata as $key=>$row)
            {
                if ($row['wappage_id']==$ids[$i]['wappage_id'])
                {
                    if($row['lastmod']>$max)
                    {
                        $max = $row['lastmod'];
                        $keys[$i] = $key;
                    }
                        
                }
            }
        }
    }

    //drop unnecessary rows
    foreach ($unsorteddata as $key=>$row)
        if (!in_array($key,$keys))
            unset($unsorteddata[$key]);

Nodes($unsorteddata, $data, 0, 0, 0);

foreach (array_keys($data) as $key) $data[$key]['path'] = WapPagePathById($data[$key]['wappage_id'], $site_id);

require($GLOBALS['cfgDirRoot']."library/"."class.datagrid.php");

$Table = new DataGrid();
$Table->cols = $cols;
$Table->data = $data;
$Table->header = false;
$Table->footer = false;
$Table->rowformat = " id=\"tablerow{%page_id%}\" onClick=\"javascript:HighLightTR(this, {%page_id%});\"";
echo $Table->output();

if($_GET['selrow'])
{
  echo '<script language="JavaScript">
        <!--
          preEl = document.getElementById("tablerow' . $_GET['selrow'] . '");
          ChangeTextColor(tablerow' . $_GET['selrow'] . ',\'tableSelected\');
          SelectedRowID = ' . $_GET['selrow'] . ';
        <!---->
        </script>';
}

?>
