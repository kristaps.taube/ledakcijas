<?

$category_id = $collection->category_id;
$site_id = $_GET['site_id'];
$page_id = $_GET['page_id'];
$GLOBALS['PageEncoding'] = PageEncoding($page_id, $site_id);
$GLOBALS['maincharset']  = PageEncoding($page_id, $site_id, false);

$Toolbar = new Toolbar();
$Toolbar->AddButton("Add New", "add_component.gif", "openDialog('?module=news&action=newnews&site_id=$site_id&page_id=$page_id&category_id=$category_id', 400, 260, 1);", "Add new news item");
$Toolbar->AddButton("Edit", "edit_properties.gif", "openDialog('?module=news&action=editnews&site_id=$site_id&page_id=$page_id&category_id=$category_id&id='+SelectedRowID, 400, 260, 1);", "Edit news item");
$Toolbar->AddLink("Delete", "delete_component.gif", "if(SelectedRowID)window.location='?module=news&action=delnews&site_id=$site_id&page_id=$page_id&category_id=$category_id&id='+SelectedRowID;", "Deletes selected news item", "Are you sure you want to delete selected news item?");
$Toolbar->AddLink("Edit Categories", "edit_contents.gif", "window.location='?module=news&action=listcategories&site_id=$site_id&page_id=$page_id'", "Edit news categories", "");
echo $Toolbar->output();


$cols = Array(

  "title"        => Array(
    "width"     => "25%",
    "title"     => "Title",
    "format"    => "<a href=\"#\" OnClick=\"javascript:openDialog('?module=news&action=editnews&category_id=$category_id&id={%news_id%}', 400, 260);\">{%title%}</a>"
  ),

  "text"    => Array(
    "width"     => "60%",
    "title"     => "Text"
  ),

  "date"     => Array(
    "width"     => "15%",
    "title"     => "Date"
  )


);

$data = sqlQueryData("SELECT news_id, title, text, date FROM ".$site_id."_news WHERE category_id=$category_id AND title<>'{%category_name%}' ORDER BY date DESC");

require($GLOBALS['cfgDirRoot']."library/"."class.datagrid.php");

$Table = new DataGrid();
$Table->cols = $cols;
$Table->data = $data;
$Table->footer = false;
$Table->rowformat = " onClick=\"javascript:HighLightTR(this, {%news_id%});\"";
echo $Table->output();


?>
