<?
if($GLOBALS['currentUserSiteID'])
{
  Header("Location: index.php?site_id=" . $GLOBALS['currentUserSiteID'] . "&module=" . defaultSiteModule);
  Die;
}


$action = $_GET['action'];
$perm_accessadmin = CheckPermission(5);

switch ($action) {

  case "truncate":
    $docScripts['exec'] = "profiler.exec.php";
    break;

  default:

    if($perm_accessadmin)
    {
//      DisplayToolbar();
    }
    $docTemplate = "main.htm";
    $docTitle = "{%langstr:profiler%}";
    $docScripts['body'] = "profiler.default.php";
    break;

}

?>
