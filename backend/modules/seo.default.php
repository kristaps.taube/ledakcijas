<div id='seo_wrap'>
<h1 id='seo_title'>{%langstr:seo_system_title%}</h1>

<?php

if(isset($GLOBALS['success']) && !isset($GLOBALS['response']['faultString']))
{
  ?>
  <div style='font-weight: bold;color:green;text-align:center'>
    {%langstr:seo_system_reg_successful%}<br /> <br />
    <a href='#' id='login'>{%langstr:seo_login%}</a>
  </div>
  <form id="login_form" action="http://admin.seoportal.eu/auth/" method="post" target="SEO" onsubmit="return thePopupWindows(this.target);">
    <input type='hidden' name='api_access_key' value="<?=Option("seo\\api_access_key")?>" />
  </form>

  <script type='text/javascript'>

    $("#login").click(function(){

       $("#login_form").trigger("submit");
       return false;

    });

    function thePopupWindows(windowsname){
      var win = window.open('', windowsname, 'width=1100,height=750');
      return true;
    }
  </script>
  <?
}else{

if(isset($GLOBALS['response']['faultString'])){

  if($GLOBALS['response']['faultCode'] == 4 )
  {

    echo "<div style='color:red;font-weight:bold;text-align:center'>{%langstr:seo_invalid_email%}</div>";

  } elseif( $GLOBALS['response']['faultCode'] == 5)
  {

    echo "<div style='color:red;font-weight:bold;text-align:center'>{%langstr:seo_email_inuse%}</div>";

  }else
  {
    var_dump($GLOBALS['response']);
    echo "<div style='color:red;font-weight:bold;text-align:center'>{%langstr:seo_system_reg_error%}</div>";

  }
}

if($GLOBALS['no_accept'])
{
  echo "<div style='color:red;font-weight:bold;text-align:center'>{%langstr:seo_didnt_accept_rules%}</div>";
}
?>

<img src='/cms/backend/gui/images/seo_chart.png' alt='seo'/>
<p>
  {%langstr:seo_system_description%}
</p>
<form method='post' action=''>
  <textarea style='width: 100%' rows='15'>{%langstr:seo_system_rules%}</textarea>
  <br /> <br />
  <label id='seo_email_label' for='email'>{%langstr:seo_email%}:</label> <input name='email' id='email' /> ({%langstr:seo_email_text%}) <br />
  <label id='seo_accept_label'>{%langstr:seo_must_accept_rules%}</label> <input type='checkbox' name='accept' /> <br /> <br />
  <input type='submit' id='seo_submit' value='{%langstr:seo_register%}' name='register' />
</form>
<?

}

?>

</div>
