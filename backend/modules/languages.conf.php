<?
$action = $_GET["action"];
$GLOBALS['site_id'] = $_GET["site_id"];
$site_id = $_GET["site_id"];
$GLOBALS['tabs'] = 'site';


unset($perm_modsite);
if(($site_id == $GLOBALS["currentUserSiteID"])or(!$GLOBALS["currentUserSiteID"]))
  $perm_modsite = CheckPermission(6, $site_id);
if(!$perm_modsite)
  AccessDenied(False);

unset($perm_managetemplates);
if(($site_id == $GLOBALS["currentUserSiteID"])or(!$GLOBALS["currentUserSiteID"]))
  $perm_managetemplates = CheckPermission(9, $site_id);



switch ($action) {

  case "create":
    $docTemplate = "form.htm";
    $GLOBALS['resizeauto'] = 1;
    $docTitle = "{%langstr:create_language%}";
    $docScripts['body'] = "languages.properties.php";
    break;

  // site properties form
  case "properties_form":
    $docTemplate = "form.htm";
    $GLOBALS['resizeauto'] = 1;
    $docTitle = "{%langstr:language_properties%}";
    $docScripts['body'] = "languages.properties.php";
    break;
    
  case "modlanguage":
  case "addlanguage":
    $docTemplate = "close.htm";
    $docScripts['exec'] = "languages.exec.php";
    break;
    
  case "dellanguage":
  case "setdefault":
    $docScripts['exec'] = "languages.exec.php";
    Header("Location: ?module=languages&site_id=" . $site_id);
    break;



  default:

    if($perm_managetemplates)
    {

      require($GLOBALS['cfgDirRoot']."library/"."class.toolbar2.php");
      $Toolbar = new Toolbar2();

      $Toolbar->AddSpacer();

      $Toolbar->AddButtonImage("add", 'lang_new', "{%langstr:toolbar_new_language%}", "", "javascript:openDialog('?module=languages&site_id=" . $GLOBALS['site_id'] . "&action=create', 400, 200, 0, 0);", 31, "{%langstr:toolbar_add_new_language%}");
      $Toolbar->AddButtonImage("props", 'lang_properties', "{%langstr:toolbar_language_properties%}", "", "javascript:if(SelectedRowID)openDialog('?module=languages&site_id=" . $GLOBALS['site_id'] . "&action=properties_form&id='+SelectedRowID, 400, 180, 0, 0);", 31, "{%langstr:toolbar_edit_language_properties%}");
      $Toolbar->AddButtonImage("def", 'lang_default', "{%langstr:toolbar_set_default%}", "javascript:if(SelectedRowID)window.location='?module=languages&site_id=" . $GLOBALS['site_id'] . "&action=setdefault&id='+SelectedRowID;", "", 31, "{%langstr:toolbar_make_selected_lang_default%}");
      $Toolbar->AddButtonImage("del", 'delete', "{%langstr:col_del%}", "javascript:if(SelectedRowID)window.location='?module=languages&site_id=" . $GLOBALS['site_id'] . "&action=dellanguage&id='+SelectedRowID;", '', 31, "{%langstr:toolbar_delete_selected_language%}", "{%langstr:sure_delete_language%}");

      $Toolbar->AddSeperator();

      $docStrings['toolbar'] =  $Toolbar->output();
    }
    $docTemplate = "main.htm";
    $docTitle = "Languages";
    $docScripts['body'] = "languages.default.php";
    break;


}

?>
