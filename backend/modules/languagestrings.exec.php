<?

$site_id = $GLOBALS['site_id'];

function ls_getLangStrings($component, $name, $fullrow=false)
{
  $langs = array();
  $langsdata = getLanguages();
  foreach ($langsdata as $lang => $row)
  {
    $langs[$row['language_id']] = $row;
  }

  $strings = array();
  $data = sqlQueryDataAssoc('SELECT * FROM '.$GLOBALS['site_id'].'_languagestrings WHERE component="'.$component.'" AND name="'.$name.'" AND value!=""');
  foreach ($data as $row)
  {
    $strings[$langs[$row['language']]['shortname']] = $fullrow ? $row : $row['value'];
  }

  return $strings;
}


switch ($_GET['action'])
{
  case 'getstrings':
    $langs = getLanguages();
    $strings = ls_getLangStrings($_POST['component'], $_POST['name']);

    $lstatuses = array();
    foreach ($langs as $lang => $lrow)
    {
      $lstatuses[$lang] = $strings[$lang];
    }

    echo json_encode(array(
      'strings' => $lstatuses
    ));
    die;

  case 'savestrings':

		//$data = json_decode($GLOBALS['HTTP_RAW_POST_DATA'], true);
    $data = json_decode(file_get_contents("php://input"), true);

		if (!$data) die;

		$noncmp_used = false;
		$langs = getLanguages();
		foreach ($data['strings'] as $cmp=>$strings){
			$cmp_name=($cmp=='_empty_')?'':$cmp;
			if ($cmp_name==''){
				$noncmp_used = true;
			}
			
			foreach ($strings as $lskey=>$ls){
				foreach ($ls as $lang=>$val){
					$ls_id = DB::GetValue('SELECT ls_id FROM '.$GLOBALS['site_id'].'_languagestrings WHERE component=:cmp AND name=:key AND language=:lang',array(':cmp'=>$cmp_name,':key'=>$lskey,':lang'=>$langs[$lang]['language_id']));
					if ($ls_id){
						if ($val===null){
							DB::Query('UPDATE '.$GLOBALS['site_id'].'_languagestrings SET value=`default` WHERE ls_id=:lsid',array(':lsid'=>$ls_id));
						}
						else{
							DB::Query('UPDATE '.$GLOBALS['site_id'].'_languagestrings SET value=:val WHERE ls_id=:lsid',array(':val'=>$val,':lsid'=>$ls_id));
						}
					}
					else if ($val!==null){
						DB::Query('INSERT INTO '.$GLOBALS['site_id'].'_languagestrings (component,name,language,value,`default`) values(:cmp,:key,:lang,:val,:val)',array(':cmp'=>$cmp_name,':key'=>$lskey,':lang'=>$langs[$lang]['language_id'],':val'=>$val));
					}
				}
			}
		}
		
		$res=DB::GetTable("select name,component,value,`default`,language from ".$GLOBALS['site_id']."_languagestrings order by language,component,name");
		$files=array();
		if (!file_exists(TRANSLATION_FOLDER)){
			mkdir(TRANSLATION_FOLDER);
		}
		foreach ($res as $r){
			if (!$files[$r['language'].'.'.$r['component']]){
				$files[$r['language'].'.'.$r['component']]=fopen(TRANSLATION_FOLDER.$r['language'].'.'.$r['component'].'.lang.php','w');
				fwrite($files[$r['language'].'.'.$r['component']],'<?php'."\n");
			}
			$value = $r['value'] ? $r['value'] : $r['default'];
			fwrite($files[$r['language'].'.'.$r['component']],'$languagestrings['.$r['language'].'][\''.$r['component'].'\'][\''.$r['name'].'\']=\''.str_replace("'","\'",str_replace('\\','\\\\',$value)).'\';'."\n");
		}
		foreach ($files as $k=>$f){
			fwrite($f,'?>');
			fclose($f);
		}
		
		if ($noncmp_used){
			sqlQuery('TRUNCATE TABLE '.$GLOBALS['site_id'].'_ctemplates');
		}
		
		die('1');


  case 'export_xls':
    function LS_colLetter($i)
    {
      return chr(ord('A')+$i);
    }

    $strings = array();
    $langs = getLanguages();
    foreach ($langs as $lang => $lrow)
    {
      $strings[$lang] = LS_getAllStrings($site_id, $lrow['language_id']);
    }

    require_once($GLOBALS['cfgDirRoot'].'library/PHPExcel.php');

    $excel = new PHPExcel();
    $excel->setActiveSheetIndex(0);
    $sheet = $excel->getActiveSheet();

    $style = array(
      'font' => array(
        'bold' => true
      )
    );

    $y = 1;
    $col = 2;
    $sheet->getColumnDimension('B')->setWidth(40);
    $sheet->setCellValue('B1', 'id' );

    foreach ($langs as $lang => $lrow)
    {
      $letter = LS_colLetter($col++);
      $sheet->setCellValue($letter.$y, strtoupper($lrow['shortname']) );
      $sheet->getColumnDimension($letter)->setWidth(40);
    }
    $y++;

    $ak = array_keys($strings);
    foreach($strings[$ak[0]]['compstrings'] as $cname => $cs)
    {
      $cname2 = ($cname != '_glotmp' ? $cname : '');

      $sheet->setCellValue('A'.$y, $cname2);
      $sheet->getStyle('A'.$y)->applyFromArray($style);
      $y++;

      foreach($cs as $cskey => $csval)
      {
        $col = 1;
        $letter = LS_colLetter($col++);
        $sheet->setCellValue($letter.$y, $cskey);
//        $sheet->mergeCells('A'.$y.':'.LS_colLetter(count($langs)).$y);

        foreach ($langs as $lang => $lrow)
        {
          $csval = $strings[$lang]['compstrings'][$cname][$cskey];

          $val = empty($csval) ? $compdefvals[$cname][$cskey] : $csval;
          $sheet->setCellValue(LS_colLetter($col++).$y, $val);
        }
        $y++;
      }

      $y++;
    }

    require_once($GLOBALS['cfgDirRoot'].'library/PHPExcel/IOFactory.php');

    header("Content-type: application/octet-stream");
    header('Content-Disposition: attachment; filename="translations_'.$_SERVER['SERVER_NAME'].'.xls"');
    header("Content-Transfer-Encoding: binary");

    $objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
    $objWriter->save('php://output');

    die;
    break;

}

?>