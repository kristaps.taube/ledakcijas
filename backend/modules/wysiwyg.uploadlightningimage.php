<?
$site_id = $_GET['site_id'];

if($_GET['close'] && $GLOBALS['lightningname'])
{
  echo '<script language="javascript">
  window.parent.document.all[\'Input\'].value = \'/images/small/'.$GLOBALS['lightningname'].'\';
  window.parent.document.all[\'InputBig\'].value = \'/images/big/'.$GLOBALS['lightningname'].'\';
  window.parent.OnOkClick();
  </script>';
}

$form_data = Array(

  "image"    => Array(
    "label"     => "Filename:",
    "size"      => "35",
    "type"      => "file",
    "uploadonly" => true
  ),

  "width"   => Array(
    "label"     => "Max width",
    "type"      => "str",
    "value"     => "700"
  ),

  "thumbwidth"  => Array(
    "label"     => "Max thumbnail width",
    "type"      => "str",
    "value"     => "350"
  )
);

require($GLOBALS['cfgDirRoot']."library/"."class.formgen.php");
$FormGen = new FormGen();
$FormGen->action = "?module=wysiwyg&action=savelightningimage&site_id=".$site_id."&close=1";
$FormGen->title = "Upload Image";
$FormGen->properties = $form_data;
$FormGen->buttons = false;
echo $FormGen->output();

?>