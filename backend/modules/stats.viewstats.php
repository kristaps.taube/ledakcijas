<?

if(!$GLOBALS['perm_accessstats'])
  Die('Access Forbidden');

if(isset($_GET['period']) && $_GET['period']!=55){
  $_SESSION['datefrom'] = '';
  $_SESSION['dateto'] = '';
}

if (!isset($_GET['period']) && !isset($_POST['periods']) )
    $_SESSION['period'] = 0;
elseif(($_SESSION['datefrom'] && $_SESSION['dateto']) || $_GET['period']==55 || $_POST['periods']==55)
    $_SESSION['period'] = 55;
else $_SESSION['period'] = $_GET['period'];

if($_POST['datefrom'])
  $_SESSION['datefrom'] = $_POST['datefrom'];

if($_POST['dateto'])
  $_SESSION['dateto'] = $_POST['dateto'];

$t = mktime(0, 0, 0, date("m")  , date("d"), date("Y"));
$y = mktime(0, 0, 0, date("m")  , date("d")-1, date("Y"));
$w = mktime(date("H"), date("i"), date("s"), date("m")  , date("d")-7, date("Y"));
$w2 = mktime(date("H"), date("i"), date("s"), date("m")  , date("d")-14, date("Y"));
$m = mktime(date("H"), date("i"), date("s"), date("m")-1  , date("d"), date("Y"));
$m2 = mktime(date("H"), date("i"), date("s"), date("m")-2  , date("d"), date("Y"));
$yr = mktime(date("H"), date("i"), date("s"), date("m")  , date("d"), date("Y")-1);

if($_POST["datefrom"])
  $datums = explode(' ', $_POST["datefrom"]);
elseif($_SESSION["datefrom"])
  $datums = explode(' ', $_SESSION["datefrom"]);

$date = explode('-',$datums[0]);
$time = explode(':',$datums[1]);

$dt1 = mktime($time[0], $time[1], $time[2], $date[1]  , $date[0], $date[2]);

if($_POST["datefrom"])
  $datums = explode(' ', $_POST["dateto"]);
elseif($_SESSION["datefrom"])
  $datums = explode(' ', $_SESSION["dateto"]);

$date = explode('-',$datums[0]);
$time = explode(':',$datums[1]);
$dt2 = mktime($time[0], $time[1], $time[2], $date[1]  , $date[0], $date[2]);

switch($_SESSION['period'])
{
    case 0: $condition = " time>".$t;
            break;
    case 1: $condition = " time>".$y." and time<".$t;
            break;
    case 2: $condition = " time>".$w;
            break;
    case 21: $condition = " time>".$w2." and time<".$w;
            break;
    case 3: $condition = " time>".$m;
            break;
    case 31: $condition = " time>".$m2." and time<".$m;
            break;
    case 4: $condition = " time>".$yr;
            break;
    case 55:$condition = ($dt1 && $dt2) ? " time>=".$dt1." and time<=".$dt2 : '';
            break;
}

//echo $condition;
//echo $_SESSION['period'];

if (!isset($_GET['page']))
    $page = 0;
else $page = $_GET['page'];

require_once($GLOBALS['cfgDirRoot']."library/"."func.files.php");

$site_id = $_GET['site_id'];
switch ($page)
{
    case 0:
        $cols = Array(
          "page"        => Array(
            "width"     => "50%",
            "title"     => "{%langstr:stats_pages%}"
          ),
          "hits"    => Array(
            "width"     => "15%",
            "title"     => "{%langstr:stats_hits%}"
          ),
          "unique"     => Array(
            "width"     => "20%",
            "title"     => "{%langstr:stats_unique_hosts%}"
          )
        );
        break;
    case 1:
        $cols = Array(
          "image"       => Array(
            "width"     => "1%",
            "title"     => ""
          ),
          "referrers"        => Array(
            "width"     => "50%",
            "title"     => "{%langstr:stats_refferers%}"
          ),
          "quantity"    => Array(
            "width"     => "15%",
            "title"     => "{%langstr:stats_quantity%}"
          )
        );
        break;
    case 2:
        $cols = Array(
          "languages"        => Array(
            "width"     => "50%",
            "title"     => "{%langstr:menu_languages%}"
          ),
          "count"    => Array(
            "width"     => "15%",
            "title"     => "{%langstr:stats_count%}"
          ),
          "rate"     => Array(
            "width"     => "20%",
            "title"     => "%"
          )
        );
        break;
    case 3:
        $cols = Array(
          "agent"        => Array(
            "width"     => "50%",
            "title"     => "{%langstr:stats_user_agent%}"
          ),
          "count"    => Array(
            "width"     => "15%",
            "title"     => "{%langstr:stats_count%}"
          ),
          "rate"     => Array(
            "width"     => "20%",
            "title"     => "%"
          )
        );
        break;
    case 4:
        $cols = Array(
          "os"        => Array(
            "width"     => "50%",
            "title"     => "{%langstr:stats_os%}"
          ),
          "count"    => Array(
            "width"     => "15%",
            "title"     => "{%langstr:stats_count%}"
          ),
          "rate"     => Array(
            "width"     => "20%",
            "title"     => "%"
          )
        );
        break;
    case 5:
        $cols = Array(
          "browsers"        => Array(
            "width"     => "50%",
            "title"     => "{%langstr:stats_browsers%}"
          ),
          "count"    => Array(
            "width"     => "15%",
            "title"     => "{%langstr:stats_count%}"
          ),
          "rate"     => Array(
            "width"     => "20%",
            "title"     => "%"
          )
        );
        break;
    case 6:
        $cols = Array(
          "proxies"        => Array(
            "width"     => "50%",
            "title"     => "{%langstr:stats_proxies%}"
          ),
          "count"    => Array(
            "width"     => "15%",
            "title"     => "{%langstr:stats_count%}"
          )
        );
        break;
    case 7:
        $cols = Array(
          "ip"        => Array(
            "width"     => "15%",
            "title"     => "{%langstr:log_ip%}"
          ),
          "last"    => Array(
            "width"     => "20%",
            "title"     => "{%langstr:stats_last_visited_page%}"
          ),
          "lasttime"    => Array(
            "width"     => "15%",
            "title"     => "{%langstr:log_time%}"
          ),
          "ref"    => Array(
            "width"     => "15%",
            "title"     => "{%langstr:stats_refferer%}"
          ),
          "total"    => Array(
            "width"     => "15%",
            "title"     => "{%langstr:stats_total_hits%}"
          ),
          "time"    => Array(
            "width"     => "30%",
            "title"     => "{%langstr:stats_session_time%}"
          )
        );
        break;
      case 9:
        $cols = Array(
          "page"        => Array(
            "width"     => "50%",
            "title"     => "{%langstr:stats_pages%}"
          ),
          "hits"    => Array(
            "width"     => "15%",
            "title"     => "{%langstr:stats_hits%}"
          ),
          "unique"     => Array(
            "width"     => "20%",
            "title"     => "{%langstr:stats_unique_hosts%}"
          )
        );
        break;
      case 10:
                  $cols = Array(
          "path"        => Array(
            "width"     => "50%",
            "title"     => "{%langstr:stats_path%}"
          ),
          "count"    => Array(
            "width"     => "10%",
            "title"     => "{%langstr:stats_count%}"
          )
        );
        break;
      case 11:
          $cols = Array(
            "version"        => Array(
              "width"     => "50%",
              "title"     => "Flash player's version"
            ),
            "count"    => Array(
              "width"     => "15%",
              "title"     => "{%langstr:stats_count%}"
            ),
            "rate"     => Array(
              "width"     => "20%",
              "title"     => "%"
            )
          );
          break;

      case 12:
          $cols = Array(
           "image"       => Array(
              "width"     => "1%",
              "title"     => ""
            ),

           "image2"       => Array(
              "width"     => "1%",
              "title"     => ""
            ),

            "keywords"        => Array(
              "title"     => "{%langstr:menu_keywords%}",
              "width"     => "50%"

            ),
            "count"    => Array(
              "title"     => "{%langstr:stats_count%}",
              "width"     => "10%"
            ),
            "engine"     => Array(
              "title"     => "{%langstr:stats_search_engine%}",
              "width"     => "15%"
            )
          );
          break;


}

function bubble($a, $ind, $dif=0)
{
    $n = count($a)-$dif;
    for ($i=0; $i<$n-1; $i++) {
      for ($j=0; $j<$n-1-$i; $j++)
      {
        if ($a[$j+1][$ind] > $a[$j][$ind]) {  /* compare the two neighbors */
          $tmp = $a[$j];         /* swap a[j] and a[j+1]      */
          $a[$j] = $a[$j+1];
          $a[$j+1] = $tmp;

        }
      }
    }
    return $a;
}

/**
 * @name implodeAssoc($glue,$arr)
 * @description makes a string from an assiciative array
 * @parameter glue: the string to glue the parts of the array with
 * @parameter arr: array to implode
 */
function implodeAssoc($glue,$arr)
{
   $keys=array_keys($arr);
   $values=array_values($arr);

   return(implode($glue,$keys).$glue.implode($glue,$values));
};

/**
 * @name explodeAssoc($glue,$arr)
 * @description makes an assiciative array from a string
 * @parameter glue: the string to glue the parts of the array with
 * @parameter arr: array to explode
 */
function explodeAssoc($glue,$str)
{
   $arr=explode($glue,$str);

   $size=count($arr);

   for ($i=0; $i < $size/2; $i++)
       $out[$arr[$i]]=$arr[$i+($size/2)];

   return($out);
};

function single_diff(&$a1,&$a2)
{
  $r = array(); // return

  foreach ($a1 as $pl) // payload
  {
   if (! in_array($pl, $a2, true) )
     $r[] = $pl;
  }

  foreach ($a2 as $pl) // payload
  {
   if (! in_array($pl, $a1, true) && ! in_array($pl, $r, true) )
     $r[] = $pl;
  }
  return $r;
}




function pages($domain, $condition) {

  /*$data_hits = sqlQueryDataAssoc("
  SELECT ".$_GET['site_id']."_stats.page_id, count(ip) as 'hits'
  FROM ".$_GET['site_id']."_stats
  INNER JOIN ".$_GET['site_id']."_pages ON ".$_GET['site_id']."_pages.page_id=".$_GET['site_id']."_stats.page_id
  WHERE ".$condition." group by ".$_GET['site_id']."_stats.page_id order by hits desc");*/

  /*
SELECT page_path, count( p.sm_id )  AS  'hits', count(  DISTINCT m.ip )  AS  'uniq' FROM 11_stats_pages AS p, 11_stats_main AS m WHERE p.time > 1125349200 AND m.sm_id = p.sm_id GROUP  BY page_path ORDER  BY uniq DESC , hits DESC
  */

  /*$sql = "
  SELECT page_path, count(sm_id) AS 'hits', count( DISTINCT sm_id ) AS 'uniq'
  FROM ".$_GET['site_id']."_stats_pages
  WHERE ".$condition." GROUP BY page_path ORDER BY uniq DESC, hits DESC";*/

  $condition_large = str_replace("time","p.time",$condition);

  $sql = "
  SELECT p.page_path, count(p.sm_id) AS 'hits', count( DISTINCT m.ip ) AS 'uniq'
  FROM ".$_GET['site_id']."_stats_pages AS p, ".$_GET['site_id']."_stats_main AS m
  WHERE ".$condition_large." AND m.sm_id = p.sm_id GROUP BY page_path ORDER BY uniq DESC, hits DESC";

  //$result = mysql_unbuffered_query($sql);

  $e = sqlQueryData($sql);

  foreach(sqlQueryData($sql) as $r) {
       $data_hits[] = array("page_path" => $r[0], "hits" => $r[1], "unique" => $r[2]);
  }

	// dump($data_hits);
  // mysql_free_result($result);

  $total_hits = 0;
  $total_unique = 0;

  foreach ($data_hits as $key=>$row)
  {
    $data[] = array('page'   => "<a href='http://" . $domain . $row['page_path'] . "' target='_blank'>"
                              . "http://" . $domain . $row['page_path']
                              . "</a>",
                    'hits'   => $row['hits'],
                    'unique' => $row['unique']
    );

    $total_hits += $row['hits'];
  }


  $unique_hits = sqlQueryValue("SELECT count( DISTINCT ip ) FROM ".$_GET['site_id']."_stats_main WHERE ".$condition);

  $data[] = array('page'   => "<b>{%langstr:stats_total%}</b>",
                  'hits'   => "<b>" . $total_hits . "</b>",
                  'unique' => "<b>" . $unique_hits . "</b>"
  );

  return $data;
}

function unvisitedpages($domain, $condition) {

  $pages_all_ids = DB::GetColumn("SELECT page_id FROM ".$_GET['site_id']."_pages ORDER BY ind");
  foreach ($pages_all_ids as $id){
     $pages_all[] = "/".PagePathById($id, $_GET['site_id']);
  }

  $sql = "
  SELECT DISTINCT page_path
  FROM ".$_GET['site_id']."_stats_pages
  WHERE ".$condition;

	foreach(sqlQueryData($sql) as $r){

		// this check should be removed
 		if (substr($r[0], -1) != "/")
			$slash = "/";
  	else
			$slash = "";

    $pages_visited[] = $r[0].$slash;
  }

  $diff = single_diff($pages_visited, $pages_all);

  foreach ($diff as $page_path){
    $data[] = array('page'   => "<a href='http://" . $domain . $page_path . "' target='_blank'>"
                               ."http://" . $domain . $page_path
                               ."</a>",
                    'hits'   => "0",
                    'unique' => "0"
    );
  }

  return $data;
}

function referrers($condition)
{
    global $page;
    $page = 1;

    //$condition = str_replace("time","timestamp",$condition);


    //$data = sqlQueryDataAssoc("SELECT referrer as 'referrers', count(ip) as 'quantity' FROM ".$_GET['site_id']."_stats WHERE referrer<>\"\" AND ".$condition." group by referrer order by quantity desc");

    //--------------

    $sql = "
    SELECT referrer, count(ip) as 'quantity'
    FROM ".$_GET['site_id']."_stats_main
    WHERE ".$condition." AND referrer<>'' GROUP BY referrer";

		foreach(sqlQueryData($sql) as $r){

        $url_data = parse_url($r[0]);

        if ($url_data['host'] == "") $url_data['host'] = "Unknown";

        //all referrers
        $refs_all[$url_data['host']][] = array('referrer' => $r[0], 'quantity' => $r[1] );


        $refs_quantity[$url_data['host']] += $r[1];


        if (!in_array($url_data['host'],$hosts))
        {
            //host array
            $hosts[] = $url_data['host'];
        }
    }

    //create output array
    foreach ($hosts as $host)
    {
        if ($_GET['open']==$host && $_GET['act']==0)
        {
            $image = $GLOBALS['cfgWebRoot']."gui/images/minus.gif";
            $act = 1;
        }
        else
        {
            $image = $GLOBALS['cfgWebRoot']."gui/images/plus.gif";
            $act = 0;
        }

        if ($refs_quantity[$host]>1)
        {
            //common refferer hosts
            $data[] = array('image'     => '<a name="' . $host . '"></a>'
                                         . '<a href="?module=stats&site_id=' . $_GET['site_id']
                                         . '&action=stats&period=' . $_SESSION['period'].'&page=' . $page . '&open=' . $host
                                         . '&act=' . $act . '#' . $host . '">'
                                         . '<IMG height=11 src="' . $image . '" width=11 align=absMiddle border=0></a>',
                            'referrers' => $host,
                            'quantity'  => $refs_quantity[$host]
            );
        }
        else
        {
            $refs_sub = $refs_all[$host];

            $data[] = array('image'     => '<IMG height=11 src="'.$GLOBALS['cfgWebRoot'].'gui/images/minus.gif" width=11 align=absMiddle border=0>',
                            'referrers' => '<a href="'.$refs_sub[0]['referrer'].'" target="_blank">'.$refs_sub[0]['referrer'].'</a>',
                            'quantity'  => $refs_quantity[$host]
            );
        }

        //all referrers for opened host
        if ($_GET['open']==$host && $_GET['act']==0)
        {
            if (count($refs_all[$host])>1)
            {
                //sort referrer sub array by quantity
                $refs_sub = bubble($refs_all[$host], "quantity");

                foreach($refs_sub as $key => $row)
                {
                    $sub = parse_url($row['referrer']);
                    $data[] = array('image'     => '',
                                    'referrers' => '<a href="'.$row['referrer'].'" target="_blank">'.$row['referrer'].'</a>',
                                    'quantity'  => $row['quantity']
                    );
                }
            }
            else
            {
                array_pop($data);

                $row = $refs_all[$host][0];
                $sub = parse_url($row['referrer']);

                $data[] = array('image'     => '<IMG height=11 src="'.$GLOBALS['cfgWebRoot'].'gui/images/minus.gif" width=11 align=absMiddle border=0>',
                                'referrers' => '<a href="'.$row['referrer'].'" target="_blank">'.$row['referrer'].'</a>',
                                'quantity'  => $row['quantity']
                );
            }
        }
        else
        {
            //show full referrer when same link for all host referrers
            if (count($refs_all[$host])==1)
            {
                array_pop($data);

                $row = $refs_all[$host][0];
                $sub = parse_url($row['referrer']);

                $data[] = array('image'     => '<IMG height=11 src="'.$GLOBALS['cfgWebRoot'].'gui/images/minus.gif" width=11 align=absMiddle border=0>',
                                'referrers' => '<a href="'.$row['referrer'].'" target="_blank">'.$row['referrer'].'</a>',
                                'quantity'  => $row['quantity']
                );
            }
        }
        
    }

    $no_ref = sqlQueryValue("SELECT count(ip) FROM ".$_GET['site_id']."_stats_main WHERE referrer=\"\" AND ".$condition);

    $data[] = array(
        'image'=>'',
        'referrers'=>'{%langstr:stats_no_referrer%}',
        'quantity'=>$no_ref
    );

    return $data;

}

function languages($condition)
{
    //get unique ip and languages for them
    //$data_all = sqlQueryDataAssoc("SELECT distinct ip,lang FROM `".$_GET['site_id']."_stats` WHERE ".$condition." order by ip desc");

    $total = 0;

    $sql = "SELECT count( DISTINCT ip) as 'quantity', lang FROM `".$_GET['site_id']."_stats_main` WHERE ".$condition." GROUP BY lang ORDER BY quantity DESC";
    foreach(sqlQueryData($sql) as $r){
        if (!$r[1]) $r[1] = "Unknown";

        $data[] = array('count' => $r[0], 'languages' => $r[1], 'rate' => 1);

        $total += $r[0];
    }

    foreach ($data as $key=>$row){
    	$data[$key]['rate'] = round( ($row['count'] / $total)*100, 0);
    }

    return $data;

}

function agents($condition){

    $total = 0;

    $sql = "SELECT user_agent, count(DISTINCT ip) AS 'c' FROM `".$_GET['site_id']."_stats_main` WHERE ".$condition." GROUP BY user_agent ORDER BY c desc";

		foreach(sqlQueryData($sql) as $r){
        $data[] = array('agent' => $r[0], 'count' => $r[1], 'rate' => 1);

        $total += $r[1];
    }

    foreach ($data as $key=>$row)
        $data[$key]['rate'] = round( ($row['count'] / $total)*100, 0);

    return $data;

}

function os($condition){

		require($GLOBALS['cfgDirRoot']."library/"."func.useragentinfo.php");

    //get unique ip and user agents for them
    $data_all = sqlQueryDataAssoc("SELECT distinct ip,user_agent FROM `".$_GET['site_id']."_stats_main` WHERE ".$condition." order by ip desc");
    $total = 0;

    foreach ($data_all as $key=>$row){

        $os = browser_detection("os",$row['user_agent']);
        if ($os=="win") $os = "Windows";
        if ($os=="mac") $os = "Mac OS";
        $os_number = browser_detection("os_number",$row['user_agent']);
        //echo $os." ".$os_number;

        $total++;
        $k = -1;
        foreach ($data as $key2=>$row2)
            if ($row2['os'] == $os." ".$os_number) $k = $key2;
        if ($k==-1)
            $data[] = array('os' => $os." ".$os_number,
                            'count' => 1,
                            'rate' => 1
            );
        else
            $data[$k]['count'] = $data[$k]['count'] + 1;
    }
    foreach ($data as $key=>$row)
        $data[$key]['rate'] = round( ($row['count'] / $total)*100, 0);

    foreach ($data as $key=>$val)
    {
        if ($val['os']==" " || $val['os']=="")
        {
            $k1 = $key;
            $data[$key]['os'] = "Unknown";
        }
    }

    if ($k1)
    {
        $tmp = $data[$k1];
        $data[$k1] = $data[count($data)-1];
        $data[count($data)-1] = $tmp;
    }

    $data = bubble($data, "count", 1);

    return $data;
}

function flashPlayerVersion($condition)
{
    //get client's flash version
    $data_all = sqlQueryDataAssoc("SELECT distinct(flashversion) as 'version', COUNT('version') as 'count' FROM `".$_GET['site_id']."_stats_main` WHERE ".$condition." GROUP BY flashversion");
    $count = 0;
    foreach ($data_all as $key=>$row)
    {
      if($row['version'] && $row['count'])
        $count += $row['count'];
    }

    foreach ($data_all as $key=>$row)
    {
      if($row['version'] && $row['count']) {
        $rate = round(($row['count']/$count)*100);
        $data [] = Array("version" => $row['version'], "count" => $row['count'], "rate" => $rate);
      }
    }

    $data = bubble($data, "count", 0);

    return $data;
}

function keywords($condition)
{
  $data_all = sqlQueryDataAssoc("SELECT keywords, COUNT(keywords) AS count FROM `".$_GET['site_id']."_stats_keywords` WHERE ".$condition." GROUP BY keywords");  

  $g = $_GET;
  $g['keywords'] = '';
  $url = '?'.http_build_query($g);
  foreach ($data_all as $row)
  {
    $count = $row['count'];
    $k = '';
    if ($count > 1)
    {
      if ($_GET['keywords'] == $row['keywords'])
        $k = '<a href="'.$url.'"><img src="gui/images/minus.gif"/></a>';
      else
        $k = '<a href="'.$url.urlencode($row['keywords']).'"><img src="gui/images/plus.gif"/></a>';

      $row['engine'] = '';
    }

    $data [] = Array(
      "image" => $k,
      "keywords" => '<a href="'.$row['referer'].'" target="_blank">'.$row['keywords'].'</a>',
      "count" => $count,
      "engine" => $row['engine'],
      'kw' => $row['keywords']
    );
  }

  $data = bubble($data, "count", 0);

  for ($i=0; $i<count($data); $i++)
  {
    if ($_GET['keywords'] == $data[$i]['kw'])
    {
      $sdata = sqlQueryDataAssoc("SELECT *, COUNT(engine) AS count FROM `".$_GET['site_id']."_stats_keywords` WHERE ".$condition." AND keywords='".addslashes($data[$i]['kw'])."' GROUP BY engine");
      $j = 1;
      $subdata = Array();
      foreach ($sdata as $srow)
      {
        $k = '&nbsp;&nbsp;&nbsp;'.$j++.'. <a href="'.$srow['referer'].'" target="_blank">'.$srow['keywords'].'</a>';
        $img = '';
/*        $url2 = $url.'&keywords='.urlencode($_GET['keywords']).'&engine=';
        if ($srow['count'] > 1)
        {
          if ($_GET['engine'] == $row['engine'])
            $img = '<a href="'.$url2.'"><img src="gui/images/minus.gif"/></a>';
          else
            $img = '<a href="'.$url2.urlencode($row['engine']).'"><img src="gui/images/plus.gif"/></a>';

        }
  */
        $subdata [] = Array("image" => '', 'image2' => $img, "keywords" => $k, "count" => $srow['count'], "engine" => $srow['engine']);
      }

      $subdata = bubble($subdata, "count", 0);
/*
      for ($j=0; $j<count($subdata); $j++)
      {
        if ($_GET['engine'] == $subdata[$j]['engine'])
        {
          $edata = sqlQueryDataAssoc("SELECT * FROM `".$_GET['site_id']."_stats_keywords` WHERE ".$condition." AND keywords='".addslashes($data[$i]['kw'])."' AND engine='".addslashes($subdata[$j]['engine'])."'");
          $m = 1;
          $subdata2 = Array();
          foreach ($edata as $erow)
          {
            $k = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="'.$erow['referer'].'" target="_blank">'.$m++.'. '.$erow['keywords'].'</a>';
            $subdata2[] = Array("image" => '', 'image2' => '', "keywords" => $k, "count" => 1, "engine" => $erow['engine']);
          }

          $subdata = array_merge(
            array_slice($subdata, 0, $j+1),
            $subdata2,
            array_slice($subdata, $j+1)
          );
          break;
        }
      }
        */
      $data = array_merge(
        array_slice($data, 0, $i+1),
        $subdata,
        array_slice($data, $i+1)
      );
      break;
    }
  }

  return $data;
}


function browsers($condition)
{
    require($GLOBALS['cfgDirRoot']."library/"."func.useragentinfo.php");

    //get unique ip and user agents for them
    $data_all = sqlQueryDataAssoc("SELECT distinct ip,user_agent FROM `".$_GET['site_id']."_stats_main` WHERE ".$condition);
    $total = 0;
    foreach ($data_all as $key=>$row)
    {
        $browser = browser_detection("browser",$row['user_agent']);
        $browser_number = browser_detection("number",$row['user_agent']);

        $total++;
        $k = -1;
        foreach ($data as $key2=>$row2)
            if ($row2['browsers'] == $browser." ".$browser_number) $k = $key2;
        if ($k==-1)
            $data[] = array('browsers' => $browser." ".$browser_number,
                            'count' => 1,
                            'rate' => 1
            );
        else
            $data[$k]['count'] = $data[$k]['count'] + 1;
    }
    foreach ($data as $key=>$row)
        $data[$key]['rate'] = round( ($row['count'] / $total)*100, 0);

    foreach ($data as $key=>$val)
    {
        if ($val['browsers']==" " || $val['browsers']=="")
        {
            $k1 = $key;
            $data[$key]['browsers'] = "Unknown";
        }
    }

    if ($k1)
    {
        $tmp = $data[$k1];
        $data[$k1] = $data[count($data)-1];
        $data[count($data)-1] = $tmp;
    }

    $data = bubble($data, "count", 1);

    return $data;
}

function proxies($condition){

	$sql = "SELECT distinct ip as 'proxies', count(sm_id) as 'count' FROM `".$_GET['site_id']."_stats_main` WHERE is_proxy=1 AND ".$condition." GROUP BY ip order by count desc";
	foreach(sqlQueryData($sql) as $r){
   	$data[] = array('proxies' => $r[0], 'count' => $r[1]);
  }

	return $data;
}

function visitors($domain){

	    $time = time() - 60 * 30;

    $sql = "SELECT p.sm_id as 'sm_id', p.page_path, p.time, m.ip, m.referrer, m.user_agent  "
          ."FROM `".$_GET['site_id']."_stats_pages` AS p, `".$_GET['site_id']."_stats_main` AS m "
          ."WHERE p.time>".$time." AND p.sm_id=m.sm_id ORDER BY p.sm_id DESC, p.time ASC";

    foreach(sqlQueryData($sql) as $r){
        $data_all_visited[] = array('sm_id' => $r[0], 'page_path' => $r[1], 'time' => $r[2], 'ip' => $r[3], 'ref' => $r[4], 'agent' => $r[5]);
    }

    foreach ($data_all_visited as $key=>$row)
    {
        if (!isset($visitors[$row['sm_id']]['begin_time']))
        {
            $visitors[$row['sm_id']]['begin_time'] = $row['time'];
            $visitors[$row['sm_id']]['time'] = 0;
        }
        else if ($row['time']<$visitors[$row['sm_id']]['begin_time'])
        {
            $visitors[$row['sm_id']]['begin_time'] = $row['time'];

            $sess_time = "";

            $session = $visitors[$row['sm_id']]['end_time'] - $visitors[$row['sm_id']]['begin_time'];
            $sess_time = round($session / 60, 0) . " min, ";
            $sess_time .= ($session%60) . " sec";

            $visitors[$row['sm_id']]['time'] = $sess_time;
        }
        if (!isset($visitors[$row['sm_id']]['end_time']))
        {
            $visitors[$row['sm_id']]['end_time'] = $row['time'];
            $visitors[$row['sm_id']]['time'] = 0;
        }
        else if ($row['time']>$visitors[$row['sm_id']]['end_time'])
        {
            $visitors[$row['sm_id']]['end_time'] = $row['time'];

            $sess_time = "";
            $session = $visitors[$row['sm_id']]['end_time'] - $visitors[$row['sm_id']]['begin_time'];
            $sess_time = round($session / 60, 0) . " min, ";
            $sess_time .= ($session%60) . " sec";

            $visitors[$row['sm_id']]['time'] = $sess_time;
        }

        if ($row['ref'] && !isset($visitors[$row['sm_id']]['ref'])) $visitors[$row['sm_id']]['ref'] = $row['ref'];

        if (!isset($visitors[$row['sm_id']]['ip']))
        {
            $visitors[$row['sm_id']]['ip'] = '<a href="javascript:void(0);" OnClick="openDialog(\'?module=stats&action=user_stats&site_id='.$_GET['site_id'].'&sm_id='.$row['sm_id'].'\', 600, 600,0,1);">'.$row['ip'].'</a>';
        }

        if (!isset($visitors[$row['sm_id']]['ua'])) $visitors[$row['sm_id']]['ua'] = $row['agent'];

        //total hists fot user session
        if (!$visitors[$row['sm_id']]['total']) $visitors[$row['sm_id']]['total'] = 1;
        else $visitors[$row['sm_id']]['total'] += 1;

        //last visited page
        $visitors[$row['sm_id']]['last'] = '<a target="_blank" href="http://'.$domain.$row['page_path'].'">http://'.$domain.$row['page_path'].'</a>';

        //last visited page time
        $dif = time() - $row['time'];
        $min = "";
        if (round($dif / 60, 0)>0) $min = round($dif / 60, 0).' min ago';
        else $min .=  (time() - $row['time']). ' sec ago';
        $visitors[$row['sm_id']]['lasttime'] = $min;

    }

    return $visitors;

}

function paths($domain, $condition){
    /*$data = sqlQueryDataAssoc("select ip, page_id, timestamp, referrer from `".$_GET['site_id']."_stats` where timestamp>".$time." group by ip");*/

    //$data_all = sqlQueryDataAssoc("select * from `".$_GET['site_id']."_stats` where ".$condition." AND is_entry=1 order by timestamp asc");

    //$condition_large = str_replace("time","p.time",$condition);

	$sql = "SELECT sm_id, page_path, time "
	."FROM `" . $_GET['site_id'] . "_stats_pages` "
	."WHERE " . $condition. " ORDER BY sm_id DESC, time ASC";

	foreach(sqlQueryData($sql) as $r){
		//this check should be removed
		if (substr($r[1], -1)!="/")
			$slash = "/";
		else
			$slash = "";

		$data_all_visited[] = array('sm_id' => $r[0], 'page_path' => $r[1] . $slash, 'time' => $r[2]);
	}
    //store all paths
    foreach ($data_all_visited as $key=>$row)
    {
        if ($user_path[$row['sm_id']]) $pref = ";";
        else $pref = "";

        $user_path[$row['sm_id']] .= $pref . $row['page_path'];
    }


    foreach ($user_path as $sm_id => $path)
    {
        if (!in_array($path,$paths))
        {
            $paths[$path]++;
        }
    }

    asort($paths);
    arsort($paths);


    //echo '<pre>'; print_r($paths);

    //create output
    foreach ($paths as $path=>$count)
    {
        //check if path contains more than one page
        if (strpos($path,";")!==false)
        {
            $current_path = explode(";",$path);
            $p = "";
            foreach ($current_path as $page_path)
            {
                if (!$p)
                {
                    $p = "<span style='color:red;'>></span>&nbsp;<a target=\"_blank\" href='http://".$domain.$page_path."'>http://".$domain.$page_path."</a>";
                }
                else
                {
                    $p .= "<br>".str_repeat("&nbsp;",4)."<a target=\"_blank\" href='http://".$domain.$page_path."'>http://".$domain.$page_path."</a>";
                }
            }
            $data[] = array('path' => $p, 'count' => $count);
        }
    }

    return array_slice($data,0,20); //return top 20

}


$domain = sqlQueryValue("SELECT domain FROM sites WHERE site_id=" . $_GET['site_id']);
if ($condition) switch ($page)
{
    case 0:
        $data = pages($domain, $condition);
        break;
    case 1:
        $data = referrers($condition);
        break;
    case 2:
        $data = languages($condition);
        break;
    case 3:
        $data = agents($condition);
        break;
    case 4:
        $data = os($condition);
        break;
    case 5:
        $data = browsers($condition);
        break;
    case 6:
        $data = proxies($condition);
        break;
    case 7:
        $data = visitors($domain);
        break;
    case 9:
        $data = unvisitedpages($domain, $condition);
        break;
    case 10:
        $data = paths($domain, $condition);
        break;
    case 11:
        $data = flashPlayerVersion($condition);
        break;
    case 12:
        $data = keywords($condition);
        break;


}

echo '
<script language="JavaScript">
  var selrow = ' . (int) ($data[0][0]) . ';
  function SelectPeriod()
  {                   document.location.href="?module=stats&site_id='.$_GET['site_id'].'&action='.$page.'&period="+document.getElementById(\'period\').value+"&page='.$page.'";
  }
</script>';

require($GLOBALS['cfgDirRoot']."library/"."class.datagrid.php");

$Table = new DataGrid();
$Table->cols = $cols;
$Table->data = $data;
$Table->footer = false;
$Table->padding = "2";
$Table->rowformat = " onClick=\"javascript:HighLightTR(this, '{%title%}');if(SelectedRowID){ EnableToolbarButtons(che); }else { DisableToolbarButtons(che); }\"";

echo '<table width="100%">';
echo '<tr><td width="230" valign="top"><form id="dateselecter" name="dateselecter">';
echo '<p align="left" style="padding-bottom:10px;">'
     .'<span style="white-space:nowrap;">{%langstr:stats_time_period%}:&nbsp;&nbsp;&nbsp;<select name="period" id="period" onChange="SelectPeriod()">'
     .'<option value="0"'; if ($_SESSION['period']==0) echo ' selected'; echo '>{%langstr:today%}</option>'
     .'<option value="1"'; if ($_SESSION['period']==1) echo ' selected'; echo '>{%langstr:yesterday%}</option>'
     .'<option value="2"'; if ($_SESSION['period']==2) echo ' selected'; echo '>{%langstr:this_week%}</option>'
     .'<option value="21"'; if ($_SESSION['period']==21) echo ' selected'; echo '>{%langstr:last_week%}</option>';

     if ($GLOBALS['cfgStatiscticsPeriod']==1 || $GLOBALS['cfgStatiscticsPeriod']==2)
     {
     echo '<option value="3"'; if ($_SESSION['period']==3) echo ' selected'; echo '>{%langstr:this_month%}</option>';
     echo '<option value="31"'; if ($_SESSION['period']==31) echo ' selected'; echo '>{%langstr:last_month%}</option>';
     }

     if ($GLOBALS['cfgStatiscticsPeriod']==2)
     echo '<option value="4"'; if ($_SESSION['period']==4) echo ' selected'; echo '>{%langstr:last_year%}</option>';

     echo '<option value="55"'; if ($_SESSION['period']==55) echo ' selected'; echo '>Lietotāja datums</option>';
echo '</select><a href="'.$GLOBALS['cfgWebRoot'].'help/help.htm?category=statistika" title="{%langstr:toolbar_help%}" target="_blank">
      &nbsp;&nbsp;<img src="'.$GLOBALS['cfgWebRoot'].'gui/img/navigation/help.gif" style="width: 23px; height:23px; border:0px solid black;" >
      </a></form>';

/*
CALENDAR
*/

echo '
  <script type="text/javascript" src="'.$GLOBALS['cfgWebRoot'].'gui/calendarfiles/calendar.js"></script>
  <script type="text/javascript" src="'.$GLOBALS['cfgWebRoot'].'gui/calendarfiles/calendar-en.js"></script>
  <script type="text/javascript" src="'.$GLOBALS['cfgWebRoot'].'gui/calendarfiles/calendar-setup.js"></script>

<table cellspacing="0" border=0 cellpadding="0" style="border-collapse: collapse; width: auto;"><tr valign="top">
 <td>&nbsp;No:&nbsp;&nbsp;<input type="text" name="date_from" id="f_date_b" value="'.$_SESSION["datefrom"].'"/></td>
 <td style="padding-top:4px;">&nbsp;<img src="'.$GLOBALS['cfgWebRoot'].'gui/calendarfiles/img.gif" id="f_trigger_b" style="cursor: pointer; border: 1px solid none;" title="Date selector"
      onmouseover="this.style.background=\'red\';" onmouseout="this.style.background=\'\'" /></td>
<script type="text/javascript">
    function changeDatesList() {
      document.forms[\'dateselecter\'].period.options[7].selected=true;
    }
    Calendar.setup({
        inputField     :    "f_date_b",      // id of the input field
        ifFormat       :    "%d-%m-%Y 00:00:00",       // format of the input field
        showsTime      :    true,            // will display a time selector
        button         :    "f_trigger_b",   // trigger for the calendar (button ID)
        singleClick    :    true,           // double-click mode
        step           :    1,              // show all years in drop-down boxes (instead of every other year as default)
        onUpdate       :    changeDatesList  //Update list with times and set it to custom
    });
</script>


 <td>&nbsp;&nbsp;&nbsp;&nbsp;Līdz:&nbsp;&nbsp;<input type="text" name="date_to" id="f_date_c" value="'.$_SESSION["dateto"].'"/></td>
 <td style="padding-top:4px;">&nbsp;<img src="'.$GLOBALS['cfgWebRoot'].'gui/calendarfiles/img.gif" id="f_trigger_c" style="cursor: pointer; border: 1px solid none;" title="Date selector"
      onmouseover="this.style.background=\'red\';" onmouseout="this.style.background=\'\'" /></td>
<td valign="left" align="middle">
<script type="text/javascript">
    Calendar.setup({
        inputField     :    "f_date_c",      // id of the input field
        ifFormat       :    "%d-%m-%Y 23:59:59",       // format of the input field
        showsTime      :    true,            // will display a time selector
        button         :    "f_trigger_c",   // trigger for the calendar (button ID)
        singleClick    :    true,           // double-click mode
        step           :    1,               // show all years in drop-down boxes (instead of every other year as default)
        onUpdate       :    changeDatesList  //Update list with times and set it to custom
    });
</script>

<form name="customdateform" id="customdateform" action="" method="POST">
<input type="hidden" name="datefrom" id="datefrom" />
<input type="hidden" name="periods" id="periods" value=55 />
<input type="hidden" name="dateto" id="dateto" />
&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" value="Atlasīt" onclick="javascript:
  document.getElementById(\'datefrom\').value = document.getElementById(\'f_date_b\').value;
  document.getElementById(\'dateto\').value = document.getElementById(\'f_date_c\').value;
  customdateform.submit();
" />
</form></td></tr></table>
';

echo '<tr><td valign="top">';
if ($page!=8) echo $Table->output();
if ($page==8){

$sql = "
  SELECT page_path, count(sm_id) AS 'hits'
  FROM ".$_GET['site_id']."_stats_pages
  WHERE ".$condition." GROUP BY page_path ORDER BY hits DESC LIMIT 5";

foreach(sqlQueryData($sql) as $r){
    $data_hits[] = array("page_path" => $r[0], "c" => $r[1]);
}

//echo '<pre>';print_r($data_hits);
foreach ($data_hits as $key=>$val){

   $sql = "SELECT time FROM " . $_GET['site_id'] . "_stats_pages WHERE " . $condition . " AND page_path='" . $val['page_path'] . "'";

   unset($time);
	 foreach(sqlQueryData($sql) as $r){
    $time[] = array("time" => $r[0]);
   }

  switch ($_SESSION['period'])
  {
     case 0:
         //create array for today
         foreach ($time as $k=>$v)
         {
             $h = date("G", $v['time']);
             $page_data[$key][$h]++;
         }
         break;
     case 1:
         //create array for yesterday
         foreach ($time as $k=>$v)
         {
             $h = date("G", $v['time']);
             $page_data[$key][$h]++;
         }
         break;
     case 2:
         //create array for this week
         foreach ($time as $k=>$v)
         {
             //$h = date("G", $v['timestamp']);
             $ind = 7 - (mktime(0,0,0,date("m"),date("d")+1,date("Y")) - $v['time'])/60/60/24 + 1;
             $page_data[$key][$ind]++;
         }
         break;
     case 21:
         //create array for last week
         foreach ($time as $k=>$v)
         {
             //$h = date("G", $v['timestamp']);
             $ind = 14 - (mktime(0,0,0,date("m"),date("d")+1,date("Y")) - $v['time'])/60/60/24 + 1;
             $page_data[$key][$ind]++;
         }
         break;
     case 3:
         //create array for a month
         //ja nav meenesha peedeejaa diena, tad dienu skaits ir ieprieksheejaa meenesha dienu skaits
         if (date("d")<date("d", mktime(0,0,0,date("m"),date("d")+1,date("Y"))))
             $days = date("t",mktime(0,0,0,date("m")-1,date("d"),date("Y")));
         else
             $days = date("t");

         foreach ($time as $k=>$v)
         {
             $ind = $days - (mktime(0,0,0,date("m"),date("d")+1,date("Y")) - $v['time'])/60/60/24 + 1;
             $page_data[$key][$ind]++;
         }
         break;
     case 31:
         //create array for a month
         if (date("d")<date("d", mktime(0,0,0,date("m"),date("d")+1,date("Y"))))
             $days_1 = date("t",mktime(0,0,0,date("m")-1,date("d"),date("Y")));
         else
             $days_1 = date("t");
         if (date("d", mktime(0,0,0,date("m")-1,date("d"),date("Y")))<date("d", mktime(0,0,0,date("m")-1,date("d")+1,date("Y"))))
             $days = date("t",mktime(0,0,0,date("m")-2,date("d"),date("Y")));
         else
             $days = date("t",mktime(0,0,0,date("m")-1,date("d"),date("Y")));

         foreach ($time as $k=>$v)
         {
             $ind = $days_1 + $days - (mktime(0,0,0,date("m"),date("d")+1,date("Y")) - $v['time'])/60/60/24 + 1;
             $page_data[$key][$ind]++;
         }
         break;
     case 4:
         //create array for a year
         foreach ($time as $k=>$v)
         {
             $dif = 12 - date("n");
             $ind = (date("n",$v['time'])+$dif) % 12;
             if ($ind==0 && date("y",$v['time'])==date("y")) $ind = 12;
             //if ($ind==0 && date("y",$v['timestamp'])<date("y")) $ind = 1;
             $page_data[$key][$ind]++;
         }
         break;
     case 55:
          //datuma sadalishana atkariba no lietotaja izveleta datumu intervala
          //12-02-2008 18:30:06
          $dat_no = explode(' ',$_SESSION["datefrom"]);
          $dat_lidz = explode(' ',$_SESSION["dateto"]);

          $datums_no = explode('-', $dat_no[0]);
          $datums_lidz = explode('-', $dat_lidz[0]);

          $days_lidz = mktime(0,0,0,$datums_lidz[1],$datums_lidz[0],$datums_lidz[2]);
          $days_no = mktime(0,0,0,$datums_no[1],$datums_no[0],$datums_no[2]);

          $day_count = abs(($days_lidz - $days_no) / (60*60*24));
          //////////////////////////////////////////////////////////////////////
          if(!$day_count || $day_count==1){ //create array for 1 day
            $day_count_mode = 1;
            foreach ($time as $k=>$v)
            {
              $h = date("G", $v['time']);
              $page_data[$key][$h]++;
            }
          }
          //////////////////////////////////////////////////////////////////////
          elseif($day_count>1 && $day_count<8){ //create array for 1 week
            $day_count_mode = 2;
             foreach ($time as $k=>$v)
             {
                 //$h = date("G", $v['timestamp']);
                 $ind = 7 - (mktime(0,0,0,date("m"),date("d")+1,date("Y")) - $v['time'])/60/60/24 + 1;
                 $page_data[$key][$ind]++;
             }
           }
          //////////////////////////////////////////////////////////////////////
          elseif($day_count>7 && $day_count<32){ //create array for 1 month
            $day_count_mode = 3;
             //ja nav meenesha peedeejaa diena, tad dienu skaits ir ieprieksheejaa meenesha dienu skaits
             if (date("d")<date("d", mktime(0,0,0,date("m"),date("d")+1,date("Y"))))
                 $days = date("t",mktime(0,0,0,date("m")-1,date("d"),date("Y")));
             else
                 $days = date("t");

             foreach ($time as $k=>$v)
             {
                 $ind = $days - (mktime(0,0,0,date("m"),date("d")+1,date("Y")) - $v['time'])/60/60/24 + 1;
                 $page_data[$key][$ind]++;
             }
           }
          //////////////////////////////////////////////////////////////////////
          elseif($day_count>31){ //create array for 1 year and more
            $day_count_mode = 4;
            foreach ($time as $k=>$v)
            {
                $dif = 12 - date("n");
                $ind = (date("n",$v['time'])+$dif) % 12;
                if ($ind==0 && date("y",$v['time'])==date("y")) $ind = 12;
                 //if ($ind==0 && date("y",$v['timestamp'])<date("y")) $ind = 1;
                 $page_data[$key][$ind]++;
            }
           }
          //////////////////////////////////////////////////////////////////////
          break;
    }
  }

   $sep = 0;
   for ($n=0; $n<=4; $n++)
    {
        if ($sep) $packed .= ';';
        $packed .= implodeAssoc(',',$page_data[$n]);
        $sep = 1;
    }

   //echo $packed;

   switch ($_SESSION['period'])
   {
        case 0:
            echo '<div>'.date("H:i:s - d.m.Y",$t). " - ".date("H:i:s - d.m.Y",mktime(0, 0, 0, date("m"),date("d")+1,date("Y"))).'</div><br>';
            break;
        case 1:
            echo '<div>'.date("H:i:s - d.m.Y",$y). " - ".date("H:i:s - d.m.Y",$t).'</div><br>';
            break;
        case 2:
            echo '<div>'.date("H:i:s - d.m.Y",$w). " - ".date("H:i:s - d.m.Y",time()).'</div><br>';
            break;
        case 21:
            echo '<div>'.date("H:i:s - d.m.Y",$w2). " - ".date("H:i:s - d.m.Y",$w).'</div><br>';
            break;
        case 3:
            echo '<div>'.date("H:i:s - d.m.Y",$m). " - ".date("H:i:s - d.m.Y",time()).'</div><br>';
            break;
        case 31:
            echo '<div>'.date("H:i:s - d.m.Y",$m2). " - ".date("H:i:s - d.m.Y",$m).'</div><br>';
            break;
        case 4:
            echo '<div>'.date("H:i:s - d.m.Y",$yr). " - ".date("H:i:s - d.m.Y",time()).'</div><br>';
            break;
        case 55:
            echo '<div>'.date("H:i:s - d.m.Y",$dt1). " - ".date("H:i:s - d.m.Y",$dt2).'</div><br>';
            break;
   }

    echo '<img src="?module=stats&action=graph&perdiod='.$_SESSION['period'].'&site_id='.$_GET['site_id'].'&data='.$packed.'&day_count_mode='.$day_count_mode.'">';

    $colors=array(0=>'FF0000',1=>'00FF00',2=>'0000FF',3=>'FFCC00',4=>'FF00FF');

    echo '<table width="400" cellspacing="5">';
    echo '<tr>
            <td></td>
            <td></td>
            <td><b>{%langstr:stats_page%}</b></td>
            <td style="padding-left:50px;"><b>{%langstr:stats_hits%}</b></td>
          </tr>';

    $i = 0;
    foreach ($data_hits as $key=>$row)
    {
        echo '<tr><td bgcolor="#'.$colors[$i].'" width="3"></td><td width="10"></td><td width="150">http://'.$domain.$row['page_path'].'</td><td style="padding-left:50px;">'.$row['c'].'</td></tr>';
        $i++;
    }
    echo '</table>';
}
echo '</td></tr>';
echo '</table>';

/*
//-----------------------------
//convert old stats

//$data = sqlQueryData("SELECT * FROM ".$_GET['site_id']."_stats");

$data_sm = sqlQueryData("SELECT IP,timestamp,referrer,user_agent,lang,is_proxy,page_id FROM ".$_GET['site_id']."_stats WHERE is_entry=1");


//print_R($data_sm);
foreach ($data_sm as $key=>$row)
{
    //sqlQuery("INSERT INTO 11_stats_main (session_id,ip,referrer,user_agent,lang,is_proxy,time)".
    //     "VALUES ('','".$row['IP']."','".$row['user_agent']."','".$row['lang']."','".$row['is_proxy']."','".$row['timestamp']."')");
    echo "INSERT INTO 11_stats_main (session_id,ip,referrer,user_agent,lang,is_proxy,time)".
         "VALUES ('','".$row['IP']."','".$row['user_agent']."','".$row['lang']."','".$row['is_proxy']."','".$row['timestamp']."')"."<br>";

    //$sm_id = sqlLastID();
    $page_path = pagePathById($row['page_id'],$_GET['site_id']);
    if ($page_path!="/") $page_path = "/".$page_path;

    //sqlQuery("INSERT INTO 11_stats_pages (sm_id,time,page_path) VALUES ('','".$row['timestamp']."','".$page_path."')");
    echo "&nbsp;&nbsp;&nbsp;INSERT INTO 11_stats_pages (sm_id,time,page_path) VALUES ('','".$row['timestamp']."','".$page_path."')"."<bR>";

    //get path for this visitor
    $data_sp = sqlQueryData("SELECT * FROM ".$_GET['site_id']."_stats WHERE IP='".$row['IP']."' AND user_agent='".$row['user_agent']."' AND lang='".$row['lang']."' AND timestamp>".$row['timestamp']." ORDER BY timestamp ASC LIMIT 50");

    foreach ($data_sp as $key2=>$row2)
    {
        if ($row2['is_entry'])
        {
            break;
        }
        else
        {
            $page_path = pagePathById($row2['page_id'],$_GET['site_id']);
            if ($page_path!="/") $page_path = "/".$page_path;

            //sqlQuery("INSERT INTO 11_stats_pages (sm_id,time,page_path) VALUES ('','".$row2['timestamp']."','".$page_path."')");
            echo "&nbsp;&nbsp;&nbsp;INSERT INTO 11_stats_pages (sm_id,time,page_path) VALUES ('','".$row2['timestamp']."','".$page_path."')"."<bR>";
        }
    }

    echo '<br> <br>';
}

//end of convert
//-----------------------------
*/

echo '<script language="JavaScript" type="text/javascript">
/*<![CDATA[*/
che = new Array("ren");
DisableToolbarButtons(che);
/*]]>*/
</script>';

?>
