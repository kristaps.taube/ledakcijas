<?
$action = $_GET["action"];
$perm_managegroups = CheckPermission(3);

unset($perm_managesitegroups);
if($_GET['site_id'])
{
  $GLOBALS['tabs'] = 'site';
  $site_id = $_GET['site_id'];
  if($site_id == $GLOBALS["currentUserSiteID"])
    $perm_managesitegroups = CheckPermission(10, $site_id);
}else if(($GLOBALS['currentUserSiteID'])and($GLOBALS['currentUserSiteID']!=$_GET['site_id']))
{
  Header("Location: index.php?site_id=" . $GLOBALS['currentUserSiteID'] . "&module=" . defaultSiteModule);
  Die;
}

$GLOBALS['PageEncoding'] = PageEncoding(0, 0);
$GLOBALS['maincharset']  = PageEncoding(0, 0, false);


switch ($action) {

  case "create":
    $docTemplate = "form.htm";
    $GLOBALS['resizeauto'] = 1;
    $docTitle = "{%langstr:create_group%}";
    $docScripts['body'] = "groups.properties.php";
    break;

  // group properties form
  case "properties_form":
    $docTemplate = "form.htm";
    $GLOBALS['resizeauto'] = 1;
    $docScripts['body'] = "groups.properties.php";
    break;

  case "close":
    $docTemplate = "close.htm";
    break;

  case "modgroup":
  case "addgroup":
    $docTemplate = "close.htm";
    $docScripts['exec'] = "groups.exec.php";
    break;

  case "delgroup":
    $docScripts['exec'] = "groups.exec.php";
    if($site_id)
      Header("Location: ?module=groups&site_id=$site_id");
    else
      Header("Location: ?module=groups");
    break;

  default:
  
    if(($perm_managegroups)or($perm_managesitegroups))
    {
      /*require($GLOBALS['cfgDirRoot']."library/"."class.toolbar.php");
      $Toolbar = new Toolbar();
      $Toolbar->AddButton("add", "New Group", "new_group.gif", "javascript:openDialog('?module=groups&action=create&site_id=$site_id', 400, 500, 0, 1);", "Creates new user group");
      $Toolbar->AddButton("props", "Group Properties", "edit_group.gif", "javascript:if(SelectedRowID)openDialog('?module=groups&action=properties_form&site_id=$site_id&id='+SelectedRowID, 400, 500, 0, 1);", "Allows to edit group properties");
      $Toolbar->AddLink("del", "Delete", "delete_group.gif", "javascript:if(SelectedRowID)window.location='?module=groups&action=delgroup&site_id=$site_id&id='+SelectedRowID", "Deletes selected user group", "Are you sure you want to delete selected user group?");
      if($site_id)
        $Toolbar->AddButton("spermissions", "Site Permissions", "edit_permissions.gif", "javascript:openDialog('?module=permissions&action=permission_frame&mod=site_form&hideitems=1&close=1&site_id=$site_id&data=$site_id&data2=&gid='+SelectedRowID, 800, 600, 0, 1);", "Edit web site access permissions");
      else
        $Toolbar->AddButton("permissions", "Access Permissions", "edit_permissions.gif", "javascript:openDialog('?module=permissions&action=permission_frame&hideitems=1&close=1&gid='+SelectedRowID, 800, 600, 0, 1);", "Edit global access permissions");
      $docStrings['toolbar'] = $Toolbar->output();*/

      require($GLOBALS['cfgDirRoot']."library/"."class.toolbar2.php");
      $Toolbar = new Toolbar2();

      $Toolbar->AddSpacer();

      $Toolbar->AddButtonImage("add", 'groups_new', "{%langstr:toolbar_new_group%}", "", "javascript:openDialog('?module=groups&action=create&site_id=$site_id', 400, 400, 0, 0);", 31, "{%langstr:toolbar_create_new_group%}");
      $Toolbar->AddButtonImage("props", 'groups_properties', "{%langstr:edit_group%}", "", "javascript:if(SelectedRowID)openDialog('?module=groups&action=properties_form&site_id=$site_id&id='+SelectedRowID, 400, 400, 0, 0);", 31, "{%langstr:toolbar_edit_group_properties%}");
      $Toolbar->AddSeperator();
      if($site_id)
        $Toolbar->AddButtonImage("spermissions", 'groups_site_permissions', "{%langstr:toolbar_site_permissions%}", "", "javascript:openDialog('?module=permissions&action=permission_frame&mod=site_form&hideitems=1&close=1&site_id=$site_id&data=$site_id&data2=&gid='+SelectedRowID, 800, 600, 0, 1);", 31, "{%langstr:toolbar_edit_site_permissions%}");
      else
        $Toolbar->AddButtonImage("permissions", 'groups_site_permissions', "{%langstr:toolbar_access_permissions%}", "", "javascript:openDialog('?module=permissions&action=permission_frame&hideitems=1&close=1&gid='+SelectedRowID, 800, 600, 0, 1);", 31, "{%langstr:toolbar_edit_global_permissions%}");
        $Toolbar->AddSeperator();
      $Toolbar->AddButtonImage("del", 'delete', "{%langstr:col_del%}", "javascript:if(SelectedRowID)window.location='?module=groups&action=delgroup&site_id=$site_id&id='+SelectedRowID", '', 31, "{%langstr:toolbar_delete_selected_group%}", "{%langstr:sure_delete_group%}");

      $Toolbar->AddSeperator();
      $Toolbar->AddButtonImage("help", 'help', "{%langstr:toolbar_help%}", $GLOBALS['cfgWebRoot']."help/help.htm?category=grupas", "", 31, "","","_blank");

      $docStrings['toolbar'] =  $Toolbar->output();
    }

    $docTemplate = "main.htm";
    $docTitle = "{%langstr:groups%}";
    $docScripts['body'] = "groups.default.php";
    break;
}

?>
