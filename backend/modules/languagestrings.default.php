<?php

    $url_data = $_GET;

    if(isset($_POST['save_language_string'])){

        $data = $_POST['texts'];
        $params = $_GET;
        $params['saved'] = 1;

        $translations = Constructor\Translation::getInstance();
        $translations->updateTexts($data)->saveTexts();

        header("location: ?".http_build_query($params));
        die();

    }

?>
<link rel="stylesheet" type="text/css" href="<?php echo Constructor\App::$app->getConfigValue('web_root')?>gui/translations.css" />
<div id='Translations'>
    <form method='get' action='?<?php echo http_build_query($url_data); ?>'>
        Meklēt: <input type='text' name='q' value='<?=htmlspecialchars(isset($_GET['q']) ? $_GET['q'] : '')?>' /> <input type='submit' value='Meklēt' name='search' />
        <input type='hidden' name='module' value='languagestrings' />
        <input type='hidden' name='site_id' value='<?php echo htmlspecialchars(isset($_GET['site_id']) ? $_GET['site_id'] : 0)?>' />
    </form>
<?php

if(!isset($_GET['q'])){
    echo "<div class='nosearch'>Ievadiet meklēšana parametru</div>";
}else{

    $translations = Constructor\Translation::getInstance();
    $texts = $_GET['q'] ? $translations->searchTexts($_GET['q']) : [];
    ?>
    <?php if(isset($_GET['saved']) && $_GET['saved']){ ?>
    <div class='success'>
        Saglabāts
    </div>
    <?php } ?>
    <div class='list'>
    <?php if($texts){?>
        <?php foreach($texts as $group => $grouped_texts){ ?>
            <?php foreach($grouped_texts as $default => $group_texts){ ?>
            <form method='post' action=''>
                <div class='text'>
                    <div class='title'>
                        <?php echo $default.' (Kategorija: '.$group.')' ?>
                    </div>
                    <div class='texts'>
                        <?php foreach(getLanguages($site_id) as $short => $lang){ ?>
                        <div class='row'>
                            <div class='lang'><?php echo $short.": " ?></div>
                            <div class='input'><input type='text' name='texts[<?php echo $group?>][<?php echo $default?>][<?php echo $short?>]' value='<?php echo htmlspecialchars(isset($group_texts[$short]) ? $group_texts[$short] : '')?>' /></div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
                <div class='submit'>
                    <input type='submit' name='save_language_string' value='Saglabāt' />
                </div>
            </form>
            <?php } ?>
        <?php } ?>
    <?php }else{ ?>
        <div class='noresults'>Nekas netika atrasts</div>
    <?php } ?>
    </div>
    <?
}

?>
</div>
