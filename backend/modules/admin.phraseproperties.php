<?

// Define form

$form_data = Array(

  "phrasename_id" => Array(
    "label"     => "ID:",
    "type"      => "hidden"
  ),

  "name"  => Array(
    "label"     => "{%langstr:admin_phrase_name%}:",
    "type"      => "str"
  ),


);

$GLOBALS['PageEncoding'] = PageEncoding(0, $site_id);
$GLOBALS['maincharset']  = PageEncoding(0, $site_id, false);

$langs = sqlQueryColumn("SELECT lang FROM phrases GROUP BY lang");
if(!$langs)
  $langs = array('');

$f = 1;
foreach($langs as $lang)
{
  $form_data['lang' . $f] = Array(
    "label"     => "{%langstr:admin_language%}:",
    "type"      => "hidden",
    "value"     => $lang
  );
  $form_data['phrase' . $f] = Array(
    "label"     => "{%langstr:admin_phrase_text%} (".($lang?$lang:'{%langstr:default_language%}')."):",
    "type"      => "html",
    "rows"      => "5",
    "cols"      => "30"
  );
  $f++;
}

// Fill some values
  $id = $_GET["id"];
  $action = $_GET['action'];
  $create = ($action == 'phrase_create');
  $site_id = $GLOBALS['site_id'];

  if(!$create){
    $row = sqlQueryRow("SELECT * FROM phrasenames WHERE phrasename_id=$id");
    $form_data['phrasename_id']['value'] = $id;
    $form_data['name']['value'] = $row['name'];
    $f = 1;
    foreach($langs as $lang)
    {
      $row2 = sqlQueryRow("SELECT * FROM phrases LEFT JOIN phrasenames ON phrasename=phrasename_id WHERE phrasename_id=$id AND lang='" . $lang . "'");
      $form_data['phrase_id' . $f] = Array(
        "label"     => "ID:",
        "type"      => "hidden",
        "value"     => $row2['phrase_id']
      );
      $form_data['phrase' . $f]['value'] = mb_convert_encoding($row2['phrase'], 'UTF-8', 'HTML-ENTITIES');
      $f++;
    }
  }
  else
  {
  }

  $invalid = explode(':', $_GET['invalid']);
  foreach($invalid as $i)
  {
    if($i)
      $form_data[$i]['error'] = true;
  }

  foreach(array_keys($_GET) as $key)
  {
    $s = explode('_', $key, 2);
    if($s[0] == 'formdata')
      $form_data[$s[1]]['value'] = stripslashes($_GET[$key]);
  }

  if($_GET['jsmessage'])
  {
    echo '<script language="JavaScript">
          <!--
          alert("' . stripslashes($_GET['jsmessage']) . '");
          <!---->
          </script>';
  }

// Use FormGen to bring this out

  require($GLOBALS['cfgDirRoot']."library/"."class.formgen.php");   
  $FormGen = new FormGen();
  if(!$create)
  {
    $FormGen->action = "?module=".$GLOBALS['module']."&site_id=".$site_id."&action=modphrase";
  }else{
    $FormGen->action = "?module=".$GLOBALS['module']."&site_id=".$site_id."&action=addphrase";
  }
  $FormGen->cancel = "close";
  if(!$create)
  {
    $FormGen->title = "{%langstr:admin_phrase_properties%} : " . $row['name'];
  }else{
    $FormGen->title = "{%langstr:toolbar_add_new_phrase%}";
  }
  $FormGen->properties = $form_data;
  $FormGen->buttons = true;
  echo $FormGen->output();


