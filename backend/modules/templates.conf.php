<?
$action = $_GET["action"];
$GLOBALS['site_id'] = $_GET["site_id"];
$site_id = $_GET["site_id"];
$GLOBALS['tabs'] = 'site';


unset($perm_modsite);
if(($site_id == $GLOBALS["currentUserSiteID"])or(!$GLOBALS["currentUserSiteID"]))
  $perm_modsite = CheckPermission(6, $site_id);
if(!$perm_modsite)
  AccessDenied(False);

unset($perm_managetemplates);
if(($site_id == $GLOBALS["currentUserSiteID"])or(!$GLOBALS["currentUserSiteID"]))
  $perm_managetemplates = CheckPermission(9, $site_id);

$GLOBALS['PageEncoding'] = PageEncoding(0, $site_id);
$GLOBALS['maincharset']  = PageEncoding(0, $site_id, false);

$docScripts['sellanguage'] = "sellanguage.default.php";
include($GLOBALS['cfgDirRoot']."backend/modules/"."sellanguage.setlanguage.php");
$GLOBALS['PageEncoding'] = FullEncoding($GLOBALS['currentlanguage']);
$GLOBALS['maincharset']  = $GLOBALS['currentlanguage'];



switch ($action) {


  case "create":
    $docTemplate = "form.htm";
    $GLOBALS['resizeauto'] = 1;
    $docTitle = "{%langstr:create_template%}";
    $docScripts['body'] = "templates.properties.php";
    break;

  // site properties form
  case "properties_form":
    $docTemplate = "form.htm";
    $GLOBALS['resizeauto'] = 1;
    $docScripts['body'] = "templates.properties.php";
    break;

  case "createview":
    $docTemplate = "form.htm";
    $GLOBALS['resizeauto'] = 0;
    $docTitle = "{%langstr:create_view%}";
    $docScripts['body'] = "templates.view.php";
    break;

  case "editview":
    $docTemplate = "form.htm";
    $GLOBALS['resizeauto'] = 0;
    $docTitle = "Edit View";
    $docScripts['body'] = "templates.view.php";
    break;

  case "components_form":
    $docTemplate = "form.htm";
    $docTitle = "{%langstr:default_values%}";
    $docScripts['body'] = "templates.components.php";
    $perm_managecontents = CheckPermission(12, $site_id, $_GET['id']);
    break;

  case "contents_form":
    $docTemplate = "templ_win.html";
    $docScripts['exec'] = "templates.contents.php";
    $docStrings['request_uri'] = $_SERVER['REQUEST_URI'];
    $perm_managecontents = CheckPermission(12, $site_id, $_GET['id']);
    break;

  case "close":
    $docTemplate = "close.htm";
    break;

  case "modtemplate":
  case "addtemplate":
  case "modcontents":
  case "updatecontrols":
  case "saveview":
  case "updateview":
    if (isset($_POST['url']) && !empty($_POST['url']))
    {
      $docTemplate = "templ_win.html";
      $docScripts['exec'] = "templates.exec.php";
      $perm_managecontents = CheckPermission(12, $site_id, $_GET['id']);
      header('Location: '.$_POST['url']);
    }
    else
    {
      $docTemplate = "close.htm";
      $docScripts['exec'] = "templates.exec.php";
      $perm_managecontents = CheckPermission(12, $site_id, $_GET['id']);
    }
    break;
  case "redirect_language_default":
  case "redirect_language_strings":
    $docScripts['exec'] = "templates.exec.php";
    break;

  case "deltemplate":
  case "delview":
  case "movetemplateup":
  case "movetemplatedown":
  case "moveviewup":
  case "moveviewdown":
    $docScripts['exec'] = "templates.exec.php";
    Header("Location: ?module=templates&site_id=" . $site_id);
    break;
  case "template_visual_frame":
    $docTemplate = "t_editor.htm";
    $domain = $GLOBALS['domain'];
    $url = parse_url($_SERVER[REQUEST_URI]);
    $url['query'] = str_replace("template_visual_frame", "template_graph", $url['query']);
    $docStrings['frame'] = "http://" . $domain . "/edit.php?" . $url['query'] . "&session_id=" . session_id() . "&session_name=" . session_name();
    $docStrings['base'] = "http://" . $domain . "/";
    $docStrings['formaction'] = "?module=templates&action=savevisualcontents&site_id=".$site_id."&template_id=".$_GET["template_id"];
    $docStrings['oldhost'] = $GLOBALS['cfgWebRoot'];
    break;
  case "template_graph":
    $docTemplate = "editor.htm";
    $docTitle = "Edit Template";
    $docScripts['body'] = "templates.graphical.php";
    break;
  case "savevisualcontents":
    $docTemplate = "close.htm";
    $docScripts['exec'] = "templates.exec.php";
    break;
  case "editorjs":
    $docTemplate = "editor.htm";
    $docTitle = "Edit Template";
    $docScripts['body'] = "templates.editorjs.php";
    break;
  case "zipskin":
    $docTemplate = "nothing.htm";
    $docTitle = "Save skin";
    $docScripts['body'] = "templates.zipskin.php";
    break;

  default:

    if($perm_managetemplates)
    {
      $havestrings = $data = sqlQueryValue("SELECT Count(1) FROM " . $site_id . "_strings WHERE page=-1");
      /*require($GLOBALS['cfgDirRoot']."library/"."class.toolbar.php");
      $Toolbar = new Toolbar();
      $Toolbar2 = new Toolbar();
      $Toolbar2->prefix = 't2';
      $Toolbar->AddButton("add", "New Template", "new_template.gif", "javascript:openDialog('?module=templates&site_id=" . $GLOBALS['site_id'] . "&action=create', 400, 190, 0, 0);", "Add new Template");
      $Toolbar2->AddButton("add2", "New Template", "new_template.gif", "javascript:openDialog('?module=templates&site_id=" . $GLOBALS['site_id'] . "&action=create', 400, 190, 0, 0);", "Add new Template");
      //$Toolbar->AddButton("Properties", "edit_properties.gif", "javascript:if(SelectedRowID)openDialog('?module=templates&site_id=" . $GLOBALS['site_id'] . "&action=properties_form&id='+SelectedRowID, 400, 190, 0, 0);", "Edit Template properties");
      $Toolbar->AddButton("edit", "Template Contents", "edit_contents.gif", "javascript:if(SelectedRowID>0)openDialog('?module=templates&site_id=" . $GLOBALS['site_id'] . "&action=contents_form&id='+SelectedRowID, 620, 540, 1, 1);", "Edit Template contents");
      $Toolbar->AddButton("addv", "New View", "new_template.gif", "javascript:if(SelectedRowID>0)openDialog('?module=templates&site_id=" . $GLOBALS['site_id'] . "&action=createview&template_id='+SelectedRowID, 400, 390, 1, 1);", "Add new View for Template");
      $Toolbar2->AddButton("editv", "Edit View", "edit_properties.gif", "javascript:if(SelectedRowID<0)openDialog('?module=templates&site_id=" . $GLOBALS['site_id'] . "&action=editview&view_id='+SelectedRowID, 400, 390, 1, 1);", "Edit Template View");
      $Toolbar->AddButton("defval", "Default Values", "edit_contents2.gif", "javascript:if(SelectedRowID>0)openDialog('?module=templates&site_id=" . $GLOBALS['site_id'] . "&action=components_form&id='+SelectedRowID, 500, 520, 1, 1);", "Edit default values for selected Template");
      $Toolbar2->AddButton("defval2", "Default Values", "edit_contents2.gif", "javascript:if(SelectedRowID<0)openDialog('?module=pages&site_id=" . $GLOBALS['site_id'] . "&action=components_form_frame&id='+SelectedRowID, 500, 520, 1, 1);", "Edit default values for selected View");
      $Toolbar->AddLink("visedit", "Visual Edit", "edit_page2.gif", "javascript:if(SelectedRowID)openDialog('"."http://" . $domain . "/edit.php"."?module=templates&site_id=" . $GLOBALS['site_id'] . "&action=template_visual_frame&template_id='+SelectedRowID + '&session_id=".session_id(). "&session_name=" . session_name() ."', screen.availWidth, screen.availHeight-25, 1 , 1);", "Edit template in visual template editor");
      //$Toolbar2->AddLink("visedit", "Visual Edit", "edit_page2.gif", "javascript:if(SelectedRowID<0)openDialog('?module=pages&site_id=" . $GLOBALS['site_id'] . "&action=redirectto_graph&id='+SelectedRowID, screen.availWidth, screen.availHeight-25, 1 , 1);", "Edit view in visual editor");
      if($havestrings)
        $Toolbar2->AddButton("strings", "Strings", "edit_properties.gif", "javascript:if(SelectedRowID<0)openDialog('?module=strings&site_id=" . $GLOBALS['site_id'] . "&action=page_strings&id='+SelectedRowID, 400, 520, 1, 0);", "Edit strings for selected view");
      $Toolbar->AddButton("perms", "Template Permissions", "edit_permissions.gif", "javascript:if(SelectedRowID>0)openDialog('?module=permissions&action=permission_frame&mod=template_form&close=1&site_id=$site_id&data=$site_id&data2='+SelectedRowID+'&single_template=1', 800, 600, 0, 1);", "Edit selected template access permissions");
      $Toolbar->AddLink("up", "Up", "move_up.gif", "javascript:if(SelectedRowID)window.location='?module=templates&site_id=" . $GLOBALS['site_id'] . "&action=movetemplateup&selrow='+SelectedRowID+'&id='+SelectedRowID;", "");
      $Toolbar->AddLink("down", "Down", "move_down.gif", "javascript:if(SelectedRowID)window.location='?module=templates&site_id=" . $GLOBALS['site_id'] . "&action=movetemplatedown&selrow='+SelectedRowID+'&id='+SelectedRowID;", "");
      $Toolbar2->AddLink("up2", "Up", "move_up.gif", "javascript:if(SelectedRowID)window.location='?module=templates&site_id=" . $GLOBALS['site_id'] . "&action=moveviewup&selrow='+(-SelectedRowID)+'&id='+(-SelectedRowID);", "");
      $Toolbar2->AddLink("down2", "Down", "move_down.gif", "javascript:if(SelectedRowID)window.location='?module=templates&site_id=" . $GLOBALS['site_id'] . "&action=moveviewdown&selrow='+(-SelectedRowID)+'&id='+(-SelectedRowID);", "");
      $Toolbar->AddLink("del", "Delete", "delete_template.gif", "javascript:if(SelectedRowID>0)window.location='?module=templates&site_id=" . $GLOBALS['site_id'] . "&action=deltemplate&id='+SelectedRowID;", "Delete selected Template", "Are you sure you want to delete selected Template?");
      $Toolbar2->AddLink("del2", "Delete", "delete_template.gif", "javascript:if(SelectedRowID<0)window.location='?module=templates&site_id=" . $GLOBALS['site_id'] . "&action=delview&id='+(-SelectedRowID);", "Delete selected View", "Are you sure you want to delete selected View?");
      $docStrings['toolbar'] = '<div id="Toolbar1Div">' . $Toolbar->output() . '</div>'
                              .'<div id="Toolbar2Div" style="display:none;">' . $Toolbar2->output() . '</div>';
      */

      $havestrings = $data = sqlQueryValue("SELECT Count(1) FROM " . $site_id . "_strings WHERE page=-1");

      require($GLOBALS['cfgDirRoot']."library/"."class.toolbar2.php");
      $Toolbar = new Toolbar2();
      $Toolbar2 = new Toolbar2();
      $Toolbar2->prefix = 't2';

      $Toolbar->AddSpacer();

      $Toolbar->AddButtonImage('add', 'templates_new', '{%langstr:new_template%}', "", "javascript:openDialog('?module=templates&site_id=" . $GLOBALS['site_id'] . "&action=create', 400, 190, 0, 0);", 31, "{%langstr:add_new_template%}");
      $Toolbar->AddButtonImage('addv', 'templates_new_view', "{%langstr:new_view%}", "", "javascript:if(SelectedRowID>0)openDialog('?module=templates&site_id=" . $GLOBALS['site_id'] . "&action=createview&template_id='+SelectedRowID, 400, 390, 1, 1);", 31, "{%langstr:add_new_view_template%}");
      $Toolbar->AddSeperator();
      $Toolbar->AddButtonImage('edit', 'templates_contents', "{%langstr:template_contents%}", "", "javascript:if(SelectedRowID>0)openDialog('?module=templates&site_id=" . $GLOBALS['site_id'] . "&action=contents_form&id='+SelectedRowID, 620, 540, 1, 1);", 31, "{%langstr:edit_template_contents%}");
      $Toolbar->AddButtonImage('defval', 'templates_default_values', "{%langstr:default_values%}", "", "javascript:if(SelectedRowID>0)openDialog('?module=templates&site_id=" . $GLOBALS['site_id'] . "&action=components_form&id='+SelectedRowID, 500, 520, 1, 1);", 31, "{%langstr:edit_default_values_template%}");
      if($_SESSION['currentlanguagenum'])
        $Toolbar->AddButtonImage('langdefval', 'templates_def_lang_values', "{%langstr:default_language_values%}", "", "javascript:if(SelectedRowID>0)openDialog('?module=templates&site_id=" . $GLOBALS['site_id'] . "&action=redirect_language_default&id='+SelectedRowID, 500, 520, 1, 1);", 31, "{%langstr:edit_default_values_template_language%}");
      if($_SESSION['currentlanguagenum'] && $havestrings)
        $Toolbar->AddButtonImage('langdefstrings', 'templates_def_lang_strings', "{%langstr:default_language_strings%}", "", "javascript:if(SelectedRowID>0)openDialog('?module=templates&site_id=" . $GLOBALS['site_id'] . "&action=redirect_language_strings&id='+SelectedRowID, 500, 520, 1, 1);", 31, "{%langstr:edit_default_strings_template_language%}");
      //$Toolbar->AddButtonImage('visedit', 'sitemap_visual_edit', "{%langstr:visual_edit%}", "", "javascript:if(SelectedRowID)openDialog('"."http://" . $domain . "/edit.php"."?module=templates&site_id=" . $GLOBALS['site_id'] . "&action=template_visual_frame&template_id='+SelectedRowID + '&session_id=".session_id(). "&session_name=" . session_name() ."', screen.availWidth, screen.availHeight-25, 1 , 1);", 31, "{%langstr:edit_template_visual_editor%}");
      $Toolbar->AddSeperator();
      $Toolbar->AddButtonImage('perms', 'templates_permissions', "{%langstr:template_permissions%}", "", "javascript:if(SelectedRowID>0)openDialog('?module=permissions&action=permission_frame&mod=template_form&close=1&site_id=$site_id&data=$site_id&data2='+SelectedRowID+'&single_template=1', 800, 600, 0, 1);", 31, "{%langstr:edit_template_pemrissions%}");

      $Toolbar->AddSeperator();

      $Toolbar->AddButtonImage('up', 'up', "{%langstr:up%}", "javascript:if(SelectedRowID)window.location='?module=templates&site_id=" . $GLOBALS['site_id'] . "&action=movetemplateup&selrow='+SelectedRowID+'&id='+SelectedRowID;", "", 31, "{%langstr:move_selected_template_up%}");
      $Toolbar->AddButtonImage('down', 'down', "{%langstr:down%}", "javascript:if(SelectedRowID)window.location='?module=templates&site_id=" . $GLOBALS['site_id'] . "&action=movetemplatedown&selrow='+SelectedRowID+'&id='+SelectedRowID;", "", 31, "{%langstr:move_selected_template_down%}");
      $Toolbar->AddButtonImage('del', 'delete', "{%langstr:col_del%}", "javascript:if(SelectedRowID>0)window.location='?module=templates&site_id=" . $GLOBALS['site_id'] . "&action=deltemplate&id='+SelectedRowID;", "", 31, "{%langstr:delete_selected_template%}", "{%langstr:sure_delete_template%}");


      if ($GLOBALS['cfgMini'] && $_SESSION['userdata']['username'] == 'Admin')
//      if ($_GET['showzip'])
      {
        $Toolbar->AddButtonImage('zipskin', 'templates_zip', "{%langstr:tpl_skin_zip%}", "", "var sname=prompt('{%langstr:enter_skin_name%}', ''); if (sname && sname.length > 0)openDialog('?module=templates&site_id=" . $GLOBALS['site_id'] . "&action=zipskin&skinname='+sname, 500, 520, 1, 1);", 31);
      }

      $Toolbar->AddSeperator();

      $Toolbar2->AddSpacer();

      $Toolbar2->AddButtonImage('add2', 'templates_new', '{%langstr:new_template%}', "", "javascript:openDialog('?module=templates&site_id=" . $GLOBALS['site_id'] . "&action=create', 400, 190, 0, 0);", 31, "{%langstr:add_new_template%}");
      $Toolbar2->AddButtonImage('editv', 'sitemap_properties', '{%langstr:edit_view%}', "", "javascript:if(SelectedRowID<0)openDialog('?module=templates&site_id=" . $GLOBALS['site_id'] . "&action=editview&view_id='+SelectedRowID, 400, 390, 1, 1);", 31, "{%langstr:edit_template_view%}");
      $Toolbar2->AddSeperator();
      $Toolbar2->AddButtonImage('defval2', 'sitemap_standart_edit', "{%langstr:default_values%}", "", "javascript:if(SelectedRowID<0)openDialog('?module=pages&site_id=" . $GLOBALS['site_id'] . "&action=components_form_frame&id='+SelectedRowID, 500, 520, 1, 1);", 31, "{%langstr:edit_default_values_selected_view%}");
      if($havestrings)
        $Toolbar2->AddButtonImage('strings', 'sitemap_strings', "{%langstr:edit_strings%}", "", "javascript:if(SelectedRowID<0)openDialog('?module=strings&site_id=" . $GLOBALS['site_id'] . "&action=page_strings&id='+SelectedRowID, 400, 520, 1, 0);", 31, "{%langstr:edit_string_selected_view%}");

      $Toolbar2->AddSeperator();

      $Toolbar2->AddButtonImage('up2', 'up', "{%langstr:up%}", "javascript:if(SelectedRowID)window.location='?module=templates&site_id=" . $GLOBALS['site_id'] . "&action=moveviewup&selrow='+(-SelectedRowID)+'&id='+(-SelectedRowID);", "", 31, "{%langstr:move_selected_view_up%}");
      $Toolbar2->AddButtonImage('down2', 'down', "{%langstr:down%}", "javascript:if(SelectedRowID)window.location='?module=templates&site_id=" . $GLOBALS['site_id'] . "&action=moveviewdown&selrow='+(-SelectedRowID)+'&id='+(-SelectedRowID);", "", 31, "{%langstr:move_selected_view_down%}");
      $Toolbar2->AddButtonImage('del2', 'delete', "{%langstr:col_del%}", "javascript:if(SelectedRowID<0)window.location='?module=templates&site_id=" . $GLOBALS['site_id'] . "&action=delview&id='+(-SelectedRowID);", "", 31, "{%langstr:delete_selected_view%}", "{%langstr:sure_delete_selected_view%}");

      $Toolbar2->AddSeperator();

      $docStrings['toolbar'] =  '<div id="Toolbar1Div">' . $Toolbar->output() . '</div>'
                              .'<div id="Toolbar2Div" style="display:none;">' . $Toolbar2->output() . '</div>';

    }

    $docTemplate = "main.htm";
    $docTitle = "{%langstr:templates%}";
    $docScripts['body'] = "templates.default.php";
    break;

}

?>
