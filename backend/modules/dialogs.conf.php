<?

$action = $_GET["action"];
$GLOBALS['site_id'] = intval($_GET["site_id"]);
$GLOBALS['path'] = $_GET["path"];

unset($perm_modsite);
if(($site_id == $GLOBALS["currentUserSiteID"])or(!$GLOBALS["currentUserSiteID"]))
  $perm_modsite = CheckPermission(6, $site_id);
if(!$perm_modsite)
  AccessDenied(True);
unset($perm_managefiles);
    if(($site_id == $GLOBALS["currentUserSiteID"])or(!$GLOBALS["currentUserSiteID"]))
      $perm_managefiles = CheckPermission(7, $site_id);


require_once($GLOBALS['cfgDirRoot']."library/"."func.files.php");
require_once($GLOBALS['cfgDirRoot']."library/"."func.search.php");

switch ($action) {

  case "filex":

    $docTitle = "{%langstr:file_explorer%}";
    if ($_GET['extended']) $docTemplate = "dlg_filex_ex.htm";
    else $docTemplate = "dlg_filex.htm";
    $docStrings['frame'] = "?module=files&action=list&site_id=".$GLOBALS['site_id']."&view=".$_GET['view'];
    $docStrings['label'] = "{%langstr:file_path%}:";
    if($perm_managefiles)
      $GLOBALS['canmanagefiles'] = 'ok';
    break;

  case "dirx":
    $docTitle = "{%langstr:directory_explorer%}";
    $docTemplate = "dlg_dirx.htm";
    $docStrings['frame'] = "?module=files&action=dirlist&site_id=".$GLOBALS['site_id']."&view=".$_GET['view']."&file=".$_GET['file'];
    $docStrings['label'] = "{%langstr:file_path%}:";
    $docStrings['file'] = $_GET['file'];
    if ($_GET['move']) $docStrings['move'] = $_GET['move'];
    else $docStrings['move'] = 0;
    if($perm_managefiles)
      $GLOBALS['canmanagefiles'] = 'ok';
    break;

  case "links":
    //$docTitle = "Browse For Link";
    if ($_GET['dlgtitle']) $docTitle = $_GET['dlgtitle'];
    else $docTitle = "Browse For Link";
    $docTemplate = "dlg_link.htm";
    $docStrings['label'] = "Link:";
    $docStrings['frame'] = "?module=pages&action=simple&site_id=".$GLOBALS['site_id'];
    break;

  case "date":
    if ($_GET['dlgtitle']) $docTitle = $_GET['dlgtitle'];
    else $docTitle = "Date dialog";
    $docTemplate = "dlg_date.htm";
    $docStrings['time'] = $_GET['time']==1 ? "true" : "false";
    break;

  case "googlemapframe":
    $docTitle = $_GET['title'];
    if(!$docTitle)
      $docTitle = 'Map coordinate picker';
    $docTemplate = "dlg_colman.htm";
    $docStrings['frame'] = "?module=dialogs&action=googlemap&site_id=".$GLOBALS['site_id']."&mapw=".$_GET['mapw']."&maph=".$_GET['maph']."&googlekey=".$_GET['googlekey'];
    break;

  case "googlemap":
    $docTemplate = "dlg_googlemap.htm";
    $docStrings['mapw'] = $_GET['mapw'];
    $docStrings['maph'] = $_GET['maph'];
    $docStrings['key'] = $_GET['googlekey'];
    break;

  case "news":

    $docTitle = "News manager";
    $docTemplate = "dlg_simple.htm";
    $docStrings['frame'] = "?module=news&action=listcategories&site_id=".$GLOBALS['site_id']."&page_id=".intval($_GET['page_id'])."&selectedcat=".$_GET['selectedcat'];
    $docStrings['label'] = "Category ID:";
    break;

  case "collection":

    $docTitle = "Collection manager";
    $docTemplate = "dlg_colman.htm";
    $docStrings['frame'] = "?module=collections&action=listcolitems&site_id=".$GLOBALS['site_id']."&page_id=".intval($_GET['page_id'])."&category_id=".intval($_GET['category_id'])."&newname=".$_GET['newname']."&coltype=".$_GET['coltype']."&justedit=" . $_GET['justedit'];
    $docStrings['label'] = "Collection ID:";
    break;

  case "componentframe":
    $GLOBALS['PageEncoding'] = PageEncoding($_GET['page_id'], $_GET['site_id']);
    $GLOBALS['maincharset']  = PageEncoding($_GET['page_id'], $_GET['site_id'], false);
    $docTitle = $_GET['title'];
    $docTemplate = "dlg_colman.htm";
    $frameLink = "?module=dialogs&action=component&site_id=".$GLOBALS['site_id']."&page_id=".$_GET['page_id']."&component_name=".$_GET['component_name']."&component_type=".$_GET['component_type']."&method_name=".$_GET['method_name']."&refresh=".$_GET['refresh']."&paramstr=".urlencode(stripslashes($_GET['paramstr'])).'&afterrefreshmethod='.$_GET['afterrefreshmethod'];
    foreach($_GET as $key => $val)
    {
      if(!in_array($key, array("module", "action", "site_id", "page_id", "component_name", "component_type", "method_name", "refresh", "paramstr", "afterrefreshmethod")))
      {
       $frameLink .= "&" . $key . "=" . urlencode($val);
      }
    }
    $docStrings['frame'] = $frameLink;
    $docStrings['action'] = $_GET['okaction'];
    break;

  case "component":
    $GLOBALS['PageEncoding'] = PageEncoding($_GET['page_id'], $_GET['site_id']);
    $GLOBALS['maincharset']  = PageEncoding($_GET['page_id'], $_GET['site_id'], false);
    $docTitle = $_GET['title'];
    $docTemplate = "nothingext.htm";
    $docScripts['body'] = "dialogs.component.php";
    break;

  case "refreshsinglecomponent":
    $docTemplate = "nothing.htm";
    $docScripts['body'] = "dialogs.refreshsinglecomponent.php";
    break;

  case "refreshsinglecomponentframe":
    $docTemplate = "dlg_null.htm";
    $url = parse_url($_SERVER['REQUEST_URI']);
    $url['query'] = str_replace("action=refreshsinglecomponentframe", "action=refreshsinglecomponent", $url['query']);
    $docStrings['frame'] = "?" . $url['query'];
    break;

  //collection dialogs
  case "coldiagframe":
    $docTitle = $_GET['title'];
    $docTemplate = "dlg_colman.htm";
    $docStrings['frame'] = "?module=dialogs&action=coldiag&site_id=".$GLOBALS['site_id']."&page_id=".$_GET['page_id']."&col_id=".$_GET['col_id']."&method_name=".$_GET['method_name']."&refresh=".$_GET['refresh']."&paramstr=".stripslashes($_GET['paramstr'])."&afterrefreshmethod=".$_GET['afterrefreshmethod'];
    $docStrings['action'] = $_GET['okaction'];
    break;

  case "coldiag":
    $GLOBALS['PageEncoding'] = PageEncoding(0, $site_id);
    $GLOBALS['maincharset']  = PageEncoding(0, $site_id, false);
    $docScripts['sellanguage'] = "sellanguage.default.php";
    include("sellanguage.setlanguage.php");
    $GLOBALS['PageEncoding'] = FullEncoding($GLOBALS['currentlanguage']);
    $GLOBALS['maincharset']  = $GLOBALS['currentlanguage'];
    $docTitle = $_GET['title'];
    if($_GET['refresh'])
    {
      $docTemplate = "close.htm";
    }else
    {
      $docTemplate = "nothing.htm";
    }
    $docScripts['body'] = "dialogs.coldiag.php";
    break;

  case "csslist":
    $docTemplate = "dlg_csslist.htm";
    $docTitle = "CSS File List";
    $GLOBALS['dlgtype'] = "checkbox";
    $docStrings['label'] = "CSS Files:";
    break;

  case "cssparser":
    $docTemplate = "dlg_cssclasses.htm";
    $docTitle = "CSS File List";
    //$GLOBALS['dlgtype'] = "checkbox";
    //$docScripts['body'] = "dialogs.cssparser.php";
    $docStrings['frame'] = "?module=pages&action=cssparser&cssfiles=".$_GET['cssfiles']."&site_id=".$GLOBALS['site_id'];
    break;

  //lbb project groups
  case "grouplist":
    $docTemplate = "dlg_simple.htm";
    $docTitle = "Select Groups";
    $GLOBALS['dlgtype'] = "checkbox";
    $docStrings['label'] = "Group IDs:";
    $docStrings['frame'] = "?module=dialogs&action=grouplistframe&site_id=$site_id&categoryid=".$_GET['categoryid'];
    break;
    
  case "grouplistframe":
    $GLOBALS['PageEncoding'] = PageEncoding(0, $site_id);
    $GLOBALS['maincharset']  = PageEncoding(0, $site_id, false);
     $docTemplate = "checkbox.htm";
    $docScripts['body'] = "dialogs.grouplist.php";
    break;

  //lbb project product list dialog
  case "productlist":
    $docTemplate = "dlg_simple.htm";
    $docTitle = "Select Products";
    $GLOBALS['dlgtype'] = "checkbox";
    $docStrings['label'] = "Products' IDs:";
    $docStrings['frame'] = "?module=dialogs&action=productlistframe&site_id=$site_id&colname=".$_GET['colname']."&groupcategoryid=".$_GET['groupcategoryid'];
    break;

  case "productlistframe":
    $GLOBALS['PageEncoding'] = PageEncoding(0, $site_id);
    $GLOBALS['maincharset']  = PageEncoding(0, $site_id, false);
    $docTemplate = "orderedcheckbox.htm";
    $docScripts['body'] = "dialogs.productlist.php";
    break;

  case "componenttemplate":
    $docTemplate = "dlg_colman.htm";
    $docTitle = "Edit Component Template";
    $docStrings['frame'] = "?module=dialogs&action=componenttemplateframe&site_id=$site_id";
    $docStrings['action'] = "close";
    break;

  case "componenttemplateframe":
    $GLOBALS['PageEncoding'] = PageEncoding(0, $site_id);
    $GLOBALS['maincharset']  = PageEncoding(0, $site_id, false);
    $docTemplate = "component_templ.html";
    break;

  //lbb copy product dialog
  case "lbbcopyproduct":
    $GLOBALS['PageEncoding'] = PageEncoding($_GET['page_id'], $_GET['site_id']);
    $GLOBALS['maincharset']  = PageEncoding($_GET['page_id'], $_GET['site_id'], false);
    $docTemplate = "dlg_simple.htm";
    $docTitle = "Select Products";
    $GLOBALS['dlgtype'] = "checkbox";
    $docStrings['label'] = "Copy Product:";
    $docStrings['frame'] = "?module=dialogs&action=lbbcopyproductframe&site_id=$site_id&page_id=".$_GET['page_id']."&categoryid=".$_GET['categoryid'];
    break;

  case "lbbcopyproductframe":
    $GLOBALS['PageEncoding'] = PageEncoding($_GET['page_id'], $_GET['site_id']);
    $GLOBALS['maincharset']  = PageEncoding($_GET['page_id'], $_GET['site_id'], false);
    $docTemplate = "nothing.htm";
    $docScripts['body'] = "dialogs.lbbcopyproduct.php";
    break;

  //lbb project user group collections
  case "usergrouplist":
  case "multichecklist":
    $GLOBALS['PageEncoding'] = PageEncoding(0, $site_id);
    $GLOBALS['maincharset']  = PageEncoding(0, $site_id, false);
    $docTemplate = "dlg_simple.htm";
    $docTitle = "User group collection";
    $GLOBALS['dlgtype'] = "checkbox";
    //$docStrings['label'] = "Products' IDs:";
    $docStrings['frame'] = "?module=dialogs&action=multichecklistframe&site_id=$site_id&coltype=".$_GET['coltype']."&categoryid=".$_GET['categoryid']."&useid=".$_GET['useid'] . "&columnname=" . $_GET['columnname'] . "&idcolumnname=" . $_GET['idcolumnname'];
    break;

  case "usergrouplistframe":
  case "multichecklistframe":
    $GLOBALS['PageEncoding'] = PageEncoding(0, $site_id);
    $GLOBALS['maincharset']  = PageEncoding(0, $site_id, false);
    $docTemplate = "checkbox.htm";
    if($_GET['add'])
    {
      $docTemplate = 'form.htm';
    }
    $docScripts['body'] = "dialogs.usergrouplist.php";
    break;

  //speedside project product collections
  case "spsproduct":
    $docTemplate = "dlg_simple.htm";
    $docTitle = "User group collection";
    $GLOBALS['dlgtype'] = "checkbox";
    //$docStrings['label'] = "Products' IDs:";
    $docStrings['frame'] = "?module=dialogs&action=spsproductframe&site_id=$site_id&coltype=".$_GET['coltype']."&categoryid=".$_GET['categoryid'];
    break;

  case "spsproductframe":
    $GLOBALS['PageEncoding'] = PageEncoding(0, $site_id);
    $GLOBALS['maincharset']  = PageEncoding(0, $site_id, false);
    $docTemplate = "checkbox.htm";
    $docScripts['body'] = "dialogs.spsproduct.php";
    break;

  case "viewpagecomponent":
    $GLOBALS['PageEncoding'] = PageEncoding($_GET['page_id'], $_GET['site_id']);
    $GLOBALS['maincharset']  = PageEncoding($_GET['page_id'], $_GET['site_id'], false);
    $docTemplate = "dlg_simple.htm";
    $docTitle = "Select Page Component";
    $GLOBALS['dlgtype'] = "dontinitinput";
    $docStrings['label'] = "Component:";
    $docStrings['frame'] = "?module=dialogs&action=viewpagecomponentframe&site_id=$site_id";
    break;

  case "viewpagecomponentframe":
    $GLOBALS['PageEncoding'] = PageEncoding($_GET['page_id'], $_GET['site_id']);
    $GLOBALS['maincharset']  = PageEncoding($_GET['page_id'], $_GET['site_id'], false);
    $docTemplate = "nothing.htm";
    $docScripts['body'] = "lists.viewpagecomponent.php";
    break;

  default:
    break;

}

?>
