<?

$perm_managefiles = $GLOBALS['perm_managefiles'];

$cols = Array(

  "name"        => Array(
    "width"     => "50%",
    "title"     => "{%langstr:col_name%}"
  ),

  "size"    => Array(
    "width"     => "10%",
    "title"     => "{%langstr:col_size%}"
  ),

  "date"     => Array(
    "width"     => "35%",
    "title"     => "{%langstr:col_modified%}"
  ),

  "del"    => Array(
    "width"     => "5%",
    "title"     => "{%langstr:col_del%}",
    "align"     => "center",
    "format"    => '<a href="?module=files&action=delete&path='.$GLOBALS['path'].'&dironly=1&file={%title%}&site_id='.$GLOBALS['site_id'].'&close=no" OnClick="return confirm(\'Are you sure you wish to delete this item?\')"><img src="'.$GLOBALS['cfgWebRoot'].'gui/images/ico_del.gif" width="16" height="16" border="0" alt="{%langstr:delete%}"></a>'
  )

);

if(!$perm_managefiles)
  unset($cols['del']);

function read_files($webroot, $dir, $path) {
  global $cfgCmsDirRoot, $cfgWebRoot;
/*
  function outputFilename($filepath, $webpath, $webdir, $filename) {
    $ext = strtolower(substr($filename,-4,4));
    if ($GLOBALS['view']=='thumbs')*//*(eregi("^images", $webdir) || eregi("^/images", $webdir))*//* {
      if ($ext==".gif" || $ext==".jpg" || $ext==".png") {
        $image = image_preview_out($filepath, $webpath, 200, 120);
      } else {
        $image = '<img src="'.$GLOBALS['cfgWebRoot'].'gui/images/ico_file.gif" width="16" height="16" align="absmiddle" border="0">';
      }
      $html = '<center><a href="javascript:;" OnClick="retVal(\'Form\', \'Input\', \'/'.$webdir.'\');">'.$image.'<br><br>'.$filename.'</a></center>';
    } else {
      $html = '<img src="'.$GLOBALS['cfgWebRoot'].'gui/images/ico_file.gif" width="16" height="16" align="absmiddle"> <a href="javascript:;" OnClick="retVal(\'Form\', \'Input\', \'/'.$webdir.'\');">'.$filename.'</a>';
    }
    return $html;
  }
*/
  chdir($dir);

  $handle = opendir($dir);
  while ($file = readdir($handle)) {
    if (is_dir($file) && $file != ".")
      $dirlist[] = $file;
    //elseif (is_file($file))
      //$filelist[] = $file;
  }
  closedir($handle);

  $i = 0;
  asort($dirlist);
  foreach($dirlist as $file) {
    if (!($file == '..' && $path == '')) {
      if ($file == '..') $url = fileUp($path); elseif ($path == '') $url = $file; else $url = $path.'/'.$file;
      $thisfile['name'] = '<img src="'.$GLOBALS['cfgWebRoot'].'gui/images/ico_folder.gif" width="16" height="16" align="absmiddle"> <a href="?module=files&action=dirlist&path='.$url.'&site_id='.$GLOBALS['site_id'].'&view='.$GLOBALS['view'].'" OnClick="retVal(\'Form\', \'Input\', \'/'.$url.'\');">'.$file.'</a>';
      $thisfile['title'] = $file;
      $thisfile['size'] = fileSizeStr($file);
      $thisfile['date'] = date("d-m-Y H:i:s", filectime($file));
      //if ($file != '..') $thisfile['del'] = 'x';
      $data[$i++] = $thisfile;
    }
  }
  /*
  asort($filelist);
  foreach($filelist as $file) {
    $ext = strtolower(substr($file,-4,4));
    if ($GLOBALS['view']!='thumbs' || ($ext==".gif" || $ext==".jpg" || $ext==".png")) {
      if ($path != "") $path = fileAddBackslash($path);
      $url = $path.$file;
      $thisfile['name'] = outputFilename(fileAddBackslash($dir).$file, fileAddBackslash($webroot).$url, $url, $file);
      $thisfile['title'] = $file;
      $thisfile['size'] = fileSizeStr($file);
      $thisfile['date'] = date("d-m-Y H:i:s", filectime($file));
      $data[$i++] = $thisfile;
    }
    
  }*/

  chdir($cfgCmsDirRoot);

  return $data;
}

$directory = sqlQueryValue("SELECT dirroot FROM sites WHERE site_id = '".$GLOBALS['site_id']."'");
$webroot = sqlQueryValue("SELECT domain FROM sites WHERE site_id = '".$GLOBALS['site_id']."'");
$path = str_replace("..", "", $GLOBALS['path']);
if ($path == "/") $path = "";
$directory = fileAddBackslash($directory).$path;

$data = read_files('http://'.$webroot.'/', $directory, $path);

echo '
<script language="JavaScript">
  var selrow = ' . (int) ($data[0][0]) . ';

  if (window.parent.Form)
    window.parent.Form.Ok.disabled = false;

  che="";
</script>';

echo("<script>\n  retVal('Form', 'Path', '".$path."');\n</script>\n");

require($GLOBALS['cfgDirRoot']."library/"."class.datagrid.php");

$Table = new DataGrid();
$Table->cols = $cols;
$Table->data = $data;
$Table->footer = false;
$Table->padding = "2";
$Table->rowformat = " onClick=\"javascript:HighLightTR(this);\"";
echo("\n<br /><p>{%langstr:docs_current_dir%}: /".$path."</p>\n");
echo $Table->output();


?>
