<?

$debug = false;

require_once($GLOBALS['cfgDirRoot']."library/models/class.langpages.php");

$site_id = $GLOBALS['site_id'];
$page_id = intval($_GET["id"]);

if (!$page_id) return;

$page = sqlQueryRow('SELECT * FROM '.$site_id.'_pages WHERE page_id='.$page_id);
$page = sqlQueryRow('SELECT * FROM '.$site_id.'_pages WHERE page_id='.$page['page_id']);  // get page id  
$pagelang = $page['language'] ? sqlQueryRow('SELECT * FROM '.$site_id.'_languages WHERE language_id='.$page['language']) : null;

$langs = getLanguages();
$links = LangPages::getLinks($page_id, $page['language']);

if ($_POST)
{
  foreach ($langs as $lang => $lrow)
  {
    if ($lang == $pagelang['shortname']) continue;
    if (!isset($_POST['langpage_'.$lang])) continue;

    $to_id = $_POST['langpage_'.$lang];
    $link = $links[$lrow['language_id']];

    if ($link)
    {
      if ($to_id)
      {
        if ($debug) echo 'CHANGE ';

        if ($link['row']['from_id'] == $link['from_id'])
          LangPages::changeLinkTo($link['row']['id'], $to_id);
        else
          LangPages::changeLinkFrom($link['row']['id'], $to_id);

      }
      else
      {
        if ($debug) echo 'DELETE ';
        LangPages::deleteLink($link['row']['id']);
      }

    }
    else if ($to_id)
    {
      if ($debug) echo 'ADD ';
      LangPages::addLink($page_id, $to_id, $page['language'], $lrow['language_id']);
    }

  }

  $links = LangPages::getLinks($page_id, $page['language']);
}

$form = array(
  'title' => array(
    'type' => 'title',
    'label' => $page['title'].' ('.$pagelang['fullname'].')'
  ),

  'desc' => array(
    'type' => 'label',
    'value' => '{%langstr:page_langpages_desc%}'
  )
);

foreach ($langs as $lang => $lrow)
{
  if ($lang == $pagelang['shortname']) continue;

  $combopages = array();
  $pages = sqlQueryData("SELECT page_id,name,parent,title FROM " . $site_id . "_pages WHERE language=".$lrow['language_id']." ORDER BY ind");
  ListNodes($pages, $combopages, 0, 0);
  $combopages[0] = '0:{%langstr:none%}';

  $link = $links[$lrow['language_id']];

  $form['langpage_'.$lang] = array(
    'type' => 'list',
    'label' => $lrow['fullname'],
    'lookup' => $combopages,
    'value' => $link['to_id']
  );
}

require($GLOBALS['cfgDirRoot']."library/"."class.formgen.php");

$FormGen = new FormGen();

$FormGen->cancel = "back";
$FormGen->hidereset = true;
$FormGen->properties = $form;
$FormGen->buttons = true;

echo $FormGen->output();

?>