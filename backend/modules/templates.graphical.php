<?

  $site_id = $GLOBALS['site_id'];
  $template_id = $_GET["template_id"];
  $GLOBALS['template_id'] = $_GET['template_id'];
  $GLOBALS['page_id'] = -1;


  $site_id = $GLOBALS['site_id'];

  $sitedata = sqlQueryRow("SELECT domain, wwwroot FROM sites WHERE site_id='".$site_id."'");
  $base = 'http://' . $sitedata['domain'] . $sitedata['wwwroot'];

  $language = $GLOBALS['currentlanguagenum'];
  if (!$language) 
  {
      $row = sqlQueryRow("SELECT language_id, shortname, fullname, encoding FROM " . $site_id . "_languages WHERE is_default = 1");
  }
  else
  {
      $row = sqlQueryRow("SELECT language_id, shortname, fullname, encoding FROM " . $site_id . "_languages WHERE language_id = ".$language);
  }
  $shortname = $row['shortname'];
  $fullname = $row['fullname'];
  $encoding = $row['encoding'];
  
  if(!$template_id)
    Die('No template selected');


  //$perm_managecontents = $GLOBALS['perm_managecontents'];
  //if(!$perm_managecontents)
    //AccessDenied(True);


  $GLOBALS['PageEncoding'] = PageEncoding($page_id, $site_id);
  $GLOBALS['maincharset']  = PageEncoding($page_id, $site_id, false);

  $templateData = sqlQueryRow("SELECT * FROM " . $site_id . "_templates WHERE template_id=" . $template_id);
  if(MySql_Error())
  {
    Die ("Sorry, database problems");
  }

  $GLOBALS['copypage'] = $pageData['copypage'];


  $body .= $templateData['body'];
  $copybody = $templateData['copybody'];
  while($copybody)
    list($body, $copybody) = sqlQueryRow("SELECT body, copybody FROM " . $site_id . "_templates WHERE template_id=$copybody");

  $body = attachSubTemplates($body, $site_id);

  //process template head
  preg_match("/(<head>[\s\S]*<\/head>)/i", $body, $matches);
  if ($matches[1])
  {
    $head = $matches[0];
    $head = str_replace("{%language:encoding%}", $encoding, $head);
    $body = str_replace($matches[0], $head, $body);
    $_SESSION['template_head_'.$template_id] = 1;
  }

  //process template language
  //process ifvisible parts
  preg_match_all ( '/{%ifvisible([0-9]*?):([a-zA-Z0-9|]*?)%}(.*?){%fivisible\\1%}/s', $body, $matches, PREG_SET_ORDER);
  foreach($matches as $key => $row)
  {
    $s = split('{%elsevisible' . $row[1] . '%}', $row[3]);
    $components = split('\|', $row[2]);
    $hidden = true;
    foreach($components as $component)
    {
      $hid = getProperty('visible', $component, '', -1, $site_id);
      if(!$hid)
        $hidden = false;
    }
//          echo "row2 = " . $row[2];
    if(!$hidden)
      $body = str_replace($row[0], $s[0], $body);
    else
      $body = str_replace($row[0], $s[1], $body);
  }
  //process ifdesign parts
  preg_match_all ( '/{%ifdesign([0-9]*?)%}(.*?){%fidesign\\1%}/s', $body, $matches, PREG_SET_ORDER);
  foreach($matches as $key => $row)
  {
    $s = split('{%elsedesign' . $row[1] . '%}', $row[2]);
    $body = str_replace($row[0], $s[0], $body);
  }

  /*
  //process iflanguage parts
  preg_match_all ( '/{%iflanguage([0-9]*?):([a-zA-Z0-9|]*?)%}(.*?){%filanguage\\1%}/s', $body, $matches, PREG_SET_ORDER);
  foreach($matches as $key => $row)
  {
    $s = split('{%elselanguage' . $row[1] . '%}', $row[3]);
    $languages = split('\|', $row[2]);
    $shortname = sqlQueryValue("SELECT shortname FROM " . $site_id . "_languages WHERE language_id=" . $pageData['language']);
    $hidden = true;

    foreach($languages as $l)
    {
      if($l == $shortname)
        $hidden = false;
    }
    if(!$hidden)
      $body = str_replace($row[0], $s[0], $body);
    else
      $body = str_replace($row[0], $s[1], $body);
  }
  */


  $body = str_replace("{%title%}", $pageData['title'], $body);



  $mustdisplayproperties = true;
  preg_match_all('/{%component:([a-zA-Z0-9_-]+):([a-zA-Z0-9_-]+)(:[a-zA-Z0-9% \\.\\,\\\"\\\'\\#\\!-]+)?%}/s', $body, $matches, PREG_SET_ORDER);
  foreach($matches as $key => $row)
  {
    $body = str_replace($row[0], outputTemplateGraphicalComponent($hascontent, $row[1], $row[2], $template_id, 0, $row[3], false, $toolbarfunc), $body);
    if($hascontent)
      $mustdisplayproperties = false;
  }


//strings
  while (ereg("{%string:([a-zA-Z0-9]+)%}", $body, $regs))
  {
    $body = ereg_replace($regs[0], GetPageString($site_id, -1, $regs[1]), $body);
  }
  //variables
  $body = str_replace("{%var:name%}", $pageData['name'], $body);
  //$body = str_replace("{%var:site_id%}", $site_id, $body);

  //$body = str_replace("{%var:template_id%}", $template_id, $body);
  $body = str_replace("{%url:host%}", $url['host'], $body);
  $body = str_replace("{%url:path%}", $url['path'], $body);
  $body = str_replace("{%url:query%}", $url['query'], $body);
  $body = str_replace("{%null%}", '', $body);

  $sitedata = sqlQueryRow("SELECT domain, wwwroot FROM sites WHERE site_id='".$site_id."'");
  $base = $sitedata['domain'] . $sitedata['wwwroot'];

echo '<SCRIPT language=JavaScript src="'.$GLOBALS['cfgWebRoot'].'gui/scripts.js"></SCRIPT>';

echo $body;

echo '<SCRIPT language=JavaScript src="?module=templates&action=editorjs&site_id='.$site_id.'&template_id='.$template_id.'"></SCRIPT>';

/*
  if($mustdisplayproperties)
  {
    echo  '<script language="JavaScript">
              <!--
                PropertiesWindow(\'?module=pages&site_id='.$site_id.'&id='.$page_id.'&component_name='.'&component_type=' .''."&session_id=" . session_id() . "&session_name=" . session_name().'\' ,\'\', null, 400, 400, 1, 1);
              <!---->
              </script>';
  }
*/
?>
