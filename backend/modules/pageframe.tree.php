<?

$GLOBALS['site_id'] = $_GET['site_id'];
$site_id = $_GET['site_id'];

//function for sorting data in tree structure
function NodesVisual($data, &$newdata, $parent, $level, $justcount, &$goodl)
{
  $goodlanguage = false;
  $nodecount = 0;
  foreach($data as $key => $row)
  {
    if($parent == $row['parent']){
      $nodecount++;

        $newdata[] = $row;
        $c = count($newdata) - 1;
        $childs = NodesVisual($data, $newdata, $row['page_id'], $level + 1, 0, $g);
        $newdata[$c]['children'] = $childs;
        $newdata[$c]['redirect'] = $row['redirect'];
        $goodlanguage = ($g or $goodlanguage);


        if($row['language'] != intval($GLOBALS['currentlanguagenum']))
        {
          //$newdata[$c]['page_id'] = 0;
          $c1 = $c;
          if(!$g)
          {
            if($c)
              $newdata = array_slice($newdata, 0, $c);
            else
              $newdata = array();
            $nodecount--;
          }else
          {
            $newdata[$c1]['title'] = "Page in " . sqlQueryValue("SELECT fullname FROM " . $GLOBALS['site_id'] . "_languages WHERE language_id = " . intval($row['language'])) . " (" . $row['name'] . ")";
          }
        }else
        {
          $newdata[$c]['clicklink'] = "?redirect_to_whatever";
          $goodlanguage = true;
        }

    }

  }
  $goodl = $goodlanguage;
  return $nodecount;
}

function AddTreeNode($tree, $data, &$f, $parentnode)
{
  if($f < count($data))
  {
    $row = $data[$f];
    if ($row['in_trash']) return;

    $row['title'] = ShortenString($row['title'], 30);
    $path = "?module=pages&site_id=" . $GLOBALS['site_id'] . "&action=redirectto_graph&id=" . $row['pagedev_id']."&page_id=".$row['page_id'];
    if(!$row['children'])
    {
      if($row['redirect'])
        $tree->add_document ($parentnode, $row['title'], $path, "ftv2link.gif", $row['page_id']);
      else
        $tree->add_document ($parentnode, $row['title'], $path, "", $row['page_id']);
    }else
    {
      if($row['redirect'])
        $node = $tree->add_folder ($parentnode, $row['title'], $path, "ftv2folderopenlink.gif", "ftv2folderclosedlink.gif", $row['page_id']);
      else
        $node = $tree->add_folder ($parentnode, $row['title'], $path, "", "", $row['page_id']);
      for($i = 0; $i < $row['children']; $i++)
      {
        $f++;
        AddTreeNode($tree, $data, $f, $node);
      }
      return $node;
    }

  }
}

function AddTreeNodeExt($data, &$f, $parentnode)
{
  if($f < count($data))
  {
    $row = $data[$f];
    if ($row['in_trash']) return;

    $row['title'] = ShortenString($row['title'], 30);
    if($row['children'])
    {
      if(!$row['redirect'])
        $cls = 'folder-node';
      else
        $cls = 'folder-redirect-node';
    }else
    {
      if(!$row['redirect'])
        $cls = 'page-node';
      else
        $cls = 'page-redirect-node';
    }
    $path = "?module=pages&site_id=" . $GLOBALS['site_id'] . "&action=redirectto_graph&id=" . $row['pagedev_id']."&page_id=".$row['page_id'];
      echo "
          var pagetreeNode".$row['page_id']." = new Ext.tree.TreeNode({
              text: '".parseForJScript($row['title'], true)."',
              id:'node-".$row['page_id']."',
              singleClickExpand: true,
              href: '".$path."',
              hrefTarget: 'koks2',
              iconCls: '".$cls."'
          });
          pagetreeNode".$row['page_id'].".page_id = '".$row['page_id']."';
          " . $parentnode . ".appendChild(pagetreeNode".$row['page_id'].");
      ";
    if($row['children'])
    {
      for($i = 0; $i < $row['children']; $i++)
      {
        $f++;
        AddTreeNodeExt($data, $f, "pagetreeNode".$row['page_id']);
      }
      return "pagetreeNode".$row['page_id'];
    }

  }
}

function AddTopNodes($data, &$f, $root, $children)
{
  for($i = 0; $i < $children; $i++)
  {
    AddTreeNode($tree, $data, $f, $root);
    $f++;
  }
}

function AddTopNodesExt($data, &$f, $root, $children)
{
  for($i = 0; $i < $children; $i++)
  {
    AddTreeNodeExt($data, $f, $root);
    $f++;
  }
}

//Output frame buttons

//Obtain page data
$unsorteddata = sqlQueryData("SELECT in_trash, pagedev_id, page_id, name, title, description, visible, enabled, parent, language, redirect, lastmod FROM " . $site_id . "_pagesdev ORDER BY ind");

//----
//get page_id array
$ids = sqlQueryData("select distinct page_id, ind from ".$site_id."_pagesdev ORDER BY ind");

//get keys of last modified dev pages
if (count($ids))
{
    $keys = array();
    for ($i=0; $i<=count($ids)-1; $i++)
    {
        $max = 0;
        foreach ($unsorteddata as $key=>$row)
        {
            //echo $row['page_id']." ".$ids[$i]['page_id']."<br>"; 
            if ($row['page_id']==$ids[$i]['page_id'])
            {
                if($row['lastmod']>$max)
                {
                    $max = $row['lastmod'];
                    $keys[$i] = $key;
                    $lastmoddev[$row['page_id']]['lastmod'] = $max;
                }                
            }
        }
    }
}

//drop unnecessary rows
foreach ($unsorteddata as $key=>$row)
    if (!in_array($key,$keys))
        unset($unsorteddata[$key]);

$lastmod_data = sqlQueryData("SELECT page_id,lastmod FROM ".$site_id."_pages");
foreach ($lastmod_data as $row)
{
    $lastmod[$row['page_id']]['lastmod'] = $row['lastmod'];
}
//----

  $g = false;
  $children = NodesVisual($unsorteddata, $data, 0, 0, 0, $g);

  //Create and output tree
  //require ($GLOBALS['cfgDirRoot'] . "library/class.tree.php");

  //$tree = new Tree($GLOBALS['cfgWebRoot'] . "gui/tree");
  //$tree->startallopen = false;
  //$root  = $tree->open_tree ("{%langstr:site_pages%}", "", "koks2", ".");

  $f = 0;
  //AddTopNodes($tree, $data, $f, $root, $children);

  //$tree->close_tree();

  if(($_GET['page_id'])or($_GET['selrow']))
  {
    if (isset($_GET['selrow']))
    {
        //selrow is a pagedev_id for added page; need to be converted to page_id
        $selrow = sqlQueryValue("SELECT page_id FROM ".$site_id."_pagesdev WHERE pagedev_id=".$_GET['selrow']);
    }

  /*
    echo '<script language="JavaScript" type="text/javascript">

  //alert("selrow="+'.IIF($_GET['selrow'], $_GET['selrow'], 0).');
  //n = FindItemByPageId('.$root.', '.IIF($_GET['selrow'], $_GET['selrow'], $_GET['page_id']).');
  n = FindItemByPageId('.$root.', '.IIF($_GET['selrow'], $selrow, $_GET['page_id']).');
  OpenFolderToNode(n);
  //alert(parent.document.body.selectedpage);
  if(n.page_id == parent.document.body.selectedpage)
  {
    clickOnNode2(n.id, true);
  }

</script>'; */
  }
    echo '<script language="JavaScript" type="text/javascript">
/*<![CDATA[*/

  var foldersTree = {
    selectedpage: \''.$_GET['page_id'].'\'
  };

  function SelectRightNode(id)
  {

    var selid = "";
    if(window.pagetree.getSelectionModel().getSelectedNode())
    {
      selid = window.pagetree.getSelectionModel().getSelectedNode().id;
    }
    if(foldersTree.selectedpage == id && selid == "node-" + id)
    {
      return false;
    }

    foldersTree.selectedpage = id;

    var n = 0;
    window.pagetreeNodeRoot.cascade(function()
      {
        if(this.id == "node-" + id)
        {
          n = this;
          return false;
        }
      }
    );
    if(n)
    {
      n.ensureVisible();
      n.select();
    }


    ////n = FindItemByPageId('.$root.', id);
    //OpenFolderToNode(n);
    //if(n)
    //{
    //  clickOnNode2(n.id, true);
    //}
    document.body.selectedpage = id;
  }

  ';
  if($_GET['refrframe'])
  {
    echo 'try{ window.parent.document.frames["koks2"].location.reload(true); } catch(e) { }';
  }
  echo '

/*]]>*/
</script>';

  echo "
  <script>

  Ext.BLANK_IMAGE_URL = '" . $GLOBALS['cfgWebRoot'] . "gui/extjs/resources/images/default/s.gif';

  function InitTree() {

    // set the root node
    window.pagetreeNodeRoot = new Ext.tree.TreeNode({
        text: '{%langstr:site_pages%}',
        draggable:false,
        id:'node-root',
        expanded: true,
        singleClickExpand: true,
        iconCls: 'folder-node'
    });
    window.pagetreeNodeRoot.page_id = 0;

    ";

    //generate all tree nodes recursively
    $g = false;
    $children = NodesVisual($unsorteddata, $data, 0, 0, 0, $g);
    AddTopNodesExt($data, $f, 'window.pagetreeNodeRoot', $children);

    echo "

    //create page tree
    var pagetree = new Ext.tree.TreePanel({
      useArrows: false,
      border: false,
      autoScroll:true,
      animate:true,
      enableDD:false,
      containerScroll: true,
      rootVisible: true,
      ddGroup: 'treeDDE',
      renderTo: 'tree-cell',
      root: window.pagetreeNodeRoot,

      //layout properties
      border: false,
      title: '',
      collapsible: false,
      region:'west',
      floatable: false,
      margins: '0 0 0 0',
//      bodyStyle: 'background-color: #000000',
      hlColor: '#000000'
    });
    window.pagetree = pagetree;

    pagetree.on('click', function(node, e)
      {
        foldersTree.selectedpage = node.page_id;
      }
    );

    postTreeInit(pagetree);

  }
  </script>

  ";
  //create right-click menu
  require($GLOBALS['cfgDirRoot']."library/"."class.rightmenu.php");
  $rightmenu = new rightmenu;
  $rightmenu->functionname = 'InitTreeMenu';
  $rightmenu->addItem('{%langstr:preview%}','document.getElementById("toolbarbutontd_prev").onclick();',false,false,'');
  if($GLOBALS['perm_managepages'])
  {
    $rightmenu->addSpacer();
    $rightmenu->addItem('{%langstr:toolbar_new_subpage%}','document.getElementById("toolbarbutontd_add").onclick();',false,false,'');
    $rightmenu->addItem('{%langstr:toolbar_new_page%}','document.getElementById("toolbarbutontd_add2").onclick();',false,false,'');
    $rightmenu->addItem('{%langstr:edit_properties%}','document.getElementById("toolbarbutontd_edit").onclick();',false,false,'');
    if(!$GLOBALS['cfgMini'])
    {
      $rightmenu->addItem('{%langstr:permissions%}','document.getElementById("toolbarbutontd_perms").onclick();',false,false,'');
    }
    $rightmenu->addItem('{%langstr:hint_up%}','document.getElementById("toolbarbutontd_up").onclick();',false,false,'');
    $rightmenu->addItem('{%langstr:hint_down%}','document.getElementById("toolbarbutontd_down").onclick();',false,false,'');
    $rightmenu->addItem('{%langstr:col_del%}','document.getElementById("toolbarbutontd_del").onclick();',false,false,'');
  }
  echo $rightmenu->output();
  echo "
  <script>
  function postTreeInit(pagetree)
  {
    InitTreeMenu();

    pagetree.on('contextmenu', function(node, e)
      {
        node.select();
        foldersTree.selectedpage = node.page_id;
        showMenu(node.ui.getEl(), e);
      }
    );

    //select node of active page visually in tree
    SelectRightNode(" . IIF($_GET['selrow'], $selrow, $_GET['page_id']) . ");
  }
  ";

  echo "

  parent.document.body.OnOpenFolderToNode = SelectRightNode;


  </script>
  ";

?>
