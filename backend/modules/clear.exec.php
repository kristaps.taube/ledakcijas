<?


    $site_id = $_POST['site_id'];

    //TURNCATE ALL TABLES

    $collections = sqlQueryData("SELECT * FROM ".$site_id."_collections");

    foreach ($collections as $collection)
    {
        sqlQuery("DROP TABLE IF EXISTS ".$site_id."_coltable_".strtolower($collection['name']));
    }

    sqlQuery("TRUNCATE TABLE " . $site_id . "_contents");
    sqlQuery("TRUNCATE TABLE " . $site_id . "_templates");
    sqlQuery("TRUNCATE TABLE " . $site_id . "_templates_history");
    sqlQuery("TRUNCATE TABLE " . $site_id . "_ctemplates");
    sqlQuery("TRUNCATE TABLE " . $site_id . "_pages");
    sqlQuery("TRUNCATE TABLE " . $site_id . "_pagesdev");
    sqlQuery("TRUNCATE TABLE " . $site_id . "_contentsdev");
    sqlQuery("TRUNCATE TABLE " . $site_id . "_search");
    sqlQuery("TRUNCATE TABLE " . $site_id . "_languages");
    sqlQuery("TRUNCATE TABLE " . $site_id . "_languagestrings");
    sqlQuery("TRUNCATE TABLE " . $site_id . "_colitems");
    sqlQuery("TRUNCATE TABLE " . $site_id . "_colproperties");
    sqlQuery("TRUNCATE TABLE " . $site_id . "_collections");
    sqlQuery("TRUNCATE TABLE " . $site_id . "_search_link");
    sqlQuery("TRUNCATE TABLE " . $site_id . "_search_object");
    sqlQuery("TRUNCATE TABLE " . $site_id . "_search_word");
    sqlQuery("TRUNCATE TABLE " . $site_id . "_search_page_col_relation");
    sqlQuery("TRUNCATE TABLE " . $site_id . "_strings");
    sqlQuery("TRUNCATE TABLE " . $site_id . "_stringsdev");
    sqlQuery("TRUNCATE TABLE " . $site_id . "_views");
    sqlQuery("TRUNCATE TABLE " . $site_id . "_options");
    sqlQuery("TRUNCATE TABLE " . $site_id . "_services");
    sqlQuery("TRUNCATE TABLE " . $site_id . "_languagestrings_info");

    if ($_POST['stats'])
    {
        //stats tables
        sqlQuery("TRUNCATE TABLE `" . $site_id . "_stats`");
        sqlQuery("TRUNCATE TABLE `" . $site_id . "_stats_main`");
        sqlQuery("TRUNCATE TABLE `" . $site_id . "_stats_pages`");
        sqlQuery("TRUNCATE TABLE `" . $site_id . "_stats_keywords`");
    }

    sqlQuery("TRUNCATE TABLE `" . $site_id . "_wapcontents`");
    sqlQuery("TRUNCATE TABLE `" . $site_id . "_wapcontentsdev`");
    sqlQuery("TRUNCATE TABLE `" . $site_id . "_wapctemplates`");
    sqlQuery("TRUNCATE TABLE `" . $site_id . "_wappages`");
    sqlQuery("TRUNCATE TABLE `" . $site_id . "_wappagesdev`");
    sqlQuery("TRUNCATE TABLE `" . $site_id . "_waptemplates`");
    sqlQuery("TRUNCATE TABLE `" . $site_id . "_documentdir`");
    sqlQuery("TRUNCATE TABLE `" . $site_id . "_documents`");
    sqlQuery("TRUNCATE TABLE `" . $site_id . "_documentstats`");
    sqlQuery("TRUNCATE TABLE `" . $site_id . "_file_keywords`");
    sqlQuery("TRUNCATE TABLE `" . $site_id . "_filesearch_keywordlink`");
    sqlQuery("TRUNCATE TABLE `" . $site_id . "_filesearch_link`");
    sqlQuery("TRUNCATE TABLE `" . $site_id . "_filesearch_object`");
    sqlQuery("TRUNCATE TABLE `" . $site_id . "_filesearch_word`");
    sqlQuery("TRUNCATE TABLE `" . $site_id . "_pageshistory`");

    sqlQuery("DELETE FROM lists WHERE site_id=" . $site_id);


    //sqlQuery("DELETE * FROM permissions WHERE data2=$site_id AND (perm_num=11 OR perm_num=13 OR perm_num=16 OR perm_num=31 " //page perms.
    //                                                            ."OR perm_num=12 OR perm_num=14 OR perm_num=15 "            //template perms.
    //                                                            ."OR perm_num=20 OR perm_num=21 OR perm_num=22 OR perm_num=27 " //wap page perms.
    //                                                            ."OR perm_num24 OR perm_num=25 OR perm_num=26)");         //wap template perms.

    if ($_POST['log'])
    {
        sqlQuery("DELETE FROM log WHERE site_id=$site_id");
        sqlQuery("DELETE FROM log WHERE site_id=0");
    }



    //FILL TABLES WITH DEFAULT CONTENT

    //Add sample template
    sqlQuery('INSERT INTO `'.$site_id.'_templates` (`template_id`, `name`, `body`, `description`) VALUES (\'1\', \'default\', \'<html> <head> <title>{%title%}</title> <meta name="description" content=""> <meta name="keywords" content=""> </head> <body> {%component:formatedtext:maincontents%} </body> </html> \', \'Default empty template\')');
    $template_id = sqlQueryValue("SELECT template_id FROM " . $site_id . "_templates WHERE name='default'");
    //Add access to the newly created template for Everyone
    AddGroupPermission(1, 12, $site_id, $template_id);

    //Add sample pages
    sqlQuery("INSERT INTO `".$site_id."_pages` ( `page_id` , `name` , `title` , `description` , `template` , `lastmod` , `created` , `ind` , `parent` , `modby` , `enabled` , `visible` , `language` , `redirect` , `copypage` )
                     VALUES (
                     '1', '404', '404 - page not found', 'The page to display when page not found',
                     '".$template_id."', '".time()."', '".time()."', '1', '0', '1', '1', '0',
                     '0', '0', '0'
                     )");
    sqlQuery("INSERT INTO `".$site_id."_pagesdev` ( `page_id` , `name` , `title` , `description` , `template` , `lastmod` , `created` , `ind` , `parent` , `modby` , `enabled` , `visible` , `language` , `redirect` , `copypage` )
                     VALUES (
                     '1', '404', '404 - page not found', 'The page to display when page not found',
                     '".$template_id."', '".time()."', '".time()."', '1', '0', '1', '1', '0',
                     '0', '0', '0'
                     )");
    $page_id = sqlQueryValue("SELECT page_id FROM " . $site_id . "_pages WHERE name='404'");
    AddGroupPermission(1, 11, $site_id, $page_id);

    sqlQuery("INSERT INTO `".$site_id."_pages` ( `page_id` , `name` , `title` , `description` , `template` , `lastmod` , `created` , `ind` , `parent` , `modby` , `enabled` , `visible` , `language` , `redirect` , `copypage` )
                     VALUES (
                     '2', 'index', 'Main Page', 'The default page',
                     '".$template_id."', '".time()."', '".time()."', '2', '0', '1', '1', '1',
                     '0', '0', '0'
                     )");
    sqlQuery("INSERT INTO `".$site_id."_pagesdev` ( `page_id` , `name` , `title` , `description` , `template` , `lastmod` , `created` , `ind` , `parent` , `modby` , `enabled` , `visible` , `language` , `redirect` , `copypage` )
                     VALUES (
                     '2', 'index', 'Main Page', 'The default page',
                     '".$template_id."', '".time()."', '".time()."', '2', '0', '1', '1', '1',
                     '0', '0', '0'
                     )");
    $page_id = sqlQueryValue("SELECT page_id FROM " . $site_id . "_pages WHERE name='index'");
    AddGroupPermission(1, 11, $site_id, $page_id);




?>
