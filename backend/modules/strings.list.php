<?

  $perm_managepages = $GLOBALS['perm_managepages'];
  if(!$perm_managepages)
    AccessDenied(True);

  require_once ($GLOBALS['cfgDirRoot']."library/".'class.formgen.php');

  $id = $_GET["id"];
  $GLOBALS['page_id'] = $id;
  $page_id = $id;
  $site_id = $GLOBALS['site_id'];

  {
    $form_data = Array();

    $data  = sqlQueryData("SELECT string_id, stringname, stringvalue FROM " . $site_id . "_strings WHERE page=-1");
    $data2 = sqlQueryData("SELECT stringdev_id, stringname, stringvalue FROM " . $site_id . "_stringsdev WHERE pagedev=" . $page_id);
    $strings = Array();
    foreach($data as $row)
    {
        $strings[$row['stringname']] = Array('name' => $row['stringname'],
                                             'value' => $row['stringvalue'],
                                             'defval' => $row['stringvalue'],
                                             'default' => 1);
    }
    foreach($data2 as $row)
    {
        if(!isset($strings[$row['stringname']]))
          $strings[$row['stringname']] = Array('name' => $row['stringname'],
                                               'value' => $row['stringvalue'],
                                               'default' => 0);
        $strings[$row['stringname']]['name'] = $row['stringname'];
        $strings[$row['stringname']]['value'] = $row['stringvalue'];
        $strings[$row['stringname']]['default'] = 0;
    }
    foreach($strings as $row)
    {
      if((!$_GET['onestringonly'])or($_GET['onestringonly']==$row['name']))
      {
        $form_data['component_string_' . $row['name']]
                     = array("label"   => $row['name'],
                             "type"      => "html",
                             "rows"      => "3",
                             "cols"      => "30",
                             "default" => $row['default'],
                             "defval"  => parseForJScript($row['defval']),
                             "value"   => $row['value']
                       );
      }
    }

    $Inspector = new Inspector();
    $Inspector->properties = $form_data;
    $Inspector->opened = false;
    $Inspector->buttons =true;
    $Inspector->cancel ="close";
    $Inspector->name = "Form";
    $Inspector->action = '?module=' .$GLOBALS['module']. '&site_id=' .$site_id. '&page_id=' .$id. '&id=' .$id. '&action=updatestrings';
    echo $Inspector->output();

  }


?>
