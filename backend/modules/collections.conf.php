<?
$GLOBALS['site_id'] = $_GET["site_id"];
$site_id = $_GET["site_id"];
$page_id = $_GET['page_id'];

$action = $_GET["action"];
$GLOBALS['tabs'] = 'site';


//echo $GLOBALS['REQUEST_URI'];


unset($perm_modsite);
if(($site_id == $GLOBALS["currentUserSiteID"])or(!$GLOBALS["currentUserSiteID"]))
  $perm_modsite = CheckPermission(6, $site_id);
  //$perm_modsite = ( CheckPermission(6, $site_id) && CheckPermission(17, $site_id) );

if(!$perm_modsite)
{
  echo "Permission Denied!";
  AccessDenied(True);
}

$colid = $_GET['category_id'];
$colname = $_GET['newname'];
$coltype = 'collection';
if($_GET['coltype']){
	$coltype = $_GET['coltype'];
}else if($colid){
  $coltype = sqlQueryValue("SELECT type FROM " . $site_id . "_collections WHERE collection_id=" . $colid);
}

if($coltype){
  $classname = pdocollection::typeToClassname($coltype);
  $collection = new $classname($colname, $colid);
}

$docScripts['sellanguage'] = "sellanguage.default.php";
include($GLOBALS['cfgDirRoot']."backend/modules/"."sellanguage.setlanguage.php");
$GLOBALS['havelanguages'] = HaveLanguages($site_id);
$GLOBALS['PageEncoding'] = FullEncoding($GLOBALS['currentlanguage']);
$GLOBALS['maincharset']  = $GLOBALS['currentlanguage'];


$action = $_GET["action"];
require($GLOBALS['cfgDirRoot']."library/"."class.toolbar.php");

switch ($action) {

  case "newcollection":
    $docTitle = "Create New Collection";
    $docTemplate = "form.htm";
    $docScripts['body'] = "collections.newcollection.php";
    break;

  case "setrelatedcollection":
    $docTitle = "Assign Related Collection";
    $docTemplate = "form.htm";
    $docScripts['body'] = "collections.setrelated.php";
    break;

  case "filtercollection":
    $docTitle = "Filter Collection";
    $docTemplate = "form.htm";
    $docScripts['body'] = "collections.filtercollection.php";
    break;

  case "listcolitems":
    $docTitle = "Edit Collection";
    $docTemplate = "form.htm";

    $docScripts['exec'] = "collections.exec.php";
    $docScripts['body'] = "collections.itemlist.php";

    break;

  case "newcolitem":
    $docTitle = "New Collection Item";
    $docTemplate = "form.htm";
    $docScripts['body'] = "collections.editcolitems.php";
    break;

  case "editcolitem":
    $docTitle = "Edit Collection Item";
    $docTemplate = "form.htm";
    $docScripts['body'] = "collections.editcolitems.php";
    break;

  case "dlgcolmethod":
    $docTitle = stripslashes($_GET['title']);
    $docTemplate = "form.htm";
    $docScripts['body'] = "collections.dialogmethod.php";
    break;

  case "modcolitem":
  case "addcolitem":
  case "updatecolproperties":
  case "addcollectionsave":
  case "setrelatedcollectionsave":
  case "filtercollectionsave":
  case "dlgcolmethodexec":
    $docTemplate = "close.htm";
    $docScripts['exec'] = "collections.exec.php";
    break;

  case "delcolitem":
  case "moveup":
  case "movedown":
  case "filtercollectionclear":
  case "execcolmethod":
    $docScripts['exec'] = "collections.exec.php";
    
    break;

  case "delcollection":
    $docScripts['exec'] = "collections.exec.php";
    break;

  case "editcollection":

    $docTitle = "{%langstr:edit_collection%}";
    $docTemplate = "main.htm";

    $docScripts['body'] = "collections.editcollection.php";

    break;

  case "colproperties":

    $docTitle = "{%langstr:collection_properties%}";
    $docTemplate = "form.htm";
    $docScripts['body'] = "collections.colproperties.php";
    break;

  case "copycollection":
    $docTitle = "{%langstr:toolbar_copy_collection%}";
    $docTemplate = "form.htm";
    $docScripts['body'] = "collections.copy.php";
    break;

  case "listcollections":

    $docTemplate = "main.htm";
    $docTitle = "Collections - " . $_GET['coltype'];
    $docScripts['body'] = "collections.listcollections.php";

    break;
  default:

    $docTemplate = "main.htm";
    $docTitle = "Collections";
    $docScripts['body'] = "collections.default.php";

    if($GLOBALS['user_site_permissions_managetemplates'])
    {
      require($GLOBALS['cfgDirRoot']."library/"."class.toolbar2.php");
      $Toolbar2 = new Toolbar2();

      $Toolbar2->AddSpacer();
      $Toolbar2->AddButtonImageText('add', 'strings_new', '{%langstr:toolbar_new_collection%}', "", "javascript:openDialog('?module=collections&site_id=" . $GLOBALS['site_id'] . "&action=newcollection', 400, 550, 1, 0);", 135, "{%langstr:toolbar_hint_new_collection%}");
      $docStrings['toolbar'] = $Toolbar2->output();
    }

    break;

    break;

}

