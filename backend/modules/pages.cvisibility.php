<?

  $perm_managecontents = $GLOBALS['perm_managecontents'];
  if(!$perm_managecontents)
    AccessDenied(True);

  require_once ($GLOBALS['cfgDirRoot']."library/".'class.formgen.php');

  $id = $_GET['page_id'];
  $GLOBALS['page_id'] = $id;
  $page_id = $id;
  $site_id = $GLOBALS['site_id'];
  $pagedev_id = $_GET['pagedev_id'];

  if($page_id>0)
  {   //real page
    $template = sqlQueryValue("SELECT template FROM ". $site_id . "_pagesdev WHERE pagedev_id=$pagedev_id");
  }
  if($template)
  {
    list($data, $copybody) = sqlQueryRow("SELECT body, copybody FROM " . $site_id . "_templates WHERE template_id=$template");
    while($copybody)
      list($data, $copybody) = sqlQueryRow("SELECT body, copybody FROM " . $site_id . "_templates WHERE template_id=$copybody");
  }

  $data = attachSubTemplates($data, $site_id);

  $r = componentArray($data, $page_id);
  if(!count($r))
  {
    echo "No components in this page";
  }
  else
  {
    $form_data = Array();
    for($f=0; $f<count($r); $f++)
    {
      displayPropertiesTable($r[$f]["type"], $r[$f]["name"], $site_id, $pagedev_id, $form_data, false, 'visible:name');
    }
    $i=0;

    echo '<form action="?module=' .$GLOBALS['module']. '&site_id=' .$site_id. '&pagedev_id=' .$pagedev_id. '&id=' .$page_id. '&action=tbarupdatecontrols" method="post">';
    echo '<table cellpadding="5" cellspacing="0" width="100%">';
    echo '<tr>';
    echo '<td  class="formTitle">{%langstr:name_component%}</td><td  class="formTitle">{%langstr:visible%}</td>';
    echo '</tr>';
    foreach ($form_data as $key=>$row)
      { 
        if ($i%3==1)
          {
            $name = split(",",$row['label']);
            echo '<tr><td class=formLabel>'.$row['label'].'</td>';
            echo '<td><input type="checkbox" name="'.$name[0].'"'; 
          }
        if ($i%3==2)
          {
            if ($row['value']==0) echo 'checked="checked"></td></tr>';
            if ($row['value']==1) echo '>';
            echo '<input type="hidden" name="component[]" value="'.$name[0].'">';
            echo '</td></tr>';
          }
        $i++;
      }
    echo '<tr>';
    echo '<td colspan="2" align="right"><input class=formButton type="submit" value="{%langstr:save%}"> <input class=formButton type="button" value="{%langstr:cancel%}" onClick="jacascript:window.close();"></td>';
    echo '</tr>';
    echo '</table>';
    echo '</form>';

    /*
    $Inspector = new Inspector();
    $Inspector->properties = $form_data;
    $Inspector->opened = True;
    $Inspector->buttons =true;
    $Inspector->cancel ="closeparent";
    $Inspector->name = "Form";
    $Inspector->action = '?module=' .$GLOBALS['module']. '&site_id=' .$site_id. '&page_id=' .$id. '&id=' .$id. '&action=updatecontrols';
    echo $Inspector->output();
    */

  }


?>
