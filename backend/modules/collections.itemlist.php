<?

$collection= $GLOBALS['collection'];


$category_id = $collection->collection_id;
$site_id = $_GET['site_id'];
$page_id = $_GET['page_id'];
if($page_id)
{
  $GLOBALS['PageEncoding'] = PageEncoding($page_id, $site_id);
  $GLOBALS['maincharset']  = PageEncoding($page_id, $site_id, false);
}



$Toolbar = new Toolbar();
$Toolbar->backendroot = $GLOBALS['cfgWebRoot'];
$Toolbar->AddButton("add", "{%langstr:add_new%}", "add_component.gif", "openDialog('?module=collections&action=newcolitem&site_id=$site_id&page_id=$page_id&category_id=$category_id&coltype=".$_GET['coltype']."&session_id=".session_id()."&session_name=".session_name()."', 400, 260, 1, 1);", "Add new collection item");
$Toolbar->AddButton("edit", "{%langstr:col_edit%}", "edit_properties.gif", "openDialog('?module=collections&action=editcolitem&site_id=$site_id&page_id=$page_id&category_id=$category_id&coltype=".$_GET['coltype']."&session_id=".session_id()."&session_name=".session_name()."&id='+SelectedRowID, 400, 260, 1, 1);", "Edit collection item");
if($collection->CanChangeOrder())
{
  $Toolbar->AddLink("up", "{%langstr:up%}", "move_up.gif", "javascript: if (SelectedRowID) window.location='?module=collections&site_id=$site_id&page_id=$page_id&action=moveup&category_id=$category_id&coltype=".$_GET['coltype']."&justedit=".$_GET['justedit']."&session_id=".session_id()."&session_name=".session_name()."&selrow='+SelectedRowID+'&id='+SelectedRowID;", "");
  $Toolbar->AddLink("down", "{%langstr:down%}", "move_down.gif", "javascript: if (SelectedRowID) window.location='?module=collections&site_id=$site_id&page_id=$page_id&action=movedown&category_id=$category_id&coltype=".$_GET['coltype']."&justedit=".$_GET['justedit']."&session_id=".session_id()."&session_name=".session_name()."&selrow='+SelectedRowID+'&id='+SelectedRowID;", "");
}
$Toolbar->AddLink("del", "{%langstr:col_del%}", "delete_component.gif", "javascript: if (SelectedRowID) window.location='?module=collections&action=delcolitem&site_id=$site_id&page_id=$page_id&category_id=$category_id&coltype=".$_GET['coltype']."&justedit=".$_GET['justedit']."&session_id=".session_id()."&session_name=".session_name()."&id='+SelectedRowID;", "Deletes selected collection item", "Are you sure you want to delete selected collection item?");
if(!$_GET['justedit'])
{
  $Toolbar->AddSeperator("210");
  $Toolbar->AddButton("properties", "{%langstr:toolbar_properties%}", "page_properties.gif", "openDialog('?module=collections&action=colproperties&site_id=$site_id&page_id=$page_id&category_id=$category_id&coltype=".$_GET['coltype']."&session_id=".session_id()."&session_name=".session_name()."', 400, 260, 1, 1);", "Edit collection properties");
  $Toolbar->AddLink("delc", "{%langstr:toolbar_delete_collection%}", "delete_component.gif", "javascript: window.location='?module=collections&action=delcollection&site_id=$site_id&page_id=$page_id&category_id=$category_id&coltype=".$_GET['coltype']."&session_id=".session_id()."&session_name=".session_name()."'", "Deletes collection and all items", "Are you sure you want to delete the collection and all items? You won\'t be able to undo this operation!");
}
echo '<div style="background-color: #5A7EC5">';
echo $Toolbar->output();
echo "</div>";

$cols = $collection->columns;

$data = $collection->GetEditData(-1, $count, $recordposition);


require($GLOBALS['cfgDirRoot']."library/"."class.datagrid.php");

$Table = new DataGrid();
$Table->cols = $cols;
$Table->data = $data;
$Table->footer = false;
$Table->rowformat = " id=\"tablerow{%item_id%}\" onClick=\"javascript:HighLightTR(this, {%item_id%});\"";


echo $Table->output();


if($_GET['refreshparent'])
{
  echo '
  <script language="JavaScript">
  <!--
    var combo = window.parent.dialogArguments[2];
    var win = window.parent.dialogArguments[1];
    if(combo)
    {

      var oOption = win.document.createElement("OPTION");

      combo.options.add(oOption);
      oOption.value = "'.$category_id.'";
      combo.selectedIndex = combo.options.length - 1;
      if(cnst.isFF)
        oOption.innerHTML = "'.$_GET['newname'].'";
      else
        oOption.innerText = "'.$_GET['newname'].'";

    }
  <!---->
  </script>
  ';
}

echo '
  <script language="JavaScript">
  <!--
    retVal(\'Form\', \'Input\', \''.$category_id.'\');
  <!---->
  </script>
  ';

  if($_GET['selrow'])
  {
      echo '<script language="JavaScript">
        <!--
          var tablerow' . $_GET['selrow'] . ' = document.getElementById("tablerow' . $_GET['selrow'] . '");
          if(typeof(tablerow' . $_GET['selrow'] . ')!= "undefined" && tablerow' . $_GET['selrow'] . ')
          {
            preEl = tablerow' . $_GET['selrow'] . ';
            ChangeTextColor(tablerow' . $_GET['selrow'] . ',\'tableRowSel\');
            SelectedRowID = ' . $_GET['selrow'] . ';
          }
        <!---->
        </script>';
  }


?>
