<?

//note: if filename starts with "/" then full path has been specified, otherwise
//  it's relative path to site's dirroot
function LogSkinFilePath($skin_id, $filename)
{
  global $site_id;

  sqlQuery('INSERT INTO ' . $site_id . '_skins_files (skin_id, filename)
              VALUES (' . $skin_id . ', "' . db_escape($filename) . '")');
}

$site_id = $GLOBALS['site_id'];

switch ($GLOBALS["action"]) {

  case "download":

    require_once($GLOBALS['cfgDirRoot']."library/"."func.xmlrpc.php");

    //get skin info
    $result = XMLRPC_request(
      $GLOBALS['xmlrpc_server'],
      $GLOBALS['xmlrpc_path'] . 'index.php',
      'Skins.ListSkins',
      array(XMLRPC_prepare(array(
        'id' => $_GET['id']
      )))
    );

    $success = $result[0];
    $data = $result[1];
    $template = $data[0];

    if($success && $template)
    {

      require_once($GLOBALS['cfgDirRoot'].'library/pclzip.lib.php');

      //download zip
      $file = 'http://' . $GLOBALS['xmlrpc_server'] . $GLOBALS['xmlrpc_path'] . $template['filename'];
      $contents = file_get_contents ($file);
      $fullpath = $GLOBALS['cfgDirRoot'] . 'temp/skin.zip';

      $fs = fopen($fullpath, 'w');
      fputs($fs, $contents);
      fclose($fs);

      $zip = new PclZip($fullpath);
      $list = $zip->listContent();

      if(!$list)
        die('Invalid skin');

      //do we already have this skin?
      $skin_id = sqlQueryValue('SELECT skin_id FROM ' . $site_id . '_skins WHERE
                                  orig_id = ' . $template['id']);
      if($skin_id)
        die('This skin is already downloaded. Delete it to download again.');

      //create skin in DB
      sqlQuery('INSERT INTO ' . $site_id . '_skins
                (orig_id, title, description, thumbnail1, thumbnail2, pic1, pic2) VALUES
                (' . intval($template['id']) . ', "' . db_escape($template['title']) . '",
                 "' . db_escape($template['description']) . '",
                 "' . ($template['thumbnail1'] ? db_escape("http://".$GLOBALS['xmlrpc_server'].$GLOBALS['xmlrpc_path'].$template['thumbnail1']) : '' ) . '",
                 "' . ($template['thumbnail2'] ? db_escape("http://".$GLOBALS['xmlrpc_server'].$GLOBALS['xmlrpc_path'].$template['thumbnail2']) : '' ) . '",
                 "' . ($template['pic1'] ? db_escape("http://".$GLOBALS['xmlrpc_server'].$GLOBALS['xmlrpc_path'].$template['pic1']) : '' ) . '",
                 "' . ($template['pic2'] ? db_escape("http://".$GLOBALS['xmlrpc_server'].$GLOBALS['xmlrpc_path'].$template['pic2']) : '' ) . '")'
              );
      $skin_id = sqlLastID();

      //unzip files according to their function specified by the folder they are in
      foreach($list as $item)
      {
        if(!$item['folder'])
        {
          $match = preg_match('/^([^\/]*?)\/((.*)\/)?(.*?)$/', $item['filename'], $matches);
          $prefix = $matches[1];
          $folder = $matches[3];
          $file   = $matches[4];
          $zipfile = $matches[0];

          switch ($prefix)
          {
            //internal files (to execute on installing, applying skin etc)
            case "cms":
              $dirpath = $GLOBALS['cfgDirRoot'] . 'skins/' . $template['id'];
              mkdir($dirpath);
              $res = $zip->extract(
                PCLZIP_OPT_BY_INDEX, $item['index'],
                PCLZIP_OPT_REMOVE_ALL_PATH,
                PCLZIP_OPT_ADD_PATH, $dirpath,
                PCLZIP_OPT_REPLACE_NEWER
              );
              chmod($dirpath . '/' . $file, 0777);
              LogSkinFilePath($skin_id, $dirpath . '/' . $file);

              if($file == 'install.php')
                require($dirpath . '/' . $file);

              break;
            //additional components and collections (chmod must be set accordingly)
            case "components":
            case "collections":
              $dirpath = $GLOBALS['cfgDirRoot'] . $prefix;
              $res = $zip->extract(
                PCLZIP_OPT_BY_INDEX, $item['index'],
                PCLZIP_OPT_REMOVE_ALL_PATH,
                PCLZIP_OPT_ADD_PATH, $dirpath,
                PCLZIP_OPT_REPLACE_NEWER
              );

              break;
            //template data (template name = file name, template content = file content)
            case "templates":
              $res = $zip->extract(
                PCLZIP_OPT_BY_INDEX, $item['index'],
                PCLZIP_OPT_EXTRACT_AS_STRING
              );
              if ($res != 0) {
                $body = $res[0]['content'];

                sqlQuery('INSERT INTO '.$site_id.'_skins_templates (skin_id, name, body)
                            VALUES (' . $skin_id . ', "' . db_escape($file) . '",
                            "' . db_escape($body) . '")');
              }

              break;
            //web files (images, css etc)
            case "files":
              $folder = '/tpl/' . $folder;
              $webpath = $folder . '/' . $file;
              forceDirectories($site_id, $folder);
              $fulldir = getFilePathFromLink($site_id, $folder);

              $res = $zip->extract(
                PCLZIP_OPT_BY_INDEX, $item['index'],
                PCLZIP_OPT_REMOVE_ALL_PATH,
                PCLZIP_OPT_ADD_PATH, $fulldir,
                PCLZIP_OPT_REPLACE_NEWER
              );
              chmod($fulldir . '/' . $file, 0777);
              LogSkinFilePath($skin_id, $webpath);
              break;
          }
        }
      }

      unlink($fullpath);
      header('Location: ?module=skins&action=list&site_id=' . $site_id . '&selrow=' . $skin_id);
      die();
    }

    break;

  case "delskin":
    list($skin_id, $active) = sqlQueryRow('SELECT skin_id, active FROM ' . $site_id . '_skins WHERE
                                  skin_id = ' . intval($_GET['id']));
    if(!$skin_id)
      die('Skin does not exist');

    header('Location: ?module=skins&action=list&site_id=' . $site_id);

    //unapply skin if skin to delete is active
    if($active)
      if(file_exists($GLOBALS['cfgDirRoot'] . 'skins/' . $skin_id . '/unapply.php'))
        include($GLOBALS['cfgDirRoot'] . 'skins/' . $skin_id . '/unapply.php');

    //uninstall skin
    if(file_exists($GLOBALS['cfgDirRoot'] . 'skins/' . $skin_id . '/uninstall.php'))
      include($GLOBALS['cfgDirRoot'] . 'skins/' . $skin_id . '/uninstall.php');

    //delete skin related files
    $files = sqlQueryData('SELECT * FROM ' . $site_id . '_skins_files WHERE skin_id = ' . $skin_id);
    $tpldir = null;
    foreach($files as $file)
    {
      $fname = $file['filename'];
      if (!$tpldir && substr($fname, 0, 5) == '/tpl/') $tpldir = explode('/', dirname($fname));

      if($fname && !$GLOBALS['cfgMiniDemo']);
      {
        if(substr($fname, 0, 5) == '/var/')
        {
          //full path
          unlink($fname);
        }else
        {
          $fname = getFilePathFromLink($site_id, $fname);
          unlink($fname);
        }
      }
    }

    $tplpath = getFilePathFromLink($site_id, '/'.$tpldir[1].'/'.$tpldir[2]);
//    recursive_remove_directory($tplpath);

    //delete skin related db entries
    sqlQuery("DELETE FROM " . $site_id . "_skins_files WHERE skin_id = " . $skin_id);
    sqlQuery("DELETE FROM " . $site_id . "_skins_templates WHERE skin_id = " . $skin_id);
    sqlQuery("DELETE FROM " . $site_id . "_skins WHERE skin_id = " . $skin_id);

    if ($_SESSION['designs_active_page'] > 0)
      $_SESSION['designs_active_page'] -= 1;

    break;

  case "applyskin":

    list($skin_id, $orig_skin_id) = sqlQueryRow('SELECT skin_id, orig_id FROM ' . $site_id . '_skins WHERE
                                  skin_id = ' . intval($_GET['id']));
    if(!$skin_id)
      die('Skin does not exist');

    header('Location: ?module=skins&site_id=' . $site_id);

    //unapply old skin
    $oldskin_id = sqlQueryValue('SELECT skin_id FROM ' . $site_id . '_skins WHERE active');
    if($oldskin_id)
      if(file_exists($GLOBALS['cfgDirRoot'] . 'skins/' . $oldskin_id . '/unapply.php'))
        include($GLOBALS['cfgDirRoot'] . 'skins/' . $oldskin_id . '/unapply.php');
    sqlQuery('UPDATE ' . $site_id . '_skins SET active = 0');

    //set skin active
    sqlQuery("UPDATE " . $site_id . "_skins SET active = 1 WHERE skin_id = " . $skin_id);

    //set skin template(s)
    $templates = sqlQueryData("SELECT * FROM " . $site_id . "_skins_templates WHERE skin_id = " . $skin_id);
    foreach($templates as $template)
    {
      //check if template already exist and either update existing or create new
      $template_id = sqlQueryValue("SELECT template_id FROM " . $site_id . "_templates
                                      WHERE name = '" . db_escape($template['name']) . "'");
      DeleteAllCompiledTemplates($site_id);
      if($template_id)
      {
        sqlQuery("UPDATE " . $site_id . "_templates SET body='" . db_escape($template['body']) . "'
                    WHERE template_id = " . $template_id);
      }
      else
      {
        sqlQuery("INSERT INTO " . $site_id . "_templates (name, body) VALUES
                    ('" . db_escape($template['name']) . "', '" . db_escape($template['body']) . "')");
      }
    }

    //call apply.php if exists
    if(file_exists($GLOBALS['cfgDirRoot'] . 'skins/' . $orig_skin_id . '/apply.php'))
    {
      include($GLOBALS['cfgDirRoot'] . 'skins/' . $orig_skin_id . '/apply.php');
    }

    break;
}
