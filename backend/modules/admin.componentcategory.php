<?

$form_data = Array(

  "categoryname"    => Array(
    "label"     => "{%langstr:col_name%}:",
    "size"      => "35",
    "type"      => "text"
  )

);

require($GLOBALS['cfgDirRoot']."library/"."class.formgen.php");
$FormGen = new FormGen();
$FormGen->action = "?module=components&action=saveaddcategory";
$FormGen->title = "{%langstr:add_category%}";
$FormGen->properties = $form_data;
$FormGen->cancel = 'close';
$FormGen->buttons = true;
echo $FormGen->output();


?>
