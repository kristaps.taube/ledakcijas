<?

$advanced = ($_SESSION['userdata']['username'] == 'Admin' && $_SESSION['options_show_advanced']);
$user = $_SESSION['userdata']['username'];

$where = '';
if ($user != 'Admin')
  $where = " AND (user='' OR user='".addslashes($user)."')";

$where .= !$advanced ? " AND is_advanced=0" : '';
$data = sqlQueryDataAssoc("SELECT id, parent, name, label FROM `".$GLOBALS['site_id']."_options` WHERE is_cat=1".$where." ORDER BY parent ASC, id");
#$data_not = sqlQueryDataAssoc("SELECT id, parent, name, label FROM `".$GLOBALS['site_id']."_options` WHERE is_cat=1 ORDER BY parent ASC, id");

foreach ($data as $key => $row){
	$data[$key]['id'] = (int)$data[$key]['id'];
	$data[$key]['parent'] = (int)$data[$key]['parent'];
  if ($data[$key]['label'] == '') $data[$key]['label'] = $data[$key]['name'];
  unset($data[$key]['name']);
}

echo "<script type='text/javascript'>

function build_dtree(name, label, data){

  var d = new dTree(name);
  d.icon.node = d.icon.folder;

  d.add(0, -1, label, '?module=options&site_id=".$GLOBALS['site_id']."&action=panel&item=0', '', 'panel');
  for (var i=0; i<data.length; i++){
    var e = data[i];
    d.add(e.id, e.parent, e.label, '?module=options&site_id=".$GLOBALS['site_id']."&action=panel&item='+e.id, '', 'panel');
  }
  document.write(d);

}

var data = ".json_encode($data)."
var dtree;
build_dtree('dtree', '{%langstr:menu_options%}', data);

</script>";

