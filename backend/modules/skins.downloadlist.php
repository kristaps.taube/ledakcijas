<?
session_start();
$site_id = $GLOBALS['site_id'];

require_once($GLOBALS['cfgDirRoot']."library/"."func.xmlrpc.php");

define("XMLRPC_DEBUG", 0);
$params=array('all' => 1);

#$params=array('all' => 1);


if(isset($_POST['search'])){
    if($_POST['hasNews']) $_SESSION['hasNews']= 1; else  $_SESSION['hasNews']= 0;
    if($_POST['hasLogo']) $_SESSION['hasLogo']= 1; else  $_SESSION['hasLogo']= 0;
    if($_POST['hasSideImg']) $_SESSION['hasSideImg']= 1; else  $_SESSION['hasSideImg']= 0;
    if($_POST['hasSideTxt']) $_SESSION['hasSideTxt']= 1; else  $_SESSION['hasSideTxt']= 0;
    if($_POST['hasTopImg']) $_SESSION['hasTopImg']= 1; else  $_SESSION['hasTopImg']= 0;
    if($_POST['blog']) $_SESSION['blog']= 1; else  $_SESSION['blog']= 0;
    if($_POST['title']!='') $_SESSION['title']=$_POST['title']; else  unset($_SESSION['title']);

    if($_POST['levels']==1){
          $_SESSION['oneLevelMenu']=1;
          $_SESSION['twoLevelMenu']=0;
    }else{ // two levels
          $_SESSION['twoLevelMenu']=1;
          $_SESSION['oneLevelMenu']=0;
    }
}


if(isset($_POST['showAll'])){
    $_SESSION['hasNews']=0;
    $_SESSION['hasLogo']=0;
    $_SESSION['hasTopImg']=0;
    $_SESSION['hasSideImg']=0;
    $_SESSION['hasSideTxt']=0;
    $_SESSION['oneLevelMenu']=0;
    $_SESSION['twoLevelMenu']=0;
    $_SESSION['blog']=0;
    $_SESSION['title']='';
}

if($_SESSION['hasNews']==1)$params['has_news'] = 1;
if($_SESSION['hasLogo']==1)$params['has_logo'] = 1;
if($_SESSION['hasTopImg']==1) $params['has_top_image'] = 1;
if($_SESSION['hasSideImg']==1) $params['has_side_image'] = 1;
if($_SESSION['hasSideTxt']==1) $params['has_side_text'] = 1;
if($_SESSION['oneLevelMenu']==1) $params['one_level_menu'] = 1;
if($_SESSION['twoLevelMenu']==1) $params['two_level_menu'] = 1;
if($_SESSION['blog']==1) $params['blog'] = 1;
if($_SESSION['title']) $params['title'] = $_SESSION['title'];



$result = XMLRPC_request(
  $GLOBALS['xmlrpc_server'],
  $GLOBALS['xmlrpc_path'] . 'index.php',
  'Skins.ListSkins',
  array(XMLRPC_prepare($params))
);

$success = $result[0];
$data = $result[1];
foreach ($data as $key => $row)
{
  $data[$key]['description'] = nl2br($row['description']);
}

if($success && $data && count($data))
{
  $pagesize = 5;

  if(isset($_SESSION['designs_downloadlist_active_page']))
    $page = $_SESSION['designs_downloadlist_active_page'];
  else
    $page = 0;

  if(isset($_GET['editpage']))
  {
    $page = intval($_GET['editpage']) - 1;
    $_SESSION['designs_downloadlist_active_page'] = $page;
  }

  ?>
    <form method='POST' action=''>
        <input type='checkbox' name='hasNews'<?=$_SESSION['hasNews'] ? ' checked="checked"' : '' ?> id='hasNews' />  <label for='hasNews'>{%langstr:has_news%}</label>
        <input type='checkbox' name='hasLogo' <?=$_SESSION['hasLogo'] ? ' checked="checked"' : '' ?> id='hasLogo' />  <label for='hasLogo'>{%langstr:has_logo%}</label>
        <input type='checkbox' name='hasTopImg'<?=$_SESSION['hasTopImg'] ? ' checked="checked"' : '' ?> id='hasTopImg' />  <label for='hasTopImg'>{%langstr:has_top_img%}</label>
        <input type='checkbox' name='hasSideImg'<?=$_SESSION['hasSideImg'] ? ' checked="checked"' : '' ?> id='hasSideImg' />  <label for='hasSideImg'>{%langstr:has_side_image%}</label>
        <input type='checkbox' name='hasSideTxt'<?=$_SESSION['hasSideTxt'] ? ' checked="checked"' : '' ?> id='hasSideTxt' />  <label for='hasSideTxt'>{%langstr:has_side_text%}</label>
        <input type='checkbox' name='blog'<?=$_SESSION['blog'] ? ' checked="checked"' : '' ?> id='blog' />  <label for='blog'>{%langstr:blog%}</label>
        <input id='title' name='title' value='<?=$_SESSION['title']?>'> <label for='title'>{%langstr:skinTitle%}</label>
        &nbsp;&nbsp;&nbsp;&nbsp;<select name='levels'><option value='1' <?=$_SESSION['oneLevelMenu'] ? ' selected="selected"' : '' ?>>{%langstr:one_level_menu%}</option><option value='2'<?=$_SESSION['twoLevelMenu'] ? ' selected="selected"' : '' ?>>{%langstr:two_level_menu%}</option></select>
        <input type='submit' value='{%langstr:filter%}' name='search' />  <input type='submit' value='{%langstr:show_all%}' name='showAll' />
    </form>
<?

  //clean out skins that are already downloaded
  foreach($data as $key => $row)
  {
    $skin_id = sqlQueryValue('SELECT skin_id FROM ' . $site_id . '_skins WHERE
                                  orig_id = ' . $row['id']);
    if($skin_id)
      unset($data[$key]);
  }

  $count = count($data);
  $page = min($page * $pagesize, $count - 1);

  $data = array_slice($data, $page, $pagesize);


  $cols = Array(

    "download"    => Array(
      "width"     => "5%",
      "title"     => "{%langstr:download%}",
      "format"    => '<a href="?module=skins&action=download&id={%id%}&site_id=' . $site_id . '"><img src="' . $GLOBALS['cfgWebRoot'] . 'gui/images/download.png" alt="{%langstr:download%}" title="{%langstr:download%}" /></a>'
    ),

    "title"    => Array(
      "width"     => "15%",
      "title"     => "{%langstr:title%}",
      "format"    => '<b>{%_self%}</b>'
    ),

    "description" => Array(
      "width"     => "30%",
      "title"     => "{%langstr:col_description%}"
    ),

    "thumbnail1"       => Array(
      "width"     => "25%",
      "format"    => "[if {%_self%}]<a href=\"#\" onclick=\"window.open('http://".$GLOBALS['xmlrpc_server'].$GLOBALS['xmlrpc_path']."{%pic1%}','galwin','width=1050,height=800,scrollbars=1,toolbar=0,resizable=1');return false\"><img src=\"http://".$GLOBALS['xmlrpc_server'].$GLOBALS['xmlrpc_path']."{%_self%}\" /></a>[/if]"
    ),

    "thumbnail2"       => Array(
      "width"     => "25%",
      "format"    => "[if {%_self%}]<a href=\"#\" onclick=\"window.open('http://".$GLOBALS['xmlrpc_server'].$GLOBALS['xmlrpc_path']."{%pic2%}','galwin','width=1050,height=800,scrollbars=1,toolbar=0,resizable=1');return false\"><img src=\"http://".$GLOBALS['xmlrpc_server'].$GLOBALS['xmlrpc_path']."{%_self%}\" /></a>[/if]"
    ),

  );

  echo '{%langstr:description_skins_downloadlist%}';

  require($GLOBALS['cfgDirRoot']."library/"."class.datagrid.php");
  $Table = new DataGrid();
  $Table->cols = $cols;
  $Table->data = $data;
  $Table->footer = false;
  $Table->rowformat = " onClick=\"javascript:HighLightTR(this, {%id%});if(SelectedRowID){  }else {  }\"";
  $Table->pagesontopandbottom = true;

  $Table->pagecount = ceil($count / $pagesize);
  $Table->pagelen = 1;
  $Table->linkformat = preg_replace('/&editpage=[0-9]*/s', '', $_SERVER["REQUEST_URI"]) . '&editpage=%d';
  $Table->pagenum = min($Table->pagecount, $_SESSION['designs_downloadlist_active_page'] + 1);

  echo $Table->output();

}else
{
  echo 'Skins not found or server error. Please try again later.';
}

?>
