<?

$collection= $GLOBALS['collection'];

$category_id = $collection->collection_id;

$site_id = $_GET['site_id'];
$page_id = $_GET['page_id'];
if($page_id)
{
  $GLOBALS['PageEncoding'] = PageEncoding($page_id, $site_id);
  $GLOBALS['maincharset']  = PageEncoding($page_id, $site_id, false);
}

if (strlen($_POST['name']))
{
  $name = $_POST['name'];

  $d1 = sqlQueryRow('SELECT * FROM '.$site_id.'_collections WHERE name="'.$name.'"');
  $d2 = sqlQueryRow('SHOW TABLES LIKE "'.$site_id.'_coltable_'.strtolower($name).'"');
  if ($d1 || $d2)
  {
//    echo '<script type="text/javascript">alert("{%langstr:collection_exists%}")</script>';
    echo '{%langstr:collection_exists%}';
  }
  else
  {
    $src = $collection->table;
    $dest = $site_id.'_coltable_'.$name;
		$dest = strtolower($dest);

    $ok2 = $ok3 = null;
    $ok1 = sqlQuery("CREATE TABLE ".$dest." LIKE ".$src."");
    if ($ok1)
    {
      $ok2 = sqlQuery("INSERT INTO ".$dest." SELECT * FROM ".$src);
      if ($ok2)
      {
        $ok3 = sqlQuery('INSERT INTO '.$site_id.'_collections SET name="'.$name.'", type="'.$collection->type.'"');
      }
    }
    if ($ok3)
    {
      echo '<script type="text/javascript">window.close()</script>';
    }
    else
      echo 'Error duplicating db table!';
  }
}

$form = array(
  'name' => array('type' => 'str', 'label' => '{%langstr:newcolname%}', 'value' => $_POST['name'])
);

require_once($GLOBALS['cfgDirRoot'].'library/class.formgen.php');

$FormGen = new FormGen();
$FormGen->title = '{%langstr:toolbar_copy_collection%}';
$FormGen->action = '';
$FormGen->cancel = "close";
$FormGen->hidereset = true;
$FormGen->properties = $form;
$FormGen->buttons = true;

echo $FormGen->output();

?>