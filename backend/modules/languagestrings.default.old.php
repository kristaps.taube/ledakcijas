<?

/*
kad gatavs:
  - modulii clear palaist kveriju UPDATE NN_languagestrings_info set is_used=0
  - backend/index.php failaa pielikt create table kveriju tabulai NN_languagestrings_info

*/

if($_FILES['excel_import_file']){

  doImport($_FILES['excel_import_file']);

}

$site_id = $GLOBALS['site_id'];

if($_POST['mark_all_as_unused']){
  sqlQuery('UPDATE '.$site_id.'_languagestrings_info SET is_used = 0');
}

if($_POST['_langnum'])
{
  $langnum = $_POST['_langnum'];
  foreach($_POST as $key => $val)
  {
    if(substr($key, 0, 1) != '_')
    {
      $s = explode('_', $key);
      if($s[0] != 'check')
      {
        $cname = $s[0];
        $sname = substr($key, strlen($cname) + 1);
        sqlQuery('DELETE FROM ' . $site_id . '_languagestrings WHERE component="'.$cname.'" AND name="'.$sname.'" AND language='.intval($langnum).'');
        if($_POST['check_' . $cname . '_' . $sname] != 'on')
        {
          sqlQuery('INSERT INTO ' . $site_id . '_languagestrings (name, component, language, value) VALUES ("'.$sname.'", "'.$cname.'", '.intval($langnum).', "'.$val.'")');
        }
      }
    }else if(substr($key, 0, 7) == '_glotmp')
    {
      $s = explode('_', $key);
      if($s[0] != 'check')
      {
        $cname = '';
        $sname = substr($key, strlen('_glotmp') + 1);
        sqlQuery('DELETE FROM ' . $site_id . '_languagestrings WHERE component="'.$cname.'" AND name="'.$sname.'" AND language='.intval($langnum).'');
        if($_POST['check__glotmp_' . $sname] != 'on')
        {
          sqlQuery('INSERT INTO ' . $site_id . '_languagestrings (name, component, language, value) VALUES ("'.$sname.'", "'.$cname.'", '.intval($langnum).', "'.$val.'")');
        }
      }
    }
  }
  DeleteAllCompiledTemplates($site_id);
}


$activelanguage = $GLOBALS['currentlanguagenum'];
$activelangrow = sqlQueryRow('SELECT * FROM '.$site_id.'_languages WHERE language_id='.$activelanguage);

if($activelanguage)
{

  extract(LS_getAllStrings($site_id, $activelanguage, true));

  require ($GLOBALS['cfgDirRoot']."library/".'class.formgen.php');

  $form_data = Array();
  $form_data['_langnum'] = Array(
    "type" => "hidden",
    "value" => $activelanguage
  );
  $data = sqlQueryDataAssoc('SELECT * FROM '.$site_id.'_languagestrings_info WHERE is_used=1');
  $compstrings_used = array();
  foreach ($data as $row){
    $cmp = empty($row['component']) ? '_template' : $row['component'];
    $compstrings_used[$cmp][$row['name']] = 1;
  }

  $langs = getLanguages();


  function getLSLabel($site_id, $cskey, $cname2, $langs)
  {
    $ls_count = sqlQueryValue('SELECT COUNT(*) FROM `'.$site_id.'_languagestrings` WHERE name = :key AND component = :name', array(":key" => $cskey, ":name" => $cname2));

    return $cskey.($ls_count < count($langs)-1 ? '<div class="lwarn">! nepilnīgs</div>' : '');
  }


  $jstrings = array();

  foreach($compstrings as $cname => $cs)
  {
    if (!$cs) continue;
    if (!$_COOKIE['ls_show_unused'] && !$compstrings_used[$cname]){
    	#echo "Metam arā ".$cname."<br />";
    	continue;
    }

    $cname2 = ($cname != '_template' ? $cname : '_template');
    $form_data[$cname] =
      Array(
        "label" => $cname2,
        "value" => $cname,
        "type"  => "title"
    );

    foreach ($compstrings_used[$cname] as $cskey => $uval)
    {
      if (!$compdefvals[$cname][$cskey]){
      	#echo "Nav def value: ".$cname." ".$cskey."<br />";
      	continue;
      }
      $csval = $cs[$cskey];

      $form_data[$cname . '_' . $cskey] = Array(
        "label" => '<div style="width:130px; overflow:hidden;"><span style="color:green">'.getLSLabel($site_id, $cskey, $cname2, $langs).'</span></div>',
        "value" => $csval,
        "width" => "90%",
        "type"      => "html",
        "rows"      => "2",
        "cols"      => "30",
        "defval"   => parseForJScript($compdefvals[$cname][$cskey], true),
        "default"   => $compdefs[$cname][$cskey]
      );

      unset($cs[$cskey]);

      $jstrings[$cname . '_' . $cskey] = array(
        'cmp' => $cname2,
        'name' => $cskey,
        'defval' => $compdefvals[$cname][$cskey]
      );
    }

    if (!$_COOKIE['ls_show_unused']) continue;

    foreach($cs as $cskey => $csval)
    {
      $form_data[$cname . '_' . $cskey] = Array(
        "label" => '<div style="width:130px; overflow:hidden;"><span style="color:black">'.getLSLabel($site_id, $cskey, $cname2, $langs).'</span></div>',
        "value" => $csval,
        "width" => "90%",
        "type"      => "html",
        "rows"      => "2",
        "cols"      => "30",
        "defval"   => parseForJScript($compdefvals[$cname][$cskey], true),
        "default"   => $compdefs[$cname][$cskey]
      );

      $jstrings[$cname . '_' . $cskey] = array(
        'cmp' => $cname2,
        'name' => $cskey,
        'defval' => $compdefvals[$cname][$cskey]
      );
    }
  }

?>
  <div id="lsd">
  <? if ($_COOKIE['ls_show_unused']) { ?>
    <button class="show_used">Rādīt tikai izmantotos tekstus</button>
  <? } else { ?>
    <button class="show_unused">Rādīt arī neizmantotos tekstus</button>
  <? } ?>

    <div class="contextbox"><div class="inner">
      <ul class="langs">
      <? foreach ($langs as $lang => $lrow) { ?>
        <li class="<?=($lang == $activelangrow['shortname'] ? 'active' : (!empty($prod['description_'.$lang]) ? ' filled' : '') ) ?>"><a href="#" rel="<?=$lang ?>"><?=strtoupper($lang) ?></a></li>
      <? } ?>
      </ul>
      <span class="edit_wrap"><a href="#" class="edit"><img src="/scr/components/shop/images/edit.png" alt=""/></a></span>
    </div></div>

<?
    $Inspector = new Inspector();
    $Inspector->properties = $form_data;
    $Inspector->buttons = false;
    $Inspector->cancel = '?module=' .$GLOBALS['module']. '&site_id=' .$site_id;
    $Inspector->name = "Form";
    $Inspector->opened = true;
    $Inspector->hidecheckboxes = false;
    $Inspector->action = '?module=' .$GLOBALS['module']. '&site_id=' .$site_id;
    echo $Inspector->output();
?>
    <div class="buttons" style="text-align:right">
      <span class="ajaxload" style="display:none"><img src="<?=$GLOBALS['cfgWebRoot'] ?>gui/ajaxload.gif" alt=""/></span>
      <button class="ok">{%langstr:ok%}</button>
      <button class="cancel">{%langstr:cancel%}</button>
    </div>

  <button id="export_xls">Eksportēt Excel failā</button>
  <form method='post' action=''>
    <input onClick="return confirm('Are you sure?')" type='submit' value='Atzīmēt visus kā neizmantotus' name='mark_all_as_unused' />
  </form>
  <form method='post' action='' enctype="multipart/form-data">
    Excel imports: <input type='file' name='excel_import_file' /> <input type='submit' value='Importēt no Excel faila' name='do_import' />
  </form>

  </div>
<?
}else
{
  echo 'Please select language to continue';
}

$ajax_url = '?module=languagestrings&site_id='.$site_id;

$jsvars = array(
  'lstrings' => $jstrings,
  'active_lang' => $activelangrow['shortname'],
  'ajax_url' => $ajax_url,
  'export_xls_url' => '?module=languagestrings&site_id='.$_GET['site_id'].'&action=export_xls'
);

?>
<script type="text/javascript" src="/scr/components/jquery.easing.js"></script>
<script type="text/javascript" src="/scr/components/jquery.cookie.js"></script>
<script type="text/javascript" src="/scr/components/json2.js"></script>
<script type="text/javascript">


var phpvars = <?=json_encode($jsvars) ?>;
var active_lstring = null;

function positionContextBox()
{
  var coord = $('#div2 td.formControl:first textarea.formEdit:first').offset();
   if(!coord) return;
  coord.top -= $('#lsd .contextbox').height() + 1;
  $('#lsd .contextbox').offset(coord);
}

function ajaxCall(action, query, data, callback, type)
{
  var url = phpvars.ajax_url+'&action='+action+query;

  if (data == null || typeof(data) == 'object')
    return $.post(url, data, callback, type);

  return $.ajax({
    type: 'POST',
    url: url,
    data: data,
    success: callback,
    processData: false,
    contentType: "application/octet-stream"
  });
}

function updateLangStatuses(ls)
{
  var cbox = $('#lsd .contextbox')[0];

  for (var lang in ls.strings)
  {
    if (lang == ls.lang)
      $('a[rel='+lang+']', cbox).parent().toggleClass('active', true).toggleClass('filled', ls.strings[lang] != null);
    else
      $('a[rel='+lang+']', cbox).parent().toggleClass('active', false).toggleClass('filled', ls.strings[lang] != null);

  }

}

function changeLSLangTo(ls, lang)
{
  if (ls.strings == null) return;

  var new_val = !$('#check_'+active_lstring)[0].checked ? $('#'+active_lstring).val() : null;

  if (new_val != ls.strings[ls.lang])
    ls['changed'] = true;

  ls.strings[ls.lang] = new_val;

  $('#'+active_lstring)
    .val(ls.strings[lang] != null ? ls.strings[lang] : ls.defval)
    .css('background-color', ls.strings[lang] != null ? '#FFFFFF' : '#F0F0F0');

  $('#check_'+active_lstring)[0].checked = (ls.strings[lang] == null);

  ls.lang = lang;
  updateLangStatuses(ls);
}

$(function()
{
  $('#export_xls').click(function()
  {
    window.open(phpvars.export_xls_url, '_blank');
    return false;
  });


  $('#lsd .show_unused').click(function()
  {
    $.cookie('ls_show_unused', 1, { expires: 30.5, path: '/' });
    window.location = window.location;
    return false;
  });

  $('#lsd .show_used').click(function()
  {
    $.cookie('ls_show_unused', 0, { expires: 30.5, path: '/' });
    window.location = window.location;
    return false;
  });

  positionContextBox();

  var cbox = $('#lsd .contextbox')[0];

  $('textarea.formEdit').each(function(i, t)
  {
    var e = this;
    var name = $(this).attr('name');

    var set_active_lstring = function()
    {
      if (name == active_lstring) return;

      if (active_lstring != null)
        changeLSLangTo(phpvars.lstrings[active_lstring], phpvars.active_lang);

      active_lstring = name;

      var coord = $('#'+name).offset();
      if(!coord) return;
      coord.top -= $(cbox).height() + 1;

      var props = { top: coord.top };

      $(cbox).stop();

      var ls = phpvars.lstrings[name];
      var post = {
        component: ls.cmp,
        name: ls.name
      };

      if (ls.strings != null)
      {
        changeLSLangTo(ls, ls.lang);
      }
      else
      {
        $('.inner', cbox).css('visibility', 'hidden');

        ajaxCall('getstrings', '', post, function(data)
        {
          try {
            var result = $.parseJSON(data);
          } catch (e)
          {
            alert('getstrings error: '+e+"\nData: "+data);
            return;
          }

          ls['strings'] = result.strings;
          ls['lang'] = phpvars.active_lang;

          changeLSLangTo(ls, phpvars.active_lang);

          $('.inner', cbox).css('visibility', '');
        });

      }

      if ($(cbox).css('opacity') == 0 || $(cbox).css('display') == 'none')
      {
        $(cbox).css('opacity', 0).css('display', 'block').css('top', coord.top+'px');
        props['opacity'] = 1;
        delete props.top;
      }
      else
        $(cbox).css('opacity', 1);

      $(cbox).animate(props, 500, 'easeOutCubic');

    }

    $('#check_'+name).change(set_active_lstring);
    $(t).focus(set_active_lstring);
  });


  $('a', cbox).click(function()
  {
    var ls = phpvars.lstrings[active_lstring];
    var lang = $(this).attr('rel');

    changeLSLangTo(ls, lang);
    $('#'+active_lstring).focus();

    return false;
  });

  var ok_clicked = false;

  $('#lsd > .buttons .ok').click(function()
  {
    if (ok_clicked) return;

    ok_clicked = true;

    if (active_lstring != null)
      changeLSLangTo(phpvars.lstrings[active_lstring], phpvars.active_lang);

    var data = { strings: {} };
    var count = 0;
    for (var lskey in phpvars.lstrings)
    {
      var ls = phpvars.lstrings[lskey];
      if (ls.changed)
      {
        if (data.strings[ls.cmp] == null) data.strings[ls.cmp] = {};
        data.strings[ls.cmp][ls.name] = ls.strings;
        count++;
      }
    }

    if (count > 0)
    {
      $('#lsd > .buttons .ajaxload').show();
                           
      ajaxCall('savestrings', '', unescape(JSON.stringify(data).replace(/\\u/g, '%u')), function(data)
      {
        $('#lsd > .buttons .ajaxload').hide();
        ok_clicked = false;

        if (data != '1')
        {
          alert("savestrings error!\nData: "+data);
          return;
        }

        for (var lskey in phpvars.lstrings)
        {
          phpvars.lstrings[lskey].changed = false;
        }

        window.scroll(null, 0);
      });

    }
    else
    {
      ok_clicked = false;
      window.scroll(null, 0);
    }

    return false;
  })

  $('#lsd > .buttons .cancel').click(function()
  {
    window.location = window.location;
    return false;
  })

});


</script>

<?

  function colLetter($i)
  {
    return chr(ord('A')+$i);
  }

  function doImport($file){

    $site_id = $GLOBALS['site_id'];

    require_once($GLOBALS['cfgDirRoot'].'library/PHPExcel.php');

    $excel = PHPExcel_IOFactory::load($file['tmp_name']);
    $sheet = $excel->getActiveSheet();

    if($sheet->getCell('B1')->getCalculatedValue() != "id"){
      return "Nekorekts XLS faila formāts";
    }

    $i = 2;
    $langs = array();
    do{
     $next_lang = $sheet->getCell(colLetter($i++).'1')->getCalculatedValue();

     if($next_lang){
      $lang = sqlQueryRow("SELECT * FROM ".$site_id."_languages WHERE shortname='".strtolower($next_lang)."'");
      $langs[$lang['language_id']] = $lang['shortname'];
     }

    }while($next_lang);

    if(empty($langs)){
      return "Nekorekts XLS faila formāts";
    }

    $rows = $sheet->getHighestRow(); // total rows
    $active_component = '';

    for ($y=2; $y<=$rows; $y++){

      $next_component = $sheet->getCell('A'.$y)->getCalculatedValue();
			#echo "component: ".$next_component."<br />";
      if($next_component){
        $active_component = $next_component;
      }

      $key = $sheet->getCell('B'.$y)->getCalculatedValue();
      if(empty($key)) continue;

      //sqlQuery('DELETE FROM ' . $site_id . '_languagestrings WHERE component="'.$active_component.'" AND name="'.mysql_real_escape_string($key).'"'); // delete

			$default_value = ''; // everyrow starts with empty default value
      $missing_default = array();

      $col = 2;

      // check lang info
			$info_check = DB::GetRow("SELECT * FROM ".$site_id."_languagestrings_info WHERE name = :name AND component = :cmp", array(":name" => $key, ":cmp" => $active_component));
      if(!$info_check){
       	DB::Insert($site_id . '_languagestrings_info', array(
         	"name" => $key,
					"component" => $active_component,
					"is_used" => 1
				));
      }elseif($info_check && !$info_check['is_used']){
       	DB::Update($site_id . '_languagestrings', array("id" => $info_check['id']), array("is_used" => 1));
      }

      foreach($langs as $id => $shortname){

      	$row = DB::GetRow('SELECT * FROM ' . $site_id . '_languagestrings WHERE component = :cmp AND name = :key AND language = :lang', array(":cmp" => $active_component, ":key" => $key, ":lang" => $id));

        $text = $sheet->getCell(colLetter($col++).$y)->getCalculatedValue();
        $text = $text ? $text : "";

        if($row){ // existing row.. update

        	$update = array("value" => $text);

        	if(!$row['default'] && $text){
          	$update['default'] = $text;
        	}

        	DB::Update($site_id . '_languagestrings', array("ls_id" => $row['ls_id']), $update);

        }elseif($text){ // new row.. insert

        	DB::Insert($site_id . '_languagestrings', array(
	        	'name' => $key,
						'component' => $active_component,
	          'language' => $id,
						'value' => $text,
						'default' => $text
					));

        }

      }

    }

    // update ls cache files
		$res=DB::GetTable("select name,component,value,`default`,language from ".$GLOBALS['site_id']."_languagestrings order by language,component,name");
		$files=array();
		if (!file_exists(TRANSLATION_FOLDER)){
			mkdir(TRANSLATION_FOLDER);
		}
		foreach ($res as $r){
			if (!$files[$r['language'].'.'.$r['component']]){
				$files[$r['language'].'.'.$r['component']]=fopen(TRANSLATION_FOLDER.$r['language'].'.'.$r['component'].'.lang.php','w');
				fwrite($files[$r['language'].'.'.$r['component']],'<?php'."\n");
			}
			$value = $r['value'] ? $r['value'] : $r['default'];
			fwrite($files[$r['language'].'.'.$r['component']],'$languagestrings['.$r['language'].'][\''.$r['component'].'\'][\''.$r['name'].'\']=\''.str_replace("'","\'",str_replace('\\','\\\\',$value)).'\';'."\n");
		}
		foreach ($files as $k=>$f){
			fwrite($f,'?>');
			fclose($f);
		}

  }
