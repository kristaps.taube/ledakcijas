<?
$action = $_GET["action"];
$GLOBALS['site_id'] = $_GET["site_id"];
$site_id = $_GET["site_id"];
$GLOBALS['tabs'] = 'site';
//check site license
$domain = $GLOBALS['domain'];
if(!CheckLicense($domain))
{
  RedirectNoLicense();
}

unset($perm_modsite);
if(($site_id == $GLOBALS["currentUserSiteID"])or(!$GLOBALS["currentUserSiteID"]))
  $perm_modsite = CheckPermission(6, $site_id);
if(!$perm_modsite)
  AccessDenied(False);

unset($perm_managepages);
if(($site_id == $GLOBALS["currentUserSiteID"])or(!$GLOBALS["currentUserSiteID"]))
  $perm_managepages = CheckPermission(8, $site_id);


$GLOBALS['PageEncoding'] = PageEncoding($id, $site_id);
$GLOBALS['maincharset']  = PageEncoding($id, $site_id, false);

$docScripts['sellanguage'] = "sellanguage.default.php";
include($GLOBALS['cfgDirRoot']."backend/modules/"."sellanguage.setlanguage.php");
$GLOBALS['havelanguages'] = HaveLanguages($site_id);
$GLOBALS['PageEncoding'] = FullEncoding($GLOBALS['currentlanguage']);
$GLOBALS['maincharset']  = $GLOBALS['currentlanguage'];


switch ($action) {

  // page properties form
  case "properties_form":
    $pagedev_id = sqlQueryValue("SELECT pagedev_id, lastmod FROM ".$site_id."_pagesdev WHERE page_id=".$_GET['id']." ORDER BY lastmod DESC LIMIT 1");
    $_GET['id'] = $pagedev_id;
    $docTemplate = "form.htm";
    $GLOBALS['resizeauto'] = 1;
    $docScripts['body'] = "pages.properties.php";

    $opt = getOptionByPath('_pages\\'.intval($_GET['id']), 0, $site_id);
    $docScripts['body'] = Array(
      "{%langstr:page_mainprops%}" => "pages.properties.php",
      "{%langstr:page_extraprops%}" => "pages.options.php"
    );

    break;

  case "delpage":
  case "moveup":
  case "movedown":
    $page_id = $_GET['page_id'];
    if (!$page_id) $page_id = $_GET['id'];
    $pagedev_id = sqlQueryValue("SELECT pagedev_id, lastmod FROM ".$site_id."_pagesdev WHERE page_id=".$page_id." ORDER BY lastmod DESC LIMIT 1");
    $_GET['id'] = $pagedev_id;
    $docScripts['exec'] = "pages.exec.php";
    Header("Location: ?module=pageframe&site_id=" . $GLOBALS['site_id']. "&page_id=" . $_GET['page_id']);
    break;

  case "close":
    $docTemplate = "close.htm";
    break;

  case "modpage":
  case "addpage":
    $docTemplate = "close_editor.htm";
    if($_POST['page_id'])
      $docStrings['urlend'] = "&page_id=" . $_POST['page_id'];
    if ($_POST['pagedev_id'])
        $page_id = sqlQueryValue("select page_id from ".$GLOBALS['site_id']."_pagesdev where pagedev_id=".$_POST['pagedev_id']);
    $docStrings['location'] = "?module=pageframe&site_id=".$GLOBALS['site_id']."&pagedev_id=".$_POST['pagedev_id']."&page_id=".$page_id."&action=frame_page_tree";
    $docScripts['exec'] = "pages.exec.php";
    break;


  default:
    $docTemplate = "lefttree.html";
    $docScripts['body'] = "pageframe.tree.php";

    $docScripts['sellanguage'] = "sellanguage.visual_editor.php";
    $GLOBALS['havelanguages'] = HaveLanguages($site_id);
    $GLOBALS['PageEncoding'] = FullEncoding($GLOBALS['currentlanguage']);
    $GLOBALS['maincharset']  = $GLOBALS['currentlanguage'];

    if($perm_managepages)
    {

      require($GLOBALS['cfgDirRoot']."library/"."class.toolbar.php");
      $Toolbar = new Toolbar();
      $Toolbar->cellpadding = 1;
      $Toolbar->AddButton("add", "{%langstr:toolbar_add%}", "1.gif", "javascript:openDialog('?module=pages&site_id=".$GLOBALS['site_id']."&action=create&parent='+foldersTree.selectedpage, 400, 550, 1, 0);", "{%langstr:hint_add_page%}", "", True);
      $Toolbar->AddButton("edit", "{%langstr:toolbar_edit%}", "2.gif", "javascript:if(foldersTree.selectedpage)openDialog('?module=pageframe&site_id=".$GLOBALS['site_id']."&action=properties_form&id='+foldersTree.selectedpage, 400, 550, 1, 0);", "{%langstr:toolbar_hint_edit_page_prop%}", "", True);
      $Toolbar->AddLink("up", "{%langstr:toolbar_up%}", "3.gif", "javascript:if(foldersTree.selectedpage)window.location='?module=pageframe&site_id=".$GLOBALS['site_id']."&action=moveup&page_id='+foldersTree.selectedpage+'&id='+foldersTree.selectedpage;", "{%langstr:hint_up%}", "", True);
      $Toolbar->AddLink("down", "{%langstr:toolbar_down%}", "4.gif", "javascript:if(foldersTree.selectedpage)window.location='?module=pageframe&site_id=".$GLOBALS['site_id']."&action=movedown&page_id='+foldersTree.selectedpage+'&id='+foldersTree.selectedpage;", "{%langstr:hint_down%}", "", True);
      $Toolbar->AddLink("del", "{%langstr:col_del%}", "5.gif", "javascript:if(foldersTree.selectedpage){window.location='?module=pageframe&site_id=".$GLOBALS['site_id']."&action=delpage&page_id=0&refrframe=1&id='+foldersTree.selectedpage;}", "{%langstr:toolbar_hint_page_delete%}", "{%langstr:toolbar_ask_page_delete%}", True);
      $docStrings['toolbar'] = $Toolbar->output();
      //hidden, non-existing buttons
      $docStrings['toolbar'] .= '<div id="toolbarbutontd_add2" style="display:none"
        onclick="' . "javascript:openDialog('?module=pages&site_id=".$GLOBALS['site_id']."&action=create&sibiling='+foldersTree.selectedpage, 400, 550, 1, 0);" . '"></div>';
      $docStrings['toolbar'] .= '<div id="toolbarbutontd_perms" style="display:none"
        onclick="' . "javascript:if(foldersTree.selectedpage)openDialog('?module=permissions&action=permission_frame&mod=page_form&close=1&site_id=".$GLOBALS['site_id']."&data=".$GLOBALS['site_id']."&data2='+foldersTree.selectedpage, 800, 600);" . '"></div>';

    }
    $docStrings['toolbar'] .= '<div id="toolbarbutontd_prev" style="display:none"
        onclick="' . "javascript:if(foldersTree.selectedpage)openNewWindow('?module=pages&site_id=".$GLOBALS['site_id']."&action=page_redirect&id='+foldersTree.selectedpage);" . '"></div>';

}

?>
