<?

$action = $_GET["action"];
$perm_manageusers = CheckPermission(2);

unset($perm_managesiteusers);
if($_GET['site_id'])
{
  $GLOBALS['tabs'] = 'site';
  $site_id = $_GET['site_id'];
  if($site_id == $GLOBALS["currentUserSiteID"])
    $perm_managesiteusers = CheckPermission(10, $site_id);
}else if(($GLOBALS['currentUserSiteID'])and($GLOBALS['currentUserSiteID']!=$_GET['site_id']))
{
  Header("Location: index.php?site_id=" . $GLOBALS['currentUserSiteID'] . "&module=" . defaultSiteModule);
  Die;
}

$GLOBALS['PageEncoding'] = PageEncoding(0, 0);
$GLOBALS['maincharset']  = PageEncoding(0, 0, false);



switch ($action) {


  case "create":
    $docTemplate = "form.htm";
    $GLOBALS['resizeauto'] = 1;
    $docTitle = "{%langstr:create_user%}";
    $docScripts['body'] = "users.properties.php";
    break;

  // site properties form
  case "properties_form":
    $docTemplate = "form.htm";
    $GLOBALS['resizeauto'] = 1;
    $docScripts['body'] = "users.properties.php";
    break;

  case "close":
    $docTemplate = "close.htm";
    break;

  case "moduser":
  case "adduser":
    $docTemplate = "close.htm";
    $docScripts['exec'] = "users.exec.php";
    break;

  case "deluser":
    $docScripts['exec'] = "users.exec.php";
    if($site_id)
      Header("Location: ?module=users&site_id=$site_id");
    else
      Header("Location: ?module=users");
    break;
    
  case "grouplist":
    $docTemplate = "dlg_simple.htm";
    $docTitle = "{%langstr:select_groups%}";
    $GLOBALS['dlgtype'] = "checkbox";
    $docStrings['label'] = "{%langstr:group_ids%}:";
    $docStrings['frame'] = "?module=users&action=grouplistframe&site_id=$site_id";
    break;
    
  case "grouplistframe":
    $docTemplate = "checkbox.htm";
    $docScripts['body'] = "users.grouplist.php";
    break;

  case "userlist":
    $docTemplate = "dlg_simple.htm";
    $docTitle = "Select Users";
    $GLOBALS['dlgtype'] = "checkbox";
    $docStrings['label'] = "User IDs:";
    $docStrings['frame'] = "?module=users&action=userlistframe&site_id=$site_id";
    break;
    
  case "userlistframe":
    $docTemplate = "checkbox.htm";
    $docScripts['body'] = "users.userlist.php";
    break;

  default:
    if(($perm_manageusers)or($perm_managesiteusers))
    {
      /*require($GLOBALS['cfgDirRoot']."library/"."class.toolbar.php");
      $Toolbar = new Toolbar();
      $Toolbar->AddButton("add", "New User", "new_user.gif", "openDialog('?module=users&action=create&site_id=$site_id', 400, 503, 1);", "Creates new user account");
      $Toolbar->AddButton("props", "User Properties", "edit_user.gif", "if(SelectedRowID)openDialog('?module=users&action=properties_form&site_id=$site_id&id='+SelectedRowID, 400, 503, 1);", "Allows to edit user properties");
      $Toolbar->AddLink("del", "Delete", "delete_user.gif", "if(SelectedRowID)window.location='?module=users&action=deluser&site_id=$site_id&id='+SelectedRowID;", "Deletes selected user account", "Are you sure you want to delete selected user account?");
      if($site_id)
        $Toolbar->AddButton("perms", "Site Permissions", "edit_permissions.gif", "javascript:openDialog('?module=permissions&action=permission_frame&mod=site_form&hideitems=1&close=1&site_id=$site_id&data=$site_id&data2=&uid='+SelectedRowID, 800, 600, 0, 1);", "Edit web site access permissions");
      else
        $Toolbar->AddButton("perms2", "Access Permissions", "edit_permissions.gif", "javascript:openDialog('?module=permissions&action=permission_frame&hideitems=1&close=1&uid='+SelectedRowID, 800, 600, 0, 1);", "Edit global access permissions");

      $docStrings['toolbar'] = $Toolbar->output();*/

      require($GLOBALS['cfgDirRoot']."library/"."class.toolbar2.php");
      $Toolbar = new Toolbar2();

      $Toolbar->AddSpacer();
      $Toolbar->AddButtonImage('add', 'users_new', '{%langstr:new_user%}', "", "openDialog('?module=users&action=create&site_id=$site_id', 400, 503, 1);", 31, "{%langstr:add_new_user%}");
      $Toolbar->AddButtonImage('props', 'users_properties', '{%langstr:edit_user%}', "", "if(SelectedRowID)openDialog('?module=users&action=properties_form&site_id=$site_id&id='+SelectedRowID, 400, 503, 1);", 31, "{%langstr:edit_user_properties%}");
      $Toolbar->AddSeperator();
      if($site_id)
        $Toolbar->AddButtonImage("perms", 'users_site_permissions', "{%langstr:toolbar_site_permissions%}", "", "javascript:openDialog('?module=permissions&action=permission_frame&mod=site_form&hideitems=1&close=1&site_id=$site_id&data=$site_id&data2=&uid='+SelectedRowID, 800, 600, 0, 1);", 31, "{%langstr:toolbar_edit_site_permissions%}");
      else
        $Toolbar->AddButtonImage("perms2", 'users_site_permissions', "{%langstr:toolbar_access_permissions%}", "", "javascript:openDialog('?module=permissions&action=permission_frame&hideitems=1&close=1&uid='+SelectedRowID, 800, 600, 0, 1);", 31, "{%langstr:toolbar_edit_global_permissions%}");
        $Toolbar->AddSeperator();
      $Toolbar->AddButtonImage('del', 'delete', "{%langstr:col_del%}", "javascript: if(SelectedRowID)window.location='?module=users&action=deluser&site_id=$site_id&id='+SelectedRowID;", "", 31, "{%langstr:delete_selected_users%}", "{%langstr:sure_delete_selected_users%}");
      $Toolbar->AddSeperator();
      $Toolbar->AddButtonImage("help", 'help', "{%langstr:toolbar_help%}", $GLOBALS['cfgWebRoot']."help/help.htm?category=lietotaji", "", 31, "","","_blank");

      $docStrings['toolbar'] =  $Toolbar->output();
    }

    $docTemplate = "main.htm";
    $docTitle = "{%langstr:users%}";
    $docScripts['body'] = "users.default.php";
    break;

}
