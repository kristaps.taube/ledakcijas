<?
  $perm_managecontents = $GLOBALS['perm_managecontents'];
  if(!$perm_managecontents)
    AccessDenied(True);

  require_once ($GLOBALS['cfgDirRoot']."library/".'class.formgen.php');

  $id = $_GET["id"];
  $GLOBALS['page_id'] = $id;
  $page_id = $id;
  $site_id = $GLOBALS['site_id'];

  if($page_id>0)
  {   //real page
    $template = sqlQueryValue("SELECT template FROM ". $site_id . "_pages WHERE page_id=$id");
  }else
  { //template view
    $template = sqlQueryValue("SELECT template_id  FROM ". $site_id . "_views WHERE view_id=".-$id);
  }
  if($template)
  {
    list($data, $copybody) = sqlQueryRow("SELECT body, copybody FROM " . $site_id . "_templates WHERE template_id=$template");
    while($copybody)
      list($data, $copybody) = sqlQueryRow("SELECT body, copybody FROM " . $site_id . "_templates WHERE template_id=$copybody");
  }
  
  $data = attachSubTemplates($data, $site_id);

  $r = componentArray($data, $page_id);
  if(!count($r))
  {
    echo "No components in this page";
  }
  else
  {
    $form_data = Array();
    for($f=0; $f<count($r); $f++)
    {
      if ($_GET['component']==$r[$f]["name"]) displayPropertiesTable($r[$f]["type"], $r[$f]["name"], $site_id, $id, $form_data);
    }
    $Inspector = new Inspector();
    $Inspector->properties = $form_data;
    $Inspector->opened = true;
    $Inspector->buttons =true;
    $Inspector->cancel ="closeparent";
    $Inspector->name = "Form";
    $Inspector->action = '?module=' .$GLOBALS['module']. '&site_id=' .$site_id. '&page_id=' .$id. '&id=' .$id. '&action=updatecontrols&redirect=1';
    
    $filename = $GLOBALS['cfgDirRoot'] . "components/class.".$_GET['type'].".php";
    if(file_exists($filename))
    {
      $GLOBALS['component_name'] = $_GET['component'];
      ob_start();
      include_once($filename);
      $type = $_GET['type'];
      $obj = new $type($_GET['component']);
      addDefaultProperties($obj);
      $obj->StandartEdit();
      $html = ob_get_contents();
      ob_end_clean();
      echo $html;
    }    
    
    echo $Inspector->output();

  }
  
?>
