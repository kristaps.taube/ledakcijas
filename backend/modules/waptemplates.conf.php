<?
$action = $_GET["action"];
$GLOBALS['site_id'] = $_GET["site_id"];
$site_id = $_GET["site_id"];
$GLOBALS['tabs'] = 'site';


unset($perm_modsite);
if(($site_id == $GLOBALS["currentUserSiteID"])or(!$GLOBALS["currentUserSiteID"]))
  $perm_modsite = CheckPermission(6, $site_id);
if(!$perm_modsite)
  AccessDenied(False);

unset($perm_managetemplates);
if(($site_id == $GLOBALS["currentUserSiteID"])or(!$GLOBALS["currentUserSiteID"]))
  $perm_managetemplates = CheckPermission(23, $site_id);

$GLOBALS['PageEncoding'] = PageEncoding(0, $site_id);
$GLOBALS['maincharset']  = PageEncoding(0, $site_id, false);

$docScripts['sellanguage'] = "sellanguage.default.php";
include($GLOBALS['cfgDirRoot']."backend/modules/"."sellanguage.setlanguage.php");
$GLOBALS['PageEncoding'] = FullEncoding($GLOBALS['currentlanguage']);
$GLOBALS['maincharset']  = $GLOBALS['currentlanguage'];



switch ($action) {


  case "create":
    $docTemplate = "form.htm";
    $GLOBALS['resizeauto'] = 1;
    $docTitle = "{%langstr:create_template%}";
    $docScripts['body'] = "waptemplates.properties.php";
    break;

  // site properties form
  case "properties_form":
    $docTemplate = "form.htm";
    $GLOBALS['resizeauto'] = 1;
    $docScripts['body'] = "waptemplates.properties.php";
    break;
/*
  case "createview":
    $docTemplate = "form.htm";
    $GLOBALS['resizeauto'] = 0;
    $docTitle = "Create View";
    $docScripts['body'] = "templates.view.php";
    break;

  case "editview":
    $docTemplate = "form.htm";
    $GLOBALS['resizeauto'] = 0;
    $docTitle = "Edit View";
    $docScripts['body'] = "templates.view.php";
    break;
*/
  case "components_form":
    $docTemplate = "form.htm";
    $docScripts['body'] = "waptemplates.components.php";
    $perm_managecontents = CheckPermission(24, $site_id, $_GET['id']);
    break;

  case "contents_form":
    $docTemplate = "templ_win.html";
    $docScripts['exec'] = "waptemplates.contents.php";
    $perm_managecontents = CheckPermission(24, $site_id, $_GET['id']);
    break;

  case "close":
    $docTemplate = "close.htm";
    break;

  case "modtemplate":
  case "addtemplate":
  case "modcontents":
  case "updatecontrols":
  /*case "saveview":
  case "updateview":*/
    $docTemplate = "close.htm";
    //$docTemplate = "form.htm";
    $docScripts['exec'] = "waptemplates.exec.php";
    $perm_managecontents = CheckPermission(24, $site_id, $_GET['id']);
    break;

  case "deltemplate":
  /*case "delview":*/
  case "movetemplateup":
  case "movetemplatedown":
  /*case "moveviewup":
  case "moveviewdown":*/
    $docScripts['exec'] = "waptemplates.exec.php";
    Header("Location: ?module=waptemplates&site_id=" . $site_id);
    break;
  default:

    if($perm_managetemplates)
    {
      $havestrings = $data = sqlQueryValue("SELECT Count(1) FROM " . $site_id . "_strings WHERE page=-1");
      /*require($GLOBALS['cfgDirRoot']."library/"."class.toolbar.php");
      $Toolbar = new Toolbar();
      $Toolbar2 = new Toolbar();
      $Toolbar2->prefix = 't2';
      $Toolbar->AddButton("add", "New Template", "new_template.gif", "javascript:openDialog('?module=waptemplates&site_id=" . $GLOBALS['site_id'] . "&action=create', 400, 190, 0, 0);", "Add new Wap Template");
      $Toolbar2->AddButton("add2", "New Template", "new_template.gif", "javascript:openDialog('?module=waptemplates&site_id=" . $GLOBALS['site_id'] . "&action=create', 400, 190, 0, 0);", "Add new Wap Template");
      $Toolbar->AddButton("edit", "Template Contents", "edit_contents.gif", "javascript:if(SelectedRowID>0)openDialog('?module=waptemplates&site_id=" . $GLOBALS['site_id'] . "&action=contents_form&id='+SelectedRowID, 620, 540, 1, 1);", "Edit Template contents");
      $Toolbar->AddButton("defval", "Default Values", "edit_contents2.gif", "javascript:if(SelectedRowID>0)openDialog('?module=waptemplates&site_id=" . $GLOBALS['site_id'] . "&action=components_form&id='+SelectedRowID, 500, 520, 1, 1);", "Edit default values for selected Template");
      $Toolbar2->AddButton("defval2", "Default Values", "edit_contents2.gif", "javascript:if(SelectedRowID<0)openDialog('?module=wap&site_id=" . $GLOBALS['site_id'] . "&action=components_form_frame&id='+SelectedRowID, 500, 520, 1, 1);", "Edit default values for selected View");
      $Toolbar->AddButton("perms", "Template Permissions", "edit_permissions.gif", "javascript:if(SelectedRowID>0)openDialog('?module=permissions&action=permission_frame&mod=waptemplate_form&close=1&site_id=$site_id&data=$site_id&data2='+SelectedRowID, 800, 600, 0, 1);", "Edit selected template access permissions");
      $Toolbar->AddLink("up", "Up", "move_up.gif", "javascript:if(SelectedRowID)window.location='?module=waptemplates&site_id=" . $GLOBALS['site_id'] . "&action=movetemplateup&selrow='+SelectedRowID+'&id='+SelectedRowID;", "");
      $Toolbar->AddLink("down", "Down", "move_down.gif", "javascript:if(SelectedRowID)window.location='?module=waptemplates&site_id=" . $GLOBALS['site_id'] . "&action=movetemplatedown&selrow='+SelectedRowID+'&id='+SelectedRowID;", "");
      $Toolbar->AddLink("del", "Delete", "delete_template.gif", "javascript:if(SelectedRowID>0)window.location='?module=waptemplates&site_id=" . $GLOBALS['site_id'] . "&action=deltemplate&id='+SelectedRowID;", "Delete selected Template", "Are you sure you want to delete selected Template?");
      $docStrings['toolbar'] = '<div id="Toolbar1Div">' . $Toolbar->output() . '</div>'
                              .'<div id="Toolbar2Div" style="display:none;">' . $Toolbar2->output() . '</div>';*/

      require($GLOBALS['cfgDirRoot']."library/"."class.toolbar2.php");
      $Toolbar = new Toolbar2();

      $Toolbar->AddSpacer();

      $Toolbar->AddButtonImage('add', 'templates_new', '{%langstr:new_template%}', "", "javascript:openDialog('?module=waptemplates&site_id=" . $GLOBALS['site_id'] . "&action=create', 400, 190, 0, 0);", 31, "{%langstr:add_new_wap_template%}");
      $Toolbar->AddSeperator();
      $Toolbar->AddButtonImage('edit', 'templates_contents', "{%langstr:template_contents%}", "", "javascript:if(SelectedRowID>0)openDialog('?module=waptemplates&site_id=" . $GLOBALS['site_id'] . "&action=contents_form&id='+SelectedRowID, 620, 540, 1, 1);", 31, "{%langstr:edit_wap_template_contents%}");
      $Toolbar->AddButtonImage('defval', 'templates_default_values', "{%langstr:default_values%}", "", "javascript:if(SelectedRowID>0)openDialog('?module=waptemplates&site_id=" . $GLOBALS['site_id'] . "&action=components_form&id='+SelectedRowID, 500, 520, 1, 1);", 31, "{%langstr:edit_default_values_selected_wap%}");
      $Toolbar->AddSeperator();
      $Toolbar->AddButtonImage('perms', 'templates_permissions', "{%langstr:template_permissions%}", "", "javascript:if(SelectedRowID>0)openDialog('?module=permissions&action=permission_frame&mod=waptemplate_form&close=1&site_id=$site_id&data=$site_id&data2='+SelectedRowID+'&single_template=1', 800, 600, 0, 1);", 31, "{%langstr:edit_wap_template_permissions%}");

      $Toolbar->AddSeperator();

      $Toolbar->AddButtonImage('up', 'up', "{%langstr:toolbar_up%}", "javascript:if(SelectedRowID)window.location='?module=waptemplates&site_id=" . $GLOBALS['site_id'] . "&action=movetemplateup&selrow='+SelectedRowID+'&id='+SelectedRowID;", "", 31, "{%langstr:move_template_up%}");
      $Toolbar->AddButtonImage('down', 'down', "{%langstr:toolbar_down%}", "javascript:if(SelectedRowID)window.location='?module=waptemplates&site_id=" . $GLOBALS['site_id'] . "&action=movetemplatedown&selrow='+SelectedRowID+'&id='+SelectedRowID;", "", 31, "{%langstr:move_template_down%}");
      $Toolbar->AddButtonImage('del', 'delete', "{%langstr:col_del%}", "javascript:if(SelectedRowID>0)window.location='?module=waptemplates&site_id=" . $GLOBALS['site_id'] . "&action=deltemplate&id='+SelectedRowID;", "", 31, "{%langstr:delete_selected_template%}", "{%langstr:sure_delete_template%}");

      $Toolbar->AddSeperator();

      $docStrings['toolbar'] =  $Toolbar->output();
    }

    $docTemplate = "main.htm";
    $docTitle = "Wap Templates";
    $docScripts['body'] = "waptemplates.default.php";
    break;

}

?>
