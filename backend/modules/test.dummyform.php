<?

// Define form

$form_data = Array(

  "id"          => Array(
    "label"     => "Hidden:",
    "type"      => "hidden"
  ),

  "title"        => Array(
    "label"     => "Title:",
    "type"      => "str",
    "maxlength" => "20"
  ),

  "html" => Array(
    "label"     => "Text:",
    "type"      => "html",
    "rows"      => "4",
    "cols"      => "30"
  ),

  "check"      => Array(
    "label"     => "Visible:",
    "type"      => "check",
    "value"     => "0"
  ),

  "list"   => Array(
    "label"     => "Type:",
    "type"      => "list",
    "lookup"    => Array('0:Standard', '1:Special'),
    "value"     => 0
  )

);


// Use FormGen to bring this out

  require($GLOBALS['cfgDirRoot']."library/"."class.formgen.php");
  $FormGen = new FormGen();
  $FormGen->action = "?module=".$GLOBALS['module']."&action=close";
  $FormGen->cancel = "close";
  //$FormGen->title = "Test";
  $FormGen->properties = $form_data;
  $FormGen->buttons = false;
  echo $FormGen->output();

?>
