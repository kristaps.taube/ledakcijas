<?
function read_dirs($webroot, $dir, $path) {
  global $cfgCmsDirRoot, $cfgWebRoot;

  chdir($dir);

  $handle = opendir($dir);
  while ($file = readdir($handle)) {
    if (is_dir($file) && $file != ".")
      $dirlist[] = $file;
  }
  closedir($handle);

  $i = 0;
  asort($dirlist);
  foreach($dirlist as $file) {
    if (!($file == '..' && $path == '')) {
      if ($file == '..') $url = fileUp($path); elseif ($path == '') $url = $file; else $url = $path.'/'.$file;
      //$data[$i++] = $url;
      //echo "dir: ".$dir." path: ".$path." file: ".$file."<br>";
      $data[] = $url; 
      if ($file!="..") $data_sub = read_dirs($webroot, $dir."/".$file, $url);
      foreach ($data_sub as $key=>$d)
          $data[]=$d;
    }
  }

  chdir($cfgCmsDirRoot);

  return array_unique($data);
}

$perm_managefiles = $GLOBALS['perm_managefiles'];
if(!$perm_managefiles)
  AccessDenied(True);

$directory = sqlQueryValue("SELECT dirroot FROM sites WHERE site_id = '".$GLOBALS['site_id']."'");
$webroot = sqlQueryValue("SELECT domain FROM sites WHERE site_id = '".$GLOBALS['site_id']."'");
//$path = str_replace("..", "", $GLOBALS['path']);
$path = "";
if ($path == "/") $path = "";
$directory = fileAddBackslash($directory).$path;

$data = read_dirs('http://'.$webroot.'/', $directory, $path);

$dirs = array();
array_push($dirs,":..");
foreach ($data as $key=>$dir)
    if ($dir) array_push($dirs,$dir.":".$dir);

if ($_GET['move']) $title = "Move";
else $title = "Copy";

$form_data = Array(

  "over"        => Array(
    "label"     => $title." to:",
    "value"     => 3,
    "lookup"    => $dirs,
    "type"      => "list"
  ),

  "file"     => Array(
    "type"      => "hidden",
    "value"	=> $_GET['file']
  ),

  "move"     => Array(
    "type"      => "hidden",
    "value"	=> $_GET['move']
  ),

);

require($GLOBALS['cfgDirRoot']."library/"."class.formgen.php");
$FormGen = new FormGen();
$FormGen->action = "?module=files&action=docopy&site_id=".$GLOBALS['site_id']."&path=".$GLOBALS['path']."&view=".$GLOBALS['view'].($GLOBALS['close'] ? "&close=".$GLOBALS['close'] : "");
$FormGen->title = $title." Selected Files";
$FormGen->cancel = ($GLOBALS['close'] ? "?module=files&action=list&site_id=".$GLOBALS['site_id']."&path=".$GLOBALS['path']."&view=".$GLOBALS['view'] : 'close');
$FormGen->properties = $form_data;
$FormGen->buttons = true;
echo $FormGen->output();


?>
