<?

$site_id = $GLOBALS['site_id'];
$id = $_GET["id"];

$page_id = sqlQueryValue("SELECT page_id FROM ".$site_id."_pagesdev WHERE pagedev_id=".$id);

$cols = Array(

  "preview"     => Array(
    "width"     => "5%",
    "align"     => "center",
    "title"     => "{%langstr:col_prev%}",
  ),

  "title"        => Array(
    "width"     => "60%",
    "title"     => "{%langstr:title%}",
  ),

  "date"        => Array(
    "width"     => "35%",
    "title"     => "{%langstr:date%}",
  )

);

$h_pages = sqlQueryData("SELECT * FROM ".$site_id."_pageshistory WHERE page_id=".$page_id."  ORDER BY date DESC");

foreach ($h_pages as $h_page)
{
    $data[] = array("preview"=>"<a href=\"?module=pages&site_id=$site_id&action=page_redirect&pagehistory_id=".$h_page['pagehistory_id']."&id=".$page_id."\"  target=\"_blank\"><img src=\"gui/images/ico_view.gif\" width=\"16\" height=\"16\" border=\"0\"></a>",
                    "title"       => $h_page['title'],
                    "date"     => date("Y-m-d H:i:s",$h_page['date'])
                    );
}

require($GLOBALS['cfgDirRoot']."library/"."class.datagrid.php");
echo '<div style="width:500px;padding-right:12px;">';
$Table = new DataGrid();
$Table->cols = $cols;
$Table->data = $data;
$Table->footer = false;
$Table->rowformat = " id=\"tablerow{%pagedev_id%}\" ";
echo $Table->output();
echo '</div>';

?>
