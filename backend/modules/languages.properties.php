<?

// Fill some values
  $id = $_GET["id"];
  $action = $_GET['action'];
  $create = ($action == 'create');
  $site_id = $GLOBALS['site_id'];

  if(!$create){
    $row = sqlQueryRow("SELECT * FROM " . $site_id . "_languages WHERE language_id=$id");
    $form_data['language_id']['value'] = $id;
    $form_data['shortname']['value'] = $row['shortname'];
    $form_data['fullname']['value'] = $row['fullname'];
  }

  $invalid = explode(':', $_GET['invalid']);
  foreach($invalid as $i)
  {
    if($i)
      $form_data[$i]['error'] = true;
  }

  foreach(array_keys($_GET) as $key)
  {
    $s = explode('_', $key, 2);
    if($s[0] == 'formdata')
      $form_data[$s[1]]['value'] = stripslashes($_GET[$key]);
  }

  if($_GET['jsmessage'])
  {
    echo '<script language="JavaScript">
          <!--
          alert("' . stripslashes($_GET['jsmessage']) . '");
          <!---->
          </script>';
  }

echo '
';

  if(!$create)
  {
    $action = "?module=".$GLOBALS['module']."&site_id=".$site_id."&action=modlanguage";
  }else{
    $action = "?module=".$GLOBALS['module']."&site_id=".$site_id."&action=addlanguage";
  }

echo '
<table width="100%" cellspacing="0" cellpadding="0" border="0">
  <tr>
    <td>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="formExFrame">
<form name="Form" enctype="multipart/form-data" action="'.$action.'" method="POST">
<tr><td class="formFrame">
<table width="100%" border="0" cellspacing="0" cellpadding="5" class="formInFrame">
<tr>
  <td width="100%" colspan="2" class="formTitle">
    {%langstr:add_new_language%}
  </td>
</tr>
<input type="hidden" name="language_id" value="'.$row['language_id'].'" style="width: 200;">
<tr>
  <td  width="50%"class="formLabel">
    {%langstr:short_name%}:
  </td>
  <td  width="50%"class="formControl">
    <input class="formEdit" name="shortname" value="'.$row['shortname'].'" style="width: 200;">
  </td>
</tr>
<tr>
  <td  width="30%"class="formLabel">
    {%langstr:full_name%}:
  </td>
  <td  width="70%"class="formControl">
    <input class="formEdit" name="fullname" value="'.$row['fullname'].'" style="width: 200;">
  </td>
</tr>
<tr>
  <td  width="30%" class="formLabel">
    {%langstr:default%}:
  </td>
  <td  width="70%" class="formControl">
    <input class="formEdit" name="default" value="1" type="checkbox" '.($row['is_default'] ? "checked='checked'" : "").'/>
  </td>
</tr>
<tr>
  <td  width="30%" class="formLabel">
    {%langstr:disabled%}:
  </td>
  <td  width="70%" class="formControl">
    <input class="formEdit" name="disabled" value="1" type="checkbox" '.($row['disabled'] ? "checked='checked'" : "").' />
  </td>
</tr>

</table>
<table width="100%" border="0" cellpadding="4" cellspacing="4">
<tr><td>&nbsp;</td></tr>
<tr>
  <td align="right">
<input ID="OKButton" class="formButton" type="submit" value="&nbsp;&nbsp;{%langstr:ok%}&nbsp;&nbsp;">&nbsp;<input class="formButton" type="button" value="{%langstr:cancel%}" OnClick="window.close();">&nbsp;<input class="formButton" type="reset" value="{%langstr:reset%}">
  </td>
</tr>
</table>
</td></tr>
</form></table>

    </td>
  </tr>
</table>';


