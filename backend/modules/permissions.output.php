<?
  die();
  $site_id = $_GET['site_id'];
  $perm_manageperms = $GLOBALS['perm_manageperms'];
  $perm_managesiteperms = $GLOBALS['perm_managesiteperms'];
  $action = $GLOBALS['action'];

  include($GLOBALS['cfgDirRoot']."backend/modules/"."permissions.texts.php");
  $permission_type = $GLOBALS['permission_type'];
  $data = $GLOBALS['data'];
  $data2 = $GLOBALS['data2'];

  $where = " WHERE site_id = '0'";
  if($site_id) $where .= " OR site_id='$site_id'";
  $groups = sqlQueryData("SELECT group_id, groupname, site_id FROM groups" . $where);
  $users = sqlQueryData("SELECT user_id, username, site_id FROM users" . $where);

  if ($action=="page_form") {
    if (isset($GLOBALS["collapsebranch"])) $_SESSION['expanded' . $_GET['collapsebranch']] = 0;
    if (isset($GLOBALS["expandbranch"])) $_SESSION['expanded' . $_GET['expandbranch']] = 1; 
  }

  if ($_GET['must_resize'])
  {
    echo '
    <script language="JavaScript">
    <!--
    window.resizeTo(800,600);
    self.moveTo(100,100);
    -->
    </script>
    ';
  }

  //Small javascript for handling permission lists
  ?>
    <script language="javascript">
<!--

function OnDocLoad()
{
  <?
  if($_GET['switchuser'])
  {
    echo '  Form.Select_1.value = "u' . $_GET['switchuser'] . '";';
    echo '  FillFromArray();';
    echo '  FillPagesFromArray();';
  }
  ?>
}
document.body.onload = OnDocLoad;

var PermNum = 0;
var SelPerms = new Array();
var AllPagesSelPerms = new Array();

function FillFromArray()
{
  for (var k=0; k < PermNum; k++)
    {
      try {
        eval('Form.perm'+k+'.checked=false');
      } catch(e) {}
    }

  for(var i=0; i < SelPerms.length; i++)
    {
        for (var j=0; j < SelPerms[i].length; j++)
        {
            try {
                if (SelPerms[i][j]==Form.Select_1.value)
                {
                    eval('Form.perm'+i+'.checked=true');
                }
            } catch(e) {}
        }
    }
}

function FillPagesFromArray()
{  
  for(var i=0; i < AllPagesSelPerms.length; i++)
    {
        try {
            eval('document.Form.perm'+0+'_'+AllPagesSelPerms[i][0]+'.checked=false');
            eval('document.Form.perm'+1+'_'+AllPagesSelPerms[i][0]+'.checked=false');
        } catch(e) {}
    }

  for(var i=0; i < AllPagesSelPerms.length; i++)
    {
      var perm;
      for (var l=1; l<=2; l++)
        {
            perm = l-1;
            for (var j=0; j < AllPagesSelPerms[i][l].length; j++)
            {  
                try {
                    if (AllPagesSelPerms[i][l][j]==Form.Select_1.value)
                    {
                        //alert('document.Form.perm'+j+'_'+AllPagesSelPerms[i][0]+'.checked=true');
                        eval('document.Form.perm'+perm+'_'+AllPagesSelPerms[i][0]+'.checked=true');
                    }
                } catch(e) {}
            }
        }
    }

}

function ChangePerms(PermID,ChkID)
    {
        if (eval('Form.perm'+ChkID+'.checked') == true)
        {
            Form.permchanges.value += "+:" + PermID + ":" + Form.Select_1.value + ";";
        }
        else
        {
            Form.permchanges.value += "-:" + PermID + ":" + Form.Select_1.value + ";";
        }
        //alert(Form.permchanges.value);
    }

function ChangePagePerms(PageID,PermID,ChkID)
    {
        if (ChkID.checked == true)
        {
            Form.pagepermchanges.value += "+:" + PageID + ":" + PermID + ":" + Form.Select_1.value + ";";
        }
        else
        {
            Form.pagepermchanges.value += "-:" + PageID + ":" + PermID + ":" + Form.Select_1.value + ";";
        }
        //alert(Form.pagepermchanges.value);

    }

function ChangeExtraData(doAdd, PermID, extradata)
{
  if(doAdd)
  {
    Form.permchanges.value += "*:" + PermID + ":" + Form.Select_1.value + ":"+extradata+";";
  }else
  {
    Form.permchanges.value += "/:" + PermID + ":" + Form.Select_1.value + ":"+extradata+";";
  }
}

var PermUsersGroups = new Array(
<?
 //Gain list of all users and groups
 $firstitem = true;
 foreach($groups as $group)
  {
    if(!$firstitem) echo ", ";
    $firstitem = false;
    $prefix = 'G:';
    if($group['site_id'])
      $prefix = 'G:site:';
    echo 'new Array("g' . $group['group_id'] . '", "' . $prefix . $group['groupname'] . '")';
  }
  foreach($users as $user)
  {
    if(!$firstitem) echo ", ";
    $firstitem = false;
    $prefix = 'U:';
    if($user['site_id'])
      $prefix = 'U:site:';
    echo 'new Array("u' . $user['user_id'] . '", "' . $prefix . $user['username'] . '")';
  }
?> );

-->
</script>

<?
//TOP LEFT MENU
if (isset($_GET['data2'])) $param = '&data2=';
echo '
<table border="1" width="100%">
<tr>
    <td width="150" valign="top" style="font-size:14px;padding-top:20px;padding-left:20px;">';

if (!$GLOBALS["currentUserSiteID"])
{
    echo '<div><a ';
    if ($action=="") echo 'style="font-weight:bold;"';
    echo ' href="?module=permissions&close=1&site_id='.$_GET["site_id"].'&data='.$_GET["site_id"].$param.'">Global permissions</a></div>';
}

if ($_GET['site_id'])
{
    echo '<div><a ';
    if ($action=="site_form") echo 'style="font-weight:bold;"';
    echo '
    href="?module=permissions&action=site_form&close=1&site_id='.$_GET["site_id"].'&data='.$_GET["site_id"].$param.'">Site Permissions</a></div>';

    if (isset($_GET['data2']))
    {
        echo '<div><a ';
        if ($action=="page_form") echo 'style="font-weight:bold;"';
        if ($_GET['action']=="page_form") $d2 = $_GET['data2'];
        else $d2 = "";
        echo '
        href="?module=permissions&action=page_form&close=1&site_id='.$_GET["site_id"].'&data='.$_GET["site_id"].'&data2='.$d2.'">Page Permissions</a></div>';


        echo '<div><a ';
        if ($action=="template_form") echo 'style="font-weight:bold;"';
        if ($_GET['action']=="template_form") $d2 = $_GET['data2'];
        else $d2 = "";
        echo '
        href="?module=permissions&action=template_form&close=1&site_id='.$_GET["site_id"].'&data='.$_GET["site_id"].'&data2='.$d2.'">Template Permissions</a></div>';
    }
}

echo '
    </td>
    
    <td>
';
//END OF TOP LEFT MENU




echo '<form name="Form" method="POST" action="?module=permissions&action='.$action.'&applied=1&close='.$_GET['close'].'&site_id='.$site_id.'&data='.$data.'&data2='.$data2.'">';

if ( (($_GET['action']=="site_form" || ($_GET['action']=="page_form") || ($_GET['action']=="template_form")) && $_GET['site_id']) || !$_GET['action'])
{

echo '
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="formExFrame">';

echo '
<tr><td class="formFrame">

<table width="100%" border="0" cellspacing="0" cellpadding="5" class="formInFrame">
<tr>
  <td width="100%" colspan="2" class="formTitle">
    Permissions
  </td>
</tr>

<tr>
  <td  width="30%" class="formLabel">Groups and Users:</td>
  <td  width="70%" class="formControl">
  <select class="formEdit" name="Select_1" size="1" onChange="FillFromArray();FillPagesFromArray();" style="width: 100%">
';
 foreach($groups as $group)
  {
    if(!$firstitem) echo ", ";
    $firstitem = false;
    $prefix = 'G:';
    if($group['site_id'])
      $prefix = 'G:site:';
    if ($group['group_id']==$_GET['gid']) $selected = "selected";
    else $selected = "";
    echo '<option value="g'.$group['group_id'].'" '.$selected.'>'.$prefix.$group['groupname'].'</option>';
  }
  foreach($users as $user)
  {
    if(!$firstitem) echo ", ";
    $firstitem = false;
    $prefix = 'U:';
    if($user['site_id'])
      $prefix = 'U:site:';
    if ($user['user_id']==$_GET['uid']) $selected = "selected"; 
    else $selected = "";
    echo '<option value="u'.$user['user_id'] . '" '.$selected.'>'.$prefix.$user['username'].'</option>';
  }
echo '
    </select>
  </td>
</tr>
        </table>';
}



    
    //print_r($_GET);
    if ( ($_GET['action']=="site_form" || $_GET['action']=="page_form" || $_GET['action']=="template_form") && isset($_GET['novariable']) )
    {
        //######################## sites.default.php ##########################
        $cols = Array(
          "title"        => Array(
            "width"     => "30%",
            "title"     => "Title",
            "format"    => "<a href=\"?module=permissions&action=".$action."&close=".$_GET["close"]."&site_id={%site_id%}&data={%site_id%}\">{%title%}</a> ({%license%})"
          ),
          "domain"    => Array(
            "width"     => "25%",
            "title"     => "Domain"
          ),
          "created"     => Array(
            "width"     => "15%",
            "title"     => "Created",
            "format"    => "<font color=\"#999999\">{%created%}</font>"
          ),
          "lastmod"    => Array(
            "width"     => "15%",
            "title"     => "Modified",
            "format"    => "<font color=\"#999999\">{%lastmod%}</font>"
          ),
          "modby"       => Array(
            "width"     => "15%",
            "title"     => "Modified By"
          )
        );


        $site_data = sqlQueryData("SELECT a.site_id as site_id,a.title as title,a.domain as domain,FROM_UNIXTIME(a.created) as created,FROM_UNIXTIME(a.lastmod) as lastmod,b.username as modby FROM sites as a, users as b WHERE a.modby=b.user_id");
        foreach($site_data as $key => $row)
        {
          CheckLicense($row['domain']);
          $site_data[$key]['license'] = $GLOBALS['licensetext'];
          if ($site_data[$key]['site_id']==$_GET['site_id']) $site_data[$key]['title'] = "<b>".$row['title']."</b>";
        }

        echo '
        <script language="JavaScript">
          var selrow = ' . (int) ($data[0][0]) . ';
        </script>';

        require_once($GLOBALS['cfgDirRoot']."library/"."class.datagrid.php");

        $Table = new DataGrid();
        $Table->cols = $cols;
        $Table->data = $site_data;
        $Table->footer = false;
        $Table->rowformat = " onClick=\"javascript:HighLightTR(this, {%site_id%});if(SelectedRowID){ EnableToolbarButtons(che); }else { DisableToolbarButtons(che); }\"";
        echo $Table->output();

        echo '<script language="JavaScript" type="text/javascript">
        /*<![CDATA[*/
        che = new Array("props", "perms", "edit", "del");
        DisableToolbarButtons(che);
        /*]]>*/
        </script>';
        //######################## sites.default.php ##########################
    }

    if ($_GET['action']=="template_form" && $_GET['site_id'])
    {
        //######################## templates.default.php ##########################
        $site_id = $GLOBALS['site_id'];
        $perm_managetemplates = $GLOBALS['perm_managetemplates'];

        $cols = Array(

          "name"        => Array(
            "width"     => "30%",
            "title"     => "Name",
            "format"    => "<a href=\"?module=permissions&site_id=" . $site_id . "&action=template_form&data=".$_GET['data']."&data2={%template_id%}&close=".$_GET["close"]."\">{%name%}</a>"
          ),

          "description" => Array(
            "width"     => "60%",
            "title"     => "Description"
          ),

          "modify"    => Array(
            "width"     => "15%",
            "title"     => "Modify template",
          ),
          "delete"       => Array(
            "width"     => "15%",
            "title"     => "Delete template"
          )

        );

        //if(!$perm_managetemplates)
          //unset($cols['name']['format']);

        $tdata = sqlQueryData("SELECT template_id, name, description FROM " . $site_id . "_templates ORDER BY ind");
        //get templates in data_
        $data_ = array();
        foreach($tdata as $row)
        {
          if ($row['template_id']==$_GET['data2']) $row['name'] = "<b>".$row['name']."</b>";
          $data_[] = array("template_id" => $row['template_id'],
                            "name" => $row['name'],
                            "modify" => '<input type="checkbox" name="perm0_'.$row['template_id'].'" onClick="javascript:ChangePagePerms('.$row['template_id'].',12, perm0_'.$row['template_id'].')">',
                            "delete" => '<input type="checkbox" name="perm1_'.$row['template_id'].'" onClick="javascript:ChangePagePerms('.$row['template_id'].',15, perm1_'.$row['template_id'].')">'
                            );

        }

        //GETTING PERMISSION LIST FOR TEMPLATES       
        foreach ($data_ as $row)
        {
             $selarr = '';
             //modify template persmissions
             $query = "SELECT * FROM permissions WHERE perm_num='12' and data='".$_GET['data']."' and data2='".$row['template_id']."'";
             $perms = sqlQueryData($query);
      
             $sellist = '';
             foreach($perms as $r)
             {
                 if ($r['isuser']) $selel = '"u'; else $selel = '"g';
                 $selel .= $r['id'] . '"';
                 if($sellist) $sellist .= ', ';
                 $sellist .= $selel;
             }
             if($selarr) $selarr .= ', ';
             $selarr .= 'new Array(' . $sellist . ')';
             if($allpages) $allpages .= ', ';
             
             $selarr2 = '';
             //delete template permissions
             $query = "SELECT * FROM permissions WHERE perm_num='15' and data='".$_GET['data']."' and data2='".$row['template_id']."'";
             $perms = sqlQueryData($query);

             $sellist2 = '';
             foreach($perms as $r)
             {
                 if ($r['isuser']) $selel = '"u'; else $selel = '"g';
                 $selel .= $r['id'] . '"';
                 if($sellist2) $sellist2 .= ', ';
                 $sellist2 .= $selel;
             }
             if($selarr2) $selarr2 .= ', ';
             $selarr2 .= 'new Array(' . $sellist2 . ')';
                  
             $allpages .= 'new Array('.$row['template_id'].', ' . $selarr . ', ' . $selarr2 . ' )';
             //echo 'new Array('.$row['template_id'].', ' . $selarr . ', ' . $selarr2 . ' )'."<br>";
        }                    
        //END OF GETTING PERMISSION LIST FOR TEMPLATES

        require_once($GLOBALS['cfgDirRoot']."library/"."class.datagrid.php");
        
        unset($Table);
        $Table = new DataGrid();
        $Table->cols = $cols;
        $Table->data = $data_;
        $Table->footer = false;
        $Table->rowformat = " onClick=\"javascript:HighLightTR(this, {%template_id%});\"";

        //echo '
        //<form style="padding:0px;margin:0px;" name="Form" method="POST" action="?module=permissions&action='.$action.'&applied=1&close='.$_GET['close'].'&site_id='.$site_id.'&data='.$data.'&data2='.$data2.'">
        //';  

        echo $Table->output();

        echo '<script language="JavaScript" type="text/javascript">
        /*<![CDATA[*/
        che = new Array("edit", "addv", "defval", "perms", "del", "up", "down");
        DisableToolbarButtons(che);
        /*]]>*/
        </script>';
        //######################## templates.default.php ##########################
    }

    if ($_GET['action'] == "page_form" && $_GET['site_id'])
    {
        //######################## pages.default.php ##########################
        //function for sorting data in tree structure
        function Nodes($data_, &$newdata, $parent, $level, $justcount, &$goodl)
        {
          global $allpages;
          $goodlanguage = false;
          $nodecount = 0;
          //print_r($data_);
          foreach($data_ as $key => $row)
          {
            if($parent == $row['parent']){
              $nodecount++;

                $row['prefix'] = str_repeat('<img src="gui/images/1x1.gif" width="16" height="16" border="0" align="absmiddle">', $level) . '<img src="gui/images/1x1.gif" width="16" height="16" border="0" align="absmiddle">&nbsp;';
                $newdata[] = $row;
                $c = count($newdata) - 1;
                $childs = Nodes($data_, $newdata, $row['page_id'], $level + 1, 0, $g);
                $goodlanguage = ($g or $goodlanguage);
                if(!$_SESSION['expanded' . $row['page_id']])
                {
                  $newdata = array_slice($newdata, 0, $c+1);
                }

                if($row['language'] != intval($GLOBALS['currentlanguagenum']))
                {
                  //$newdata[$c]['page_id'] = 0;

                  if(!$g)
                  {
                    if($c)
                      $newdata = array_slice($newdata, 0, $c);
                    else
                      $newdata = array();
                    $nodecount--;
                  }else
                  {
                    $newdata[$c]['clicklink'] = "Page in " . sqlQueryValue("SELECT fullname FROM " . $GLOBALS['site_id'] . "_languages WHERE language_id = " . intval($row['language'])) . " (" . $row['name'] . ")";
                  }
                }else
                {
                  if ($row['page_id']==$_GET['data2']) $style = " style='font-weight:bold;' ";
                  else $style = "";
                  $newdata[$c]['clicklink'] = "<a ".$style." href=\"?module=permissions&action=page_form&site_id=" . $_GET['site_id'] . "&action=page_form&data=".$_GET['site_id']."&data2=".$row['page_id']."&close=".$_GET['close']."\">".ShortenString($row['title'],40)."</a>";

                  $newdata[$c]['modify'] = '<input type="checkbox" name="perm0_'.$row['page_id'].'" onClick="javascript:ChangePagePerms('.$row['page_id'].',11, perm0_'.$row['page_id'].')">';
                  $newdata[$c]['delete'] = '<input type="checkbox" name="perm1_'.$row['page_id'].'" onClick="javascript:ChangePagePerms('.$row['page_id'].',13, perm1_'.$row['page_id'].')">';
                  $goodlanguage = true;
                }
                if(($childs)and(isset($newdata[$c]))){
                  if($_SESSION['expanded' . $row['page_id']])
                  {
                    $newdata[$c]['prefix'] = '&nbsp;'.str_repeat('<img src="gui/images/1x1.gif" width="16" height="16" border="0" align="absmiddle">', $level) . '<a href="?module=permissions&action=page_form&site_id=' . $_GET['site_id'] . '&data='.$_GET['site_id'].'&collapsebranch=' . $newdata[$c]['page_id'] . '&close='.$_GET["close"].'"><img src="gui/images/minus.gif" width="11" height="11" border="0" align="absmiddle"></a>&nbsp; ';
                  }else
                  {
                    $newdata[$c]['prefix'] = '&nbsp;'.str_repeat('<img src="gui/images/1x1.gif" width="16" height="16" border="0" align="absmiddle">', $level) . '<a href="?module=permissions&action=page_form&site_id=' . $_GET['site_id'] . '&data='.$_GET['site_id'].'&expandbranch=' . $newdata[$c]['page_id'] . '&close='.$_GET["close"].'"><img src="gui/images/plus.gif" width="11" height="11" border="0" align="absmiddle"></a>&nbsp; ';
                  }
                }

            }

          }
          $goodl = $goodlanguage;
          return $nodecount;
        }

        $site_id = $GLOBALS['site_id'];

        // Let's try table generator

        $cols = Array(

          "title"        => Array(
            "width"     => "30%",
            "title"     => "Title",
            "format"    => "{%prefix%}{%scut%}{%clicklink%}"
          ),

          "description" => Array(
            "width"     => "60%",
            "title"     => "Description"
          ),

          "modify"    => Array(
            "width"     => "15%",
            "title"     => "Modify page",
          ),
          "delete"       => Array(
            "width"     => "15%",
            "title"     => "Delete page"
          )

        );



        $shortcutimage = '<img src="gui/images/shortcut.gif" width="11" height="11" border="0"> ';
        $unsorteddata = sqlQueryData("SELECT page_id, IF(redirect>0, CONCAT('$shortcutimage', '&nbsp;'), '') AS scut, name, title, description, parent, language FROM " . $site_id . "_pages ORDER BY ind");
        $g = false;

        //GETTING PERMISSION LIST FOR PAGES
        foreach ($unsorteddata as $row)
        {
             $selarr = '';
             //modify page persmissions
             $query = "SELECT * FROM permissions WHERE perm_num='11' and data='".$_GET['data']."' and data2='".$row['page_id']."'";
             $perms = sqlQueryData($query);
      
             $sellist = '';
             foreach($perms as $r)
             {
                 if ($r['isuser']) $selel = '"u'; else $selel = '"g';
                 $selel .= $r['id'] . '"';
                 if($sellist) $sellist .= ', ';
                 $sellist .= $selel;
             }
             if($selarr) $selarr .= ', ';
             $selarr .= 'new Array(' . $sellist . ')';
             if($allpages) $allpages .= ', ';

             $selarr2 = '';
             //delete page permissions
             $query = "SELECT * FROM permissions WHERE perm_num='13' and data='".$_GET['data']."' and data2='".$row['page_id']."'";
             $perms = sqlQueryData($query);

             $sellist2 = '';
             foreach($perms as $r)
             {
                 if ($r['isuser']) $selel = '"u'; else $selel = '"g';
                 $selel .= $r['id'] . '"';
                 if($sellist2) $sellist2 .= ', ';
                 $sellist2 .= $selel;
             }
             if($selarr2) $selarr2 .= ', ';
             $selarr2 .= 'new Array(' . $sellist2 . ')';
                  
             $allpages .= 'new Array('.$row['page_id'].', ' . $selarr . ', ' . $selarr2 . ' )';
             //echo 'new Array('.$row['page_id'].', ' . $selarr . ', ' . $selarr2 . ' )'."<br>";
        }   
        //GETTING PERMISSION LIST FOR PAGES


        Nodes($unsorteddata, $data_, 0, 0, 0, $g);
        //print_r($data_);

        require_once($GLOBALS['cfgDirRoot']."library/"."class.datagrid.php");

        $Table = new DataGrid();
        $Table->cols = $cols;
        $Table->data = $data_;
        $Table->footer = false;
        $Table->rowformat = " id=\"tablerow{%page_id%}\" onClick=\"javascript:HighLightTR(this, {%page_id%});\"";

        //echo '
        //<form style="padding:0px;margin:0px;" name="Form" method="POST" action="?module=permissions&action='.$action.'&applied=1&close='.$_GET['close'].'&site_id='.$site_id.'&data='.$data.'&data2='.$data2.'">
        //';        
        echo $Table->output();
        //######################## pages.default.php ##########################
    }





if ( (($_GET['action']=="site_form" || ($_GET['action']=="page_form") || ($_GET['action']=="template_form")) && $_GET['site_id']) || !$_GET['action'])
{
/*
echo '
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="formExFrame">';

if ($action!="page_form" && $action!="template_form") echo '<form name="Form" method="POST" action="?module=permissions&action='.$action.'&applied=1&close='.$_GET['close'].'&site_id='.$site_id.'&data='.$data.'&data2='.$data2.'">';

echo '
<tr><td class="formFrame">

<table width="100%" border="0" cellspacing="0" cellpadding="5" class="formInFrame">
<tr>
  <td width="100%" colspan="2" class="formTitle">
    Permissions
  </td>
</tr>

<tr>
  <td  width="30%" class="formLabel">Groups and Users:</td>
  <td  width="70%" class="formControl">
  <select class="formEdit" name="Select_1" size="1" onChange="FillFromArray();FillPagesFromArray();" style="width: 100%">
';
 foreach($groups as $group)
  {
    if(!$firstitem) echo ", ";
    $firstitem = false;
    $prefix = 'G:';
    if($group['site_id'])
      $prefix = 'G:site:';
    if ($group['group_id']==$_GET['gid']) $selected = "selected";
    else $selected = "";
    echo '<option value="g'.$group['group_id'].'" '.$selected.'>'.$prefix.$group['groupname'].'</option>';
  }
  foreach($users as $user)
  {
    if(!$firstitem) echo ", ";
    $firstitem = false;
    $prefix = 'U:';
    if($user['site_id'])
      $prefix = 'U:site:';
    if ($user['user_id']==$_GET['uid']) $selected = "selected"; 
    else $selected = "";
    echo '<option value="u'.$user['user_id'] . '" '.$selected.'>'.$prefix.$user['username'].'</option>';
  }
echo '
    </select>
  </td>
</tr>';
*/

//table begin & empty blue line
echo '
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="formExFrame">';

if (($action == "page_form" || $action == "template_form") && $_GET['data2'])
echo '
<tr><td class="formFrame">

<table width="100%" border="0" cellspacing="0" cellpadding="5" class="formInFrame">
<tr>
  <td width="100%" colspan="2" class="formTitle">
    &nbsp;
  </td>
</tr>';
//---------------


//PAGE AND TEMPLATE PERMISSIONS ONLY IF DATA2 IS SET 
if (($_GET['action']=='page_form' && $_GET['data2']) || ($_GET['action']=='template_form' && $_GET['data2']) || ($_GET['action']!="page_form" && $_GET['action']!="template_form"))
{

echo '
<tr>
  <td  width="30%" class="formLabel" valign="top">Permissions:</td>
  <td  width="70%" class="formControl">
';

  $selarr = '';
  $i = 0;
  $k = array_keys($permissions_list);
  //print_r($k);
  //Go through the resource list
  echo '<table cellspacing="0" cellpadding="0" width="100%">';
  foreach($k as $num)
  {
    if($permissions_list[$num]["type"] == $permission_type)
    {
      //Draw checkbox for resource
      if ($num!=11 && $num!=12 && $num!=13 && $num!=15)
            echo '<tr><td>'.$permissions_list[$num]["text"].'</td><td><input name="perm'.$i.'" type="checkbox" value="'.$num.'" onClick="javascript:ChangePerms('.$num.', '.$i.')"></td></tr>';
      $i++;
      //Obtain the list of users/groups who have access to this resource

      $query = "SELECT * FROM permissions WHERE perm_num='$num'";
      if($data)
        $query .= " AND data='$data'";
      if($data2)
        $query .= " AND data2='$data2'";
      $perms = sqlQueryData($query);

      $sellist = '';
      foreach($perms as $row)
      {
        if($row['isuser']) $selel = '"u'; else $selel = '"g';
        $selel .= $row['id'] . '"';
        if($sellist) $sellist .= ', ';
        $sellist .= $selel;
      }
      if($selarr) $selarr .= ', ';
      $selarr .= 'new Array(' . $sellist . ')';
    }
  }
  //echo $selarr;
  echo '</table>';
  


 if($_GET['data']){ 
    echo '<input type="hidden" name="data" value="'.$_GET['data'].'">';
 }
 if($_GET['data2']) { 
    echo '<input type="hidden" name="data2" value="'.$_GET['data2'].'">';
 }
 echo '
    <input type="hidden" name="permchanges" value="">
  </td>
</tr>

<tr><td colspan="2">
     '; 
echo OutputPermProc($permission_type, $site_id, $data, $data2); 
echo '
    </td><td></td></tr>
     ';
} //END OF PAGE AND TEMPLATE PERMISSIONS ONLY IF DATA2 IS SET
echo '
</table>

<table width="100%" border="0" cellpadding="4" cellspacing="4">
<tr><td>&nbsp;</td></tr>
<tr>
  <td align="right">
  <input type="hidden" name="pagepermchanges" value="">
    ';
    if(($perm_manageperms)or($perm_managesiteperms)) { 
        echo '
            <input class="formButton" type="submit" name="submitPermChanges" value="&nbsp;&nbsp;OK&nbsp;&nbsp;">
            ';
    }
    echo '
  <input class="formButton" type="button" value="Cancel" OnClick="window.close();">
  </td>
</tr>
</table>
        ';
}

echo '
</td></tr>
</form></table>

    </td>
</tr>
</table>
<script language="JavaScript">
<!--
var SelPerms = new Array('.$selarr.');
var AllPagesSelPerms = new Array('.$allpages.');
var PermNum = '; if (!isset($i)) $i=0; echo $i--;
echo ';
FillFromArray();
FillPagesFromArray();
<!---->
</script>
';
