<?

$GLOBALS['tabs'] = 'site';
$site_id = $_GET['site_id'];

function DisplayToolbar()
{
  global $docStrings;
  /*require($GLOBALS['cfgDirRoot']."library/"."class.toolbar.php");
  $Toolbar = new Toolbar();
  $Toolbar->AddLink("log", "View Log", "view_log.gif", "javascript:window.location='?module=admin&action=log&site_id=".$_GET['site_id']."'", "View activity log");
  $Toolbar->AddLink("backup", "Backup Site", "backup_db.gif", "javascript:window.location='?module=admin&site_id=".$_GET['site_id']."&action=database'", "Backup database");
  $Toolbar->AddLink("restore", "Restore Site", "restore_db.gif", "javascript:window.location='?module=admin&site_id=".$_GET['site_id']."&action=browsebackups'", "Restore database from backup");
  $Toolbar->AddButton("reindex", "Index Pages For Search", "index_pages.gif", "javascript:openDialog('?module=admin&action=reindex&close=0&site_id=".$_GET['site_id']."&page_id=0', 400, 100);", "Index all pages for search");
  //$Toolbar->AddLink("components", "Add/Remove Components", "components.gif", "javascript:window.location='?module=admin&action=components'", "Install and uninstall components");
  $Toolbar->AddButton("permissions", "Access Permissions", "edit_permissions.gif", "javascript:openDialog('?module=permissions&action=permission_frame&mod=site_form&hideitems=1&close=1&site_id=".$_GET['site_id']."&data=".$_GET['site_id']."&data2=', 800, 600, 0, 1);", "Edit global access permissions");
  //$Toolbar->AddLink("statistics", "Site Statistics", "page_statistics.gif", "javascript:window.location='?module=admin&site_id=".$_GET['site_id']."&action=stats'", "Stats");
  $docStrings['toolbar'] = $Toolbar->output();*/
  require($GLOBALS['cfgDirRoot']."library/"."class.toolbar2.php");
  $Toolbar = new Toolbar2();

  $Toolbar->AddSpacer();

  $Toolbar->AddButtonImage("log", 'admin_log', "{%langstr:toolbar_view_log%}", "javascript:window.location='?module=admin&action=log&site_id=".$_GET['site_id']."'", "", 31, "{%langstr:toolbar_view_activity_log%}");
//  $Toolbar->AddButtonImage("backup", 'admin_backup', "{%langstr:toolbar_backup_site%}", "javascript:window.location='?module=admin&site_id=".$_GET['site_id']."&action=database'", '', 31, "{%langstr:toolbar_backup_database%}");
//  $Toolbar->AddButtonImage("restore", 'admin_restore', "{%langstr:toolbar_restore_site%}", "javascript:window.location='?module=admin&site_id=".$_GET['site_id']."&action=browsebackups'", '', 31, "{%langstr:toolbar_restore_database%}");
  $Toolbar->AddButtonImage("reindex", 'admin_index', "{%langstr:toolbar_index_pages_search%}", "", "javascript:openDialog('?module=admin&action=reindex&close=0&site_id=".$_GET['site_id']."&page_id=0', 400, 100);", 31, "{%langstr:toolbar_index_all_pages_search%}");
  $Toolbar->AddButtonImage("permissions", 'site_permissions', "{%langstr:toolbar_access_permissions%}", "", "javascript:openDialog('?module=permissions&action=permission_frame&mod=site_form&hideitems=1&close=1&site_id=".$_GET['site_id']."&data=".$_GET['site_id']."&data2=', 800, 600, 0, 1);", 31, "{%langstr:toolbar_edit_global_permissions%}");

  $Toolbar->AddSeperator();
  $Toolbar->AddButtonImage("help", 'help', "{%langstr:toolbar_help%}", $GLOBALS['cfgWebRoot']."help/help.htm?category=administracija", "", 31, "","","_blank");

  $docStrings['toolbar'] =  $Toolbar->output();

}

function DisplayBrowseBackupsToolbar()
{
  global $docStrings;
  require($GLOBALS['cfgDirRoot']."library/"."class.toolbar2.php");

  $site_id = $_GET['site_id'];
  $cfgBackupRoot = $GLOBALS['cfgDirRoot'] . "backup/site".$site_id."/";

  $Toolbar = new Toolbar2();
  $Toolbar->AddSpacer();
  $Toolbar->AddButtonImage("upload", 'files_upload', "{%langstr:toolbar_upload_backup%}", "", "openDialog('?module=files&site_id=".$GLOBALS['site_id']."&backup=1&path=".$GLOBALS['path']."&action=upload', 500, 300);", 31, "{%langstr:toolbar_upload_backup_from_file%}");
  $Toolbar->AddSeperator();
  $docStrings['toolbar'] = $Toolbar->output();
}


function DisplayLangstringsToolbar()
{
  global $docStrings;
  require($GLOBALS['cfgDirRoot']."library/"."class.toolbar2.php");

  $site_id = $_GET['site_id'];
  $cfgBackupRoot = $GLOBALS['cfgDirRoot'] . "backup/site".$site_id."/";

  $Toolbar = new Toolbar2();
  $Toolbar->AddSpacer();
  //$Toolbar->AddButtonImage("upload", 'files_upload', "Upload backup", "", "openDialog('?module=files&site_id=".$GLOBALS['site_id']."&backup=1&path=".$GLOBALS['path']."&action=upload', 500, 300);", 31, "Upload backup from file");
  $Toolbar->AddSeperator();
  $docStrings['toolbar'] = $Toolbar->output();
}


/*if($GLOBALS['currentUserSiteID'])
{
  Header("Location: index.php?site_id=" . $GLOBALS['currentUserSiteID'] . "&module=" . defaultSiteModule);
  Die;
} */


$action = $_GET['action'];
$perm_accessadmin = CheckPermission(18, $site_id);

switch ($action) {

  case "dummy":
    $docTemplate = "dlg_form.htm";
    $docTitle = "Test Form";
    $docStrings['frame'] = "?module=test&action=form_content";
    break;

  case "components":
    if($perm_accessadmin)
    {
      DisplayToolbar();
    }
    $docTemplate = "main.htm";
    $docTitle = "Components";
    $docScripts['body'] = "admin.components.php";
    break;

  case "delcomponent":
    $docScripts['exec'] = "admin.exec.php";
    Header("Location: ?module=admin&action=components");
    break;

  case "log":

    if(($perm_accessadmin)and($_GET['site_id']))
    {
      $GLOBALS['site_id'] = $_GET['site_id'];
      $docTemplate = "main.htm";
      $docTitle = "System Log";
      $docScripts['body'] = "admin.log.php";

      DisplayToolbar();
    }else
    {
      AccessDenied(False, True);
      echo '
      <script language="JavaScript" type="text/javascript">
      /*<![CDATA[*/
      window.document.location.href = "?module=admin&site_id=' . $GLOBALS['site_id']. '"
      /*]]>*/
      </script>
      ';
    }
    break;

  case "database":
    if($perm_accessadmin)
    {
      DisplayToolbar();
    }
    $docTemplate = "main.htm";
    $docTitle = "Backup/Restore Database";
    $docScripts['body'] = "admin.backup.php";
    break;
    
  case "uploadcomponent":
    $docTitle = "Add Component";
    $docTemplate = "form.htm";
    $GLOBALS['resizeauto'] = 1;
    $docScripts['body'] = "admin.uploadcomponent.php";
    break;

  case "reindex":
    $docTitle = "Indexing pages";
    $docTemplate = "nothing.htm";
    $GLOBALS['resizeauto'] = 1;
    $docScripts['body'] = "admin.indexforsearch.php";
    break;

  case "savecomponent":
    $docScripts['exec'] = "admin.savecomponent.php";
    /*if ($GLOBALS['close'] == 'no') {
      $docTemplate = "form.htm";
      $GLOBALS['resizeauto'] = 1;
      $docScripts['body'] = "admin.components.php";
    } else {
      $docTemplate = "close.htm";
    }*/
    break;

  case "backupdatabase":
    $docScripts['exec'] = "admin.exec.php";
    Header("Location: ?module=admin&site_id=".$_GET['site_id']."&action=browsebackups");
    break;

  case "restorebackup":
    $docScripts['exec'] = "admin.exec.php";
    Header("Location: ?module=admin&site_id=".$_GET['site_id']."&action=browsebackups");
    break;

  case "ask_restore":
    $docTemplate = "nothing.htm";
    $docScripts['body'] = "admin.askrestorebackup.php";
    $docTitle = "Restore Site";
    break;

  case "download_backup":
    $docTemplate = "nothing.htm";
    $docScripts['body'] = "admin.downloadbackup.php";
    $docTitle = "Restore Site";
    break;


  case "deletebackup":
    $docScripts['exec'] = "admin.exec.php";
    Header("Location: ?module=admin&site_id=".$_GET['site_id']."&action=browsebackups");
    break;
  case "langstrings":
    if($perm_accessadmin)
    {
      DisplayLangStringsToolbar();
      $docTemplate = "main.htm";
      $docTitle = "Language Phrase Manager";
      $docScripts['body'] = "admin.langstrings.php";
      break;
    }
  case "browsebackups":
    if($perm_accessadmin)
    {
      DisplayBrowseBackupsToolbar();
      $docTemplate = "main.htm";
      $docTitle = "Browse/Restore Database";
      $docScripts['body'] = "admin.browsebackups.php";
      break;
    }
  case "stats":
    if($perm_accessadmin)
    {
      DisplayToolbar();
    }
    $docTemplate = "main.htm";
    $docTitle = "Site Statistics";
    $docScripts['body'] = "admin.viewstats.php";
    break;

  case "user_stats":
    $docTemplate = "nothing.htm";
    $docScripts['body'] = "admin.userstats.php";
    $docTitle = "User statistics";
    break;

  case "graph":
    $docTemplate = "blank.html";
    $docScripts['body'] = "admin.graph.php";
    $docTitle = "Restore Site";
    break;


    case "phrase_create":
    $docTemplate = "form.htm";
    $GLOBALS['resizeauto'] = 1;
    $docTitle = "Create Phrase";
    $docScripts['body'] = "admin.phraseproperties.php";
    break;

  // site properties form
  case "phrase_properties_form":
    $docTemplate = "form.htm";
    $GLOBALS['resizeauto'] = 1;
    $docTitle = "Phrase Properties";
    $docScripts['body'] = "admin.phraseproperties.php";
    break;

  case "modphrase":
  case "addphrase":
    $docTemplate = "close.htm";
    $docScripts['exec'] = "admin.phraseexec.php";
    break;

  case "delphrase":
    $docScripts['exec'] = "admin.phraseexec.php";
    Header("Location: ?module=admin&action=langstrings&site_id=" . $site_id);
    break;
    
  case "backup_langstrings":
    $docScripts['exec'] = "admin.exec.php";
    Header("Location: ?module=admin&site_id=".$_GET['site_id']."&action=browslangstrebackups");
    break;
  case 'browslangstrebackups':
    if($perm_accessadmin)
    {
      //DisplayBrowseBackupsToolbar();
      $docTemplate = "main.htm";
      $docTitle = "Browse/Restore Database";
      $docScripts['body'] = "admin.browselangstrbackups.php";
      break;
    }
  case "deletelangstr":
    $docScripts['exec'] = "admin.exec.php";
    Header("Location: ?module=admin&site_id=".$_GET['site_id']."&action=browslangstrebackups");
    break;
  case "restorelangstr":
    $docScripts['exec'] = "admin.exec.php";
    Header("Location: ?module=admin&site_id=".$_GET['site_id']."&action=browslangstrebackups");
    break;

  case "ask_restorelangstr":
    $docTemplate = "nothing.htm";
    $docScripts['body'] = "admin.ask_restorelangstr.php";
    $docTitle = "Restore Site";
    break;

  default:

    if($perm_accessadmin)
    {
      DisplayToolbar();
    }

    $docTemplate = "main.htm";
    $docTitle = "Administration";
    $docScripts['body'] = "admin.default.php";
    break;
}

?>
