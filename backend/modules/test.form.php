<?

// Define form

$form_data = Array(

  "id"          => Array(
    "label"     => "Hidden:",
    "type"      => "hidden"
  ),

  "list"   => Array(
    "label"     => "List Box:",
    "type"      => "list",
    "lookup"    => Array('0:item 1', '1:item 2'),
    "value"     => 0
  ),

  "ind"       => Array(
    "noshow"    => true
  ),

  "str1"        => Array(
    "label"     => "String:",
    "type"      => "str",
    "maxlength" => "20"
  ),

  "str2"       => Array(
    "label"     => "Another String:",
    "type"      => "str_dlg",
    "dialog"    => "?module=test&action=modal",
    "size"      => "30",
    "maxlength" => "100"
  ),

  "str3"       => Array(
    "label"     => "Another String:",
    "type"      => "str",
    "maxlength" => "100"
  ),

  "html" => Array(
    "label"     => "HTML Text:",
    "type"      => "html",
    "rows"      => "4",
    "cols"      => "30"
  ),

  "check"      => Array(
    "label"     => "Checkbox:",
    "type"      => "check",
    "value"     => "0"
  ),

  "check2"     => Array(
    "label"     => "Other Checkbox:",
    "type"      => "check",
    "value"     => "1"
  ),

  "list2" => Array(
    "label"     => "List Box:",
    "type"      => "list"
  ),

  "nothing" => Array(
    "noshow"    => true
  ),

  "date"    => Array(
    "label"     => "Date:",
    "type"      => "date"
  )

);

// Fill some values

  $form_data['str2']['value'] = 'Could read this from DB';
  $form_data['str1']['value'] = 'Invalid input';
  $form_data['str1']['error'] = 1;

// Use FormGen to bring this out

  require($GLOBALS['cfgDirRoot']."library/"."class.formgen.php");
  $FormGen = new FormGen();
  $FormGen->action = "?module=".$GLOBALS['module']."&action=close";
  $FormGen->cancel = "close";
  //$FormGen->title = "Test";
  $FormGen->properties = $form_data;
  $FormGen->buttons = false;
  echo $FormGen->output();

?>
