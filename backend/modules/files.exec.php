<?

$perm_managefiles = $GLOBALS['perm_managefiles'];

$directory = sqlQueryValue("SELECT dirroot FROM sites WHERE site_id = '".intval($GLOBALS['site_id'])."'");
$path = str_replace("..", "", $GLOBALS['path']);
if ($path == "/") $path = "";
if ($_GET['backup']!="") $directory = $GLOBALS['cfgDirRoot']."backup/site".$GLOBALS['site_id']."/";
$directory = fileAddBackslash($directory).$path;


function SaveFile($directory,$filename2="") {
  require_once($GLOBALS['cfgDirRoot']."library/"."class.fileupload.php");
  $UPLOADPATH = fileAddBackslash($directory);
  $FILENAME = "userfile";
  if ($filename2) $FILENAME = $filename2;
  $EXTENSION = "";

  if (isset($_POST['over'])) $SAVE_MODE = $_POST['over'];
  else if (isset($_GET['over'])) $SAVE_MODE = $_GET['over'];
  else $SAVE_MODE = 3;

  $upload = new uploader;
  $upload->max_filesize(100000000);
  $check = $upload->upload($FILENAME, "", $EXTENSION);

	if($check) {
    if (preg_match("/".$upload->file["extention"]."/i", '.php5 .htaccess .php .phtml .cgi .pl')) {
      $upload->file["extention"] = ".dat";
      $upload->file["name"] = $upload->file["name"] . ".dat";
    }
    if($upload->save_file($UPLOADPATH, $SAVE_MODE)) {
      Add_Log("Uploaded file '" . $upload->file["name"], $GLOBALS['site_id']);
    }
  }
  foreach($upload->errors as $error) $errors .= $error."<br>";
  $filename = $upload->saved_filename;
  unset($upload);
  return $filename;
}


switch ($GLOBALS["action"]) {

  case "save":
    if($perm_managefiles)
    {
/*      for ($i=1; $i<=5; $i++)
        {
            if ($_FILES['userfile'.$i]['name'])
            {
              $filename = SaveFile($directory,'userfile'.$i);
              if (empty($first_file_name))
              {
                $first_file_name = $filename;
              }
            }
        } */


        foreach ($_FILES as $key => $f)
        {
          if (!$f['name']) continue;

          if($GLOBALS['cfgMini'] && $GLOBALS['cfgMiniDemo'])
          {
            $allow_save = false;
            if(preg_match('/\.([^.]+)$/', $f['name'], $matches))
            {
              $ext = strtolower($matches[1]);
              if(in_array($ext, Array('jpg', 'swf', 'jpeg', 'txt', 'gif', 'png', 'css', 'avi', 'wmv', 'divx', 'mpg', 'mpeg', 'mp3', 'flv')))
              {
                $allow_save = true;
              }
              else
              {
                 send_mail('Constructor Demo', 'constructor@datateks.lv',
                   'Constructor', 'constructor@datateks.lv',
                   'utf-8', 'Slikts megijinajums failu uploadeet!',
                   'IP ' . $_SERVER['REMOTE_ADDR'] . ', Name ' . $f['name']);
              }
            }
          }else
            $allow_save = true;

          if($allow_save)
          {
            $filename = SaveFile($directory, $key);
            if (empty($first_file_name))
            {
              $first_file_name = $filename;
            }
          }else
          {
            die('ERROR: Demo versijaa atlauts augshupieladet tikai attelus un teksta failus');
          }
        }

      ?>
      <script>
        try {

          parent.document.Form.Input.value = '<?php echo '/'.parseForJScript($_GET['path'].'/'.$first_file_name); ?>';
        } catch (e) {}
      </script>
      <?php

    }
    break;

  case "savecontents":
        $dirroot = sqlQueryValue("SELECT dirroot FROM sites WHERE site_id=".$_GET['site_id']);
        $path = $_GET['path'];
        $file = $_GET['file'];
        $contents = stripslashes($_POST['contents']);
        if (is_file($dirroot."/".$path."/".$file))
        {
            //$contents = file_get_contents($dirroot."/".$path."/".$file);

        }

        $file_error = false;
        // Let's make sure the file exists and is writable first.
        if (is_writable($dirroot."/".$path."/".$file)) {
           if (!$handle = fopen($dirroot."/".$path."/".$file, 'w')) {
                 $file_error = true;
           }
           // Write $content to our opened file.
           if (fwrite($handle, $contents) === FALSE) {
               $file_error = true;
           }
           fclose($handle);

        } else {
           $file_error = true;
        }
      break;

  case "delete":

    function files_exec_mergeDirectories($src, $dest)
    {
      foreach (glob($src.'/*') as $f)
      {
        $name = basename($f);
        $target = $dest.'/'.$name;


        if (file_exists($target))
        {
          if (is_dir($target))
          {
            if (is_file($f))
            {
              recursive_remove_directory($target);

              if (!rename($f, $target))
                return false;

            }
          }
          else
          {
            @unlink($target);

            if (!rename($f, $target))
              return false;

          }
        }
        else
        {
          if (!rename($f, $target))
            return false;

        }

        if (is_dir($f) && is_dir($target))
        {
          files_exec_mergeDirectories($f, $target);
          rmdir($f);
        }

      }

    }

    if($perm_managefiles){

    	foreach(explode(";", $_GET['file']) as $f){

	      $p = explode('/', $path);
	      $webpath = $path.'/'.$f;
	      $disallow = array('/files', '/images', '/scr', '/trash', '/upload');

	      if (!in_array($webpath, $disallow)){
	        if ($p[0] == 'trash'){
	          DeleteFile($directory, $path);
	        }else{

	          $file = $directory.'/'.$f;
	          $trashfile = GetFilePathFromLink($GLOBALS['site_id'], '/trash/'.$path.'/'.$f);
	          if (is_file($trashfile))
	            @unlink($trashfile);

	          forceDirectories($GLOBALS['site_id'], 'trash/'.$path);

	          $rename = true;
	          if (is_dir($trashfile)){
	            if (is_dir($file)){
	              files_exec_mergeDirectories($file, $trashfile);
	              rmdir($file);
	              $rename = false;
	            }else if (is_file($file)){
	              recursive_remove_directory($trashfile);
	            }
	          }

	          if ($rename && rename($file, $trashfile)){
	            $fname = fileAddBackslash($directory).$f;
	            Add_Log("Trashed file '$fname'", $GLOBALS['site_id']);
	          }

	        }
	      }

      }

    }

		$get = array();
    $get['module'] = $_GET['module'];
    $get['path'] = $_GET['path'];
    $get['site_id'] = $_GET['site_id'];

    header("location: ?".http_build_query($get));
		die();

    break;

  case "dorename":
    $i=0;

    while ($_POST['oldfile'.$i]){

      if(($_POST['newfile'.$i] != '.htaccess') AND ($_POST['newfile'.$i] != '.gitignore') AND $perm_managefiles && !in_array(getFileExtension($_POST['newfile'.$i]), array(".php", ".php5", ".exe", ".sh", ".pl", ".py", ".phtml", ".cgi"))){
        RenameFile($directory,$_POST['oldfile'.$i], $_POST['newfile'.$i]);
      }else{
        $jsmessage = 'Unable to rename file ' . $_POST['oldfile'.$i] . ' to ' . $_POST['newfile'.$i];
        Redirect('?module='.$module.'&action=rename&invalid=newfile'.$i . '&path='.$GLOBALS['path'] . '&view='.$GLOBALS['view'] . '&name='.$_GET['name'].'&close='.$GLOBALS['close'].'&jsmessage='.urlencode($jsmessage).'&site_id='.$site_id);
        die();
      }
      $i++;
    }
    break;

  case "docopy":
      //echo $directory;
        
      if ($_GET['file'] != "" ) {
        $files = explode(";",$_GET['file']);
        $dir = $_GET['over'];
        if ($dir=="/") $dir = "";
        //echo fileAddBackslash($directory).$file. "      ". $rootdir.$dir."/".$file. "   ".$_GET['move'];
        $rootdir = sqlQueryValue("SELECT dirroot FROM sites WHERE site_id = '".intval($GLOBALS['site_id'])."'");
        foreach ($files as $key=>$file){
                    
					if($perm_managefiles){
          	$to = $rootdir.$dir.($dir? "/" : "").$file;
            $from = fileAddBackslash($directory).$file;

            if ($_GET['move']==1)
							MoveFile($from, $to);
            else
							CopyFile(fileAddBackslash($directory).$file, $to);

              chmod($to, 0644);
          }
        }
      }
    break;

  case "mkdir":
    if($perm_managefiles)
      $nm = fileAddBackslash($directory).$_POST['name'];
      mkdir($nm, 0755);
      chmod($nm, 0755);
      Add_Log("New directory '" . fileAddBackslash($directory).$_POST['name'] . "'", $GLOBALS['site_id']);
    break;

  case "savekeywordcat":
    sqlQuery("INSERT INTO " . $_GET['site_id'] . "_file_keywords (word, category_id) VALUES ('".$_POST['title']."', 0)");
    break;

  case "savekeyword":
    sqlQuery("INSERT INTO " . $_GET['site_id'] . "_file_keywords (word, category_id) VALUES ('".$_POST['title']."', '".$_POST['category']."')");
    break;

  case "dodelkeyword":
    //see if this is category or just keyword
    $cat = sqlQueryValue("SELECT keyword_id FROM " . $_GET['site_id'] . "_file_keywords  WHERE category_id=0 AND keyword_id=".$_POST['category']);
    if($cat)
    {
      sqlQuery("DELETE FROM " . $_GET['site_id'] . "_file_keywords WHERE category_id=".$cat);
      sqlQuery("DELETE FROM " . $_GET['site_id'] . "_filesearch_keywordlink WHERE keyword_id = ".$cat);
    }
    sqlQuery("DELETE FROM " . $_GET['site_id'] . "_file_keywords WHERE keyword_id=".$_POST['category']);
    sqlQuery("DELETE FROM " . $_GET['site_id'] . "_filesearch_keywordlink WHERE keyword_id = ".$_POST['category']);
    break;
  case "setfilekeywords":
    if($_GET['file'] != '')
    {
      //remove old keywords for this file
      sqlQuery("DELETE FROM " . $_GET['site_id'] . "_filesearch_keywordlink WHERE file_path = '".$_GET['file']."'");
      //add the new ones
      foreach($_POST as $key => $p)
      {
        if(($p)and(substr($key, 0, 4) == 'chk_'))
        {
          $id = intval(substr($key, 4));
          if($id)
          {
            sqlQuery("INSERT INTO " . $_GET['site_id'] . "_filesearch_keywordlink (keyword_id, file_path) VALUES ($id, '" . $_GET['file'] . "')");
          }
        }
      }
    }
    break;

  case 'trash_restore':
    $p = explode('/', $path);
    $webpath = $path.'/'.$_GET['file'];

    if ($p[0] == 'trash')
    {
      array_shift($p);
      $destpath = implode('/', $p);
      $trashfile = $directory.'/'.$_GET['file'];
      $file = GetFilePathFromLink($GLOBALS['site_id'], $destpath.'/'.$_GET['file']);

      if (!file_exists($file))
      {
        forceDirectories($GLOBALS['site_id'], $destpath);

        if (rename($trashfile, $file))
        {
          $fname = fileAddBackslash($directory).$_GET['file'];
          Add_Log("Restored file from trash: '".$destpath.'/'.$_GET['file']."'", $GLOBALS['site_id']);
        }
      }


    }

    header('Location: ?module=files&site_id=' . $GLOBALS['site_id']);
    break;
}

