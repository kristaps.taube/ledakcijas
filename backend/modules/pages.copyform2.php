<?

// Define form
$site_id = $GLOBALS['site_id'];
if (isset($_GET['id']))
{
    $pagedev_id = preg_split("/[\s,]+/", $_GET['id'],-1,PREG_SPLIT_NO_EMPTY);
    foreach ($pagedev_id as $pid){
      $row = sqlQueryRow("SELECT * FROM ".$site_id."_pagesdev WHERE pagedev_id=$pid");
      $page_id[] = $row['page_id'];
      $title[] = $row['title'];
  }
}


function getTree($page_id, &$data)
{
  foreach($page_id as $pid)
  {
    if($pid)
    {
      $childs = sqlQueryData("select page_id from ".$_GET['site_id']."_pages where parent=$pid");
      foreach ($childs as $child)
      {
        $data[] = $child['page_id'];
        getTree(array($child['page_id']), &$data);
      }
    }
  }
}
//get array of subpages for current page
$data = $page_id;
getTree($page_id, $data);

function page_level($page_id, $site_id)
{
  $l = 0;
  while($page_id)
  {
    $l++;
    $page_id = sqlQueryValue("SELECT parent FROM " . $site_id . "_pages WHERE page_id=$page_id");
  }
  return $l;
}


$pages = sqlQueryData("SELECT page_id,name,parent,title FROM " . $site_id . "_pages ORDER BY ind");

ListNodes($pages, $combopages, 0, 0);


foreach ($combopages as $key=>$row)
{
    $p = explode(":",$row);
    if (in_array($p[0],$data))
        unset($combopages[$key]);
}

$form_data = Array(

  "from"      => Array(
    "type"      => "hidden"
  ),

/*  "fromname"      => Array(
    "label"     => "{%langstr:pages_from%}:",
    "disabled"  => true,
    "value"     => "",
    "type"      => "str"
  ),*/

  "to"      => Array(
    "label"     => "{%langstr:pages_to%}:",
    "type"      => "list",
    "lookup"    => $combopages
  ),

);

// Fill some values
  $action = $_GET['action'];
  $create = ($action == 'create');

  $form_data['from']['value'] = implode(",",$page_id);
//  $form_data['fromname']['value'] = $title;

  $invalid = split(':', $_GET['invalid']);
  foreach($invalid as $i)
  {
    if($i)
      $form_data[$i]['error'] = true;
  }

/*
  foreach(array_keys($_GET) as $key)
  {
    $s = Split('_', $key, 2);
    if($s[0] == 'formdata')
      $form_data[$s[1]]['value'] = stripslashes($_GET[$key]);
  }
*/
  if($_GET['jsmessage'])
  {
    echo '<script language="JavaScript">
          <!--
          alert("' . stripslashes($_GET['jsmessage']) . '");
          <!---->
          </script>';
  }



// Use FormGen to bring this out

  require($GLOBALS['cfgDirRoot']."library/"."class.formgen.php");
  $FormGen = new FormGen();

  $FormGen->action = "?module=".$GLOBALS['module']."&site_id=".$site_id."&action=docopy2";

  $FormGen->cancel = "close";
  $FormGen->title = "{%langstr:copy_tree%}";
  
  $FormGen->properties = $form_data;
  $FormGen->buttons = true;
  echo $FormGen->output();

?>
