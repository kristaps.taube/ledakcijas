<?

  $site_id = $GLOBALS['site_id'];
  $template_id = $_GET["template_id"];
  $GLOBALS['template_id'] = $_GET['template_id'];
  $GLOBALS['page_id'] = -1;


  $site_id = $GLOBALS['site_id'];

  $sitedata = sqlQueryRow("SELECT domain, wwwroot FROM sites WHERE site_id='".$site_id."'");
  $base = 'http://' . $sitedata['domain'] . $sitedata['wwwroot'];

  $language = $GLOBALS['currentlanguagenum'];
  if (!$language) 
  {
      $row = sqlQueryRow("SELECT language_id, shortname, fullname, encoding FROM " . $site_id . "_languages WHERE is_default = 1");
  }
  else
  {
      $row = sqlQueryRow("SELECT language_id, shortname, fullname, encoding FROM " . $site_id . "_languages WHERE language_id = ".$language);
  }
  $shortname = $row['shortname'];
  $fullname = $row['fullname'];
  $encoding = $row['encoding'];

echo '
function checkForUnsavedComponents()
{

  head_2 = document.getElementsByTagName("head").item(0);

  if((window.parent.UnsavedComponents)&&(window.parent.UnsavedComponents.length))
  {
    if(confirm("Do you want to save modifications?"))
    {
      SaveComponents(true);
    }
  }
}

function onLoadActions()
{
    document.body.contentEditable = true;
    
    //save original head html code
    head = document.getElementsByTagName("head").item(0);

    window.elements = Array();
    var n = 0;
    
    for (i=0; i<=(document.all.length-1); i++)
    {
        //try {
        //if (document.all[i].tagName!="BODY" && document.all[i].tagName!="HTML")
        //{
        //    if (document.all[i].outerHTML) document.all[i].origCode = document.all[i].outerHTML;
        //}
        //catch (e) {}
        var tag = document.all[i].tagName;
        //get content of the tag
        var inside = document.all[i].outerHTML.match(/^<(.*)>/i);
        if (inside && (inside[1].match(/{%language:shortname%}/) || inside[1].match(/{%language:fullname%}/) || inside[1].match(/{%language:encoding%}/) || inside[1].match(/{%var:site_id%}/) || inside[1].match(/{%var:template_id%}/)))
        {
            var insideold = inside[1];

            inside[1] = inside[1].replace(/{%language:shortname%}/, "'.$shortname.'");
            inside[1] = inside[1].replace(/{%language:fullname%}/, "'.$fullname.'");
            inside[1] = inside[1].replace(/{%language:encoding%}/, "'.$encoding.'");
            inside[1] = inside[1].replace(/{%var:site_id%}/, "'.$site_id.'");
            inside[1] = inside[1].replace(/{%var:template_id%}/, "'.$template_id.'");
            try {
                document.all[i].outerHTML = document.all[i].outerHTML.replace(insideold, inside[1]);
                window.elements[n] = new Array(document.all[i], insideold, inside[1]);
                n++;
            }
            catch (e) {}
        }
    }
    //alert(window.elements);

    //strip base from links
    for(f=0; f<window.document.all.length;f++)
    {
      if(window.document.all[f].tagName.toLowerCase() == "img")
      {
        if(window.document.all[f].src.indexOf("'.$base.'") == 0)
        {
          window.document.all[f].src = window.document.all[f].src.substring("'.$base.'".length - 1);
        }
      }
      if(window.document.all[f].tagName.toLowerCase() == "a")
      {
         if(window.document.all[f].href.indexOf("'.$base.'") == 0)
         {
           window.document.all[f].href = window.document.all[f].href.substring("'.$base.'".length - 1);
         }
      }
    }

}

//try { 
    document.body.onload = onLoadActions; 
    document.body.onunload = checkForUnsavedComponents;
//} catch (e) {}

';

?>