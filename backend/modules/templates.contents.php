<?
$perm_managecontents = $GLOBALS['perm_managecontents'];
if(!$perm_managecontents)
  AccessDenied(True);


// Fill some values
  $id = intval($_GET["id"]);
  $action = $_GET['action'];
  $site_id = $GLOBALS['site_id'];



// Define form
$templates = sqlQueryData("SELECT * FROM " . $site_id . "_templates WHERE template_id <> $id");
$templatescombo = Array();
$templatescombo[] = "0:Use contents below";
if($templates)
{
  foreach($templates AS $row)
  {
    $templatescombo[] = $row['template_id'] . ":Template " . $row['name'];
  }
}

$form_data = Array(

  "template_id" => Array(
    "label"     => "ID:",
    "type"      => "hidden"
  ),

  "ind"         => Array(
    "noshow"    => true
  ),

  "name"        => Array(
    "label"     => "Name:",
    "type"      => "str"
  ),

  "description"  => Array(
    "label"     => "Description:",
    "type"      => "str"
  ),

/*  "copybody"    => Array(
    "label"     => "Use contents from:",
    "type"      => "list",
    "lookup"    => $templatescombo
  ),    */


  "toolbar"     => Array(
    "label"     => "Components:",
    "type"      => "code"
  ),

  "bodytext"        => Array(
    "label"     => "Contents:",
    "wrap"      => "off",
    "type"      => "html",
    "rows"      => "23",
    "cols"      => "85",
    "custom"    => ' onselect="storeCaret(this);" onclick="storeCaret(this);" onkeyup="storeCaret(this);" onchange="storeCaret(this);"'
  )

);


  $row = sqlQueryRow("SELECT * FROM " . $site_id . "_templates WHERE template_id=$id");
  $form_data['template_id']['value'] = $id;
  $form_data['name']['value'] = $row['name'];
  $form_data['bodytext']['value'] = $row['body'];
  $form_data['description']['value'] = $row['description'];

  $components = sqlQueryColumn("SELECT type FROM components WHERE wap=0 AND enabled=1 ORDER BY type");

  $toolbar .= "\n".'<select name="combocomponents" size="1">'."\n";
  foreach($components as $componenttype)
  {
    $toolbar .= '  <option value="' . $componenttype . '" selected="selected">' . $componenttype . '</option>'."\n";
  }
  $toolbar .= '</select>';
  $toolbar .= '<input type="button" name="cmdaddcomponent" style="width:auto;" value="{%langstr:add_coponent%}" onClick="javascript:defval=FindAvailableComponentName(document.Form.bodytext.value, combocomponents.value, ' .$id . ');var s=prompt(\'{%langstr:please_enter_component_name%}:\',defval);if(s)AddTextEditArea(\'{%component:\'+combocomponents.value+\':\'+s+\'%}\');">'."\n";

  $toolbar .= '<script language="JavaScript" type="text/javascript">
                /*<![CDATA[*/
                elems = new Array("{%title%}", "{%var:site_id%}", "{%var:page_id%}", "{%var:template_id%}",
                                   "{%language:shortname%}", "{%language:fullname%}", "{%language:encoding%}",
                                   "{%url:host%}", "{%url:path%}", "{%url:query%}", "{%var:name%}", "{%var:sitedomain%}", "{%var:sessionid%}");
                /*]]>*/
                </script>';
  $toolbar .= ' <select name="comboelements" size="1">
                <option value="0">{%langstr:page_title%}</option>
                <option value="10">{%langstr:page_name%}</option>
                <option value="1">{%langstr:site_id%}</option>
                <option value="2">{%langstr:page_id%}</option>
                <option value="3">{%langstr:template_id%}</option>
                <option value="4">{%langstr:language_short_name%}</option>
                <option value="5">{%langstr:language_full_name%}</option>
                <option value="6">{%langstr:language_encoding%}</option>
                <option value="7">{%langstr:url_hostname%}</option>
                <option value="8">{%langstr:url_pathname%}</option>
                <option value="9">{%langstr:url_query%}</option>
                <option value="11">{%langstr:site_domain%}</option>
                <option value="12">{%langstr:session_id%}</option>
                </select>';
  $toolbar .= '<input type="button" name="cmdaddelement" style="width:auto;" value="{%langstr:add_element%}" onClick="javascript:AddTextEditArea(elems[comboelements.value]);">'."\n";
  $strings = sqlQueryData("SELECT string_id, stringname FROM " . $site_id . "_strings WHERE page=-1");
  if($strings)
  {
    $toolbar .= ' <select name="combostrings" size="1">';
    foreach($strings as $s)
    {
      $toolbar .= '<option value="'.$s['stringname'].'">'.$s['stringname'].'</option>
      ';
    }
    $toolbar .= '</select>';
    $toolbar .= '<input type="button" name="cmdaddstring" style="width:auto;" value="{%langstr:add_string_short%}" onClick="javascript:AddTextEditArea(\'{%string:\' + combostrings.value + \'%}\');">'."\n";
  }


  $form_data['toolbar']['value'] = $toolbar;

  require($GLOBALS['cfgDirRoot']."library/"."class.formgen.php");
  $FormGen = new FormGen();
  $FormGen->action = "?module=".$GLOBALS['module']."&site_id=".$site_id."&action=modcontents";
  $FormGen->cancel = "close";
  $FormGen->title = "Template Contents : " . $row['title'];
  $FormGen->properties = $form_data;
  $FormGen->buttons = true;

  //echo $FormGen->output();

  $docStrings['formaction'] = "?module=".$GLOBALS['module']."&site_id=".$site_id."&action=modcontents";
  $docStrings['templ_id'] = $id;
  $docStrings['templ_name'] = $row['name'];
  $docStrings['templ_desc'] = $row['description'];
  $docStrings['componentscombo'] = $toolbar;
  $docStrings['templ_contents'] = htmlspecialchars($row['body']);

?>
