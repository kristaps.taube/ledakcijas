<?
$site_id = $_GET['site_id'];
$page_id = $_GET['page_id'];
if($page_id)
{
  $GLOBALS['PageEncoding'] = PageEncoding($page_id, $site_id);
  $GLOBALS['maincharset']  = PageEncoding($page_id, $site_id, false);
}

$dir = $GLOBALS['cfgDirRoot'] . 'collections/';
chdir($dir);

$handle = opendir($dir);
while ($file = readdir($handle)) {
  if (is_dir($file) && $file != ".")
    $dirlist[] = $file;
  elseif (is_file($file))
    $filelist[] = $file;
}
closedir($handle);
asort($filelist);

//form lookup
$allcollectiontypes = Array();
foreach($filelist as $file)
{
  if(preg_match('/^class\.(.*?)\.php$/s', $file, $matches))
  {
    $allcollectiontypes[] = $matches[1] . ':'. $matches[1];
  }
}



$form_data = Array(
  "title"        => Array(
    "label"     => "Collection title:",
    "type"      => "str"
  ),

  "type"        => Array(
    "label"     => "Collection type:",
    "type"      => "list",
    "lookup"    => $allcollectiontypes,
  ),
);



require($GLOBALS['cfgDirRoot']."library/"."class.formgen.php");
$FormGen = new FormGen();
$FormGen->action = "?module=".$GLOBALS['module']."&site_id=".$site_id."&action=addcollectionsave";
$FormGen->title = "Create Collection";
$FormGen->cancel =  'close';
$FormGen->properties = $form_data;
$FormGen->buttons = true;
echo $FormGen->output();


?>
