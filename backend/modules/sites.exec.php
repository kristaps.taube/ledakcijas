<?

$perm_managesite = CheckPermission(7, $_POST["site_id"]);
$perm_managesites = $GLOBALS['perm_managesites'];
include($GLOBALS['cfgDirRoot']."library/"."func.apache.php");

  if((($action == 'addsite')and($perm_managesites))or(($action == 'modsite')and(($perm_managesites)or($perm_managesite))))
  {

    $title = $_POST['title'];
    $description = $_POST['description'];
    $domain = $_POST['domain'];
    $alias = $_POST['alias'];
    $wwwroot = $_POST['wwwroot'];
    $dirroot = $_POST['dirroot'];
    $docroot = $_POST['docroot'];
    $site_id = $_POST['site_id']; //this will be empty for new sites
    if($_POST['wapsite']){
      $wapsite = 1;
    }else{
      $wapsite = 0;
    }
    //Check if field input is valid
    $invalid = Array();
    $jsmessage = '';
    if(!CheckLicense($domain))
    {
      $invalid[] = 'domain';
      $jsmessage .= 'No license for domain ' . $domain . ' was found.\n';
    }
    if((IsEmpty($title))or(IsLtGtSymbols($title))or(sqlQueryValue("SELECT site_id FROM sites WHERE title='$title' AND site_id<>'$site_id'")))
    {
      $invalid[] = 'title';
      if(!IsEmpty($title))
        $jsmessage .= 'Site title ' . msgValidCharsUnique . '\n';
    }
    if(IsLtGtSymbols($description))
    {
      $invalid[] = "description";
      $jsmessage .= 'Description ' . msgNoLtGt . '\n';
    }
    if((IsEmpty($domain))or(IsBadHost($domain)))
    {
      $invalid[] = 'domain';
      if(!IsEmpty($domain))
        $jsmessage .= 'Domain name ' . msgValidDomainChars . '\n';
    }
    if((IsEmpty($wwwroot))or(IsBadRoot($wwwroot)))
    {
      $invalid[] = 'wwwroot';
      if(!IsEmpty($wwwroot))
        $jsmessage .= 'Web root ' . msgInvalidChars . '\n';
    }
    if((IsEmpty($dirroot))or($baddir = IsBadRoot($dirroot))/*or($notdir = !is_dir($dirroot))*/)
    {
      $invalid[] = 'dirroot';
      if($baddir)
        $jsmessage .= 'Directory root ' . msgInvalidChars . '\n';
      else
      if($notdir)
        $jsmessage .= 'Directory root ' . msgNotExist . '\n';
    }
    
    //if((IsEmpty($docroot))or($baddir = IsBadRoot($docroot))/*or($notdir = !is_dir($dirroot))*/)
    /*{
      $invalid[] = 'docroot';
      if($baddir)
        $jsmessage .= 'Document root ' . msgInvalidChars . '\n';
      else
      if($notdir)
        $jsmessage .= 'Document root ' . msgNotExist . '\n';
    }*/
    if(count($invalid))
    {
      if($action == 'addsite')
        $action = 'create';
      if($action == 'modsite')
        $action = 'properties_form';
      $invalid = join(':', $invalid);
      $title = urlencode(stripslashes($title));
      $description = urlencode(stripslashes($description));
      $domain = urlencode(stripslashes($domain));
      $wwwroot = urlencode(stripslashes($wwwroot));
      $dirroot = urlencode(stripslashes($dirroot));
      Redirect('?module='.$module.'&action='.$action.'&invalid=' . $invalid . '&formdata_title='.$title.'&formdata_description='.$description.'&formdata_domain='.$domain.'&formdata_alias='.$alias.'&formdata_wwwroot='.$wwwroot.'&formdata_dirroot='.$dirroot.'&formdata_docroot='.$docroot.'&formdata_wapsite='.$wapsite.'&jsmessage='.$jsmessage.'&id='.$site_id);
      Die();
    }
  }


  if(($action == 'addsite')and($perm_managesites))
  {

    $created = time();
    $lastmod = time();
    $modby = $GLOBALS['currentUserID'];

    sqlQuery("INSERT INTO sites (title, description, domain, alias, wwwroot, dirroot, docroot, created, lastmod, modby, wapsite) VALUES ('$title', '$description', '$domain', '$alias', '$wwwroot', '$dirroot', '$docroot', '$created', '$lastmod', '$modby', '$wapsite')");
    //Obtain the ID of newly created site
    $site_id = sqlQueryValue("SELECT site_id FROM sites WHERE title='$title' AND description='$description' AND domain='$domain' AND wwwroot='$wwwroot' AND dirroot='$dirroot' AND docroot='$docroot'");
    //Create all site related tables - contents, templates and pages
    sqlQuery("CREATE TABLE " . $site_id . "_contents (content_id INT (32) not null AUTO_INCREMENT, page INT (32) not null ,
              componentname VARCHAR (250) not null , propertyname VARCHAR (250) not null , propertyvalue
              MEDIUMTEXT not null , PRIMARY KEY (content_id), INDEX (content_id))");
    sqlQuery("CREATE TABLE " . $site_id . "_templates (template_id INT (32) not null AUTO_INCREMENT,
              `ind` int(32) NOT NULL default '0', name VARCHAR (250) not null , body MEDIUMTEXT ,
              description TEXT , copybody INT (32) not null, PRIMARY KEY (template_id),
              INDEX (template_id))");
    sqlQuery("CREATE TABLE " . $site_id . "_ctemplates (ctemplate_id INT (32) not null AUTO_INCREMENT,
              template_id INT (32) not null , page_id INT (32) not null ,
              body MEDIUMTEXT , PRIMARY KEY (ctemplate_id), INDEX (ctemplate_id))");
    sqlQuery("CREATE TABLE " . $site_id . "_pages (page_id INT (32) not null AUTO_INCREMENT, name VARCHAR (50) not null ,
              title TEXT not null , description TEXT not null , template INT (32) not null ,
              lastmod INT (32) not null , created INT (32) not null , ind INT (16) not null ,
              parent INT (32) not null , modby INT (32) not null , enabled INT (1) not null ,
              visible INT (1) not null , passprotect INT (1) not null , language int(32) NOT NULL default '0' ,
              redirect int(32) NOT NULL default '0' , copypage int(32) NOT NULL default '0',
              PRIMARY KEY (page_id), INDEX (page_id))");
    sqlQuery("CREATE TABLE `".$site_id."_pagesdev` (
             `pagedev_id` int(32) NOT NULL auto_increment,
             `page_id` int(32) NOT NULL default '0',
             `name` varchar(50) NOT NULL default '',
             `title` text NOT NULL,
             `description` text NOT NULL,
             `template` int(32) NOT NULL default '0',
             `lastmod` int(32) NOT NULL default '0',
             `created` int(32) NOT NULL default '0',
             `ind` int(16) NOT NULL default '0',
             `parent` int(32) NOT NULL default '0',
             `modby` int(32) NOT NULL default '0',
             `enabled` int(1) NOT NULL default '0',
             `visible` int(1) NOT NULL default '0',
             `passprotect` int(1) NOT NULL default '0',
             `language` int(32) NOT NULL default '0',
             `redirect` int(32) NOT NULL default '0',
             `copypage` int(32) NOT NULL default '0',
             `childrencount` int(11) NOT NULL default '0',
             PRIMARY KEY  (`pagedev_id`),
             KEY `sitepage_id` (`pagedev_id`),
             KEY `page_id` (`page_id`)
            ) TYPE=MyISAM
     ");
    sqlQuery("CREATE TABLE `".$site_id."_contentsdev` (
             `contentdev_id` int(32) NOT NULL auto_increment,
             `pagedev` int(32) NOT NULL default '0',
             `componentname` varchar(250) NOT NULL default '',
             `propertyname` varchar(250) NOT NULL default '',
             `propertyvalue` mediumtext NOT NULL,
             PRIMARY KEY  (`contentdev_id`),
             KEY `content_id` (`contentdev_id`)
            ) TYPE=MyISAM
    ");
    sqlQuery("CREATE TABLE `" . $site_id . "_search` ( `search_id` int(32) NOT NULL auto_increment,
              `content_id` int(32) NOT NULL default '0', PRIMARY KEY (`search_id`), UNIQUE KEY
              `content_id` (`content_id`), KEY `search_id` (`search_id`) )");
    sqlQuery("CREATE TABLE " . $site_id . "_languages (language_id INT (32) not null AUTO_INCREMENT,
              shortname VARCHAR (10) not null , fullname VARCHAR (50) not null ,
              encoding VARCHAR (200) not null , is_default INT (1) not null, PRIMARY KEY (language_id), INDEX (language_id))");
    sqlQuery("CREATE TABLE `" . $site_id . "_colitems` (
               `item_id` int(32) NOT NULL auto_increment,
               `collection_id` int(32) NOT NULL default '0',
               `property_num` int(11) NOT NULL default '0',
               `property_val` MEDIUMTEXT NOT NULL,
               `ind` int(11) NOT NULL default '0',
               PRIMARY KEY  (`item_id`)
              )");
    sqlQuery("CREATE TABLE `" . $site_id . "_collections` (
               `collection_id` int(32) NOT NULL auto_increment,
               `name` varchar(100) NOT NULL default '',
               `type` varchar(50) NOT NULL default '',
               PRIMARY KEY  (`collection_id`)
              )");
    sqlQuery("CREATE TABLE `" . $site_id . "_search_link` (
               `id` int(11) NOT NULL auto_increment,
               `page_id` int(11) NOT NULL default '0',
               `word_id` int(11) NOT NULL default '0',
               `frequency` float NOT NULL default '0',
               `word_count` int(11) NOT NULL default '0',
               PRIMARY KEY  (`id`)
              )");
    sqlQuery("CREATE TABLE `" . $site_id . "_search_object` (
               `id` int(11) NOT NULL auto_increment,
               `data` mediumtext,
               `page_id` int(11) NOT NULL default '0',
               PRIMARY KEY  (`id`)
              ) ");
    sqlQuery("CREATE TABLE `" . $site_id . "_search_word` (
               `id` int(11) NOT NULL auto_increment,
               `word` varchar(150) binary default NULL,
               `object_count` int(11) NOT NULL default '0',
               PRIMARY KEY  (`id`)
              ) ");
    sqlQuery("CREATE TABLE `" . $site_id . "_strings` (
               `string_id` int(32) NOT NULL auto_increment,
               `page` int(32) NOT NULL default '0',
               `stringname` varchar(250) NOT NULL default '',
               `stringvalue` mediumtext NOT NULL,
               PRIMARY KEY  (`string_id`),
               KEY `content_id` (`string_id`)
              )");
    sqlQuery("CREATE TABLE `".$site_id."_stringsdev` (
               `stringdev_id` int(32) NOT NULL auto_increment,
               `pagedev` int(32) NOT NULL default '0',
               `stringname` varchar(250) NOT NULL default '',
               `stringvalue` mediumtext NOT NULL,
               PRIMARY KEY (`stringdev_id`)
              ) TYPE=MyISAM");
    sqlQuery("CREATE TABLE `" . $site_id . "_views` (
               `view_id` int(32) NOT NULL auto_increment,
               `ind` int(11) NOT NULL default '0',
               `template_id` int(32) NOT NULL default '0',
               `name` varchar(100) NOT NULL default '',
               `parent` INT( 32 ) NOT NULL,
               `description` text NOT NULL,
               `levels` varchar(250) NOT NULL default '',
               `parents` varchar(250) NOT NULL default '',
               `languages` varchar(250) NOT NULL default '',
               `language_id` int(32) NOT NULL default 0,
               PRIMARY KEY  (`view_id`)
              ) TYPE=MyISAM");
    sqlQuery("CREATE TABLE `" . $site_id . "_stats` (
                 `ID` int(10) unsigned NOT NULL auto_increment,
                 `timestamp` varchar(20) default NULL,
                 `page_id` int(11) default NULL,
                 `IP` varchar(20) default NULL,
                 `referrer` text,
                 `user_agent` varchar(100) default NULL,
                 `lang` varchar(5) default NULL,
                 `is_proxy` int(1) NOT NULL default '0',
                 PRIMARY KEY  (`ID`),
                 UNIQUE KEY `ID` (`ID`),
                 UNIQUE KEY `ID_2` (`ID`)
                ) TYPE=MyISAM");


    //SQL for new stats tables
    sqlQuery("
        CREATE TABLE `".$site_id."_stats_main` (
        `sm_id` INT NOT NULL AUTO_INCREMENT,
        `session_id` VARCHAR( 128 ) NOT NULL ,
        `ip` VARCHAR( 20 ) NOT NULL ,
        `referrer` TEXT NOT NULL ,
        `user_agent` VARCHAR( 255 ) NOT NULL ,
        `lang` VARCHAR( 5 ) NOT NULL ,
        `is_proxy` INT( 1 ) DEFAULT '0' NOT NULL ,
        `time` INT UNSIGNED NOT NULL ,
        UNIQUE KEY `sm_id` (`sm_id`),
        KEY `session_id` (`session_id`),
        KEY `time` (`time`)
        )
    ");
    sqlQuery("
        CREATE TABLE `".$site_id."_stats_pages` (
        `sp_id` INT NOT NULL AUTO_INCREMENT,
        `sm_id` INT NOT NULL ,
        `time` INT NOT NULL ,
        `page_path` VARCHAR( 255 ) NOT NULL ,
        UNIQUE KEY `sp_id` (`sp_id`),
        KEY `sm_id` (`sm_id`),
        KEY `time` (`time`)
        )
    ");
    sqlQuery("CREATE TABLE `".$site_id."_wapcontents` (
         `wapcontent_id` int(32) NOT NULL auto_increment,
         `wappage` int(32) NOT NULL default '0',
         `componentname` varchar(250) NOT NULL default '',
         `propertyname` varchar(250) NOT NULL default '',
         `propertyvalue` text NOT NULL,
         PRIMARY KEY  (`wapcontent_id`),
         KEY `wapcontent_id` (`wapcontent_id`)
        ) TYPE=MyISAM
    ");
    sqlQuery("CREATE TABLE `".$site_id."_wapcontentsdev` (
         `wapcontentdev_id` int(32) NOT NULL auto_increment,
         `wappagedev` int(32) NOT NULL default '0',
         `componentname` varchar(250) NOT NULL default '',
         `propertyname` varchar(250) NOT NULL default '',
         `propertyvalue` text NOT NULL,
         PRIMARY KEY  (`wapcontentdev_id`),
         KEY `wapcontentdev_id` (`wapcontentdev_id`)
        ) TYPE=MyISAM
    ");
    sqlQuery("CREATE TABLE `".$site_id."_wapctemplates` (
         `ctemplate_id` int(32) NOT NULL auto_increment,
         `template_id` int(32) NOT NULL default '0',
         `wappage_id` int(32) NOT NULL default '0',
         `body` mediumtext,
         PRIMARY KEY  (`ctemplate_id`),
         KEY `ctemplate_id` (`ctemplate_id`)
        ) TYPE=MyISAM
    ");
    sqlQuery("CREATE TABLE `".$site_id."_wappages` (
         `wappage_id` int(32) NOT NULL auto_increment,
         `name` varchar(50) NOT NULL default '',
         `title` text NOT NULL,
         `description` text NOT NULL,
         `template` int(32) NOT NULL default '0',
         `lastmod` int(32) NOT NULL default '0',
         `created` int(32) NOT NULL default '0',
         `ind` int(16) NOT NULL default '0',
         `parent` int(32) NOT NULL default '0',
         `modby` int(32) NOT NULL default '0',
         `enabled` int(1) NOT NULL default '0',
         `visible` int(1) NOT NULL default '0',
         `redirect` int(32) NOT NULL default '0',
         `copypage` int(32) NOT NULL default '0',
         `is_card` tinyint(1) NOT NULL default '0',
         `is_root` char(1) NOT NULL default '1',
         PRIMARY KEY  (`wappage_id`),
         KEY `page_id` (`wappage_id`)
        ) TYPE=MyISAM
    ");
    sqlQuery("CREATE TABLE `".$site_id."_wappagesdev` (
         `wappagedev_id` int(32) NOT NULL auto_increment,
         `name` varchar(50) NOT NULL default '',
         `title` text NOT NULL,
         `description` text NOT NULL,
         `template` int(32) NOT NULL default '0',
         `lastmod` int(32) NOT NULL default '0',
         `created` int(32) NOT NULL default '0',
         `ind` int(16) NOT NULL default '0',
         `parent` int(32) NOT NULL default '0',
         `modby` int(32) NOT NULL default '0',
         `enabled` int(1) NOT NULL default '0',
         `visible` int(1) NOT NULL default '0',
         `redirect` int(32) NOT NULL default '0',
         `copypage` int(32) NOT NULL default '0',
         `is_card` tinyint(1) NOT NULL default '0',
         `wappage_id` int(11) NOT NULL default '0',
         `is_root` char(1) NOT NULL default '1',
         PRIMARY KEY  (`wappagedev_id`),
         KEY `page_id` (`wappagedev_id`)
        ) TYPE=MyISAM
    ");
    sqlQuery("CREATE TABLE `".$site_id."_waptemplates` (
         `template_id` int(32) NOT NULL auto_increment,
         `ind` int(32) NOT NULL default '0',
         `name` varchar(250) NOT NULL default '',
         `body` mediumtext,
         `description` text,
         `copybody` int(32) NOT NULL default '0',
         PRIMARY KEY  (`template_id`),
         KEY `template_id` (`template_id`)
        ) TYPE=MyISAM
    ");
    sqlQuery("CREATE TABLE `".$site_id."_documentdir` (
         `dir_id` int(11) NOT NULL auto_increment,
         `dir_name` text NOT NULL,
         `dir_description` text NOT NULL,
         `parent_directory` int(11) NOT NULL default '0',
         `creation_date` text NOT NULL,
         `ind` int(11) NOT NULL default '0',
         UNIQUE KEY `dir_id` (`dir_id`),
         UNIQUE KEY `dir_id_2` (`dir_id`),
         UNIQUE KEY `dir_id_3` (`dir_id`)
        ) TYPE=MyISAM
    ");
    sqlQuery("CREATE TABLE `".$site_id."_documents` (
         `file_id` int(11) NOT NULL auto_increment,
         `file_name` text NOT NULL,
         `file_description` text NOT NULL,
         `parent_directory` int(11) NOT NULL default '0',
         `real_name` text NOT NULL,
         `creation_date` text NOT NULL,
         `ind` int(11) NOT NULL default '0',
         `public` int(1) NOT NULL default '1',
         `deleted` int(1) NOT NULL default '0',
         UNIQUE KEY `file_id` (`file_id`)
        ) TYPE=MyISAM
    ");
    sqlQuery("CREATE TABLE `".$site_id."_documentstats` (
         `id` int(11) NOT NULL auto_increment,
         `document_id` int(11) NOT NULL default '0',
         `type` varchar(250) NOT NULL default '',
         `time` varchar(250) NOT NULL default '',
         `IP` varchar(15) NOT NULL default '',
         `user_id` int(11) NOT NULL default '0',
         UNIQUE KEY `id` (`id`)
        ) TYPE=MyISAM
    ");
    sqlQuery("CREATE TABLE `".$site_id."_file_keywords` (
         `keyword_id` int(11) NOT NULL auto_increment,
         `word` varchar(250) NOT NULL default '',
         `category_id` int(11) NOT NULL default '0',
         PRIMARY KEY (`keyword_id`)
        ) TYPE=MyISAM
    ");
    sqlQuery("CREATE TABLE `".$site_id."_filesearch_keywordlink` (
         `link_id` int(11) NOT NULL auto_increment,
         `keyword_id` int(11) NOT NULL default '0',
         `file_path` varchar(255) NOT NULL default '',
         PRIMARY KEY (`link_id`)
        ) TYPE=MyISAM
    ");
    sqlQuery("CREATE TABLE `".$site_id."_filesearch_link` (
         `id` int(11) NOT NULL auto_increment,
         `word_id` int(11) NOT NULL default '0',
         `frequency` float NOT NULL default '0',
         `word_count` int(11) NOT NULL default '0',
         `filename` varchar(255) NOT NULL default '',
         PRIMARY KEY (`id`)
        ) TYPE=MyISAM
    ");
    sqlQuery("CREATE TABLE `".$site_id."_filesearch_object` (
         `id` int(11) NOT NULL auto_increment,
         `data` text,
         `filename` varchar(255) NOT NULL default '',
         PRIMARY KEY (`id`)
        ) TYPE=MyISAM
    ");
    sqlQuery("CREATE TABLE `".$site_id."_filesearch_word` (
         `id` int(11) NOT NULL auto_increment,
         `word` varchar(150) character set latin1 collate latin1_bin default NULL,
         `object_count` int(11) NOT NULL default '0',
         PRIMARY KEY (`id`)
        ) TYPE=MyISAM
    ");            
    sqlQuery("CREATE TABLE `".$site_id."_pageshistory` (
         `pagehistory_id` int(32) NOT NULL auto_increment,
         `page_id` int(32) NOT NULL default '0',
         `title` text NOT NULL,
         `date` int(32) NOT NULL default '0',
         `html` text,
         PRIMARY KEY (`pagehistory_id`),
         KEY `ctemplate_id` (`pagehistory_id`)
        ) TYPE=MyISAM
    ");


    //Add permissions to newly created site for admins group
    //manage site, pages, templates, users, groups, permissions and files
    sqlQuery("INSERT INTO permissions (perm_num, isuser, id, data) VALUES (6, 0, 2, $site_id)");
    sqlQuery("INSERT INTO permissions (perm_num, isuser, id, data) VALUES (7, 0, 2, $site_id)");
    sqlQuery("INSERT INTO permissions (perm_num, isuser, id, data) VALUES (8, 0, 2, $site_id)");
    sqlQuery("INSERT INTO permissions (perm_num, isuser, id, data) VALUES (9, 0, 2, $site_id)");
    sqlQuery("INSERT INTO permissions (perm_num, isuser, id, data) VALUES (10, 0, 2, $site_id)");
    //Add permissions to newly created site for users group
    //manage site, pages and files
    sqlQuery("INSERT INTO permissions (perm_num, isuser, id, data) VALUES (6, 0, 4, $site_id)");
    sqlQuery("INSERT INTO permissions (perm_num, isuser, id, data) VALUES (7, 0, 4, $site_id)");
    sqlQuery("INSERT INTO permissions (perm_num, isuser, id, data) VALUES (8, 0, 4, $site_id)");
    //Add permissions to newly created site for power users group
    //manage site, pages, templates and files
    sqlQuery("INSERT INTO permissions (perm_num, isuser, id, data) VALUES (6, 0, 3, $site_id)");
    sqlQuery("INSERT INTO permissions (perm_num, isuser, id, data) VALUES (7, 0, 3, $site_id)");
    sqlQuery("INSERT INTO permissions (perm_num, isuser, id, data) VALUES (8, 0, 3, $site_id)");
    sqlQuery("INSERT INTO permissions (perm_num, isuser, id, data) VALUES (9, 0, 3, $site_id)");

    //Add sample template
    sqlQuery('INSERT INTO `'.$site_id.'_templates` (`template_id`, `name`, `body`, `description`) VALUES (\'1\', \'default\', \'<html> <head> <title>{%title%}</title> <meta name="description" content=""> <meta name="keywords" content=""> </head> <body> {%component:formatedtext:maincontents%} </body> </html> \', \'Default empty template\')');
    $template_id = sqlQueryValue("SELECT template_id FROM " . $site_id . "_templates WHERE name='default'");
    //Add access to the newly created template for Everyone
    AddGroupPermission(1, 12, $site_id, $template_id);

    //Add sample pages
    sqlQuery("INSERT INTO `".$site_id."_pages` ( `page_id` , `name` , `title` , `description` , `template` , `lastmod` , `created` , `ind` , `parent` , `modby` , `enabled` , `visible` , `language` , `redirect` , `copypage` )
                     VALUES (
                     '1', '404', '404 - page not found', 'The page to display when page not found',
                     '".$template_id."', '".time()."', '".time()."', '1', '0', '1', '1', '0',
                     '0', '0', '0'
                     )");
    sqlQuery("INSERT INTO `".$site_id."_pagesdev` ( `page_id` , `name` , `title` , `description` , `template` , `lastmod` , `created` , `ind` , `parent` , `modby` , `enabled` , `visible` , `language` , `redirect` , `copypage` )
                     VALUES (
                     '1', '404', '404 - page not found', 'The page to display when page not found',
                     '".$template_id."', '".time()."', '".time()."', '1', '0', '1', '1', '0',
                     '0', '0', '0'
                     )");
    $page_id = sqlQueryValue("SELECT page_id FROM " . $site_id . "_pages WHERE name='404'");
    AddGroupPermission(1, 11, $site_id, $page_id);

    sqlQuery("INSERT INTO `".$site_id."_pages` ( `page_id` , `name` , `title` , `description` , `template` , `lastmod` , `created` , `ind` , `parent` , `modby` , `enabled` , `visible` , `language` , `redirect` , `copypage` )
                     VALUES (
                     '2', 'index', 'Main Page', 'The default page',
                     '".$template_id."', '".time()."', '".time()."', '2', '0', '1', '1', '1',
                     '0', '0', '0'
                     )");
    sqlQuery("INSERT INTO `".$site_id."_pagesdev` ( `page_id` , `name` , `title` , `description` , `template` , `lastmod` , `created` , `ind` , `parent` , `modby` , `enabled` , `visible` , `language` , `redirect` , `copypage` )
                     VALUES (
                     '2', 'index', 'Main Page', 'The default page',
                     '".$template_id."', '".time()."', '".time()."', '2', '0', '1', '1', '1',
                     '0', '0', '0'
                     )");
    $page_id = sqlQueryValue("SELECT page_id FROM " . $site_id . "_pages WHERE name='index'");
    AddGroupPermission(1, 11, $site_id, $page_id);


    Add_Log("New site '$title' ($site_id)", $site_id);

    //Update Apache configuration file
    if(!AddVirtualHost($domain, StripSlashes($dirroot)))
    {
      if($GLOBALS['ApacheConfig'])
      {
        $docTemplate = "infoform.htm";
        $docStrings['intro'] = "Constructor was unable to update Apache configuration file.<br>
                                Please add following lines at the end of httpd.conf:<br><br>";
        $docStrings['lines'] = htmlspecialchars(VirtualHostString($domain, StripSlashes($dirroot)));
      }
    }else
    {
      exec($ApacheRestartCmd);
    }

  }else if(($action == 'modsite')and(($perm_managesites)or($perm_managesite)))
  {
    $lastmod = time();
    $modby = $GLOBALS['currentUserID'];
    list($lastdomain, $lastdirroot) = sqlQueryRow("SELECT domain, dirroot FROM sites WHERE site_id='$site_id'");
    sqlQuery("UPDATE sites SET title='$title', description='$description', domain='$domain', alias='$alias', wwwroot='$wwwroot', dirroot='$dirroot', docroot='$docroot', modby='$modby', wapsite='$wapsite' WHERE site_id='$site_id'");
    Add_Log("Updated site '$title' ($site_id)", $site_id);
    if(($domain != $lastdomain)or($dirroot != $lastdirroot)){
      RemoveVirtualHost($lastdomain, $lastdirroot);
      if($GLOBALS['ApacheConfig'])
      {
        if(!AddVirtualHost($domain, StripSlashes($dirroot)))
        {
          $docTemplate = "infoform.htm";
          $docStrings['intro'] = "Constructor was unable to update Apache configuration file.<br>
                              Please change the following lines in httpd.conf:<br><br>";
          $docStrings['lines'] = htmlspecialchars(VirtualHostString($domain, StripSlashes($dirroot)));
        }else
        {
          exec($ApacheRestartCmd);
        }
      }
    }
  }else if(($action == 'delsite')and($perm_managesites))
  {
    $site_id = $_GET['id'];
    list($title, $lastdomain, $lastdirroot) = sqlQueryRow("SELECT title, domain, dirroot FROM sites WHERE site_id=$site_id");
    sqlQuery("DELETE FROM sites WHERE site_id=$site_id");
    sqlQuery("DROP TABLE IF EXISTS " . $site_id . "_pages");
    sqlQuery("DROP TABLE IF EXISTS " . $site_id . "_pagesdev");
    sqlQuery("DROP TABLE IF EXISTS " . $site_id . "_contents");
    sqlQuery("DROP TABLE IF EXISTS " . $site_id . "_contentsdev");

    sqlQuery("DROP TABLE IF EXISTS " . $site_id . "_documentdir");
    sqlQuery("DROP TABLE IF EXISTS " . $site_id . "_documents");
    sqlQuery("DROP TABLE IF EXISTS " . $site_id . "_documentstats");

    sqlQuery("DROP TABLE IF EXISTS " . $site_id . "_file_keywords");
    sqlQuery("DROP TABLE IF EXISTS " . $site_id . "_filesearch_keywordlink");
    sqlQuery("DROP TABLE IF EXISTS " . $site_id . "_filesearch_link");
    sqlQuery("DROP TABLE IF EXISTS " . $site_id . "_filesearch_object");
    sqlQuery("DROP TABLE IF EXISTS " . $site_id . "_filesearch_word");

    sqlQuery("DROP TABLE IF EXISTS " . $site_id . "_wappages");
    sqlQuery("DROP TABLE IF EXISTS " . $site_id . "_wappagesdev");
    sqlQuery("DROP TABLE IF EXISTS " . $site_id . "_wapcontents");
    sqlQuery("DROP TABLE IF EXISTS " . $site_id . "_wapcontentsdev");
    sqlQuery("DROP TABLE IF EXISTS " . $site_id . "_waptemplates");
    sqlQuery("DROP TABLE IF EXISTS " . $site_id . "_wapctemplates");

    sqlQuery("DROP TABLE IF EXISTS " . $site_id . "_pageshistory");
    sqlQuery("DROP TABLE IF EXISTS " . $site_id . "_templates");
    sqlQuery("DROP TABLE IF EXISTS " . $site_id . "_ctemplates");
    sqlQuery("DROP TABLE IF EXISTS " . $site_id . "_search");
    sqlQuery("DROP TABLE IF EXISTS " . $site_id . "_languages");
    sqlQuery("DROP TABLE IF EXISTS " . $site_id . "_colitems");
    sqlQuery("DROP TABLE IF EXISTS " . $site_id . "_collections");
    sqlQuery("DROP TABLE IF EXISTS " . $site_id . "_search_link");
    sqlQuery("DROP TABLE IF EXISTS " . $site_id . "_search_object");
    sqlQuery("DROP TABLE IF EXISTS " . $site_id . "_search_word");
    sqlQuery("DROP TABLE IF EXISTS " . $site_id . "_strings");
    sqlQuery("DROP TABLE IF EXISTS " . $site_id . "_stringsdev");
    sqlQuery("DROP TABLE IF EXISTS " . $site_id . "_views");
    sqlQuery("DROP TABLE IF EXISTS " . $site_id . "_stats");
    sqlQuery("DROP TABLE IF EXISTS " . $site_id . "_stats_main");
    sqlQuery("DROP TABLE IF EXISTS " . $site_id . "_stats_pages");
    sqlQuery("DELETE FROM permissions WHERE data=$site_id");
    sqlQuery("DELETE FROM users WHERE site_id=$site_id");
    sqlQuery("DELETE FROM groups WHERE site_id=$site_id");
    Add_Log("Deleted site '$title' ($site_id)", $site_id);
    if(!RemoveVirtualHost($lastdomain, $lastdirroot))
    {
      //umm... warning that site will be deleted...
    }else
    {
      exec($ApacheRestartCmd);
    }
  }
?>
