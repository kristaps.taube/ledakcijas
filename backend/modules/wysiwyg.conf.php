<?

$action = $_GET["action"];
$site_id = $_GET["site_id"];
$page_id = $_GET["page_id"];

unset($perm_managefiles);
    if(($site_id == $GLOBALS["currentUserSiteID"])or(!$GLOBALS["currentUserSiteID"]))
      $perm_managefiles = CheckPermission(7, $site_id);

switch ($action) {

  case "edithtml":
    $GLOBALS['guiFolder'] = "wysiwyg";
    $docTemplate = "dlg_html.htm";
    $docTitle = "HTML Editor";
    break;

  case "image":
    $GLOBALS['guiFolder'] = "wysiwyg";
    $docTemplate = "dlg_image.htm";
    $docTitle = "{%langstr:insert_image%}";
    $docStrings['frame'] = "?module=files&action=list&site_id=".$GLOBALS['site_id']."&view=".$GLOBALS['view'];
    $docStrings['label'] = "{%langstr:file_path%}:";
    if($perm_managefiles)
      $GLOBALS['canmanagefiles'] = 'ok';
    break;

  case "lightningimage":
    $GLOBALS['guiFolder'] = "wysiwyg";
    $docTemplate = "dlg_lightningimage.htm";
    $docTitle = "Insert Lightning Image";
    $docStrings['frame'] = "?module=wysiwyg&action=uploadlightningimage&site_id=".$GLOBALS['site_id'];
    break;

  case "uploadlightningimage":
    $docTemplate = "form.htm";
    $docScripts['body'] = "wysiwyg.uploadlightningimage.php";
    break;

  case "savelightningimage":
    $docTemplate = "form.htm";
    $docScripts['exec'] = "wysiwyg.savelightningimage.php";
    $docScripts['body'] = "wysiwyg.uploadlightningimage.php";
    break;

  case "hyperlink":
    $GLOBALS['guiFolder'] = "wysiwyg";
    $docTemplate = "dlg_link.htm";
    $docTitle = "{%langstr:insert_hyperlink%}";
    $docStrings['frame'] = "?module=pages&action=simple&site_id=".$site_id;
    break;

  case "waphyperlink":
    $GLOBALS['guiFolder'] = "wysiwyg";
    $docTemplate = "dlg_link.htm";
    $docTitle = "{%langstr:insert_hyperlink%}";
    $docStrings['frame'] = "?module=wap&action=simple&site_id=".$site_id;
    break;

  case "ghyperlink":
    $GLOBALS['guiFolder'] = "wysiwyg";
    $docTemplate = "dlg_glink.htm";
    $docTitle = "Insert Global Hyperlink";
    //$docStrings['frame'] = "?module=pages&action=simple&site_id=".$site_id;
    break;

  case "popupimage":
    $GLOBALS['guiFolder'] = "wysiwyg";
    $docTemplate = "dlg_popupimage.htm";
    $docTitle = "Insert Popup Image";
    $docStrings['frame'] = "?module=files&action=list&site_id=".$GLOBALS['site_id']."&view=".$GLOBALS['view'];
    $docStrings['label'] = "Image path:";
    if($perm_managefiles)
      $GLOBALS['canmanagefiles'] = 'ok';
    break;

  case "table":
    $GLOBALS['guiFolder'] = "wysiwyg";
    $docTemplate = "dlg_table.htm";
    $docTitle = "Insert Table";
    break;

  case "tprop":
    $GLOBALS['guiFolder'] = "wysiwyg";
    $docTemplate = "dlg_tprop.htm";
    $docTitle = "Table Properties";
    break;

  case "cellprop":
    $GLOBALS['guiFolder'] = "wysiwyg";
    $docTemplate = "dlg_cellprop.htm";
    $docTitle = "Cell Properties";
    break;

  case "rowprop":
    $GLOBALS['guiFolder'] = "wysiwyg";
    $docTemplate = "dlg_rowprop.htm";
    $docTitle = "Row Properties";
    break;

  case "setcolor":
    $GLOBALS['guiFolder'] = "wysiwyg";
    $docTemplate = "dlg_setcolor.htm";
    $docTitle = "Set Color";
    break;

  case "paste":
    $GLOBALS['guiFolder'] = "wysiwyg";
    $docTemplate = "dlg_paste.htm";
    $docTitle = "Paste From Office";
    break;

  case "instext":
    $GLOBALS['guiFolder'] = "wysiwyg";
    $docTemplate = "dlg_instext.htm";
    $docTitle = "Insert Plain Text";
    break;

  case "formgeneratoremail":
    $GLOBALS['guiFolder'] = "wysiwyg";
    $docTemplate = "dlg_fmail.htm";
    $docTitle = "Formgenerator email properties";
    break;
    
  case "formgeneratormsgs":
    $GLOBALS['guiFolder'] = "wysiwyg";
    $docTemplate = "dlg_fmsgs.htm";
    $docTitle = "Formgenerator error messages";
    break;

  case "maillistdlg":
    $GLOBALS['guiFolder'] = "wysiwyg";
    $docTemplate = "dlg_mlistdlg.htm";
    $docTitle = "Mailing List";
    break;

  case "maillistdlgsign":
    $GLOBALS['guiFolder'] = "wysiwyg";
    $docTemplate = "dlg_mlistdlgsign.htm";
    $docTitle = "Mailing List";
    break;

  case "imageprops":
    $GLOBALS['guiFolder'] = "wysiwyg";
    $docTemplate = "dlg_imgprops.htm";
    $docTitle = "Formgenerator error messages";
    break;

  case "dialog":
    $GLOBALS['guiFolder'] = "wysiwyg";
    $docTemplate = "formgeniframe.html";
    break;

  case "editimage":
    $docTemplate = "editor.htm";
    $docScripts['body'] = "wysiwyg.editimage.php";
    break;

  case "editorFrame":
    $docTemplate = "editor.htm";
    $docScripts['body'] = "wysiwyg.editorframe.php";
    break;
    
  default:
    $GLOBALS['guiFolder'] = "wysiwyg";
    $docTemplate = "dlg_wysiwyg_fck.htm";
    $docTitle = "Text Editor";
    $sitedata = sqlQueryRow("SELECT domain, wwwroot FROM sites WHERE site_id='".$site_id."'");
    $docStrings['base'] = $sitedata['domain'] . $sitedata['wwwroot'];
    if($page_id)
    {
      $docStrings['PageStylesheet'] = GetPageStylesheet($site_id, $page_id);
    }
    break;

}

?>
