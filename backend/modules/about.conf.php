<?


if($GLOBALS['currentUserSiteID'])
{
  Header("Location: index.php?site_id=" . $GLOBALS['currentUserSiteID'] . "&module=" . defaultSiteModule);
  Die;
}

switch ($action) {

  default:
    $docTemplate = "main.htm";
    $docTitle = "About";
    $docScripts['body'] = "about.default.php";
    $docStrings['toolbar'] = '<div style="padding-left: 25px; padding-top: 10px; padding-bottom: 10px"><span style="font-size: 18pt; color: #9CB4E4">Datateks Constructor</span></div>';
    break;
}

?>
