<?

$perm_managefiles = $GLOBALS['perm_managefiles'];
if(!$perm_managefiles)
  AccessDenied(True);

$site_id = $GLOBALS['site_id'];

$data = sqlQueryData("SELECT * FROM " . $site_id . "_file_keywords ORDER BY category_id");

$keywords = Array();
$categories = Array();
foreach($data as $row)
{
  if($row['category_id'] == 0)
  {
    $keywords[$row['keyword_id']] = Array();
    $categories[] = Array("word" => $row['word'],"id" => $row['keyword_id']);
  }else
  {
    $keywords[$row['category_id']][] = Array('word' => $row['word'], 'id' => $row['keyword_id']);
  }
}

foreach($categories as $c)
{
  $lookup[] = $c['id'] . ':' . $c['word'];
  foreach($keywords[$c['id']] as $k)
  {
    $lookup[] = $k['id'] . ':&nbsp;&nbsp;&nbsp;&nbsp;' . $k['word'];
  }
}


$form_data = Array(

  "category"        => Array(
    "label"     => "Keyword:",
    "lookup"    => $lookup,
    "type"      => "list"
  )

);
echo "<br>&nbsp;&nbsp;Please select keword or category you want to delete.<br>
&nbsp;&nbsp;Deleting category will delete all its keywords as well.<br><br>";
require($GLOBALS['cfgDirRoot']."library/"."class.formgen.php");
$FormGen = new FormGen();
$FormGen->action = "?module=files&action=dodelkeyword&site_id=".$_GET['site_id'];
$FormGen->title = "Delete";
$FormGen->cancel = 'close';
$FormGen->properties = $form_data;
$FormGen->buttons = true;
echo $FormGen->output();

?>
