<?
//Permissions array module
// Created by Aivars Irmejs
//   all permissions are numbered
//      text - description of permission, for permission lists
//      type - permission group
//             0 - global permissions
//             1 - site permissions
//             2 - page permissions
//             3 - template permissions
//             4 - wap page permissions
//             5 - wap template permissions
//      minglob - array of global permission numbers. user must have one of them to modify the permission
//      minsite - array of site permission numbers. user must have one of them to modify the permission
//user must have either minglob or minsite to modify the permission


$permissions_list = Array(
  1           => Array(
    "text"    => "{%langstr:manage_all_sites%}",
    "type"    => 0,
    "minglob" => Array(4)
  ),
  2           => Array(
    "text"    => "{%langstr:manage_all_users%}",
    "type"    => 0,
    "minglob" => Array(4)
  ),
  3           => Array(
    "text"    => "{%langstr:manage_all_groups%}",
    "type"    => 0,
    "minglob" => Array(4)
  ),
  4           => Array(
    "text"    => "{%langstr:manage_permissions%}",
    "type"    => 0,
    "minglob" => Array(4)
  ),
  /*
  5           => Array(
    "text"    => "Access Administration",
    "type"    => 0,
    "minglob" => Array(4)
  ),
  */
  6           => Array(
    "text"    => "{%langstr:modify_site_contents%}",
    "type"    => 1,
    "minglob" => Array(4),
    "minsite" => Array(10)
  ),
  8           => Array(
    "text"    => "{%langstr:manage_all_pages%}",
    "type"    => 1,
    "minglob" => Array(4),
    "minsite" => Array(10)
  ),
  9           => Array(
    "text"    => "{%langstr:manage_all_templates%}",
    "type"    => 1,
    "minglob" => Array(4),
    "minsite" => Array(10)
  ),
  7           => Array(
    "text"    => "{%langstr:manage_files%}",
    "type"    => 1,
    "minglob" => Array(4),
    "minsite" => Array(10)
  ),
/*  19          => Array(
    "text"    => "{%langstr:manage_wap_pages%}",
    "type"    => 1,
    "minglob" => Array(4),
    "minsite" => Array(10)
  ),
  23          => Array(
    "text"    => "{%langstr:manage_wap_templates%}",
    "type"    => 1,
    "minglob" => Array(4),
    "minsite" => Array(10)
  ),
  28          => Array(
    "text"    => "{%langstr:manage_documents%}",
    "type"    => 1,
    "minglob" => Array(4),
    "minsite" => Array(10)
  ), */
  10          => Array(
    "text"    => "{%langstr:manage_site_users_groups_permissions%}",
    "type"    => 1,
    "minglob" => Array(4),
    "minsite" => Array(10)
  ),
  17          => Array(
    "text"    => "{%langstr:manage_all_collections%}",
    "type"    => 1,
    "minglob" => Array(4),
    "minsite" => Array(10)
  ),
  66          => Array(
    "text"    => "{%langstr:manage_all_lists%}",
    "type"    => 1,
    "minglob" => Array(4),
    "minsite" => Array(10)
  ),
  18          => Array(
    "text"    => "{%langstr:access_site_administration%}",
    "type"    => 1,
    "minglob" => Array(4),
    "minsite" => Array(10)
  ),
  34          => Array(
    "text"    => "{%langstr:access_site_statistics%}",
    "type"    => 1,
    "minglob" => Array(4),
    "minsite" => Array(10)
  ),
  32          => Array(
    "text"    => "{%langstr:autopublish_all%}",
    "type"    => 1,
    "minglob" => Array(4),
    "minsite" => Array(10)
  ),
  33          => Array(
    "text"    => "{%langstr:can_publish_all_pages%}",
    "type"    => 1,
    "minglob" => Array(4),
    "minsite" => Array(10)
  ),
  11          => Array(
    "text"    => "{%langstr:modify_page_contents%}",
    "type"    => 2,
    "minglob" => Array(4),
    "minsite" => Array(10,8)
  ),
  13          => Array(
    "text"    => "{%langstr:delete_page%}",
    "type"    => 2,
    "minglob" => Array(4),
    "minsite" => Array(10,8)
  ),
  16          => Array(
    "text"    => "{%langstr:only_access_spec_page%}",
    "type"    => 2,
    "minglob" => Array(4),
    "minsite" => Array(10,8)
  ),
  31          => Array(
    "text"    => "{%langstr:publish_page%}",
    "type"    => 2,
    "minglob" => Array(4),
    "minsite" => Array(10,8)
  ),
  12          => Array(
    "text"    => "Modify Template Contents",
    "type"    => 3,
    "minglob" => Array(4),
    "minsite" => Array(10,9)
  ),
  14          => Array(
    "text"    => "{%langstr:only_allow_access_specified_template%}",
    "type"    => 3,
    "minglob" => Array(4),
    "minsite" => Array(10,9)
  ),
  15          => Array(
    "text"    => "{%langstr:delete_template%}",
    "type"    => 3,
    "minglob" => Array(4),
    "minsite" => Array(10,8)
  ),

  20         => Array(
    "text"    => "Modify WAP Page Contents",
    "type"    => 4,
    "minglob" => Array(4),
    "minsite" => Array(10,19)
  ),
  21          => Array(
    "text"    => "Delete WAP Page",
    "type"    => 4,
    "minglob" => Array(4),
    "minsite" => Array(10,19)
  ),
  22          => Array(
    "text"    => "{%langstr:only_allow_access_specified_wap_page_components%}",
    "type"    => 4,
    "minglob" => Array(4),
    "minsite" => Array(10,19)
  ),
  27          => Array(
    "text"    => "Publish WAP Page",
    "type"    => 4,
    "minglob" => Array(4),
    "minsite" => Array(10,19)
  ),

  24         => Array(
    "text"    => "Modify WAP template Contents",
    "type"    => 5,
    "minglob" => Array(4),
    "minsite" => Array(10,23)
  ),
  25          => Array(
    "text"    => "Delete WAP template",
    "type"    => 5,
    "minglob" => Array(4),
    "minsite" => Array(10,23)
  ),
  26          => Array(
    "text"    => "{%langstr:only_access_wap_template_components%}",
    "type"    => 5,
    "minglob" => Array(4),
    "minsite" => Array(10,23)
  ),

  29         => Array(
    "text"    => "{%langstr:download_document%}",
    "type"    => 5,
    "minglob" => Array(4),
    "minsite" => Array(10,28)
  ),
  30          => Array(
    "text"    => "{%langstr:delete_document%}",
    "type"    => 5,
    "minglob" => Array(4),
    "minsite" => Array(10,28)
  ),

);



?>
