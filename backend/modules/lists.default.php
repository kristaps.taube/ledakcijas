<?php

$site_id = $GLOBALS['site_id'];

#$data = SQLQueryData('SELECT DISTINCT li.list_id, li.ind, li.visible, li.page_id, li.title, li.componentname, li.componenttype, li.site_id, li.collection_id FROM lists li, '.$site_id.'_pagesdev pag WHERE site_id = '.$site_id.' AND ((li.page_id = pag.page_id AND pag.language = '.$_SESSION['currentlanguagenum'].') OR li.collection_id > 0) ORDER BY ind');
$data = SQLQueryData('SELECT DISTINCT li.list_id, li.ind, li.visible, li.page_id, li.title, li.componentname, li.componenttype, li.site_id, li.collection_id FROM lists li, '.$site_id.'_pagesdev pag WHERE site_id = '.$site_id.' ORDER BY ind');

if ($data)
{
  echo '<style>
  .borderdiv { display: none; visibility: hidden;}
  </style>';

  echo '<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formExFrame">';

  foreach ($data as $d)
  {
    if (!$GLOBALS['user_site_permissions_managelists'] && !$d['visible']) continue;

    if($d['collection_id'])
    {
      echo '<tr class="list">
              <td width="100%" class="tableHead">
                <a href="?module=collections&site_id='.$GLOBALS['site_id'].'&action=editcollection&id='.$d['collection_id'].'&justedit=1&resetfilters=1">&raquo; '.$d['title'].'</a></td>';
      echo '<td nowrap="nowrap">';
      if ($GLOBALS['user_site_permissions_managelists'])
      {
        echo '<input type="checkbox" '.($d['visible'] ? 'checked="checked"' : '' ).' onclick="javascript: window.location.href =\'?module=lists&site_id='.$GLOBALS['site_id'].'&action=change_visibility&list_id='.$d['list_id'].'\';" />
              <a href="?module=lists&site_id='.$GLOBALS['site_id'].'&action=up&ind='.$d['ind'].'" title="{%langstr:up%}" ><img src="/cms/backend/gui/images/new/up.png" /></a>

              <a href="?module=lists&site_id='.$GLOBALS['site_id'].'&action=down&ind='.$d['ind'].'" title="{%langstr:down%}" ><img src="/cms/backend/gui/images/new/down.png" /></a>

              &nbsp; <a href="?module='.$_GET['module'].'&site_id='.$_GET['site_id'].'&action=delete_list&list_id='.$d['list_id'].'" onclick="return confirm(\'Are You sure to delete?\')" title="{%langstr:col_del%}"><img src="/cms/backend/gui/images/new/delete.png" /></a> &nbsp;

              <a href="#" onclick="openDialog(\'?module=lists&site_id='.$GLOBALS['site_id'].'&action=edit_list_collection&list_id='.$d['list_id'].'\', 400, 550, 1, 0); return false;" title="{%langstr:col_edit%}" ><img src="/cms/backend/gui/images/new/edit.png" /></a>

              &nbsp;&nbsp;';
      }
      echo '</td></tr>';

    }else
    {


      $GLOBALS['page_id'] = $d['page_id'];
      $GLOBALS['pagedev_id'] = sqlQueryValue('SELECT pagedev_id FROM ' . $site_id . '_pagesdev WHERE page_id = ' . $GLOBALS['page_id'] . ' ORDER BY lastmod DESC LIMIT 1');
      $pageData = sqlQueryRow("SELECT * FROM " . $site_id . "_pagesdev LEFT JOIN " . $site_id . "_languages ON " . $site_id . "_languages.language_id=" . $site_id . "_pagesdev.language WHERE " . $site_id . "_pagesdev.pagedev_id='".$GLOBALS['pagedev_id']."'");

      $GLOBALS['pageData'] = $pageData;
      $GLOBALS['copypage'] = $pageData['copypage'];
      $filename = getComponentFilePath($d['componenttype']);
      if(file_exists($filename))
      {
        echo '<tr class="list">
              <td width="100%">
                <a href="?module=lists&site_id='.$GLOBALS['site_id'].'&action=open_close&list_id='.$d['list_id'].'"><img src="/cms/backend/gui/images/'.(isset($_SESSION['open_close_lists'][$d['list_id']]) ? 'minus' : 'plus').'.gif" /></a> '.$d['title'].'</td>';

        echo '<td nowrap="nowrap">';

        if ($GLOBALS['user_site_permissions_managelists'])
        {
          echo '<input type="checkbox" '.($d['visible'] ? 'checked="checked"' : '' ).' onclick="javascript: window.location.href =\'?module=lists&site_id='.$GLOBALS['site_id'].'&action=change_visibility&list_id='.$d['list_id'].'\';" />
                <a href="?module=lists&site_id='.$GLOBALS['site_id'].'&action=up&ind='.$d['ind'].'" title="{%langstr:up%}" ><img src="/cms/backend/gui/images/new/up.png" /></a>

                <a href="?module=lists&site_id='.$GLOBALS['site_id'].'&action=down&ind='.$d['ind'].'" title="{%langstr:down%}" ><img src="/cms/backend/gui/images/new/down.png" /></a>

                &nbsp; <a href="?module='.$_GET['module'].'&site_id='.$_GET['site_id'].'&action=delete_list&list_id='.$d['list_id'].'" onclick="return confirm(\'Are You sure to delete?\')" title="{%langstr:col_del%}"><img src="/cms/backend/gui/images/new/delete.png" /></a> &nbsp;

                <a href="#" onclick="openDialog(\'?module=lists&site_id='.$GLOBALS['site_id'].'&action=edit_list&list_id='.$d['list_id'].'\', 400, 550, 1, 0); return false;" title="{%langstr:col_edit%}" ><img src="/cms/backend/gui/images/new/edit.png" /></a>
                ';
        }

        echo '<a href="#" onclick="openDialog(\'?module=pages&site_id='.$site_id.'&action=components_visual_frame&id='.$d['page_id'].'&nopageid=1\', screen.availWidth, screen.availHeight-25, 1 , 1);"><img src="/cms/backend/gui/rightMenu/sitemap_visual_edit.gif" /></a>&nbsp;

              <a href="?module=pages&site_id='.$site_id.'&action=page_redirect&id='.$d['page_id'].'"  target="_blank"><img src="/cms/backend/gui/images/new/view.png" /></a> &nbsp;&nbsp;';

        echo '</td>';

        echo '</td></tr>';

        if (isset($_SESSION['open_close_lists'][$d['list_id']]))
        {

          $GLOBALS['component_name'] = $d['componentname'];
        ob_start();
        $obj = new $d['componenttype']($d['componentname']);
        $obj->componentParamStr = ':noborder "1"';
        addDefaultProperties($obj);
        $lang = getCurrentLanguage();
        $obj->lang = $lang['shortname'];

        if((!$_GET['formatpageforsearch'])or(!$obj->getProperty('nosearch')))
        {
          $obj->ListDesign();
          $html = ob_get_contents();

        }
        ob_end_clean();


          echo '<tr><td '.($GLOBALS['user_site_permissions_managelists'] ? 'colspan="2"' : '').' style="padding: 10px"><div class="graphicaleditdiv" id="div_'.$d['componentname'].'" name="div_'.$d['componentname'].'">'.$html.'</div></td></tr>';
        }
      }
    }
  }

  echo '</table>';

}
