<?

function DisplayToolbar()
{
  global $docStrings;
  require("class.toolbar.php");
  $Toolbar = new Toolbar();
  $Toolbar->AddLink("View Log", "view_log.gif", "javascript:window.location='?module=admin&action=log'", "View activity log");
  //$Toolbar->AddLink("Backup Database", "backup_db.gif", "javascript:window.location='?module=admin&action=database'", "Backup database");
  //$Toolbar->AddLink("Restore Database", "restore_db.gif", "javascript:window.location='?module=admin&action=restoredatabase'", "Restore database from backup");
  $Toolbar->AddLink("Add/Remove Components", "components.gif", "javascript:window.location='?module=admin&action=components'", "Install and uninstall components");
  $Toolbar->AddButton("Access Permissions", "edit_permissions.gif", "javascript:openDialog('?module=permissions&close=1', 400, 380);", "Edit global access permissions");
  $docStrings['toolbar'] = $Toolbar->output();
}


if($GLOBALS['currentUserSiteID'])
{
  Header("Location: index.php?site_id=" . $GLOBALS['currentUserSiteID'] . "&module=" . defaultSiteModule);
  Die;
}


$action = $_GET['action'];
$perm_accessadmin = CheckPermission(5);

switch ($action) {

  case "dummy":
    $docTemplate = "dlg_form.htm";
    $docTitle = "Test Form";
    $docStrings['frame'] = "?module=test&action=form_content";
    break;

  case "components":
    if($perm_accessadmin)
    {
      DisplayToolbar();
    }
    $docTemplate = "main.htm";
    $docTitle = "Components";
    $docScripts['body'] = "admin.components.php";
    break;

  case "delcomponent":
    $docScripts['exec'] = "admin.exec.php";
    Header("Location: ?module=admin&action=components");
    break;

  case "log":
    if($perm_accessadmin)
    {
      DisplayToolbar();
    }
    $docTemplate = "main.htm";
    $docTitle = "System Log";
    $docScripts['body'] = "admin.log.php";
    break;

  case "database":
    if($perm_accessadmin)
    {
      DisplayToolbar();
    }
    $docTemplate = "main.htm";
    $docTitle = "Backup/Restore Database";
    $docScripts['body'] = "admin.backup.php";
    break;

  case "uploadcomponent":
    $docTitle = "Add Component";
    $docTemplate = "form.htm";
    $docScripts['body'] = "admin.uploadcomponent.php";
    break;

  case "savecomponent":
    $docScripts['exec'] = "admin.savecomponent.php";
    if ($GLOBALS['close'] == 'no') {
      $docTemplate = "form.htm";
      $docScripts['body'] = "admin.components.php";
    } else {
      $docTemplate = "close.htm";
    }
    break;

  case "backupdatabase":
    $docScripts['exec'] = "admin.exec.php";
    break;

  case "restoredatabase":
    $docScripts['exec'] = "admin.exec.php";
    Header("Location: ?module=admin");
    break;



  default:

    if($perm_accessadmin)
    {
      DisplayToolbar();
    }

    $docTemplate = "main.htm";
    $docTitle = "Administration";
    $docScripts['body'] = "admin.default.php";
    break;
}

?>
