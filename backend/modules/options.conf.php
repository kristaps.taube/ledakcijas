<?

$GLOBALS['site_id'] = $_GET["site_id"];
$site_id = $_GET["site_id"];
$page_id = $_GET['page_id'];

$action = $_GET["action"];
$GLOBALS['tabs'] = 'site';

unset($perm_modsite);
if(($site_id == $GLOBALS["currentUserSiteID"])or(!$GLOBALS["currentUserSiteID"]))
  $perm_modsite = CheckPermission(6, $site_id);

$GLOBALS['PageEncoding'] = PageEncoding(0, $site_id);
$GLOBALS['maincharset']  = PageEncoding(0, $site_id, false);

if(!$perm_modsite)
{
  echo "Permission Denied!";
  AccessDenied(True);
}

$action = $_GET["action"];
require($GLOBALS['cfgDirRoot']."library/"."class.toolbar.php");

switch ($action) {
  case 'tree':
    $docTemplate = "nothing.htm";
    $docScripts['body'] = "options.tree.php";
    break;

  case 'panel':
    $docTemplate = "nothing.htm";
    $docScripts['body'] = "options.panel.php";
    break;

  case 'delcat':
    if ($_SESSION['userdata']['username'] == 'Admin')
    {
      $id = intval($_COOKIE['csdtree']);
      if ($id)
        deleteOption($id);

    }
    header('Location: ?module=options&site_id='.$GLOBALS['site_id']);
    exit();

  case 'show_advanced':

	if ($_SESSION['userdata']['username'] == 'Admin'){
    $_SESSION['options_show_advanced'] ^= 1;
  }

  default:
    if ($_SESSION['userdata']['username'] == 'Admin')
    {
      require($GLOBALS['cfgDirRoot']."library/"."class.toolbar2.php");
      $Toolbar = new Toolbar2();

      $Toolbar->AddSpacer();

      if ($_SESSION['options_show_advanced'])
      {
        $Toolbar->AddButtonImage("advanced", 'advanced_hide', "{%langstr:opt_advanced%}", "?module=options&site_id=".$GLOBALS['site_id']."&path=".$GLOBALS['path']."&action=show_advanced", "", 31, "{%langstr:opt_advanced%}");
      }
      else
      {
        $Toolbar->AddButtonImage("advanced", 'advanced', "{%langstr:opt_advanced%}", "?module=options&site_id=".$GLOBALS['site_id']."&path=".$GLOBALS['path']."&action=show_advanced", "", 31, "{%langstr:opt_advanced%}");
      }
      $Toolbar->AddButtonImage("delete", 'button_cancel', "{%langstr:opt_delete_cat%}", "?module=options&site_id=".$GLOBALS['site_id']."&path=".$GLOBALS['path']."&action=delcat", "return confirm('{%langstr:opt_really_del_cat%}')", 31, "{%langstr:opt_delete_cat%}");

      $docStrings['toolbar'] =  $Toolbar->output();
    }

    $docTemplate = "main.htm";
    $docTitle = "Options";

    $docScripts['body'] = "options.default.php";

    break;

}
