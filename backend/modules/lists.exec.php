<?


  require_once ($GLOBALS['cfgDirRoot']."library/".'func.search.php');

  $site_id = $GLOBALS['site_id'];

  $perm_managelists = $GLOBALS['user_site_permissions_managelists'];
  
  if(((($action == 'addlist')and($perm_managelists)))or((($action == 'modlist')and($perm_managelists)))){


    if (isset($_POST['list_id']) && !empty($_POST['list_id'])){ //edit
      sqlQuery("UPDATE lists SET visible = ".($_POST['visible'] == 'on' ? 1 : 0).",
                                           page_id = ".$_POST['page_id'].",
                                           title = '".$_POST['title']."',
                                           componentname = '".$_POST['componentname']."',
                                           componenttype =  '".$_POST['componenttype']."',
                                           collection_id = '".$_POST['collection_id']."',
                                           show_in_menu = ".($_POST['show_in_menu'] == 'on' ? 1 : 0)."
                       WHERE list_id =  ".$_POST['list_id']);
    }else{ //create new
      $ind = sqlQueryValue("SELECT MAX(ind) FROM lists");
      #sqlQuery("INSERT INTO lists VALUES (NULL, $ind+1, ".($_POST['visible'] == 'on' ? 1 : 0).", ".intval($_POST['page_id']).", '".$_POST['title']."', '".$_POST['componentname']."', '".$_POST['componenttype']."', ".$site_id.", '".$_POST['collection_id']."', ".($_POST['show_in_menu'] == 'on' ? 1 : 0)." )");
        DB::Insert("lists", array(
            "ind" => $ind+1,
            "visible" => $_POST['visible'] == 'on' ? 1 : 0,
            "page_id" => (int)$_POST['page_id'],
            "title" => $_POST['title'],
            "componentname" => $_POST['componentname'],
            "componenttype" => $_POST['componenttype'],
            "site_id" => $site_id,
            "collection_id" => $_POST['collection_id'] ? $_POST['collection_id'] : 0,
            "show_in_menu" => $_POST['show_in_menu'] ? 1 : 0,
            ));
        }
  }


  /* S : DELETE LIST */
  if ($action=='delete_list')
  {
     sqlQuery("DELETE FROM lists WHERE list_id = ".intval($_GET['list_id']));
  }
  /* B : DELETE LIST */

  /* S : UP */
  if ($action == 'up')
  {
    sqlQuery("DELETE FROM lists WHERE ind='-1'");
    sqlQuery("UPDATE lists SET ind='-1' WHERE ind = ".$_GET['ind']);
    sqlQuery("UPDATE lists SET ind=".$_GET['ind']." WHERE ind = ".($_GET['ind']-1));
    sqlQuery("UPDATE lists SET ind = ".($_GET['ind']-1)." WHERE ind = '-1'");
  }
  /* B : UP */

  /* S : DOWN */
  if ($action == 'down')
  {
    sqlQuery("DELETE FROM lists WHERE ind='-1'");
    sqlQuery("UPDATE lists SET ind='-1' WHERE ind = ".$_GET['ind']);
    sqlQuery("UPDATE lists SET ind=".$_GET['ind']." WHERE ind = ".($_GET['ind']+1));
    sqlQuery("UPDATE lists SET ind = ".($_GET['ind']+1)." WHERE ind = '-1'");
  }
  /* B : DOWN */

  /* S : OPEN CLOSE */
  if ($action == 'open_close')
  {
    if (!isset($_SESSION['open_close_lists']))
    {
      $_SESSION['open_close_lists'] = array();
    }

    if (isset($_SESSION['open_close_lists'][intval($_GET['list_id'])]))
    {
      unset($_SESSION['open_close_lists'][intval($_GET['list_id'])]);
    }
    else
    {
      $_SESSION['open_close_lists'][intval($_GET['list_id'])] = 1;
    }
  }
  /* B : OPEN CLOSE */

  /* S : CHANGE VISIBILITY */
  if ($action == 'change_visibility')
  {
    $visibility = sqlQueryValue("SELECT visible FROM lists WHERE list_id = ".intval($_GET['list_id']));
    $visibility = $visibility ? 0 : 1;
    sqlQuery("UPDATE lists SET visible = ".$visibility." WHERE list_id = ".intval($_GET['list_id']));
  }
  /* B : CHANGE VISIBILITY */

