<?

$perm_managewappages = $GLOBALS['perm_managewappages'];

//function for sorting data in tree structure
function Nodes($data, &$newdata, $parent, $level, $justcount, &$goodl, $perms)
{
  $goodlanguage = false;
  $nodecount = 0;
  foreach($data as $key => $row)
  {
    if($parent == $row['parent']){
      $nodecount++;

        $row['prefix'] = str_repeat('<img src="gui/images/1x1.gif" width="16" height="16" border="0" align="absmiddle">', $level) . '<img src="gui/images/1x1.gif" width="16" height="16" border="0" align="absmiddle">&nbsp;';
        $newdata[] = $row;
        $c = count($newdata) - 1;
        $childs = Nodes($data, $newdata, $row['wappage_id'], $level + 1, 0, $g, $perms);
        $goodlanguage = ($g or $goodlanguage);

        if(!$_SESSION['wapexpanded' . $row['wappage_id']])
        {
            $newdata = array_slice($newdata, 0, $c+1);
        }

        /*if($row['language'] != intval($GLOBALS['currentlanguagenum']))
        {
          //$newdata[$c]['page_id'] = 0;
          $newdata[$c]['clicklink'] = "Page in " . sqlQueryValue("SELECT fullname FROM " . $GLOBALS['site_id'] . "_languages WHERE language_id = " . intval($row['language'])) . " (" . $row['name'] . ")";
          if(!$g)
          {
            if($c)
              $newdata = array_slice($newdata, 0, $c);
            else
              $newdata = array();
          }
        }else
        {*/
          /*if ($GLOBALS['perm_managepages'] || in_array($row['page_id'],$perms))
            $newdata[$c]['clicklink'] = "<a href=\"#\" OnClick=\"javascript:openDialog('?module=pages&site_id=" . $GLOBALS['site_id'] . "&action=components_visual_frame&id=".$row['page_id']."', screen.availWidth, screen.availHeight-25, 1 , 1);\">".ShortenString($row['title'],40)."</a>";
          else*/
            $newdata[$c]['clicklink'] = "<a href=\"#\" OnClick=\"javascript:openDialog('?module=wap&site_id=" . $GLOBALS['site_id'] . "&action=components_form&id=".$row['wappagedev_id']."', 800, 600, 1, 1);\">".ShortenString($row['title'],40)."</a>";
            //$newdata[$c]['publish'] = date("Y m d - H:i:s",$row['lastmod']);

            $newdata[$c]['publish'] = '<a href="?module=wap&site_id='.$_GET['site_id'].'&action=publish&wappagedev_id='.$row['wappagedev_id'].'&wappage_id='.$row['wappage_id'].'">Publish</a>';

          $goodlanguage = true;
        //}

        if(($childs)and(isset($newdata[$c]))){
          if($_SESSION['wapexpanded' . $row['wappage_id']])
          {
            $newdata[$c]['prefix'] = '&nbsp;'.str_repeat('<img src="gui/images/1x1.gif" width="16" height="16" border="0" align="absmiddle">', $level) . '<a href="?module=wap&site_id=' . $GLOBALS['site_id'] . '&collapsebranch=' . $newdata[$c]['wappage_id'] . '"><img src="gui/images/minus.gif" width="11" height="11" border="0" align="absmiddle"></a>&nbsp; ';
          }else
          {
            $newdata[$c]['prefix'] = '&nbsp;'.str_repeat('<img src="gui/images/1x1.gif" width="16" height="16" border="0" align="absmiddle">', $level) . '<a href="?module=wap&site_id=' . $GLOBALS['site_id'] . '&expandbranch=' . $newdata[$c]['wappage_id'] . '"><img src="gui/images/plus.gif" width="11" height="11" border="0" align="absmiddle"></a>&nbsp; ';
          }
        }

    }

  }
  $goodl = $goodlanguage;
  return $nodecount;
}

$site_id = $GLOBALS['site_id'];

// Let's try table generator

$cols = Array(

  "preview"     => Array(
    "width"     => "5%",
    "align"     => "center",
    "title"     => "{%langstr:col_prev%}",
    "format"    => "<a href=\"?module=wap&site_id=$site_id&action=page_redirect&id={%wappage_id%}\"  target=\"_blank\"><img src=\"gui/images/ico_view_wap.gif\" width=\"16\" height=\"16\" border=\"0\"></a>&nbsp;&nbsp;&nbsp;<a href=\"?module=wap&site_id=$site_id&action=page_redirect&id={%wappage_id%}&inbrowser=1\"  target=\"_blank\"><img src=\"gui/images/ico_view_glob.gif\" width=\"20\" height=\"16\" border=\"0\"></a>"
  ),

  "visible"       => Array(
    "width"     => "5%",
    "align"     => "center",
    "title"     => "{%langstr:col_vis%}"
  ),

  "enabled"     => Array(
    "width"     => "5%",
    "align"     => "center",
    "title"     => "{%langstr:col_enab%}"
  ),

  "title"        => Array(
    "width"     => "30%",
    "title"     => "{%langstr:title%}",
    "format"    => "{%prefix%}{%scut%}{%link%}{%ishome%}{%clicklink%}"
  ),

  "description" => Array(
    "width"     => "40%",
    "title"     => "{%langstr:col_description%}"
  ),

  "modby" => Array(
    "width"     => "15%",
    "title"     => "{%langstr:modify_by%}"
  ),

  "publish"     => Array(
    "width"     => "5%",
    "align"     => "center",
    "title"     => "{%langstr:col_pub%}"
  ),
 
  "previewdev"     => Array(
    "width"     => "5%",
    "align"     => "center",
    "title"     => ""
  ),

);

if(!$perm_managewappages)
{
  //unset($cols['title']['format']);
}


$javascriptv = ' onClick="javascript:SubmitWapPageVisibleCheckbox(this, \',wappagedev_id,\',' . $site_id . ')"';
$javascripte = ' onClick="javascript:SubmitWapPageEnabledCheckbox(this, \',wappagedev_id,\',' . $site_id . ')"';

if(!$perm_managewappages)
{
  $javascriptv = '';
  $javascripte = '';
}
$checkstylev = 'CONCAT(\'<input type="checkbox" name="cccb"' . $javascriptv . ' checked>\')';
$uncheckstylev = 'CONCAT(\'<input type="checkbox" name="cccb"' . $javascriptv . '>\')';
$checkstylee = 'CONCAT(\'<input type="checkbox" name="cccb"' . $javascripte . ' checked>\')';
$uncheckstylee = 'CONCAT(\'<input type="checkbox" name="cccb"' . $javascripte . '>\')';

$shortcutimage = '<img src="gui/images/shortcut.gif" width="11" height="11" border="0"> ';
$homeimage = '<img src="gui/images/home.gif" width="11" height="11" border="0"> ';
$cardimage = '<img src="gui/images/link.gif" border="0">';

    //get all data from wappagesdev
    $unsorteddata = sqlQueryData("SELECT wappagedev_id, IF(redirect>0, CONCAT('$shortcutimage', '&nbsp;'), '') AS scut, IF(name='index', CONCAT('$homeimage', '&nbsp;'), '') AS ishome, name, IF(is_card>0,CONCAT('$cardimage', '&nbsp;'), '') as link, title, description, lastmod, ELT(visible+1,$uncheckstylev,$checkstylev) AS visible, ELT(w.enabled+1,$uncheckstylee,$checkstylee) AS enabled, parent, wappage_id, username as modby FROM " . $site_id . "_wappagesdev as w, users as u WHERE modby = u.user_id ORDER BY ind");

    //get wappage_id array
    $ids = sqlQueryData("select distinct wappage_id, ind from ".$site_id."_wappagesdev ORDER BY ind");
    
    //get keys of last modified dev pages
    if (count($ids))
    {
        $keys = array();
        for ($i=0; $i<=count($ids)-1; $i++)
        {
            $max = 0;
            foreach ($unsorteddata as $key=>$row)
            {
                if ($row['wappage_id']==$ids[$i]['wappage_id'])
                {
                    if($row['lastmod']>$max)
                    {
                        $max = $row['lastmod'];
                        $keys[$i] = $key;
                        $lastmoddev[$row['wappage_id']]['lastmod'] = $max;
                    }
                        
                }
            }
        }
    }

    //drop unnecessary rows
    foreach ($unsorteddata as $key=>$row)
        if (!in_array($key,$keys))
            unset($unsorteddata[$key]);
    $lastmod_data = sqlQueryData("SELECT wappage_id,lastmod FROM ".$site_id."_wappages");
    foreach ($lastmod_data as $row)
    {
        $lastmod[$row['wappage_id']]['lastmod'] = $row['lastmod'];
    }
    
    /*
    //print_r($lastmod);
    foreach ($lastmod as $row)
    {
        if ($row['lastmod']==$lastmoddev[$row['wappage_id']]['lastmod'])
            echo $row['wappage_id'];
    }
    */


$g = false;
Nodes($unsorteddata, $data, 0, 0, 0, $g, $perms);

foreach ($data as $key=>$row)
{
    if ($row['lastmod']==$lastmod[$row['wappage_id']]['lastmod'])
    {
        unset($data[$key]['publish']);
        //unset($data[$key]['previewdev']);
    }
    else
    {
        $data[$key]['previewdev'] = "<a href=\"?module=wap&site_id=$site_id&action=page_redirect&id=".$row['wappage_id']."&wappagedev_id=".$row['wappagedev_id']."&inbrowser=2\"  target=\"_blank\"><img src=\"gui/images/ico_view.gif\" width=\"16\" height=\"16\" border=\"0\"></a>";
    }
}

require($GLOBALS['cfgDirRoot']."library/"."class.datagrid.php");

$Table = new DataGrid();
$Table->cols = $cols;
$Table->data = $data;
$Table->footer = false;
$Table->rowformat = " id=\"tablerow{%wappagedev_id%}\" onClick=\"javascript:HighLightTR(this, {%wappagedev_id%});
                         if(SelectedRowID){ EnableToolbarButtons(che); }else { DisableToolbarButtons(che); }
                         \"";
echo $Table->output();

if($_GET['accessdenied'])
{
  AccessDenied(false, true);
  echo '
  <script language="JavaScript" type="text/javascript">
  /*<![CDATA[*/
  window.document.location.href = "?module=wap&site_id=' . $GLOBALS['site_id'] . '&selrow=' . $_GET['selrow'] . '" 
  /*]]>*/
  </script>
  ';

}

echo '<script language="JavaScript" type="text/javascript">
/*<![CDATA[*/
che = new Array("add2", "properties", "perms", "sedit", "visedit", "strings", "up", "down", "del", "history");
DisableToolbarButtons(che);
/*]]>*/
</script>';

if($_GET['selrow'])
{
  echo '<script language="JavaScript">
        <!--
          var tablerow' . $_GET['selrow'] . ' = document.getElementById("tablerow' . $_GET['selrow'] . '");
          if(typeof(tablerow' . $_GET['selrow'] . ')!= "undefined" && tablerow' . $_GET['selrow'] . ')
          {
            preEl = tablerow' . $_GET['selrow'] . ';
            ChangeTextColor(tablerow' . $_GET['selrow'] . ',\'tableRowSel\');
            SelectedRowID = ' . $_GET['selrow'] . ';
            if(SelectedRowID){ EnableToolbarButtons(che); }else { DisableToolbarButtons(che); }
          }
        <!---->
        </script>';
}

?>
