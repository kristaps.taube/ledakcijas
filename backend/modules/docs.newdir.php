<?

$perm_managedocuments = $GLOBALS['perm_managedocuments'];
if(!$perm_managedocuments)
  AccessDenied(True);

$form_data = Array(

  "name"    => Array(
    "label"     => "Directory Name:",
    "size"      => "30",
    "type"      => "string"
  ),

  "description" => Array(
    "label"     => "Description:",
    "type"      => "html",
    "rows"      => "4",
    "cols"      => "30"
  )

);


if ($_GET['dironly']) $param = '&dironly=1';
else $param ='';

if ($_GET['dironly']) $action = "dirlist";
else $action = "list";

require($GLOBALS['cfgDirRoot']."library/"."class.formgen.php");
$FormGen = new FormGen();
$FormGen->action = "?module=docs&action=mkdir&site_id=".$GLOBALS['site_id']."&parent_dir=".$_SESSION['parent_dir'].$param."&view=".$GLOBALS['view'].($GLOBALS['close'] ? "&close=".$GLOBALS['close'] : "");
$FormGen->title = "Make Directory";
$FormGen->cancel = ($GLOBALS['close'] ? "?module=docs&action=".$action."&site_id=".$GLOBALS['site_id']."&parent_dir=".$_SESSION['parent_dir']."&view=".$GLOBALS['view'] : 'close');;
$FormGen->properties = $form_data;
$FormGen->buttons = true;
echo $FormGen->output();

echo '
    <script language="JavaScript">
        try {
        window.parent.Form.Ok.disabled = true;
        } catch (e) {}
    </script>
';
?>
