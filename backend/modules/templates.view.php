<?


// Fill some values
  $id = $_GET["id"];
  $action = $_GET['action'];
  if($_POST['action'])
    $action = $_POST['action'];
  $create = ($action == 'createview');
  $site_id = $GLOBALS['site_id'];
  $template_id = $_GET['template_id'];

  echo '
  <script language="JavaScript" type="text/javascript">
  function ShowDivs()
  {
    var checked;
    var lastchecked;
    lastchecked = false;
    for(f=0;f<20;f++)
    {
      eval("checked = document.Form.uselevel"+f+".checked;");
      if(checked || lastchecked || f == 0)
      {
        eval("leveldiv"+f+".style.display=\'inline\'");
      }else
      {
        eval("leveldiv"+f+".style.display=\'none\'");
      }
      lastchecked = checked;
    }
    lastchecked = false;
    for(f=0;f<20;f++)
    {
      eval("checked = document.Form.useparent"+f+".checked;");
      if(checked || lastchecked || f == 0)
      {
        eval("parentdiv"+f+".style.display=\'inline\'");
      }else
      {
        eval("parentdiv"+f+".style.display=\'none\'");
      }
      lastchecked = checked;
    }
    lastchecked = false;
    for(f=0;f<20;f++)
    {
      eval("checked = document.Form.uselanguage"+f+".checked;");
      if(checked || lastchecked || f == 0)
      {
        eval("languagediv"+f+".style.display=\'inline\'");
      }else
      {
        eval("languagediv"+f+".style.display=\'none\'");
      }
      lastchecked = checked;
    }

  }

  </script>
  ';


  require($GLOBALS['cfgDirRoot']."library/"."class.formgen.php");
  $FormGen = new FormGen();

  if($create)
    $title = "{%langstr:create_template_view%}";
  else
    $title = "{%langstr:edit_template_view%}";
  $title = $FormGen->get_title($title, '', false, false);
  echo '<table width="100%">';
  echo '<form name="Form" action="?module='.$GLOBALS['module'].'&site_id='.$site_id.'&action='.iif($create,'saveview','updateview').'&template_id='.$template_id.'" method="POST">';
  echo $title;

  //obtain values if edition not creating
  $values = array();
  if(!$create)
  {
    $view_id = abs(intval($_GET['view_id']));
    echo '<input type="hidden" name="view_id" value="'.$view_id.'">';
    $values = sqlQueryRow("SELECT name, parent, description, levels, parents, languages FROM " . $site_id . "_views WHERE view_id = $view_id");
    $levels = explode(":", $values['levels']);
    $parents = explode(":", $values['parents']);
    foreach($parents as $key=>$row)
    {
      $parents[$key] = explode("|", $parents[$key]);
    }
    $languages = explode(":", $values['languages']);
  }


  echo $FormGen->get_row('{%langstr:col_name%}:', '<input name="name" size="40" value="'.$values['name'].'" />', false);
  echo $FormGen->get_row('{%langstr:col_description%}:', '<textarea name="description" rows="4" cols="25">'.$values['description'].'</textarea>', false);

  $allviews = sqlQueryData("SELECT view_id AS page_id, name, parent,name AS title FROM " . $site_id . "_views ORDER BY ind");

  if ($view_id)
  {
    foreach ($allviews as $vkey => $vrow)
    {
      if ($vrow['page_id'] == $view_id)
      {
        unset($allviews[$vkey]);
      }
    }
  }

  ListNodes($allviews, $comboviews, 0, 0);
  $comboviews[0] = "0:Template";
  $input = new InputControl();
  $input->property = Array("lookup" => $comboviews, "value" => $values['parent']);
  $input->name = 'parent';
  echo $FormGen->get_row('{%langstr:parent%}:', $input->fieldlist());


  echo '</table>';


  $title = "{%langstr:apply_view_any_levels%}:";
  $title = $FormGen->get_title($title, '', false, false);
  echo '<table width="100%">';
  echo $title;
  echo '</table>';

  for($f = 0; $f < 20; $f++ )
  {
    echo '
      <div id="leveldiv'.$f.'" style="display:none;">
      <input type="checkbox" '.iif($levels[$f]!='','checked="checked" ','').'name="uselevel'.$f.'" id="uselevel'.$f.'" onclick="javascript:ShowDivs()" />
      {%langstr:level%}: <input name="level'.$f.'" size="5" value="'.iif($levels[$f],$levels[$f],"").'"/>
      <br /></div>';
  }

  $allpages = sqlQueryData("SELECT page_id,name,parent,title FROM " . $site_id . "_pages ORDER BY ind");
  ListNodes($allpages, $combopages2, 0, 0);
  $combopages2[0] = "0:None";
  $input = new InputControl();

  $title = "{%langstr:apply_view_subpages%}:";
  $title = $FormGen->get_title($title, '', false, false);
  echo '<table width="100%">';
  echo $title;
  echo '</table>';
  for($f = 0; $f < 20; $f++ )
  {
    $input->property = Array("lookup" => $combopages2, "value" => $parents[$f][0], "width" => "200px");
    $input->name = 'parent'.$f;
    echo '
      <div id="parentdiv'.$f.'" style="display:none;">
      <input type="checkbox" '.iif($parents[$f][0]!='','checked="checked" ','').'name="useparent'.$f.'" id="useparent'.$f.'" onclick="javascript:ShowDivs()" />
      {%langstr:parent%}: '.$input->fieldList().' {%langstr:immediate_parent%} <input type="checkbox" '.iif($parents[$f][1],'checked="checked" ','').'name="useimmediateparent'.$f.'" />
      <br /></div>';
  }

  $alllanguages = sqlQueryData("SELECT language_id, fullname FROM " . $site_id . "_languages");
  $combolanguages [] = "0:None";
  if($alllanguages == null)
  {

  }else
  {
    foreach ($alllanguages as $row) {
      $combolanguages [] = $row['language_id'] . ":" . $row['fullname'];
    }
  }
  $input = new InputControl();
  $title = "{%langstr:apply_view_pages_languages%}:";
  $title = $FormGen->get_title($title, '', false, false);
  echo '<table width="100%">';
  echo $title;
  echo '</table>';
  for($f = 0; $f < 20; $f++ )
  {
    $input->property = Array("lookup" => $combolanguages, "value" => $languages[$f], "width" => "200px");
    $input->name = 'language'.$f;
    echo '
      <div id="languagediv'.$f.'" style="display:none;">
      <input type="checkbox" '.iif($languages[$f]!='','checked="checked" ','').'name="uselanguage'.$f.'" id="uselanguage'.$f.'" onclick="javascript:ShowDivs()" />
      {%langstr:language%}: '.$input->fieldList().'
      <br /></div>';
  }

  echo '<table width="100%" border="0" cellpadding="4" cellspacing="4">
<tr><td>&nbsp;</td></tr>
<tr>
  <td align="right">
<input ID="OKButton" class="formButton" type="submit" value="&nbsp;&nbsp;{%langstr:ok%}&nbsp;&nbsp;">&nbsp;
<input class="formButton" type="button" value="{%langstr:cancel%}" OnClick="window.close();">&nbsp;
<input class="formButton" type="reset" value="{%langstr:reset%}">
  </td>
</tr>
</table>';
  echo '</form>';
  echo '
  <script language="JavaScript" type="text/javascript">
  ShowDivs();
  </script>
  ';

?>
