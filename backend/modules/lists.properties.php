<?
// Define form
$site_id = $GLOBALS['site_id'];


$pages = sqlQueryData("SELECT page_id,name,parent,title FROM " . $site_id . "_pages WHERE page_id <> '$id' ORDER BY ind");
ListNodes($pages, $combopages, 0, 0);
$allpages = sqlQueryData("SELECT page_id,name,parent,title FROM " . $site_id . "_pages ORDER BY ind");
ListNodes($allpages, $combopages2, 0, 0);
$combopages2[0] = "0:None";


$form_data = Array(
  "list_id"     => Array(
    "label"     => "IDs:",
    "type"      => "hidden"
  ),


  "ind"         => Array(
    "noshow"    => true
  ),

  "title"        => Array(
    "label"     => "{%langstr:title%}:",
    "type"      => "str"
  ),

  "page_id"      => Array(
    "label"     => "{%langstr:stats_page%}:",
    "type"      => "list",
    "lookup"    => $combopages
  ),

  "componentname"  => Array(
    "label"     => "{%langstr:component_name%}:",
    "type"      => "scriptdialog",
    "onclick"   => "      var a = Array(document.Form.componentname.value, document.Form.componenttype.value, document.Form.page_id.value);
    											openDlg('?module=dialogs&action=viewpagecomponent&site_id=" . $_GET['site_id'] . "', a, {w: 400, h: 450}, function(val){

                          	if (val) {
                              var bothstrings = val.split(':');
                              parent.document.Form.componentname.value=bothstrings[0];
                            	parent.document.Form.componenttype.value=bothstrings[1];
                            }
														
													});
                             "
  ),

  "componenttype" => Array(
    "label"     => "{%langstr:component_type%}:",
    "type"      => "str"
  ),

  "visible"     => Array(
    "label"     => "{%langstr:visible%}:",
    "type"      => "check",
    "value"         => "1"
  )

);

// Fill some values
  $action = $_GET['action'];
  $create = ($action == 'new_list');

  if(!$create)
  {
    $list = sqlQueryRow("SELECT * FROM lists WHERE list_id = ".intval($_GET['list_id']));
    $form_data['title']['value'] = $list['title'];
    $form_data['componentname']['value'] = $list['componentname'];
    $form_data['componenttype']['value'] = $list['componenttype'];
    $form_data['visible']['value'] = $list['visible'];
    $form_data['page_id']['value'] = $list['page_id'];
    $form_data['list_id']['value'] = $list['list_id'];
  }


// Use FormGen to bring this out

  require($GLOBALS['cfgDirRoot']."library/"."class.formgen.php");
  $FormGen = new FormGen();
  if(!$create)
  {
    $FormGen->action = "?module=".$GLOBALS['module']."&site_id=".$site_id."&action=addlist";
  }else{
    $FormGen->action = "?module=".$GLOBALS['module']."&site_id=".$site_id."&action=modlist";
  }

  $FormGen->cancel = "close";
  if(!$create)
  {
    $FormGen->title = "{%langstr:page_properties%} : " . ShortenString($row['title'],40);
  }else{
    $FormGen->title = "{%langstr:create_list%}";
  }
  $FormGen->properties = $form_data;
  $FormGen->buttons = true;
  echo $FormGen->output();

?>
