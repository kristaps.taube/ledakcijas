<?
$action = $_GET['action'];
if($GLOBALS['currentUserSiteID'] && $action != 'ajaxdesign')
{
  Header("Location: index.php?site_id=" . $GLOBALS['currentUserSiteID'] . "&module=" . defaultSiteModule);
  Die;
}


$perm_accessadmin = CheckPermission(5);

switch ($action) {

  case "ajaxdesign":
    $docTemplate = "blank.html";
    $docScripts['body'] = "components.ajaxdesigncomponent.php";
    break;

  case "setcomponentenabled":
    $docScripts['exec'] = "admin.exec.php";
    Header("Location: ?module=components");
    break;

  case "unsetcomponentenabled":
    $docScripts['exec'] = "admin.exec.php";
    Header("Location: ?module=components");
    break;

  case "setcomponentenabledfile":
    $docScripts['exec'] = "admin.exec.php";
    Header("Location: ?module=components");
    break;

  case "delcomponent":
    $docScripts['exec'] = "admin.exec.php";
    Header("Location: ?module=components&action=components");
    break;

  case "uploadcomponent":
    $docTitle = "{%langstr:add_component%}";
    $docTemplate = "form.htm";
    $GLOBALS['resizeauto'] = 1;
    $docScripts['body'] = "admin.uploadcomponent.php";
    break;

  case "installcomponentlist":
    $docTitle = "{%langstr:install_existing%}";
    $docTemplate = "form.htm";
    $GLOBALS['resizeauto'] = 0;
    $docScripts['body'] = "admin.installcomponentlist.php";
    break;

  case 'saveinstallcomponents':
    $docScripts['exec'] = "admin.saveinstallcomponents.php";
    $docTemplate = "close.htm";
    $GLOBALS['resizeauto'] = 1;
    break;

  case 'changecomponent':
    $docTitle = "{%langstr:change_category%}";
    $docTemplate = "form.htm";
    $GLOBALS['resizeauto'] = 1;
    $docScripts['body'] = "admin.changecategory.php";
    break;

  case "savecomponent":
    $docScripts['exec'] = "admin.savecomponent.php";
    if ($GLOBALS['close'] == 'no') {
      $docTemplate = "form.htm";
      $GLOBALS['resizeauto'] = 1;
      $docScripts['body'] = "admin.components.php";
    } else {
      $docTemplate = "close.htm";
    }
    break;

  case 'savechangedcomponent':
    $docScripts['exec'] = "admin.exec.php";
    if ($GLOBALS['close'] == 'no') {
      $docTemplate = "form.htm";
      $GLOBALS['resizeauto'] = 1;
      $docScripts['body'] = "admin.components.php";
    } else {
      $docTemplate = "close.htm";
    }
    break;
  
  case 'addcategory':
    $docTitle = "{%langstr:add_category%}";
    $docTemplate = "form.htm";
    $GLOBALS['resizeauto'] = 1;
    $docScripts['body'] = "admin.componentcategory.php";
    break;

  case 'delcategory':
    $docTitle = "{%langstr:add_category%}";
    $docTemplate = "close.htm";
    $GLOBALS['resizeauto'] = 1;
    $docScripts['body'] = "admin.delcomponentcategory.php";
    break;
    
  case "saveaddcategory":
    $docScripts['exec'] = "admin.savecomponentcategory.php";
    if ($GLOBALS['close'] == 'no') {
      $docTemplate = "form.htm";
      $GLOBALS['resizeauto'] = 1;
      $docScripts['body'] = "admin.savecomponentcategory.php";
    } else {
      $docTemplate = "close.htm";
    }
    break;


  default:

    if($perm_accessadmin)
    {
//      DisplayToolbar();
    }
    $docTemplate = "main.htm";
    $docTitle = "{%langstr:components%}";
    $docScripts['body'] = "admin.components.php";
    break;

}

?>
