<?

  $site_id = $_GET['site_id'];
  $GLOBALS['site_id'] = $site_id;
  $page_id = $_GET['page_id'];
  $GLOBALS['page_id'] = $page_id;
  $copypage = sqlQueryValue("SELECT copypage FROM ".$site_id."_pages WHERE page_id=$page_id");
  $GLOBALS['copypage'] = $copypage;

  $pagedev_id = sqlQueryValue("SELECT pagedev_id FROM ".$site_id."_pagesdev WHERE page_id=".$page_id." ORDER BY lastmod DESC LIMIT 1");
  $GLOBALS['pagedev_id'] = $pagedev_id;

  $name = $_GET['component_name'];
  $type = $_GET['component_type'];
  $method = $_GET['method_name'];
  include_once(getComponentFilePath($type));
  $obj = new $type($name);
  $lang = getCurrentLanguage();
  if($lang){
      $obj->lang = $lang['shortname'];
  }
  $obj->dev = true;
  $obj->componentParamStr = stripslashes($_GET['paramstr']);
  $obj->$method();

  //update page time if parameter 'nochangetime' not set
  if($_GET['notime'])
    $_GET['nochngetime'] = $_GET['notime'];
  if ((!isset($_GET['nochngetime']) && $_GET['refresh']) || (isset($_GET['nochngetime']) && $_GET['nochngetime'] == 0))
  {
      //update modification date and user
      //$obj->method(); could change component contents
      $modby = $GLOBALS['currentUserID'];
      sqlQuery("UPDATE ".$site_id."_pagesdev SET lastmod='".time()."', modby=$modby WHERE pagedev_id=$pagedev_id");
      if (CheckPermission(31, $site_id, $page_id) || CheckPermission(32, $site_id))
      {
        pageautopublish($site_id, $pagedev_id);
      }
  }

  if($_GET['refresh'])
  {
    if(!$_GET['afterrefreshmethod'])
    {
      $newDiv = outputGraphicalComponent($hascontent, $type, $name, $page_id, 1, $obj->componentParamStr, false, $toolbarfunc, false, $pagedev_id);
      $params = getComponentParams($obj->componentParamStr);
      if(!$params['noborder']&&!$params['outborder'])
        $newDiv = borderDiv() . $newDiv;
    }else
    {
      $aftermethod = $_GET['afterrefreshmethod'];
      ob_start();
      $obj->$aftermethod();
      $newDiv = ob_get_contents();
      ob_end_clean();
    }
    $newDiv = parseForJScript($newDiv);
    echo '
    <script language="JavaScript">


<!--

  try
  {
    var win = window.parent.dialogArguments[0];
    '.($_GET['afterrefreshreload'] ? 'win.location=win.location;' : '').'
    var el = window.parent.dialogArguments[1];
    el.innerHTML = "' . $newDiv . '";
    el.set_tb_cnt = "' . IIF($toolbarfunc, ParseForJScript($toolbarfunc), 'null') . '";
    el.style.display = "' . $displaymode . '";
  }catch(e)
  {
  }

  //window.parent.close();

//-->

</script> ';

  }

?>
