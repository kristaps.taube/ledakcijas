<?
$GLOBALS['site_id'] = $_GET['site_id'];
$site_id = $_GET['site_id'];

 //function for sorting data in tree structure
function NodesVisual($data, &$newdata, $parent, $level, $justcount, &$goodl)
{
  $goodlanguage = false;
  $nodecount = 0;
  foreach($data as $key => $row)
  {
    if($parent == $row['parent']){
      $nodecount++;

        $newdata[] = $row;
        $c = count($newdata) - 1;
        $childs = NodesVisual($data, $newdata, $row['wappage_id'], $level + 1, 0, $g);
        $newdata[$c]['children'] = $childs;
        $goodlanguage = ($g or $goodlanguage);

        /*
        if($row['language'] != intval($GLOBALS['currentlanguagenum']))
        {
          //$newdata[$c]['page_id'] = 0;
          $newdata[$c]['clicklink'] = "Page in " . sqlQueryValue("SELECT fullname FROM " . $GLOBALS['site_id'] . "_languages WHERE language_id = " . intval($row['language'])) . " (" . $row['name'] . ")";
          if(!$g)
          {
            if($c)
              $newdata = array_slice($newdata, 0, $c);
            else
              $newdata = array();
          }
        }else
        {*/
          $newdata[$c]['clicklink'] = "?redirect_to_whatever";
          $goodlanguage = true;
        //}

    }

  }
  $goodl = $goodlanguage;
  return $nodecount;
}

function AddTreeNode($tree, $data, &$f, $parentnode)
{
  if($f < count($data))
  {
    $row = $data[$f];
    $row['title'] = ShortenString($row['title'], 30); 
    //$max_id = sqlQueryValue("select max(page_id) from ".$site_id."_pages");
    if ($row['component'])
      {
         $page_id = $row['parent']."&component=".$row['name']."&type=".$row['description'];
      }
    else $page_id = $row['wappagedev_id'];
    $path = "?module=wap&site_id=" . $GLOBALS['site_id'] . "&action=components_form_frame&id=" . $page_id;
    if(!$row['children'])
    {
      if ($row['component'])
        $tree->add_document ($parentnode, $row['title'], $path, "/ftv2doc.gif", $row['wappagedev_id']);
      else
        $tree->add_document ($parentnode, $row['title'], $path, "/ftv2folderclosed.gif", $row['wappagedev_id']);
    }else
    {
      $node = $tree->add_folder ($parentnode, $row['title'], $path, "", "", $row['wappagedev_id']);
      for($i = 0; $i < $row['children']; $i++)
      {
        $f++;
        AddTreeNode($tree, $data, $f, $node);
      }
      return $node;
    }
  }
}

function AddTopNodes($tree, $data, &$f, $root, $children)
{
  for($i = 0; $i < $children; $i++)
  {
    AddTreeNode($tree, $data, $f, $root);
    $f++;
  }
}

  //Obtain page data
  //$unsorteddata = sqlQueryData("SELECT wappagedev_id, name, title, description, visible, enabled, parent, MAX(lastmod), wappage_id FROM " . $site_id . "_wappagesdev GROUP BY wappage_id ORDER BY ind");

  $unsorteddata = sqlQueryData("SELECT wappagedev_id, name, title, description, visible, enabled, parent, lastmod, wappage_id FROM " . $site_id . "_wappagesdev ORDER BY ind");

  //print_r($unsorteddata);

  //get all data from wappagesdev
  //$unsorteddata = sqlQueryData("SELECT wappagedev_id, IF(redirect>0, CONCAT('$shortcutimage', '&nbsp;'), '') AS scut, name, title, description, lastmod, ELT(visible+1,$uncheckstylev,$checkstylev) AS visible, ELT(enabled+1,$uncheckstylee,$checkstylee) AS enabled, parent, wappage_id FROM " . $site_id . "_wappagesdev ORDER BY ind");

  //get wappage_id array
  $ids = sqlQueryData("select distinct wappage_id, ind from ".$site_id."_wappagesdev ORDER BY ind");
    
  //get keys of last modified dev pages
  if (count($ids))
  {
      $keys = array();
      for ($i=0; $i<=count($ids)-1; $i++)
      {
          $max = 0;
          foreach ($unsorteddata as $key=>$row)
          {
              if ($row['wappage_id']==$ids[$i]['wappage_id'])
              {
                  if($row['lastmod']>$max)
                  {
                      $max = $row['lastmod'];
                      $keys[$i] = $key;
                  }                     
              }
          }
      }
  }

  //drop unnecessary rows
  foreach ($unsorteddata as $key=>$row)
    if (!in_array($key,$keys))
      unset($unsorteddata[$key]);


  $g = false;
  $children = NodesVisual($unsorteddata, $data, 0, 0, 0, $g);

  //Create and output tree
  require ($GLOBALS['cfgDirRoot'] . "library/class.tree.php");

  $tree = new Tree($GLOBALS['cfgWebRoot'] . "gui/tree");
  $root  = $tree->open_tree ("{%langstr:site_pages%}", "", "koks2", ".");

  $f = 0;
  AddTopNodes($tree, $data, $f, $root, $children);

  $tree->close_tree();

?>
