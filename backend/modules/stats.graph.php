<?

function explodeAssoc($glue,$str)
{
   $arr=explode($glue,$str);

   $size=count($arr);

   for ($i=0; $i < $size/2; $i++)
       $out[$arr[$i]]=$arr[$i+($size/2)];

   return($out);
};

$colors=array(0=>'FF0000',1=>'00FF00',2=>'0000FF',3=>'FFCC00',4=>'FF00FF');

$day_count_mode = $_GET['day_count_mode'];

$data = explode(';',$_GET['data']);
$page_data[0] = (explodeAssoc(',',$data[0]));
$page_data[1] = (explodeAssoc(',',$data[1]));
$page_data[2] = (explodeAssoc(',',$data[2]));
$page_data[3] = (explodeAssoc(',',$data[3]));
$page_data[4] = (explodeAssoc(',',$data[4]));

$max=0;
if ($_SESSION['period']==1 || $_SESSION['period']==0 || $day_count_mode==1)
    for ($i=0; $i<=4; $i++)
    {
        for($j=0; $j<=23; $j++)
        {
            if ($page_data[$i][$j]>$max) $max = $page_data[$i][$j];
        }
    }

if ($_SESSION['period']==2 || $_SESSION['period']==21 || $day_count_mode==2)
    for ($i=0; $i<=4; $i++)
    {
        for($j=0; $j<=23; $j++)
        {
            if ($page_data[$i][$j]>$max) $max = $page_data[$i][$j];
        }
    }
if ($_SESSION['period']==3 || $_SESSION['period']==31 || $day_count_mode==3)
    if (date("d")<date("d", mktime(0,0,0,date("m"),date("d")+1,date("Y"))))
        $days = date("t",mktime(0,0,0,date("m")-1,date("d"),date("Y"))) - 1;
    else
        $days = date("t") - 1;
    for ($i=0; $i<=4; $i++)
    {
        for($j=0; $j<=$days; $j++)
        {
            if ($page_data[$i][$j]>$max) $max = $page_data[$i][$j];
        }
    }
if ($_SESSION['period']==4 || $day_count_mode==4)
    for ($i=0; $i<=4; $i++)
    {
        for($j=0; $j<=12; $j++)
        {
            if ($page_data[$i][$j]>$max) $max = $page_data[$i][$j];
        }
    }

if ($max <= 5) $step = 0.5;
else if ($max >5 && $max<= 10) $step = 1;
else if ($max >10 && $max<= 20) $step = 2;
else if ($max >20 && $max<= 50) $step = 5;
else if ($max >50 && $max<= 100) $step = 10;
else if ($max >100 && $max<= 500) $step = 50;
else if ($max >500 && $max<= 1000) $step = 100;
else if ($max >1000 && $max<= 5000) $step = 500;
else if ($max >5000 && $max<=10000) $step = 1000;
else if ($max >10000 && $max<=100000) $step = 10000;
else $step = 100000;



// Add values to the graph
$graphValues=array(0,80,23,11,190,245,50,80,111,240,55,0,0,10);



// Define .PNG image
header("Content-type: image/png");
$imgWidth=640;
$imgHeight=270;



// Create image and define colors
$image=imagecreate($imgWidth, $imgHeight);
$colorWhite=imagecolorallocate($image, 255, 255, 255);
$colorGrey=imagecolorallocate($image, 192, 192, 192);

$colorRed=imagecolorallocate($image, 255, 0, 0);
$colorGreen=imagecolorallocate($image, 0, 255, 0);
$colorBlue=imagecolorallocate($image, 0, 0, 255);
$colorYellow=imagecolorallocate($image, 255, 204, 0);
$colorPurple=imagecolorallocate($image, 255, 0, 255);



// Create border around image
imageline($image, 0+20, 0, 0+20, 250, $colorGrey);
imageline($image, 0+20, 0, 600+20, 0, $colorGrey);
imageline($image, 599+20, 0, 599+20, 249, $colorGrey);
imageline($image, 0+20, 249, 599+20, 249, $colorGrey);



// Create grid
if ($_SESSION['period']==1 || $_SESSION['period']==0  || $day_count_mode==1)
{
    for ($i=1; $i<11; $i++){
        imageline($image, 0+20, $i*25, 600+20, $i*25, $colorGrey);
    }
    for ($i=1; $i<24; $i++){
        imageline($image, $i*25+20, 0, $i*25+20, 250, $colorGrey);
    }
}
if ($_SESSION['period']==2 || $_SESSION['period']==21 || $day_count_mode==2)
{
    for ($i=1; $i<11; $i++){
        imageline($image, 0+20, $i*25, 600+20, $i*25, $colorGrey);
    }
    for ($i=1; $i<7; $i++){
        imageline($image, $i*600/7+20, 0, $i*600/7+20, 250, $colorGrey);
    }
}
if ($_SESSION['period']==3 || $_SESSION['period']==31 || $day_count_mode==3)
{
    for ($i=1; $i<11; $i++){
        imageline($image, 0+20, $i*25, 600+20, $i*25, $colorGrey);
    }
    for ($i=1; $i<$days; $i++){
        imageline($image, $i*(600/$days)+20, 0, $i*(600/$days)+20, 250, $colorGrey);
    }
}
if ($_SESSION['period']==4 || $day_count_mode==4)
{
    for ($i=1; $i<11; $i++){
        imageline($image, 0+20, $i*25, 600+20, $i*25, $colorGrey);
    }
    for ($i=1; $i<12; $i++){
        imageline($image, $i*(600/12)+20, 0, $i*(600/12)+20, 250, $colorGrey);
    }
}


if ($_SESSION['period']==1 || $_SESSION['period']==0  || $day_count_mode==1)
{
    for ($i=0; $i<=24; $i++)
        imagestring($image, 2, 0+$i*25+20, 250, $i, $colorBlue);
}
if ($_SESSION['period']==2 || $_SESSION['period']==21 || $day_count_mode==2)
{
    for ($i=8,$j=-1; $i>=0; $i--,$j++)
        imagestring($image, 2, 0+$i*(600/7)+10, 250, date("D",mktime(0,0,0,date("m"),date("d")-$j,date("Y"))), $colorBlue);
}
if ($_SESSION['period']==3 || $day_count_mode==3)
{
    imagestring($image, 1, 0, 253, date("d.m.Y",mktime(0,0,0,date("m")-1,date("d"),date("Y"))), $colorBlue);
    imagestring($image, 1, 590, 253, date("d.m.Y",mktime(0,0,0,date("m"),date("d"),date("Y"))), $colorBlue);
}
if ($_SESSION['period']==31 || $day_count_mode==3)
{
    imagestring($image, 1, 0, 253, date("d.m.Y",mktime(0,0,0,date("m")-2,date("d"),date("Y"))), $colorBlue);
    imagestring($image, 1, 590, 253, date("d.m.Y",mktime(0,0,0,date("m")-1,date("d"),date("Y"))), $colorBlue);
}
if ($_SESSION['period']==4 || $day_count_mode==4)
{
    for ($i=12,$j=0; $i>=0; $i--,$j++)
        imagestring($image, 2, 0+$i*(600/12)+5, 250, date("m.y",mktime(0,0,0,date("m")-$j,date("d"),date("Y"))), $colorBlue);
}
for ($i=1; $i<=10; $i++)
    if (!($step==0.5 && $i%2==1)) imagestring($image, 2, 10, 250 - $i*25, $i*$step, $colorBlue);



// Add in graph values
if ($_SESSION['period']==1 || $_SESSION['period']==0  || $day_count_mode==1)
{
    for ($i=0; $i<24; $i++){
    imageline($image, $i*25+20, (250-$page_data[0][$i]*25/$step), ($i+1)*25+20, (250-$page_data[0][$i+1]*25/$step), $colorRed);
    imageline($image, $i*25+20, (250-$page_data[1][$i]*25/$step), ($i+1)*25+20, (250-$page_data[1][$i+1]*25/$step), $colorGreen);
    imageline($image, $i*25+20, (250-$page_data[2][$i]*25/$step), ($i+1)*25+20, (250-$page_data[2][$i+1]*25/$step), $colorBlue);
    imageline($image, $i*25+20, (250-$page_data[3][$i]*25/$step), ($i+1)*25+20, (250-$page_data[3][$i+1]*25/$step), $colorYellow);
    imageline($image, $i*25+20, (250-$page_data[4][$i]*25/$step), ($i+1)*25+20, (250-$page_data[4][$i+1]*25/$step), $colorPurple);
    }
}

if ($_SESSION['period']==2 || $_SESSION['period']==21 || $day_count_mode==2)
{
    for ($i=0; $i<7; $i++){
    imageline($image, $i*600/7+20, (250-$page_data[0][$i]*25/$step), ($i+1)*600/7+20, (250-$page_data[0][$i+1]*25/$step), $colorRed);
    imageline($image, $i*600/7+20, (250-$page_data[1][$i]*25/$step), ($i+1)*600/7+20, (250-$page_data[1][$i+1]*25/$step), $colorGreen);
    imageline($image, $i*600/7+20, (250-$page_data[2][$i]*25/$step), ($i+1)*600/7+20, (250-$page_data[2][$i+1]*25/$step), $colorBlue);
    imageline($image, $i*600/7+20, (250-$page_data[3][$i]*25/$step), ($i+1)*600/7+20, (250-$page_data[3][$i+1]*25/$step), $colorYellow);
    imageline($image, $i*600/7+20, (250-$page_data[4][$i]*25/$step), ($i+1)*600/7+20, (250-$page_data[4][$i+1]*25/$step), $colorPurple);
    }
}

if ($_SESSION['period']==3 || $_SESSION['period']==31 || $day_count_mode==3)
{
    for ($i=1; $i<=$days; $i++){
    imageline($image, ($i-1)*(600/$days)+20, (250-$page_data[0][$i]*25/$step), $i*(600/$days)+20, (250-$page_data[0][$i+1]*25/$step), $colorRed);
    imageline($image, ($i-1)*(600/$days)+20, (250-$page_data[1][$i]*25/$step), $i*(600/$days)+20, (250-$page_data[1][$i+1]*25/$step), $colorGreen);
    imageline($image, ($i-1)*(600/$days)+20, (250-$page_data[2][$i]*25/$step), $i*(600/$days)+20, (250-$page_data[2][$i+1]*25/$step), $colorBlue);
    imageline($image, ($i-1)*(600/$days)+20, (250-$page_data[3][$i]*25/$step), $i*(600/$days)+20, (250-$page_data[3][$i+1]*25/$step), $colorYellow);
    imageline($image, ($i-1)*(600/$days)+20, (250-$page_data[4][$i]*25/$step), $i*(600/$days)+20, (250-$page_data[4][$i+1]*25/$step), $colorPurple);
    }
}

if ($_SESSION['period']==4 || $day_count_mode==4)
{
    for ($i=0; $i<12; $i++){
    imageline($image, $i*(600/12)+20, (250-$page_data[0][$i]*25/$step), ($i+1)*(600/12)+20, (250-$page_data[0][$i+1]*25/$step), $colorRed);
    imageline($image, $i*(600/12)+20, (250-$page_data[1][$i]*25/$step), ($i+1)*(600/12)+20, (250-$page_data[1][$i+1]*25/$step), $colorGreen);
    imageline($image, $i*(600/12)+20, (250-$page_data[2][$i]*25/$step), ($i+1)*(600/12)+20, (250-$page_data[2][$i+1]*25/$step), $colorBlue);
    imageline($image, $i*(600/12)+20, (250-$page_data[3][$i]*25/$step), ($i+1)*(600/12)+20, (250-$page_data[3][$i+1]*25/$step), $colorYellow);
    imageline($image, $i*(600/12)+20, (250-$page_data[4][$i]*25/$step), ($i+1)*(600/12)+20, (250-$page_data[4][$i+1]*25/$step), $colorPurple);
    }
}

// Output graph and delete image from memory
imagepng($image);
imagedestroy($image);

?>

