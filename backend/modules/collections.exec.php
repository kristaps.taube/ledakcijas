<?
  $site_id = $GLOBALS['site_id'];
  $category_id = $_GET['category_id'];

  $cols = $collection->columns;


  if(!function_exists('redirect_to_collection_edit'))
  {
    function redirect_to_collection_edit($sel, $oldsel)
    {
      global $collection;

      if(!$collection->table)
      {
        $sel = $oldsel;
      }

      //calculate page number to make selected item visible
      if($sel)
      {
        $pagesize = $collection->GetEditPageSize();
        $position = $sel;
        $collection->GetEditData(intval($_SESSION[$collection->name . '_coledit_active_page']), $count, $position, $_GET['ordercolumn']);
        $_SESSION[$collection->name . '_coledit_active_page'] = floor($position / $pagesize);
      }

      if($_GET['main'])
      {
        header('Location: ?module='.$_GET['module'].'&site_id='.$_GET['site_id'].
                 '&action=editcollection&id='.$_GET['category_id'].'&selrow='.$sel.
                 '&coltype='.$_GET['coltype'].'&justedit='.$_GET['justedit']);
      }else
      {
        header('Location: ?module='.$_GET['module'].'&action=listcolitems&site_id='.$_GET['site_id'].
                 '&category_id='.$_GET['category_id'].'&newname='.$_GET['newname'].
                 '&coltype='.$_GET['coltype'].'&page_id='.$_GET['page_id'].'&justedit='.$_GET['justedit'].'&selrow='.$sel);
      }

    }
  }


  foreach ($cols as $key=>$col)
  {
      if ($col['timestamp'] && substr($key, 0, 3) == 'col' && is_numeric(substr($key,3)))
      {
          $date_cols[] = substr($key,3);
      }
  }

  if ($date_cols)
  {
      foreach ($data as $key=>$row)
      {
          foreach ($date_cols as $date_col)
              $data[$key][$date_col] = date("Y-m-d H:i:s", $row[$date_col]);
      }
  }

  if($action == 'addcollectionsave')
  {
    $colname = $_POST['title'];
    $coltype = $_POST['type'];

    if($colname && $coltype)
    {
      include_once($GLOBALS['cfgDirRoot'] . "collections/class." . $coltype . ".php");
      $collection = new $coltype($colname, 0);
      if ($collection->collectionExists($colname))
      {
        echo '
        <script type="text/javascript">
          alert("Collection with such name already exists!")
        </script>
        ';
      }
      else
      {
        $category_id = $collection->addNewCollection($colname);
        $docStrings['urlend'] = '&action=editcollection&id=' . $category_id;
      }
    }
  }

  else if($action == 'filtercollectionsave')
  {
    $_SESSION['collections_viewfilter_' . $collection->name] = Array();
    $fields = $collection->GetDBProperties();
    foreach($_POST as $key => $val)
    {
      if($val)
      {
        if($fields[$key]['related_collection'])
          $_SESSION['collections_viewfilter_' . $collection->name][$key] = $val;
        else
          $_SESSION['collections_viewfilter_' . $collection->name][$key] = '%' . $val . '%';
      }
    }
  }

  else if($action == 'filtercollectionclear')
  {
    $_SESSION['collections_viewfilter_' . $collection->name] = Array();
    redirect_to_collection_edit($_GET['selrow'], $_GET['selrow']);
    die();
  }

  else if($action == 'setrelatedcollectionsave')
  {
    $collection->setProperty('relation_' . $collection->name . '_' . $_GET['fieldname'], $_POST['rel_col']);
  }

  else if($action == 'listcolitems')
  {
    if($category_id==0)
    {

      $category_id = $collection->addNewCollection($colname);
      header('Location: ?module='.$_GET['module'].'&action='.$_GET['action'].'&site_id='.$_GET['site_id'].
             '&category_id='.$category_id.'&newname='.$_GET['newname'].'&coltype='.$_GET['coltype'].
             '&page_id='.$_GET['page_id'].'&refreshparent=1'.'&justedit='.$_GET['justedit']);
      die();
    }
  }else if($action == 'addcolitem')
  {
    $collection->execAddItemSave('?module=collections&action=newcolitem');

  }else if($action == 'modcolitem')
  {
    $collection->execModItemSave(intval($_GET['id']), '?module=collections&action=editcolitem');

  }else if($action == 'delcolitem')
  {

    $delitems = explode(',',$_GET['id']);
    if (empty($delitems))
    {
      $collection->DelDBItem($_GET['id']);
    }
    else
    {
      arsort($delitems);
      foreach ($delitems as $id)
      {
        if (!empty($id))
        {
          $collection->DelDBItem($id);
        }
      }
    }
    redirect_to_collection_edit(0, 0);
    die();

  }else if($action == 'moveup')
  {
    $ind = $collection->GetItemInd($_GET['id']);
    $query_params = $collection->GetDefaultQueryParams(-1, '', true);
    $sel = $collection->MoveItemUp($ind, $query_params['countwhere']);
    redirect_to_collection_edit($_GET['id'], $sel);
    die();

  }else if($action == 'movedown')
  {
    $ind = $collection->GetItemInd($_GET['id']);
    $query_params = $collection->GetDefaultQueryParams(-1, '', true);
    $sel = $collection->MoveItemDown($ind, $query_params['countwhere']);
    redirect_to_collection_edit($_GET['id'], $sel);
    die();

  }else if($action == 'delcollection')
  {
    $collection->DelCollection();
    if(!$_GET['main'])
    {
      echo '
      <script language="JavaScript">
      <!--
        if(window.parent)
        {
          var combo = window.parent.dialogArguments[2];
          var win = window.parent.dialogArguments[1];
          if(combo)
          {
            if(combo.selectedIndex>0)
              combo.options.remove(combo.selectedIndex);
            window.parent.returnValue = 0;
            window.parent.close();
          }
        }
      <!---->
      </script>
      ';
    }
    else
    {
      header('Location: ?module='.$_GET['module'].'&site_id='.$_GET['site_id'].
               '&action=editcollection&id='.$_GET['category_id']);
    }
    Die();
  }else if($action == 'updatecolproperties')
  {
    $collection= $GLOBALS['collection'];
    foreach($_POST as $key => $val)
    {
      if(substr($key, 0, 6) == 'check_')
      { //default property
        $key = substr($key, 6);
        sqlQuery('DELETE FROM ' . $_GET['site_id'] . '_colproperties WHERE propertyname="'.$key.'" AND collection_id='.$_GET['category_id'].' AND coltype="' . $_GET['coltype'] . '"');
      }
    }
    foreach($_POST as $key => $val)
    {
      if(substr($key, 0, 6) != 'check_')
      {
        if(!$_POST['check_' . $key])
        { //collection property
          $collection->setProperty($key, $val);
        }
      }
    }
  }else if($action == 'execcolmethod' || $action == 'dlgcolmethodexec')
  {
    $ids = explode(',',$_GET['id']);
    if (empty($ids))
    {
      $ids = Array(intval($_GET['id']));
    }else
    {
      foreach($ids as $key => $val)
      {
        $ids[$key] = intval($val);
        if(!$ids[$key])
        {
          unset($ids[$key]);
        }
      }
    }

//    print_r($ids);
//    die();

    $method = $_GET['method_name'];
    $collection->$method($ids);

    if($action == 'execcolmethod')
    {
      redirect_to_collection_edit($ids[0], $ids[0]);
      die();
    }
  }
?>