<?
  $perm_managecontents = $GLOBALS['perm_managecontents'];
  if(!$perm_managecontents)
    AccessDenied(True);

  require_once ($GLOBALS['cfgDirRoot']."library/".'class.formgen.php');
  $id = $_GET["id"];
  if (!$id) $id = $_GET['pagedev_id'];
  $GLOBALS['pagedev_id'] = $id;
  $pagedev_id = $id;
  $site_id = $GLOBALS['site_id'];
    
  //page_id from pages table
  $page_id = sqlQueryValue("SELECT page_id FROM ".$site_id."_pagesdev WHERE pagedev_id=$id");

  if($pagedev_id>0)
  {   //real page
    $template = sqlQueryValue("SELECT template FROM ". $site_id . "_pagesdev WHERE pagedev_id=$pagedev_id");
  }else
  { //template view
    $template = sqlQueryValue("SELECT template_id  FROM ". $site_id . "_views WHERE view_id=".-$id);
  }

  if($template)
  {
    list($data, $copybody) = sqlQueryRow("SELECT body, copybody FROM " . $site_id . "_templates WHERE template_id=$template");
    while($copybody)
      list($data, $copybody) = sqlQueryRow("SELECT body, copybody FROM " . $site_id . "_templates WHERE template_id=$copybody");
  }

  if ($pagedev_id > 0)
  {

    $page_name = sqlQueryValue("SELECT title FROM ".$site_id."_pagesdev WHERE pagedev_id=" . $pagedev_id);
    if(!empty($page_name))
      echo '<h5>{%langstr:component_properties_for_page%} '.$page_name.'</h5>';

  } else {

    $language_id = intval($_SESSION['currentlanguagenum']);
    $template_id = intval($template);

    $viewdata = sqlQueryRow('SELECT * FROM ' . $site_id . '_views WHERE template_id = ' . $template_id . ' AND language_id = ' . $language_id);
    if($viewdata)
    {
      $language = sqlQueryValue("SELECT fullname FROM ".$site_id."_languages WHERE language_id=" . $language_id);
      if($language)
        echo '<h5>{%langstr:default_values_for_language%} '.$language.'</h5>';
    }

  }


  $data = attachSubTemplates($data, $site_id);
  $r = componentArray($data, $pagedev_id);
  if(!count($r))
  {
    echo "No components in this page";
  }
  else
  {
    $form_data = Array();
    for($f=0; $f<count($r); $f++)
    {
      displayPropertiesTable($r[$f]["type"], $r[$f]["name"], $site_id, $id, $form_data);
    }

    $Inspector = new Inspector();
    $Inspector->properties = $form_data;
    $Inspector->opened = (count($r) == 1);
    $Inspector->buttons =true;

    $Inspector->cancel ="closeparent";
    $Inspector->name = "Form";
    $Inspector->action = '?module=' .$GLOBALS['module']. '&site_id=' .$site_id. '&pagedev_id=' .$id. '&page_id=' .$page_id. '&action=updatecontrols';
    if($page_id > 0)
      $Inspector->action .= '&redirect=1';
    echo $Inspector->output();

  }
  
  
?>
