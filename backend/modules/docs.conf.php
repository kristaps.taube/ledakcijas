<?
/*
sqlQuery("
    CREATE TABLE IF NOT EXISTS `".$site_id."_documents` (
    `file_id` INT NOT NULL AUTO_INCREMENT,
    `file_name` TEXT NOT NULL ,
    `file_description` TEXT NOT NULL ,
    `parent_directory` INT NOT NULL ,
    `real_name` TEXT NOT NULL ,
    `creation_date` TEXT NOT NULL ,
    `ind` INT NOT NULL ,
    `public` INT( 1 ) DEFAULT '1' NOT NULL ,
    `deleted` INT( 1 ) DEFAULT '0' NOT NULL ,
    UNIQUE (
    `file_id` 
    )
    )
");

sqlQuery("
    CREATE TABLE `".$site_id."_documentdir` (
    `dir_id` INT NOT NULL ,
    `dir_name` TEXT NOT NULL ,
    `dir_description` TEXT NOT NULL ,
    `parent_directory` INT NOT NULL ,
    `creation_date` TEXT NOT NULL ,
    `ind` INT NOT NULL 
    )
");

*/

$action = $_GET["action"];
$GLOBALS['site_id'] = $_GET["site_id"];
/*
if (isset($_GET['path'])) { $_SESSION['path_img'] = $_GET['path']; }
if ($_SESSION['path_img'] != "") $GLOBALS['path'] = $_SESSION['path_img'];
else $GLOBALS['path'] = $_GET["path"];
*/

if (isset($_GET['parent_dir']) && $_GET['parent_dir']!="") { $_SESSION['parent_dir'] = $_GET['parent_dir']; }
else if (!isset($_SESSION['parent_dir']) || $_GET['parent_dir']=="") $_SESSION['parent_dir'] = 0;


$GLOBALS['close'] = $_GET["close"];
$GLOBALS['view'] = $_GET["view"];
$GLOBALS['tabs'] = 'site';
/*if (isset($_GET['backup'])) $GLOBALS['backup'] = $_GET['backup'];*/

unset($perm_modsite);
if(($site_id == $GLOBALS["currentUserSiteID"])or(!$GLOBALS["currentUserSiteID"]))
  $perm_modsite = CheckPermission(6, $site_id);
if(!$perm_modsite)
  AccessDenied(False);

unset($perm_managedocuments);
if(($site_id == $GLOBALS["currentUserSiteID"])or(!$GLOBALS["currentUserSiteID"]))
  $perm_managedocuments = CheckPermission(28, $site_id);

require($GLOBALS['cfgDirRoot']."library/"."func.files.php");

switch ($action) {

  case "newdir":
    $docTitle = "Make Directory";
    $docTemplate = "form.htm";
    $docScripts['body'] = "docs.newdir.php";
    break;

  case "upload":
    $docTitle = "Upload File";
    $docTemplate = "form.htm";
    $docScripts['body'] = "docs.upload.php";
    break;

  case "rename":
    $docTitle = "Rename File";
    $docTemplate = "form.htm";
    $docScripts['body'] = "docs.rename.php";
    break;

  case "mkdir":
    $docScripts['exec'] = "docs.exec.php";
    if ($GLOBALS['close'] == 'no') {
      $docTemplate = "form.htm";
      if ($_GET['dironly']) $docScripts['body'] = "docs.dirlist.php";
      else $docScripts['body'] = "files.list.php";
    } else {
      $docTemplate = "close.htm";
    }
    break;
  
  case "history":
    $docTemplate = "form.htm";
    $docScripts['exec'] = "docs.history.php";
    $docTitle = "Document history";
    break;

  case "save"://print_r($_GET);die;
  case "dorename":
  case "doc_permissionssave":
    $docScripts['exec'] = "docs.exec.php";
    if ($GLOBALS['close'] == 'no') {
      $docTemplate = "form.htm";
      $docScripts['body'] = "files.list.php";
    } else {
      $docTemplate = "close.htm";
    }
    break;

  case "list":
    $docTemplate = "form.htm";
    $docScripts['body'] = "files.list.php";
    break;

  case "dirlist":
    $docTemplate = "form.htm";
    $docScripts['body'] = "files.dirlist.php";
    break;

  case "copy":
    $docTitle = "Rename File";
    $docTemplate = "form.htm";
    $docScripts['body'] = "files.copy.php";
    break;

  case "docopy":
    $docScripts['exec'] = "files.exec.php";
    if ($GLOBALS['close'] == 'no') {
      $docTemplate = "form.htm";
      $docScripts['body'] = "files.list.php";
    } else {
      $docTemplate = "close.htm";
    }
    break;

  case "moveupfile":
  case "movedownfile":
  case "moveupdir":
  case "movedowndir":
  case "setdocpublic":
  case "unsetdocpublic":
    $docScripts['exec'] = "docs.exec.php";
    Header("Location: ?module=docs&site_id=" . $GLOBALS['site_id'] . '&parent_dir='.$_SESSION['parent_dir'].'&selrow=' . $_GET['selrow']);
    break;
  
  case "doc_permissions":
    $docTitle = "Document Permissions";
    $docTemplate = "form.htm";
    $docScripts['body'] = "docs.permissions.php";        
      break;

  case "delete":
    $docScripts['exec'] = "docs.exec.php";
    if ($GLOBALS['close'] == 'no') {
      $docTemplate = "form.htm";
      if ($_GET['dironly']) $docScripts['body'] = "docs.dirlist.php";
      else $docScripts['body'] = "files.list.php";
      break;
    }

  default:

    $docroot = sqlQueryValue("SELECT docroot FROM sites WHERE site_id=".$GLOBALS['site_id']);


    require($GLOBALS['cfgDirRoot']."library/"."class.toolbar.php");
    if($perm_managedocuments)
    {
      /*$Toolbar = new Toolbar();
      $Toolbar->AddButton("mkdir", "Make Directory", "make_directory.gif", "openDialog('?module=docs&site_id=".$GLOBALS['site_id']."&parent_dir=".$_SESSION['parent_dir']."&action=newdir', 400, 200);", "Make new directory");
      $Toolbar->AddButton("upload", "Upload", "upload.gif", "openDialog('?module=docs&site_id=".$GLOBALS['site_id']."&parent_dir=".$_SESSION['parent_dir']."&action=upload', 400, 200, 0, 1);", "Upload document to the current directory");
      $Toolbar->AddButton("ren", "Rename", "edit_properties.gif", "if (SelectedRowID) openDialog('?module=docs&site_id=".$GLOBALS['site_id']."&action=rename&name='+escape(SelectedRowID), 500, 200);", "Rename file to the current directory");
      $Toolbar->AddButton("perms", "Document Permissions", "edit_permissions.gif", "javascript:openDialog('?module=permissions&action=permission_frame&mod=documents_form&close=1&site_id=$site_id&data=$site_id&data2=', 800, 600, 0, 1);", "Edit selected template access permissions");
      $Toolbar->AddLink("del", "Delete", "delete.gif", "javascript:
      if (window.fff || SelectedRowID)
          if (confirm('Are you sure you want to delete selected files?'))
      if (window.fff) window.location='?module=docs&action=delete&parent_dir=".$_SESSION['parent_dir']."&file='+window.fff+'&site_id=".$GLOBALS['site_id']."';
      else if (SelectedRowID) window.location='?module=docs&action=delete&parent_dir=".$_SESSION['parent_dir']."&file='+SelectedRowID+'&site_id=".$GLOBALS['site_id']."';", "Delete selected files");

      if ($docroot) $docStrings['toolbar'] = $Toolbar->output();*/

      require($GLOBALS['cfgDirRoot']."library/"."class.toolbar2.php");
      $Toolbar = new Toolbar2();

      $Toolbar->AddSpacer();
      $Toolbar->AddButtonImage("mkdir", 'files_new_folder', "{%langstr:docs_make_directory%}", "", "openDialog('?module=docs&site_id=".$GLOBALS['site_id']."&parent_dir=".$_SESSION['parent_dir']."&action=newdir', 400, 200);", 31, "{%langstr:docs_make_new_directory%}");
      $Toolbar->AddButtonImage("upload", 'files_upload', "{%langstr:docs_upload%}", "", "openDialog('?module=docs&site_id=".$GLOBALS['site_id']."&parent_dir=".$_SESSION['parent_dir']."&action=upload', 400, 200, 0, 1);", 31, "{%langstr:docs_upload_doc%}");
      $Toolbar->AddButtonImage("ren", 'files_rename', "{%langstr:docs_rename%}", "", "if (SelectedRowID) openDialog('?module=docs&site_id=".$GLOBALS['site_id']."&action=rename&name='+escape(SelectedRowID), 500, 200);", 31, "{%langstr:docs_rename_selected_doc%}");
      $Toolbar->AddButtonImage("perms", 'sitemap_permissions', "{%langstr:docs_document_permissions%}", "", "javascript:openDialog('?module=permissions&action=permission_frame&mod=documents_form&close=1&site_id=$site_id&data=$site_id&data2=', 800, 600, 0, 1);", 31, "{%langstr:docs_edit_document_access%}");
      $Toolbar->AddButtonImage("del", 'delete', "{%langstr:toolbar_delete%}", "javascript:
      if (window.fff || SelectedRowID)
          if (confirm('{%langstr:toolbar_delete_selected_files%}'))
      if (window.fff) window.location='?module=docs&action=delete&parent_dir=".$_SESSION['parent_dir']."&file='+window.fff+'&site_id=".$GLOBALS['site_id']."';
      else if (SelectedRowID) window.location='?module=docs&action=delete&parent_dir=".$_SESSION['parent_dir']."&file='+SelectedRowID+'&site_id=".$GLOBALS['site_id']."';", '', 31, "{%langstr:docs_delete_selected_doc%}", '{%langstr:toolbar_delete_selected_files%}');
      $Toolbar->AddSeperator();

      if ($docroot) $docStrings['toolbar'] =  $Toolbar->output();
    }

    $docTemplate = "main.htm";
    $docTitle = "Document Explorer";
    $docScripts['body'] = "docs.default.php";
    break;

}

?>
