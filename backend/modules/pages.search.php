<?

$perm_managepages = $GLOBALS['perm_managepages'];

//function for sorting data in flat table
function NodesFlat($data, &$newdata, $parent, $level, $justcount, &$goodl, $perms)
{
  $goodlanguage = false;
  $nodecount = 0;
  foreach($data as $key => $row)
  {
        $nodecount++;

        $row['prefix'] = str_repeat('<img src="gui/images/1x1.gif" width="16" height="16" border="0" align="absmiddle">', $level) . '<img src="gui/images/1x1.gif" width="16" height="16" border="0" align="absmiddle">&nbsp;';
        $newdata[] = $row;
        $c = count($newdata) - 1;
        $childs = array();
        $goodlanguage = ($g or $goodlanguage);
        if(!$_SESSION['expanded' . $row['page_id']])
        {
          $newdata = array_slice($newdata, 0, $c+1);
        }

        if($row['language'] != intval($GLOBALS['currentlanguagenum']))
        {
          //$newdata[$c]['page_id'] = 0;
          if(!$g)
          {
            if($c)
              $newdata = array_slice($newdata, 0, $c);
            else
              $newdata = array();
            $nodecount--;
          }else
          {
            $newdata[$c]['clicklink'] = "Page in " . sqlQueryValue("SELECT fullname FROM " . $GLOBALS['site_id'] . "_languages WHERE language_id = " . intval($row['language'])) . " (" . $row['name'] . ")";
          }
        }else
        {
          if ($GLOBALS['perm_managepages'] || in_array($row['page_id'],$perms))
          {
            $newdata[$c]['clicklink'] = "<a href=\"#\" OnClick=\"javascript:openDialog('?module=pages&site_id=" . $GLOBALS['site_id'] . "&action=components_visual_frame&id=".$row['pagedev_id']."&page_id=".$row['page_id']."', screen.availWidth, screen.availHeight-25, 1 , 1);\">".ShortenString($row['title'],40)."</a>";

            //publish page if 'Can Publish All Pages' permission is set for current user
            if (CheckPermission(33, $site_id)) $newdata[$c]['publish'] = '<a href="?module=pages&site_id='.$_GET['site_id'].'&action=publish&pagedev_id='.$row['pagedev_id'].'&page_id='.$row['page_id'].'">Publish</a>';
            else $newdata[$c]['publish'] = 'Publish';
          }
          else
            $newdata[$c]['clicklink'] = ShortenString($row['title'],40);
          $goodlanguage = true;
        }
  }
  $goodl = $goodlanguage;
  return $nodecount;
}


$site_id = $GLOBALS['site_id'];

// Let's try table generator

$cols = Array(

  "preview"     => Array(
    "width"     => "5%",
    "align"     => "center",
    "title"     => "Prev.",
    "format"    => "<a href=\"?module=pages&site_id=$site_id&action=page_redirect&id={%page_id%}\"  target=\"_blank\"><img src=\"gui/images/ico_view.gif\" width=\"16\" height=\"16\" border=\"0\"></a>"
  ),

  "visible"       => Array(
    "width"     => "5%",
    "align"     => "center",
    "title"     => "Vis."
  ),

  "enabled"     => Array(
    "width"     => "5%",
    "align"     => "center",
    "title"     => "Enab."
  ),

  "title"        => Array(
    "width"     => "30%",
    "title"     => "Title",
    "format"    => "{%prefix%}{%scut%}{%clicklink%}"
  ),

  "description" => Array(
    "width"     => "40%",
    "title"     => "Description"
  ),

  "modby" => Array(
    "width"     => "15%",
    "title"     => "Modified by"
  ),

  "publish"     => Array(
    "width"     => "5%",
    "align"     => "center",
    "title"     => "Pub."
  ),

  "previewdev"     => Array(
    "width"     => "5%",
    "align"     => "center",
    "title"     => ""
  ),

);

if(!$perm_managepages)
{
  //unset($cols['title']['format']);
}


$javascriptv = ' onClick="javascript:SubmitPageVisibleCheckbox(this, \',pagedev_id,\',' . $site_id . ')"';
$javascripte = ' onClick="javascript:SubmitPageEnabledCheckbox(this, \',pagedev_id,\',' . $site_id . ')"';

if(!$perm_managepages)
{
  $javascriptv = '';
  $javascripte = '';
}
$checkstylev = 'CONCAT(\'<input type="checkbox" name="cccb"' . $javascriptv . ' checked>\')';
$uncheckstylev = 'CONCAT(\'<input type="checkbox" name="cccb"' . $javascriptv . '>\')';
$checkstylee = 'CONCAT(\'<input type="checkbox" name="cccb"' . $javascripte . ' checked>\')';
$uncheckstylee = 'CONCAT(\'<input type="checkbox" name="cccb"' . $javascripte . '>\')';

$shortcutimage = '<img src="gui/images/shortcut.gif" width="11" height="11" border="0"> ';


//$unsorteddata = sqlQueryData("SELECT pagedev_id, page_id, IF(redirect>0, CONCAT('$shortcutimage', '&nbsp;'), '') AS scut, name, title, description, ELT(visible+1,$uncheckstylev,$checkstylev) AS visible, ELT(p.enabled+1,$uncheckstylee,$checkstylee) AS enabled, parent, language, lastmod, username as modby FROM " . $site_id . "_pagesdev as p, users as u WHERE modby = u.user_id ORDER BY ind");
//get data from search
include_once($GLOBALS['cfgDirRoot'] . "library/func.search.php");
$s = strip_tags($_GET['q']);
$searchdata = SearchPages($site_id, $s, $swords);
$unsorteddata = array();
$pages = '';
foreach($searchdata as $se)
{
  if($pages)
    $pages .= ', ';
    $pages .= $se['page_id'];
}
if($pages)
  $unsorteddata = sqlQueryData("SELECT pagedev_id, page_id, IF(redirect>0, CONCAT('$shortcutimage', '&nbsp;'), '') AS scut, name, title, description, ELT(visible+1,$uncheckstylev,$checkstylev) AS visible, ELT(p.enabled+1,$uncheckstylee,$checkstylee) AS enabled, parent, language, lastmod, username as modby FROM " . $site_id . "_pagesdev as p, users AS u WHERE modby = u.user_id AND page_id IN (".$pages.") ORDER BY ind");

//get page_id array
$ids = sqlQueryData("select distinct page_id, ind from ".$site_id."_pagesdev ORDER BY ind");

//get keys of last modified dev pages
if (count($ids))
{
    $keys = array();
    for ($i=0; $i<=count($ids)-1; $i++)
    {
        $max = 0;
        foreach ($unsorteddata as $key=>$row)
        {
            //echo $row['page_id']." ".$ids[$i]['page_id']."<br>";
            if ($row['page_id']==$ids[$i]['page_id'])
            {
                if($row['lastmod']>$max)
                {
                    $max = $row['lastmod'];
                    $keys[$i] = $key;
                    $lastmoddev[$row['page_id']]['lastmod'] = $max;
                }
            }
        }
    }
}

//drop unnecessary rows
foreach ($unsorteddata as $key=>$row)
    if (!in_array($key,$keys))
        unset($unsorteddata[$key]);
$lastmod_data = sqlQueryData("SELECT page_id,lastmod FROM ".$site_id."_pages");
foreach ($lastmod_data as $row)
{
    $lastmod[$row['page_id']]['lastmod'] = $row['lastmod'];
}

$groups = sqlQueryDataAssoc("SELECT group_id from users_groups where user_id=".$GLOBALS['currentUserID']);
foreach ($groups as $group)
    $where .= " OR (perm_num=11 AND  isuser=0 AND id=".$group['group_id']." AND data=".$site_id." AND data2<>'')";

$perm_data = sqlQueryDataAssoc("SELECT data2 FROM permissions WHERE (perm_num=11 AND  isuser=1 AND id=".$GLOBALS['currentUserID']." AND data=".$site_id." AND data2<>'') ".$where);
//echo "SELECT data2 FROM permissions WHERE (perm_num=11 AND  isuser=1 AND id=".$GLOBALS['currentUserID']." AND data=".$site_id." AND data2<>'') ".$where;
foreach ($perm_data as $row)
    if (!in_array($row['data2'],$perms)) $perms[] = $row['data2'];
//print_r($perms);

$g = false;
NodesFlat($unsorteddata, $data, 0, 0, 0, $g, $perms);

//check which pages to publish
foreach ($data as $key=>$row)
{
    if ($row['lastmod']==$lastmod[$row['page_id']]['lastmod'])
    {
        unset($data[$key]['publish']);
    }
    else
    {
        $data[$key]['previewdev'] = "<a href=\"?module=pages&site_id=$site_id&action=page_redirect&id=".$row['page_id']."&pagedev_id=".$row['pagedev_id']."&showdevpage=1\"  target=\"_blank\"><img src=\"gui/images/ico_view.gif\" width=\"16\" height=\"16\" border=\"0\"></a>";
    }
}

require($GLOBALS['cfgDirRoot']."library/"."class.datagrid.php");

$Table = new DataGrid();
$Table->cols = $cols;
$Table->data = $data;
$Table->footer = false;
$Table->rowformat = " id=\"tablerow{%pagedev_id%}\" onClick=\"javascript:HighLightTR(this, {%pagedev_id%});
                         if(SelectedRowID){ EnableToolbarButtons(che); }else { DisableToolbarButtons(che); }
                         \"";
echo $Table->output();

if($_GET['accessdenied'])
{
  AccessDenied(false, true);
  echo '
  <script language="JavaScript" type="text/javascript">
  /*<![CDATA[*/
  window.document.location.href = "?module=pages&site_id=' . $GLOBALS['site_id'] . '&selrow=' . $_GET['selrow'] . '" 
  /*]]>*/
  </script>
  ';

}

echo '<script language="JavaScript" type="text/javascript">
/*<![CDATA[*/
che = new Array("add2", "properties", "perms", "sedit", "visedit", "strings", "up", "down", "del");
DisableToolbarButtons(che);
/*]]>*/
</script>';

if($_GET['selrow'])
{
  echo '<script language="JavaScript">
        <!--
          var tablerow' . $_GET['selrow'] . ' = document.getElementById("tablerow' . $_GET['selrow'] . '");
          if(typeof(tablerow' . $_GET['selrow'] . ')!= "undefined" && tablerow' . $_GET['selrow'] . ')
          {
            preEl = tablerow' . $_GET['selrow'] . ';
            ChangeTextColor(tablerow' . $_GET['selrow'] . ',\'tableRowSel\');
            SelectedRowID = ' . $_GET['selrow'] . ';
            if(SelectedRowID){ EnableToolbarButtons(che); }else { DisableToolbarButtons(che); }
          }
        <!---->
        </script>';
}

?>
