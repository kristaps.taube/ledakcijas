<?

// checkbox supports
if ($_POST)
{
  // atlasam komponentus, par kuriem ir info ieksh POST
  $cmp = Array();
  foreach(array_keys($_POST) as $key)
  {
    $s = explode('_', $key, 2);
    if($s[0] == 'type')
    {
      $cmp[$s[1]] = $_POST[$key];
    }
  }

  // uzstadam tuksho checkbokshu vertibu kaa nulli
  foreach ($cmp as $name => $type)
  {
    $props = getPropertiesTable($type);
    foreach ($props as $key => $p)
    {
      if ($p['type'] == 'check')
      {
        $k = 'component_'.$name.'_'.$key;
        if (!isset($_POST[$k]))
          $_POST[$k] = '0';

      }
    }
  }
}

  require_once ($GLOBALS['cfgDirRoot']."library/".'func.search.php');

  $site_id = $GLOBALS['site_id'];
  $perm_managetemplates = $GLOBALS['perm_managetemplates'];
  $perm_managecontents = $GLOBALS['perm_managecontents'];

  function IndexAndCompileAllPages($template_id, $site_id)
  {
    $pages = sqlQueryColumn("SELECT page_id FROM " . $site_id . "_pages WHERE template=$template_id");
    foreach($pages as $page)
    {
      //IndexPageForSearch($site_id, $page);
      //CreateCompiledTemplate($site_id, $page);
    }
  }

  function DeleteAllCompiledPages($template_id, $site_id)
  {
    sqlQuery("DELETE FROM " . $site_id . "_ctemplates WHERE template_id=$template_id");
  }

  if(((($action == 'addtemplate')and($perm_managetemplates)))or((($action == 'modtemplate')and($perm_managetemplates))))
  {
    $name = $_POST['name'];
    $description = $_POST['description'];
    $template_id = $_POST['template_id'];
    $copybody = $_POST['copybody'];
    //Check if field input is valid
    $invalid = Array();
    $jsmessage = '';
    if((IsEmpty($name))or(IsBadSymbols($name, ' '))or(sqlQueryValue("SELECT template_id FROM " . $site_id . "_templates WHERE name='$name' AND template_id<>'$template_id'")))
    {
      $invalid[] = "name";
      if(!IsEmpty($name))
        $jsmessage .= 'Template name ' . msgValidCharsUnique . '\n';
    }
    if(IsLtGtSymbols($description))
    {
      $invalid[] = "description";
      $jsmessage .= 'Description ' . msgNoLtGt . '\n';
    }
    if(count($invalid))
    {
      if($action == 'addtemplate')
        $action = 'create';
      if($action == 'modtemplate')
        $action = 'properties_form';
      $invalid = join(':', $invalid);
      $name = urlencode(stripslashes($name));
      $description = urlencode(stripslashes($description));

      Redirect('?module='.$module.'&action='.$action.'&invalid=' . $invalid . '&formdata_name='.$name . '&formdata_description='.$description . '&jsmessage='.$jsmessage.'&id='.$template_id.'&site_id='.$site_id);
      Die();
    }
  }


  if(($action == 'addtemplate')and($perm_managetemplates))
  {
    $ind = sqlQueryValue("SELECT max(ind)+1 FROM " . $site_id . "_templates");
    sqlQuery("INSERT INTO " . $site_id . "_templates (name,description, ind, updated) VALUES ('$name','$description',$ind, ".date("Y-m-d H:i:s").")");
    $template_id = sqlQueryValue("SELECT template_id FROM " . $site_id . "_templates WHERE name='$name'");

    $template = DB::GetRow("SELECT * FROM " . $site_id . "_templates WHERE template_id = :id", array(":id" => $template_id));
		if(defined("TEMPLATE_FOLDER")){
			$template_file_path = TEMPLATE_FOLDER.$template['name'].".php";
			file_put_contents($template_file_path, "");
		}

    //Add access to the newly created template for Everyone
    AddGroupPermission(1, 12, $site_id, $template_id);
    AddGroupPermission(1, 15, $site_id, $template_id);
    AddGroupPermission(1, 15, $site_id, $template_id);
    AddGroupPermission(1, 15, $site_id, $template_id);
    Add_Log("New template '$name' ($template_id)", $site_id);

  }else if(($action == 'modtemplate')and($perm_managetemplates))
  {

    DB::Query("UPDATE " . $site_id . "_templates SET name = :name, description = :desc, copybody = :copybody WHERE template_id = :id", array(
			":desc" => $description,
			":name" => $name,
			":copybody" => $copybody,
			":id" => $template_id,
		));
    Add_Log("Updated template '$name' ($template_id)", $site_id);

  }else if(($action== 'modcontents')and($perm_managecontents))
  {
    $body = $_POST['bodytext'];
    $name = $_POST['name'];
    $template_id = $_POST['template_id'];
    $copybody = $_POST['copybody'] ? 1 : 0;
    $description = $_POST['description'];

    //Check if all components have different names
    $oldbody = $body;
    $comptypes = Array();
    $compnames = Array();
    while (preg_match("/\{%component:([a-zA-Z0-9]+):([a-zA-Z0-9]+)(:[a-zA-Z0-9% \\.\\,\\\"\\\'\\#\\!-]+)%\}/U", $body, $regs))
    {

//      echo $regs[0] . "<br>";
      $body = str_replace($regs[0], "", $body);
      $comptype = $regs[1];
      $compname = $regs[2];
      if(in_array($compname, $compnames)){
        $pos = array_search($compname, $compnames);
        if($comptype != $comptypes[$pos])
        {
          $oldbody = str_replace($regs[0], "{%component:$comptype:???".$regs[3]."%}", $oldbody);
        }
      }else
      {
        $comptypes[] = $comptype;
        $compnames[] = $compname;
      }

    }
    $body = $oldbody;

    if(IsLtGtSymbols($description))
    {
      $description = htmlspecialchars($description);
    }

    IndexAndCompileAllPages($template_id, $site_id);
    DeleteAllCompiledTemplates($site_id);

    $old_data = sqlQueryRow("SELECT * FROM " . $site_id . "_templates WHERE template_id = ".$template_id);

    if (!empty($old_data) && is_array($old_data)){
      DB::Insert($site_id . "_templates_history", array(
      	"template_id" => (int)$template_id,
      	"name" => $old_data['name'],
      	"body" => $old_data['body'],
      	"description" => $old_data['description'],
      	"copybody" => $old_data['copybody'],
			));
    }

    DB::Update($site_id . "_templates", array("template_id" => $template_id), array(
    	"name" => $name,
    	"body" => $body,
    	"description" => $description,
    	"copybody" => $copybody,
    	"updated" => date("Y-m-d H:i:s"),
		));

		if(defined("TEMPLATE_FOLDER")){
			$template = DB::GetRow("SELECT * FROM " . $site_id . "_templates WHERE template_id = :id", array(":id" => $template_id));
			$template_file_path = TEMPLATE_FOLDER.$template['name'].".php";
			file_put_contents($template_file_path, $body);
		}

    Add_Log("Changed contents of template '$name' ($template_id)", $site_id);

  }else if(($action == 'deltemplate')and($perm_managetemplates))
  {

    $template_id = $_GET['id'];

    $perm_deltemplate = CheckPermission(15, $site_id, $template_id);
    if(!$perm_deltemplate)
    {
      $name = sqlQueryValue("SELECT name FROM " . $site_id . "_waptemplates WHERE template_id=$template_id");
      Add_Log("Permission denied to deleted template '$name' ($template_id)", $site_id); 
      Header("Location: ?module=".$_GET['module']."&site_id=" . $GLOBALS['site_id'] . "&selrow=" . $wappagedev_id . "&accessdenied=1");
    }
    else
    {
        $used = sqlQueryValue("SELECT page_id FROM " . $site_id . "_pages WHERE template=$template_id");
        if($used==null){

        	$template = DB::GetRow("SELECT * FROM " . $site_id . "_templates WHERE template_id = :id", array(":id" => $template_id));
          $template_file_path = TEMPLATE_FOLDER.$template['name'].".php";
					if(file_exists($template_file_path)){
          	unlink($template_file_path);
          }

          $name = sqlQueryValue("SELECT name FROM " . $site_id . "_templates WHERE template_id=$template_id");
          sqlQuery("DELETE FROM " . $site_id . "_templates WHERE template_id=$template_id");
          sqlQuery("DELETE FROM permissions WHERE perm_num=12 AND data=$site_id AND data2=$template_id");
          sqlQuery("DELETE FROM permissions WHERE perm_num=15 AND data=$site_id AND data2=$template_id");
          Add_Log("Deleted template '".$template['name']."' (".$template_id.")", $site_id);
        }
    }

    IndexAndCompileAllPages($template_id, $site_id);
    DeleteAllCompiledPages($template_id, $site_id);

  }else if(($action == 'delview')and($perm_managetemplates))
  {
    $view_id = $_GET['id'];
    $used = sqlQueryValue("SELECT page_id FROM " . $site_id . "_pages WHERE copypage=-$view_id LIMIT 1");
    if($used!=null)
    {
      //does the view have any parents
      $parent = sqlQueryValue("SELECT parent FROM " . $site_id . "_views WHERE view_id=$view_id");
      if(!$parent)
        $parent = 0;
      sqlQuery("UPDATE " . $site_id . "_pages SET copypage=$parent WHERE copypage=$view_id");
    }

    $name = sqlQueryValue("SELECT name FROM " . $site_id . "_views WHERE view_id=$view_id");
    $template_id = sqlQueryValue("SELECT template_id FROM ".$site_id."_views WHERE view_id=$view_id");
    sqlQuery("DELETE FROM " . $site_id . "_views WHERE view_id=$view_id");
    Add_Log("Deleted view '$name' ($view_id)", $site_id);

    IndexAndCompileAllPages($template_id, $site_id);
    DeleteAllCompiledPages($template_id, $site_id);

  }else if($action == 'updatecontrols')
  {
    $template_id = $_GET['id'];

    $template = DB::GetRow("SELECT * FROM " . $site_id . "_templates WHERE template_id = :id", array(":id" => $template_id));
		if(defined("TEMPLATE_FOLDER")){
			$template_file_path = TEMPLATE_FOLDER.$template['name'].".php";
		}

    $name = $template['name'];
    $updated = false;

    foreach(array_keys($_POST) as $key)
    {
      $s = explode('_', $key, 3);
      if($s[0] == 'component')
      {

        $oldcontID = sqlQueryValue("SELECT content_id FROM " . $site_id . "_contents WHERE page=-1 AND componentname = :cmp AND propertyname = :prop_name", array(":cmp" => $s[1], ":prop_name" => $s[2]));
        $contID = $oldcontID;
        sqlQuery("DELETE FROM " . $site_id . "_contents WHERE page=-1 AND componentname = :cmp AND propertyname=:prop_name", array(":cmp" => $s[1], ":prop_name" => $s[2]));
        sqlQuery("INSERT " . $site_id . "_contents SET page=-1, componentname = :cmp, propertyname = :prop_name, propertyvalue = :prop_value", array(":cmp" => $s[1], ":prop_name" => $s[2], ":prop_value" => $_POST[$key]));
        $updated = true;
        $contID = sqlQueryValue("SELECT content_id FROM " . $site_id . "_contents WHERE page=-1 AND componentname = :cmp AND propertyname = :prop_name", array(":cmp" => $s[1], ":prop_name" => $s[2]));
        //Update search table
        $type = $_POST["type_" . $s[1]];
        $properties = getPropertiesTable($type);
        if($properties[$s[2]]['search'])
        {
          sqlQuery("DELETE FROM " . $site_id . "_search WHERE content_id='" . $oldcontID . "'");
          if(!$_POST['component_' . $s[1] . '_nosearch'])
          {
            sqlQuery("INSERT INTO " . $site_id . "_search SET content_id=$contID");
          }
        }
      }
    }
    if($updated)
    {
      IndexAndCompileAllPages($template_id, $site_id);
      DeleteAllCompiledTemplates($site_id);
      Add_Log("Changed default component values of template '$name' ($template_id)", $site_id);
    }
  }else if(($action == 'saveview')or($action == 'updateview'))
  {
    $template_id = intval($_GET['template_id']);
    $name = $_POST['name'];
    $description = $_POST['description'];
    $parent = $_POST['parent'];
    $view_id = intval($_POST['view_id']);
    $s = '';
    for($f = 0; $f < 20; $f++ )
    {
      if($_POST['uselevel' . $f])
      {
        $s .= intval($_POST['level'.$f]) . ':';
      }
    }
    $levels = $s;
    $s = '';
    for($f = 0; $f < 20; $f++ )
    {
      if($_POST['useparent' . $f])
      {
        $s .= intval($_POST['parent'.$f]) . '|' . IIF($_POST['useimmediateparent'.$f],'1','0') . ':';
      }
    }
    $parents = $s;
    $s = '';
    for($f = 0; $f < 20; $f++ )
    {
      if($_POST['uselanguage' . $f])
      {
        $s .= intval($_POST['language'.$f]) . ':';
      }
    }
    $languages = $s;
    $ind = sqlQueryValue('SELECT max(ind)+1 FROM '.$site_id.'_views WHERE template_id='.$template_id);
    if(!$ind)
      $ind = 1;
//    echo "<br /><br />Values: $ind, $template_id, $name, $description, $levels, $parents, $languages<br /><br />";
    if($action == 'saveview')
    {
      //do not allow view_id to be -1 as -1 represents default (template) values
      $maxid = sqlQueryValue("SELECT max(view_id) FROM ".$site_id."_views");
      if($maxid)
        $newid = '';
      else
        $newid = '2';
      sqlQuery("INSERT INTO ".$site_id."_views (view_id, ind, template_id, name, parent, description, levels, parents, languages)
                VALUES ('$newid', $ind, $template_id, '$name', '$parent', '$description', '$levels', '$parents', '$languages')");
    }else
    {
      sqlQuery("UPDATE ".$site_id."_views SET name='$name', description='$description', parent='$parent',
                   levels='$levels', parents='$parents', languages='$languages'
                   WHERE view_id=$view_id");
    }
    DeleteAllCompiledTemplates($site_id);

  }else if(($action == 'movetemplateup')and($perm_managetemplates))
  {
    $id = $_GET['id'];
    $row = sqlQueryRow("SELECT ind, template_id FROM " . $site_id . "_templates WHERE template_id=$id");
    if($row != null)
    {
      //Obtain the template_id above
      $aboverow = sqlQueryRow("SELECT ind, template_id FROM " . $site_id . "_templates WHERE ind<" . $row['ind'] . " ORDER BY ind DESC LIMIT 1");
      if($aboverow != null)
      {
        sqlQueryValue("UPDATE " . $site_id . "_templates SET ind=" . $aboverow['ind'] . " WHERE template_id=$id");
        sqlQueryValue("UPDATE " . $site_id . "_templates SET ind=" . $row['ind'] . " WHERE template_id=" . $aboverow['template_id']);
      }
    }
  }else if(($action == 'movetemplatedown')and($perm_managetemplates))
  {
    $id = $_GET['id'];
    $row = sqlQueryRow("SELECT ind, template_id FROM " . $site_id . "_templates WHERE template_id=$id");
    if($row != null)
    {
      //Obtain the template_id below
      $belowrow = sqlQueryRow("SELECT ind, template_id FROM " . $site_id . "_templates WHERE ind>" . $row['ind'] . " ORDER BY ind LIMIT 1");
      print_r($belowrow);
      if($belowrow != null)
      {
        sqlQueryValue("UPDATE " . $site_id . "_templates SET ind=" . $belowrow['ind'] . " WHERE template_id=$id");
        sqlQueryValue("UPDATE " . $site_id . "_templates SET ind=" . $row['ind'] . " WHERE template_id=" . $belowrow['template_id']);
      }
    }
  }else if(($action == 'moveviewup')and($perm_managetemplates))
  {
    $id = $_GET['id'];
    $row = sqlQueryRow("SELECT ind, view_id FROM " . $site_id . "_views WHERE view_id=$id");
    if($row != null)
    {
      //Obtain the view_id above
      $aboverow = sqlQueryRow("SELECT ind, view_id FROM " . $site_id . "_views WHERE ind<" . $row['ind'] . " ORDER BY ind DESC LIMIT 1");
      if($aboverow != null)
      {
        sqlQueryValue("UPDATE " . $site_id . "_views SET ind=" . $aboverow['ind'] . " WHERE view_id=$id");
        sqlQueryValue("UPDATE " . $site_id . "_views SET ind=" . $row['ind'] . " WHERE view_id=" . $aboverow['view_id']);
      }
    }
  }else if(($action == 'moveviewdown')and($perm_managetemplates))
  {
    $id = $_GET['id'];
    $row = sqlQueryRow("SELECT ind, view_id FROM " . $site_id . "_views WHERE view_id=$id");
    if($row != null)
    {
      //Obtain the view_id below
      $belowrow = sqlQueryRow("SELECT ind, view_id FROM " . $site_id . "_views WHERE ind>" . $row['ind'] . " ORDER BY ind LIMIT 1");
      print_r($belowrow);
      if($belowrow != null)
      {
        sqlQueryValue("UPDATE " . $site_id . "_views SET ind=" . $belowrow['ind'] . " WHERE view_id=$id");
        sqlQueryValue("UPDATE " . $site_id . "_views SET ind=" . $row['ind'] . " WHERE view_id=" . $belowrow['view_id']);
      }
    }
  }
  else if(($action == 'savevisualcontents')and($perm_managetemplates))
  {
    $template_id = $_GET['template_id'];
    $body = $_POST['contents'];

    $body = str_replace('<SCRIPT language=JavaScript src=\"'.$GLOBALS['cfgWebRoot'].'gui/scripts.js\"></SCRIPT>', "", $body);
    $body = str_replace('<SCRIPT language=JavaScript src=\"?module=templates&amp;action=editorjs&amp;site_id='.$_GET['site_id'].'&amp;template_id='.$template_id.'\"></SCRIPT>', "", $body);

    if ($_SESSION['template_head_'.$template_id])
    {
        $language = $GLOBALS['currentlanguagenum'];
        if (!$language) 
        {
            $row = sqlQueryRow("SELECT language_id, shortname, fullname, encoding FROM " . $site_id . "_languages WHERE is_default = 1");
        }
        else
        {
            $row = sqlQueryRow("SELECT language_id, shortname, fullname, encoding FROM " . $site_id . "_languages WHERE language_id = ".$language);
        }
        $shortname = $row['shortname'];
        $fullname = $row['fullname'];
        $encoding = $row['encoding'];

        //process template head
        preg_match("/(<head>[\s\S]*<\/head>)/i", $body, $matches);
        if ($matches[1])
        {
          $head = $matches[0];
          $head = str_replace($encoding, "{%language:encoding%}", $head);
          $body = str_replace($matches[0], $head, $body);
        }
        unset($_SESSION['template_head_'.$template_id]);
    }

    IndexAndCompileAllPages($template_id, $site_id);
    DeleteAllCompiledTemplates($site_id);

    sqlQuery("UPDATE " . $site_id . "_templates SET body='$body' WHERE template_id='$template_id'");
    Add_Log("Changed contents of template '$name' ($template_id)", $site_id);
  }else if(($action == 'redirect_language_default' || $action == 'redirect_language_strings') && $perm_managetemplates)
  {
    //get current selected language id
    $language_id = intval($_SESSION['currentlanguagenum']);
    $template_id = intval($_GET['id']);
    if($language_id)
    {
      //does view exist?
      $row = sqlQueryRow('SELECT * FROM ' . $site_id . '_views WHERE template_id = ' . $template_id . ' AND language_id = ' . $language_id);
      if(!$row)
      {
        //create new autogenerated view
        $maxid = DB::GetValue("SELECT max(view_id) FROM ".$site_id."_views");
        $newid = $maxid + 1;

        sqlQuery("INSERT INTO `".$site_id."_views` (`view_id`, `ind` , `template_id` , `name` , `parent` , `description` , `levels` , `parents` , `languages` , `language_id` )
                  VALUES (
                  '".$newid."', '0', '".$template_id."', 'autolanguage_".$template_id."_".$language_id."', '0', '', '', '', '', '".$language_id."'
                  )");
        $row = sqlQueryRow('SELECT * FROM ' . $site_id . '_views WHERE template_id = ' . $template_id . ' AND language_id = ' . $language_id);
      }
      if($action == 'redirect_language_default')
        header('Location: index.php?module=pages&site_id='.$site_id.'&action=components_form_frame&id=-' . $row['view_id']);
      else
        header('Location: index.php?module=strings&site_id='.$site_id.'&action=page_strings&id=-' . $row['view_id']);
      die();
    }
  }
?>
