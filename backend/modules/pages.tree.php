<?
die('ok');
$GLOBALS['site_id'] = $_GET['site_id'];
$site_id = $_GET['site_id'];

 //function for sorting data in tree structure
function NodesVisual($data, &$newdata, $parent, $level, $justcount, &$goodl)
{
  $goodlanguage = false;
  $nodecount = 0;
  foreach($data as $key => $row)
  {
    if($parent == $row['parent']){
      $nodecount++;

        $newdata[] = $row;
        $c = count($newdata) - 1;
        $childs = NodesVisual($data, $newdata, $row['page_id'], $level + 1, 0, $g);
        $newdata[$c]['children'] = $childs;
        $goodlanguage = ($g or $goodlanguage);


        if($row['language'] != intval($GLOBALS['currentlanguagenum']))
        {
          //$newdata[$c]['page_id'] = 0;

          if(!$g)
          {
            if($c)
              $newdata = array_slice($newdata, 0, $c);
            else
              $newdata = array();
            $nodecount--;
          }else
          {
            $newdata[$c]['clicklink'] = "Page in " . sqlQueryValue("SELECT fullname FROM " . $GLOBALS['site_id'] . "_languages WHERE language_id = " . intval($row['language'])) . " (" . $row['name'] . ")";
          }
        }else
        {
          $newdata[$c]['clicklink'] = "?redirect_to_whatever";
          $goodlanguage = true;
        }

    }

  }
  $goodl = $goodlanguage;
  return $nodecount;
}

function AddTreeNode($tree, $data, &$f, $parentnode)
{
  if($f < count($data))
  {
    $row = $data[$f];
    $path = "?module=pages&site_id=" . $GLOBALS['site_id'] . "&action=redirectto_graph&id=" . $row['page_id'];
    if(!$row['children'])
    {
      $tree->add_document ($parentnode, $row['title'], $path, "", $row['page_id']);
    }else
    {
      $node = $tree->add_folder ($parentnode, $row['title'], $path, "", "", $row['page_id']);
      for($i = 0; $i < $row['children']; $i++)
      {
        $f++;
        AddTreeNode($tree, $data, $f, $node);
      }
      return $node;
    }
  }
}

function AddTopNodes($tree, $data, &$f, $root, $children)
{
  for($i = 0; $i < $children; $i++)
  {
    AddTreeNode($tree, $data, $f, $root);
    $f++;
  }
}

  //Output frame buttons
  echo '<a href="#" onClick="javascript:window.parent.document.location.href=\'?module=pages&site_id=' . $GLOBALS['site_id'] . '&action=redirectto_graph&id=\'+foldersTree.selectedpage;">Close Frame [X]</a><br /><br />';

  //Obtain page data
  $unsorteddata = sqlQueryData("SELECT page_id, name, title, description, visible, enabled, parent, language FROM " . $site_id . "_pages ORDER BY ind");
  $g = false;
  $children = NodesVisual($unsorteddata, $data, 0, 0, 0, $g);

  //Create and output tree
  require ($GLOBALS['cfgDirRoot'] . "library/class.tree.php");

  $tree = new Tree($GLOBALS['cfgWebRoot'] . "gui/tree");
  $root  = $tree->open_tree ("Site Pages", "", "koks2", ".");

  $f = 0;
  AddTopNodes($tree, $data, $f, $root, $children);

  $tree->close_tree();
?>
