<?
  $perm_managecontents = $GLOBALS['perm_managecontents'];
  if(!$perm_managecontents)
    AccessDenied(True);
  

  require_once ($GLOBALS['cfgDirRoot']."library/".'class.formgen.php');

  $GLOBALS['page_id'] = $_GET['id'];
  $page_id = $_GET['id'];
  $site_id = $GLOBALS['site_id'];
  $component_name = $_GET['component_name'];
  $component_type = $_GET['component_type'];
  $GLOBALS['PageEncoding'] = PageEncoding($page_id, $site_id);
  $GLOBALS['maincharset']  = PageEncoding($page_id, $site_id, false);

  
  if(!$component_name)
  {
    echo "No components selected.";
  }
  if(($_GET['noclose'])and($component_name))
  {
    //refresh parent page

    $newDiv = outputGraphicalComponent($hascontent, $component_type, $component_name, $page_id, 1, '', false, $toolbarfunc);
    $newDiv = borderDiv() . $newDiv;
    $newDiv = parseForJScript($newDiv);


    $obj = new $component_type($component_name);
    addDefaultProperties($obj);
    if($obj->getProperty('visible') == 0)
    {
      $displaymode = 'block';
      $optcol = '800000';
    }else
    {
      $displaymode = 'none';
      $optcol = '0000FF';
    }



    echo '
    <script language="JavaScript">

<!--

  var win = window.parent.dialogArguments[0];
  var el = window.parent.dialogArguments[1];
  try{
    el.innerHTML = "' . $newDiv . '";
    el.set_tb_cnt = "' . IIF($toolbarfunc, ParseForJScript($toolbarfunc), 'null') . '";
    el.style.display = "' . $displaymode . '";
    window.parent.selectcomponent.options.item(window.parent.selectcomponent.selectedIndex).style.color = "'.$optcol.'";
  }catch(e){}

//-->

</script> ';
  }

//  echo '<form name="Form" action="?module=' .$GLOBALS['module']. '&site_id=' .$site_id. '&page_id=' .$page_id. '&component_type=' .$component_type. '&component_name=' .$component_name .'&id=' .$page_id. '&action=updatecontrols&noclose=1" method=POST>
//          <input type="hidden" name="id" value="' . $page_id . '">';
//output default values buttons
  echo '
  <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%" id="AutoNumber1" height="28">
  <tr>
    <td id="poga1" width="*" height="28" align="center" style="border-style: solid; border-width: 1; padding-left: 4; padding-right: 4; padding-top: 1; padding-bottom: 1; background-color: #C8C8C8">
    <a href="#" onclick="javascript:poga1.style.backgroundColor = \'#C8C8C8\'; poga2.style.backgroundColor = \'#F0F0F0\'; pageprops.style.display=\'block\';defprops.style.display=\'none\';document.Form = Form1;">Page Values</a></td>
    <td width="8" height="28" align="center">&nbsp;</td>
    <td id="poga2" width="*" height="28" align="center" style="border-style: solid; border-width: 1; padding-left: 4; padding-right: 4; padding-top: 1; padding-bottom: 1; background-color: #F0F0F0">
    <a href="#" onclick="javascript:poga2.style.backgroundColor = \'#C8C8C8\'; poga1.style.backgroundColor = \'#F0F0F0\'; pageprops.style.display=\'none\';defprops.style.display=\'block\';document.Form = Form2;">Default Values</a></td>

  </tr>
</table>';
//if ($_GET['action']=='updatecontrols')
//{
    echo '
    <script>
        function showimage()
        {
            document.all.prgbar.style.display="block";    
        }
        //document.all.prgbar.style.display="none";
        //setTimeout("hideimage();",5000);
    </script>
    ';
    echo '<div id="prgbar" align="center" style="padding-top:3px;display:none;"><img src="'.$GLOBALS['cfgWebRoot'].'gui/images/progresbar.gif" width="95%" height="10"></div>';
//}
///
  echo '<div id="pageprops">';
  $form_data = Array();
  if(HasComponentPermissionPage($site_id, $page_id, $component_name))
  {
    displayPropertiesTable($component_type, $component_name, $site_id, $page_id, $form_data, true);
  }
  $Inspector = new Inspector();
  $Inspector->properties = $form_data;
  $Inspector->opened = true;
  $Inspector->buttons =false;
  $Inspector->cancel ="close";
  $Inspector->name = "Form1";
  $Inspector->action = '?module=' .$GLOBALS['module']. '&site_id=' .$site_id. '&page_id=' .$page_id. '&component_type=' .$component_type. '&component_name=' .$component_name .'&id=' .$page_id. '&action=updatecontrols&noclose=1';
  $Inspector->displayplus = False;
  $Inspector->displaytitle = False;
  echo $Inspector->output();
  echo '</div>';
///
  echo '<div id="defprops" style="display:none">';
  $form_data = Array();
  displayPropertiesTable($component_type, $component_name, $site_id, -1, $form_data, true);
  $Inspector = new Inspector();
  $Inspector->properties = $form_data;
  $Inspector->opened = true;
  $Inspector->buttons =false;
  $Inspector->cancel ="close";
  $Inspector->name = "Form2";
  $Inspector->hidecheckboxes = true;
  $Inspector->action = '?module=' .$GLOBALS['module']. '&site_id=' .$site_id. '&page_id=' .$page_id. '&component_type=' .$component_type. '&component_name=' .$component_name .'&id=' .$page_id. '&action=updatecontrolsdef&noclose=1';
  $Inspector->displayplus = False;
  $Inspector->displaytitle = False;
  echo $Inspector->output();
  echo '</div>';

  echo '<script language="JavaScript" type="text/javascript">
/*<![CDATA[*/
document.Form = Form1;
/*]]>*/
</script>
';


?>
