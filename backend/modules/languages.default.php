<?

$site_id = $GLOBALS['site_id'];
$perm_managetemplates = $GLOBALS['perm_managetemplates'];


// Let's try table generator

$cols = Array(

  "shortname"   => Array(
    "width"     => "20%",
    "title"     => "{%langstr:short_name%}",
    "format"    => "<a href=\"#\" OnClick=\"javascript:openDialog('?module=languages&site_id=" . $site_id . "&action=properties_form&id={%language_id%}', 400, 180);\">{%shortname%}</a>"
  ),

  "fullname"    => Array(
    "width"     => "30%",
    "title"     => "{%langstr:col_name%}"
  ),

	"is_default"    => Array(
    "width"     => "20%",
    "title"     => "{%langstr:default%}"
  ),

	"disabled"    => Array(
    "width"     => "20%",
    "title"     => "{%langstr:disabled%}"
  ),

);

if(!$perm_managetemplates)
  unset($cols['shortname']['format']);

$data = sqlQueryData("SELECT language_id, shortname, fullname, encoding,is_default,disabled FROM " . $site_id . "_languages ORDER BY is_default DESC");
foreach($data as $id => $row){
	$data[$id]['is_default'] = $row['is_default'] ? lcms("Jā") : lcms("Nē");
	$data[$id]['disabled'] = $row['disabled'] ? lcms("Jā") : lcms("Nē");
}

require($GLOBALS['cfgDirRoot']."library/"."class.datagrid.php");

$Table = new DataGrid();
$Table->cols = $cols;
$Table->data = $data;
$Table->footer = false;
$Table->rowformat = " onClick=\"javascript:HighLightTR(this, {%language_id%});if(SelectedRowID){ EnableToolbarButtons(che); }else { DisableToolbarButtons(che); }\"";
echo $Table->output();

echo '<script language="JavaScript" type="text/javascript">
/*<![CDATA[*/
che = new Array("props", "def", "del");
DisableToolbarButtons(che);
/*]]>*/
</script>';


?>
