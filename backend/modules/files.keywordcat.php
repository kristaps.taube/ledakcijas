<?

$perm_managefiles = $GLOBALS['perm_managefiles'];
if(!$perm_managefiles)
  AccessDenied(True);

$form_data = Array(

  "title"    => Array(
    "label"     => "{%langstr:title%}:",
    "size"      => "35",
    "type"      => "text"
  )

);

require($GLOBALS['cfgDirRoot']."library/"."class.formgen.php");
$FormGen = new FormGen();
$FormGen->action = "?module=files&action=savekeywordcat&site_id=".$_GET['site_id'];
$FormGen->title = "{%langstr:add_new_category%}";
$FormGen->cancel = 'close';
$FormGen->properties = $form_data;
$FormGen->buttons = true;
echo $FormGen->output();

?>
