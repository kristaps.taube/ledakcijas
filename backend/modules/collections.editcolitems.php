<?
$collection= $GLOBALS['collection'];

$category_id = $collection->collection_id;

$cols = $collection->columns;

foreach ($cols as $key=>$col)
{
    if ($col['timestamp'] && substr($key, 0, 3) == 'col' && is_numeric(substr($key,3)))
    {
        $date_cols[] = substr($key,3);
    }
}

$site_id = $_GET['site_id'];
$page_id = $_GET['page_id'];
if($page_id)
{
  $GLOBALS['PageEncoding'] = PageEncoding($page_id, $site_id);
  $GLOBALS['maincharset']  = PageEncoding($page_id, $site_id, false);
}


$action = $_GET['action'];
$category_id = $_GET['category_id'];
$create = ($action == 'newcolitem');
$id = $_GET['id'];

if(!$create)
{
  $action = "?module=".$GLOBALS['module']."&site_id=".$site_id."&page_id=$page_id&action=modcolitem&category_id=$category_id&coltype=".$_GET['coltype'].'&justedit='.$_GET['justedit']."&id=".$id;
}else{
  $action = "?module=".$GLOBALS['module']."&site_id=".$site_id."&page_id=$page_id&action=addcolitem&category_id=$category_id&coltype=".$_GET['coltype'].'&justedit='.$_GET['justedit'];
}                 

$collection->displayAddEditForm($create, $id, $action);



