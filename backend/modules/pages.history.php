<?

$site_id = $GLOBALS['site_id'];
$id = $_GET["id"];

$page_id = sqlQueryValue("select page_id from ".$site_id."_pagesdev where pagedev_id=".$id);

$cols = Array(

  "preview"     => Array(
    "width"     => "5%",
    "align"     => "center",
    "title"     => "{%langstr:col_prev%}",
  ),

  "title"        => Array(
    "width"     => "30%",
    "title"     => "{%langstr:title%}",
  ),

  "lastmod"        => Array(
    "width"     => "30%",
    "title"     => "{%langstr:col_modified%}",
  ),

  "modby"        => Array(
    "width"     => "30%",
    "title"     => "{%langstr:modify_by%}",
  ),
/*
  "description" => Array(
    "width"     => "30%",
    "title"     => "Description"
  ),
*/
  "restore"     => Array(
    "width"     => "15%",
    "title"     => ""
  )

);

$h_pages = sqlQueryData("SELECT p.pagedev_id, p.title, p.lastmod, p.description, users.username FROM ".$site_id."_pagesdev as p LEFT JOIN users ON users.user_id=p.modby WHERE page_id=".$page_id."  ORDER BY lastmod DESC");

//published page
$page = sqlQueryRow("SELECT p.page_id, p.title, p.lastmod, p.description, users.username FROM ".$site_id."_pages as p LEFT JOIN users ON users.user_id=p.modby WHERE page_id=".$page_id."  ORDER BY lastmod DESC");


/*
//?module=wap&amp;site_id=11&amp;action=page_redirect&amp;id=16
$data[] = array("preview"=>"<a href=\"?module=pages&site_id=$site_id&action=page_redirect&id=".$page['page_id']."\"  target=\"_blank\"><img src=\"gui/images/ico_view.gif\" width=\"16\" height=\"16\" border=\"0\"></a>",
                    "title"       => $page['title'].' <i>(published)</i>',
                    "description" => $page['description'],
                    "lastmod"     => date("Y-m-d H:i:s",$page['lastmod']),
                    "modby"       => $page['username'],
                    "restore"     => "<a href=\"?module=pages&action=restore_history&page_id=".$page['page_id']."&site_id=$site_id&published=1\">restore</a>"
                    );
*/
//active dev page goes first
//$active_dev = true;
foreach ($h_pages as $h_page)
{
    if (!$active_dev)
    $data[] = array("preview"=>"<a href=\"?module=pages&site_id=$site_id&action=page_redirect&pagedev_id=".$h_page['pagedev_id']."&id=".$page_id."&showdevpage=1\"  target=\"_blank\"><img src=\"gui/images/ico_view.gif\" width=\"16\" height=\"16\" border=\"0\"></a>",
                    "title"       => $h_page['title'],
                    "description" => $h_page['description'],
                    "lastmod"     => date("Y-m-d H:i:s",$h_page['lastmod']),
                    "modby"       => $h_page['username'],
                    "restore"     => ($h_page['pagedev_id']!=$id?"<a href=\"javascript:if (document.all['publish'].checked) document.location='?module=pages&action=restore_history&pagedev_id=".$h_page['pagedev_id']."&id=".$page_id."&site_id=$site_id&publish=1'; else document.location='?module=pages&action=restore_history&pagedev_id=".$h_page['pagedev_id']."&id=".$page_id."&site_id=$site_id&publish=0';\">{%langstr:col_restore%}</a>":"")
                    );
    $active_dev = false;
}
require($GLOBALS['cfgDirRoot']."library/"."class.datagrid.php");
echo '<div style="width:500px;padding-right:12px;">';
$Table = new DataGrid();
$Table->cols = $cols;
$Table->data = $data;
$Table->footer = false;
$Table->rowformat = " id=\"tablerow{%pagedev_id%}\" ";
echo $Table->output();
echo '<div align="right">{%langstr:publish_after_restore%}: <input type="checkbox" name="publish"></div>';
echo '</div>';

?>
