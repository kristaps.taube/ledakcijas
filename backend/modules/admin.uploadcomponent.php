<?
$cat_ar = sqlQueryData('SELECT category_id, category_name FROM component_categories ORDER BY category_name');
foreach ($cat_ar as $id => $value)
{
  $categories[$id] = $value['category_id'].':'.$value['category_name'];
}
$form_data = Array(

  "userfile"    => Array(
    "label"     => "{%langstr:filename%}:",
    "size"      => "35",
    "type"      => "file",
    "uploadonly" => true
  ),

  "category"    => Array(
    "label"     => "{%langstr:category%}:",
    "type"      => "list",
    "lookup"    => $categories)

);

require($GLOBALS['cfgDirRoot']."library/"."class.formgen.php");
$FormGen = new FormGen();
$FormGen->action = "?module=components&action=savecomponent";
$FormGen->title = "{%langstr:add_component%}";
$FormGen->properties = $form_data;
$FormGen->cancel = 'close';
$FormGen->buttons = true;
echo $FormGen->output();


?>
