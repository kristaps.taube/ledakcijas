<?

// Define form

$form_data = Array(

  "template_id" => Array(
    "label"     => "ID:",
    "type"      => "hidden"
  ),

  "ind"         => Array(
    "noshow"    => true
  ),

  "name"        => Array(
    "label"     => "{%langstr:col_name%}:",
    "type"      => "str"
  ),

  "description" => Array(
    "label"     => "{%langstr:col_description%}:",
    "type"      => "html",
    "rows"      => "4",
    "cols"      => "30"
  )

);

// Fill some values
  $id = $_GET["id"];
  $action = $_GET['action'];
  $create = ($action == 'create');
  $site_id = $GLOBALS['site_id'];

  if(!$create){
    $row = sqlQueryRow("SELECT * FROM " . $site_id . "_templates WHERE template_id=$id");
    $form_data['template_id']['value'] = $id;
    $form_data['name']['value'] = $row['name'];
    $form_data['description']['value']  =$row['description'];
  }
  
  $invalid = split(':', $_GET['invalid']);
  foreach($invalid as $i)
  {
    if($i)
      $form_data[$i]['error'] = true;
  }

  foreach(array_keys($_GET) as $key)
  {
    $s = Split('_', $key, 2);
    if($s[0] == 'formdata')
      $form_data[$s[1]]['value'] = stripslashes($_GET[$key]);
  }
  
  if($_GET['jsmessage'])
  {
    echo '<script language="JavaScript">
          <!--
          alert("' . stripslashes($_GET['jsmessage']) . '");
          <!---->
          </script>';
  }

// Use FormGen to bring this out

  require($GLOBALS['cfgDirRoot']."library/"."class.formgen.php");
  $FormGen = new FormGen();
  if(!$create)
  {
    $FormGen->action = "?module=".$GLOBALS['module']."&site_id=".$site_id."&action=modtemplate";
  }else{
    $FormGen->action = "?module=".$GLOBALS['module']."&site_id=".$site_id."&action=addtemplate";
  }
  $FormGen->cancel = "close";
  if(!$create)
  {
    $FormGen->title = "{%langstr:template_properties%} : " . $row['title'];
  }else{
    $FormGen->title = "{%langstr:create_new_template%}";
  }
  $FormGen->properties = $form_data;
  $FormGen->buttons = true;
  echo $FormGen->output();

?>
