<?
$component =  sqlQueryData('SELECT component_id, category_id, (SELECT category_name FROM component_categories WHERE category_id = components.category_id) as category_name, type FROM components WHERE component_id = '.$_GET['component_id']);
$cat_ar = sqlQueryData('SELECT category_id, category_name FROM component_categories WHERE category_id != '.$component[0]['category_id'].'  ORDER BY category_name');
foreach ($cat_ar as $id => $value)
{
  $categories[$id] = $value['category_id'].':'.$value['category_name'];
}


$form_data = Array(

  "category"    => Array(
    "label"     => "{%langstr:category%}:",
    "type"      => "list",
    "lookup"    => $categories)

);
require($GLOBALS['cfgDirRoot']."library/"."class.formgen.php");
$FormGen = new FormGen();
$FormGen->action = "?module=components&action=savechangedcomponent&component_id=".$_GET['component_id'];
$FormGen->title = "{%langstr:component%} : " .$component[0]['type'].' : '.$component[0]['category_name'];
$FormGen->properties = $form_data;
$FormGen->cancel = 'close';
$FormGen->buttons = true;
echo $FormGen->output();


?>
