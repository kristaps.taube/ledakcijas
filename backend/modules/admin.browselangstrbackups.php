<?

if(!$GLOBALS['perm_accessadmin'])
  Die('Access Forbidden');

require_once($GLOBALS['cfgDirRoot']."library/"."func.files.php");

$site_id = $_GET['site_id'];

$cols = Array(

  "name"        => Array(
    "width"     => "50%",
    "title"     => "{%langstr:col_name%}"
  ),

  "size"    => Array(
    "width"     => "10%",
    "title"     => "{%langstr:col_size%}"
  ),

  "date"     => Array(
    "width"     => "30%",
    "title"     => "{%langstr:col_created%}"
  ),

   "restore"    => Array(
    "width"     => "5%",
    "title"     => "{%langstr:col_restore%}",
    "align"     => "center",
    "format"    => '<a href="#" OnClick="if (confirm(\'{%langstr:sure_restore_langstr%}\')) {openDialog(\'?module=admin&action=ask_restorelangstr&site_id='.$site_id.'&filename={%title%}&site_id='.$GLOBALS['site_id'].'\', 400, 225);}">{%langstr:restore%}</a>'
  ),
 

  "del"    => Array(
    "width"     => "5%",
    "title"     => "{%langstr:col_del%}",
    "align"     => "center",
    "format"    => '<a href="?module=admin&action=deletelangstr&site_id='.$site_id.'&file={%title%}" OnClick="return confirm(\'{%langstr:sure_to_delete_item%}\')"><img src="gui/images/ico_del.gif" width="16" height="16" border="0" alt="Delete"></a>'
  )

);


function read_files($webroot, $dir, $path) {
  global $cfgCmsDirRoot, $cfgWebRoot;

  function outputFilename($filepath, $webpath, $webdir, $filename) {
	$download_path = ereg_replace("backend/", "", $GLOBALS['cfgWebRoot']);

    $ext = strtolower(substr($filename,-4,4));
    $html = '<img src="'.$GLOBALS['cfgWebRoot'].'gui/images/ico_file.gif" width="16" height="16" align="absmiddle"> <a href="'.$download_path.'/backup/langstrings/'.$filename.'">'.$filename.'</a>';
    return $html;
  }

  /*
OnClick="openDialog(\'?module=admin&action=ask_restore&site_id='.$GLOBALS['site_id'].'&filename='.urlencode($filename).'\', 400, 225);"
  */

  chdir($dir);

  $handle = opendir($dir);
  while ($file = readdir($handle)) {
    if (is_dir($file) && $file != ".")
      $dirlist[] = $file;
    elseif (is_file($file))
      $filelist[] = $file;
  }
  closedir($handle);

  $i = 0;
  //Don't display subdirectories for backups
  /*asort($dirlist);
  foreach($dirlist as $file) {
    if (!($file == '..' && $path == '')) {
      if ($file == '..') $url = fileUp($path); elseif ($path == '') $url = $file; else $url = $path.'/'.$file;
      $thisfile['name'] = '<img src="gui/images/ico_folder.gif" width="16" height="16" align="absmiddle"> <a href="?module=files&path='.$url.'&site_id='.$GLOBALS['site_id'].'">'.$file.'</a>';
      $thisfile['title'] = $file;
      $thisfile['size'] = fileSizeStr($file);
      $thisfile['date'] = date("d-m-Y H:i:s", filectime($file));
      //if ($file != '..') $thisfile['del'] = 'x';
      $data[$i++] = $thisfile;
    }
  } */
  asort($filelist);
  foreach($filelist as $file) {
    if ($path != "") $path = fileAddBackslash($path);
    $url = $path.$file;
    $thisfile['name'] = outputFilename(fileAddBackslash($dir).$file, fileAddBackslash($webroot).$url, $url, $file);
    //'<img src="gui/images/ico_file.gif" width="16" height="16" align="absmiddle"> <a href="'.$url.'" target="_blank">'.$file.'</a>';
    $thisfile['title'] = $file;
    $thisfile['size'] = fileSizeStr($file);
    $thisfile['date'] = date("d-m-Y H:i:s", filectime($file));
    $data[$i++] = $thisfile;
  }

  chdir($cfgCmsDirRoot);

  return $data;
}


if(!$GLOBALS['cfgBackupRoot'])
  $cfgBackupRoot = $GLOBALS['cfgDirRoot'] . "backup/";
else
  $cfgBackupRoot = $GLOBALS['cfgBackupRoot'];

$directory = $cfgBackupRoot."langstrings";
$webroot = sqlQueryValue("SELECT domain FROM sites WHERE site_id = '".$GLOBALS['site_id']."'");
$path = "";


$data = read_files('http://'.$webroot.'/', $directory, $path);

echo '
<script language="JavaScript">
  var selrow = ' . (int) ($data[0][0]) . ';
</script>';

require($GLOBALS['cfgDirRoot']."library/"."class.datagrid.php");

$Table = new DataGrid();
$Table->cols = $cols;
$Table->data = $data;
$Table->footer = false;
$Table->padding = "2";
$Table->rowformat = " onClick=\"javascript:HighLightTR(this, '{%title%}');if(SelectedRowID){ EnableToolbarButtons(che); }else { DisableToolbarButtons(che); }\"";
echo("\n<br /><p><span class=\"gray\">{%langstr:backup_available_langstrings%}:</span> </p>\n");
echo $Table->output();

echo '<script language="JavaScript" type="text/javascript">
/*<![CDATA[*/
che = new Array("ren");
DisableToolbarButtons(che);
/*]]>*/
</script>';

?>
