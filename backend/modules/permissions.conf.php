<?

$action = $_GET["action"];
$perm_manageperms = CheckPermission(4);

$data = $_GET['data'];
$data2 = $_GET['data2'];


$GLOBALS['PageEncoding'] = PageEncoding(0, $site_id);
$GLOBALS['maincharset']  = PageEncoding(0, $site_id, false);
$GLOBALS['PageEncoding'] = FullEncoding($GLOBALS['currentlanguage']);
$GLOBALS['maincharset']  = $GLOBALS['currentlanguage'];


if (($action=="items" && $_GET['mod']=="wappage_form") || $action=="") {
  if (isset($GLOBALS["collapsebranch"])) $_SESSION['pwapexpanded' . $_GET['collapsebranch']] = 0;
  if (isset($GLOBALS["expandbranch"])) $_SESSION['pwapexpanded' . $_GET['expandbranch']] = 1;
}


if($_GET['site_id'])
{
  $GLOBALS['tabs'] = 'site';
  $site_id = $_GET['site_id'];
  if($site_id == $GLOBALS["currentUserSiteID"])
    $perm_managesiteperms = CheckPermission(10, $site_id);;
}

if ($_GET['mod']=="site_form")
   $GLOBALS['permission_type'] = 1;
else if ($_GET['mod']=="page_form")
   $GLOBALS['permission_type'] = 2;
else if ($_GET['mod']=="template_form")
   $GLOBALS['permission_type'] = 3;
else if ($_GET['mod']=="wappage_form")
   $GLOBALS['permission_type'] = 4;
else if ($_GET['mod']=="waptemplate_form")
   $GLOBALS['permission_type'] = 5;
else 

   $GLOBALS['permission_type'] = 0;


switch ($action) {
  case "site_form":
    $GLOBALS['permission_type'] = 1;
    $docTitle = "Site Permissions";
    break;
  case "page_form":
    $GLOBALS['permission_type'] = 2;
    $docTitle = "Page Permissions";
    //Allow user change page access permissions if he has permission to manage site pages
    if(!$perm_managesiteperms)
      $perm_managesiteperms = CheckPermission(8, $site_id);;
    break;
  case "template_form":
    $GLOBALS['permission_type'] = 3;
    $docTitle = "Template Permissions";
    //Allow user change page access permissions if he has permission to manage site templates
    if(!$perm_managesiteperms)
      $perm_managesiteperms = CheckPermission(9, $site_id);;
    break;
  case "wappage_form":
    $GLOBALS['permission_type'] = 4;
    $docTitle = "WAP Page Permissions";
    //Allow user change page access permissions if he has permission to manage site pages
    if(!$perm_managesiteperms)
      $perm_managesiteperms = CheckPermission(19, $site_id);;
    break;
    $GLOBALS['permission_type'] = 5;
    $docTitle = "WAP Template Permissions";
    //Allow user change page access permissions if he has permission to manage site wap templates
    if(!$perm_managesiteperms)
      $perm_managesiteperms = CheckPermission(23, $site_id);;
    break;
  //----------------------------------
  case "navigation":
    $docTemplate = "form.htm";
    $docScripts['body'] = "permissions.navigation.php";
    break;
  case "usersgroups":
    $docTemplate = "form.htm";
    $docScripts['body'] = "permissions.usersgroups.php";
    break;
  case "items":

    $docTemplate = "form.htm";
    if ($_GET['mod']=="page_form" && $_GET['single_page'])
    {
        $docScripts['body'] = "permissions.singlepage.php";
    }
    else if ($_GET['mod']=="template_form" && $_GET['single_template'])
    {
        $docScripts['body'] = "permissions.singletemplate.php";
    }
    else
    {
        $docScripts['body'] = "permissions.items.php";
    }
    break;
  case "permlist":
    $docTemplate = "form.htm";
    $docScripts['body'] = "permissions.permlist.php";
    break;
  case "buttons":
    $docTemplate = "form.htm";
    $docScripts['body'] = "permissions.buttons.php";
    break;
  case "permission_frame":
    $docTemplate = "permframeset.htm";
    
    $url = parse_url($_SERVER['REQUEST_URI']);
    $url_1 = str_replace("permission_frame","usersgroups",$url['query']);
    $docStrings['rightframesrc1'] = "?".$url_1;

    $url_2 = str_replace("permission_frame","items",$url['query']);
    $docStrings['rightframesrc2'] = "?".$url_2;

    $url_3 = str_replace("permission_frame","permlist",$url['query']);
    $docStrings['rightframesrc3'] = "?".$url_3;

    $url_4 = str_replace("permission_frame","buttons",$url['query']);
    $docStrings['rightframesrc4'] = "?".$url_4;
    
    $url = parse_url($_SERVER['REQUEST_URI']);
    $url['query'] = str_replace("permission_frame","navigation",$url['query']);

    $docStrings['leftframesrc'] = "?".$url['query'];

    $docStrings['onload'] = 'frames.permlist.pages()';
    break;
  //----------------------------------
  default:
    $GLOBALS['permission_type'] = 0;
    $docTitle = "Access Permissions";
    break;
}
$docScripts['exec'] = "permissions.exec.php";
//print_r($_GET);
//print_r($_POST);
//echo "mp".$perm_manageperms." msp".$perm_managesiteperms;

if(($_GET['close'])and($_GET['applied']))
    echo '
    <script>
        function appendQueryString(str)
        {
          var date = new Date();
          var name = "" + date.getTime();
          if((str == null) || (str.length == 0))  str = "?";
          else
          {
            if(str.charAt(0) != "?")  str = "?" + str;
            if(str.length != 1)  str += "&";
          }
          str += name;
          return str;
        }

        if (parent.window.opener != null) {
            //alert(parent.window.opener.location.search);
            parent.window.opener.location.search = appendQueryString(parent.window.opener.location.search)
        } else if(window.parent != null) {
          if (window.parent.dialogArguments != null) {
            var win = window.parent.dialogArguments[0];
            if(win != null) {
              win.location.search = appendQueryString(win.location.search);
            }
          }
        }

        //close();
       parent.window.close();
    </script>
    ';

/*
  $docTemplate = "close.htm";
else
  $docTemplate = "form.htm";
  
$docScripts['body'] = "permissions.output.php";
$docScripts['exec'] = "permissions.exec.php";
*/
