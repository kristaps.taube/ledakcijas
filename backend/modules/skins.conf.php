<?

$GLOBALS['xmlrpc_server'] = 'demo0.datateks.lv';
$GLOBALS['xmlrpc_path'] = '/mini_services/';

$GLOBALS['site_id'] = $_GET["site_id"];
$site_id = $_GET["site_id"];
$page_id = $_GET['page_id'];

if(!$site_id)
  die("No site specified");

$action = $_GET["action"];
$GLOBALS['tabs'] = 'site';

unset($perm_modsite);
if(($site_id == $GLOBALS["currentUserSiteID"])or(!$GLOBALS["currentUserSiteID"]))
  $perm_modsite = CheckPermission(6, $site_id);

if(!$perm_modsite)
{
  echo "Permission Denied!";
  AccessDenied(True);
}

$GLOBALS['PageEncoding'] = PageEncoding($_GET['id'], $site_id);
$GLOBALS['maincharset']  = PageEncoding($_GET['id'], $site_id, false);

$action = $_GET["action"];
require($GLOBALS['cfgDirRoot']."library/"."class.toolbar.php");

switch ($action) {
  case 'downloadlist':
    $docTemplate = "main.htm";
    $docTitle = "Download skins";
    $docScripts['body'] = "skins.downloadlist.php";
    break;

  case 'download':
  case 'delskin':
  case 'applyskin':
    $docScripts['exec'] = "skins.exec.php";
    //header('Location: ?module=skins&site_id=' . $GLOBALS['site_id']);
    break;

  case 'list':
    $docTemplate = "main.htm";
    $docTitle = "Skins";
    $docScripts['body'] = "skins.list.php";

    require($GLOBALS['cfgDirRoot']."library/"."class.toolbar2.php");
    $Toolbar = new Toolbar2();

    $Toolbar->AddSpacer();
    $Toolbar->AddButtonImageText('apply', 'idea', "{%langstr:skins_apply%}", "javascript: if(SelectedRowID)window.location='?module=skins&action=applyskin&site_id=$site_id&id='+SelectedRowID;", "", 71, "{%langstr:hint_apply_skin%}", "");
    $Toolbar->AddSeperator();
    $Toolbar->AddButtonImage('del', 'delete', "{%langstr:skins_del%}", "javascript: if(SelectedRowID)window.location='?module=skins&action=delskin&site_id=$site_id&id='+SelectedRowID;", "", 31, "{%langstr:hint_delete_skin%}", "{%langstrjs:ask_delete_skin%}");

    $docStrings['toolbar'] =  $Toolbar->output();

    break;

  default:
    $docTemplate = "main.htm";
    $docTitle = "Skins";
    $docScripts['body'] = "skins.default.php";

    break;

}

?>
