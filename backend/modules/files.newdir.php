<?

$perm_managefiles = $GLOBALS['perm_managefiles'];
if(!$perm_managefiles)
  AccessDenied(True);

$form_data = Array(

  "name"    => Array(
    "label"     => "{%langstr:files_directory_name%}:",
    "size"      => "30",
    "type"      => "string"
  )

);

if ($GLOBALS['path'] == '') {
  $form_data['name']['type'] = "list";
  $form_data['name']['value'] = "images";
  $form_data['name']['lookup'] = Array('images:images', 'files:files', 'upload:upload');
}
if ($_GET['dironly']) $param = '&dironly=1';
else $param ='';

if ($_GET['dironly']) $action = "dirlist";
else $action = "list";

require($GLOBALS['cfgDirRoot']."library/"."class.formgen.php");
$FormGen = new FormGen();
$FormGen->action = "?module=files&action=mkdir&site_id=".$GLOBALS['site_id']."&path=".$GLOBALS['path'].$param."&view=".$GLOBALS['view'].($GLOBALS['close'] ? "&close=".$GLOBALS['close'] : "");
$FormGen->title = "{%langstr:docs_make_directory%}";
$FormGen->cancel = ($GLOBALS['close'] ? "?module=files&action=".$action."&site_id=".$GLOBALS['site_id']."&path=".$GLOBALS['path']."&view=".$GLOBALS['view'] : 'close');;
$FormGen->properties = $form_data;
$FormGen->buttons = true;
echo $FormGen->output();

echo '
    <script language="JavaScript">
        try {
        window.parent.Form.Ok.disabled = true;
        } catch (e) {}
    </script>
';
?>
