<?php

$site_id = $GLOBALS['site_id'];
$page_id = $_GET["id"];

$params = array(
  'page_id' => $page_id,
  'use_pagecat' => 1,
  'show_pageprop' => 1,
  'type' => 'html',
  'fieldparams' => array(
    'rows' => 4
  )
);

//option('meta_keywords', null, 'Meta keywords', $params, null);
option('meta_description', null, 'Meta description', $params, null);
option('title_tag', null, 'Title tag', array('page_id' => $page_id,'use_pagecat' => 1,'show_pageprop' => 1), null);

$row = getOptionByPath('_pages\\'.$page_id, 0, $site_id);
//if (!isset($row['id'])) return;

require($GLOBALS['cfgDirRoot']."library/"."class.formgen.php");

$title = sqlQueryValue("SELECT title FROM `".$site_id."_pages` WHERE page_id=".$page_id);
echo displayAndProcessOptionForms($row['id'], $site_id, true, $title);

