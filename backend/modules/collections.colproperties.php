<?

$collection= $GLOBALS['collection'];

$category_id = $collection->collection_id;

$site_id = $_GET['site_id'];
$page_id = $_GET['page_id'];
if($page_id)
{
  $GLOBALS['PageEncoding'] = PageEncoding($page_id, $site_id);
  $GLOBALS['maincharset']  = PageEncoding($page_id, $site_id, false);
}

$form_data = $collection->colproperties;
foreach($form_data as $key => $val)
{
  $form_data[$key]['default'] = true;
}

//read property values
$data = sqlQueryData('SELECT * FROM ' . $site_id . '_colproperties WHERE coltype = "' . $collection->type . '" AND collection_id=-1');
foreach($data as $row)
{
  if(isset($form_data[$row['propertyname']]))
  {
    $form_data[$row['propertyname']]['value'] = $row['propertyvalue'];
    $form_data[$row['propertyname']]['default'] = true;
  }
}
$data = sqlQueryData('SELECT * FROM ' . $site_id . '_colproperties WHERE coltype = "' . $collection->type . '" AND collection_id=' . $category_id);
foreach($data as $row)
{
  if(isset($form_data[$row['propertyname']]))
  {
    $form_data[$row['propertyname']]['value'] = $row['propertyvalue'];
    $form_data[$row['propertyname']]['default'] = false;
  }
}

require($GLOBALS['cfgDirRoot']."library/"."class.formgen.php");
$Inspector = new Inspector();
$Inspector->properties = $form_data;
$Inspector->buttons =true;
$Inspector->cancel ="close";
$Inspector->name = "Form";
$Inspector->hidecheckboxes = false;
$Inspector->action = "?module=".$GLOBALS['module']."&site_id=".$site_id."&page_id=$page_id&action=updatecolproperties&category_id=$category_id&coltype=".$_GET['coltype'];
echo $Inspector->output();

?>