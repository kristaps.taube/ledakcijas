<?

$perm_manageusers = $GLOBALS['perm_manageusers'];
$perm_managesiteusers = $GLOBALS['perm_managesiteusers'];
$site_id = $GLOBALS['site_id'];

$cols = Array(

  "username"    => Array(
    "width"     => "25%",
    "title"     => "{%langstr:username%}",
    "format"    => "<a href=\"#\" OnClick=\"javascript:openDialog('?module=users&action=properties_form&site_id=$site_id&id={%user_id%}', 400, 500);\">{%username%}</a>"
  ),

  "fname"       => Array(
    "width"     => "15%",
    "title"     => "{%langstr:first_name%}"
  ),

  "lname"       => Array(
    "width"     => "15%",
    "title"     => "{%langstr:last_name%}"
  ),

  "email"       => Array(
    "width"     => "15%",
    "title"     => "{%langstr:e_mail%}",
    "format"    => "<a href=\"mailto:{%email%}\">{%email%}</a>"
  )


);

if((!$perm_manageusers)and(!$perm_managesiteusers))
{
  unset($cols['username']['format']);
}

if($site_id)
{
  $data = sqlQueryData("SELECT user_id,username,fname,lname,email FROM users WHERE site_id='$site_id'");
}else
{
  $data = sqlQueryData("SELECT user_id,username,fname,lname,email FROM users WHERE site_id=0");
}

echo '
<script language="JavaScript">
  var selrow = ' . (int) ($data[0][0]) . ';
</script>';


require($GLOBALS['cfgDirRoot']."library/"."class.datagrid.php");

$Table = new DataGrid();
$Table->cols = $cols;
$Table->data = $data;
$Table->footer = false;
$Table->rowformat = " onClick=\"javascript:HighLightTR(this, {%user_id%});if(SelectedRowID){ EnableToolbarButtons(che); }else { DisableToolbarButtons(che); }\"";
echo $Table->output();

echo '<script language="JavaScript" type="text/javascript">
/*<![CDATA[*/
che = new Array("props", "del");
DisableToolbarButtons(che);
/*]]>*/
</script>';



?>
