<?
$GLOBALS['site_id'] = $_GET['site_id'];
$site_id = $_GET['site_id'];

 //function for sorting data in tree structure
function NodesVisual($data, &$newdata, $parent, $level, $justcount, &$goodl)
{
  $goodlanguage = false;
  $nodecount = 0;
  foreach($data as $key => $row)
  {
    if($parent == $row['parent']){
      $nodecount++;

        $newdata[] = $row;
        $c = count($newdata) - 1;
        $childs = NodesVisual($data, $newdata, $row['page_id'], $level + 1, 0, $g);
        $newdata[$c]['children'] = $childs;
        $goodlanguage = ($g or $goodlanguage);


        if($row['language'] != intval($GLOBALS['currentlanguagenum']))
        {
          //$newdata[$c]['page_id'] = 0;

          if(!$g)
          {
            if($c)
              $newdata = array_slice($newdata, 0, $c);
            else
              $newdata = array();
            $nodecount--;
          }else
          {
            $newdata[$c]['clicklink'] = "Page in " . sqlQueryValue("SELECT fullname FROM " . $GLOBALS['site_id'] . "_languages WHERE language_id = " . intval($row['language'])) . " (" . $row['name'] . ")";
          }
        }else
        {
          $newdata[$c]['clicklink'] = "?redirect_to_whatever";
          $goodlanguage = true;
        }

    }

  }
  $goodl = $goodlanguage;
  return $nodecount;
}

function AddTreeNode($tree, $data, &$f, $parentnode)
{
  if($f < count($data))
  {
    $row = $data[$f];
    $row['title'] = ShortenString($row['title'], 30); 
    //$max_id = sqlQueryValue("select max(page_id) from ".$site_id."_pages");
    if ($row['component'])
      {
         $page_id = $row['parent']."&component=".$row['name']."&type=".$row['description'];
      }
    else $page_id = $row['page_id'];
    $path = "?module=pages&site_id=" . $GLOBALS['site_id'] . "&action=components_form_frame&id=" . $page_id;
    if(!$row['children'])
    {
      if ($row['component'])
        $tree->add_document ($parentnode, $row['title'], $path, "/ftv2doc.gif", $row['page_id']);
      else
        $tree->add_document ($parentnode, $row['title'], $path, "/ftv2folderclosed.gif", $row['page_id']);
    }else
    {
      $node = $tree->add_folder ($parentnode, $row['title'], $path, "", "", $row['page_id']);
      for($i = 0; $i < $row['children']; $i++)
      {
        $f++;
        AddTreeNode($tree, $data, $f, $node);
      }
      return $node;
    }
  }
}

function AddTopNodes($tree, $data, &$f, $root, $children)
{
  for($i = 0; $i < $children; $i++)
  {
    AddTreeNode($tree, $data, $f, $root);
    $f++;
  }
}

  //Obtain page data
  $unsorteddata = sqlQueryData("SELECT page_id, name, title, description, visible, enabled, parent, language FROM " . $site_id . "_pages ORDER BY ind");
//-----

/*foreach ($unsorteddata as $key=>$row)
{
  if($row['page_id']>0)
  {   //real page
    $template = sqlQueryValue("SELECT template FROM ". $site_id . "_pages WHERE page_id=".$row['page_id']);
  }else
  { //template view
    $template = sqlQueryValue("SELECT template_id  FROM ". $site_id . "_views WHERE view_id=".-$row['page_id']);
  }
  if($template)
  {
    list($data0, $copybody) = sqlQueryRow("SELECT body, copybody FROM " . $site_id . "_templates WHERE template_id=$template");
    while($copybody)
      list($data0, $copybody) = sqlQueryRow("SELECT body, copybody FROM " . $site_id . "_templates WHERE template_id=$copybody");
  }
  $max_id = sqlQueryValue("select max(page_id) from ".$site_id."_pages");
  $r = componentArray($data0, $page_id);
  foreach ($r as $key=>$component)
    {
        $max_id++;
        $unsorteddata[] = array("page_id"=>$max_id,
                                "name"=>$component['name'],
                                "title"=>$component['name'],
                                "description"=>$component['type'],
                                "visible"=>1,
                                "enabled"=>1,
                                "parent"=>$row['page_id'],
                                "language"=>$row['language'],
                                "component"=>1
                                );
    }
} */

//-----
  $g = false;
  $children = NodesVisual($unsorteddata, $data, 0, 0, 0, $g);

  //Create and output tree
  require ($GLOBALS['cfgDirRoot'] . "library/class.tree.php");

  $tree = new Tree($GLOBALS['cfgWebRoot'] . "gui/tree");
  $root  = $tree->open_tree ("{%langstr:site_pages%}", "", "koks2", ".");

  $f = 0;
  AddTopNodes($tree, $data, $f, $root, $children);

  $tree->close_tree();

  if(!empty($_GET['pagedev_id']) && is_numeric($_GET['pagedev_id']))
  {

    // pagedev_id actually is page_id

    echo '
    <script language="JavaScript" type="text/javascript">

      n = FindItemByPageId('.$root.', '.$_GET['pagedev_id'].');

      OpenFolderToNode(n);

      clickOnNode2(n.id, true);

    </script>';

  }
  echo '
    <script language="JavaScript" type="text/javascript">
    function SelectRightNode(id)
    {

      n = FindItemByPageId('.$root.', id);

      OpenFolderToNode(n);

      if(n)
      {
        clickOnNode2(n.id, true);
      }

      document.body.selectedpage = id;
    }

    parent.document.body.OnOpenFolderToNode = SelectRightNode;
  ';

  if($_GET['refrframe'])
  {
    echo 'try{ window.parent.document.frames["koks2"].location.reload(true); } catch(e) { }';
  }

  echo '
  </script>
  ';

?>
