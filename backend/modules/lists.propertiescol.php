<?
// Define form
$site_id = $GLOBALS['site_id'];


$form_data = Array(
  "list_id"     => Array(
    "label"     => "IDs:",
    "type"      => "hidden"
  ),


  "ind"         => Array(
    "noshow"    => true
  ),

  "title"        => Array(
    "label"     => "{%langstr:title%}:",
    "type"      => "str"
  ),

  "page_id"      => Array(
    "label"     => "{%langstr:stats_page%}:",
    "type"      => "hidden",
    "lookup"    => $combopages
  ),

  "componentname"  => Array(
    "label"     => "{%langstr:component_name%}:",
    "type"      => "hidden",
  ),

  "componenttype" => Array(
    "label"     => "{%langstr:component_type%}:",
    "type"      => "hidden"
  ),

  "visible"     => Array(
    "label"     => "{%langstr:visible%}:",
    "type"      => "check",
    "value"         => "1"
  ),

  "collection_id" => Array(
    "label"     => "Collection ID:",
    "type"      => "hidden",
    "value"         => $_GET['collection_id'],
  ),

  "show_in_menu"     => Array(
    "label"     => "{%langstr:show_in_menu%}:",
    "type"      => "check",
    "value"         => "1"
  ),

);

// Fill some values
  $action = $_GET['action'];
  $create = ($action == 'new_list_collection');

  if(!$create)
  {
    $list = sqlQueryRow("SELECT * FROM lists WHERE list_id = ".intval($_GET['list_id']));
    $form_data['title']['value'] = $list['title'];
    $form_data['componentname']['value'] = $list['componentname'];
    $form_data['componenttype']['value'] = $list['componenttype'];
    $form_data['visible']['value'] = $list['visible'];
    $form_data['page_id']['value'] = $list['page_id'];
    $form_data['list_id']['value'] = $list['list_id'];
    $form_data['collection_id']['value'] = $list['collection_id'];
    $form_data['show_in_menu']['value'] = $list['show_in_menu'];
  }


// Use FormGen to bring this out

  require($GLOBALS['cfgDirRoot']."library/"."class.formgen.php");
  $FormGen = new FormGen();
  if(!$create)
  {
    $FormGen->action = "?module=".$GLOBALS['module']."&site_id=".$site_id."&action=addlist";
  }else{
    $FormGen->action = "?module=".$GLOBALS['module']."&site_id=".$site_id."&action=modlist";
  }

  $FormGen->cancel = "close";
  if(!$create)
  {
    $FormGen->title = "{%langstr:edit_properties%} : " . ShortenString($row['title'],40);
  }else{
    $FormGen->title = "{%langstr:create_list%}";
  }
  $FormGen->properties = $form_data;
  $FormGen->buttons = true;
  echo $FormGen->output();

?>
