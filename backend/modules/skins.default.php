<?php

  $site_id = $GLOBALS['site_id'];

  $row = sqlQueryRow('SELECT * FROM ' . $site_id . '_skins WHERE active LIMIT 1');
  if($row)
  {
    echo '<div style="width: 50%;"><center>';
    echo '{%langstr:current_design%} <b>'.$row['title'].'</b><br /><br />';
    if($row['thumbnail1'])
    {
      echo '<img src="'.$row['thumbnail1'].'" /> ';
    }
    if($row['thumbnail2'])
    {
      echo '<img src="'.$row['thumbnail2'].'" /> ';
    }
    echo '</center></div><br /><br />';
  }


  echo '{%langstr:choose_activity%}:
  <ul>
    <li><a href="?module=skins&site_id='.$site_id.'&action=list">{%langstr:choose_dif_design%}</a></li>
    <li><a href="?module=skins&site_id='.$site_id.'&action=downloadlist">{%langstr:browse_and_download_des%}</a></li>
  </ul>
  ';


?>