<?

switch ($GLOBALS["action"])
{
  case "insert":
    $component_type = $_POST['type'];
    if (empty($component_type) || !$_POST['component_page_id']) break;

    sqlQuery('INSERT INTO '.$site_id.'_dynamic_components SET
      type="'.$component_type.'",
      page_id='.(int)$_POST['component_page_id'].',
      holder="'.$_POST['holder'].'",
      width="'.$_POST['width'].'",
      align="'.$_POST['align'].'",
      is_deleted=0
    ');

    echo json_encode(array(
      'id' => sqlLastID()
    ));

    break;

  case "update":
    $id = intval($_POST['id']);
    if (!$id) break;

    sqlQuery('UPDATE '.$site_id.'_dynamic_components SET
        type="'.$_POST['type'].'",
        page_id='.(int)$_POST['component_page_id'].',
        holder="'.$_POST['holder'].'",
        width="'.$_POST['width'].'",
        align="'.$_POST['align'].'",
        is_deleted=0
      WHERE
        id = '.$id.'
    ');

    echo '1';
    break;

  case 'load':
    $id = intval($_GET['id']);
    if (!$id) break;

    $obj = createDynamicComponentInstance($id, $page_id, true);
    if ($obj)
      $obj->design();

    break;

  case 'paste':
    $id = intval($_POST['id']);
    if (!$id || !$_POST['action'] || !$_POST['holder'] || !$_POST['component_page_id']) break;

    $src = sqlQueryRow('SELECT * FROM '.$site_id.'_dynamic_components WHERE id = '.$id);
    if (!$src) break;

    if ($_POST['action'] == 'copy' || !$src['is_deleted'])
    {
      sqlQuery('INSERT INTO '.$site_id.'_dynamic_components SET
        type="'.$src['type'].'",
        page_id='.(int)$_POST['component_page_id'].',
        holder="'.$_POST['holder'].'",
        width="'.$src['width'].'",
        align="'.$src['align'].'",
        is_deleted=0
      ');

      $new_id = sqlLastID();

      $obj = createDynamicComponentInstance($id, $page_id, true);
      if ($obj)
        $obj->onCopy($new_id);

      $row = sqlQueryRow('SELECT * FROM '.$site_id.'_dynamic_components WHERE id = '.(int)$new_id);
    }
    else
    {
      sqlQuery('
        UPDATE '.$site_id.'_dynamic_components SET
          page_id='.(int)$_POST['component_page_id'].',
          holder="'.$_POST['holder'].'",
          is_deleted=0,
          delete_time=NULL
        WHERE
          id = '.$id.'
      ');

      $row = sqlQueryRow('SELECT * FROM '.$site_id.'_dynamic_components WHERE id = '.(int)$id);
    }

    echo json_encode($row);
    break;

  case 'cut':
    $id = intval($_POST['id']);
    if (!$id) break;

    sqlQuery('UPDATE '.$site_id.'_dynamic_components SET is_deleted=1, delete_time=NOW() WHERE id='.$id);

    echo 1;
    break;
}

?>