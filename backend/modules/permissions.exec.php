<?



  $perm_manageperms = $GLOBALS['perm_manageperms'];
  $perm_managesiteperms = $GLOBALS['perm_managesiteperms'];
  $site_id = $GLOBALS['site_id'];

  include_once($GLOBALS['cfgDirRoot']."backend/modules/"."permissions.texts.php");
  if(($_POST['permchanges'])and(($perm_manageperms)or($perm_managesiteperms)))
  {
    $commands = explode(';', $_POST['permchanges']);
    $updated = false;
    foreach($commands as $command)
    {
      if($command)
      {
        $s = explode(':', $command);
        $num = $s[2];
        //Check if user has rights to modify the permission
        if((CheckGlobalPermissions($permissions_list[$num]['minglob']))or(CheckSitePermissions($permissions_list[$num]['minsite'],$GLOBALS["currentUserSiteID"])))
        {
          //Remove permission
          if($s[0] == '-')
          {
            $where = 'WHERE perm_num="' . $num . '"';
            //if($s[2][0] == 'u')
            if($s[3][0] == 'u')
              $where .= ' AND isuser=1';
            else
              $where .= ' AND isuser=0';
            //if($_POST['data2'])
            if($s[1])
              $where .= ' AND data2=\'' . $s[1] . '\'';
            if($_POST['data'])
              $where .= ' AND data=\'' . $s[4] . '\'';
            //$p_id = substr($s[2], 1, strlen($s[2]));
            $p_id = substr($s[3], 1, strlen($s[3]));
            $where .= ' AND id="' . $p_id . '"';
            //BeforeDeletePermission($num, ($s[2][0] == 'u'), $p_id, $_POST['data'], $_POST['data2']);
            BeforeDeletePermission($num, ($s[3][0] == 'u'), $p_id, $s[4], $s[1]);
            sqlQuery("DELETE FROM permissions " . $where);
            echo "DELETE FROM permissions " . $where;
            $updated = true;
          }
          //Add permission
          if($s[0] == '+')
          {
            //delete the same permission so that there are no duplicates
            $where = 'WHERE perm_num="' . $num . '"';
            //if($s[2][0] == 'u')
            if($s[3][0] == 'u')
              $where .= ' AND isuser=1';
            else
              $where .= ' AND isuser=0';
            //if($_POST['data2'])
            if($s[1])
              $where .= ' AND data2=\'' . $s[1] . '\'';
            if($_POST['data'])
              $where .= ' AND data=\'' . $s[4] . '\'';
            //$p_id = substr($s[2], 1, strlen($s[2]));
            $p_id = substr($s[3], 1, strlen($s[3]));
            $where .= ' AND id="' . $p_id . '"';
            sqlQuery("DELETE FROM permissions " . $where);
            //add permission
            $vals = '"' . $num . '", ';
            //if($s[2][0] == 'u')
            if($s[3][0] == 'u')
              $vals .= '1, ';
            else
              $vals .= '0, ';
            //$vals .= substr($s[2], 1, strlen($s[2]));
            $vals .= substr($s[3], 1, strlen($s[3]));
            echo $vals;
            //if($_POST['data2'])
            if($s[1])
              sqlQuery("INSERT INTO permissions (perm_num, isuser, id, data, data2) VALUES (" . $vals . ", " . $s[4] . ", " . $s[1] . ")");
            else if($s[4])
              sqlQuery("INSERT INTO permissions (perm_num, isuser, id, data) VALUES (" . $vals . ", " . $s[4] . ")");
            else
              sqlQuery("INSERT INTO permissions (perm_num, isuser, id) VALUES (" . $vals . ")");
            $updated = true;
            //obtain id of newly inserted row
            $where = 'WHERE perm_num="' . $num . '"';
            //if($s[2][0] == 'u')
            if($s[3][0] == 'u')
              $where .= ' AND isuser=1';
            else
              $where .= ' AND isuser=0';
            //if($_POST['data2'])
            if($s[1])
              $where .= ' AND data2=\'' . $s[1] . '\'';
            if($_POST['data'])
              $where .= ' AND data=\'' . $s[4] . '\'';
            //$p_id = substr($s[2], 1, strlen($s[2]));
            $p_id = substr($s[3], 1, strlen($s[3]));
            $where .= ' AND id="' . $p_id . '"';
            $perm_id = sqlQueryValue("SELECT perm_id FROM permissions " . $where);
            //AfterAddPermission($perm_id, $num, ($s[2][0] == 'u'), $p_id, $_POST['data'], $_POST['data2']);
            
            AfterAddPermission($perm_id, $num, ($s[3][0] == 'u'), $p_id, $s[4], $s[1]);
          }
        }
      }
    }

    if($updated)
      Add_Log("Changed permissions", $site_id);
  }


//#############################################################################

  if(($_POST['pagepermchanges']!="")and(($perm_manageperms)or($perm_managesiteperms)))
  {
    $commands = explode(';', $_POST['pagepermchanges']);
    $updated = false;
    foreach($commands as $command)
    {
      if($command)
      {
        $s = explode(':', $command);
        //$num = $s[1];
        $num = $s[2];
        //Check if user has rights to modify the permission
        if((CheckGlobalPermissions($permissions_list[$num]['minglob']))or(CheckSitePermissions($permissions_list[$num]['minsite'],$GLOBALS["currentUserSiteID"])))
        {
          //Remove permission
          if($s[0] == '-')
          {
              
            $where = 'WHERE perm_num="' . $num . '"';
            //if($s[2][0] == 'u')
            if($s[3][0] == 'u')
              $where .= ' AND isuser=1';
            else
              $where .= ' AND isuser=0';
            //if($_POST['data2'])
            if($s[1])
              {
                $where .= ' AND data2=\'' . $s[1] . '\'';
              //$where .= ' AND data2=\'' . $_POST['data2'] . '\'';
              }
              
            //if($_POST['data'])
            if($s[4])
              $where .= ' AND data=\'' . $s[4] . '\'';
              //$where .= ' AND data=\'' . $_POST['data'] . '\'';
            //$p_id = substr($s[2], 1, strlen($s[2]));
            $p_id = substr($s[3], 1, strlen($s[3]));
            $where .= ' AND id="' . $p_id . '"';
            //BeforeDeletePermission($num, ($s[2][0] == 'u'), $p_id, $_POST['data'], $_POST['data2']);
            BeforeDeletePermission($num, ($s[3][0] == 'u'), $p_id, $s[4], $s[1]);
            sqlQuery("DELETE FROM permissions " . $where);
            //echo "DELETE FROM permissions " . $where."<br>";die;
            $updated = true;
            
          }
          //Add permission
          if($s[0] == '+')
          {
            //delete the same permission so that there are no duplicates
            $where = 'WHERE perm_num="' . $num . '"';
            if($s[3][0] == 'u')
              $where .= ' AND isuser=1';
            else
              $where .= ' AND isuser=0';
            if($s[1])
              $where .= ' AND data2=\'' . $s[1] . '\'';
            if($s[4])
                $where .= ' AND data=\'' . $s[4] . '\'';
            //if($_POST['data'])
            //  $where .= ' AND data=\'' . $_POST['data'] . '\'';
            $p_id = substr($s[3], 1, strlen($s[3]));
            $where .= ' AND id="' . $p_id . '"';
            sqlQuery("DELETE FROM permissions " . $where);
            //echo "DELETE FROM permissions " . $where;
            //add permission
            $vals = '"' . $num . '", ';
            if($s[3][0] == 'u')
              $vals .= '1, ';
            else
              $vals .= '0, ';
            $vals .= substr($s[3], 1, strlen($s[3]));
            if($s[1])
              {
                //echo "INSERT INTO permissions (perm_num, isuser, id, data, data2) VALUES (" . $vals . ", " . $_POST['data'] . ", " . $s[1] . ")<br>";
                sqlQuery("INSERT INTO permissions (perm_num, isuser, id, data, data2) VALUES (" . $vals . ", " . $s[4] . ", " . $s[1] . ")");
              }
            else if($s[4])
              {
                //echo "INSERT INTO permissions (perm_num, isuser, id, data) VALUES (" . $vals . ", " . $_POST['data'] . ")<br>";
                sqlQuery("INSERT INTO permissions (perm_num, isuser, id, data) VALUES (" . $vals . ", " . $s[4] . ")");
              }
            else
              {
                //echo "INSERT INTO permissions (perm_num, isuser, id) VALUES (" . $vals . ")<Br>";
                sqlQuery("INSERT INTO permissions (perm_num, isuser, id) VALUES (" . $vals . ")");
              }
            $updated = true;
            //obtain id of newly inserted row
            $where = 'WHERE perm_num="' . $num . '"';
            if($s[3][0] == 'u')
              $where .= ' AND isuser=1';
            else
              $where .= ' AND isuser=0';
            if($s[1])
              $where .= ' AND data2=\'' . $s[1] . '\'';
            if($s[4])
              $where .= ' AND data=\'' . $s[4] . '\'';
            $p_id = substr($s[3], 1, strlen($s[3]));
            $where .= ' AND id="' . $p_id . '"';
            $perm_id = sqlQueryValue("SELECT perm_id FROM permissions " . $where);
            //AfterAddPermission($perm_id, $num, ($s[2][0] == 'u'), $p_id, $_POST['data'], $_POST['data2']);
            AfterAddPermission($perm_id, $num, ($s[3][0] == 'u'), $p_id, $s[4], $s[1]);
          }
        }
      }
    } 

    if($updated)
    {
      Add_Log("Changed permissions", $site_id);
      unset($_SESSION['user_site_permissions']);
      MakeGlobalPermVars($permissions_list);
    }
  }

