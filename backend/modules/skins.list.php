<?

  $site_id = $GLOBALS['site_id'];

  $pagesize = 3;

  if(isset($_SESSION['designs_active_page']))
    $page = $_SESSION['designs_active_page'];
  else
    $page = 0;

  if(isset($_GET['editpage']))
  {
    $page = intval($_GET['editpage']) - 1;
    $_SESSION['designs_active_page'] = $page;
  }

  $page = $page * $pagesize;


  $data = sqlQueryData('SELECT * FROM ' . $site_id . '_skins ORDER BY active DESC, title LIMIT ' . $page . ', ' . $pagesize);
  $count = sqlQueryValue('SELECT Count(*) FROM ' . $site_id . '_skins');

  foreach ($data as $key => $row)
  {
    $data[$key]['description'] = nl2br($row['description']);
  }

  $cols = Array(

    "select"    => Array(
      "width"     => "5%",
      "title"     => "",
      "format"    => '<input type="radio" name="selectskin" value="{%skin_id%}" id="selectskin_{%skin_id%}" />'
    ),

    "title"    => Array(
      "width"     => "15%",
      "title"     => "{%langstr:title%}",
      "format"    => '[if {%active%}]<b>{%_self%}</b> (active)[else]{%_self%}[/if]'
    ),

    "description" => Array(
      "width"     => "30%",
      "title"     => "{%langstr:col_description%}"
    ),

    "thumbnail1"       => Array(
      "width"     => "25%",
      "format"    => "[if {%_self%}]<a href=\"#\" onclick=\"window.open('{%pic1%}','galwin','width=1050,height=800,scrollbars=1,toolbar=0,resizable=1');return false\"><img src=\"{%_self%}\" /></a>[/if]"
    ),

    "thumbnail2"       => Array(
      "width"     => "25%",
      "format"    => "[if {%_self%}]<a href=\"#\" onclick=\"window.open('{%pic2%}','galwin','width=1050,height=800,scrollbars=1,toolbar=0,resizable=1');return false\"><img src=\"{%_self%}\" /></a>[/if]"
    ),

  );

  require($GLOBALS['cfgDirRoot']."library/"."class.rightmenu.php");
$rightmenu = new rightmenu;

$rightmenu->addItem('{%langstr:skins_apply%}','javascript:if(SelectedRowID)window.location=\'?module=skins&action=applyskin&site_id=' . $site_id . '&id=\'+SelectedRowID;',false,false,'idea');
$rightmenu->addSpacer();
$rightmenu->addItem('{%langstr:skins_del%}','javascript:if(SelectedRowID && confirm(\'{%langstrjs:ask_delete_skin%}\'))window.location=\'?module=skins&action=delskin&site_id=' . $site_id . '&id=\'+SelectedRowID;',false,false,'delete');

echo $rightmenu->output();

  require($GLOBALS['cfgDirRoot']."library/"."class.datagrid.php");
  $Table = new DataGrid();
  $Table->cols = $cols;
  $Table->data = $data;
  $Table->footer = false;
  $Table->rowformat = " id=\"tablerow{%skin_id%}\" onClick=\"javascript:HighLightTR(this, {%skin_id%});if(SelectedRowID){ document.getElementById('selectskin_{%skin_id%}').checked = true; EnableToolbarButtons(che);  }else { DisableToolbarButtons(che);  }\" oncontextmenu=\"javascript:HighLightTR(this, {%skin_id%},1); if(SelectedRowID){ document.getElementById('selectskin_{%skin_id%}').checked = true; showMenu(this, event); } return false;\"";
  if($count > 0)
  {
    $Table->pagecount = ceil($count / $pagesize);
    $Table->pagelen = 1;
    $Table->linkformat = preg_replace('/&editpage=[0-9]*/s', '', $_SERVER["REQUEST_URI"]) . '&editpage=%d';
    $Table->pagenum = min($Table->pagecount, $_SESSION['designs_active_page'] + 1);
  }
  echo $Table->output();

  echo '<p>&nbsp;</p><p><a href="?module=skins&site_id=' . $site_id . '&action=downloadlist">&raquo; {%langstr:link_click_to_download_skins%}</a></p>';

echo '<script language="JavaScript" type="text/javascript">
/*<![CDATA[*/
che = new Array("apply", "del");

DisableToolbarButtons(che);
/*]]>*/
</script>';

if($_GET['selrow'])
{
  echo '<script language="JavaScript">
        <!--
          var tablerow' . $_GET['selrow'] . ' = document.getElementById("tablerow' . $_GET['selrow'] . '");
          if(typeof(tablerow' . $_GET['selrow'] . ')!= "undefined" && tablerow' . $_GET['selrow'] . ')
          {
            preEl = tablerow' . $_GET['selrow'] . ';
            ChangeTextColor(tablerow' . $_GET['selrow'] . ',\'tableRowSel\');
            SelectedRowID = ' . $_GET['selrow'] . ';
            if(SelectedRowID){ EnableToolbarButtons(che); }else { DisableToolbarButtons(che); }
            document.getElementById(\'selectskin_' . $_GET['selrow'] . '\').checked = true;
          }

          if(confirm("{%langstrjs:ask_apply_downloaded_skin%}"))
          {
            window.location=\'?module=skins&action=applyskin&site_id=' . $site_id . '&id=\'+'.$_GET['selrow'].';
          }

        <!---->
        </script>';
}

?>
