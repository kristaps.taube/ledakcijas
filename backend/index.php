<?php

// adding Constructor autoloader
require __DIR__ . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "library" . DIRECTORY_SEPARATOR . "class.AutoLoader.php";
spl_autoload_register(array('AutoLoader', 'load'));
AutoLoader::setBase(__DIR__ . DIRECTORY_SEPARATOR . "..");

// vendor autoloader
require(__DIR__ . DIRECTORY_SEPARATOR . "..". DIRECTORY_SEPARATOR ."vendor/autoload.php");

$config = ArrayHelper::merge(
    require __DIR__ . DIRECTORY_SEPARATOR . "..". DIRECTORY_SEPARATOR . 'config_array.php',
    require __DIR__ . DIRECTORY_SEPARATOR . "..". DIRECTORY_SEPARATOR . 'config_array_local.php',
    ['backend' => true]
);                                                                                  

$app = (new Constructor\WebApp($config))->initForBackend(); // dont run, just create app with settings

require_once("const.php");
require_once("languages.php");
require_once("../library/func.backend.php");
//require_once("../init.php");

$site_id = isset($_GET['site_id']) ? (int)$_GET['site_id'] : 4;
$page_id = $_GET['page_id'];
$GLOBALS['site_id'] = $site_id;
$GLOBALS['page_id'] = $page_id;

if (isset($_GET['session_id'])) session_id($_GET['session_id']);

$_SESSION['site_id'] = $site_id;

if ($_SERVER['HTTP_HOST'] != $GLOBALS['cfgDomain']){
  header('Location: http://'.$GLOBALS['cfgDomain'].$_SERVER['REQUEST_URI'], true, 301);
  die;
}

if($site_id){
  $row = DB::GetRow('SELECT site_id FROM sites WHERE site_id = :id', array(":id" => $site_id));
  if(!$row) die('invalid site');
}

$dir_root = DB::GetValue("select dirroot from sites where site_id = :site_id", array(":site_id" => $site_id));
define("DIR_ROOT", $dir_root);

$GLOBALS['from_backend'] = true;
$GLOBALS['runningfrombackend'] = true; // this is totally different variable than previous
$GLOBALS['admin_mode'] = $_SESSION['admin_logged_in'] ? 1 : 0;

$languages = cmsconfig::getLanguages();

$GLOBALS['cfgLanguage'] = option('CMS\\language', null, 'Language', Array('value_on_create' => 1, 'type' => 'list', 'componenttype' => 'cmsconfig'), 'Latvian');

if(!$GLOBALS['cfgLanguage']){
	foreach(getLanguages() as $lang_key => $lang){
		if($lang['is_default']){
    	$GLOBALS['cfgLanguage'] = $lang['shortname'];
			$_SESSION['cms_language'] = $lang['shortname'];
			break;
		}
	}
}

if (!empty($_GET['set_language']) && isset($languages[$_GET['set_language']])){
  $_SESSION['cms_language'] = $languages[$_GET['set_language']];
  $GLOBALS['cfgLanguage'] = $languages[$_GET['set_language']];
}

define("CMS_LANG", $GLOBALS['cfgLanguage']);

if($_GET['session_id']) session_id($_GET['session_id']);
if($_GET['session_name']) session_name($_GET['session_name']);

$module = $_GET['module'] ? $_GET['module'] : defaultModule;

if (!logged_in()){
  $module = 'login';
}else{
  SessionManager::registerSession();
}

MakeGlobalPermVars();

if ($site_id){
	$domain = sqlQueryValue("select domain from sites where site_id=".$site_id);
  $GLOBALS['domain'] = $domain;
}

if ($module == $GLOBALS['custommodule'][$domain]){
	require('custommodules/'.$module.'.conf.php');
}else{
	require('modules/'.$module.'.conf.php');
}

$guiFolder = $GLOBALS['guiFolder'] ? $GLOBALS['guiFolder'] : 'gui';

// Run special exec script
if ($_GET['cmodule']==1 && isset($GLOBALS['docScripts']['exec']) && file_exists("custommodules/".$GLOBALS['docScripts']['exec'])){
	$execScrip = "custommodules/".$GLOBALS['docScripts']['exec'];
}else if (isset($GLOBALS['docScripts']['exec']) && file_exists("modules/".$GLOBALS['docScripts']['exec'])){
	$execScrip = "modules/".$GLOBALS['docScripts']['exec'];
}

if($execScrip){
	require($execScrip);
}

// Read template
$pageBody = implode("", file($guiFolder.'/'.$GLOBALS['docTemplate']));
//print_r($_SESSION['user_site_permissions']);
$GLOBALS['docScripts']['sitetabs'] = 'global.sitetabs.php';

// Parse everything
$pageOutput = parseVariables($pageBody);

//Headers
$GLOBALS['nocache'] = true;
require($GLOBALS['cfgDirRoot'] . "library/inc.http_headers.php");

// Output
if ($pageOutput) echo($pageOutput); else echo "";

