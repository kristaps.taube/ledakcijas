<?
// Constructor XML_RPC function library

function rpc_deletePagePics($params)
{
  $GLOBALS['site_id'] = $GLOBALS['xmlrpc_site_id'];
  $page_id = $params[0];
  $directory = sqlQueryValue("SELECT dirroot FROM sites WHERE site_id = '".$GLOBALS['site_id']."'");
  $directory .= 'images/wordpics/';
  $dir = $directory;

  $handle = opendir($dir);
  while ($file = readdir($handle)) {
    if (is_file($dir . $file) && substr($file, 0, strlen('i' . $page_id . '_')) == 'i' . $page_id . '_')
    {
      //echo $file;
      unlink($dir . $file);
    }
  }
  closedir($handle);
  XMLRPC_response(XMLRPC_prepare('ok'));
}

function rpc_uploadWordPic($params)
{
  $GLOBALS['site_id'] = $GLOBALS['xmlrpc_site_id'];
  $data = $params[0];
  $ext = $params[1];
  $page_id = $params[2];
  $domain = sqlQueryValue("SELECT domain FROM sites WHERE site_id=".$GLOBALS['site_id']);
  $directory = sqlQueryValue("SELECT dirroot FROM sites WHERE site_id = '".$GLOBALS['site_id']."'");
  if(!file_exists($directory . 'images/wordpics'))
  {
    mkdir($directory . 'images/wordpics', 777);
  }else
  {
  }

  do
  {
    $fname = "i" . $page_id . '_' . rand(1, 9999999) . $ext;
  }while(file_exists($directory . 'images/wordpics/' . $fname));
  $text = base64_decode($data);
  $filename = $directory . 'images/wordpics/' . $fname;

  $handle = fopen($filename, "w");
  if($handle)
  {
    fwrite($handle, $text);
    fclose($handle);
  }

  XMLRPC_response(XMLRPC_prepare('/images/wordpics/' . $fname));

}

?>
