<?
// Constructor XML_RPC function library

function rpc_getPagesTree($params)
{
  $allpages = sqlQueryData("SELECT pagedev_id,name,parent,title,encoding FROM " . $GLOBALS['xmlrpc_site_id'] . "_pagesdev LEFT JOIN " . $GLOBALS['xmlrpc_site_id'] . "_languages ON language=language_id ORDER BY ind");
  foreach($allpages as $row => $val)
  {
    $allpages[$row][3] = base64_encode($allpages[$row][3]);
  }
  $data = array();
  foreach($allpages as $row)
  {
    $data[] = array($row[0], $row[1], $row[2], $row[3], $row[4]);
  }
  XMLRPC_response(XMLRPC_prepare($data));
}

function rpc_getPageContents($params)
{
  //$GLOBALS['page_id'] = $params[0];
  $GLOBALS['site_id'] = $GLOBALS['xmlrpc_site_id'];
  $pagedev_id = $params[0];
  $page_id = sqlQueryValue("SELECT page_id FROM " . $GLOBALS['site_id'] . "_pagesdev WHERE pagedev_id=" . $pagedev_id);
  $GLOBALS['page_id'] = $page_id;
  $type = $GLOBALS['xmlrpc_componenttype'];
  $filename = $GLOBALS['cfgDirRoot'] . "components/class.".$GLOBALS['xmlrpc_componenttype'].".php";
  $GLOBALS['component_name'] = $GLOBALS['xmlrpc_componentname'];
  $name = $GLOBALS['xmlrpc_componentname'];
  include_once($filename);
  $obj = new $type($name);
  $obj->pagedev_id = $pagedev_id;
  addDefaultProperties($obj);
  $text = $obj->getProperty($GLOBALS['xmlrpc_componentproperty']);
  $text = base64_encode($text);
  XMLRPC_response(XMLRPC_prepare($text));
}

function rpc_setPageContents($params)
{
  //$GLOBALS['page_id'] = $params[0];
  $GLOBALS['site_id'] = $GLOBALS['xmlrpc_site_id'];
  $pagedev_id = $params[0];
  $page_id = sqlQueryValue("SELECT page_id FROM " . $GLOBALS['site_id'] . "_pagesdev WHERE pagedev_id=" . $pagedev_id);
  $GLOBALS['page_id'] = $page_id;
  $type = $GLOBALS['xmlrpc_componenttype'];
  $filename = $GLOBALS['cfgDirRoot'] . "components/class.".$GLOBALS['xmlrpc_componenttype'].".php";
  $GLOBALS['component_name'] = $GLOBALS['xmlrpc_componentname'];
  $name = $GLOBALS['xmlrpc_componentname'];
  include_once($filename);
  $obj = new $type($name);
  $obj->pagedev_id = $pagedev_id;
  $text = base64_decode($params[1]);
  $obj->setProperty($GLOBALS['xmlrpc_componentproperty'], AddSlashes($text));
  $modby = 1;
  //sqlQuery("INSERT INTO test (t) VALUES ('" . AddSlashes("UPDATE ".$GLOBALS['site_id']."_pagesdev SET lastmod='".time()."', modby=$modby WHERE pagedev_id=$pagedev_id") . "')");
  sqlQuery("UPDATE ".$GLOBALS['site_id']."_pagesdev SET lastmod='".time()."', modby=$modby WHERE pagedev_id=$pagedev_id");
  if (CheckPermission(31, $GLOBALS['site_id'], $page_id) || CheckPermission(32, $GLOBALS['site_id']))
  {
      pageautopublish($GLOBALS['site_id'], $pagedev_id, true);
  }

  XMLRPC_response(XMLRPC_prepare("ok"));
}

function rpc_getPageEncoding($params)
{
  //$GLOBALS['page_id'] = $params[0];
  $GLOBALS['site_id'] = $GLOBALS['xmlrpc_site_id'];
  $pagedev_id = $params[0];
  $page_id = sqlQueryValue("SELECT page_id FROM " . $GLOBALS['site_id'] . "_pagesdev WHERE pagedev_id=" . $pagedev_id);
  $GLOBALS['page_id'] = $page_id;
  $encoding = '';
  $lang_id = sqlQueryValue("SELECT language FROM " . $GLOBALS['site_id'] . "_pagesdev WHERE pagedev_id=" . $GLOBALS['page_id']);
  if($lang_id)
  {
    $encoding = sqlQueryValue("SELECT encoding FROM " . $GLOBALS['site_id'] . "_languages WHERE language_id = " . $lang_id);
  }
  XMLRPC_response(XMLRPC_prepare($encoding));
}



?>
