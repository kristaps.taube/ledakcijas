<?

require_once($GLOBALS['cfgDirRoot'] . 'xmlrpc_config.php');
if(!$GLOBALS['xmlrpc_site_id'])
{ //select first site in list
  $GLOBALS['xmlrpc_site_id'] = sqlQueryValue('SELECT site_id FROM sites');
}
if(!$GLOBALS['xmlrpc_componentname'])
{
  $GLOBALS['xmlrpc_componentname'] = 'maincontents';
}
if(!$GLOBALS['xmlrpc_componentproperty'])
{
  $GLOBALS['xmlrpc_componentproperty'] = 'text';
}
if(!$GLOBALS['xmlrpc_componenttype'])
{
  $GLOBALS['xmlrpc_componenttype'] = 'formatedtext0';
}

$xmlrpc_libraries = Array();
$xmlrpc_methods = Array();

//--- test --
$xmlrpc_libraries['interopEchoTests'] = $GLOBALS['cfgDirRoot'] . "backend/xmlrpc/func.tests.php";
$xmlrpc_methods['interopEchoTests.echoString']  = 'rpc_echoString';

//--- page structure manipulation library ---
$xmlrpc_libraries['rpcPages'] = $GLOBALS['cfgDirRoot'] . "backend/xmlrpc/func.rpcpages.php";
$xmlrpc_methods['rpcPages.getPagesTree']  = 'rpc_getPagesTree';
$xmlrpc_methods['rpcPages.getPageContents']  = 'rpc_getPageContents';
$xmlrpc_methods['rpcPages.setPageContents']  = 'rpc_setPageContents';
$xmlrpc_methods['rpcPages.getPageEncoding']  = 'rpc_getPageEncoding';

//--- image manipulation library ---
$xmlrpc_libraries['rpcImages'] = $GLOBALS['cfgDirRoot'] . "backend/xmlrpc/func.rpcimages.php";
$xmlrpc_methods['rpcImages.uploadWordPic']  = 'rpc_uploadWordPic';
$xmlrpc_methods['rpcImages.deletePagePics']  = 'rpc_deletePagePics';

//--- misc functions library ---
$xmlrpc_libraries['rpcMisc'] = $GLOBALS['cfgDirRoot'] . "backend/xmlrpc/func.rpcmisc.php";
$xmlrpc_methods['rpcMisc.getDomainName']  = 'rpc_getDomainName';

?>
