<?

     require_once("../config.php");

     //Obtain the site_id and page_id by parsing the uri
     $url = parse_url($_SERVER['REQUEST_URI']);
     if((!$url['host'])or($_SERVER['REQUEST_URI']{0}=='/'))
     {
       $url = 'http://' . strtolower($_SERVER['HTTP_HOST']) . $_SERVER['REQUEST_URI'];
       $url = parse_url($url);
       //$url['host'] = strtolower($_SERVER['HTTP_HOST']);
     }

     if(($GLOBALS['forced_site_id'])and($GLOBALS['forced_page_id']))
     {
       $site_id = $GLOBALS['forced_site_id'];
       $page_id = $GLOBALS['forced_page_id'];
     }else
     {

       //Obtain site_id
       $row = sqlQueryRow("SELECT site_id, wapsite FROM sites WHERE domain='" . $url['host'] . "'");
       $site_id = $row['site_id'];

       if(!$site_id)
       {
         //check if site is in aliases
         $sites = sqlQueryData("SELECT site_id, alias FROM sites WHERE LOCATE('" . $url['host'] . "',alias)");
         foreach($sites as $row)
         {
           $aliases = explode(";", $row['alias']);
           if(in_array($url['host'],$aliases))
           {
             $site_id = $row['site_id'];
           }
         }
       }
       if(!$site_id)
       {
         header("HTTP/1.0 404 Not Found");
         //Die ("Error 404 - page not found");
       }

     }













   if ($site_id)
   {

     //$extract_file_name = explode('/', $_GET['download_file']);
     $file_data = sqlQueryRow("SELECT file_id, file_name, public FROM ".$site_id."_documents WHERE file_id=".intval($_GET['file']));

     $docroot = sqlQueryValue("SELECT docroot FROM sites WHERE site_id=".$site_id);
     //print_r($file_data);

     if (is_file($docroot . $file_data['file_id']))
     {


        if($file_data['public']==0)
        {
            if ( ((!isset( $GLOBALS['PHP_AUTH_USER'] )) || (!isset($GLOBALS['PHP_AUTH_PW'])))) {

                    header( 'WWW-Authenticate: Basic realm="Private"' );
                    header( 'HTTP/1.0 401 Unauthorized' );
                    echo 'Authorization Required.';
                    exit;

                } else {
                    if((valid_login($GLOBALS['PHP_AUTH_USER'], $GLOBALS['PHP_AUTH_PW'], $_SERVER['REMOTE_ADDR'], true))and((!$GLOBALS['currentUserSiteID'])or($GLOBALS['currentUserSiteID'] == $site_id)))
                        {
                          $perm_acceessdocument = CheckPermission(29, $GLOBALS['site_id'], $file_data['file_id']);
                          if ($perm_acceessdocument)
                          {
                            $GLOBALS['hasbeenauthorized'] = true;
                          }
                          else
                          {
                              header( 'WWW-Authenticate: Basic realm="Private"' );
                              header( 'HTTP/1.0 401 Unauthorized' );
                              unset($GLOBALS['PHP_AUTH_USER']);
                              Die('Access denied');
                          }
                        }else
                        {
                          header( 'WWW-Authenticate: Basic realm="Private"' );
                          header( 'HTTP/1.0 401 Unauthorized' );
                          unset($GLOBALS['PHP_AUTH_USER']);
                          Die('Wrong username or password');
                        }

                }
        }


         $filesize = filesize($docroot . $file_data['file_id']);
         $ifn      = $file_data['file_name'];

         header("Expires: Mon, 26 Jul 2001 05:00:00 GMT");
         header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
         header("Cache-Control: no-store, no-cache, must-revalidate");
         header("Cache-Control: post-check=0, pre-check=0", false);
         header("Pragma: no-cache");
         header("Cache-control: private");
         header("Content-Length: " . $filesize);
         header("Content-Type: application/force-download");
         header("Content-Type: application/download");

         if (preg_match('#Opera(/| )([0-9].[0-9]{1,2})#', getenv('HTTP_USER_AGENT')) or preg_match('#MSIE ([0-9].[0-9]{1,2})#', getenv('HTTP_USER_AGENT'))) {
             header("Content-Type: application/octetstream");
         } else {
             header("Content-Type: application/octet-stream");
         }
         header("Content-Disposition: attachment; filename=\"".$ifn."".$extract_file_name[count($extract_file_name)-1]."\"");
         readfile($docroot . $file_data['file_id']);

         $user_id = sqlQueryValue("SELECT user_id FROM users WHERE username='".$GLOBALS['PHP_AUTH_USER']."'");
         sqlQuery("INSERT INTO ".$site_id."_documentstats (document_id, type, time, IP, user_id) VALUES (".$file_data['file_id'].",'access','".time()."','".$_SERVER["REMOTE_ADDR"]."',".$user_id.") ");

         exit;
     }
     else
     {
        header("HTTP/1.0 404 Not Found");
     }
   }

?>
