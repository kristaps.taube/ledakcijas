<?

$action = $_GET['action'];

$parsed = false;
if ($action=="parserates")
{
  //we should have 3 files uploaded, parse them and change appropriate collections
  //currency rates for main collection
  if((isset($_FILES['userfile1']))and((!$_FILES['userfile1']['error'])))
  {
    $s = file($_FILES['userfile1']['tmp_name']);
    $rows = Array();
    $row = Array();
    $prevline = '';
    foreach($s as $line)
    {
      $line = trim($line);
      if($line == '<tr>')
        $row = Array();
      if($line == '</td>')
        $row[] = $prevline;
      if($line == '</tr>')
      {
        $rows[] = $row;
        $row = Array();
      }
      $prevline = $line;
    }
    //add rates to collection
    include_once($GLOBALS['cfgDirRoot'] . "collections/class.ltbcurrencyratescollection.php");
    $col =  new ltbcurrencyratescollection('kursi_all', 0);
    if($col->FindCollection('kursi_all'))
    {
      $col->Clear();
      foreach($rows as $row)
      {
        $r = Array();
        $r[] = $row[0];
        if($row[0] != 'RUB')
          $r[] = '1'; //all currencies have rate "1" except RUB
        else
          $r[] = '1000';
        $r[] = $row[3];
        $r[] = $row[4];
        $r[] = $row[1];
        $r[] = $row[2];
        $r[] = $row[5];
        $r[] = $row[6];
        $i = $col->AddNewItem();
        $col->ChangeItem($i, $r);
      }
      $parsed = true;
    }
  }

  //currency rates for lv collection
  if((isset($_FILES['userfile2']))and((!$_FILES['userfile2']['error'])))
  {
    $s = file($_FILES['userfile2']['tmp_name']);
    $rows = Array();
    $row = Array();
    $prevline = '';
    foreach($s as $line)
    {
      $line = trim($line);
      if($line == '<tr>')
        $row = Array();
      if($line == '</td>')
        $row[] = $prevline;
      if($line == '</tr>')
      {
        $rows[] = $row;
        $row = Array();
      }
      $prevline = $line;
    }
    //add rates to collection
    include_once($GLOBALS['cfgDirRoot'] . "collections/class.ltbcurrencyratescollection.php");
    $col =  new ltbcurrencyratescollection('kursi_lv', 0);
    if($col->FindCollection('kursi_lv'))
    {
      $col->Clear();
      foreach($rows as $row)
      {
        $r = Array();
        $r[] = $row[0];
        if($row[0] != 'RUB')
          $r[] = '1'; //all currencies have rate "1" except RUB
        else
          $r[] = '1000';
        $r[] = '-';
        $r[] = '-';
        $r[] = '-';
        $r[] = '-';
        $r[] = $row[1];
        $r[] = $row[2];
        $i = $col->AddNewItem();
        $col->ChangeItem($i, $r);
      }
      $parsed = true;
    }
  }

  //currency rates for ru collection
  if((isset($_FILES['userfile3']))and((!$_FILES['userfile3']['error'])))
  {
    $s = file($_FILES['userfile3']['tmp_name']);
    $rows = Array();
    $row = Array();
    $prevline = '';
    foreach($s as $line)
    {
      $line = trim($line);
      if($line == '<tr>')
        $row = Array();
      if($line == '</td>')
        $row[] = $prevline;
      if($line == '</tr>')
      {
        $rows[] = $row;
        $row = Array();
      }
      $prevline = $line;
    }
    //add rates to collection
    include_once($GLOBALS['cfgDirRoot'] . "collections/class.ltbcurrencyratescollection.php");
    $col =  new ltbcurrencyratescollection('kursi_ru', 0);
    if($col->FindCollection('kursi_ru'))
    {
      $col->Clear();
      foreach($rows as $row)
      {
        $r = Array();
        $r[] = $row[0];
        if($row[0] != 'RUB')
          $r[] = '1'; //all currencies have rate "1" except RUB
        else
          $r[] = '1000';
        $r[] = '-';
        $r[] = '-';
        $r[] = '-';
        $r[] = '-';
        $r[] = $row[1];
        $r[] = $row[2];
        $i = $col->AddNewItem();
        $col->ChangeItem($i, $r);
      }
      $parsed = true;
    }
  }
  if($parsed)
    header("Location: index.php?module=collections&site_id=" . $GLOBALS['site_id']);
}


?>
