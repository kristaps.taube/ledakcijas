<?

$GLOBALS['tabs'] = 'site';

function DisplayToolbar()
{
  global $docStrings;
  require($GLOBALS['cfgDirRoot']."library/"."class.toolbar.php");
  $Toolbar = new Toolbar();
  $Toolbar->AddLink("log", "View Log", "view_log.gif", "javascript:window.location='?module=admin&action=log&site_id=".$_GET['site_id']."'", "View activity log");
  $Toolbar->AddLink("backup", "Backup Site", "backup_db.gif", "javascript:window.location='?module=admin&site_id=".$_GET['site_id']."&action=database'", "Backup database");
  $Toolbar->AddLink("restore", "Restore Site", "restore_db.gif", "javascript:window.location='?module=admin&site_id=".$_GET['site_id']."&action=browsebackups'", "Restore database from backup");
  $Toolbar->AddButton("reindex", "Index Pages For Search", "index_pages.gif", "javascript:openDialog('?module=admin&action=reindex&close=0&site_id=".$_GET['site_id']."&page_id=0', 400, 100);", "Index all pages for search");
  //$Toolbar->AddLink("components", "Add/Remove Components", "components.gif", "javascript:window.location='?module=admin&action=components'", "Install and uninstall components");
  $Toolbar->AddButton("permissions", "Access Permissions", "edit_permissions.gif", "javascript:openDialog('?module=permissions&close=1&site_id=".$_GET['site_id']."&data=".$_GET['site_id']."&action=site_form', 400, 380);", "Edit global access permissions");
  $Toolbar->AddLink("statistics", "Site Statistics", "page_statistics.gif", "javascript:window.location='?module=admin&site_id=".$_GET['site_id']."&action=stats'", "Stats");
  $docStrings['toolbar'] = $Toolbar->output();
}

function DisplayBrowseBackupsToolbar()
{
  global $docStrings;
  require($GLOBALS['cfgDirRoot']."library/"."class.toolbar.php");

  $site_id = $_GET['site_id'];
  $cfgBackupRoot = $GLOBALS['cfgDirRoot'] . "backup/site".$site_id."/";

  $Toolbar = new Toolbar();
  $Toolbar->AddButton("upload", "Upload backup", "upload.gif", "openDialog('?module=files&site_id=".$GLOBALS['site_id']."&backup=1&path=".$GLOBALS['path']."&action=upload', 500, 145);");
  $docStrings['toolbar'] = $Toolbar->output();
}


/*if($GLOBALS['currentUserSiteID'])
{
  Header("Location: index.php?site_id=" . $GLOBALS['currentUserSiteID'] . "&module=" . defaultSiteModule);
  Die;
} */


$action = $_GET['action'];
$perm_accessadmin = CheckPermission(18);

switch ($action) {

  case "dummy":
    $docTemplate = "dlg_form.htm";
    $docTitle = "Test Form";
    $docStrings['frame'] = "?module=test&action=form_content";
    break;

  default:

    if($perm_accessadmin)
    {
      //DisplayToolbar();
    }

    $docTemplate = "main.htm";
    $docTitle = "--== CUSTOM ==--";
    $docScripts['body'] = "yfehome.default.php";
    break;
}


?>
