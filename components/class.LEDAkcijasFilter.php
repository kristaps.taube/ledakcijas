<?php

class LEDAkcijasFilter extends component
{

    public function output($category = false, $filter_link = false)
    {

        $filter_groups = $this->getFilterGroupsForCat($category);

        $active_filters = $filter_link ? ProductFilter::getActiveLinkFilters($filter_link['item_id']) : ProductFilter::getActiveCategoryFilters($category['item_id']);

        return $this->render('index', [
            'category' => $category,
            'filter_groups' => $filter_groups,
            'active_filters' => $active_filters,
            'filter_link' => $filter_link
        ]);

    }

    public function getFilterGroupsForCat($cat)
    {

        static $cache;

        $catIds = [];

        $cats_col = shopcatcollection::getInstance();
        $CatFilterGroupRel = EVeikalsCatFilterGroupRel::getInstance();
        $FilterGroups = EVeikalsFilterGroups::getInstance();

        if(!$cat) return [];
        $cat = (is_array($cat))? $cat : $cats_col->getFromCache($cat);

        if(isset($cache[$cat['item_id']])) return $cache[$cat['item_id']];
        $catIds[] = $cat['item_id'];

        while($cat['parent']){
            $cat = $cats_col->getFromCache($cat['parent']);
            $catIds[] = $cat['item_id'];
        };

        $ancestor_cats = $cats_col->getAncestorIds($cat);
        $catIds = array_merge($catIds, $ancestor_cats);

        $data = $CatFilterGroupRel->getByCatIds($catIds);
        $ids = [];
        foreach($data as $row){
            if(!in_array($row['filter_group_id'], $ids)){
                $ids[] = $row['filter_group_id'];
            }
        }

        if($ids){
            $cache[$cat['item_id']] = $FilterGroups->getByIds($ids);
            return $cache[$cat['item_id']];
        }

        $cache[$cat['item_id']] = Array();
        return Array();

    }

}