<?php
/**
*
*  Title: Password recovery
*  Author: Kristaps Taube <ktaube@datateks.lv>
*  Date: 22.01.2014
*  Project: E-Veikals
*
*/

include_once("class.component.php");

class EVeikalsPasswordRecovery extends component{

    public function __construct($name){
        parent::__construct($name);
        $this->SetProperties();
    }

  function initOptions(){

    $this->sender = option('PasswordRecovery\\sender', null, 'Password recovery sender', null, 'Info');
    $this->sender_email = option('PasswordRecovery\\sender_email', null, 'Password recovery sender email', null, 'ktaube@datateks.lv');

    return true;

  }

    function execute(){

        if(isset($_POST["ChangePassword"])){
            $user = $this->users->getByRecoverHash($_GET['recovery_key']);
            if($user){
                $this->change_errors = $this->process_password_change($user, $_POST);
            }
        }

    }

    function output()
    {

        if(!$this->initCollections()){
            echo $this->name.": Can't init collections.<br />";
            return;
        }

        $this->initOptions();

        if(isset($_GET['recovery_key'])){
            $r = $this->output_change_form($_GET['recovery_key']);
            return;
        }elseif($_GET['view'] == 'changed'){
            $this->output_changed();
            return;
        }

        $this->output_request_form();

    }

    function output_changed()
    {

        echo "<div class='success padding'>".$this->l("Parole atjaunota veiksmīgi")."</div>";
        return;

    }

  function process_password_change($user, $data){

    $errors = array();

    if(empty($data['password'])) $errors[] = $this->l("Jaunā parole ievadīta nekorekti");
    if(!empty($data['password']) && $data['password'] != $data['password2']) $errors[] = $this->l("Paroles nesakrīt");


    if(empty($errors)){

      // salt
      $salt = generate_random_string(5);

      // hashing password
      $upd['password'] = $salt.sha1($salt.$data['password']);
      $upd['recover_hash'] = '';

      $this->users->changeItemByIDAssoc($user['item_id'], $upd);
      header("location:?view=changed");
      die();
      return true;

    }

    return $errors;

  }

  function output_request_form($resp = NULL){

    if(isset($_POST['RecoverPassword'])){
      $resp = $this->process_recovery_request($_POST);
    }

    ?>
    <form method='post' action='<?=$_SERVER["REQUEST_URI"]?>' id='PasswordRecovery' class='padding'>
      <?=($resp === false)? "<div class='error'>".$this->l("Lietotājs netika atrasts")."</div>" : "" ?>
      <?=($resp === true)? "<div class='success'>".$this->l("Paroles atjaunošanas pieprasījums nosūtīts uz e-pastu")."</div>" : "" ?>
      <table>
        <tr>
          <td><label for='field_email'><?=$this->l("E-pasts")?></label>:</td>
          <td><input type='text' name='email' value='' id='field_email' /></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td><input type='submit' name='RecoverPassword' value='<?=$this->l("Atjaunot")?>' /></td>
        </tr>
      </table>
    </form>
    <?


  }

  function output_change_form($key){

    $user = $this->users->getByRecoverHash($key);

    if(!$user){
      echo "<div class='error padding'>".$this->l("Lietotājs netika atrasts")."</div>";
      return;
    }

    ?>
      <form class='padding' id='PaswChangeForm' method='post' action='<?=$_SERVER["REQUEST_URI"]?>'>
        <? if(isset($this->change_errors) && !empty($this->change_errors)){ ?>
        <ul class='errors'>
          <li><?=implode("</li><li>", $this->change_errors)?></li>
        </ul>
        <? } ?>
        <table>
          <tr>
            <td><label for='field_password'><?=$this->l("Jaunā parole")?></label></td>
            <td><input type='password' name='password' id='field_password' /></td>
          </tr>
          <tr>
            <td><label for='field_password_again'><?=$this->l("Jaunā parole atkārtoti")?></label></td>
            <td><input type='password' name='password2' id='field_password_again' /></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td><input type='submit' name='ChangePassword' value='<?=$this->l("Nomainīt paroli")?>' /></td>
          </tr>
        </table>
      </form>
    <?

  }

  function process_recovery_request($data){

    $user = $this->users->getUserByEmail($data['email']);
    if($user){

      $upd['recover_hash'] = sha1(rand().time());
      $this->users->changeItemByIDAssoc($user['item_id'], $upd);

      $subject = $this->l("Paroles atjaunošana");
      $text = $this->l("Sveicināts [name], <br /><br /> Lai atjaunotu paroli, sekojiet saitei [link]. <br /><br /> Ar cieņu,<br /> E-Veikala administrācija");

      $url = "http://".$GLOBALS['cfgDomain'].$_SERVER["REQUEST_URI"]."?recovery_key=".$upd['recover_hash'];

      $link = "<a href='".$url."'>".$url."</a>";

      $text = str_replace("[name]", $user['name'], $text);
      $text = str_replace("[link]", $link, $text);

      $t = phpMailerSend($text, $subject, $this->sender, $this->sender_email, $user['email'], true, true);

      return true;

    }else{
      return false;
    }

  }


  function SetProperties(){
    $this->properties = Array(

      "name"        => Array(
        "label"     => "Name:",
        "type"      => "str"
      ),

      "users" => Array(
        "label"       => "User collection:",
        "type"        => "collection",
        "collectiontype" => "shopusercollection",
        "lookup"      => GetCollections($this->site_id, "shopusercollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),
    );
  }



}