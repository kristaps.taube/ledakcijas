<?
//Serveris.LV Constructor component
//Created by Aivars Irmejs, 2006, aivars@serveris.lv

include_once("class.component.php");
require_once($GLOBALS['cfgDirRoot']."library/mPDF/mpdf.php");

class shopordereditor extends component{

    var $parent;
    var $normalclass;
    var $normalstyle;
    var $normalbullet;
    var $selectedclass;
    var $selectedstyle;
    var $selectedbullet;
    var $orderreciever;

    function shopordereditor($name){

        parent::__construct($name);
        $this->SetProperties();
        $this->use_automatic_prod_stock = option('Shop\\use_automatic_prod_stock', null, 'Use automatic product stock', Array("is_advanced" => true,'type' => 'check'));

    }

  function visibleService()
  {
    return true;
  }

  function getLanguageStrings()
  {
    return Array(
      'apm_cash_or_transfer' => 'Skaidrā naudā vai ar pārskaitījumu',
      'apm_firstdata' => 'Norēķinu karte',
    );
  }

    function init()
    {

        if (!$this->InitCollections()) return false;

        $this->shopman = shopmanager::getInstance();

        $this->orderform = OrderForm::getInstance();
        /*$this->orderform = shoporderform::getInstance();
        if ($this->orderform){
            $this->orderform->getServices();
            $this->orderform->InitLanguageStrings();
            $this->orderform->initCustomerForm();
        }else{
            echo 'shoporderform backendService not found!';
        }*/

        option('Shop\\OrderEditor', '', 'Order editor');
        $optcat = getOptionByPath('Shop\\OrderEditor');
        $params = Array('parent_id' => $optcat['id']);

        $this->allow_order_edit = option('allow_order_edit', null, 'Allow editing', array_merge($params, Array('is_advanced' => 1, 'type' => 'check')) );

        $this->use_automatic_prod_stock = option('Shop\\use_automatic_prod_stock', null, 'Use automatic product stock', Array("is_advanced" => true,'type' => 'check'));

        $this->use_Swedbank = readOption('Swedbank\\use');

        return true;

    }

    function ajax()
    {
        if (empty($_GET['ajaxmethod'])) die('Method not defined!');

        $method = 'ajax_' . $_GET['ajaxmethod'];

        if (!method_exists($this, $method)) die('Method not found!');

        ob_start();

            $this->$method();

        $html = ob_get_contents();
        ob_end_clean();

        echo parseVariables($html);
        die;
    }

    public function ajax_generateInvoice()
    {

        $this->initCollections();

        $id = isset($_GET['id']) ? $_GET['id'] : false;

        $order = $this->ordercol->getById($id);
        if(!$order) return;

        $data = unserialize($order['order_data']);
        $fields = $data['allfields'];
        $items = $data['products'];

        $orderform = OrderForm::getInstance();

        $user = $order['user_id'] ? $this->usercol->getById($order['user_id']) : false;

        $num = $orderform->getInvoiceNumber($order['item_id']);

        #$html = $orderform->getHtml($fields, $items, $user, $num, 'lv', $order, );

        $this->ordercol->update(['pavadzime' => $html], $order['item_id']);

        echo $html;

    }

	function dialogSwedbankLog(){

		$id = $_GET['id'];

		$entries = $this->swedbank_log->getByOrder($id);
		if(!$entries){
			echo lcms("No entries");
			return;
		}

		?>
		<link rel="stylesheet" type="text/css" href="/scr/components/shop/order_editor.css" />
		<script type="text/javascript" src="/scr/components/shop/order_editor.js"></script>
		<table id='SwedbankLogTable' style='width: 100%' class='data'>
	    <tr>
	      <th>#</th>
	      <th><?=lcms('Date')?></th>
	      <th><?=lcms('Event')?></th>
	      <th><?=lcms('Result')?></th>
	      <th><?=lcms('Details')?></th>
	    </tr>
			<?php foreach($entries as $entry){ ?>
			<tr data-id="<?=$entry['item_id']?>">
				<td><?=(++$i)?>.</td>
				<td><?=date("d.m.Y H:i:s",strtotime($entry['date']))?></td>
				<td><?=$this->swedbank_log->properties_assoc['event']['lookupassoc'][$entry['event']]?></td>
				<td><?=$entry['result']?></td>
				<td><a href='#' class='details'><?=lcms('Details')?></a></td>
			</tr>
			<tr class='details' data-for='<?=$entry['item_id']?>'>
				<td colspan='5'>
        	<table>
        		<tr>
        			<td class='key'><?=lcms("Date")?></td>
        			<td class='value'><?=date("d.m.Y H:i:s",strtotime($entry['date']))?></td>
        		</tr>
        		<tr>
        			<td class='key'><?=lcms("Notikums")?></td>
        			<td class='value'><?=$this->swedbank_log->properties_assoc['event']['lookupassoc'][$entry['event']]?></td>
        		</tr>
						<tr>
        			<td class='key'><?=lcms("Iznākums")?></td>
        			<td class='value'><?=$entry['result']?></td>
        		</tr>
						<tr>
        			<td class='key'><?=lcms("Merchant reference")?></td>
        			<td class='value'><?=$entry['merchant_reference']?></td>
        		</tr>
						<tr>
        			<td class='key'><?=lcms("Datacash reference")?></td>
        			<td class='value'><?=$entry['datacash_reference']?></td>
        		</tr>
						<tr>
							<td class='key'><?=lcms('Request')?></td>
							<td><pre><?=hsc($entry['request'])?></pre></td>
						</tr>
						<tr>
							<td class='key'><?=lcms('Response')?></td>
							<td><pre><?=htmlspecialchars($entry['response'])?></pre></td>
						</tr>
        	</table>
				</td>
			</tr>
			<? } ?>
		</table>
		<script>
			$(document).ready(function(){
				initSwedbankLog();
			});
		</script>
		<?



	}

  function Design(){
    if (!$this->init()) return;

    #$_SESSION['ordereditor_data'] = array();

    $jsparams = array(
      'ajax_url' => $this->getComponentModifierLink('ajax'),
      'edit_url' => $this->getComponentDialogLink('dialogEditOrder', Array(), lcms('Edit order'), 'submit'),
      'swedbank_log' => $this->getComponentDialogLink('dialogSwedbankLog', Array(), lcms('Swedbank log'), 'submit'),
    );

?>
  <script type="text/javascript" src="/scr/components/component.js"></script>

  <!-- jquery ui -->
  <script src="/scr/components/jquery-ui/js/jquery-ui-1.10.4.custom.js" type="text/javascript"></script>
  <link rel="stylesheet" href="/scr/components/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.min.css" type="text/css" />

  <!-- autocomplete -->
  <script type='text/javascript' src='/scr/js/jquery.autocomplete.js'></script>

  <!-- shop order editor -->
  <script type="text/javascript" src="/scr/components/shop/order_editor.js"></script>
  <link rel="stylesheet" type="text/css" href="/scr/components/shop/order_editor.css" />
  <link rel="stylesheet" type="text/css" href="/scr/css/autocomplete.css" />

  <script type="text/javascript">
    var shopordereditor = new ShopOrderEditor(<?=json_encode($jsparams) ?>);
    var ajax_url = <?=json_encode($this->getComponentModifierLink('ajax'))?>;
    var order_edit_url = <?=json_encode($this->getComponentDialogLink('dialogEditOrder', Array(), lcms('Edit order'), 'submit'))?>;
    var swedbank_log_url = <?=json_encode($this->getComponentDialogLink('dialogSwedbankUrl', Array(), lcms('Swedbank log'), 'submit'))?>;
  </script>
  <button style="float:right" id="new_order"><?=lcms('New order')?></button>
  <div class='cb'></div>
  <div id="order_editor_wrap">
    <div>
      <form action="" method="post" id="order_ed_form">
        <table id='OrderFilter'>
          <tr>
            <td class='label'><?=lcms('Email')?>:</td>
            <td><input type="text" name="email" value="" /> </td>
            <td class='label'><?=lcms('Customer')?>:</td>
            <td><input type="text" name="name" value="" /></td>
            <td class='label'><?=lcms('Date from')?>:</td>
            <td><input type="text" name="date_from" value="" class='date' /></td>
          </tr>
          <tr>
            <td class='label'><?=lcms('Phone nr')?>:</td>
            <td><input type="text" name="phone" value="" /></td>
            <td class='label'><?=lcms('Adress')?>:</td>
            <td><input type="text" name="address" value="" /></td>
            <td class='label'><?=lcms('Date to')?>:</td>
            <td><input type="text" name="date_to" value="" class='date' /></td>
          </tr>
          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td class='right'><button class="submit"><?=lcms('Filter')?></button> <span class="ajaxload" style="display:none"><img src="<?=$GLOBALS['cfgWebRoot'] ?>gui/ajaxload.gif" alt=""/></span></td>
          </tr>
        </table>
        <div class='cb'></div>
        <input type="hidden" name="page" value="0"/>
        <input type="hidden" name="sort_by" value="<?=$_SESSION['rekinieditorsortfield'] ?>"/>
        <input type="hidden" name="sort_asc" value="<?=$_SESSION['rekinieditorsortfieldasc'] ?>"/>
      </form>
    </div>
    <div class="orders_list">
      <? $this->outputOrders() ?>
    </div>
  </div>
<?
  }

  function extractOrdersFilterVars(){

    $vars = array(
      'telefons' => '~<td><span class="mazsTd">'.lcms('Customer phone').':</span></td>\s*<td><span class="mazsTd">(.*?)</span></td>~s',
      'adrese' => '~<td><span class="mazsTd">'.lcms('Delivery address').':</span></td>\s*<td><span class="mazsTd">(.*?)</span></td>~s',
      'epasts' => '~<td><span class="mazsTd">'.lcms('Customer email address').':</span></td>\s*<td><span class="mazsTd">(.*?)</span></td>~s',

    );

		$r = DB::GetTable('SELECT * FROM '.$this->ordercol->table);
    foreach ($r as $row){
      $item = array();
      foreach ($vars as $key => $regex){
        preg_match($regex, $row['rekins'], $m);
        $item[$key] = $m[1];
      }

      $this->ordercol->changeItemByIdAssoc($row['item_id'], $item);
    }

  }

  function ajax_getOrders()
  {
    if (!$this->init()) return;

    $this->outputOrders();
  }

  function dialogOrderHistory(){

    $this->initCollections();

    $id = (int)$_GET['order_id'];

    $order = $this->ordercol->getItemByIDAssoc($id);

    $i = 0;

    if($_POST['save']){

      $item = $_POST['entry'];

      $item['order_id'] = $id;
      $item['date'] = date("Y-m-d H:i:s");
      $item['status_from_id'] = $order['status_id'];
      $item['user'] = $_SESSION['session_user'];
      $this->order_history->addItemAssoc($item);
      $this->ordercol->changeItemByIDAssoc($id, array("status_id" => $item['status_to_id']));

      LEDAkcijasOrderStatusChangeNotifications::getInstance()->process($id, $order['status_id'], $item['status_to_id']);

    }

    $history = $this->order_history->getDBData(array("where" => "order_id = ".$id, "order" => "item_id DESC"));

    ?>
      <form method='post' action='' style='margin-bottom: 10px;'>
        <table>
          <tr>
            <td><?=lcms('New status')?>:</td>
            <td>
              <select style='margin:0px;padding:0px;' name='entry[status_to_id]'>
              <? foreach($this->statuses->getDBData() as $status){ ?>
              <option value='<?=$status['item_id']?>'><?=$status['status_'.CMS_LANG]?></option>
              <? } ?>
              </select>
            </td>
          </tr>
          <tr>
            <td><?=lcms('Comments')?>:</td>
            <td>
              <textarea style='margin:0px;padding:3px;width: 300px;height: 100px;' name='entry[comments]'></textarea>
            </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td><input type='submit' name='save' value='<?=lcms('Save')?>' /></td>
          </tr>
        </table>
      </form>
      <table id='OrderHistoryTable' style='width: 100%'>
        <tr>
          <th>#</th>
          <th><?=lcms('Status from')?></th>
          <th><?=lcms('Status to')?></th>
          <th><?=lcms('Date')?></th>
          <th><?=lcms('Comments')?></th>
          <th><?=lcms('User')?></th>
        </tr>
        <? foreach($history as $entry){  ?>
        <?
          $i++;
          $status_from = $this->getOrderStatus($entry['status_from_id']);
          $status_to = $this->getOrderStatus($entry['status_to_id']);
        ?>
        <tr>
          <td><?=$i?>.</td>
          <td><?=$status_from['status_'.CMS_LANG]?></td>
          <td><?=$status_to['status_'.CMS_LANG]?></td>
          <td><?=$entry['date']?></td>
          <td><?=$entry['comments']?></td>
          <td><?=$entry['user']?></td>
        </tr>
        <? } ?>
      </table>

			<script>

	      window.parent.onOk = function(){

					if(typeof parent.opener_callback_function == 'function'){
				  	parent.opener_callback_function();
				  }

					window.parent.close();

		      return false;
		    }
			</script>
    <?

  }

  function getOrderStatus($id){

    static $cache;

    if(isset($cache[$id])) return $cache[$id];

    $cache[$id] = $this->statuses->getItemByIDAssoc($id);

    return $cache[$id];

  }

  function outputOrders(){

    $pagesize = 20;
    $page = intval($_POST['page']);

    if(isset($_POST['sort_by'])){
      $_SESSION['rekinieditorsortfield'] = $_POST['sort_by'];
    }

    if(isset($_POST['sort_asc'])){
      $_SESSION['rekinieditorsortfieldasc'] = $_POST['sort_asc'];
    }

    $sortfield = $_SESSION['rekinieditorsortfield'];
    $asc = $_SESSION['rekinieditorsortfieldasc'];

    $params = array(
      'phone' => trim($_POST['phone']),
      'email' => trim($_POST['email']),
      'name' => trim($_POST['name']),
      'address' => trim($_POST['address']),
      'date_from' => trim($_POST['date_from']),
      'date_to' => trim($_POST['date_to']),
    );

    list($data, $count) = $this->ordercol->GetOrdersWithUsers($this->usercol, $sortfield, $page, $pagesize, $asc, $params);

    ?>
    <input type="submit" name='ExportToExcel' value='<?=lcms('Export')?>' id='ExportToExcel' />
    <table id="orders_table">
      <tr class="head">
        <th class='nr'>Nr.</th>
        <th><a href="#" rel="O.datums"><?=lcms('Date')?><?=($sortfield == 'O.datums' ? ($asc ? ' ↑' : ' ↓') : '')?></a></th>
        <th><a href="#" rel="O.kopeja_summa"><?=lcms('Sum')?><?=($sortfield == 'O.kopeja_summa' ? ($asc ? ' ↑' : ' ↓') : '')?></a></th>
        <th><a href="#" rel="U.email"><?=lcms('Customer')?><?=($sortfield == 'U.email' ? ($asc ? ' ↑' : ' ↓') : '')?></a></th>
        <th><a href="#" rel="O.telefons"><?=lcms('Phone')?><?=($sortfield == 'O.telefons' ? ($asc ? ' ↑' : ' ↓') : '')?></a></th>
        <th><a href="#" rel="O.piegadats"><?=lcms('Delivered')?><?=($sortfield == 'O.piegadats' ? ($asc ? ' ↑' : ' ↓') : '')?></a></th>
        <th><a href="#" rel="O.apmaksats"><?=lcms('Paid')?><?=($sortfield == 'O.apmaksats' ? ($asc ? ' ↑' : ' ↓') : '')?></a></th>
        <th><a href="#" rel="O.status_id"><?=lcms('Status')?></a></th>
        <th class='actionhead'>&nbsp;</th>
      </tr>
    <?

    $i = 0;
    foreach($data as $row){
      $i++;

      $url = $this->getComponentDialogLink('dialogEditOrder', Array('div' => 1, 'order_id' => $row['item_id']), lcms('Edit order'), 'submit');
      $edit_js = $this->getDesignIconScript($url, 700, 700,'','','function(){ window.location.reload(); }');

      $history_url = $this->getComponentDialogLink('dialogOrderHistory', Array('order_id' => $row['item_id']), lcms('Order history'), '');
      $history_edit_js = $this->getDesignIconScript($history_url, 700, 700,'','','function(){ window.location.reload(); }');

      $status = $this->statuses->getItemByIDAssoc($row['status_id']);

      $del_link = $this->getComponentModifierLink('del', Array('index' => $row['item_id']));
      ?>
      <tr bgcolor="<?=$status['color']?>" class="o-<?=$row['item_id']?>" data-id="<?=$row['item_id']?>">
        <td class='nr'><?=$row['item_id']?></td>
        <td class="center"><?=date('d.m.Y', strtotime($row['datums']))?><br /><?=date('H:i', strtotime($row['datums']))?></td>
        <td><?=$row['kopeja_summa']?></td>
        <td><?=$row['pasutitajs']?></td>
        <td><?=$row['telefons']?></td>
        <td class="center"><input type="checkbox" name="piegadats" value="<?=$row['item_id']?>" <?=($row['piegadats'] ? 'checked' : '')?> /></td>
        <td class="center"><input type="checkbox" name="apmaksats" value="<?=$row['item_id']?>" <?=($row['apmaksats'] ? 'checked' : '')?> /></td>
        <td class="center"><a href="#" onclick="<?=$history_edit_js?>"><?=$status['status_'.CMS_LANG]?></a></td>
        <td class="actions">
            <div class='wrap'>
                <div class='title'><?=lcms("Actions")?></div>
                <div class='list'>
                    <ul>
                        <li><a href='#' class='toggleOrder'><?=lcms('Order')?></a></li>
                        <li><a href='#' class='toggleInvoice'><?=lcms('Invoice')?></a></li>
                        <?php /* <li><a href="<?=$this->SaveOrderInMSword($row['item_id'])?>"><?=lcms('Save(.DOC)')?></a></li> */ ?>
                        <li><a target='_blank' href="<?=$this->SaveOrderInPDF($row['item_id'])?>"><?=lcms('Save(.PDF)')?></a></li>
                        <?php if($this->use_Swedbank){ ?>
                        <li><a href='#' class='viewSwedbankLog'><?=lcms('Swedbank log')?></a></li>
                        <?php } ?>
                        <?php if(!$row['apmaksats']){ ?>
                        <li><a href="#" onclick="<?=$edit_js?>"><?=lcms('Edit')?></a></li>
                        <?php } ?>
                        <? if($row['order_data']){ ?>
                        <li><a href="#" class="new" rel="o-<?=$row['item_id']?>"><?=lcms('New')?></a></li>
                        <?php } ?>
                        <li><a href="#" class="generate-invoice" data-id="<?=$row['item_id']?>"><?=lcms('Generate invoice')?></a></li>
                        <li><a href="#" class="send" rel="o-<?=$row['item_id']?>"><?=lcms('Send')?></a></li>
                        <li><a href="#" class="delete" rel="o-<?=$row['item_id']?>" data-link="<?=$del_link?>"><?=lcms('Delete')?></a></li>
                    </ul>
                </div>
            </div>
        </td>
      </tr>
      <tr id="orderrow_<?=$row['item_id']?>" class='order orderrow'>
        <td colspan="9" style="border: 1px solid black; width:100%" ><br /><?php echo $row['rekins']?><br /></td>
      </tr>
      <tr id="invoicerow_<?=$row['item_id']?>" class='order orderrow'>
        <td colspan="9" style="border: 1px solid black; width:100%" ><br /><?php echo $row['pavadzime'] ? $row['pavadzime'] : lcms('No invoice')?><br /></td>
      </tr>
      <?

    }
    ?>
    </table>
    <?

    //pages
    echo '<div class="pages">';
    $pagecount = ceil($count / $pagesize);
    for($f = 0; $f < $pagecount; $f++)
    {
      echo '<a href="#" rel="pg-'.$f.'"'.($page == $f ? ' style="font-weight:bold"' : '').'>' . ($f + 1) . '</a> ';
    }
    echo '</div>';
  }

  function downloadPDFOrder()
  {
    $this->InitCollections();
    if(isset($_GET["order_id"]) && intval($_GET["order_id"]) ){

      $ordertext = $this->ordercol->GetItemByIdAssoc(intval($_GET["order_id"]));
      $ordertext = $ordertext['rekins'];

      $mpdf = new mPDF('');
      $mpdf->WriteHTML($ordertext);
      $mpdf->Output();

      die();
    }
  }

  function downloadOrder()
  {
    $this->InitCollections();
    //save order to MS Word file   /files/rekini/
    if(isset($_GET["order_id"]) && intval($_GET["order_id"]) ){
      $savedir = sqlQueryValue("select dirroot from sites where site_id=".$this->site_id);
      $savedir .= 'files/rekini/';
      $datums = date('H_i_s__d_m_Y');
      $ordertext = $this->ordercol->GetItemByIdAssoc(intval($_GET["order_id"]));
      $ordertext = $ordertext['rekins'];

      header("Content-Type: application/octet-stream");
      header('Content-Disposition: inline; filename="rekins_'.$datums.'.doc"');
      header("Content-Transfer-Encoding: binary");

      echo chr(0xFF).chr(0xFE).mb_convert_encoding($ordertext, 'UTF-16LE', 'UTF-8');

      die();
    }
  }

  function ajax_resendOrder()
  {
    $this->initCollections();

    $orderform = $this->getBackendServiceByName('OrderGenerator');
    if (!$orderform) die('OrderGenerator backendService not found!');
    $orderform->getServices();
		$orderform->InitLanguageStrings();

    $order_id = intval($_GET['order_id']);
    list($o) = $this->getOrderData($order_id);
    if (!$o) return;

    $orderform->srvSendOrders($o['products'], $o['sum'], $o['allfields'], $o['orderlang'], $o['user'], $order_id, true);

    $this->ordercol->changeItemByIdAssoc($order_id, Array(
      'sent' => 1
    ));

    $_GET['order_id'] = '';

    die('1');
  }

  function getOrderData($id)
  {
    if (!$id) return;

    $this->InitCollections();

    $order = $this->ordercol->getItemByIdAssoc($id);
    if (!$order) return;

    return Array(unserialize($order['order_data']), $order);
  }

  function getProductAddTableRow($data, $key){

    $id = $data['product']['item_id'];

    if($data['sizes'] === NULL){ // nav norādīts, tātad no pop-up izvēlnes => vajag pielikt
      $size_ids = $this->product_sizes->getSizeIdsForProduct($id);
      if(count($size_ids)){
        $data['sizes'] = $this->sizes->getDBData(array("where" => "item_id IN(".implode(",", $size_ids).")", "assoc" => true));
      }else{
        $data['sizes'] = false;
      }
    }

    if($data['colors'] === NULL){
      $color_ids = $this->product_colors->getColorIdsForProduct($id);
      if(count($color_ids)){
        $data['colors'] = $this->colors->getDBData(array("where" => "item_id IN(".implode(",", $color_ids).")", "assoc" => true));
      }else{
        $data['colors'] = false;
      }
    }

    if($data['volumes'] === NULL){
      $data['volumes'] = $this->pricescol->getPricesByProduct($id);
    }

    ob_start();

  ?>
    <tr id="cartid-<?=$key?>">
      <td><?=$data['name']?></td>
      <td>
        <? if($data['volumes']){ ?>
        <select name="price_id">
        <? foreach($data['volumes'] as $volume){ ?>
          <option value="<?=$volume['item_id']?>" <?=($volume['item_id'] == $data['price_id'])? "selected='selected'" : "" ?>><?=$volume['name_lv']?></option>
        <? } ?>
        </select>
        <? }else{ ?>
        <?=lcms('There is no volumes')?>			
        <? } ?>
      </td>
      <td>
        <? if($data['sizes']){ ?>
        <select name="size">
        <? foreach($data['sizes'] as $size){ ?>
          <option value="<?=$size['item_id']?>" <?=($size['item_id'] == $data['size'])? "selected='selected'" : "" ?>><?=$size['title_lv']?></option>
        <? } ?>
        </select>
        <? }else{ ?>
       <?=lcms('There is no sizes')?>
        <? } ?>
      </td>
      <td>
        <? if($data['colors']){ ?>
        <select name="color">
        <? foreach($data['colors'] as $color){ ?>
          <option value="<?=$color['item_id']?>" <?=($color['item_id'] == $data['color'])? "selected='selected'" : "" ?>><?=$color['title_lv']?></option>
        <? } ?>
        </select>
        <? }else{ ?>
        <?=lcms('There is no colors')?>
        <? } ?>
      </td>
      <td>
        <input type="text" name="count" value="<?=$data['count']?>" />
      </td>
      <td class="price">
        <input type="text" name="price" class='price' value="<?=$data['price']?>" />
      </td>
      <td class="sum">
        <?=number_format($data['count'] * $data['price'], 2)?>
      </td>
      <td>
        <img src="gui/images/new/delete.png" alt="" class='remove' />
      </td>
    </tr>
  <?

    $row = ob_get_contents();
    ob_end_clean();

    return $row;

  }

  function ajax_removeProduct(){

    unset($_SESSION['ordereditor_data']['products'][$_POST['key']]);

  }

  function ajax_changeProduct(){

    $this->initCollections();

    $product = &$_SESSION['ordereditor_data']['products'][$_POST['key']];

    $product[$_POST['property']] = $_POST['value'];

    if($_POST['property'] == 'price_id'){
      $pr = $this->pricescol->getItemByIdAssoc($_POST['value']);
      $product['price_row'] = $pr;
      $product['price'] = $pr['price'];
    }

    $product['name'] = $this->generateProductNameForOrder($product); // generate new name

    die($product['name']);

  }

  function generateProductNameForOrder($prod){

    $name = $prod['product']['name_lv'];

    $add = array();


    if($prod['price_id']){
      $volume = $this->pricescol->getItemByIDAssoc($prod['price_id']);
      $add[] = $volume['name'];
    }

    if($prod['size']){
      $size = $this->sizes->getItemByIDAssoc($prod['size']);
      $add[] = $size['title_lv'];
    }

    if($prod['color']){
      $color = $this->colors->getItemByIDAssoc($prod['color']);
      $add[] = $color['title_lv'];
    }

    $name = (empty($add))? $name : $name." (".implode(", ", $add).")";

    return $name;

  }

  function ajax_ExportOrderList(){

    require_once($GLOBALS['cfgDirRoot'].'library/PHPExcel.php');
    require_once($GLOBALS['cfgDirRoot'].'library/PHPExcel/IOFactory.php');

    $this->initCollections();

    $pagesize = 20;
    $page = intval($_POST['page']);

    $sortfield = $_SESSION['rekinieditorsortfield'];
    $asc = $_SESSION['rekinieditorsortfieldasc'];

    $params = array(
      'phone' => trim($_GET['phone']),
      'email' => trim($_GET['email']),
      'name' => trim($_GET['name']),
      'address' => trim($_GET['address']),
      'date_from' => trim($_GET['date_from']),
      'date_to' => trim($_GET['date_to']),
    );

    list($data, $count) = $this->ordercol->GetOrdersWithUsers($this->usercol, $sortfield, $page, $pagesize, $asc, $params);

    $excel = new PHPExcel();
    $excel->setActiveSheetIndex(0);
    $sheet = $excel->getActiveSheet();

    $style = array('font' => array('bold' => true));

    $col = 0;
    $row = 1;


    $headers = array(
      lcms("Nr."),
      lcms("Date"),
      lcms("Sum"),
      lcms("Customer"),
      lcms("Phone"),
      lcms("Delivered"),
      lcms("Delivery date"),
      lcms("Paid"),
      lcms("Date of payment"),
      lcms("Status")
    );

    // writting headers
    foreach($headers as $header){
      $sheet->setCellValue($this->colLetter($col).$row, $header);
      $sheet->getStyle($this->colLetter($col++).$row)->applyFromArray($style);
    }

    foreach($data as $order){
      $row++;
      $col = 0;

      $status = $this->statuses->getItemByIDAssoc($order['status_id']);
      $sheet->setCellValue($this->colLetter($col++).$row, $order['item_id']);
      $sheet->setCellValue($this->colLetter($col++).$row, date('d.m.Y H:i:s', strtotime($order['datums'])));
      $sheet->setCellValue($this->colLetter($col++).$row, $order['kopeja_summa']);
      $sheet->setCellValue($this->colLetter($col++).$row, $order['pasutitajs']);
      $sheet->setCellValue($this->colLetter($col++).$row, $order['telefons']);
      $sheet->setCellValue($this->colLetter($col++).$row, $order['piegadats']? lcms("Yes") : lcms("No") );
      $sheet->setCellValue($this->colLetter($col++).$row, $order['piegadats']? convertDateFromMySQL($order['piegades_datums']) : "");
      $sheet->setCellValue($this->colLetter($col++).$row, $order['apmaksats']? lcms("Yes") : lcms("No") );
      $sheet->setCellValue($this->colLetter($col++).$row, $order['apmaksats']? convertDateFromMySQL($order['apmaksas_datums']) : "");
      $sheet->setCellValue($this->colLetter($col++).$row, $status['status']);

    }

    // output file
    header("Content-type: application/octet-stream");
    header('Content-Disposition: attachment; filename="Orders.xls"');
    header("Content-Transfer-Encoding: binary");

    $objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
    $objWriter->save('php://output');

  }

  function colLetter($i){
    return chr(ord('A')+$i);
  }

  function ajax_addProduct(){

    $this->initCollections();

    $prod = DB::GetRow("SELECT * FROM `".$this->prodcol->table."` WHERE name_lv = :name AND shortcut = 0", array(":name" => $_POST['name']));

    if(!$prod){
      $prod = DB::GetRow("SELECT * FROM `".$this->prodcol->table."` WHERE code = :code AND shortcut = 0", array(":code" => $_POST['name']));
    }

    $response = array();

    if($prod){

      $response['error'] = false;

      $color_ids = $this->product_colors->getColorIdsForProduct($prod['item_id']);
      if(count($color_ids)){
        $colors = $this->colors->getDBData(array("where" => "item_id IN(".implode(",", $color_ids).")", "assoc" => true));
      }else{
        $colors = false;
      }

      $size_ids = $this->product_sizes->getSizeIdsForProduct($prod['item_id']);
      if(count($size_ids)){
        $sizes = $this->sizes->getDBData(array("where" => "item_id IN(".implode(",", $size_ids).")", "assoc" => true));
      }else{
        $sizes = false;
      }

      $volumes = $this->pricescol->getPricesByProduct($prod['item_id']);

      $response['colors'] = $colors;
      $response['sizes'] = $sizes;
      $response['volumes'] = $volumes;
      $response['product'] = $prod;

      $price = (count($volumes) && $volumes[0]['price'])? $volumes[0]['price'] : $prod['price'];

      $pdata = array();
      foreach($volumes as $vol){
        $pdata[$vol['item_id']] = $vol;
      }

      $response['name'] = $prod['name_lv'];
      $response['count'] = 1;
      $response['price'] = $price;

      // first we need to add product to basket
      $o = &$_SESSION['ordereditor_data'];
      $key = $o['products'] ? max(array_keys($o['products'])) + 1 : 1; // get next key
      $response['key'] = $key;
      $o['products'][$key] = array(
        "size" => count($sizes)? $sizes[0]['item_id'] : false,
        "price_id" => count($volumes)? $volumes[0]['item_id'] : false,
        "color" => count($colors)? $colors[0]['item_id'] : false,
        "product" => $prod,
        "id" => $prod['item_id'],
        "count" => 1,
        "price" => $price,
        "base_price" => $price,
        "prices" => $pdata
      );

      $o['products'][$key]['name'] = $this->generateProductNameForOrder($o['products'][$key]);

      $response['table_row'] = $this->getProductAddTableRow($response, $key);
      $response['ProductRowData'] = $o['products'][$key];

    }else{

      $response['error'] = true;
      $response['error_msg'] = lcms("Product not found");

    }

    echo json_encode($response);

  }

  function ajax_OrderFilterAutocomplete(){

    $this->initCollections();
    $query = $_GET['query'];

    $field = $_GET['field'];

    $results = array();

    // form_field => db_field
    $default_fields = array(
      'email' => 'epasts',
      'phone' => 'telefons',
      'address' => 'adrese',
      'name' => 'pasutitajs',
    );

    if(isset($default_fields[$field])){

      $f = $default_fields[$field];

      $entries = $this->ordercol->getDBData(array(
        "what" => $f,
        "where" => $f." LIKE :query",
        "assoc" => true,
        "count" => 100,
        "order" => $f,
				"params" => array(":query" => "%".$query."%")
      ));

      foreach($entries as $entry){
        if(!in_array($entry[$f], $results)){
          $results[]  = $entry[$f];
        }
      }

    }

    echo json_encode(array("suggestions" => $results));

  }

  function ajax_autocomplete(){

    $this->initCollections();

    $query = $_GET['query'];

    $search_cond = array();

		$params = array(":query" => "%".$query."%");
    $search_cond[] = "(name_lv LIKE :query OR code LIKE :query)";
    $search_cond[] = "shortcut = 0"; // no shortcuts please

    $prods = DB::GetTable("
    	SELECT name_lv, code
			FROM `".$this->prodcol->table."`
			WHERE ".implode(" AND ", $search_cond)."
			ORDER BY name_lv
			LIMIT 100
		", $params);


    $results = array();
    foreach($prods as $prod){
      $results[]  = $prod['name_lv'];
      if($prod['code']) $results[]  = $prod['code'];
    }

    echo json_encode(array("suggestions" => $results));

  }

  function outputEditOrderProductsTable($order_data){
    $this->initCollections();

    $data = $order_data['products'] ? $order_data['products'] : array();
    #dump($data);
    $sum = 0;
    foreach ($data as $key => $prod){
      $delconfirm = ($prod['count'] == 1) ? lcms('Do you really want to remove this product from invoice?') : '';
      $data[$key]['icons_col'] =
        '<a href="#" class="edit" rel="p-'.$key.'"><img src="'.$GLOBALS['cfgWebRoot'].'gui/images/edit.gif" alt=""/></a> ' .
        '<a href="#" class="del" rel="p-'.$key.'"><img src="'.$GLOBALS['cfgWebRoot'].'gui/images/del.gif" alt=""/></a> ';

      $price_name = '';

      if($prod['price_id']){

        $price = $this->pricescol->getItemByIDAssoc($prod['price_id']);
        $price_name = $price['name'];
        $data[$key]['price'] = $data[$key]['price'] ? $data[$key]['price'] : $price['price'];

        $price_data = array();
        foreach($this->pricescol->getPricesByProduct($prod['product']['item_id']) as $p){
          $price_data[$p['item_id']] = $p;
        }
        $data[$key]['prices'] = $price_data;

      }

      $sum += $data[$key]['price'] * $data[$key]['count'];

      $data[$key]['price_name'] = $price_name;

      $data[$key]['name'] = $data[$key]['product']['name_lv'];

    }



    if ($_GET['div']) echo '<div id="div_'.$this->name.'" class="products">';
    ?>
      <script>
        var ajax_url = <?=json_encode($this->getComponentModifierLink('ajax'))?>;
        var ProductData = <?=json_encode($data)?>;
      </script>
      <div class='catalog-menu'></div>
      <div id='cart'>
          <div id='ProductList'>
            <table width="100%" border="0" cellspacing="0" cellpadding="4" style="border: 1px solid #C0C0C0;">
              <tr>
                <td class='tableHead'><?=lcms('Product')?></td>
                <td class='tableHead'><?=lcms('Volume')?></td>
                <td class='tableHead'><?=lcms('Size')?></td>
                <td class='tableHead'><?=lcms('Color')?></td>
                <td class='tableHead'><?=lcms('Count')?></td>
                <td class='tableHead'><?=lcms('Price')?></td>
                <td class='tableHead'><?=lcms('Sum')?></td>
                <td class='tableHead'><?=lcms('Delete')?></td>
              </tr>
              <? foreach($data as $key => $prod){ ?>
               <?=$this->getProductAddTableRow($prod, $key)?>
              <? } ?>
            </table>
            <div class='total'>
              <div class='f'><?=lcms('Product')?>: <input style='width: 200px' type='text' name='autocomplete' value='' /><input type='submit' name='add' value='<?=lcms('Add')?>' /> (<?=lcms('Name or code')?>)</div>
              <div class='t'><?=lcms('Total')?>: <span><?=number_format($sum, 2)?></span></div>
              <div class='cb'></div>
            </div>
          </div>
      </div>

    <?

    if ($_GET['div']) echo '</div>';
    #dump($_SESSION['ordereditor_data']['products']);

  }

  function dialogEditOrder(){

    $this->init();

    $order_id = intval($_GET['order_id']);
    if ($order_id){

      list($order_data) = $this->getOrderData($order_id);
      $_SESSION['ordereditor_data'] = $order_data;
      if (!$order_data) { echo lcms("Invoice doesn't contain information for editing!");  return; }

    }else{

      if (!is_array($_SESSION['ordereditor_data'])){
        $_SESSION['ordereditor_data'] = array();
      }

      $order_data = $_SESSION['ordereditor_data'];
    }

    $fields = array();
    if ($_GET['new_from']){
      $id = intval($_GET['new_from']);
      $ord = $this->ordercol->getItemByIdAssoc($id);
      if (!$ord['order_data']) { echo lcms("Invoice doesn't contain information for editing!");  return; }

      $od = unserialize($ord['order_data']);
      $fields = $od['allfields'];

      $_SESSION['ordereditor_data']['products'] = $od['products'];
      $order_data = $_SESSION['ordereditor_data'];
      $order_data['user'] = $od['user'];
    }else{

        $fields = $order_data['allfields'];

    }

    $this->outputEditOrderProductsTable($order_data);

    echo '<button id="add_product">'.lcms('Add product').'</button>';

    ob_start();

    $this->orderform->loadFieldData($fields);

    #$orderform = shoporderform::getInstance();
    #$orderform->initCustomerForm($fields);
    #$orderform->getServices();
    #$orderform->InitLanguageStrings();
    #$err = $orderform->processCustomerForm();

    echo '
        <link rel="stylesheet" href="/styles/assets.min.css">
        <link rel="stylesheet" href="/styles/style.min.css">
        <link rel="stylesheet" href="/styles/custom.css">
        <link rel="stylesheet" type="text/css" href="/scr/components/shop/order_editor_popup.css" />

        <script type="text/javascript" src="/scr/components/shop/order_editor_popup.js"></script>
        <script src="/scripts/dependencies.min.js"></script>
        <script type="text/javascript" src="/scr/js/jquery.autocomplete.js"></script>
        <script src="/scripts/scripts.min.js"></script>
        <script src="/scripts/custom.js"></script>
        <script src="/scripts/checkout.js"></script>
    ';

    echo '<div id="cust_form" class="order-form-section">';
    echo $this->orderform->output(true);
    echo '</div>';

    $form_html = ob_get_contents();
    ob_end_clean();

    $ajax_url = $this->getComponentModifierLink('ajax');

    $users = $this->usercol->getDBData(array('order' => 'email', 'where' => 'savedfields!=""'));

    $jsvars = array(
      'order_id' => $order_id,
      'ajax_url' => $ajax_url,
      'edit_url' => $this->getComponentDialogLink('dialogOrderEditProduct', Array(), lcms('Edit product')),
      'del_url' => $this->getComponentModifierLink('dialogOrderDelProduct'),
      'add_url' => $this->getSelectProductDialogUrl(Array('order_id', 'user_id'), lcms('Add product'), 'dialogSelectProductSubmit','',false)
    );

    ob_start();
?>

  <select id="user_id" name="user_id">
    <option value="0"></option>
  <? foreach ($users as $u) { ?>
    <option value="<?=$u['item_id'] ?>"<?=($order_data['user']['item_id'] == $u['item_id']) ? ' selected="selected"' : '' ?>><?=$u['email'] ?></option>
  <? } ?>
  </select>
  <button id="load_user_data"><?=lcms('Load')?></button>

  <script type="text/javascript">

  var phpvars = <?=json_encode($jsvars) ?>;

  $(function()
  {
    $('#load_user_data').click(function()
    {
      var user_id = $('#user_id').val();

      $('#cust_form').html('');

      $.post(phpvars.ajax_url+'&ajaxmethod=changeOrderUser', { user_id: user_id, order_id: phpvars.order_id }, function(data)
      {
        $.get(phpvars.ajax_url+'&ajaxmethod=getOrderForm&user_id='+user_id, function(data)
        {
          $('#cust_form').html(data);
        });
      });

      return false;
    });

    $('#add_product').click(function()
    {
      try {
          PWin.close();
      } catch(e) { }
      var args = new Array(window, null);

      var user_id = $('#user_id').val();

      //openModalWin(phpvars.add_url+"&order_id="+phpvars.order_id+"&user_id="+user_id, args, 800, 500, 0, 0, 0);
      openDlg(phpvars.add_url+"&order_id="+phpvars.order_id+"&user_id="+user_id, args, {w: 800, h: 500}, function(){

      	location.reload();

      });

      load_products_table();
      return false;
    });

    init_products_table();


    var old_onOk = window.parent.onOk;

    window.parent.onOk = function(){
      var post = get_form_values('#Form');
      var form = $('#Form');

      $(".validation.error", form).remove();

      $.post(phpvars.ajax_url+'&ajaxmethod=validateOrderForm', post, function(data){
        if (data == '1'){

					// here we are submitting form via AJAX, cuz we need to reload opener after form is submited a.k.a. in callback
        	//old_onOk();
					$.ajax({
					  type: "POST",
					  url: form.attr("action"),
					  data: form.serialize(),
					  success: function() {
					    if(typeof parent.opener_callback_function == 'function'){
						  	parent.opener_callback_function();
						  }

							window.parent.close();
					  }
					});

        }else{
        	form.prepend(data);
        }


      });

      return false;
    }

  });

  function init_products_table()
  {
    $('.products a.edit').click(function()
    {
      var key = $(this).attr('rel').substr(2);

      try {
          PWin.close();
      } catch(e) { }
      var args = new Array(window, null);

      //openModalWin(phpvars.edit_url+"&order_id="+phpvars.order_id+'&prod_key='+key, args, 450, 500, 0, 0, 0);
      openDlg(phpvars.edit_url+"&order_id="+phpvars.order_id+'&prod_key='+key, {w: 450, h: 500}, function(val){

      	//console.log(val);

      });

      load_products_table();

      return false;
    });

    $('.products a.del').click(function()
    {
      var key = $(this).attr('rel').substr(2);

      if (!confirm('<?=lcms('Do you really want to remove this product from invoice?')?>'))
        return false;

      $.post(phpvars.del_url+'&order_id='+phpvars.order_id+'&prod_key='+key, function(data)
      {
        load_products_table();

      });

      return false;
    });

  }

  function load_products_table()
  {
    $.get(phpvars.ajax_url+'&ajaxmethod=getOrderProductsTable&order_id='+phpvars.order_id, function(data)
    {
      $('.products').html(data);
      init_products_table();
    });

  }

  function get_form_values(formsel)
  {
    var values = {};
    var a = $(formsel).serializeArray();
    for (var k in a)
    {
      if (!a[k]['name']) continue;
      values[a[k]['name']] = a[k]['value'];
    }

    return values;
  }

  </script>
<?
    $user_html = ob_get_contents();
    ob_end_clean();

    #echo $form_html;

    $form = array(
      'form' => array(
        'type' => 'code',
        'value' => $form_html
      )
    );
    echo $this->getDialogFormOutput2($form, 'dialogEditOrderSave', '', Array('order_id' => $_GET['order_id']));


  }

  function ajax_validateOrderForm()
  {

    $this->init();
    $this->orderform->loadFieldData($_POST);
    $no_errors = $this->orderform->validateFields();

    if (!$no_errors){

      $erro = $this->orderform->getErrors();
      $err = [];
      foreach($erro as $field => $field_errors){
        foreach($field_errors as $error){
          $err[] = $error;
        }
      }

      echo 'Errors: '.implode("<br />", $err).'<br/>';

    }else{
        echo '1';
    }

    die();

      #echo lcms('Error').': '.implode("<br />", $err).'<br/>';


    die(empty($err) ? '1' : "<ul class='error validation'><li>".implode("</li><li>", $err)."</li></ul>");
  }

  function ajax_getOrderProductsTable()
  {
    $order_id = intval($_GET['order_id']);
    if ($order_id)
    {
      list($order_data) = $this->getOrderData($order_id);
      if (!$order_data) return;
    }
    else
      $order_data = $_SESSION['ordereditor_data'];

    $this->outputEditOrderProductsTable($order_data);
  }

  function ajax_changeOrderUser()
  {
    if (!$this->initCollections()) die;

    $order_id = intval($_POST['order_id']);
    $user_id = intval($_POST['user_id']);

    list($order_data) = $this->getOrderData($order_id);

    $user = null;
    if ($user_id)
      $user = $this->usercol->getItemByIdAssoc($user_id);

    $order_data['user'] = $user;

    $this->ordercol->changeItemByIdAssoc($order_id, array(
      'user_id' => $user_id,
      'order_data' => db_escape(serialize($order_data))
    ));

    die;
  }

  function ajax_getOrderForm()
  {
    if (!$this->initCollections()) die;

    $user_id = intval($_GET['user_id']);
    if ($user_id)
      $user = $this->usercol->getItemByIdAssoc($user_id);

    $orderform = $this->getBackendServiceByName('shoporderform', true);

    $fields = unserialize($user['savedfields']);
    $orderform->initCustomerForm($fields);

    $orderform->outputCustomerForm(false);

    die;
  }

  function getSelectProductDialogUrl($paramkeys, $title, $submitmethod, $cmpname='', $cmptype='', $checkboxes = false)
  {
    $params = array('pcatparamkeys' => implode(',', $paramkeys) );
    $params['pcatsubmitmethod'] = $submitmethod;
    $params['pcatcmpname'] = $cmpname ? $cmpname : $this->name;
    $params['pcatcmptype'] = $cmptype ? $cmptype : get_class($this);
		$params['checkboxes'] = $checkboxes? 1 : 0;

    return $this->getComponentDialogLink('dialogSelectProduct', $params, $title, 'close');
  }

  function getSelectProductDialogJs($paramkeys, $title, $submitmethod, $submitafterrefreshmethod='', $cmpname='', $cmptype='', $div_id='', $url_suffix='')
  {
    $params = array('pcatparamkeys' => implode(',', $paramkeys));
    $params['pcatsubmitmethod'] = $submitmethod;
    $params['pcatsubmitafterrefreshmethod'] = $submitafterrefreshmethod;
    $params['pcatcmpname'] = $cmpname ? $cmpname : $this->name;
    $params['pcatcmptype'] = $cmptype ? $cmptype : get_class($this);
    if (!$div_id) $div_id = 'div_'.$this->name;
    $url = $this->getComponentDialogLink('dialogSelectProduct', $params, $title, 'submit');
    $url .= $url_suffix;
    return $this->getDesignIconScript($url, 800, 500, '', $div_id);
  }

  function srvGetSelectProductDialogJs($params, $title, $submitmethod, $submitafterrefreshmethod='', $cmpname='', $cmptype='', $div_id='', $url_suffix=''){
    return $this->getSelectProductDialogJs($params, $title, $submitmethod, $submitafterrefreshmethod, $cmpname, $cmptype, $div_id, $url_suffix);
  }

  function dialogEditOrderSave(){

    $this->init();
    $order_id = isset($_GET['order_id']) && $_GET['order_id'] ? intval($_GET['order_id']) : false;

    $this->orderform->loadFieldData($_POST);

    $no_errors = $this->orderform->validateFields();
    if (!$no_errors){

      $erro = $this->orderform->getErrors();
      $err = [];
      foreach($erro as $field => $field_errors){
        foreach($field_errors as $error){
          $err[] = $error;
        }
      }

      echo lcms('Error').': '.implode("<br />", $err).'<br/>';
      $this->dialogEditOrder();
      die;
    }

    $order_data = $_SESSION['ordereditor_data'];
    $order_data['allfields'] = $this->orderform->getFieldValues();
    $order_data['orderlang'] = 'lv';

    if($order_id){
      $orderprods = $this->orderprodcol->getProductsByOrder($order_id, true);
      $order = $this->ordercol->getByID($order_id);
    }else{
      $orderprods = $_SESSION['ordereditor_data']['products'];
      $order = false;
    }

    $sum = 0;
    foreach ($order_data['products'] as $pk => $prod){

      $sum += $prod['price'] * $prod['count'];

      if ($order_id){

        foreach ($orderprods as $key => $op){
          if ($op['cart_key'] == $key){
            if($this->use_automatic_prod_stock){
                $count = (int)$prod['count'];
                if($op['count'] > $count){
                    $new_count = array($op['prod_id'] => round($op['count'] - $count));
                    //dump($new_count);die;
                    $this->prodcol->changeBackProdCount($new_count);
                }elseif($op['count'] != $count){
                    $new_count = array($op['prod_id'] => round($count - $op['count']));
                    //dump($new_count);die;
                    $this->prodcol->updateProductsTimesPurchased($new_count,$this->use_automatic_prod_stock);
                }
            }
            $this->orderprodcol->changeItemByIdAssoc($op['item_id'], array(
              'price' => $prod['price'],
              'count' => $prod['count'],
              'price_id' => $prod['price_id'] ? $prod['price_id'] : 0,
              'size_id' => $prod['size'] ? $prod['size'] : 0,
              'color_id' => $prod['color'] ? $prod['color'] : 0,
            ));

          }

        }

      }

    }

    $fields = $this->orderform->getFields();
    $field_values = $this->orderform->getFieldValues();

    $delivery_method = $this->orderform->getDeliveryMethod($fields, $order_data['products'], 'lv');
    $delivery_price = $delivery_method && isset($delivery_method['price']) ? $delivery_method['price'] : 0;

    $sum_discount_data = $this->orderform->processSumDiscount($sum, $order_data['products']);

    $sum = $totalsum = $sum + $delivery_price - $sum_discount_data['sum'];

    $order_data['sum'] = $sum;
    $order_data['user'] = $_POST['user_id'] ? $this->usercol->getItemByIdAssoc($_POST['user_id']) : null;

    #$ordertext = $this->orderform->srvGetOrder($order_data['products'], $order_data['sum'], $order_data['allfields'], '', $order_data['user'], $order_id ? $order_id :  $this->ordercol->getNextId());

    $o_id = $order_id ? $order_id : $this->ordercol->getNextId();
    $order_num = $this->orderform->getOrderNumber($o_id);
    $ordertext = $this->orderform->getHtml($field_values, $order_data['products'], $order_data['user'], $order_num, 'lv', $order, $sum_discount_data);

    $totalsum = $sum;

    $allfields = $order_data['allfields'];
    $buyer = ($allfields['payer'] == 'private') ? $allfields['name'] : $allfields['company_name'];
    $telefons = $allfields['phone'];
    $epasts = $allfields['email'];


    $item = Array(
      'rekins' => $ordertext,
      'summa' => $sum,
      'kopeja_summa' => $totalsum,
      'order_data' => serialize($order_data),
      'sent' => 0,
      'user_id' => (int)$_POST['user_id'],
      'datums' => date('Y-m-d H:i:s'),
      "pasutitajs" => $buyer,
      "epasts" => $epasts,
      'telefons' => $telefons
    );

    if ($order_id){
      unset($item['datums']);
      $this->ordercol->Update($item, array("item_id" => $order_id));
    }else{
      $new_status = $this->statuses->getNewStatusRow();
      $item['status_id'] = $new_status['item_id'];

      $order_id = $this->ordercol->Insert($item);

      // adding history entry
      $this->order_history->AddItemAssoc(array(
        "order_id" => $order_id,
        "status_to_id" => $new_status['item_id'],
        "comments" => lcms("Invoice created in CMS"),
        "date" => date('Y-m-d H:i:s'),
        "user" => $_SESSION['session_user']
      ));

      $_SESSION['ordereditor_data'] = array();

      foreach ($order_data['products'] as $key => $prod){
        $this->orderprodcol->addItemAssoc(array(
          'price' => $prod['price'],
          'count' => $prod['count'],
          'prod_id' => $prod['id'],
          'price_id' => $prod['price_id'] ? $prod['price_id'] : 0,
          'size_id' => $prod['size'] ? $prod['size'] : 0,
          'color_id' => $prod['color'] ? $prod['color'] : 0,
          'cart_key' => $prod['cart_key'] ? $prod['cart_key'] : 0,
          'order_id' => $order_id
        ));
      }

    }

    $_GET['order_id'] = '';
    $_SESSION['ordereditor_data'] = array();

  }

  function dialogEditOrderSaveOld(){

    $this->init();
    $order_id = intval($_GET['order_id']);
    $order_data = $_SESSION['ordereditor_data'];

    $err = $this->orderform->processCustomerForm();
    if ($err){
      echo lcms('Error').': '.$err.'<br/>';
      $this->dialogEditOrder();
      die;
    }

    if (!$order_id){
      $orderlang = $this->orderform->order_lang;
      if (!$orderlang) $orderlang = 'lv';

      $order_data = $_SESSION['ordereditor_data'];
      $order_data['allfields'] = $this->orderform->allfields;
      $order_data['orderlang'] = $orderlang;
    }

    foreach ($order_data['allfields'] as $key => $f){
      if (!isset($this->orderform->allfields[$key])) continue;

      $order_data['allfields'][$key]['value'] = $this->orderform->allfields[$key]['value'];
    }

    $orderprods = $this->orderprodcol->getProductsByOrder($order_id);
    $sum = 0;
    foreach ($order_data['products'] as $prod){

      $sum += $prod['price'] * $prod['count'];

      if ($order_id){

        foreach ($orderprods as $op){
          if ($op['prod_id'] == $prod['id']){

            $this->orderprodcol->changeItemByIdAssoc($op['item_id'], array(
              'price' => $prod['price'],
              'count' => $prod['count'],
              'price_id' => $prod['price_id'],
              'size_id' => $prod['size'],
              'color_id' => $prod['color'],
            ));

          }
        }

      }

    }
    $dif = round($order['kopeja_summa'] - $order['summa'], 2);
    $sum = round($sum, 2);

    $order_data['sum'] = $sum;

    $u = null;
    if ($_POST['user_id'])
      $u = $this->usercol->getItemByIdAssoc($_POST['user_id']);

    $order_data['user'] = $u;

    foreach ($order_data['products'] as $pk => $prod){
      $pr = $this->prodcol->getItemByIdAssoc($prod['id']);
      $price = $pr['price'];
      if($prod['price_row']){
        $price = $prod['price_row']['price'];
      }

      $order_data['products'][$pk]['undiscounted_price'] = $price;
    }

    $ordertext = $this->orderform->srvGetOrder($order_data['products'], $order_data['sum'], $order_data['allfields'], '', $order_data['user'], $order_id ? $order_id :  $this->ordercol->getNextId());

    $totalsum = $sum + $dif;
    $cart = serialize($order_data);

    $allfields = $order_data['allfields'];
    if($allfields['maksatajs']['value'] == 'fiziska'){
      $buyer = $allfields['f_vards']['value'];
      $telefons = $allfields['f_telefons']['value'];
    }else{
      $buyer = $allfields['j_nosaukums']['value'];
      $telefons = $allfields['j_telefons']['value'];
    }

    $item = Array(
      'rekins' => addslashes($ordertext),
      'summa' => $sum,
      'kopeja_summa' => $totalsum,
      'order_data' => addslashes($cart),
      'sent' => 0,
      'user_id' => (int)$_POST['user_id'],
      'datums' => date('Y-m-d H:i:s'),
      "pasutitajs" => $buyer,
      'telefons' => $telefons
    );

    if ($order_id){
      unset($item['datums']);
      $this->ordercol->changeItemByIdAssoc($order_id, $item);
    }else{
      $new_status = $this->statuses->getNewStatusRow();
      $item['status_id'] = $new_status['item_id'];

      $ind = $this->ordercol->addItemAssoc($item);
      $order_id = $this->ordercol->getItemID($ind);

      // adding history entry
      $this->order_history->AddItemAssoc(array(
        "order_id" => $order_id,
        "status_to_id" => $new_status['item_id'],
        "comments" => lcms("Invoice created in CMS"),
        "date" => date('Y-m-d H:i:s'),
        "user" => $_SESSION['session_user']
      ));

      $_SESSION['ordereditor_data'] = array();

      foreach ($order_data['products'] as $prod){
        $this->orderprodcol->addItemAssoc(array(
          'price' => $prod['price'],
          'count' => $prod['count'],
          'prod_id' => $prod['id'],
          'price_id' => $prod['price_id'],
          'size_id' => $prod['size'],
          'color_id' => $prod['color'],
          'order_id' => $order_id
        ));
      }

    }

    $_GET['order_id'] = '';
  }

  function dialogOrderEditProduct()
  {
    $this->initCollections();

    $order_id = intval($_GET['order_id']);
    if ($order_id)
    {
      list($order_data) = $this->getOrderData($order_id);
      if (!$order_data) return;
    }
    else
      $order_data = $_SESSION['ordereditor_data'];

    $this->lang = $this->getLanguageName();

    $prod = $order_data['products'][$_GET['prod_key']];

    $pr = $this->prodcol->getItemByIdAssoc($prod['id']);

    $shopman = $this->getBackendServiceByName('shopmanager');

    $prices = array();
    $pvalues = array();

    $prices[0] = '';

    list($price, $old_price, $undiscounted_price) = $shopman->getProductPrice($pr, $order_data['user']);

    $prices = $pvalues = $prices_arr = array();
    $prices_arr = array("0" => lcms("Nothing"));
    $pvalues = array("0" => $price);
	if($this->pricescol){
    $prices = $this->pricescol->getPricesByProduct($pr['item_id']);
    }
	foreach($prices as $price){
      $prices_arr[$price['item_id']] = $price['name'];
      $pvalues[$price['item_id']] = $price['price'];
    }

    $form_data = Array(
      'price_id' => Array(
        'type' => 'list',
        'label' => lcms('Package'),
        'value' => $prod['price_id'],
        'lookupassoc' => $prices_arr
      ),

      'price' => Array(
        'type' => 'str',
        'label' => lcms('Price'),
        'value' => $prod['price']
      ),

      'count' => Array(
        'type' => 'str',
        'label' => lcms('Count'),
        'value' => $prod['count']
      )
    );
    echo $this->getDialogFormOutput($form_data, 'dialogOrderEditProductSave', lcms('Edit product'), Array('order_id' => $_GET['order_id'], 'prod_key' => $_GET['prod_key'], 'afterrefreshmethod' => 'dialogEditOrder' ));

?>

  <script type="text/javascript">

  var prices = <?=json_encode($pvalues) ?>;

  $(function()
  {
    $('#price_id').change(function()
    {
      $('#price').val(prices[this.value]);
    });

  });

  </script>
<?
  }

  function dialogOrderEditProductSave()
  {
    $this->initCollections();

    $order_id = intval($_GET['order_id']);
    if ($order_id)
    {
      list($o) = $this->getOrderData($order_id);
      if (!$o) return;
    }
    else
      $o = &$_SESSION['ordereditor_data'];

    $g = $o['products'][$_GET['prod_key']];
    $item = $this->prodcol->getItemByIdAssoc($g['id']);

    $val = Array(
      'id' => $g['id'],
      'count' => strtr($_POST['count'], ',', '.'),
      'price_id' => $_POST['price_id'],
      'patversme' => $g['patversme'],
      'dzivnieks' => $g['dzivnieks']
    );

    $cart = $this->getBackendServiceByName('shopmanager', true);

    $u = $cart->user;
    $cart->user = $o['user'];

    $o['products'][$_GET['prod_key']] = $cart->srvGetCartItem($val, $item, $o['user'], $o['orderlang'], $_GET['prod_key']);
    $o['products'][$_GET['prod_key']]['price'] = $_POST['price'];

    $cart->user = $u;

    if ($order_id)
    {
			$changed = $this->ordercol->changeItemByIdAssoc($order_id, Array(
        'order_data' => AddSlashes(serialize($o))
      ));
			if($changed AND $this->use_automatic_prod_stock){
				$prod = $this->orderprodcol->getProductByID($order_id, $item['item_id'], $_POST['price_id']);
				$count = (int)$_POST['count'];
				//existing prod in order
				if(!empty($prod)){										
					if($prod['price_id'] == $_POST['price_id']){							
						if($prod['count'] > $count){
							$new_count = array($item['item_id'] => round($prod['count'] - $count));
							$this->orderprodcol->Update(array('count' => $count),$prod['item_id']);
							$this->prodcol->changeBackProdCount($new_count);						
						}
						elseif($prod['count'] != $count){
							$new_count = array($item['item_id'] => round($count - $prod['count']));
							$this->orderprodcol->Update(array('count' => $count),$prod['item_id']);
							$this->prodcol->updateProductsTimesPurchased($new_count,$this->use_automatic_prod_stock);
						}
					}										
				}
				else{
					$new_count = array($item['item_id'] => $count);
					$neworderprod = array('order_id' => $order_id, 'prod_id' => $item['item_id'], 'count' => $count);
					if($_POST['price_id'])
						$neworderprod['price_id'] = $_POST['price_id'];
					$this->orderprodcol->Insert($neworderprod);
					$this->prodcol->updateProductsTimesPurchased($new_count);
				}
			}
    }
  }

  function dialogOrderDelProduct()
  {
    $this->initCollections();

    $order_id = intval($_GET['order_id']);
    if ($order_id)
    {
      list($o) = $this->getOrderData($order_id);
      if (!$o) return;
    }
    else
      $o = &$_SESSION['ordereditor_data'];

    $prod = &$o['products'][$_GET['prod_key']];
    if ($prod['count'] <= 1)
      array_splice($o['products'], $_GET['prod_key'], 1);
    else
      $prod['count'] -= 1;

    if ($order_id)
    {
      $this->ordercol->changeItemByIdAssoc($order_id, Array(
        'order_data' => addslashes(serialize($o))
      ));
    }

//    $_GET['order_id'] = '';
  }

  function getDialogFormOutput2($form_data, $target_method, $title, $additional_params = Array(), $refresh = '1', $cancel = 'close', $okaction = 'close', $cmptype='', $cmpname='', $shortcut=false)
  {
    $addurl = '';
    foreach($additional_params as $key => $val)
    {
      $addurl .= '&' . urlencode($key) . '=' . urlencode($val);
    }
    if (!$cmptype) $cmptype = get_class($this);
    if (!$cmpname) $cmpname = $this->name;

    #dump($shortcut);
    require_once($GLOBALS['cfgDirRoot']."library/"."class.formgen.php");
    $FormGen = new FormGen();
    $FormGen->action = '?module=dialogs&action=component&site_id='.
    $this->site_id.'&page_id='.$this->page_id.'&component_name='.
    $cmpname.'&component_type='.
    $cmptype.'&method_name='.$target_method.'&refresh='.$refresh.
        '&okaction='.$okaction.$addurl;
    $FormGen->title = $title;
    $FormGen->properties = $form_data;
    $FormGen->cancel = $cancel;
    $FormGen->buttons = false;
    return $FormGen->output();
  }

  function dialogSelectProduct()
  {
    $this->initCollections();
    $params = array();
    if (strlen($_POST['searchcat']))
    {
      $params['where'] = 'title_lv LIKE "%'.$_POST['searchcat'].'%"';
    }

    $data = $this->catcol->getDBData($params);

    $lang = $this->GetLanguageName();
    $cats = Array();
    foreach ($data as $key => $row)
    {
      $cats[] = Array(
        'id' => $row['item_id'],
        'parent' => $row['parent'],
        'label' => $row['title_'.$lang]
      );
    }

    $form_data = Array(
      'prod_id' => Array(
        'type' => 'hidden',
        'value' => '0'
      )
    );

    $params = array();
    $paramkeys = explode(',', $_GET['pcatparamkeys']);
    foreach ($paramkeys as $key)
    {
      $params[$key] = $_GET[$key];
    }

    if ($_GET['pcatsubmitafterrefreshmethod'])
      $params['afterrefreshmethod'] = $_GET['pcatsubmitafterrefreshmethod'];

    $form = $this->getDialogFormOutput2($form_data, $_GET['pcatsubmitmethod'], '', $params, '1', 'close', 'close', $_GET['pcatcmptype'], $_GET['pcatcmpname'], $_GET['checkboxes']);

    $checkboxes = (isset($_GET['checkboxes']))? "&checkboxes=".($_GET['checkboxes']? 1 : 0) : "";

    $panel_url = "?module=dialogs&action=component&site_id=".$this->site_id."&page_id=".$this->page_id."&component_name=".$this->name."&component_type=".get_class($this)."&method_name=listProducts&refresh=0".$checkboxes;
?>
  <html>
  <head>
    <link rel='stylesheet' type='text/css' href='gui/dtree/dtree.css' />
    <script type='text/javascript' src='gui/dtree/dtree.js'></script>
    <style type="text/css">
      #wrap {
        padding: 8px;
      }

      #shopcattree {
        float: left;
        width: 45%;
      }
      #panel {
        float: left;
        min-height: 400px;
        border-left: 1px solid #cccccc;
        width: 50%;
        margin: 0;
      }

      form {
        margin: 0;
        padding: 0;
      }

      .search {
        margin-bottom: 10px;
      }

    </style>

</head>
  <body>
  <div id="wrap">
    <div class="search"><form action="" method="post">

      <input type="text" name="searchcat" value="<?=htmlspecialchars($_POST['searchcat']) ?>"/> <input type="submit" value="<?=lcms('Search')?>"/>

    </form></div>
    <div id="shopcattree">
<? foreach ($data as $row){
    echo str_repeat('&nbsp;&nbsp', $row['lvl']).'<a href="'.$panel_url.'&cat_id='.$row['item_id'].'&product_id='.$_GET['product_id'].'" target="panel">'.$row['title_'.$lang].'</a><br/>';
  } ?>
    </div>
    <iframe id="panel" name="panel" src="<?=$panel_url ?>" width="55%" height="100%" frameborder="0"></iframe>
    <div style="clear:both"></div>
  </div>
  <?=$form ?>

  <script type='text/javascript'>

  function selectProduct(id)
  {

    var f = document.getElementById('Form');
    f.prod_id.value = id;

    var form = $("#Form");
    $("#prod_id", form).val(id);

		// ajax submit here, cuz we need reload parent after product is added
		form.unbind("submit").submit(function(e){

    	$.ajax({
			  type: "POST",
			  url: form.attr("action"),
			  data: form.serialize(),
			  success: function(response) {

          if(typeof parent.opener_callback_function == 'function'){
				  	parent.opener_callback_function(response, window.parent);
				  }

			  }
			});

			e.preventDefault();
		}).submit();

  }

  function build_dtree(name, label, data)
  {
    this[name] = new dTree(name);
    this[name].icon.node = this[name].icon.folder;

    this[name].add(0,-1, label, '<?=$panel_url ?>&cat_id=0', '', 'panel');
    for (var i=0; i<data.length; i++)
    {
      var e = data[i];
      this[name].add(e.id, e.parent, e.label, '<?=$panel_url ?>&cat_id='+e.id, '', 'panel');
    }
    document.getElementById(name).innerHTML = this[name];
  }

//  var data = <?=json_encode($cats) ?>;
  var dtree;
//  build_dtree('shopcattree', 'Katalogs', data);

  </script>
  </body>
  </html>
<?

  }

  function dialogSelectProductSubmit()
  {
    $prod_id = intval($_POST['prod_id']);
    if (!$prod_id) return;

    $this->init();
    $this->shopman->initCollections();

    $order_id = intval($_GET['order_id']);
    if ($order_id)
    {
      list($o) = $this->getOrderData($order_id);
    }
    else
    {
      if (!isset($_SESSION['ordereditor_data']))
        $_SESSION['ordereditor_data'] = array();

      if (!isset($_SESSION['ordereditor_data']['products']))
        $_SESSION['ordereditor_data']['products'] = array();

      if (!isset($_SESSION['ordereditor_data']['products']['user']))
        $_SESSION['ordereditor_data']['user'] = $this->shopman->userscol->getItemByIdAssoc(intval($_GET['user_id']));

      $o = &$_SESSION['ordereditor_data'];
    }

    $val = array(
      'id' => $prod_id
    );

    foreach ($o['products'] as $key => $prod)
    {
      if ($prod['val'] == $val)
      {
        $o['products'][$key]['count'] += 1;
        $this->ordercol->changeItemByIdAssoc($order_id, Array(
          'order_data' => addslashes(serialize($o))
        ));
        return;
      }
    }

    $cart = $this->getBackendServiceByName('shopmanager');
    if (!$cart) die('backendService shopmanager not found!');

    $prod = $this->prodcol->getItemByIdAssoc($prod_id);
    $volumes = $this->pricescol->getPricesByProduct($prod['item_id']);

    $val = Array('id' => $prod_id, 'count' => 1);
		if($volumes){
    	$val['price_id'] = $volumes[0]['item_id'];
		}

    if (!$o['orderlang'])
    {
      $orderlang = $orderform->order_lang;
      if (!$orderlang) $orderlang = 'lv';

      $o['orderlang'] = $orderlang;
    }

    $u = $cart->user;
    $cart->user = $o['user'];

    $o['products'][] = $cart->srvGetCartItem($val, $prod, $o['user'], $o['orderlang']);

    $cart->user = $u;

    if ($order_id)
    {
      $this->ordercol->changeItemByIdAssoc($order_id, Array(
        'order_data' => addslashes(serialize($o))
      ));
    }

  }

  function listProducts()
  {
    $cat_id = intval($_GET['cat_id']);
    if (!$cat_id)
    {
      $cat_id = intval($_COOKIE['csshopcattree']);
      if (!$cat_id)
      {
        echo '('.lcms('product category not chosen').')';
        return;
      }
    }

    $this->initCollections();

    if($_POST['action'] == 'togglerelated'){

      $relation = $this->relatedprods->getRelation($_POST['prod_id'], $_POST['related']);
      if($relation){
        $this->relatedprods->DelDBItem($relation[0]['item_id']);
      }else{
        $this->relatedprods->addItemAssoc(array("prod_id" => $_POST['prod_id'], "related_id" => $_POST['related']));
      }

      die();

    }

    $cat = $this->catcol->getItemByIdAssoc($cat_id);
    if (!$cat) return;

    $lang = $this->GetLanguageName();
    $data = $this->prodcol->getCategoryProducts($cat_id);

    $cond = array("shortcut = 0");
    if (!isset($_POST['catrange']) || $_POST['catrange'] == 'this')
      $cond[] = 'category_id='.$cat_id;

    if (strlen($_POST['searchq']))
    {
      $searchcols = Array('name_'.$lang, 'code');
      $this->prodcol->condSearchProducts($cond, $_POST['searchq'], $searchcols);

    }

	 #	dump($_GET);

    $pg = 0;
    $maxcount = 0;

    list($data, $count) = $this->prodcol->queryProducts($cond, $pg, $maxcount, $orderby, '*', QUERYPROD_DATACOUNT);

//    $data = $this->prodcol->getCategoryProducts($cat_id);

    $catrange = isset($_POST['catrange']) ? $_POST['catrange'] : 'this';

    if(isset($_GET['product_id'])){

      $related_ids = $this->relatedprods->getRelatedIds((int)$_GET['product_id']);
      $related_ids = $related_ids ? $related_ids : array();
      $show_checkboxes = true;

    }else{
      $show_checkboxes = false;
    }

		if(isset($_GET['checkboxes']) && $_GET['checkboxes'] == 0) $show_checkboxes = false;

?>
  <h4><?=$cat['title_'.$lang] ?></h4>

  <div class="search"><form action="" method="post">
    <label><input type="radio" name="catrange" value="this"<?=($catrange == 'this') ? ' checked="checked"' : '' ?>/> <?=lcms('in this')?></label>
    <label><input type="radio" name="catrange" value="all"<?=($catrange == 'all') ? ' checked="checked"' : '' ?>/> <?=lcms('in all')?></label><br/>
    <input type="text" name="searchq" value="<?=htmlspecialchars($_POST['searchq']) ?>"/> <input type="submit" value="<?=lcms('Search')?>"/>

  </form></div>

  <table>
  <? foreach ($data as $row) { ?>
    <tr>
      <? if($show_checkboxes){ ?>
      <td><input class='togglerel' <?=(in_array($row['item_id'], $related_ids)) ? ' checked="checked"' : '' ?> type='checkbox' name='relted_prods[<?=$row['item_id']?>]' value='<?=$row['item_id']?>' /></td>
      <? } ?>
      <td><? if ($row['picture']) { ?><a href="<?=$row['picture'] ?>" target="_blank"><img src="<?=getThumbURL($row['picture'], 30, 23, 6) ?>" alt=""/></a><? } ?></td>
      <td>
        <? if($show_checkboxes && false){ ?>
          <?=htmlspecialchars($row['name_'.$lang])?>
        <? }else{ ?>
          <a href="javascript:parent.selectProduct(<?=$row['item_id'] ?>);void(0)"<?=$row['disabled'] ? ' style="text-decoration:line-through" ': '' ?>><?=htmlspecialchars($row['name_'.$lang]) ?></a>
        <? } ?>
      </td>
    </tr>
  <? } ?>
  </table>
  <? if($show_checkboxes){ ?>

    <script type='text/javascript'>

      $(".togglerel").click(function(){

        var val = $(this).val();

        $.ajax({
          type: "POST",
          data: { action: "togglerelated", prod_id: <?=(int)$_GET['product_id']?>, related: val },
          success: function(response) {
            console.log(response);
          }
        });

      });

    </script>


  <? } ?>
<?
  }

  function GetDesignIcons($item_id)
  {
    $link = $this->getComponentModifierLink('del', Array('index' => $item_id));
    return $this->getDesignIconOutput('del', $link, 1, 1, lcms('Do you really want to delete this invoice? This action will be permanent!'));
  }


  function SaveOrderInPDF($order_id)
  {
    return $this->getComponentModifierLink('downloadPDFOrder', Array('order_id' => $order_id));
  }

  function SaveOrderInMSword($order_id)
  {
    return $this->getComponentModifierLink('downloadOrder', Array('order_id' => $order_id));
  }

  //MODIFIER method
  function ajax_checkToggle(){
    if($this->InitCollections()){
      $order = $this->ordercol->GetItemByIdAssoc($_GET['order_id']);
      if($order){
         if($_GET['fieldname'] == 'apmaksats' && (int)(boolean)$_GET['setval'] == 1){

        	$cards = $this->GiftCards->cards->getByOrderId($_GET['order_id']);
          foreach($cards as $card){
            $this->GiftCards->ProcessPaid($card);
          }

          	$this->GiftCards->sendCardsToEmail((int)$_GET['order_id']);
			$paid_status = $this->statuses->getPaidStatusRow();

         	$this->ordercol->changeItemByIdAssoc($order['item_id'], array(
          	"apmaksas_datums" => date("Y-m-d H:i:s"),
          	"apmaksats" => 1,
			"status_id" => $paid_status['item_id'] ? $paid_status['item_id'] : $order['status_id']
         	));

			if($paid_status){
				$this->order_history->Insert(array(
					"order_id" => $order['item_id'],
					"status_from_id" => $order['status_id'],
					"status_to_id" => $paid_status['item_id'],
					"comments" => "Rēķina apmaksa saņemta",
					"user" => $_SESSION['session_user'],
					"date" => date('Y-m-d H:i:s')
				));
			}
        }
		else if($_GET['fieldname'] == 'apmaksats' && (int)(boolean)$_GET['setval'] == 0){
			$previous_history_entry = $this->order_history->GetRow(array('where'=>'order_id = '.$order['item_id'], 'order' => 'date DESC'));
         	$this->ordercol->changeItemByIdAssoc($order['item_id'], array(
          	"apmaksats" => 0,
			"status_id" => $previous_history_entry['status_from_id'] ? $previous_history_entry['status_from_id'] : $order['status_id']
         	));

			if($previous_history_entry['status_from_id']){
				$this->order_history->Insert(array(
					"order_id" => $order['item_id'],
					"status_from_id" => $order['status_id'],
					"status_to_id" => $previous_history_entry['status_from_id'],
					"comments" => "Rēķina apmaksa atcelta",
					"user" => $_SESSION['session_user'],
					"date" => date('Y-m-d H:i:s')
				));
			}

		}elseif($_GET['fieldname'] == 'piegadats' && (int)(boolean)$_GET['setval'] == 1){

          $this->ordercol->changeItemByIdAssoc($order['item_id'], array(
            "piegades_datums" => date("Y-m-d H:i:s"),
            "piegadats" => 1
           ));

        }else{          
          $this->ordercol->Update(array($_GET['fieldname']=>(int)(boolean)$_GET['setval']), $order['item_id']);					
        }

      }
    }

    die('1');
  }

  //MODIFIER method
  function del()
  {
    if($this->init())
    {
      $order = $this->ordercol->GetItemByIdAssoc($_GET['index']);
      if($order)
      {
        $this->ordercol->DelDBItem($order['item_id']);
        $this->orderprodcol->deleteProductsByOrder($order['item_id']);
      }
    }
  }

  function InitCollections(){

    if($this->CollectionsInited) return;

    $this->prodcol = $this->InitPropertyCollection('prod_collection');
    $this->usercol = $this->InitPropertyCollection('user_collection');
    $this->ordercol = $this->InitPropertyCollection('order_collection');
    $this->orderprodcol = $this->InitPropertyCollection('orderprod_collection');
    $this->transcol = $this->InitPropertyCollection('transcol');
    $this->catcol = $this->InitPropertyCollection('cat_collection');
    $this->pricescol = $this->InitPropertyCollection('pricescol');
    $this->statuses = $this->InitPropertyCollection('statuses');
    $this->order_history = $this->InitPropertyCollection('order_history');
    $this->relatedprods = $this->InitPropertyCollection('relatedprodscol');

    $this->product_sizes = $this->initPropertyCollection("product_sizes");
    $this->sizes = $this->initPropertyCollection("sizes");
    $this->product_colors = $this->initPropertyCollection("product_colors");
    $this->colors = $this->initPropertyCollection("colors");
    $this->swedbank_log = $this->initPropertyCollection("swedbank_log");

    $this->GiftCards = EVeikalsGiftCard::getInstance();
    if($this->GiftCards) $this->GiftCards->init();

    return ($this->prodcol && $this->usercol && $this->ordercol && $this->orderprodcol);

  }


  // FirstData

  function reverseDialog()
  {
    $form_data = Array();
    $id = intval($_GET['torder_id']);
    $sum = 0.0;
    $sum_label = '';
    if ($id)
    {
      $this->InitCollections();

      $trans = $this->transcol->getTransactionByOrderId($id, 'OK', '000');
      $tid = $trans['item_id'];
      if ($trans)
      {
        if (isset($_POST['summa']))
        {
          $merchant = $this->getMerchantObject();
          $resp = $merchant->reverse(urlencode($trans['trans_id']), urlencode($trans['amount']));
          if (substr($resp, 8, 2) == "OK")
          {
            $this->transcol->ChangeItemByIdAssoc($tid, Array(
              'reversed' => 1
            ));
          }
          else
          {
            echo '
              <script type="text/javascript">
                alert("'.lcms('Money return procedure failed!').'\n'.$resp.'");
              </script>
            ';
          }
          return;
        }

        $sum = round($trans['amount'] / 100.0, 2);
        $sum_label = number_format($sum, 2).' '.$trans['currency_code'];
        $form_data = Array(
          'summa_lbl' => Array(
            'label' => lcms('Sum:'),
            'type'  => 'label',
            'value' => $sum_label
          ),

          'host' => Array(
            'label' => lcms('Hostname:'),
            'type'  => 'label',
            'value' => $trans['client_host']
          ),

          'ip' => Array(
            'label' => lcms('IP adress:'),
            'type'  => 'label',
            'value' => $trans['client_ip']
          ),

          'trans_id' => Array(
            'label' => lcms('Transaction ID:'),
            'type'  => 'str',
            'value' => $trans['trans_id']
          ),

          'info' => Array(
            'label' => lcms('Info:'),
            'type'  => 'label',
            'value' => nl2br($trans['response'])
          ),

          'summa' => Array(
            'label' => lcms('Input money amount to return:'),
            'type'  => 'text'
          )

        );
      }
    }

    $jsvalidate = '
      if (Number(document.Form.summa.value) != '.$sum.')
      {
        alert("'.lcms('Amount entered incorrect!').'");
        return false;
      }

      if (!confirm("'.lcms('Do you really want to return').' '.$sum_label.' '.lcms('for invoice nr.').' '.$id.' ?"))
      {
        return false;
      }
    ';
    $this->displayForm($form_data, reverseDialog, 1, lcms('Return money'), '', '&torder_id='.$id, false, $jsvalidate);
  }

  function transactionDialog()
  {
    $form_data = Array();
    $id = intval($_GET['torder_id']);
    $sum = 0.0;
    $sum_label = '';
    if ($id)
    {
      $this->InitCollections();

      $trans = $this->transcol->getTransactionByOrderId($id, 'OK', '000');
      $tid = $trans['item_id'];
      if ($trans)
      {
        $sum = round($trans['amount'] / 100.0, 2);
        $form_data = Array(
          'summa_lbl' => Array(
            'label' => lcms('Sum:'),
            'type'  => 'label',
            'value' => number_format($sum, 2).' '.$trans['currency_code']
          ),

          'host' => Array(
            'label' => lcms('Hostname:'),
            'type'  => 'label',
            'value' => $trans['client_host']
          ),

          'ip' => Array(
            'label' => lcms('IP adress:'),
            'type'  => 'label',
            'value' => $trans['client_ip']
          ),

          'trans_id' => Array(
            'label' => lcms('Transaction ID:'),
            'type'  => 'label',
            'value' => $trans['trans_id']
          ),

          'info' => Array(
            'label' => lcms('Info:'),
            'type'  => 'label',
            'value' => nl2br($trans['response'])
          )

        );
      }
    }
    $this->displayForm($form_data, reverseDialog, 1, lcms('Transaction info'), '', '&torder_id='.$id);
  }

  function displayForm($form_data, $methodname, $index, $title, $afterrefreshmethod = '', $additionalgetdata = '', $showbuttons = false, $validatejs = '')
  {
    require_once($GLOBALS['cfgDirRoot']."library/"."class.formgen.php");
    $FormGen = new FormGen();
    $FormGen->action = '?module=dialogs&action=component&site_id='.
		$this->site_id.'&page_id='.$this->page_id.'&component_name='.
		$this->name.'&component_type='.
		get_class($this).'&method_name='.$methodname.'&afterrefreshmethod='.$afterrefreshmethod.'&refresh=1&index=' . $index . '&p_pagedev_id='.$this->pagedev_id . $additionalgetdata;
    $FormGen->title = $title;
    $FormGen->properties = $form_data;
    $FormGen->buttons = $showbuttons;
    $FormGen->validatejs = $validatejs;
    echo $FormGen->output();
  }


  function getDomain()
  {
    return $this->properties['domain']['urls'][$this->getProperty('domain')];
  }

  function getMerchantObject()
  {
    require_once($GLOBALS['cfgDirRoot'].'/library/func.files.php');
    require_once($GLOBALS['cfgDirRoot'].'/library/firstdata/Merchant.php');

    $domain = $this->getDomain();
    $ecomm_url = $domain.':8443/ecomm/MerchantHandler';

    $cert_path = getFilePathFromLink($this->site_id, $this->getProperty('cert'));
    if (!file_exists($cert_path))
    {
      echo 'PEM Certificate not found: '.$cert_path;
      return null;
    }

    return new Merchant($ecomm_url, $cert_path, $this->getProperty('cert_pass'), false);
  }

  //=========================================================================//
  // Default properties
  //=========================================================================//

  function CanBeCompiled()
  {
    return True;
  }

  function toolbar(&$script, &$toolbars, &$btnids, &$onclick)
  {
    return true;
  }

  function SetProperties()
  {
    $certs = Array();
    foreach (glob($GLOBALS['cfgDirRoot'].'certs/*.pem') as $f)
    {
      $certs[$f] = pathinfo($f, PATHINFO_BASENAME);
    }

    $this->properties = Array(

      "name"        => Array(
        "type"      => "string",
        "label"     => "Name:"
      ),

      "cat_collection" => Array(
        "label"       => "Category collection:",
        "type"        => "collection",
        "collectiontype" => "shopcatcollection",
        "lookup"      => GetCollections($this->site_id, "shopcatcollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "prod_collection" => Array(
        "label"       => "Product collection:",
        "type"        => "collection",
        "collectiontype" => "shopprodcollection",
        "lookup"      => GetCollections($this->site_id, "shopprodcollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "user_collection" => Array(
        "label"       => "User collection:",
        "type"        => "collection",
        "collectiontype" => "shopusercollection",
        "lookup"      => GetCollections($this->site_id, "shopusercollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "order_collection" => Array(
        "label"       => "Order collection:",
        "type"        => "collection",
        "collectiontype" => "EVeikalsOrderCollection",
        "lookup"      => GetCollections($this->site_id, "EVeikalsOrderCollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "orderprod_collection" => Array(
        "label"       => "Order prod collection:",
        "type"        => "collection",
        "collectiontype" => "shoporderprodcollection",
        "lookup"      => GetCollections($this->site_id, "shoporderprodcollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "transcol" => Array(
        "label"       => "First Data transactions collection:",
        "type"        => "collection",
        "collectiontype" => "firstdatatransactionscollection",
        "lookup"      => GetCollections($this->site_id, "firstdatatransactionscollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "pricescol" => Array(
        "label"       => "Prices collection:",
        "type"        => "collection",
        "collectiontype" => "shoppricescollection",
        "lookup"      => GetCollections($this->site_id, "shoppricescollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "statuses" => Array(
        "label"       => "Statuses collection:",
        "type"        => "collection",
        "collectiontype" => "EVeikalsOrderStatuses",
        "lookup"      => GetCollections($this->site_id, "EVeikalsOrderStatuses"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "order_history" => Array(
        "label"       => "Order history collection:",
        "type"        => "collection",
        "collectiontype" => "EVeikalsOrderHistory",
        "lookup"      => GetCollections($this->site_id, "EVeikalsOrderHistory"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "relatedprodscol" => Array(
        "label"       => "Related products collection:",
        "type"        => "collection",
        "collectiontype" => "shoprelatedprodscollection",
        "lookup"      => GetCollections($this->site_id, "shoprelatedprodscollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      'domain'    => Array(
        'label'     => 'FirstData serveris:',
        'type'      => 'list',
        'lookup'    => Array('0: https://secureshop-test.firstdata.lv', '1: https://secureshop.firstdata.lv'),
        'value'     => '0',
        'urls'   => Array('https://secureshop-test.firstdata.lv', 'https://secureshop.firstdata.lv'),
      ),

      "colors" => Array(
        "label"       => "Color collection:",
        "type"        => "collection",
        "collectiontype" => "EVeikalsColorCollection",
        "lookup"      => GetCollections($this->site_id, "EVeikalsColorCollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "product_colors" => Array(
        "label"       => "Product color collection:",
        "type"        => "collection",
        "collectiontype" => "EVeikalsProductColorCollection",
        "lookup"      => GetCollections($this->site_id, "EVeikalsProductColorCollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "sizes" => Array(
        "label"       => "Size collection:",
        "type"        => "collection",
        "collectiontype" => "EVeikalsSizeCollection",
        "lookup"      => GetCollections($this->site_id, "EVeikalsSizeCollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "product_sizes" => Array(
        "label"       => "Product size collection:",
        "type"        => "collection",
        "collectiontype" => "EVeikalsProductSizeCollection",
        "lookup"      => GetCollections($this->site_id, "EVeikalsProductSizeCollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

    "swedbank_log" => Array(
        "label" => "Swedbank Log collection:",
        "type" => "collection",
        "collectiontype" => "EVeikalsSwedbankLog",
        "lookup"      => GetCollections($this->site_id, "EVeikalsSwedbankLog"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
    ),

      'cert'    => Array(
        'label'     => 'FirstData PEM Sertifikāta fails:',
        'type'      => 'list',
        'lookupassoc'  => $certs
      ),

      'cert_pass'    => Array(
        'label'     => 'FirstData Sertifikāta parole:',
        'type'      => 'str'
      ),

      "servicemode"    => Array(
        "label"     => "Service mode:",
        "type"      => "check",
      ),

    );
  }

}
