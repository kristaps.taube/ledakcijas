<?php
/**
*
*  Title: Shop maintenance
*  Author: Kristaps Taube <ktaube@datateks.lv>
*  Date: 20.02.2014
*  Project: EVeikals
*  Description: This component does some maintenance tasks.
*
*/

include_once("class.component.php");

class EVeikalsMaintenance extends component{

  function __construct($name){
    parent::__construct($name);
    $this->SetProperties();
  }

  function execute(){

    $this->initCollections();
    $this->getServices();

    if($_SERVER["REMOTE_ADDR"] == '213.21.219.21' && false){

      $this->FilterGroups->truncate();
      $this->Filters->truncate();
      $this->cats->truncate();
      $this->prods->truncate();
      $this->CatFilterGroupRel->truncate();
      $this->ProdFilterRel->truncate();

      $this->generateFilterGroups(10);

      $this->generateCategories();
      echo "Doing...";
      ?>
        <script>
          document.location.reload();
        </script>
      <?
      die();
    }

    if($_SERVER["REMOTE_ADDR"] == '213.21.219.21' && false){

      $cats = $this->cats->getDBData();
      foreach($cats as $cat){
        $this->fixCatProdInds($cat['item_id']);
      }
      echo "done";
      return;

    }

    if($_SERVER["REMOTE_ADDR"] == '213.21.219.21' && false){
    	set_time_limit(0);
			$this->ClearProducts();
      $this->generateProducts(false, 100);
			die("done");
    }

  }

  function design(){

		// only me!
  	if($_SERVER["REMOTE_ADDR"] != '213.21.219.21') return;

    $this->getServices();

    if(isset($_POST['ClearProducts'])){
    	$this->ClearProducts();
    }elseif(isset($_POST['GenerateProducts'])){
    	$count = (int)$_POST['count'];
			$count = $count > 0 ? $count : 0;
    	$this->generateProducts(false, $count);
    }

    ?>
    	<form method='post' action=''>
      	<input type='submit' name='ClearProducts' value='Clear Products' />
			</form><br />
      <form method='post' action=''>
      	<input type='text' name='count' value='1000' /><input type='submit' name='GenerateProducts' value='Generate Products' /> <br />
			</form>

		<?

  }

  function ClearProducts(){

    $this->prods->truncate();
		$this->ProdFilterRel->truncate();
		$this->ratings->truncate();
		$this->prod_images->truncate();
		$this->related_prods->truncate();
		$this->product_sizes->truncate();
		$this->product_colors->truncate();
		$this->prices->truncate();

  	echo "Products cleared";

  }

  function getServices(){

    $this->ShopFilter = $this->getServiceByName("ShopFilter");
    $this->ShopFilter = $this->ShopFilter ? $this->ShopFilter : $this->getBackendServiceByName("ShopFilter");
    $this->ShopFilter->init();
  }

  function generateCategories($count = 10){

    for($i = 0; $i != $count; $i++){

      $cat = $this->cats->getRandomLowestLevelCat();

      $cat = array();
      $cat['parent'] = $cat['parent']? $cat['parent'] : 0;

      $title = "Test category ".round(rand(1, 10000));
      $langs = getLanguages();
      foreach($langs as $lang){
        $cat['title_'.$lang['shortname']] = $title;
      }

      $ind = $this->cats->addCategory($cat);
      $id = $this->cats->GetItemID($ind);

      $filter_groups = $this->FilterGroups->getDBData(array("order" => "RAND()", "count" => 5));

      foreach($filter_groups as $group){
        $this->CatFilterGroupRel->addItemAssoc(array("category_id" => $id, "filter_group_id" => $group['item_id']));
      }

      $this->generateProducts($id);

    }

  }

  function generateFilterGroups($count = 10){

    for($i = 0; $i != $count; $i++){

      $ind = $this->FilterGroups->addItemAssoc(array("title_lv" => "Filter group ".round(rand(1,10000))));
      $id = $this->FilterGroups->GetItemID($ind);
      $this->generateFilters($id, 5);

    }

  }

  function generateFilters($group = false, $count = 50){

    for($i = 0; $i != $count; $i++){

      $group = $group ? $group : $this->FilterGroups->getDBData(array("order" => "RAND()", "count" => 1));
      $group_id = is_array($group)? $group[0]['item_id'] : $group;
      $this->Filters->addItemAssoc(array("group" => $group_id, "param_title_lv" => "Filter ".round(rand(1,10000))));

    }


  }

  function generateProducts($def_category = false, $count = 10){

    $f = 1;

    for($i = 0; $i != $count; $i++){

      $category = $def_category ? $def_category : $this->cats->getRandomLowestLevelCat();
      $cat_id = is_array($category)? $category['item_id'] : $category;

      $name = "Test product ".round(rand(1, 10000));

      $prod = array();
      $prod['name_lv'] = $name;
      $prod['name_en'] = $name." en";
      $prod['name_ru'] = $name." ru";
      $prod['price'] = round(rand(0, 500));
      $prod['category_id'] = $cat_id;
      $prod['featured'] = (rand(1, 100) < $f)? 1 : 0;
      $prod['is_new'] = (rand(1, 100) < $f && !$prod['featured'])? 1 : 0;
      $prod['is_top'] = (rand(1, 100) < $f && !$prod['featured'] && !$prod['is_new'])? 1 : 0;

			$image_folder = $this->prods->getImageFolder($prod, $this->cats);
      $web_path_folder = "upload/products/".$image_folder;
      $web_path_full = $web_path_folder."lorem.png";

			$from = DIR_ROOT."images/html/lorem.png";
			$to = DIR_ROOT.$web_path_full;
      forceDirectories($this->site_id, $web_path_folder); // making directory tree

			$c = copy($from, $to);

      $prod['picture'] = '/'.$web_path_full;
      $prod['has_prices'] = (rand(1, 10) > 7);
      $prod['has_colors'] = (rand(1, 10) > 7);
      $prod['has_sizes'] = (rand(1, 10) > 7);

      $id = $this->prods->add($prod);

      // sub prices ?
			if($prod['has_prices']){ // yes we do!

      	$volume_count = rand(2,5);

      	for($j = 0; $j != $volume_count; $j++){

        	$name = "Volume ".rand(1,100);

        	$volume = array(
          	"product_id" => $id,
						"name_lv" => $name,
						"name_en" => $name,
						"name_ru" => $name,
						"price" => rand(1, 100),
						"saleout_price" => 0,
						"code" => rand(1, 10000)
					);
					$volume = $this->prices->processPropertyDataTypes($volume);
        	$this->prices->Insert($volume);

      	}

			}

			// colors
			if($prod['has_colors']){

      	$color_group = $this->cats->getColorGroupId($prod['category_id']);
        if($color_group){

        	$possible_colors = $this->colors->getIdsByGroup($color_group);
					foreach($possible_colors as $ind => $color_id){

          	if(!$ind || rand(0,1)){ // first or 50% chance

            	$this->product_colors->Insert(array("prod_id" => $id, "color_id" => $color_id));

          	}

          }

        }

			}

			// sizes
			if($prod['has_sizes']){

      	$group = $this->cats->getSizeGroupId($prod['category_id']);
        if($group){

        	$possible_sizes = $this->sizes->getIdsByGroup($group);

					foreach($possible_sizes as $ind => $size_id){

          	if(!$ind || rand(0,1)){ // first or 50% chance

            	$this->product_sizes->Insert(array("prod_id" => $id, "size_id" => $size_id));

          	}

          }

        }

			}

      $this->prod_images->Insert(array(
        'product_id' => $id,
        'image' => $prod['picture']
      ));

      $filter_groups = $this->ShopFilter->getFilterGroupsForCat($cat_id);
      foreach($filter_groups as $group){

        $filters = $this->Filters->getByGroup($group['item_id']);
        foreach($filters as $ind => $filter){

          if(!$ind || rand(1, 10) > 7){ // make relation and atleast one
            $this->ProdFilterRel->addItemAssoc(array("product_id" => $id, "filter_id" => $filter['item_id']));
          }

        }

      }

    }

  }

  function fixCatProdInds($cat){

    $prods = $this->prods->getDBData(array("where" => "category_id=".$cat));
    $ind = 1;
    foreach($prods as $prod){
      $this->prods->changeItemByIDAssoc($prod['item_id'], array('cat_ind' => $ind++));
    }

    $cats = $this->cats->getDBData(array("where" => "parent = ".$cat));
    foreach($cats as $cat){
      $this->fixCatProdInds($cat['item_id']);
    }

  }

  function output(){

    if(!$this->initCollections()){
      echo $this->name.": Can't init collections.<br />";
      return;
    }

  }

  function SetProperties(){
    $this->properties = Array(

      "name"        => Array(
        "label"     => "Name:",
        "type"      => "str"
      ),

      "cats" => Array(
        "label"       => "Category collection:",
        "type"        => "collection",
        "collectiontype" => "shopcatcollection",
      ),

      "prods" => Array(
        "label"       => "Product collection:",
        "type"        => "collection",
        "collectiontype" => "shopprodcollection",
      ),

      "FilterGroups" => Array(
        "label"       => "Filter group collection:",
        "type"        => "collection",
        "collectiontype" => "EVeikalsFilterGroups",
      ),

      "Filters" => Array(
        "label"       => "Filters collection:",
        "type"        => "collection",
        "collectiontype" => "EVeikalsFilters",
      ),

      "ProdFilterRel" => Array(
        "label"       => "Prod. filter rel. collection:",
        "type"        => "collection",
        "collectiontype" => "EVeikalsProdFilterRel",
      ),

      "CatFilterGroupRel" => Array(
        "label"       => "Cat. filter group rel. collection:",
        "type"        => "collection",
        "collectiontype" => "EVeikalsCatFilterGroupRel",
      ),

			"prices" => Array(
        "label"       => "Prices collection:",
        "type"        => "collection",
        "collectiontype" => "shoppricescollection",
      ),

      "ratings" => Array(
        "label"       => "Rating collection:",
        "type"        => "collection",
        "collectiontype" => "EVeikalsProductRatingCollection",
      ),

			"prod_images" => Array(
        "label"       => "Product images collection:",
        "type"        => "collection",
        "collectiontype" => "shopcustomimagescollection",
      ),

      "related_prods" => Array(
        "label"       => "Related products collection:",
        "type"        => "collection",
        "collectiontype" => "shoprelatedprodscollection",
      ),

			"product_sizes" => Array(
        "label"       => "Product size collection:",
        "type"        => "collection",
        "collectiontype" => "EVeikalsProductSizeCollection",
      ),

			"product_colors" => Array(
        "label"       => "Product color collection:",
        "type"        => "collection",
        "collectiontype" => "EVeikalsProductColorCollection",
      ),

			"colors" => Array(
        "label"       => "Color collection:",
        "type"        => "collection",
        "collectiontype" => "EVeikalsColorCollection",
      ),

			"sizes" => Array(
        "label"       => "Size collection:",
        "type"        => "collection",
        "collectiontype" => "EVeikalsSizeCollection",
        "lookup"      => GetCollections($this->site_id, "EVeikalsSizeCollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

    );

		$this->PostSetProperties();

  }

}
