<?php

use Constructor\App;
use Constructor\WebApp;

class Component
{
    var $name;
    var $properties;
    var $page_id;
    var $site_id;
    var $copypage;
    var $propertyvalues;
    var $defpropertyvalues;
    var $copypropertyvalues;
    var $copypageparents;
    var $dev;
    var $pagedev_id;
    var $template_id;
    var $LS = array();
    var $activelanguage;

    private static $_instances = [];
    private $collections = [];

    public function __construct($name = false)
    {

        $this->type = get_called_class();
        $name = $name ? $name : $this->type;

        $this->name = $name;
        $this->page_id = false;
        $this->site_id = App::$app->getSiteId();
        $this->lang = App::$app->getLanguage();

        $this->setProperties();

        $this->LS[0] = $this->getLanguageStrings();

        self::$_instances[$this->type][$name] = $this;

    }

    //Class initialization
    function Component_old($name)
    {

        $this->name = $name;
        $this->properties = Array();
        $this->site_id = $GLOBALS['site_id'];
        $this->copypage = $GLOBALS['copypage'];

        $this->activelanguage = $GLOBALS['language_id'] ? $GLOBALS['language_id'] : getLangIdByPageId($this->page_id);
        $this->template_id = $GLOBALS['template_id'];
        $this->setTemplate();
        $this->view_id = $this->getViewId(); 
        $this->lang = $GLOBALS['language'];
        $this->LS[0] = $this->getLanguageStrings();
        $this->setLanguage();


    }

    function __get($name)
    {

        if(isset($this->properties[$name])){ // its a property

            if($this->properties[$name]['type'] == 'collection'){ // its a collection property

                if(isset($this->collections[$name]) && $this->collections[$name]){
                    return $this->collections[$name];
                }else{
                    $this->collections[$name] = $this->initPropertyCollection($name);
                    if(!$this->collections[$name]){
                        var_dump($this->collections[$name]);
                        echo get_class($this).": Can't init collection '".$name."(".$this->properties[$name]['collectiontype'].")'";
                        die();
                    }
                    return $this->collections[$name];
                }

            }else{
                return $this->getProperty($name);
            }

        }elseif($GLOBALS['page_components'][$name]['obj']){ // maybe it's an service?

            return $GLOBALS['page_components'][$name]['obj'];

        }elseif(isset(self::$_instances[$this->type][$name])){

            return self::$_instances[$this->type][$name];

        }

        // make property
        $this->$name = null;
        return $this->$name;

    }

    public static function run($params = [], $name = false)
    {

        $instance = self::getInstance($name);
        $instance->execute();
        $output_params = makeMethodParameters(get_class($instance), 'output', $params);
        $response = call_user_func_array(array($instance, 'output'), $output_params);
        return $response;

    }

    public static function getInstances()
    {
        return self::$_instances;
    }

    public static function getInstanceNames()
    {

        $data = [];
        foreach(self::getInstances() as $type => $instances){
            $data[$type] = [];
            foreach($instances as $name => $instance){
                $data[$type][] = $name;
            }
        }

        return $data;

    }

    // this method turned out more complicated than planned.. sry
    public static function getInstance($name = false)
    {

        $received_name = $name ? true : false;

        $type = get_called_class();
        $name = $name ? $name : $type;

        if(isset(self::$_instances[$type][$name])){
            return self::$_instances[$type][$name];
        }else{

            if(isset(self::$_instances[$type])){

                if(count(self::$_instances[$type]) == 1){
                    return array_values(self::$_instances[$type])[0];
                }elseif($received_name){ // more than 1 and received name - create new with name
                    return new $type($received_name);
                }else{ // more than 1 and not received name -> error
                    echo "Trying to get: ".$oname."(".$name.")\n";
                    echo "Multiple instances of type `".$type."`\n";
                    echo "<pre>".print_r(array_keys(self::$_instances[$type], true))."</pre>";
                    die();
                }

            }else{
                return new $type($name);
            }

        }

    }

    public function render($view, $data = array())
    {

        $view_path = $this->getViewPath($view);

        if(file_exists($view_path)){

            ob_start();
            extract($data);
            require($view_path);
            return ob_get_clean();

        }else{

            die("View not found: ".$view_path);

        }


    }

    private function getViewPath($path)
    {

        if(substr($path,0,1) == '/'){

            $result = substr($path,1);

        }else{

            $result = get_class($this)."/".$path;

        }

        return App::$app->getConfigValue('view_folder').$result.'.php';

    }

    function get_cache_time(){
        return $GLOBALS['default_cache_time']; // 10min
    }

    function get_cache_file_name($cache_name=false,$custom_folder=false){

        if($custom_folder AND $cache_name){
            return $custom_folder.$this->name.'_'.$cache_name.'.inc';
        }elseif($custom_folder){
            return $custom_folder.$this->name.'.inc';
        }elseif($cache_name){
            return $this->cache_file.'_'.$cache_name.'.inc';
        }else{
            return $this->cache_file.'.inc';
        }

    }

  function deleteAllCache($custom_folder = false){

    $dir = $GLOBALS['cache_folder'];
    if($custom_folder){
      $dir .= $custom_folder;
    }

    if(is_dir($dir)){
        $files = scandir($dir);

        foreach($files as $file){

          if(strpos($file, $this->name."_") !== false){
            unlink($dir.$file);
          }

        }
    }

  }

  function deleteCache($cache_name=false,$custom_folder=false){

    $cache_file = $this->get_cache_file_name($cache_name, $custom_folder);
    unlink($cache_file);

  }

  function start_cache($cache_name=false,$custom_folder=false){
    
	  $this->cache_array[] = $cache_name;
	  $this->custom_folder = $GLOBALS['cache_folder'].$custom_folder;
	  if($custom_folder && !is_dir($custom_folder)) mkdir($custom_folder, 0755);

	  $cache_time = $this->get_cache_time();//in seconds
    $cache_file = $this->get_cache_file_name($cache_name, $custom_folder);
	  if ( file_exists (  $cache_file ) && (( filemtime (  $cache_file ) + $cache_time ) > time() )){
			include($cache_file);
			return true;
    }else{
    	//create cache :
      $this->caching = true;
			ob_start();
			return false;
    }

  }

  function end_cache(){

  	if ( $this->caching ){
	  	$data = ob_get_clean();
	  	echo $data;
			if($this->cache_array){
		  	$fp = fopen( $this->get_cache_file_name(array_pop($this->cache_array),$this->custom_folder)  , 'w' );
		  	fwrite ( $fp , $data );
	      fclose ( $fp );
			}
		}

  }

  function delete_dir($src) {
    $dir = opendir($src);
    while(false !== ( $file = readdir($dir)) ) {
        if (( $file != '.' ) && ( $file != '..' )) {
            if ( is_dir($src . '/' . $file) ) {
                delete_dir($src . '/' . $file);
            }
            else {
                unlink($src . '/' . $file);
            }
        }
    }
    rmdir($src);
    closedir($dir);
  }

  function getDynamicComponentsIDs()
  {
    return null;
  }

  function outputDynamicComponent($id, $isdesign=false)
  {
    $content = '';
    $obj = createDynamicComponentInstance($id, $this->page_id, false);
    if ($obj)
    {
      $GLOBALS['profiler_component'] = $obj->name;
      profiler_add('Init component ' . get_class($type) . ' ' . $obj->name, 3);

      ob_start();

      if ($obj->visible())
        $obj->output($isdesign);

      $content = ob_get_contents();
      ob_end_clean();

      profiler_add('Init component ' . get_class($type) . ' ' . $obj->name, 4);
      $GLOBALS['profiler_component'] = '';
    }


    return $content;
  }


  function updateDynamicComponents($page_id)
  {
    $new_ids = $this->getDynamicComponentsIDs();
    $new_comps = $new_ids ? sqlQueryDataAssoc('SELECT * FROM '.$this->site_id.'_dynamic_components WHERE id IN ('.implode(',', $new_ids).')') : null;

    $old_comps = array();
    $data = sqlQueryDataAssoc('SELECT * FROM '.$this->site_id.'_dynamic_components WHERE page_id='.$page_id.' AND is_deleted = 0 AND holder="'.$this->name.'"');
    foreach ($data as $row)
    {
      $old_comps[$row['id']] = $row;
    }

    foreach ($old_comps as $id => $row)
    {
      if (!in_array($id, $new_ids))
      {
        sqlQuery('UPDATE '.$this->site_id.'_dynamic_components SET is_deleted=1, delete_time=NOW() WHERE id='.(int)$id);
      }
    }

    foreach ($new_comps as $row)
    {
      if ($row['is_deleted'])
      {
        sqlQuery('UPDATE '.$this->site_id.'_dynamic_components SET is_deleted=0, delete_time=NULL WHERE id='.(int)$row['id']);
      }
    }

    $t = sqlQueryValue('SELECT DATE_SUB(NOW(), INTERVAL 30.5 DAY)');
    $data = sqlQueryDataAssoc('SELECT id FROM '.$this->site_id.'_dynamic_components WHERE delete_time < "'.$t.'"');
    foreach ($data as $row)
    {
      $obj = createDynamicComponentInstance($row['id'], $this->page_id, false);
      if ($obj)
        $obj->onDelete();

    }

    sqlQuery('DELETE FROM '.$this->site_id.'_dynamic_components WHERE delete_time < "'.$t.'"');
  }

  function initForPage($page_id, $devmode=false)
  {
    static $pages = Array();

    $site_id = $this->site_id;
    $ckey = $site_id.'_'.$page_id;

    if (isset($pages[$ckey]))
      $pageData = $pages[$ckey];
    else
    {
      $pageData = sqlQueryRow("SELECT * FROM " . $site_id . "_pages LEFT JOIN " . $site_id . "_languages ON " . $site_id . "_languages.language_id=" . $site_id . "_pages.language WHERE " . $site_id . "_pages.page_id='".$page_id."'");
      $pageData['pagedev_id'] = intval(sqlQueryValue("SELECT pagedev_id FROM ".$site_id."_pagesdev WHERE page_id=".$page_id));
      $pages[$ckey] = $pageData;
    }

    $this->page_id = $page_id;
    $this->pagedev_id = $devmode ? $pageData['pagedev_id'] : 0;
    $this->dev = $devmode;
    $this->copypage = $pageData['copypage'];
    $this->template_id = $pageData['template_id'];
    $this->setTemplate();
    $this->activelanguage = $pageData['language'];
    $this->lang = $pageData['shortname'];
    $this->setLanguage();
  }

  function _getBackendServiceByName($name){
    static $checked = Array();
		static $cmp_cache = Array();

    $row = sqlQueryRow("SELECT * FROM `".$this->site_id."_services` WHERE page_id=".$this->page_id." AND name='".$name."'");
    if (!$row) return null;

    if ($GLOBALS['developmode']){

      $ckey = $this->site_id.'_'.$this->page_id.'_'.$name;
      if (!isset($checked[$ckey])){
        $checked[$ckey] = true;

        if($cmp_cache[$this->site_id][$this->page_id]){
        	$cmps = $cmp_cache[$this->site_id][$this->page_id];
        }else{
        	$cmp_cache[$this->site_id][$this->page_id] = listPageComponents($this->site_id, $this->page_id);
          $cmps = $cmp_cache[$this->site_id][$this->page_id];
        }

        $found = false;
        foreach ($cmps as $cmp){
          if ($row['componentname'] == $cmp['name'] && $row['componenttype'] == $cmp['type']){
            $found = true;
            break;
          }
        }

        if (!$found){
          sqlQuery("DELETE FROM `".$this->site_id."_services` WHERE id=".$row['id']);
          return null;
        }
      }

    }

    $type = $row['componenttype'];

    if (!class_exists($type))
    {
      $filename = $GLOBALS['cfgDirRoot'] . "components/class.".$type.".php";
      if (!file_exists($filename)) return null;

      include_once($filename);
      if (!class_exists($type)) return null;
    }

    $obj = new $type($row['componentname']);
    $obj->initForPage($row['page_id'], true);

    return $obj;
  }

  function LoadBackendServices(){

  	if($GLOBALS['BackendServices']) return;

    foreach(sqlQueryData("SELECT * FROM `".$this->site_id."_services`") as $row){
     	$GLOBALS['BackendServices'][$row['page_id']][$row['name']] = $row;
    }

  }

	function isBackendServiceAdded($page_id, $name){

  	$this->LoadBackendServices();

  	return ($GLOBALS['BackendServices'][$page_id][$name]) ? true : false;

	}


  function registerAsBackendService($name)
  {
    static $reged = Array();
    if (!$this->site_id || !$this->page_id) return;

    $ckey = $this->site_id.'_'.$this->page_id.'_'.$name;
    if (isset($reged[$ckey])) return;

    $reged[$ckey] = true;
    if ($this->isBackendServiceAdded($this->page_id, $name)) return;

    sqlQuery("INSERT INTO `".$this->site_id."_services` SET name='".$name."', componentname='".$this->name."', componenttype='".get_class($this)."', page_id=".$this->page_id);

  }

  static function processOptionBeforeDisplay($path, $opt, &$data)
  {
  }

  static function processOptionBeforeSave($path, $opt, $value)
  {
    return $value;
  }

  function setComponentFlag($flagname, $value)
  {
    $GLOBALS['component_flags'][$flagname][$this->name] = $value;
  }

  function getComponentFlag($flagname)
  {
    $this_component_flag = $GLOBALS['component_flags'][$flagname][$this->name];
    $group_component_flag = false;
    if($GLOBALS['component_flags'][$flagname])
    {
      foreach($GLOBALS['component_flags'][$flagname] as $flag)
        $group_component_flag = ($group_component_flag || $flag);
    }
    return Array('this' => $this_component_flag, 'group' => $group_component_flag);
  }

  function includeScript($path)
  {
    if ($GLOBALS['scripts_manager'])
      return $GLOBALS['scripts_manager']->includeScript($path);
  }

  function addScript($script)
  {
    if ($GLOBALS['scripts_manager'])
      return $GLOBALS['scripts_manager']->addScript($script);
  }

  function includeCSS($path)
  {
    if ($GLOBALS['scripts_manager'])
      return $GLOBALS['scripts_manager']->includeCSS($path);
  }

  function addCSS($script)
  {
    if ($GLOBALS['scripts_manager'])
      return $GLOBALS['scripts_manager']->addCSS($script);
  }

  function includeLibrary($name)
  {
    if ($GLOBALS['scripts_manager'])
      return $GLOBALS['scripts_manager']->includeLibrary($name);
  }

  function onAjaxQueryChange($querykeys, $onchange_js)
  {
    $query = Array();
    foreach ($querykeys as $key)
    {
      $query[$key] = isset($_GET[$key]) ? $_GET[$key] : '';
    }
    $this->addScript('
  cmp_queries[\''.$this->name.'\'] = '.json_encode($query).'
  onAjaxQueryChange(\''.$this->name.'\', function(query, old_query){'.$onchange_js.'})    
    ');        
  }
  
  function ajaxClick($method, $id='', $query=null, $data=null, $callback='')
  {
    if ($id == '') $id = $this->name;
    if (!$query) $query = Array();
    if (!$data) $data = Array();
    return 'return cmpPost(\''.$this->name.'\', \''.$method.'\', \''.$id.'\', '.json_encode($query, JSON_HEX_APOS).', '.json_encode($data, JSON_HEX_APOS) . ($callback ? ', '.$callback : '').')';
  }
  
  function ajaxQuery($query)
  {
    return 'return cmpQuery('.json_encode($query).')';
  }
  
  
  function outputXMLLS($out)
  {
    foreach (array_keys($this->LS[0]) as $k)
    {
      $n = new XMLNode('LS');
      $n->attribs['name'] = $k;
      $n->text = $this->LS($k);
      $out->addChild($n);
    }
  }

  function outputXmlVardump($out_xml, $var)
  {
    ob_start();
    var_dump($var);

    $t = new XMLNode('_vardump');
    $t->cdata = true;
    $t->text = ob_get_contents();
    $out_xml->addChild($t);

    ob_end_clean();
  }

  function xmlRequest($in_xml, $out_xml)
  {
    //Empty
  }

  function xmlAddResource($xml, $id, $url)
  {
    $n = new XMLNode('resource');
    $n->attribs = Array('id' => $id, 'url' => $url);
    $xml->addChild($n);
  }

  function embedSWF($swfurl, $id, $width=0, $height=0, $params=null, $flashvars=null, $attribs=null, $version='9.0.115')
  {
    $this->includeLibrary('jquery');
    $this->includeLibrary('swfobject');

    if (!$width || !$height)
    {
      $f = getFilePathFromLink($this->site_id, $swfurl);
      $sz = getimagesize($f);
      if (!$width) $width = $sz[0];
      if (!$height) $height = $sz[1];
    }

    if ($params === null) $params = Array();
    if ($flashvars === null) $flashvars = Array();
    if ($attribs === null) $attribs = Array();

    if (!isset($attribs['id'])) $attribs['id'] = $id;

    $this->addScript('
    $(document).ready(function() {
    	$(\'#'.$id.'\').html(\'You need Flash player (v'.$version.') to view this content:<p><a href="http://www.adobe.com/go/getflashplayer"><img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" /></a></p>\');
    	if (swfobject.hasFlashPlayerVersion("'.$version.'")) {
    	  swfobject.embedSWF("'.$swfurl.'", "'.$id.'", "'.$width.'", "'.$height.'", "'.$version.'", "/scr/components/swfobject/expressInstall.swf", '.($flashvars ? json_encode($flashvars) : '{}').', '.($params ? json_encode($params) : '{}').', '.($attribs ? json_encode($attribs) : '{}').');
    	}
    });
    ');
  }

  function embedConstructorSWF($swfurl, $id, $width=0, $height=0, $params=null, $flashvars=null, $attribs=null, $version='9.0.115')
  {
    $url = substr(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH), 1);
    if ($flashvars === null) $flashvars = Array();
    $flashvars['url'] = $url;
    return $this->embedSWF($swfurl, $id, $width, $height, $params, $flashvars, $attribs, $version);
  }

  function indexData($lastindextime)
  {
    return null;
  }

  function getLanguageStrings()
  {
    $ls = Array();
    return $ls;
  }

  function setLanguage()
  {
    if(!isset($this->activelanguage) && $this->site_id && $this->page_id > 0)
    {
      if($this->page_id)
      {
        if($this->pagedev_id)
        {
          $language_id = sqlQueryValue("SELECT language FROM " . $this->site_id . "_pagesdev WHERE pagedev_id=". $this->pagedev_id);
        }else if($this->page_id)
        {
          $language_id = sqlQueryValue("SELECT language FROM " . $this->site_id . "_pages WHERE page_id=". $this->page_id);
        }
        $this->activelanguage = sqlQueryValue("SELECT language_id FROM ".$this->site_id."_languages WHERE language_id=" . intval($language_id));
      }
    }
  }

  function getLanguageName()
  {
    static $cache = array();
    $ckey = $this->site_id.'_'.$this->activelanguage;
    if (isset($cache[$ckey])) return $cache[$ckey];

    $this->setLanguage();
    $value = sqlQueryValue("SELECT shortname FROM ".$this->site_id."_languages WHERE language_id=" . intval($this->activelanguage));

    $cache[$ckey] = $value;
    return $value;
  }

    // new
    public function l($string, $lang = false, $backend = false)
    {

        $params = [];
        if($lang){
            $params['lang'] = $lang;
        }

        return App::l($string, $params);

    }

  function l_v2($string, $lang = false, $backend = false){
		global $languagestrings,$cfgDirRoot;

    $key = transliterateText($string);
    $cmp = get_class($this);
    if ($lang && !is_int($lang)){
			$languages=getLanguages();
			$lang=intval($languages[$lang]['language_id']);
		}
		$lang = $lang ? $lang : $this->activelanguage;

    if(!$lang){
    	return $string; // quick fix for backend default language properties
    }

		if (file_exists(TRANSLATION_FOLDER.$lang.'.'.$cmp.'.lang.php')){
			require_once(TRANSLATION_FOLDER.$lang.'.'.$cmp.'.lang.php');
		}

    if (!isset($languagestrings[$lang][$cmp][$key])){

    	$test=DB::GetValue("select ls_id from ".$this->site_id."_languagestrings where language=:lang and name=:key and component=:cmp",array(':lang'=>$lang,':key'=>$key,':cmp'=>$cmp));
			if (!$test){
      	DB::Query("INSERT INTO `".$this->site_id."_languagestrings`(language, name, value, `default`, component) VALUES(:lang,:key,:val,:val,:cmp)",array(':lang'=>$lang,':key'=>$key,':val'=>$string,':cmp'=>$cmp));
				DB::Query("INSERT INTO ".$this->site_id."_languagestrings_info(name, component, is_used) VALUES(:key,:cmp,1)",array(':key'=>$key,':cmp'=>$cmp));
			}
      $res=DB::GetTable("select name,value,`default` from ".$GLOBALS['site_id']."_languagestrings where language=:lang and component=:cmp order by name",array(':lang'=>$lang,':cmp'=>$cmp));
			if (!file_exists($GLOBALS['cfgDirRoot'].'lang')){
				mkdir($GLOBALS['cfgDirRoot'].'lang');
			}
			$f=fopen(TRANSLATION_FOLDER.$lang.'.'.$cmp.'.lang.php','w');
			fwrite($f,'<?php'."\n");
			foreach ($res as $r){
				$value = $r['value']?$r['value']:$r['default'];
				fwrite($f,'$languagestrings['.$lang.'][\''.$cmp.'\'][\''.$r['name'].'\']=\''.str_replace("'","\'",str_replace('\\','\\\\',$value)).'\';'."\n");
			}
			fwrite($f,'?>');
			fclose($f);
			unset($languagestrings[$lang][$cmp]);
			clearstatcache();
			include(TRANSLATION_FOLDER.$lang.'.'.$cmp.'.lang.php');
    }

    return $languagestrings[$lang][$cmp][$key];

  }

  function l_old($string, $lang = false, $backend = false){

    static $cache = array();

    $key = transliterateText($string);

    $cmp = get_class($this);

    if(isset($cache[$key])) return $cache[$key];

    $lang = $lang? $lang : $this->activelanguage;

    $text_row = sqlQueryRow("
      SELECT l.*, li.is_used, li.id as info_id
      FROM `".$this->site_id."_languagestrings` AS l
      LEFT JOIN ".$this->site_id."_languagestrings_info AS li ON li.component = l.component AND li.name = l.name
      WHERE
        l.language='".$lang."' AND
        l.name='".mysql_real_escape_string($key)."' AND
        l.component='".$cmp."'
      ");

    // new text, lets add
    if(!$text_row){
      sqlQuery("INSERT INTO `".$this->site_id."_languagestrings`(language, name, value, `default`, component) VALUES('".$lang."','".mysql_real_escape_string($key)."', '".mysql_real_escape_string($string)."', '".mysql_real_escape_string($string)."', '".$cmp."')");
      $id = mysql_insert_id();
      $value = $string;
    }else{
      $value = $text_row['value'];
    }


    if(!$backend){

      if(!$text_row['is_used']){

        if($text_row['info_id']){ // existing update
          sqlQuery("UPDATE ".$this->site_id."_languagestrings_info SET is_used = 1 WHERE id = ".$text_row['info_id']);
        }else{ // new adding
          sqlQuery("INSERT INTO ".$this->site_id."_languagestrings_info(name, component, is_used) VALUES('".mysql_real_escape_string($key)."', '".$cmp."', 1)");
        }

      }

    }

    return $value;

  }

  function LS($stringname, $devmode = false, $lang = false){

    $this->setLanguage();

    if($lang){
			$cur_lang = $this->activelanguage;
			$langs = getLanguages();
			$lang = $langs[$lang];
			$this->activelanguage = $lang['language_id'];
		}

    if(!$this->LS[$this->activelanguage][$stringname] && $this->site_id)
    {
      if(substr($stringname, 0, 1) == '_')
      {
        $this->LS[$this->activelanguage][$stringname] = GetLanguageString($this->site_id, $stringname, '', intval($this->activelanguage));
        $cmp = '';
      }else
      {
        $this->LS[$this->activelanguage][$stringname] = GetLanguageString($this->site_id, $stringname, get_class($this), intval($this->activelanguage));
        $cmp = get_class($this);
      }

      if (!$devmode){

        $linfo = sqlQueryRow('SELECT id, is_used FROM '.$this->site_id.'_languagestrings_info WHERE name=:name AND component=:cmp LIMIT 1', array(":name" => $stringname, ":cmp" => $cmp));
        if (!$linfo){
          sqlQuery('INSERT INTO '.$this->site_id.'_languagestrings_info SET name=:name, component=:cmp, is_used=1', array(":name" => $stringname, ":cmp" => $cmp));
        }else if (!$linfo['is_used']){
          sqlQuery('UPDATE '.$this->site_id.'_languagestrings_info SET is_used=1 WHERE id='.$linfo['id']);
        }
      }

      if(!$this->LS[$this->activelanguage][$stringname])
      {
        $this->LS[$this->activelanguage][$stringname] = $this->LS[0][$stringname];
        $this->LS_def[$this->activelanguage][$stringname] = true;
      }else
      {
        $this->LS_def[$this->activelanguage][$stringname] = false;
      }
    }

    $r = $this->LS[$this->activelanguage][$stringname];

    if($cur_lang){
    	$this->activelanguage = $cur_lang;
		}

    return $r;
  }

  function initCollections(){
                           
    if($this->collections_inited === true) return true;

    $ok = true;
    foreach($this->properties as $key => $property){

      if($property['type'] == 'collection'){
        $this->$key = $this->initPropertyCollection($key);
        if(!$this->$key){
          $ok = false;
          break;
        }
      }

    }

    $this->collections_inited = $ok;

    return $ok;

  }

  function setTemplate()
  {
    if(!$this->template_id && $this->page_id)
    {
      $this->template_id = sqlQueryValue('SELECT template FROM ' . $this->site_id . '_pages WHERE page_id = ' . $this->page_id);
    }
  }

  function getViewId(){

    if(!isset($GLOBALS['views'][$this->template_id][$this->activelanguage]) && $this->activelanguage){
      $GLOBALS['views'][$this->template_id][$this->activelanguage] = sqlQueryValue("SELECT view_id FROM " . $this->site_id . "_views WHERE template_id = ".$this->template_id." AND language_id = ".$this->activelanguage);
    }

    return $GLOBALS['views'][$this->template_id][$this->activelanguage];

  }

  /*
    Loads all component properties in memory for faster access
  */
  function loadPropertiesInMemory(){

    if(!empty($GLOBALS['ComponentProperties'])) return;

    $query = "
        SELECT
          propertyname, propertyvalue, componentname
        FROM " . $this->site_id . "_contents
        WHERE
          page = -1
          ".($this->page_id ? " OR page = ".$this->page_id : "")."
        ORDER BY
          IF(page = -1, 1, 0) DESC
          ".($this->page_id ? ",IF(page = ".$this->page_id.", 1, 0) DESC " : "")."
        ";

      $GLOBALS['ComponentProperties'] = array();

      $data = sqlQueryData($query);

      foreach($data as $row){
        $GLOBALS['ComponentProperties'][$row['componentname']][$row['propertyname']] = $row['propertyvalue'];
      }

  }

  function getPropertyEnchanted($property){

    if (!$this->dev && $this->pagedev_id > 0){
      $this->dev = true;
    }

    $this->loadPropertiesInMemory();

    return isset($GLOBALS['ComponentProperties'][$this->name][$property]) ? $GLOBALS['ComponentProperties'][$this->name][$property] : $this->properties[$property]['value'];

  }

  function getProperty($property){

    if (!$this->dev){
        if ($this->pagedev_id > 0) $this->dev = true;
    }

    // no page_id, use read default values
    if(!$this->page_id){
      $this->page_id = -1;
    }

    return $this->getPropertyEnchanted($property);

    // REST IS OLD

    //obtain all properties of this component unless obtained
    if(!isset($this->propertyvalues))
    {
      if ($this->dev==true)
      {
        $this->propertyvalues = sqlQueryData("SELECT propertyname, propertyvalue FROM " . $this->site_id . "_contentsdev WHERE componentname='".$this->name."' AND pagedev=".intval($this->pagedev_id)."");
      }
      else
      {
        $this->propertyvalues = sqlQueryData("SELECT propertyname, propertyvalue FROM " . $this->site_id . "_contents WHERE componentname='".$this->name."' AND page=".intval($this->page_id)."");
      }
      //$this->propertyvalues = sqlQueryData("SELECT propertyname, propertyvalue FROM " . $this->site_id . "_contents WHERE componentname='".$this->name."' AND page=".$this->page_id."");
    }

    $pageData = null;
    $foundproperties = '';
    //find property in obtained properties list
    foreach($this->propertyvalues as $p)
    {
      if($p['propertyname'] == $property)
      {
        $pageData = $p;
      }
      if($foundproperties) $foundproperties .= ",";
      $foundproperties .= "'" . $p['propertyname'] . "'";
    }

    //find propery in copypages list
    $copypage = 0;
    $usinglangcopypage = false;
    if(($pageData==null)and($this->page_id!=-1))
    {
      $data = null;
      $copypage = $this->copypage;
        while((($copypage || !$usinglangcopypage)and($data == null)))
        {
          if($copypage)
          {
            if(!isset($this->copypropertyvalues[$copypage]))
            {
              $this->copypropertyvalues[$copypage] = sqlQueryData("SELECT propertyname, propertyvalue FROM " . $this->site_id . "_contents WHERE ".IIF($foundproperties," propertyname NOT IN ($foundproperties) AND", "")." componentname='".$this->name."' AND page=".$copypage."");
            }


            //find property in obtained copypage properties list
            if($this->copypropertyvalues[$copypage])
            {
              foreach($this->copypropertyvalues[$copypage] as $p)
              {
                if($p['propertyname'] == $property)
                {
                  $pageData = $p;
                  $data = $p['propertyvalue'];
                }
                if($foundproperties) $foundproperties .= ",";
                $foundproperties .= "'" . $p['propertyname'] . "'";
              }
            }
          }
          //get next copypage
          if($data == null)
          {
            if($copypage)
            {
              if(!isset($this->copypageparents[Abs($copypage)]))
              {
                $c = Abs($copypage);
                if($GLOBALS['runningfromfrontend'] && isset($GLOBALS['cachedviewparent.' . $c]))
                {
                  $copypage = $GLOBALS['cachedviewparent.' . $c];
                }else
                {
                  $copypage = sqlQueryValue("SELECT -parent FROM " . $this->site_id . "_views WHERE view_id=" . Abs($copypage));
                }
                $GLOBALS['cachedviewparent.' . $c] = Abs($copypage);
                $this->copypageparents[$c] = $copypage;
              }else
              {
                $copypage = $this->copypageparents[Abs($copypage)];
              }
            }
            if(!$copypage && !$usinglangcopypage)
            {
              $usinglangcopypage = true;
              if($this->template_id && $this->activelanguage)
              {
                if(!isset($GLOBALS['cachedlangview' . $this->template_id . '_' . $this->activelanguage]))
                {
                  $copypage = sqlQueryValue('SELECT -view_id FROM ' . $this->site_id . '_views WHERE template_id = ' . $this->template_id . ' AND language_id = ' . $this->activelanguage);
                  $GLOBALS['cachedlangview' . $this->template_id . '_' . $this->activelanguage] = $copypage;
                }else
                {
                  $copypage = $GLOBALS['cachedlangview' . $this->template_id . '_' . $this->activelanguage];
                }
              }
            }
          }

        }

      if($data==null)
      {
        //get all default properties for this page
        if(!isset($this->defpropertyvalues))
        {
          $this->defpropertyvalues = sqlQueryData("SELECT propertyname,propertyvalue FROM " . $this->site_id . "_contents WHERE" . IIF($foundproperties," propertyname NOT IN ($foundproperties) AND", "") . " componentname='".$this->name."' AND page=-1");
        }
        //find property in obtained properties list
        foreach($this->defpropertyvalues as $p)
        {
          if($p['propertyname'] == $property)
          {
            $data = $p['propertyvalue'];
          }
        }
      }

    }else
    {
      $data = $pageData['propertyvalue'];
    }
    if($data==null)
    {
      $data = $this->properties[$property]['value'];
    }

    return $data;
  }

  function setProperty($property, $value, $donotautopublish = false)
  {
    if (!$this->dev)
    {
        if ($this->pagedev_id > 0) $this->dev = true;
    }

    if ($this->dev) $page_id = $this->pagedev_id;
    else $page_id = $this->page_id;

    //return setProperty($property, $value, $this->name, $this->page_id, $this->site_id, $this->dev);
    $p_ = setProperty($property, $value, $this->name, $page_id, $this->site_id, $this->dev);

    //autopublishing
    if($this->dev && !$donotautopublish)
    {
      if (CheckPermission(31, $this->site_id, $this->page_id) || CheckPermission(32, $this->site_id))
      {
          pageautopublish($this->site_id, $this->pagedev_id, true);
      }
    }

    //find property in obtained properties list
    foreach($this->propertyvalues as $key => $p)
    {
      if($p['propertyname'] == $property)
      {
        $this->propertyvalues[$key]['propertyvalue'] = $value;
      }
    }

    return $p_;
  }

  function setDefaultProperty($property, $value)
  {
    $p_ = setProperty($property, $value, $this->name, -1, $this->site_id, 0);

    unset($this->defpropertyvalues);

    return $p_;
  }

  //NB: call from backend only
  function setLanguageDefaultProperty($property, $value, $language_id = 0)
  {
    if(!$language_id)
    {
      $this->setLanguage();
      $language_id = intval($this->activelanguage);
    }
    $template_id = sqlQueryValue("SELECT template FROM ".$this->site_id."_pagesdev WHERE pagedev_id=".$this->pagedev_id." ORDER BY lastmod DESC LIMIT 1");
    if($language_id)
    {
      //does view exist?
      $row = sqlQueryRow('SELECT * FROM ' . $this->site_id . '_views WHERE template_id = ' . $template_id . ' AND language_id = ' . $language_id);
      if(!$row)
      {
        //do not allow view_id to be -1 as -1 represents default (template) values
        $maxid = sqlQueryValue("SELECT max(view_id) FROM ".$site_id."_views");
        if($maxid)
          $newid = '';
        else
          $newid = '2';
        //create new autogenerated view
        sqlQuery("INSERT INTO `".$this->site_id."_views` ( `view_id`, `ind` , `template_id` , `name` , `parent` , `description` , `levels` , `parents` , `languages` , `language_id` )
                  VALUES (
                  '".$newid."', '0', '".$template_id."', 'autolanguage_".$template_id."_".$language_id."', '0', '', '', '', '', '".$language_id."'
                  )");
        $row = sqlQueryRow('SELECT * FROM ' . $this->site_id . '_views WHERE template_id = ' . $template_id . ' AND language_id = ' . $language_id);
      }
      $p_ = setProperty($property, $value, $this->name, -$row['view_id'], $this->site_id, 0);

      unset($this->copypropertyvalues);

      return $p_;
    }
    return false;
  }

  function Output()
  {
    //Empty
  }

  function Execute()
  {
    //Empty
  }

  function Design()
  {
    echo '<TABLE border=0 cellspacing="0" bordercolor="#000000">';
    echo '<TR><TD>' . $this->name . '</TD></TR></TABLE>';
  }

  function ListDesign ()
  {
    $this->defaultAfterRefreshMethod = 'ListDesign';
    $this->Design();
  }

  function designTemplate()
  {
    //echo '';
  }

  function designTemplateEx()
  {
    if ($this->designTemplateAsText())
    {
        $this->designTemplateText();
    }
    else
    {
        $this->designTemplateDiv();
    }
  }

  function designTemplateAsText()
  {
    return false;
  }

  function designTemplateDiv()
  {
      echo '<div specComponentTag="'.$this->specTag.'" id="'.$this->name.'" contentEditable=false>';
      $this->designTemplate();
      echo '</div>';
  }

  function designTemplateText()
  {
      echo $this->specTag;
  }

  function Visible()
  {
    return !$this->getProperty('visible');
  }

  function ProcessTemplate($template)
  {
    $this->outcalls['lang'] = '$this->getLanguageName()';

    // matches double quoted strings:
    // "foobar"
    // "foo\"bar"
    $this->_db_qstr_regexp = '"[^"\\\\]*(?:\\\\.[^"\\\\]*)*"';

    // matches single quoted strings:
    // 'foobar'
    // 'foo\'bar'
    $this->_si_qstr_regexp = '\'[^\'\\\\]*(?:\\\\.[^\'\\\\]*)*\'';

    // matches single or double quoted strings
    $this->_qstr_regexp = '(?:' . $this->_db_qstr_regexp . '|' . $this->_si_qstr_regexp . ')';


    // matches numerical constants
    // 30
    // -12
    $this->_num_const_regexp = '(?:\-?\d+(?:\d+)?)';

    // matches out vars:
    // !foo
    $this->_svar_regexp = '\!\w+';
    $this->_svar_regexpex = '\!(\w+)';

    // matches vars:
    // $foo
    $this->_plvar_regexp = '\$\w+';

    // matches $ vars (not objects):
    // $foo
    // $foo.bar
    // $foo.bar.foobar
    // $foo[0]
    // $foo[$bar]
    // $foo[5][blah]
    // $foo[5].bar[$foobar][4]
    $this->_dvar_regexp = '(?:'.$this->_plvar_regexp.'|'.$this->_svar_regexp.')(?:'
            . ')*(?:\.(?:\$?\w+|'.$this->_svar_regexp.'))*(?:(?:' . $this->_num_const_regexp . ')*)?';


    // matches property vars:
    // #foo#
    // #foobar123_foo#
    $this->_cvar_regexp = '\#\w+\#';
    $this->_cvar_regexpex = '\#(\w+)\#';

    // matches languagestrings vars:
    // ^foo^
    // ^foobar123_foo^
    $this->_lsvar_regexp = '\^[^\^]+\^';
    $this->_lsvar_regexpex = '\^([^\^]+?)(:([^\^]+))?\^';

    // matches all valid method calls
    $this->_methodcall_regexp = '\$this->[a-zA-Z0-9_]+?\([^)]*\)';
    $this->_methodcall_regexpex = '\$this->([a-zA-Z0-9_]+?)\(([^)]*)\)';

    // matches all valid variables (no quotes, no modifiers)
    $this->_avar_regexp = '(?:' . $this->_dvar_regexp . '|'
    . $this->_cvar_regexp . '|'. $this->_lsvar_regexp . '|' . $this->_svar_regexp . '|' . $this->_methodcall_regexp . ')';

    // matches valid variable syntax:
    // $foo
    // $foo
    // #foo#
    // #foo#
    // "text"
    // "text"
    $this->_var_regexp = '(?:' . $this->_avar_regexp . '|' . $this->_qstr_regexp . ')';

    //matches all variables and values
    $this->_val_regexp = '(?:' . $this->_var_regexp . '|' . $this->_num_const_regexp . '|\w+)';

    //matches php code area
    $this->_php_code = '\[php\](.*?)\[\/php\]';
    $this->_php_foreach = '\[foreach (' . $this->_var_regexp . ') (' . $this->_var_regexp . ') (' . $this->_var_regexp . ')(?: ('.$this->_num_const_regexp.'))?\]';
    $this->_php_foreachend = '\[\/foreach]';
    $this->_php_if = '\[if (' . $this->_val_regexp . ')(?:[ ]*?(<|>|==|!=|>=|<=)[ ]*?(' . $this->_val_regexp . '))?\]';
    $this->_php_elseif = '\[elseif (' . $this->_val_regexp . ')(?:[ ]*?(<|>|==|!=|>=|<=)[ ]*?(' . $this->_val_regexp . '))?\]';
    $this->_php_ifelse = '\[else]';
    $this->_php_ifend = '\[\/if]';
    $this->_php_cycle = '\[cycle (\w+) (\S+) (' . $this->_qstr_regexp . ')\]';
    $this->_php_list = '\[list (' . $this->_val_regexp . ')]';
    $this->_php_listend = '\[\/list]';
    $this->_php_code_all = '(?:' . $this->_php_code . '|' . $this->_php_foreach . '|' . $this->_php_foreachend
                            . '|' . $this->_php_if . '|' . $this->_php_ifelse . '|' . $this->_php_ifend
                            . '|' . $this->_php_elseif . '|' . $this->_php_cycle . '|' . $this->_php_list
                            . '|' . $this->_php_listend . ')';

    $t = $this->getProperty($template);
    //languagestrings - use page template model as an alternative
    $t = preg_replace('/{%ls:([a-zA-Z0-9_]+)(:(([^%])*))?%}/s', '{^\1\2^}', $t);
    $t1 = $t;

    preg_match_all('/' . $this->_php_code_all . '/s', $t, $phpmatches, PREG_SET_ORDER | PREG_OFFSET_CAPTURE);

    $positions = $this->_getPhpCodePositions($phpmatches);

    $lastend = strlen($t);
    foreach($positions as $p)
    {
      $t = substr_replace($t, 'echo \'' . $this->_processTemplateText($this->addslashesex(substr($t, $p[1], $lastend - $p[1]))) . '\';', $p[1], $lastend - $p[1]);
      $lastend = $p[0];
      $t = substr_replace($t, $this->_phpreplace(substr($t, $p[0], $p[1] - $p[0])), $p[0], $p[1] - $p[0]);
    }
    $t = substr_replace($t, 'echo \'' . $this->_processTemplateText($this->addslashesex(substr($t, 0, $lastend))) . '\';', 0, $lastend);

    eval($t);
  }


  function ReturnTemplate($template)
  {
    ob_start();
    $this->ProcessTemplate($template);
    $s = ob_get_contents();
    ob_end_clean();
    return $s;
  }


  function parseTemplatePropertiesForLanguageStrings()
  {
    foreach($this->properties as $key => $property)
    {
      if($property['type'] == 'template')
      {
        $value = $this->getProperty($key);
        $value = preg_replace('/{%ls:([a-zA-Z0-9_]+)(:(([^%])*))?%}/s', '{^\1\2^}', $value);
        preg_match_all('/\{\^([^\^]+?)(:([^\^]+))?\^\}/s', $value, $matches);
        for ($i=0; $i<count($matches[1]); $i++)
        {
          if (strlen($matches[3][$i]))
            $this->LS[0][$matches[1][$i]] = $matches[3][$i];

        }
      }
    }
  }


  function addslashesex($s)
  {
    $s = str_replace('\\', '\\\\', $s);
    $s = str_replace('\'', '\\\'', $s);

    return $s;
  }


  function _processTemplateVar($var)
  {
     if(preg_match('/^' . $this->_methodcall_regexpex . '|' . $this->_lsvar_regexpex . '$/', $var, $m))
     {
       $bits = Array($var);
     }else
     {
       $bits = explode('.', $var);
     }
     $s = '';
     foreach($bits as $bit)
     {
       if(!$s)
       {
         $s = $this->_bitreplace($bit);
       }else
       {
         $s = $s . '[' . $this->_bitreplace($bit) . ']';
       }
     }
     return $s;
  }



  function _processTemplateText($t)
  {
    preg_match_all('/{(' . $this->_var_regexp . ')}/s', $t, $matches, PREG_SET_ORDER | PREG_OFFSET_CAPTURE);
    foreach($matches as $match)
    {
       $s = $this->_processTemplateVar($match[1][0]);
       $t = str_replace($match[0][0], '\' . ' . $s . ' . \'', $t);
    }
    return $t;
  }


  function _getPhpCodePositions($phpmatches)
  {
    $positions = array();
    foreach($phpmatches as $phpm)
    {
      $positions[] = Array($phpm[0][1], $phpm[0][1] + strlen($phpm[0][0]));
    }
    arsort($positions);
    return($positions);
  }


  function _phpreplace($s)
  {
    if(preg_match('/^' . $this->_php_code . '$/s', $s, $m))
    {
      return $m[1];
    }else if(preg_match('/^' . $this->_php_foreach . '$/s', $s, $m))
    {
      if($m[4])
      {
        do
        {
          $name = 'foreach_counter_' . rand(0, 99999);
        }while(isset($this->$name));
        $this->$name = 0;
        $r1 = '$this->' . $name . ' = 0;';
        $r2 = 'if($this->' . $name . ' >= ' . $m[4] . ') break; $this->' . $name . '++;';
      }else
      {
        $r1 = '';
        $r2 = '';
      }
      return $r1 . 'foreach(' . $this->_processTemplateVar($m[1]) . ' as ' . $this->_processTemplateVar($m[2]) . ' => ' . $this->_processTemplateVar($m[3]) . '):' . $r2;
    }else if(preg_match('/^' . $this->_php_foreachend . '$/s', $s, $m))
    {
      return 'endforeach;';
    }else if(preg_match('/^' . $this->_php_if . '$/s', $s, $m))
    {
      return 'if(' . $this->_processTemplateVar($m[1]) . ($m[2] ? $m[2] . $this->_processTemplateVar($m[3]) : '') . '):';
    }else if(preg_match('/^' . $this->_php_elseif . '$/s', $s, $m))
    {
      return 'elseif(' . $this->_processTemplateVar($m[1]) . ($m[2] ? $m[2] . $this->_processTemplateVar($m[3]) : '') . '):';
    }else if(preg_match('/^' . $this->_php_ifelse . '$/s', $s, $m))
    {
      return 'else:';
    }else if(preg_match('/^' . $this->_php_ifend . '$/s', $s, $m))
    {
      return 'endif;';
    }else if(preg_match('/^' . $this->_php_cycle . '$/s', $s, $m))
    {
      return '
        if(!isset($this->cyclevars[\'' . $m[1] . '\']))
        {
          $this->cyclevars[\'' . $m[1] . '\'][\'pos\'] = 0;
          $this->cyclevars[\'' . $m[1] . '\'][\'array\'] = explode(\'' . $m[2] . '\', ' . $m[3] . ');
        }else
        {
          $this->cyclevars[\'' . $m[1] . '\'][\'pos\']++;
          if($this->cyclevars[\'' . $m[1] . '\'][\'pos\'] >= count($this->cyclevars[\'' . $m[1] . '\'][\'array\']))
          {
            $this->cyclevars[\'' . $m[1] . '\'][\'pos\'] = 0;
          }
        }
        echo $this->cyclevars[\'' . $m[1] . '\'][\'array\'][$this->cyclevars[\'' . $m[1] . '\'][\'pos\']];
      ';
    }else if(preg_match('/^' . $this->_php_list . '$/s', $s, $m))
    {
      //izveelas unikaalu nosaukumu rindas apziimeejumam
      do
      {
        $name = 'list_row_' . rand(0, 99999);
      }while(isset($this->$name));
      $this->$name = true;

      return 'foreach(' . $this->_processTemplateVar($m[1]) . ' as $'.$name.'_list_row):
      if(!isset($this->_list_old_outvars)) $this->_list_old_outvars = array();
      array_push($this->_list_old_outvars, $this->outvars);
      $this->outvars = array_merge($this->outvars, $'.$name.'_list_row);
      ';
    }else if(preg_match('/^' . $this->_php_listend . '$/s', $s, $m))
    {
      return '$this->outvars = array_pop($this->_list_old_outvars);
      endforeach;';
    }
    return '';
  }


  function _bitreplace($s)
  {
    if(preg_match('/^' . $this->_svar_regexpex . '$/', $s, $m))
    {
      if($this->outcalls[$m[1]])
        return $this->outcalls[$m[1]];
      else
        return '$this->outvars[\'' . $m[1] . '\']';
    }else if(preg_match('/^' . $this->_plvar_regexp . '$/', $s, $m))
    {
      return $s;
    }else if(preg_match('/^' . $this->_cvar_regexpex . '$/', $s, $m))
    {
      return '$this->getProperty(\'' . $m[1] . '\')';
    }else if(preg_match('/^' . $this->_lsvar_regexpex . '$/', $s, $m))
    {
      return '$this->LS(\'' . $m[1] . '\')';
    }else if(preg_match('/^' . $this->_methodcall_regexpex . '$/', $s, $m))
    {
      return '$this->'.$m[1].'(' . $m[2] . ')';
    }
    else if(is_numeric($s) || preg_match('/^' . $this->_qstr_regexp . '$/', $s, $m))
    {
      return $s;
    }else
    {
      return '\'' . $s . '\'';
    }

  }

    function InitPropertyCollection($property){

        static $collection_ids;

        if(!$collection_ids){
            $collection_ids = collection::getCollectionIds($this->site_id);
        }

        //initialize collection
        $colid = $this->getProperty($property);

        if(!$colid || !in_array($colid, $collection_ids)){ // we dont have collection_id set, but maybe we can find/create one?

            $type = $this->properties[$property]['collectiontype'];

            if($type){

                $collections = DB::GetTable("SELECT * FROM ".$this->site_id."_collections WHERE `type` = :type", array(":type" => $type));
                  if(count($collections) == 1){ // we have only one collection of this type.. lets assume it's THE ONE!
                    $this->setDefaultProperty($property, $collections[0]['collection_id']);
                    $colid = $collections[0]['collection_id'];
                }elseif(count($collections) == 0){ // ooo.. we dont have any collections of this type.. let's auto-create one!

                    $colname = strtolower(pdocollection::classnameToType($type));
                    $collection = new $type($colname, 0);
                    $collection->addNewCollection($colname);
                    $colid = $collection->collection_id;
                    $collection_ids[] = $colid;
                    $this->setDefaultProperty($property, $colid);

                }else{
                // sorry, there are multiple collections of this type, we cannot auto-detect THE ONE
                }

            }

        }

        if($colid){

            $type = $this->properties[$property]['collectiontype'];

            $colname = '';
            $collection = new $type($colname, $colid);

            if($collection->isValidCollection()){
                return $collection;
            }else{ // collection not found
                return false;
            }

        }

        return false;

    }

  function getDialogFormOutput($form_data, $target_method, $title, $additional_params = Array(), $refresh = '1', $cancel = 'close', $okaction = 'close')
  {
    $addurl = '';
    foreach($additional_params as $key => $val)
    {
      $addurl .= '&' . urlencode($key) . '=' . urlencode($val);
    }
    require_once($GLOBALS['cfgDirRoot']."library/"."class.formgen.php");
    $FormGen = new FormGen();
    $FormGen->action = '?module=dialogs&action=component&site_id='.
		$this->site_id.'&page_id='.$this->page_id.'&component_name='.
		$this->name.'&component_type='.
		get_class($this).'&method_name='.$target_method.'&refresh='.$refresh.
        '&okaction='.$okaction.'&afterrefreshmethod='.$this->defaultAfterRefreshMethod.'&paramstr='.urlencode($this->componentParamStr).$addurl;
    $FormGen->title = $title;
    $FormGen->properties = $form_data;
    $FormGen->cancel = $cancel;
    $FormGen->buttons = false;
    return $FormGen->output();
  }

  function getComponentDialogLink($target_method, $additional_params = Array(), $title='', $okaction = 'submit', $refresh = '0')
  {
    $addurl = '';
    if(is_array($additional_params)){
        foreach($additional_params as $key => $val)
        {
          $addurl .= '&' . urlencode($key) . '=' . urlencode($val);
        }
    }
    return '?module=dialogs&action=componentframe&site_id='.$this->site_id.'&page_id='.$this->page_id.'&component_name='.$this->name.'&component_type='.get_class($this).'&method_name='.$target_method.'&refresh='.$refresh.'&paramstr='.urlencode($this->componentParamStr).'&okaction='.$okaction.'&afterrefreshmethod='.$this->defaultAfterRefreshMethod.'&session_id=' . session_id() . '&session_name=' . session_name() . '&title=' . urlencode($title) . $addurl;
  }

  function getComponentModifierLink($target_method, $additional_params = Array(), $refresh = '1', $afterrefreshmethod = '')
  {
    $addurl = '';
    foreach($additional_params as $key => $val)
    {
      $addurl .= '&' . urlencode($key) . '=' . urlencode($val);
    }
    if(!$afterrefreshmethod)
      $afterrefreshmethod = $this->defaultAfterRefreshMethod;
    return '?module=dialogs&action=component&site_id='.$this->site_id.'&page_id='.$this->page_id.'&component_name='.$this->name.'&component_type='.get_class($this).'&method_name='.$target_method.'&refresh='.$refresh.'&paramstr='.urlencode($this->componentParamStr).'&session_id=' . session_id() . '&session_name=' . session_name() . '&afterrefreshmethod=' . $afterrefreshmethod . $addurl;
  }

  function getDesignIconScript($link, $width=1, $height=1, $confirm='', $targetdiv='', $callback = false)
  {
    if(!$targetdiv)
      $targetdiv = 'div_'.$this->name;
    $r = '
    javascript:';
    if($confirm)
      $r .= 'if(!confirm(\''.parseForJScript($confirm, false).'\'))
                                           return false;';
    $r .= '
                 try {
                      PWin.close();
                     } catch(e) { }
                 var args = new Array(window, document.getElementById(\''.$targetdiv.'\'));
								 openDlg(\''.$link.'\', args, {w: '.$width.', h: '.$height.'}'.($callback?", ".$callback : "").');
								 

    ';
    return $r;
  }

  function getDesignIconOutput($img, $link, $width=1, $height=1, $confirm='', $targetdiv='')
  {
    $imgsrc = $GLOBALS['cfgWebRoot'].'gui/images/'.$img.'.gif';
    $r = '<img src="'.$imgsrc.'"
			onclick="'.$this->getDesignIconScript($link, $width, $height, $confirm, $targetdiv).'"
			>';
    return $r;
  }

  function clearCache($thispageonly = true)
  {
    if($thispageonly)
      sqlQuery("DELETE FROM `".$this->site_id."_componentcache` WHERE page_id = ".$this->page_id." AND component = '".$this->name."'");
    else
      sqlQuery("DELETE FROM `".$this->site_id."_componentcache` WHERE component = '".$this->name."'");

    unset($GLOBALS['component_cache'][$this->name]);
  }


  function getCache($name)
  {
    if(isset($GLOBALS['component_cache'][$this->name][$name]))
      return $GLOBALS['component_cache'][$this->name][$name];
    else
    {
      //attempt to query for cache
      $row = sqlQueryRow("SELECT * FROM `".$this->site_id."_componentcache` WHERE page_id IN (-1, ".$this->page_id.") AND component='".$this->name."' AND cachename='".$name."' ORDER BY page_id DESC");
      if($row)
      {
        $GLOBALS['component_cache'][$this->name][$name] = $row['content'];
        return $GLOBALS['component_cache'][$this->name][$name];
      }
      else
        return false;
    }
  }

  function unsetCache($name)
  {
    if(isset($GLOBALS['component_cache'][$this->name][$name]))
      unset($GLOBALS['component_cache'][$this->name][$name]);
    sqlQuery("DELETE FROM `".$this->site_id."_componentcache` WHERE page_id IN (-1, ".$this->page_id.") AND component='".$this->name."' AND cachename='".$name."'");
  }

  function setCache($name, $value, $page = 0, $loadonstartup = 1)
  {
    $this->unsetCache($name);
    if($page == 0)
      $page = $this->page_id;
    sqlQuery("INSERT INTO `".$this->site_id."_componentcache` (component, cachename, page_id, read_on_load, content) VALUES
      ('".$this->name."', '".$name."', '".$page."', '".$loadonstartup."', '".AddSlashes($value)."')");
    $GLOBALS['component_cache'][$this->name][$name] = $value;
  }


  //default handler for calling collection methods from component;
  // expects GET parameters: id = id of collection item processed,
  //                         colproperty = component's property name containing collection,
  //                         collection_method = collection's method name
  function collection_call_method()
  {
    $ids = explode(',',$_GET['id']);
    if (empty($ids))
    {
      $ids = Array(intval($_GET['id']));
    }else
    {
      foreach($ids as $key => $val)
      {
        $ids[$key] = intval($val);
        if(!$ids[$key])
        {
          unset($ids[$key]);
        }
      }
    }

    $colproperty = $_GET['colproperty'];
    $method = $_GET['collection_method'];
    $collection = $this->InitPropertyCollection($colproperty);
    if($collection)
    {
      $collection->$method($ids);
    }else
    {
      echo 'Invalid collection '.$colproperty.'!';
    }
  }


  function DefaultCollectionEditAddForm()
  {
    $collection = $this->InitPropertyCollection($_GET['colproperty']);
    if($collection)
    {
      $id = intval($_GET['id']);
      $create = !$id;
      if(!$create)
      {
        $params = Array('colproperty' => $_GET['colproperty'], 'id' => $id, 'collection_method' => 'saveEditForComponents');
      }else{
        $params = Array('colproperty' => $_GET['colproperty'], 'collection_method' => 'saveAddForComponents');
      }
      $action = $this->getComponentModifierLink('collection_call_method', $params);
      $collection->displayAddEditForm($create, $id, $action, $_GET['title'], false);
    }
  }

  function DefaultCollectionItemEditIcon($colproperty, $id, $collection = false, $width = 500, $height = 500)
  {
    $r = '';

    $params = Array('colproperty' => $colproperty, 'id' => $id);
    $link = $this->getComponentDialogLink('DefaultCollectionEditAddForm', $params, 'Edit item');
    $r .=   $this->getDesignIconOutput('edit', $link, $width, $height);

     return $r;
  }


  function DefaultCollectionItemIcons($colproperty, $id, $collection = false, $width = 500, $height = 500)
  {
    $r = '';

    $r .= $this->DefaultCollectionItemEditIcon($colproperty, $id, $collection, $width, $height);

    if(!$collection || $collection->CanChangeOrder())
    {
      $params = Array('colproperty' => $colproperty, 'id' => $id, 'collection_method' => 'moveItemUpForComponents');
      $link = $this->getComponentModifierLink('collection_call_method', $params);
      $r .=   $this->getDesignIconOutput('up', $link, 1, 1);

      $params = Array('colproperty' => $colproperty, 'id' => $id, 'collection_method' => 'moveItemDownForComponents');
      $link = $this->getComponentModifierLink('collection_call_method', $params);
      $r .=   $this->getDesignIconOutput('down', $link, 1, 1);
    }

    $params = Array('colproperty' => $colproperty, 'id' => $id, 'collection_method' => 'deleteItemForComponents');
    $link = $this->getComponentModifierLink('collection_call_method', $params);
    $r .=   $this->getDesignIconOutput('del', $link, 1, 1, 'Are you sure you want to delete this item?');


    return $r;
  }


  function DefaultCollectionAddButton($title, $colproperty, $collection = false, $width = 500, $height = 500)
  {
    $params = Array('colproperty' => $colproperty, 'id' => '0');
    $link = $this->getComponentDialogLink('DefaultCollectionEditAddForm', $params, 'Add Item');
    return '<input type="button" value="'.$title.'" OnClick="'.
      $this->getDesignIconScript($link, $width, $height)
      .'">';
  }

  function visibleService()
  {
    return true;
  }

  public function registerAsService($name)
  {

    $this->service_name = $name;
    self::$_instances[$this->type][$name] = $this;

  }

  function getBackendServiceByName($name, $showerror=false)
  {
    $s = $this->_getBackendServiceByName($name);
    if ($showerror && !$s) die(get_class($this).' '.$this->name.': backendService '.$name.' not found!');

    if($s) $s->afterGetService();

    return $s;
  }

    function getServiceByName($name, $showerror=false)
    {

        /*static $counter;
        $counter++;
        echo $this->name.' -> '.$name."<br/>";
        if($counter == 10){
            die();
        }*/

        foreach(self::getInstances() as $type => $instances){
            foreach($instances as $instance_name => $instance){
                if($instance_name == $name){
                    break 2;
                }
            }
        }

        $service = $instance;

        if ($showerror && !$service && !$this->dev){
            echo get_class($this).' '.$this->name.': service '.$name.' not found!';
        }

        if($service){
            $service->afterGetService();
        }

        return $service;

    }

  function afterGetService(){}

  function RefineParams(&$params)
  {
    //Empty
  }

  function Install()
  {
    return true;
  }

  function CanBeCompiled()
  {
    return False;
  }

  function StandartEdit()
  {
    //Empty
  }

  function toolbar(&$script, &$toolbars, &$btnids, &$onclick)
  {
    return False;
  }

  function CanLoadDesignDynamically()
  {
    return false;
  }

  function InitFormData()
  {
    foreach($this->properties as $propertyname => $property)
    {
      if($property['type'] == 'template')
      {
        //add language name to sample tags
        if($this->properties[$propertyname]['tags'])
          $this->properties[$propertyname]['tags'] .= ';';
        $this->properties[$propertyname]['tags'] .= '{!lang}';
        //add language strings to sample tags
        $ls = $this->getLanguageStrings();
        foreach($ls as $key => $val)
        {
          if($this->properties[$propertyname]['tags'])
            $this->properties[$propertyname]['tags'] .= ';';
          $this->properties[$propertyname]['tags'] .= '{^' . $key . '^}';
        }
      }
    }
  }

    protected function postSetProperties(){}

    protected function prepareEditProperties()
    {

        foreach($this->properties as $key => $property){

            if($property['type'] == 'collection'){

                if(!$property['lookup']) $this->properties[$key]['lookup'] = GetCollections($this->site_id, $property['collectiontype']);
                if(!$property['dialog']) $this->properties[$key]['dialog'] = "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=";
                if(!$property['dialogw']) $this->properties[$key]['dialogw'] = "600";

            }elseif($property['type'] == 'page_id'){

                static $combopages;
                if(!$combopages){
                    $pages = sqlQueryData("SELECT page_id,name,parent,title FROM " . $this->site_id . "_pages ORDER BY ind");
                    ListNodes($pages, $combopages, 0, 0);
                }

                $this->properties[$key]['lookup'] = $combopages;
                $this->properties[$key]['type'] = 'list';

            }

        }

    }

    protected function addProperties()
    {
        return [];
    }

    private function setProperties()
    {

        $properties = [
            "name" => [
                "label"     => "Name:",
                "type"      => "string"
            ]
        ];

        if(is_array($this->addProperties())){
            foreach($this->addProperties() as $key => $property){
                $properties[$key] = $property;
            }
        }

        $this->properties = $properties;

        $this->PostSetProperties();

    }

}


