<?php
/**
*
*  Title: Language menu
*  Author: Kristaps Taube <ktaube@datateks.lv>
*  Date: 14.07.2016
*  Project: EVeikals
*
*/

require_once("class.component.php");

class EVeikalsLangMenu extends component{

  function __construct($name){
    parent::__construct($name);
    $this->SetProperties();
  }

  function output(){

		?>
    <ul class='lang menu'>
			<? foreach(getLanguages() as $lang => $lang_data){ ?>
			<? if($lang_data['disabled']) continue; ?>
      <li class='<?=($this->lang == $lang) ? 'active' : ''?>'><a href="<?=$this->shoplangswitcher->getLinkForLang($lang)?>"><?=strtoupper($lang)?></a></li>
			<? } ?>
    </ul>
		<?

  }

  function SetProperties(){
    $this->properties = Array(

      "name"        => Array(
        "label"     => "Name:",
        "type"      => "str"
      ),

    );

		$this->PostSetProperties(); 

  }

}