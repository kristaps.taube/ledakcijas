<?php
/**
*
*  Title: Password change
*  Author: Kristaps Taube <ktaube@datateks.lv>
*  Date: 02.03.2016
*  Project: E-Veikals
*
*/

require_once("class.component.php");

class EVeikalsPasswordChange extends component{

  function __construct($name){
    parent::__construct($name);
    $this->SetProperties();
  }

  function validate_psw_change($data){

  	$errors = array();

    if($this->shopmanager->user['password']){ // if only user has password

      $salt = substr($this->shopmanager->user['password'], 0, 5);
      $hash = substr($this->shopmanager->user['password'], 5);

      if(sha1($salt.$data['current']) != $hash){
       	$errors[] = $this->l("Nekorekta pašreizējā parole");
      }

    }

    if(!$data['new']){
    	$errors[] = $this->l("Nav ievadīta jaunā parole");
    }elseif($data['new'] != $data['new_again']){
    	$errors[] = $this->l("Jaunās paroles nesakrīt");
    }

		return $errors;

  }

    function process_psw_change($data)
    {

        $errors = $this->validate_psw_change($data);

        if(empty($errors)){

            $usercol = shopusercollection::getInstance();

            $pasw = $usercol->encodePassword($data['new']);
            $_SESSION['logged_in']['password'] = $pasw;
            $usercol->Update(array("password" => $pasw), array("item_id" => $this->shopmanager->user['item_id']));

        }

        return $errors;

    }

    function output()
    {

        if(isset($_POST['change_psw'])){
            $errors = $this->process_psw_change($_POST);
            if(!empty($errors)){
                $msg = "<ul class='errors'><li>".implode("</li><li>", $errors)."</li></ul>";
            }else{
                $msg = "<div class='success'>".$this->l("Parole nomainīta")."</div>";
            }
        }

        ?>
        <form id='PasswordChange' class='padding' method="post" action="?">
            <?=($msg) ? $msg : ''?>
            <table>
                <? if($this->shopmanager->user['password']){ ?>
                <tr>
                    <td><label for="current"><?=$this->l("Pašreizējā parole")?>:</label></td>
                    <td><input class='styled' type='password' name='current' id='current' value='' /></td>
                </tr>
                <? } ?>
                <tr>
                    <td><label for="new"><?=$this->l("Jaunā parole")?>:</label></td>
                    <td><input class='styled' type='password' name='new' value='' id='new' /></td>
                </tr>
                <tr>
                    <td><label for="new_again"><?=$this->l("Jaunā parole atkārtoti")?>:</label></td>
                    <td><input class='styled' type='password' name='new_again' value='' id='new_again' /></td>
                    </tr>
                <tr>
                    <td></td>
                    <td><input type='submit' name='change_psw' class='styled' value='<?=$this->l("Mainīt")?>' /></td>
                    </tr>
            </table>
        </form>
        <?

    }

  function SetProperties(){
    $this->properties = Array(

      "name"        => Array(
        "label"     => "Name:",
        "type"      => "str"
      ),

    );

		$this->PostSetProperties();

  }

}