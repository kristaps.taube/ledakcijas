<?php

use Constructor\WebApp;
use Constructor\Url;

class LEDAkcijasTopMenu extends component
{

    public function output()
    {

        echo $this->render('index', ['menu' => $this->getMenu()]);

    }

    private function getMenu()
    {

        $controller = WebApp::getControllerName();

        return [
            [
                'label' => WebApp::l("Katalogi"),
                'link' => Url::get("catalog"),
                'active' => $controller == 'catalog',
            ],
            /*[
                'label' => WebApp::l("LED kalkulators"),
                'link' => Url::get("calculator"),
                'active' => $controller == 'calculator',
            ],*/
            [
                'label' => WebApp::l("Blogs"),
                'link' => Url::get("blog"),
                'active' => $controller == 'blog',
            ],
            [
                'label' => WebApp::l("BUJ"),
                'link' => Url::get("buj"),
                'active' => $controller == 'faq',
            ],

        ];

    }

}