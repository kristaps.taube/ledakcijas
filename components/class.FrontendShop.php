<?php
/**
*
*  Title: FrontendShop
*  Author: Kristaps Taube <ktaube@datateks.lv>
*  Date: 14.11.2013
*  Project: Constructor
*
*/

include_once("class.component.php");

class FrontendShop extends component{

  private $manager;
  private $product_page_size;
  private $product_page;

  function __construct($name){
    parent::__construct($name);
    $this->SetProperties();
  }

  function execute(){

    $this->initOptions();
    $this->manager = $this->getServiceByName('shopmanager', true);

    if($this->manager){

	    $this->manager->initCollections();
      $this->manager->processShopPath();
      $this->processMetaTags();

    }

    $this->product_page = (isset($_GET['p']) && (int)$_GET['p']> 1)? (int)$_GET['p'] : 1;

  }

  function processMetaTags(){

    if($this->manager->prod){

      $GLOBALS['meta_keywords'] .= $this->manager->prod['keywords_' . $this->lang];
      $GLOBALS['meta_description'] .= strip_tags($this->manager->prod['meta_description_'.$this->lang]);
      $GLOBALS['additional_header_tags'] .= $this->manager->prod['additional_header_tags_'.$this->lang];
      $GLOBALS['page_title'] = $this->manager->prod['name_'.$this->lang];
      $GLOBALS['force_title'] = $this->manager->prod['name_'.$this->lang];

    }elseif($this->manager->cat){

      $GLOBALS['meta_description'] .= strip_tags($this->manager->cat['description_'.$this->lang]);
      $GLOBALS['meta_keywords'] .= $this->manager->cat['keywords_' . $this->lang];
      $GLOBALS['additional_header_tags'] = $this->manager->cat['additional_header_tags_'.$this->lang];
      $GLOBALS['force_title'] = $this->manager->cat['title_'.$this->lang];

    }

  }

  function indexData(){

    $data = array();
    $check = $this->initCollections();

    if($this->name == 'shop'){

      $products = $this->prods->getDBData(array("where" => "disabled = 0"));

      foreach($products as $p){

        $data_text = $p['name_'.$this->lang];
        $data_text .= " ".strip_tags($p['description_'.$this->lang]);
        $data_text .= " ".strip_tags($p['long_description_'.$this->lang]);

        $data[$this->prods->collection_id][$p['item_id']] = array(
          'query' => $this->manager->getProductURL($p, '', true),
          'title' => $p['name_'.$this->lang],
          'data' => $data_text
        );

      }

      $categories = $this->cats->getDBData(array("where" => "disabled = 0"));

      foreach($categories as $c){
        $data[$this->cats->collection_id][$c['item_id']] = array(
          'query' => $this->manager->getCategoryURL($c['item_id'], '', true),
          'title' => $c['title_'.$this->lang],
          'data' => $c['name_'.$this->lang].' '.strip_tags($c['description_'.$this->lang])
        );
      }

    }

    return $data;

  }

  function initOptions(){

    $this->product_page_size = option('Shop\\products_in_page', null, 'Products in page', array('value_on_create' => 2) );

    option("shop\\category_table", $this->cats->table, "Kategoriju tabula", Array("is_advanced" => true), $this->cats->table, $this->site_id);
    option("shop\\product_table", $this->prods->table, "Produktu tabula", Array("is_advanced" => true), $this->prods->table, $this->site_id);
    $path = $this->getProperty("shop_url");
    if($path){
      $page_id = PageIdByPath($path, $this->site_id);
      option("shop\\catalog_page_id_".$this->lang, $page_id, "Kataloga(".$this->lang.") page id", Array("is_advanced" => true), $page_id, $this->site_id);
    }

    $this->cat_output_type = option('Shop\\cat_output_type', null, 'Category output type', array(
      'type' => 'list',
      'value_on_create' => 'categories',
      'fieldparams' => array(
        'lookupassoc' => array(
          'categories' => 'Categories',
          'products' => 'Products'
        )
      )
    ));

  }

  // this function checks if none of the parent categories arent disabled
  function validateShopPath(){

    foreach ($this->manager->cat_path as $cat){
      if ($cat['disabled']){
        header('Location: /');
        die;
      }
    }

  }

  function design(){}

  function output(){

    if(!$this->manager){
      echo $this->name.": Can't get manager. Where is he?<br />";
      return;
    }

    $this->validateShopPath();

    if($this->manager->prod && !$this->manager->prod['disabled']){

      $this->outputProduct($this->manager->prod);

    }elseif($this->manager->cat && $this->cat_output_type == 'categories'){

      $this->outputSubCategories($this->manager->cat);

    }elseif($this->manager->cat && $this->cat_output_type == 'products'){

      $this->outputProductList($this->manager->cat);

    }

  }

  function outputProduct($prod){

    /** HERE YOU SHOULD OUTPUT SINGLE PRODUCT **/

  }

  function outputSubCategories($cat){

    /** HERE YOU SHOULD PUT CATEGORY LIST OUTPUT **/
    $sub_categories = $this->cats->getDBData(array("where" => "disabled = 0 AND parent = ".$cat['item_id'], "order" => "ind", "assoc" => true));

    if(!$sub_categories === false || empty($sub_categories)){
      $this->outputProductList($cat);
      return;
    }

    foreach($sub_categories as $cat){

    }

  }

  function outputProductList($cat){

    $cats = $this->cats->getActivePredecessorIds($cat['item_id']);

    /** HERE YOU SHOULD PUT PRODUCT LIST OUTPUT **/
    $prods = $this->getProducts($cats);

    foreach($prods as $prod){

    }

    if($this->product_pages > 1){
      $this->outputPagination();
    }

  }

  // $cat should be integer or array of integers
  function getProducts($cat){

    $cond = array("disabled = 0");

    if(is_array($cat)){
      $cond[] = "category_id IN (".implode(", ", $cat).")";
    }elseif($cat){
      $cond[] = "category_id = ".$cat['item_id'];
    }

    $prods = $this->prods->getDBData(array(
      "where" => implode(" AND ", $cond),
      "order" => "cat_ind",
      "offset" => $this->product_page_size * ($this->product_page - 1),
      "count" => $this->product_page_size,
      "assoc" => true
    ));

    $total_products = sqlQueryValue("SELECT COUNT(*) FROM `".$this->prods->table."` WHERE ".implode(" AND ", $cond));
    $this->product_pages = ceil($total_products / $this->product_page_size);

    return $prods;

  }

  function outputPagination($count = false, $active = false){

    $count = $count ? $count : $this->product_pages;
    $active = $active ? $active : $this->product_page;

    for($i = 1; $i <= $count; $i++){
    ?>
      <a href='?p=<?=($i)?>' class='<?=($i == $active)? "active" : ""?>'><?=$i?></a>
    <?
    }

  }


  function SetProperties(){
    $this->properties = Array(

      "name"        => Array(
        "label"     => "Name:",
        "type"      => "str"
      ),

      "cats" => Array(
        "label"       => "Category collection:",
        "type"        => "collection",
        "collectiontype" => "shopcatcollection",
        "lookup"      => GetCollections($this->site_id, "shopcatcollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "prods" => Array(
        "label"       => "Product collection:",
        "type"        => "collection",
        "collectiontype" => "shopprodcollection",
        "lookup"      => GetCollections($this->site_id, "shopprodcollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "users" => Array(
        "label"       => "User collection:",
        "type"        => "collection",
        "collectiontype" => "shopusercollection",
        "lookup"      => GetCollections($this->site_id, "shopusercollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "units" => Array(
        "label"       => "Units collection:",
        "type"        => "collection",
        "collectiontype" => "shopunitscollection",
        "lookup"      => GetCollections($this->site_id, "shopunitscollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "prod_images" => Array(
        "label"       => "Product images collection:",
        "type"        => "collection",
        "collectiontype" => "shopcustomimagescollection",
        "lookup"      => GetCollections($this->site_id, "shopcustomimagescollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "related_prods" => Array(
        "label"       => "Related products collection:",
        "type"        => "collection",
        "collectiontype" => "shoprelatedprodscollection",
        "lookup"      => GetCollections($this->site_id, "shoprelatedprodscollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "brandscol" => Array(
        "label"       => "Brands collection:",
        "type"        => "collection",
        "collectiontype" => "shopbrandscollection",
        "lookup"      => GetCollections($this->site_id, "shopbrandscollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "prices" => Array(
        "label"       => "Prices collection:",
        "type"        => "collection",
        "collectiontype" => "shoppricescollection",
        "lookup"      => GetCollections($this->site_id, "shoppricescollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      'shop_url' => Array(
        'label'     => 'Kataloga lapa:',
        'type'      => 'dialog',
        'dialog'    => '?module=dialogs&action=links&site_id='.$this->site_id,
      ),

    );
		
  }

}

