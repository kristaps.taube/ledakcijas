<?php
/**
*
*  Title: [EVeikals piedavajumi]
*  Author: Jānis Polis <j.polis@datateks.lv>
*  Date: [29.07.2016]
*  Project:  E-Veikals
*
*/

include_once("class.component.php");

class EVeikalsPiedavajumi extends component{

  function __construct($name){
    parent::__construct($name);
    $this->SetProperties();
  }

  function initOptions(){

    if ($this->opts_initialized) return;

    option('Shop\\OrderForm', '', 'Order form');
    $optcat = getOptionByPath('Shop\\OrderForm');
    $params = Array('parent_id' => $optcat['id']);

    $this->use_user_offers = option('use_user_offers', null, 'Lietot lietotāju piedāvājumus', array_merge($params, Array('type' => 'check','is_advanced' => true)), null);

		
    $this->opts_initialized = true;
  }
	
	function EditOffer($id){
		$offer = $this->piedavajumi->GetRow(array('where'=>'item_id='.$id));
		?>
		<div class="padding offer editoffer" id="BigCart">
			<input style="display:none;float: left;" class="titlehold" type="text" id="offertitleinput" offerid="<?=$id?>" value="<?=$offer['name']?>" />
			<h2 class="titlehold" id="offertitle"><?=$offer['name']?></h2>
			<a href="#" id="edittitle" class="edit"></a>
			<div class="cl"></div>
			<p class="date"><?=$offer['datums']?></p>
			 <div class="offerlinks">
				<a href="#offerform" itemid="<?=$of['item_id']?>" class="sendmail popupf"></a>
				<a href="?pdf=<?=$offer['item_id']?>" target="_blank" class="openpdf"></a>
				<a href="?pdf=<?=$offer['item_id']?>&print=<?=$offer['item_id']?>" target="_blank" class="print"></a>
			</div>
			<table id="single_offer_table">
       	<thead>
					<tr>
						<th>&nbsp;</th>
						<th><?=$this->l('Nr')?></th>
						<th><?=$this->l('Artikuls')?></th>
						<th>&nbsp;</th>
						<th><?=$this->l('Nosaukums')?></th>
						<th><?=$this->l('Noliktavā')?></th>
						<th><?=$this->l('Daudzums')?></th>
						<th><?=$this->l('Cena')?></th>					
						<th><?=$this->l('Mana cena')?></th>
						<th>&nbsp;</th>
					</tr>
				</thead>
				<?$a=1;
				$prods = $this->artikuli->GetTable(array('where' => 'piedavajums='.$id));				
				
				foreach($prods as $prod){
					$proddata = $this->manager->prodscol->GetRow(array('where'=>'item_id="'.$prod['prod_id'].'"'));				
					$featured = $this->manager->isProductFeatured($proddata);
					$new = $this->manager->isProductNew($proddata);
					$top = $this->manager->isProductTop($proddata);
					$image = $this->manager->getProductPicture($proddata);
					$image = $image? $image : $this->shop->default_product_image;
					$can_add = $this->manager->canAddProductFromList($proddata);
					$has_discount = ($featured && $proddata['sale_price'] && $proddata['sale_price'] != '0.00' && $proddata['sale_price'] != $proddata['price'])? true : false;
					$price = ($has_discount)? $proddata['sale_price']  : $proddata['price'];
					$discounted_price = $proddata['price'];

					if($proddata['has_prices']){
						$prices = $this->shop->prices->getPricesByProduct($proddata['item_id']);
						if($prices){
							$min_price_row = array();
							foreach($prices as $price_row){

								$has_discount = ($featured && $price_row['saleout_price'] && $price_row['saleout_price'] != '0.00' && $price_row['saleout_price'] != $price_row['price'])? true : false;
								$price = ($has_discount)? $price_row['saleout_price']  : $price_row['price'];
								$price_row['final_price'] = $price;

								if(empty($min_price_row) || $min_price_row['final_price'] > $price_row['final_price']){
									$min_price_row = $price_row;
								}

							}

							$price = $min_price_row['final_price'];
							$discounted_price = $min_price_row['price'];

						}
					}	
				?>
					<tr>
						<td><input type="checkbox" id="row<?=$prod['item_id']?>" name="row[]" value="<?=$prod['item_id']?>" /><label for="row<?=$prod['item_id']?>"></label></td>
						<td><?=$a?>.</td>
						<td><?=$proddata['code']?></td>
						<td><img src="<?=getThumbUrl($image,30,50,6)?>" alt="" /></td>
						<td><?=$proddata['name_'.$this->lang]?></td>
						<td><?=$proddata['prod_count']?></td>
						<td style="text-align:center;"><input type="text" class="editcount" priceid="<?=$prod['item_id']?>" name="count[<?=$prod['item_id']?>]" value="<?=(!empty($prod['count']) ? $prod['count'] : '')?>" /></td>
						<td><?=number_format($price / $_SESSION['currency']['rate'], 2)?> <?=$_SESSION['currency']['label']?></td>
						<td><input type="text" class="editprice" priceid="<?=$prod['item_id']?>" name="custom_price[<?=$prod['item_id']?>]" value="<?=(!empty($prod['custom_price']) ? number_format($prod['custom_price']/ $_SESSION['currency']['rate'], 2) : '')?>" /> <?=$_SESSION['currency']['label']?></td>
						<td class="offerlinks">
							<a href="?artdel=<?=$prod['item_id']?>&off=<?=$id?>" onclick="return confirm('<?=hsc($this->l('Are you sure you want to delete?'))?>')" class="delete"></a>
						</td>
					</tr>
				<?$a++;}
				$a=1;
				?>					
				</table>							
				<a href="?buy=<?=$id?>" id="buyoffer" offerid="<?=$id?>" class="button left"><?=$this->l('Pirkt')?></a>
				<div id="offerdescriptiondiv">
					<label for="offerdescription"><?=$this->l('Piedāvājuma apraksts')?>:</label>
					<textarea name="offerdescription" descid="<?=$id?>" id="offerdescription"><?=$offer['desc']?></textarea>
					<a href="#" id="adddesc" class="button right"><?=$this->l('Pievienot Aprakstu')?></a>
				</div>
			</div>
			<div style="display:none;">
				<div id="offerform">
						<div class="popform">
								<div class="popcontent">
										<div class="content">												
												<h2><?=$this->l('Nosūtīšana')?></h2>												
												<form action="" class="offerform" method="post">
														<div class="cl"></div>
														<label for="offermails"><?=$this->l('Ievadiet saņēmēju epasta adreses, atdalot ar komatu.')?></label>
														<textarea required id="offermails" name="offermails"></textarea>
														<br />
														<br />
														<label for="subject"><?=$this->l('Ziņas temats')?></label>
														<input type="text" id="subject" name="subject"/>
														<br />	
														<br />														
														<label for="offermails"><?=$this->l('Ziņas teksts')?></label>
														<textarea id="mailtext" name="mailtext"></textarea>																
														<br />
														<br />		
														<span id="offerresponse"></span>														
														<input type="submit" value="<?=$this->l('Nosūtīt')?>" class="button" />
														<br />
												</form>
										</div>
								</div>
						</div>						
				</div>
			</div>
							<? /*if(!empty($offer['desc'])){?>
								<script>
									$(document).ready(function(){
										$('#adddesc').click();
									});
								</script>
							<? }*/ ?>
		<?
	}
	
	function execute(){	
	
		if(is_numeric($_POST['priceid']) AND $_POST['price']){
			$update = $this->artikuli->Update(array('custom_price' => $_POST['price']),$_POST['priceid']);
			if($update)
				die(number_format($_POST['price'],2));
			else
				die('Problem');			
		}
		if(is_numeric($_POST['priceid']) AND $_POST['count']){
			$update = $this->artikuli->Update(array('count' => $_POST['count']),$_POST['priceid']);
			if($update)
				die($_POST['count']);
			else
				die('Problem');			
		}
		if(is_numeric($_POST['descid']) AND $_POST['desc']){
			$update = $this->piedavajumi->Update(array('desc' => $_POST['desc']),$_POST['descid']);
			if($update)
				die('<div class="success">'.$this->l('Apraksts saglabāts!').'</div>');
			 else
				die('Problem');			
		}
		if(is_numeric($_POST['offerid']) AND $_POST['offertitle']){
			$update = $this->piedavajumi->Update(array('name' => $_POST['offertitle']),$_POST['offerid']);
			if($update)
				die($_POST['offertitle']);
			else
				die('Problem');			
		}
		if($_GET['create_new_offer'] AND $_SESSION['cartitems'] && count($_SESSION['cartitems'])){
			$this->manager = $this->getServiceByName('shopmanager', true);
		$this->shop = $this->getServiceByName('FrontendShop', true);
		if(!$this->manager)
			return;
			$offerid = $this->piedavajumi->Insert(array('name' => 'New offer ','datums' => date('d.m.Y'), 'user'=> $_SESSION['logged_in']['item_id']));				
			if($offerid){
				$this->piedavajumi->Update(array('name' => 'New offer '.$offerid),$offerid);
				$tmp = array();
				foreach($_SESSION['cartitems'] as $p){
					$tmp[] = $p['id'];
					echo $p['product']['code'].'<br />';
				}
				
				$prods = $this->manager->prodscol->GetTable(array('what' => 'item_id,code','where' => 'item_id IN('.implode(',',$tmp).')'));
				$properties = array();
				foreach($prods as $k => $prod){
					$properties[$prod['item_id']] = $prod['item_id'];				
				}
				foreach($_SESSION['cartitems'] as $p){
					if($properties[$p['id']]){
						$properties[$p['id']]['count'] = $p['count'];
						if($p['price_id'])
							$properties[$p['id']]['price_id'] = $p['price_id'];
					}	
				}
				
				foreach($properties as $k => $prop){
					$this->artikuli->Insert(array(
						'prod_id' => $k,	
						'price_id' => $prop['price_id'] ? $prop['price_id'] : 0, 	
						'count' => $prop['count'],
						'piedavajums' => $offerid
					));	
				}
			}
			else
				echo 'Houston we have a problem..';
			unset($_SESSION['cartitems']);
			die(header('Location: ?'));
		}
	}
	
	
	
	
  function check_email($x) {
		 if (strpos($x, "@") !== false) {
			$split = explode("@", $x);
			if (strpos($split['1'], ".") !== false) 
				return true; 
			else
				return false; 
		 }
		 else 
			return false; 

	}
	
	function sendEmail($mails, $id, $my_email, $subject, $mailtext , $my_name){
					
				//$email = $data['email'];
				$subject = $subject ? $subject : $this->l('Piedāvājums');
				$mailtext = $mailtext ? $mailtext : '';
				$encoding = 'UTF-8';						
				require_once($GLOBALS['cfgDirRoot']."library/"."class.phpmailer.php");
				$mail = new PHPMailer();
				$mail->IsMail();
				$mail->From     = $my_email;
				$mail->FromName = $my_name ? $my_name : $my_email;
				$emails = explode(' ',$data['emails']);
				$a = 0;
				$mail->AddAddress($my_email);
				foreach($mails as $m){					
					$mail->AddBCC(trim($m));							
				}
				//$mail->AddAddress($email);
				$mail->Sender = $my_email;//new fix
				$mail->AddReplyTo($my_email);//new fix
				$url = 'http://'.$GLOBALS['cfgDomain'].$_SERVER['SCRIPT_URL'].'?pdf='.$id.'&magic=1';		
				
				$binary_content = file_get_contents($url);
				
				if($binary_content)
					$mail->AddStringAttachment($binary_content, "piedavajums_".$id.".pdf", 'base64', 'application/pdf');
				
				$mail->WordWrap = 70;
				$mail->CharSet = $encoding;
				$mail->IsHTML(true);
				$mail->Subject  =  $subject ? $subject : $this->l('Piedāvājums');
				$mail->Body     =  $mailtext ? $mailtext : $this->l('Piedāvājums');

				$mailtext = str_replace("<p>","<p>\n",$mailtext);
				$mailtext = str_replace("<P>","<P>\n",$mailtext);
				$mailtext = str_replace("<br>","\n",$mailtext);
				$mailtext = str_replace("<br />","\n",$mailtext);
				$mailtext = str_replace("<BR>","\n",$mailtext);
				$mailtext = str_replace("<BR />","\n",$mailtext);
				$mailtext = str_replace("&nbsp;"," ",$mailtext);
				$mailtext = str_replace("</tr>","\n",$mailtext);
				$mail->AltBody  =  strip_tags($mailtext);
				//dump($mail);die;
				if (!$mail->Send())
				{
					$msg = 'Order send to '.$email.' failed: '.$mail->ErrorInfo;
					add_log($msg, $this->site_id);
					$response = $this->l('Radusies kļūme ar e-pastu nosūtīšanu!');					
				}
				else{										
					$response = $this->l('E-pasts nosūtīts veiksmīgi!');							
				}			
				die($response);
	}
	
	
	
	function output(){
		$this->initOptions();
		if(!$this->use_user_offers)
			return;
		   
		$this->manager = $this->getServiceByName('shopmanager', true);
		$this->shop = $this->getServiceByName('FrontendShop', true);
		if(!$this->manager OR !$this->shop)
			return;
		if($_POST['offerdata'] AND is_numeric($_POST['id'])){
			$s = unserialize($_SESSION['logged_in']['savedfields']);			
			$epasts = $s['j_epasts'];	
			$my_name = $s['j_kontaktpersona'];	
			$subject = $_POST['subject'];
			$mailtext = $_POST['mailtext'];	
			
			if(!$this->check_email($epasts))
				die($this->l('Jūsu rekvizītos norādītā epasta adrese nav korekta!').' '.$epasts);
			$mails = explode(',',$_POST['offerdata']);
			$mail_arr = array();
			$notmail_arr = array();
			foreach($mails as $m){
				if($this->check_email($m))
				{
					$mail_arr[] = trim($m);
				}
				else
				{
					$notmail_arr[] = trim($m);
				}
			}			
			$response = array();
			if(!empty($notmail_arr))		
				$response['badmail'] = $notmail_arr; 
			if(!empty($mail_arr))				
				$response['success'] = $this->sendEmail($mail_arr, $_POST['id'], $epasts, $subject, $mailtext , $my_name);			
			$response = json_encode($response);
			unset($_POST);
			die($response);
		}
		if(is_numeric($_GET['pdf'])){
			$id = $_GET['pdf'];
			if(is_numeric($_GET['magic']) AND ($_GET['magic'] == 1)){				
				$offer = $this->piedavajumi->GetRow(array('where' => 'item_id = :id','params' => array(':id' => $id)));
				$prods = $this->artikuli->GetTable(array('where' => 'piedavajums= :pid', 'params' => array(':pid'=>$id)));
				return $this->outputPDF($offer, $prods);
			}else{
				$check = $this->piedavajumi->GetValue(array('what' => 'item_id','where' => 'item_id = :id AND user = :uid',	'params' => array(':id' => $id, ':uid' => $_SESSION['logged_in']['item_id'])));
				if($check){
					$offer = $this->piedavajumi->GetRow(array('where' => 'item_id = :id','params'=>array(':id'=>$id)));
					$prods = $this->artikuli->GetTable(array('where' => 'piedavajums = :pid','params'=>array(':pid'=>$id)));
					return $this->outputPDF($offer, $prods);
				}	
			}
			
			
		}
		
		
		if(is_numeric($_GET['del'])){
			$id = $_GET['del'];
			$check = $this->piedavajumi->GetValue(array('what' => 'item_id','where' => 'item_id = :id AND user = :uid','params' => array(':id'=>$id,':uid'=>$_SESSION['logged_in']['item_id'])));
			if($check){
				$this->piedavajumi->Delete(array('item_id'=>$id));
				$this->artikuli->Delete(array('piedavajums'=>$id));
			}	
			die(header('Location: ?'));
		}
		if(is_numeric($_GET['artdel']) AND is_numeric($_GET['off'])){
			$id = $_GET['off'];
			$del = $_GET['artdel'];
			$check = $this->artikuli->GetValue(array('what' => 'item_id','where' => 'piedavajums = :pid AND item_id = :id','params'=>array(':pid'=>$id,':id'=>$del)));
			if($check)			
				$this->artikuli->Delete(array('item_id'=>$del));			
			die(header('Location: ?edit='.$id));
		}
		if(is_numeric($_GET['edit'])){
			$id = $_GET['edit'];			
			$check = $this->piedavajumi->GetValue(array('what' => 'item_id','where' => 'item_id = :id AND user = :uid',	'params' => array(':id' => $id, ':uid' => $_SESSION['logged_in']['item_id'])));
			if($check)
				return $this->EditOffer($id);
		}
		if(is_numeric($_GET['buy']) OR (is_numeric($_POST['buy']) AND $_POST['prods'])){
			$id = $_GET['buy'] ? $_GET['buy'] : $_POST['buy'];
			$check = $this->piedavajumi->GetValue(array('what' => 'item_id','where' => 'item_id = :id AND user = :uid',	'params' => array(':id' => $id, ':uid' => $_SESSION['logged_in']['item_id'])));
			if($check){
				if($_POST['prods'])
					$prods = $this->artikuli->GetTable(array('where' => 'piedavajums = :id AND item_id IN(:prods)', 'params' => array(':id'=>$id,':prods'=>$_POST['prods'])));	
				else
					$prods = $this->artikuli->GetTable(array('where' => 'piedavajums = :id','params' => array(':id' => $id)));				
				foreach($prods as $prod){
					$item = Array('id' => $prod['prod_id']);
					if($prod['price_id'])
						$item['price_id'] = $prod['price_id'];
					$count = $prod['count'] ? $prod['count'] : 1;//maybe smthng else in meantime..
					$this->smallcart->addCartItem($item, $count);					
				}
				if($_POST['prods']){					
					die($this->smallcart->getProperty("orderform_url"));
				}	
				else
					die(header('Location:'.$this->smallcart->getProperty("orderform_url")));
					//die(header('Location: ?edit='.$id));
			}
		}
		//sum(distinct m.count)
		if($_SESSION['logged_in']['item_id'])
		$myoffers = DB::GetTable("SELECT p.*,sum(m.count) as ProdCount FROM ".$this->piedavajumi->table." p
		LEFT JOIN ".$this->artikuli->table." m	
		ON p.item_id = m.piedavajums
		WHERE user = ".$_SESSION['logged_in']['item_id']."
		group by p.item_id
		");
		
		//$this->piedavajumi->GetTable(array('where' => 'user = '.$_SESSION['logged_in']['item_id']));
		?>
		<div class="padding offer" id="BigCart">
			<? if($_SESSION['cartitems'] && count($_SESSION['cartitems'])){?>
			<a href="?create_new_offer=1" class="button right"/><?=$this->l('Izveidot jaunu piedāvājumu')?></a>
			<?}else{
			echo $this->l('Lai veidotu jaunu piedāvājumu nepieciešams ievietot grozā preces.').'<br /><br />';					
			}
			if($myoffers){
			?>
			<table id="offer_table">
       	<thead>
				<tr>
				  <th><?=$this->l('Datums')?></th>
          <th><?=$this->l('Nosaukums')?></th>
          <th><?=$this->l('Produktu skaits')?></th>
          <th>&nbsp;</th>				
				</tr>
				</thead>
				<tbody>
				<?foreach($myoffers as $of){?>
				<tr>
          <td><?=$of['datums']?></td>
					<td>
						<a href="?edit=<?=$of['item_id']?>"><?=$of['name']?></a>
					</td>
					<td><?=$of['ProdCount']?></td>
          <td class="offerlinks">
						<a href="#offerform" itemid="<?=$of['item_id']?>" class="sendmail popupf"></a> 
						<a href="?pdf=<?=$of['item_id']?>" target="_blank" class="openpdf"></a>
						<a href="?pdf=<?=$of['item_id']?>&print=<?=$of['item_id']?>" target="_blank" class="print"></a>
						<a href="?del=<?=$of['item_id']?>" class="delete" onclick="return confirm('<?=hsc($this->l('Are you sure you want to delete?'))?>')"></a>
					</td>
				</tr>
				<? } ?>	
				</tbody>
      </table>	
	<? } ?>			
		</div>
	
			<div style="display:none;">
				<div id="offerform">
						<div class="popform">
								<div class="popcontent">
										<div class="content">												
												<h2><?=$this->l('Nosūtīšana')?></h2>												
												<form action="" class="offerform" method="post">
														<div class="cl"></div>
														<label for="offermails"><?=$this->l('Ievadiet saņēmēju epasta adreses, atdalot ar komatu.')?></label>
														<textarea required id="offermails" name="offermails"></textarea>
														<br />
														<br />
														<label for="subject"><?=$this->l('Ziņas temats')?></label>
														<input type="text" id="subject" name="subject"/>
														<br />	
														<br />														
														<label for="offermails"><?=$this->l('Ziņas teksts')?></label>
														<textarea id="mailtext" name="mailtext"></textarea>																
														<br />
														<br />		
														<span id="offerresponse"></span>														
														<input type="submit" value="<?=$this->l('Nosūtīt')?>" class="button" />
														<br />
												</form>
										</div>
								</div>
						</div>						
				</div>
			</div>
		<?
		
  }

  	function outputPDF($offer, $prods){
		
		ob_start();	

			$sum = 0;
			//$months = explode('.',$this->LS('menesu_nosaukumi'));			
			//$username = $this->users->GetValue(array('what' => 'name','where' => 'item_id='.$offer['user']));
			
			?>
		<html>
		<head>
			<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700&subset=latin,cyrillic-ext,latin-ext,cyrillic' rel='stylesheet' type='text/css'>
		<style type="text/css">
				@page {
    		margin-top: 0px;
    		margin-left: 0px;
    		margin-right: 0px;
	 			margin-header: 25px;
	 			margin-footer: 0px;
	  		margin-bottom: 130px;				
				odd-footer-name: html_myFooter;
  			even-footer-name: html_myFooter;				
    	}

			@page htmlpagefooter{
				margin: 0px;
			}
				body{font-size:11px;font-family: 'Open Sans', sans-serif; 	}
				/*table,table tr,table td{border:none;}*/
				#ProdTable{
					width: 100%;					
					border-collapse: collapse;
					
					/*border-bottom:1px solid #989898;*/
					font-size:11px;
				}
				#ProdTable tr:last-child td{
					border-bottom:1px solid #233e6d;
				}				
				#ProdTable td{
					border:none;
					padding:8px 11px;
					border-bottom:1px solid #989898;					
				}
				#ProdTable tr.noborder td{border:none;}
				#ProdTable td img{
					vertical-align:middle;
				}
				#ProdTable th{
					border-top:1px solid #233e6d;
					border-bottom:1px solid #233e6d;
					font-weight:normal;					
				}	
				.blackline,.grayline{width:100%;height:6px;}
				.blackline{background:#233e6d;}
				.grayline{background:#e6e6e6;}
				.pad{padding-left:39px;padding-right:39px;padding-top:22px;}
				.textleft{text-align:left;}
				.imghold{width:30%;float:left;				
				}
				.imghold img{
					vertical-align:middle;
					
				}
				.compdesc{width:70%;float:left;}
				.cl{clear:both;}
				.bordered{
					border-top:1px solid #ccc;
					border-bottom:1px solid #ccc;
					padding-top:3px;
					padding-bottom:3px;
					text-transform:uppercase;
				}
				h1.upper{font-weight:normal;font-family: 'Palatino Linotype',sans-serif;font-size:18px;}
				.upper{text-transform:uppercase;}
				.bottomborder{
				border-bottom:1px solid #233e6d;			
				}	
				.col{
					width:50%;
					float:left;
				}	
				.black{
					background:#233e6d;
					color:#fff;
					width:100%;					
				}
				.gray{
					background:#e6e6e6;
				}				
				.right{width:100%;text-align:right;margin-top:-16px;}
				.left{width:80%;margin-top:0px;}
				/*
				.credentials .pad{
					border-top:1px solid #ccc;
				}*/		
				</style>
			</head>
			<body>
			<?
			$date = $offer['datums'];
			// $date = explode('-',$date);
			// $year = $date[0];
			// $month = $date[1];
			// $day = explode(' ',$date[2]);
			// $day = $day[0];
			// $date = $day.'.'.$month.'.'.$year.'.';
						
			
?>
<?
			
				$user = $this->users->GetRow(array('where'=>'item_id='.$offer['user']));		
				$userdata = unserialize($user['savedfields']);	
				$logo = $user['logoimage'];	
			?>
			<htmlpagefooter name="myFooter" style="display:none;margin: 0px;padding: 0px;" id='Footer'>
    	<div class="footer">				
				<div class="black upper">					
					<div class="pad">
						<div class="left"><?echo $this->l("Piedāvājumu izveidoja:").' '.$userdata['j_kontaktpersona'].', '.$date.', '.$this->l('tas ir spēkā 10 dienas.')?></div>
						<div class="right">{PAGENO}</div>					
					</div>
					<br />
				</div>
				</div>
		</htmlpagefooter>
			<div class="blackline"></div>
			<div class="grayline"></div>
      <div class="pad">
			
			<? if($logo){?>
			<table><?/*autosize="1" style="page-break-inside:avoid"*/?>
				<tr>
					<td width="40%"><div class="imghold">
        <?=($logo ? '<img src="http://'.$GLOBALS['cfgDomain'].$logo.'" alt="">' : '')?>
			</div></td>
					<td width="60%"><div class="textleft compdesc">
				<b><?=$userdata['j_nosaukums']?></b>
				<p><?=$userdata['company_description']?></p>
			</div></td>
				</tr>
			</table>
			<? } ?>
			<br />
			
      <div class="cl"></div>
			
      <div class="bordered">
      <?=$this->l('Visas cenas norādītas EUR, cenā iekļauts 21% pvn')?>
      </div>
			<?/*
			<h1 class="upper"><?=$this->l('Piedāvājums Nr.').' '.$order['item_id']?></h1>
			*/?>
			<?$ordername = $offer['name'];?>
			<h1 class="upper"><?=$ordername?></h1>			
					<?
					$this->orderform->initOptions();
					$this->pvn = option('Shop\\pvn', null, 'PVN (%)', null, 22) / 100;
					

    $summa_price_sum = 0;
    $summa_atlaides_sum = 0;
    $summa_pvn_sum = 0;
    $summa_price_sum_ar_pvn = 0;

    $a=1;
		$prods = $this->artikuli->GetTable(array('where' => 'piedavajums='.$offer['item_id']));				
			?>
		<table autosize="1" width="100%">	
		<tr>
		<th><?=$this->l('Nr.p.k.')?></th>
		<th></th>
		<th><?=$this->l('Artikuls')?></th>		
		<th><?=$this->l('Nosaukums')?></th>
		<th><?=$this->l('Daudzums')?></th>
		<th><?=$this->l('Cena')?></th>
		<th><?=$this->l('Kopā')?></th>
		</tr>
			<?	
		foreach($prods as $prod){
			$proddata = $this->manager->prodscol->GetRow(array('where'=>'item_id="'.$prod['prod_id'].'"'));				
			$featured = $this->manager->isProductFeatured($proddata);
			$new = $this->manager->isProductNew($proddata);
			$top = $this->manager->isProductTop($proddata);
			$image = $this->manager->getProductPicture($proddata);
			$image = $image? $image : $this->shop->default_product_image;
			$image = getThumbUrl($image,30,50,6);
			$can_add = $this->manager->canAddProductFromList($proddata);
			$has_discount = ($featured && $proddata['sale_price'] && $proddata['sale_price'] != '0.00' && $proddata['sale_price'] != $proddata['price'])? true : false;
			$price = ($has_discount)? $proddata['sale_price']  : $proddata['price'];
			$discounted_price = $proddata['price'];

			if($proddata['has_prices']){
				$prices = $this->shop->prices->getPricesByProduct($proddata['item_id']);
				if($prices){
					$min_price_row = array();
					foreach($prices as $price_row){

						$has_discount = ($featured && $price_row['saleout_price'] && $price_row['saleout_price'] != '0.00' && $price_row['saleout_price'] != $price_row['price'])? true : false;
						$price = ($has_discount)? $price_row['saleout_price']  : $price_row['price'];
						$price_row['final_price'] = $price;

						if(empty($min_price_row) || $min_price_row['final_price'] > $price_row['final_price']){
							$min_price_row = $price_row;
						}

					}

					$price = $min_price_row['final_price'];
					$discounted_price = $min_price_row['price'];

				}
			}	
			
		?>
				<tr>          
					
					<td align="center"><?=$a?>.</td>
					<td><img src="<?='http://'.$GLOBALS['cfgDomain'].$image?>" alt="" /></td>
					<td align="center"><?=$proddata['code']?></td>					
					<td align="center"><?=$proddata['name_'.$this->lang]?></td>
					<td align="center"><?=$prod['count']?></td>
					<td align="right"><?=number_format((((!empty($prod['custom_price']) AND $prod['custom_price'] != '0.00') ? $prod['custom_price'] : $price)) / $_SESSION['currency']['rate'], 2)?> <?=$_SESSION['currency']['label']?></td>
					<td align="right"><?=number_format(((((!empty($prod['custom_price']) AND $prod['custom_price'] != '0.00') ? $prod['custom_price'] : $price)) * $prod['count']) / $_SESSION['currency']['rate'], 2)?> <?=$_SESSION['currency']['label']?></td>
					
				</tr>
				<?
				$price_sum += (((((!empty($prod['custom_price']) AND $prod['custom_price'] != '0.00') ? $prod['custom_price'] : $price)) * $prod['count']) / $_SESSION['currency']['rate']);
				$pvn = (float)$proddata['pvn'] ? (float)$proddata['pvn'] : ($this->pvn * 100);
				$price_sum_ar_pvn = $price_sum;
				$pvn_sum = $price_sum_ar_pvn - $price_sum_ar_pvn / (1 + ($pvn / 100));				
				$a++;} 
		?>
		<tr>
			<td colspan="7"><hr style="margin-top:-1px;" /></td>
		</tr>
		<tr>
			<td colspan="5">&nbsp;</td>
			<td><?=$this->l('Kopā pvn')?></td>
			<td align="right"><?=number_format($pvn_sum,2)?> <?=$_SESSION['currency']['label']?></td>
		</tr>
		<tr>
			<td colspan="5">&nbsp;</td>
			<td><?=$this->l('Kopā bez pvn')?></td>
			<td align="right"><?=number_format(($price_sum - $pvn_sum),2)?> <?=$_SESSION['currency']['label']?></td>
		</tr>
		<tr>
			<td colspan="5">&nbsp;</td>
			<td><?=$this->l('Kopā ar pvn')?></td>
			<td align="right"><?=number_format($price_sum,2)?> <?=$_SESSION['currency']['label']?></td>
		</tr>
		</table>
		
	
 
	
				
				<hr style="margin-top:-1px;" />
				
			<table>
      <?				
				
			
			
			if($offer['desc']){ ?>
			<tr>
				<td style="padding-left:0;" colspan="7">
        <strong><?=$this->l('Pievērsiet uzmanību')?> :</strong><br /> <?=$offer['desc']?>
				</td>
			</tr>
			<? }?>
      </table>
			
			
			<div class="credentials">
					<div class="pad">		
					<? if($userdata['j_epasts'] OR $userdata['j_kontaktpersona_tel'] OR $userdata['company_fax'] OR $userdata['company_address'] OR $userdata['company_worktime']){ ?>		
					<div class="col">
						<h2 class="upper"><?=$this->l('Sazinies ar mums')?></h2>
						<? if($userdata['j_kontaktpersona']){ ?>
						<strong><?=$this->l('Kontaktpersona')?>:</strong> <?=$userdata['j_kontaktpersona']?><br />
						<? }?>
						<? if($userdata['j_epasts']){ ?>
						<strong><?=$this->l('E-pasts')?>:</strong> <?=$userdata['j_epasts']?><br />
						<? }?>
						<? if($userdata['j_kontaktpersona_tel']){ ?>
						<strong><?=$this->l('Tel')?>:</strong> <?=$userdata['j_kontaktpersona_tel']?><br />
						<? }?>
						<? if($userdata['company_fax']){ ?>
						<strong><?=$this->l('Fakss')?>:</strong> <?=$userdata['company_fax']?><br />
						<? }?>						
						<? if($userdata['company_address']){ ?>
						<strong><?=$this->l('Adrese')?>:</strong> <?=$userdata['company_address']?><br />
						<? }?>
						<? if($userdata['company_worktime']){ ?>
						<strong><?=$this->l('Darba laiks')?>:</strong> <?=$userdata['company_worktime']?><br />
						<? }?>
					</div>	
					<? }?>
					<? if($userdata['j_nosaukums'] OR $userdata['jur_address'] OR $userdata['j_regnr'] OR $userdata['j_bank'] OR $userdata['j_bank_acc']){ ?>
					<div class="col">
						<h2 class="upper"><?=$this->l('Rekvizīti')?></h2>
						<? if($userdata['j_nosaukums']){ ?>
						<?=$userdata['j_nosaukums']?><br />
						<? }?>
						<? if($userdata['jur_address']){ ?>
						<strong><?=$this->l('Juridiskā adrese')?>:</strong> <?=$userdata['jur_address']?><br />
						<? }?>						
						<? if($userdata['j_regnr']){ ?>
						<strong><?=$this->l('Reģistrācijs numurs')?>:</strong> <?=$userdata['j_regnr']?><br />
						<? }?>
						<? if($userdata['j_pvnregnr']){ ?>
						<strong><?=$this->l('PVN maksātāja numurs')?>:</strong> <?=$userdata['j_pvnregnr']?><br />
						<? }?>						
						<? if($userdata['j_bank']){ ?>
						<strong><?=$this->l('Banka')?>:</strong> <?=$userdata['j_bank']?><br />
						<? }?>
						<? if($userdata['j_bank_acc']){ ?>
						<strong><?=$this->l('Bankas kods')?>:</strong> <?=$userdata['j_bank_acc']?><br />
						<? }?>						
						<? if($userdata['j_bank_code']){ ?>
						<strong><?=$this->l('Bankas konts')?>:</strong> <?=$userdata['j_bank_code']?><br />
						<? }?>	
						<?/*
						<? if($userdata['company_bank_code']){ ?>
						<strong><?=$this->l('Bankas kods')?>:</strong> <?=$userdata['company_bank_code']?><br />
						<? }?>
						*/?>						
					</div>
					<? }?>
					</div>
					<br />
				</div>					
			</div>
			</body>
			</html>
			<?
			if($_GET['testing']){
				header('Content-Type: text/html; charset=utf-8');
				die;
			}	
			$html = ob_get_contents();		
			
		ob_end_clean();
		if($_GET['html']){
		header('Content-Type: text/html; charset=utf-8');
		echo $html;die;
		}
		$mpdf=new mPDF('','', 0, '', 0, 0, 0, 10, 0, 0);
		
		//$mpdf = new mPDF('',    // mode - default ''
//'A4',    // format - A4, for example, default ''
//0,     // font size - default 0
//'',    // default font family
//'',    // 15 margin_left
//'',    // 15 margin right
//25,     // 16 margin top
//55,    // margin bottom
//'',     // 9 margin header
//'',     // 9 margin footer
//'L');  // L - landscape, P - portrait
		$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list		
			
		$mpdf->WriteHTML($html);
		
		if($_GET['print'])
			$mpdf->SetJS('this.print();');
		die($mpdf->Output());
	}
	
  function SetProperties(){
    $this->properties = Array(
      "name"        => Array(
        "label"     => "Name:",
        "type"      => "str"
      ),
      "piedavajumi" => Array(
        "label"       => "Kolekcija piedavajumi:",
        "type"        => "collection",
        "collectiontype" => "EVeikalsPiedavajumiCol",       
      ),
			
			"artikuli" => Array(
        "label"       => "Kolekcija piedavajumu precu artikuli:",
        "type"        => "collection",
        "collectiontype" => "EVeikalsProdArticlesCol",        
      ),
			
			"prod_images" => Array(
        "label"       => "Product images collection:",
        "type"        => "collection",
        "collectiontype" => "shopcustomimagescollection",
        "lookup"      => GetCollections($this->site_id, "shopcustomimagescollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),
			
			"users" => Array(
        "label"       => "User collection:",
        "type"        => "collection",
        "collectiontype" => "shopusercollection",
        "lookup"      => GetCollections($this->site_id, "shopusercollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),	
			
			
    );
		$this->PostSetProperties();
  }



}