<?php

use Constructor\WebApp;

class LEDAkcijasBottomMenu extends component
{

    public function output()
    {


        $pages = $this->getPages();

        echo $this->render('index', ['pages' => $pages]);

    }

    private function getPages()
    {

        $lang = WebApp::$app->getLanguageId();
        return DB::GetTable("SELECT * FROM " . $this->site_id . "_pages WHERE parent=0 AND enabled=1 AND visible=1 AND in_trash=0 AND language = :lang ORDER BY ind", ['lang' => $lang]);

    }

}