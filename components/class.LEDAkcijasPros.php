<?php

use LEDAkcijas\ProsCollection as Pros;

class LEDAkcijasPros extends component
{

    public function output()
    {

        $pros = Pros::getInstance()->getDbData();

        return $this->render('index', ['pros' => $pros]);

    }


}