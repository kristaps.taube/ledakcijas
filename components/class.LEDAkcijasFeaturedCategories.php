<?php

class LEDAkcijasFeaturedCategories extends component
{

    public function output()
    {


        $categories = \shopcatcollection::getInstance()->getFeatured();

        echo $this->render('index', ['categories' => $categories]);

    }


}