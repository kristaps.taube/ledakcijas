<?php
/**
*
*  Title: Big cart
*  Author: Kristaps Taube <ktaube@datateks.lv>
*  Date: 28.01.2014
*  Project: E-Veikals
*
*/

include_once("class.component.php");

class EVeikalsBigCart extends component{

    public function __construct($name){
        parent::__construct($name);
        $this->initOptions();
    }

    public function initOptions()
    {

        $this->use_promo_codes = option('Shop\\use_promo_codes', null, 'Use Promo codes', Array("is_advanced" => true, 'type' => 'check'));
        $this->use_giftcards = option('Shop\\use_giftcards', null, 'Use Gift cards', Array("is_advanced" => true, 'type' => 'check'));

    }

  function output_mobile($goods){

    $sum = 0;
    ?>
    <div id="MobileBigCart">
      <? if(!empty($goods)){ ?>

      <? foreach($goods as $ind => $good){ ?>
      <?
        $image = $this->manager->getProductPicture($good['product']);
        $image = $image? $image['image'] : $this->shop->default_product_image;
        $prod_sum = $good["count"] * $good['price'];
        $sum += $prod_sum;
        $goods_js[$good['key']] = array("price" => $good['price']);
        if($this->manager->use_automatic_prod_stock)
            $good['prod_count'] = $this->manager->prodscol->GetValue(array('what' => 'prod_count','where' =>'item_id='.$good['id']));
      ?>
        <div class="product <?=($good['too_much_in_cart'] ? 'too_much' : '')?>" id='product-<?=$good['key']?>'>
          <a class='remove' href='?action=remove_cart_item&id=<?=$good['key']?>'><img src='/images/html/bigcart_remove_item.png' alt='' /></a>
          <div class="image">
            <img src='<?=getThumbUrl($image, 55, 100, 4)?>' alt='' />
          </div>
          <div class="row">
            <div class="label"><?=$this->l("Prece")?></div>
            <div class="value"><a href='<?=$this->manager->getProductURL($good['product'])?>'><?=$good['name']?></a></div>
            <div class="cb"></div>
          </div>
          <div class="row">
            <div class="label"><?=$this->l("Cena")?></div>
            <div class="value">
              <span class='primary'><span><?=number_format($good['price']  / $_SESSION['currency']['rate'], 2)?></span> <?=$_SESSION['currency']['label']?></span>
              <? if($this->CurrencyMenu->secondary){ ?>
              / <span class='secondary'><span><?=number_format($good['price'] / $this->CurrencyMenu->secondary['rate'], 2)?></span> <?=$this->CurrencyMenu->secondary['label']?></span>
              <? } ?>
            </div>
            <div class="cb"></div>
          </div>
          <div class="row counts">
            <div class="label"><?=$this->l("Daudzums")?></div>
            <div class="value">
              <input type='text' id="item<?=$good['id']?>" name='count[<?=$good['id']?>][]' value='<?=$good['count']?>' />
							<? if($this->manager->use_automatic_prod_stock){ ?>
							<script>
							$("#item<?=$good['id']?>").ForceNumericOnly();
							$(function(){
									$("#item<?=$good['id']?>").keyup(function () {
											var value = $(this).val();
											if( value > <?=$good['prod_count'] ?> ){
													$("#item<?=$good['id']?>").val('<?=$good['prod_count']?>');
												$(this).closest('.product').addClass('too_much');
											}
											else
												$(this).closest('.product').removeClass('too_much');											
									}).keyup();
							});
							</script>
							<? } ?>
            </div>
            <div class="cb"></div>
          </div>
          <div class="row sum">
            <div class="label"><?=$this->l("Kopā")?></div>
            <div class="value">
              <span class='primary'><span><?=number_format($prod_sum  / $_SESSION['currency']['rate'], 2)?></span> <?=$_SESSION['currency']['label']?></span>
              <? if($this->CurrencyMenu->secondary){ ?>
              / <span class='secondary'><span><?=number_format($prod_sum / $this->CurrencyMenu->secondary['rate'], 2)?></span> <?=$this->CurrencyMenu->secondary['label']?></span>
              <? } ?>
            </div>
            <div class="cb"></div>
          </div>
        </div>
      <? } ?>
      <div class='sep'></div>
        <? if($this->use_promo_codes || $this->use_giftcards){ ?>
      <div class='promobox'>
        <div class='box'>
          <table>
            <? if($this->use_promo_codes){ ?>
            <tr>
              <td><?=$this->l("Promo kods")?>:</td>
              <td><input type='text' name='promo' value='' /> </td>
            </tr>
            <tr id='promo_response' style='display: none'>
              <td colspan='2' class='no-padding'><div></div> </td>
            </tr>
            <? } ?>
            <? if($this->use_giftcards){ ?>
            <tr>
              <td><?=$this->l("Dāvanu kartes kods")?>: </td>
              <td>
                <input type='text' name='giftcard' value='<?=htmlspecialchars($_SESSION['giftcard_code'])?>' />
                <div class='giftcard_loader'></div>
              </td>
            </tr>
            <tr id='giftcard_response' style='display: none'>
              <td colspan='2' class='no-padding'><div></div> </td>
            </tr>
        <? } ?>
          </table>
        </div>
      </div>
      <div class='sep'></div>
        <? } ?>
      <div class="total_box">
        <div class='total_row'>
          <div class="title"><?=$this->l("Kopā")?></div>
          <div class="box">
            <div class="total">
              <div class='primary'>
                <span><?=number_format($sum  / $_SESSION['currency']['rate'], 2)?></span> <?=$_SESSION['currency']['label']?>
              </div>
              <? if($this->CurrencyMenu->secondary){ ?>
                <div class='secondary'>
                  <span><?=number_format($sum / $this->CurrencyMenu->secondary['rate'], 2)?></span> <?=$this->CurrencyMenu->secondary['label']?>
                </div>
                <? } ?>
            </div>
            <div class="delivery">
              <?=$this->l("+ piegādes izmaksas")?>
            </div>
          </div>
        </div>
      </div>
      <div class='actions'>
        <a class='continue' href='/<?=PagePathById($this->shop_url)?>'><?=$this->l("Turpināt iepirkties")?></a>
        <a class='checkout' href='/<?=PagePathById($this->orderform_url)?>'><?=$this->l("Noformēt pasūtījumu")?></a>
        <div class='cb'></div>
      </div>
      <script>
        var goods = <?=json_encode($goods_js)?>;
      </script>
      <? }else{ ?>
        <div class='empty'><?=$this->l("Grozā preču nav")?></div>
      <? } ?>
    </div>
    <?

  }

  function output(){

    $this->manager = shopmanager::getInstance();
    $this->shop = EveikalsFrontendShop::getInstance();
    $this->CurrencyMenu = EVeikalsCurrencyMenu::getInstance();

    $this->manager->getSumAndGoodsList($goods, $sum, $hasgoods);
    $sum = 0;

    if($this->manager->mobile_session){
      $this->output_mobile($goods);
      return;
    }

    #dump($goods);

    ?>
    <div id='BigCart'>
      <? if(!empty($goods)){ ?>
      <table>
        <tr>
          <th><?=$this->l("Prece")?></th>
          <th>&nbsp;</th>
          <th><?=$this->l("Cena")?></th>
          <th><?=$this->l("Daudzums")?></th>
          <th><?=$this->l("Kopā")?></th>
          <th>&nbsp;</th>
        </tr>
        <? foreach($goods as $ind => $good){ ?>
        <?
        $image = $this->manager->getProductPicture($good['product']);
        $image = $image? $image : $this->shop->default_product_image;
        $prod_sum = $good["count"] * $good['price'];
        $sum += $prod_sum;
        $goods_js[$good['key']] = array("price" => $good['price']);
        if($this->manager->use_automatic_prod_stock)
        $good['prod_count'] = $this->manager->prodscol->GetValue(array('what' => 'prod_count','where' =>'item_id='.$good['id']));
        if($this->manager->use_automatic_prod_stock){
        ?>
            <script>
            $("#item<?=$good['id']?>").ForceNumericOnly();
            $(function(){
                $("#item<?=$good['id']?>").keyup(function () {
                var value = $(this).val();
                if( value > <?=$good['prod_count'] ?> ){
                    $("#item<?=$good['id']?>").val('<?=$good['prod_count']?>');
                    $(this).closest('.product').addClass('too_much');
                }
                else{
                    $(this).closest('.product').removeClass('too_much');
                }
                }).keyup();
            });
            </script>
        <? } ?>
        <tr class='product' id='<?=$good['giftcard']? "giftcard" : "product"?>-<?=$good['key']?>'>
          <td class='picture'><img src='<?=getThumbUrl($image, 55, 50, 6)?>' alt='' /></td>
          <td>
            <? if($good['giftcard']){ ?>
              <span><?=$good['name']?></span>
            <? }else{ ?>
              <a href='<?=$this->manager->getProductURL($good['product'])?>' title='<?=hsc($good['name'])?>'><?=$good['name']?></a>
            <? } ?>
          </td>
          <td class='price'>
            <?=number_format($good['price'] / $_SESSION['currency']['rate'], 2)?> <?=$_SESSION['currency']['label']?>
            <? if($this->CurrencyMenu->secondary && $this->CurrencyMenu->secondary['title'] != $_SESSION['currency']['title']){ ?>
            <div class='secondary'><?=number_format($good['price'] / $this->CurrencyMenu->secondary['rate'], 2)?> <?=$this->CurrencyMenu->secondary['label']?></div>
            <? } ?>
          </td>
          <td class='counts'><input type='text' id="item<?=$good['id']?>" name='count[<?=$good['id']?>][]' value='<?=$good['count']?>' /></td>
          <td class='sum'>
            <div class='primary'><span><?=number_format($prod_sum / $_SESSION['currency']['rate'], 2)?></span> <?=$_SESSION['currency']['label']?></div>
            <? if($this->CurrencyMenu->secondary && $this->CurrencyMenu->secondary['title'] != $_SESSION['currency']['title']){ ?>
            <div class='secondary'><span><?=number_format($good["count"] * $good['price'] / $this->CurrencyMenu->secondary['rate'], 2)?></span> <?=$this->CurrencyMenu->secondary['label']?></div>
            <? } ?>
          </td>
          <td class='remove'>
            <a href='?action=remove_cart_item&id=<?=$good['key']?>' title='<?=hsc($this->l("Noņemt"))?>'><img src='/images/html/bigcart_remove_item.png' alt='' /></a>
          </td>
        </tr>
        <? } ?>
      </table>
      <? if($this->use_promo_codes || $this->use_giftcards){ ?>
      <div class='promobox'>
        <table>
          <? if($this->use_promo_codes){ ?>
          <tr>
            <td><?=$this->l("Promo kods")?>:</td>
            <td>
              <input type='text' name='promo' value='<?=htmlspecialchars($_SESSION['promo_code'], ENT_QUOTES)?>' />
              <div class='promo_loader'></div>
            </td>
          </tr>
          <tr id='promo_response' style='display: none'>
            <td colspan='2' class='no-padding'><div></div> </td>
          </tr>
          <? } ?>
          <? if($this->use_giftcards){ ?>
          <tr>
            <td><?=$this->l("Dāvanu kartes kods")?>: </td>
            <td>
              <input type='text' name='giftcard' value='<?=htmlspecialchars($_SESSION['giftcard_code'], ENT_QUOTES)?>' />
              <div class='giftcard_loader'></div>
            </td>
          </tr>
          <tr id='giftcard_response' style='display: none'>
            <td colspan='2' class='no-padding'><div></div> </td>
          </tr>
          <? } ?>
        </table>
      </div>
      <? } ?>
      <div class='total_box'>
        <div class='row giftcard_total' style='display: none'>
          <div class='label'><?=$this->l("Dāvanu karte")?>:</div>
          <div class='total'><span></span> <?=$_SESSION['currency']['label']?></div>
          <div class='cb'></div>
        </div>
        <div class='row promo_total' style='display: none'>
          <div class='label'><?=$this->l("Promo koda atlaide")?>:</div>
          <div class='total'><span></span> <?=$_SESSION['currency']['label']?></div>
          <div class='cb'></div>
        </div>
        <div class='total_row'>
          <div class='label'><?=$this->l("Kopā")?>:</div>
          <div class='total'><span><?=number_format($sum / $_SESSION['currency']['rate'], 2)?></span> <?=$_SESSION['currency']['label']?></div>
          <? if($this->CurrencyMenu->secondary && $this->CurrencyMenu->secondary['title'] != $_SESSION['currency']['title']){ ?>
            <div class='secondary'><span><?=number_format($sum / $this->CurrencyMenu->secondary['rate'], 2)?></span> <?=$this->CurrencyMenu->secondary['label']?></div>
          <? } ?>
          <div class='delivery'><?=$this->l("+ piegādes izmaksas")?></div>
        </div>
      </div>
      <div class='cb'></div>
      <div class='actions'>
        <a class='continue' href='/' title='<?=hsc($this->l("Turpināt iepirkties"))?>'><?=$this->l("Turpināt iepirkties")?></a>
        <a class='checkout' href='<?php echo Constructor\SystemUrl::get("checkout")?>' title='<?=hsc($this->l("Noformēt pasūtījumu"))?>'><?=$this->l("Noformēt pasūtījumu")?></a>
        <div class='cb'></div>
      </div>
      <script>
        var goods = <?=json_encode($goods_js)?>;
      </script>
      <? }else{ ?>
      <div class='empty'><?=$this->l("Grozā preču nav")?></div>
      <? } ?>
    </div>
    <?

  }

  function SetProperties(){

    $this->properties = Array(

      "name"        => Array(
        "label"     => "Name:",
        "type"      => "str"
      ),

      'orderform_url' => Array(
        'label'     => 'Noformēšanas lapa:',
        'type'      => 'page_id',
      ),

      'bigcart_url' => Array(
        'label'     => 'Lielā groza lapa:',
        'type'      => 'page_id',
      ),

    );

		$this->PostSetProperties();

  }

}
