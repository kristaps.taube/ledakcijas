<?php
/**
*
*  Title: formatedtext0 v2.0
*  Author: Kristaps Taube <ktaube@datateks.lv>
*  Date: 08.02.2017
*  Project: Constructor
*
*/


class formatedtext0 extends component
{

    public function output($page_id = false, $name = false)
    {

        if(Constructor\App::$app->getConfigValue('backend')){
            $this->design($page_id, $name);
        }else{
            $name = $name ? $name : $this->name;
            $content = $this->contents->getContent($page_id, $name, $this->savemode);
            echo "<div class='formatedtext'>".$content."</div>";
        }

    }

    public function ajax()
    {

        $page_id = $_POST['page_id'];
        $name = $_POST['name'];
        $content = $_POST['content'];

        $this->contents->setContent($page_id, $name, $content, $this->savemode);

        echo json_encode($_POST);

        die();

    }

    public function design($page_id = false, $name = false)
    {

        $name = $name ? $name : $this->name;
        $content = $this->contents->getContent($page_id, $name, $this->savemode);
        $content = $content ? $content : Constructor\App::l('Tukšs');

        $id = $this->type.'_'.$name;
        $conf = [
            'page_id' => $page_id,
            'name' => $name,
            'id' => $id,
            'save_url' => $this->getComponentModifierLink('ajax')
        ];


        echo "<div id='".$id."' class='formatedtext_wrap'>".$content."</div>";
        ?>
        <link rel="stylesheet" type="text/css" href="<?php echo Constructor\App::$app->getConfigValue('web_root')?>gui/formatedtext.css" />
        <script>
            includeJsOnce("<?php echo Constructor\App::$app->getConfigValue('web_root')?>gui/ckeditor/ckeditor.js");
            $.getScript("<?php echo Constructor\App::$app->getConfigValue('web_root')?>gui/spinner.js");
            $.getScript("<?php echo Constructor\App::$app->getConfigValue('web_root')?>gui/formatedtext.js", function(){
                initFormatedtext0(<?=json_encode($conf)?>);
            });
        </script>

        <?php

    }

    public function addProperties()
    {

        return [
            "savemode"    => [
                "label"     => "Save mode:",
                "type"      => "list",
                "lookupassoc"    => [
                    0 => 'To edited page',
                    1 => 'As language default values',
                    2 => 'As default values'
                ]
            ],
            "contents" => [
                "label"       => "Content collection:",
                "type"        => "collection",
                "collectiontype" => "Formatedtext0Collection",
            ],
        ];

    }

}
