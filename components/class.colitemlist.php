<?
//Serveris.LV Constructor component
//Created by Aivars Irmejs, 2003, aivars@serveris.lv

include_once("class.component.php");

class colitemlist extends component
{

  //Class initialization
  function colitemlist($name)
  {
    parent::__construct($name);
    $this->collectiontype = 'collection';
    $this->SetProperties();
    $this->createmode = 0; //0 - per site, 1 - per page
  }



  function InitCollection(&$coltype, &$colname, &$categoryID)
  {
    $coltype = $this->collectiontype;
    if($coltype)
    {
      include_once($GLOBALS['cfgDirRoot'] . "collections/class." . $coltype . ".php");
      $colname = '';
      $categoryID = $this->getProperty('collectionID');

      $this->collection = new $coltype($colname,$categoryID);

      return true;
    }else
    {
      return false;
    }

  }

  function GetTableData($isdesign = false)
  {
    if($this->collection)
      return $this->collection->GetDBData();
  }

  function Execute()
  {
    $this->Executed = true;
    if($this->collectiontype)
    {
      $this->InitCollection($coltype, $colname, $categoryID);

      if (!$categoryID && $this->getProperty('creatauto'))
  	  {
        if(!$this->createmode)
          $colname = $this->name . "_collection";
        else
           $colname = $this->name . '_' . $this->page_id . "_collection";

        $categoryID = $this->collection->FindCollection($colname);
        if(!$categoryID)
          $categoryID=$this->collection->AddNewCollection($colname);
  		$this->setProperty('collectionID',$categoryID);
  	  }
    }

  }

  function Output($isdesign = false)
  {
    //$this->Design();

    if(!$this->Executed)
      $this->Execute();

  	//initialize collection class
    if($this->collectiontype)
    {
      if($this->collection->IsValidCollection())
      {
        $data = $this->GetTableData($isdesign);
        echo $this->OutputTable($data, $isdesign);
      }
    }
  }

  function OutputTable($data, $design)
  {
    $d = '';
    foreach($data as $key => $row)
	{
      $d .= $this->OutputRow($row, $design);
	}

    if($design)
    {
      $d .= $this->OutputAddButton();
    }
    return $d;

  }

  function OutputRow($row, $design, &$lastletter)
  {
    $data = '';
    foreach($row as $cell)
    {
      $data .= $cell . " ";
    }

    if($design)
    {
      $data .= $this->OutputEditButtons($row['item_id']);
    }
    $data .= "<br>";
    return $data;
  }

  function WindowDimensions(&$w, &$h)
  {
    $w = '520';
    $h = '500';
  }

  function OutputAddButton($text="Add")
  {
    $this->WindowDimensions($w, $h);
    return $this->DefaultCollectionAddButton($text, 'collectionID',$this->collection, $w, $h);
  }

  function OutputEditButtons($id)
  {
    $this->WindowDimensions($w, $h);
    return $this->DefaultCollectionItemIcons('collectionID', $id, $this->collection, $w, $h);
  }

  function Design()
  {
    $this->Output(true);
  }

  function toolbar(&$script, &$toolbars, &$btnids, &$onclick)
  {
    return True;
  }

  function SetProperties()
  {
    $this->properties = Array(

      "name"        => Array(
        "label"     => "Name:",
        "type"      => "string"
      ),

    "collectionID" => Array(
      "label"       => "Collection:",
      "type"        => "collection",
      "collectiontype" => $this->collectiontype,
      "lookup"      => GetCollections($this->site_id, $this->collectiontype),
      "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
      "dialogw"     => "600"
    ),

    "creatauto" => Array(
      "label"       => "Auto create collection:",
      "type"        => "list",
      "lookup"      => Array("0:No automatic creation", "1:Create if not assigned"),
    ),



    );


  }

}

?>
