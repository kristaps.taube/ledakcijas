<?php
/**
*
*  Title: Paysera
*  Author: Kristaps Taube <ktaube@datateks.lv>
*  Date: 04.04.2018
*  Project: LEDAkcijas.lv
*
*/

use Constructor\App;
use Constructor\Url;

class EVeikalsPaysera extends component
{

    public function __construct($name = false)
    {

        parent::__construct($name);
        $this->init();

    }

    public function redirectToPayment($order_id)
    {

        $order = $this->orders->getById($order_id);
        $products = $this->order_products->getOrderProducts($order_id);
        $order_data = unserialize($order['order_data']);
        $fields = $order_data['allfields'];

        $lang = $order['valoda'];

        $description = App::l("Pasūtījuma [num] apmaksa", ['lang' => $lang, 'replace' => ['[num]' => $order['num']]]);

        switch($lang){
            case 'lv': $p_lang = 'LAT'; break;
            case 'ru': $p_lang = 'RUS'; break;
            case 'en': $p_lang = 'ENG'; break;
            default: $p_lang = 'ENG'; break;
        }

        $accept_url = Url::get('/paysera/accept', ['lang' => $lang], true);
        $cancel_url = Url::get('/paysera/cancel', ['lang' => $lang], true);
        $callback_url = Url::get('/paysera/callback', [], true);

        if($fields['payer'] == 'legal'){
            $firstname = 'Company';
            $lastname = $fields['company_name'];
        }else{
            $name = explode(" ", $fields['name']);
            $lastname = $name[count($name)-1];
            unset($name[count($name)-1]);
            $firstname = implode(" ", $name);
        }

        $invoice = [
            'projectid' => $this->service_id,
            'sign_password' => $this->secret,
            'orderid' => $order_id,
            'amount' => $order['summa'] * 100,
            'currency' => 'EUR',
            'payment' => 'other', // emmm ?
            'country' => 'LV',
            'lang' => $p_lang,
            'paytext' => $description,
            'p_firstname' => $firstname,
            'p_lastname' => $lastname,
            'p_email' => $fields['email'],
            'accepturl' => $accept_url,
            'cancelurl' => $cancel_url,
            'callbackurl' => $callback_url,
            'test' => $this->test_mode ? 1 : 0
        ];

        $this->log('Invoice: '.print_r($invoice, true));
        $this->paysera_service->redirectToPayment($invoice, true);

    }

    public function processCallback($data)
    {

        $result = false;

        try {

            $this->log('Callback received: '.print_r($data, true));

            $response = $this->paysera_service->checkResponse($data, [
                'projectid' => $this->paysera_service_id,
                'sign_password' => $this->paysera_secret,
            ]);

            if(!$response){
                throw new Exception('No response. Data: '.print_r($data, true));
            }

            if ($response['type'] !== 'macro') {
                throw new Exception('Only macro payment callbacks are accepted');
            }

            $result = $this->processCallbackResponse($response);

        } catch (Exception $e) {
            $message = get_class($e) . ': ' . $e->getMessage();
            $this->log($message);
        }

        return $result;

    }

    private function processCallbackResponse($response)
    {

        $order = $this->orders->getById($response['orderid']);

        $result = false;

        if($order){

            if(!$order['apmaksats']){

                $paid = ($response['status'] == 1) ? 1 : 0;

                $order_update = [
                    'apmaksats' => $paid,
                    'apmaksas_datums' => $paid ? date('Y-m-d H:i:s') : null
                ];

                $paid_status = $this->statuses->getPaidStatusRow();

                if($paid_status){

                    $this->order_history->Insert([
                        "order_id" => $order['item_id'],
                        "status_from_id" => $order['status_id'],
                        "status_to_id" => $paid_status['item_id'],
                        "comments" => "Rēķina apmaksa saņemta no paysera",
                        "date" => date('Y-m-d H:i:s')
                    ]);

                    $order_update['status_id'] = $paid_status['item_id'];

                }

                $this->orders->Update($order_update, $order['item_id']);

                if($paid_status){ // send notification after order update
                    LEDAkcijasOrderStatusChangeNotifications::getInstance()->process($order['item_id'], $order['status_id'], $paid_status['item_id']);
                }

            }

            $result = true; // if we are so far, lets assume everything is ok

        }

        return $result;

    }

    private function log($line)
    {

        $file = App::$app->getConfigValue('log/directory').'paysera.log';
        file_put_contents($file, $line.PHP_EOL, FILE_APPEND);

    }

    private function init()
    {

        #$this->use_paysera = option('Paysera\\use', null, 'Use paysera', Array("is_advanced" => true, 'type' => 'check'));
        $this->test_mode = option('Paysera\\test_mode', null, 'Use paysera test mode', ['type' => 'check']);
        $this->service_id = option('Paysera\\service_id', null, 'Project ID', [], '');
        $this->secret = option('Paysera\\secret', null, 'Project password', [], '');

        $this->paysera_service = new WebToPay();

    }

    public function addProperties()
    {

        return [
            "orders" => [
                "label"       => "Order collection:",
                "type"        => "collection",
                "collectiontype" => "EVeikalsOrderCollection",
            ],
            "order_products" => [
                "label"       => "Order product collection:",
                "type"        => "collection",
                "collectiontype" => "shoporderprodcollection",
            ],
            "statuses" => [
                "label" => "Statuses collection:",
                "type" => "collection",
                "collectiontype" => "EVeikalsOrderStatuses",
            ],
            "order_history" => [
                "label" => "Order history collection:",
                "type" => "collection",
                "collectiontype" => "EVeikalsOrderHistory",
            ],
        ];

    }

}