<?
//Serveris.LV Constructor collection
//Created by Aivars Irmejs, 2007, aivars@datateks.lv

include_once("class.component.php");
require_once($GLOBALS['cfgDirRoot']."library/models/class.langpages.php");

class shoplangswitcher extends component{

  //Class initialization
  function shoplangswitcher($name){
    parent::__construct($name);
    $this->SetProperties();
  }

  function getLinkForLang($lang){

    if(!$this->shopmanager->cat_id){

      if ($this->lang == $lang){
        $return = '/'.PagePathById($this->page_id, $this->site_id);
      }else{

      	$langs = getLanguages();
	      $link = LangPages::getLink($this->page_id, $langs[$this->lang]['language_id'], $langs[$lang]['language_id']);

	      if ($link){
	        $id = ($link['from_id'] == $this->page_id) ? $link['to_id'] : $link['from_id'];
	        $return = '/'.PagePathById($id, $this->site_id);
	      }else{
	        $return =  '/'.$lang;
				}

      }

    }else{

    	$url = $this->shopmanager->prod_id ? $this->shopmanager->getProductURL($this->shopmanager->prod_id, $lang) : $this->shopmanager->getCategoryURL($this->shopmanager->cat_id, $lang);

	    $params_to_keep = $this->getProperty('keepparams');
	    $paramlist = explode(',', $params_to_keep);

	    //add specified GET parameters to destination URL
	    $params = Array();
	    foreach($paramlist as $param)
	    {
	      if (isset($_GET[$param]))
	        $params[$param] = $_GET[$param];

	    }
	    $paramstr = http_build_query($params);
	    $return = $url.($paramstr != '' ? '?'.$paramstr : '');

    }

		return $return;

  }


	// no output for this comp
  function Output_old(){
    $urlman = $this->getServiceByName('shopmanager');
    $lang = $this->getProperty('lang');

    if (!$urlman || !$urlman->cat_id)
    {
      if ($this->lang == $lang)
      {
        echo '/'.PagePathById($this->page_id, $this->site_id);
        return;
      }
      $langs = getLanguages();
      $link = LangPages::getLink($this->page_id, $langs[$this->lang]['language_id'], $langs[$lang]['language_id']);

      if ($link)
      {
        $id = ($link['from_id'] == $this->page_id) ? $link['to_id'] : $link['from_id'];
        echo '/'.PagePathById($id, $this->site_id);
      }
      else
        echo '/'.$lang;

      return;
    }

    $url = $urlman->prod_id ? $urlman->getProductURL($urlman->prod_id, $lang) : $urlman->getCategoryURL($urlman->cat_id, $lang);

    $params_to_keep = $this->getProperty('keepparams');
    $paramlist = explode(',', $params_to_keep);

    //add specified GET parameters to destination URL
    $params = Array();
    foreach($paramlist as $param)
    {
      if (isset($_GET[$param]))
        $params[$param] = $_GET[$param];

    }
    $paramstr = http_build_query($params);
    echo $url.($paramstr != '' ? '?'.$paramstr : '');
  }

  function Design()
  {
    $this->Output();
  }

  function SetProperties()
  {
    $langs = Array();
    $data = sqlQueryData("SELECT shortname, fullname FROM `".$this->site_id."_languages`");
    foreach ($data as $row)
    {
      $langs[$row['shortname']] = $row['fullname'];
    }

    $this->properties = Array(
      "name"        => Array(
        "label"     => "Name:",
        "type"      => "string"
      ),

      "keepparams"        => Array(
        "label"     => "Parameters to keep:",
        "type"      => "string",
        "value"     => "page,searchq"
      ),

      "lang"    => Array(
        "label"     => "Language:",
        "type"      => "list",
        "lookupassoc" => $langs,
        "value"     => "lv"
      ),

    );

  }
}
?>