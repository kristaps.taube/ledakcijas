<?php

use Constructor\App;
use Constructor\Url;

class LEDAkcijasProductList extends component
{

    public function output($type = 'new')
    {

        $products = $this->getProducts($type);

        return $this->render('index', [
            'type' => $type,
            'title' => $type == 'new' ? App::l("Jaunākās preces") : App::l("Pirktākās preces"),
            'link' => $type == 'new' ? Url::get('new') : Url::get('popular'),
            'products' => $products
        ]);

    }

    private function getProducts($type)
    {

        $cond = $params = [];
        $cond = ['shortcut = 0', 'disabled = 0'];

        if($type == 'new'){

            $cond[] = '(is_new = 1 AND (ISNULL(new_until) OR new_until="0000-00-00" OR NOW() < new_until))';

        }else{

            $cond[] = 'is_top = 1';

        }

        return $this->products->getTable(['where' => implode(" AND ", $cond), 'limit' => 4, 'order' => 'rand()', 'params' => $params]);

    }

    public function addProperties()
    {

        return [
            'products' => [
                'label'       => 'Products:',
                'type'        => 'collection',
                'collectiontype' => 'shopprodcollection',
            ],
        ];

    }

}