<?php
/**
*
*  Title: Logo
*  Author: Kristaps Taube <ktaube@datateks.lv>
*  Date: 24.03.2014
*  Project: E-Veikals
*
*/

include_once("class.component.php");

class EVeikalsLogo extends component{

  function __construct($name){
    parent::__construct($name);
    $this->SetProperties();
  }

  function design(){
    $this->DesignColors = EVeikalsDesignColors::getInstance();
    $this->DesignColors->init();
    $GLOBALS['designcolor'] = strtolower($this->DesignColors->color['name']);
    $this->output(true);
  }

  static function processOptionBeforeDisplay($path, $opt, &$data){

    switch ($path){
			case 'Shop\fav_icon':
        $data['dialog'] = '?module=dialogs&action=filex&site_id='.$GLOBALS['site_id'].'&view=thumbs';
        break;
    }
  }

  function output($design = false){

    if(!$this->initCollections()){
      echo $this->name.": Can't init collections.<br />";
      return;
    }

    $this->fav_icon = option('Shop\\fav_icon', null, 'Fav. Icon(.png 16x16px)', Array('type' => 'dialog', 'componenttype' => get_class($this) ) );
    if($this->fav_icon){
    	$GLOBALS['additional_header_tags'] .= "<link rel='shortcut icon' type='image/png' href='".$this->fav_icon."' />";
    }                    

    if($design){
      $link = $this->getComponentDialogLink('changeLogo', array(), 'Logo īpašības');
      $js = $this->getDesignIconScript($link, 480, 500);
      echo "<div style='position: relative'>";
      echo '<input type="button" onclick="'.$js.';return false;" value="Nomainīt logo" style="padding-left: 5px; padding-right: 5px; left: 3px; top: 3px; position:absolute;z-index:1000;'.$this->getProperty('contentstyle').'"/>';
    }

    $logo = $this->getProperty("logo");
    $title = $this->getProperty("title");
    $alt = $this->getProperty("alt");
		$alt = $alt ? $alt : $this->l("Kompānijas nosaukums");
    if(!$logo) $logo = '/images/html/'.$GLOBALS['designcolor'].'/logo.png';

    ?>
      <img src='<?=$logo?>' alt='<?=$alt?>' />
    <?
    if($design){
      echo "</div>";
    }

  }

  function changeLogo(){

    $form_data = Array(
      'logo' => $this->properties['logo'],
      'title' => $this->properties['title'],
      'alt' => $this->properties['alt'],
    );

    $form_data['alt']['value'] = $this->getProperty('alt');
    $form_data['title']['value'] = $this->getProperty('title');

    echo $this->getDialogFormOutput($form_data, 'changeLogoSave', 'Logo īpašības', array('afterrefreshreload' => 1) );

  }

  function changeLogoSave(){

    $this->setDefaultProperty('logo', $_POST['logo']);
    $this->setDefaultProperty('title', $_POST['title']);
    $this->setDefaultProperty('alt', $_POST['alt']);

  }

  function SetProperties(){
    $this->properties = Array(

      "name"        => Array(
        "label"     => "Name:",
        "type"      => "str"
      ),

      "logo"    => Array(
         "label"     => "Logo bilde:",
         "type"      => "dialog",
         "dialog"    => "?module=dialogs&action=filex&site_id=".$this->site_id."&view=thumbs",
         "dialogw"   => "600"
      ),

      'title' => Array(
        'label'     => 'Virsraksts(title):',
        'type'      => 'str'
      ),

      'alt' => Array(
        'label'     => 'Alternatīvais teksts(alt):',
        'type'      => 'str'
      ),

    );
  }



}
