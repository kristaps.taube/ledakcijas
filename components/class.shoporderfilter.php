<?

include_once("class.component.php");

class shoporderfilter extends component
{
  function shoporderfilter($name)
  {
    parent::__construct($name);
    $this->SetProperties();
  }

  function design()
  {
    if (isset($_POST['datefrom']))
    {
      $_SESSION['of_datefrom'] = $_POST['datefrom'];
      $_SESSION['of_dateto'] = $_POST['dateto'];
    }
    else if (!isset($_SESSION['of_dateform']))
    {
      $t = time();
      $_SESSION['of_datefrom'] = date('d-m-Y', $t).' 00:00:00';
      $_SESSION['of_dateto'] =  date('d-m-Y', $t+86400).' 00:00:00';
    }

    require_once($GLOBALS['cfgDirRoot']."library/"."class.datagrid.php");

    $ordercol = $this->initPropertyCollection('ordercol');
    if (!$ordercol) return;

    $summa = 0;
    $data = $ordercol->getOrdersByDateInterval(strtotime($_SESSION['of_datefrom']), strtotime($_SESSION['of_dateto']));
    foreach ($data as $key => $row)
    {
      $summa += $row['summa'];
    }
    $data[] = Array(
      'pasutitajs' => '<b>'.lcms('Total').':</b>',
      'summa' => '<b>'.number_format($summa, 2, '.', ' ').'</b>',
    );

    $table = new DataGrid();
    $table->cols = Array(
      'pasutitajs' => Array('title' => lcms('Customer')),
      'summa' => Array('title' => lcms('Total sum')),
    );

    $table->data = $data;
    $table->footer = false;

?>
  <script type="text/javascript" src="<?=$GLOBALS['cfgWebRoot'] ?>gui/calendarfiles/calendar.js"></script>
  <script type="text/javascript" src="<?=$GLOBALS['cfgWebRoot'] ?>gui/calendarfiles/calendar-en.js"></script>
  <script type="text/javascript" src="<?=$GLOBALS['cfgWebRoot'] ?>gui/calendarfiles/calendar-setup.js"></script>

  <form id="dateselecter" name="dateselecter" method="post" action="?<?=http_build_query($_GET) ?>">
  <?=lcms('From')?>: <input type="text" name="datefrom" id="f_date_b" value="<?=$_SESSION["of_datefrom"] ?>"/>
  &nbsp;<img src="<?=$GLOBALS['cfgWebRoot'] ?>gui/calendarfiles/img.gif" id="f_trigger_b" style="cursor: pointer; border: 1px solid none;" title="Date selector" onmouseover="this.style.background='red';" onmouseout="this.style.background=''" />
<script type="text/javascript">
    function changeDatesList() {
//      document.forms['dateselecter'].period.options[7].selected=true;
    }
    Calendar.setup({
        inputField     :    "f_date_b",      // id of the input field
        ifFormat       :    "%d-%m-%Y 00:00:00",       // format of the input field
        showsTime      :    true,            // will display a time selector
        button         :    "f_trigger_b",   // trigger for the calendar (button ID)
        singleClick    :    true,           // double-click mode
        step           :    1,              // show all years in drop-down boxes (instead of every other year as default)
        onUpdate       :    changeDatesList  //Update list with times and set it to custom
    });
</script>
  &nbsp;&nbsp;&nbsp;
  <?=lcms('To')?>: <input type="text" name="dateto" id="f_date_c" value="<?=$_SESSION["of_dateto"] ?>"/>
  &nbsp;<img src="<?=$GLOBALS['cfgWebRoot'] ?>gui/calendarfiles/img.gif" id="f_trigger_c" style="cursor: pointer; border: 1px solid none;" title="Date selector" onmouseover="this.style.background='red';" onmouseout="this.style.background=''" />
<script type="text/javascript">
    Calendar.setup({
        inputField     :    "f_date_c",      // id of the input field
        ifFormat       :    "%d-%m-%Y 00:00:00",       // format of the input field
        showsTime      :    true,            // will display a time selector
        button         :    "f_trigger_c",   // trigger for the calendar (button ID)
        singleClick    :    true,           // double-click mode
        step           :    1,              // show all years in drop-down boxes (instead of every other year as default)
        onUpdate       :    changeDatesList  //Update list with times and set it to custom
    });
</script>

  &nbsp;&nbsp;&nbsp;
  <input type="submit" value="<?=lcms('Filter')?>" name="filtret"/>

  </form><br/>
  <?=$table->output() ?>
<?
  }


  function toolbar(&$script, &$toolbars, &$btnids, &$onclick)
  {
    return true;
  }

  function SetProperties()
  {
    $this->properties = Array(
      'name' => Array(
        'label'     => 'Name:',
        'type'      => 'str'
      ),

      "ordercol" => Array(
        "label"       => "Order collection:",
        "type"        => "collection",
        "collectiontype" => "EVeikalsOrderCollection",
        "lookup"      => GetCollections($this->site_id, "EVeikalsOrderCollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

    );
  }
}





