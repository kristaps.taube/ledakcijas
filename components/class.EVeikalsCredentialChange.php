<?php
/**
*
*  Title: [EVeikals user credential change]
*  Author: Jānis Polis <j.polis@datateks.lv>
*  Date: [29.07.2016]
*  Project: E-Veikals
*
*/

include_once("class.component.php");

class EVeikalsCredentialChange extends component{

  function __construct($name){
    parent::__construct($name);
    $this->SetProperties();
  }

	function uploadImage(){

    $imagetypes = array('jpg', 'png','jpeg');
	
    foreach ($_FILES as $name => $f){
      if (!in_array(strtolower(pathinfo($f['name'], PATHINFO_EXTENSION)), $imagetypes)) continue;
      $info = uploadSiteFile($this->site_id, $f, "upload/logos/");
    }

    return $info;

  }
	
	function initOptions(){

    if ($this->opts_initialized) return;

    option('Shop\\OrderForm', '', 'Order form');
    $optcat = getOptionByPath('Shop\\OrderForm');
    $params = Array('parent_id' => $optcat['id']);

    $this->use_user_offers = option('use_user_offers', null, 'Lietot lietotāju piedāvājumus', array_merge($params, Array('type' => 'check','is_advanced' => true)), null);

		
    $this->opts_initialized = true;
  }
	
  function output(){
		
		if(!$this->initCollections()){
      echo $this->name.": Can't init collections.<br />";
      return;
    }
		$this->initOptions();
		if(!$this->use_user_offers)
			return;
			
		if($_SESSION['logged_in']){
		/* 	if($_POST['changeaddress']){
				$toSave = array('email'=> $_POST['email']);				
				$upd = $this->users->Update($toSave,$_SESSION['logged_in']['item_id']);				
				if($upd){
					$_SESSION['logged_in']['delivery_address'] = $_POST['address'];
					$_SESSION['logged_in']['email'] = $_POST['email'];
					echo '<p>'.$this->l('Izmaiņas ir veiktas').'</p>';
				}	
			}	
			?>
			<form method="post">
				<table>
					<tr>
						<th colspan="2"><?=$this->l('Lietotāja datu rediģēšana')?></th>
					</tr>
					<tr>
						<td><label for="email"><?=$this->l('E-pasts')?></label></td>
						<td><input type="email" required name="email" id="email" value="<?=$_SESSION['logged_in']['email']?>" /></td>
					</tr>
					<tr>
						<td colspan="2"><input type="submit" name="changeaddress" class="button" value="<?=$this->l('Saglabāt')?>" /></td>
					</tr>	
				</table>
			</form>
			<?	
		}
		else{ */
			$current_data = unserialize($_SESSION['logged_in']['savedfields']);
			
			if($_POST['changecredentials']){	
			
				$new_data = array();
				foreach($current_data as $k => $d){
						if(isset($_POST[$k]))
							$new_data[$k] = $_POST[$k];
						else
							$new_data[$k] = $d;
				}		
				foreach($_POST as $pk => $v){
					if(!$new_data[$pk])
						$new_data[$pk] = $v;
				}	
				
				$toSave = serialize($new_data);
				$this->users->Update(Array('savedfields' => $toSave),$_SESSION['logged_in']['item_id']);
				$_SESSION['logged_in']['savedfields'] = $toSave;		
				if($_POST['dellogo']){
					$ind = $this->users->Update(Array('logoimage' => ''),$_SESSION['logged_in']['item_id']);
						if($ind)
							$_SESSION['logged_in']['logoimage'] = '';							
				}
				if($_FILES)	
				 $info = $this->uploadImage();			 
					if($info){								
						$ind = $this->users->Update(Array('logoimage' => $info['url']),$_SESSION['logged_in']['item_id']);
						if($ind)
							$_SESSION['logged_in']['logoimage'] = $info['url'];										
					}
				
				?>		
				<div class="formatedtext"><div class="cl"></div><p><?=$this->l('Izmaiņas ir veiktas!')?></p></div>
				<?
			}
			else{    
			?>
			<form action="" class="regform padding" method="post" enctype="multipart/form-data">
				<div class="left">
					<table>
						<tr>
							<td>
								<label for="j_nosaukums"><?=$this->l('Nosaukums')?>:</label><br>
								<input id="j_nosaukums" type="text" name="j_nosaukums" value="<?=hsc($current_data['j_nosaukums'])?>" />
							</td>
						</tr>
						<tr>
							<td>
								<label for="j_regnr"><?=$this->l('Reģ. Nr.')?>:</label><br>
								<input id="j_regnr" type="text" name="j_regnr" value="<?=hsc($current_data['j_regnr'])?>" />
							</td>
						</tr>
						<tr>
							<td>
								<label for="j_pvnregnr"><?=$this->l('PVN maksātāja Nr.')?>:</label><br>
								<input id="j_pvnregnr" type="text" name="j_pvnregnr" value="<?=hsc($current_data['j_pvnregnr'])?>" />
							</td>
						</tr>
						<tr>
							<td>
								<label for="j_juradrese"><?=$this->l('Adrese')?>:</label><br>
									<input id="j_juradrese" type="text" name="j_juradrese" value="<?=hsc($current_data['j_juradrese'])?>" />
							</td>
						</tr>
						<tr>
							<td>
								<label for="j_pilseta"><?=$this->l('Pilsēta')?>:</label><br>
								<input id="j_pilseta" type="text" name="j_pilseta" value="<?=hsc($current_data['j_pilseta'])?>" />
							</td>
						</tr>
						<tr>
							<td>
								<label for="j_telefons"><?=$this->l('Tālrunis')?>:</label><br>
								<input id="j_telefons" type="text" name="j_telefons"  value="<?=hsc($current_data['j_telefons'])?>" />
							</td>
						</tr>					
						<tr>
							<td>
								<label for="file_upload"><?=$this->l('Logo')?></label>
								<input id="file_upload" name="logoimage" type="file"  multiple="false" />		
								
							</td>
						</tr>
						<tr>
							<td>
								<div class="logoholder">
									<span class="helper"></span>
									<img class="logoImage" <?=$_SESSION['logged_in']['logoimage'] ? '' : 'style="display:none;"'?> src="<?=getThumbURL($_SESSION['logged_in']['logoimage'] , 100, 100, 6) ?>" alt="" />
								</div>
							</td>
						</tr>
					</table>
				</div>
				<div class="right">
					<table>
						<tr>
							<td>
								<label for="j_kontaktpersona" ><?=$this->l('Kontaktpersonas vārds, uzvārds')?>:</label><br>
								<input id="j_kontaktpersona" type="text" name="j_kontaktpersona" value="<?=hsc($current_data['j_kontaktpersona'])?>" />
							</td>
						</tr>
						<tr>
							<td>
								<label for="j_kontaktpersona_tel" ><?=$this->l('Kontaktpersonas tālrunis')?>:</label><br>
								<input id="j_kontaktpersona_tel" type="text" name="j_kontaktpersona_tel" value="<?=hsc($current_data['j_kontaktpersona_tel'])?>" />
							</td>
						</tr>
						<tr>
							<td>
								<label for="j_bank" ><?=$this->l('Banka')?>:</label><br>
								<input id="j_bank" type="text" name="j_bank"  value="<?=hsc($current_data['j_bank'])?>" />
							</td>
						</tr>
						<tr>
							<td>
								<label for="j_bank_acc" ><?=$this->l('Bankas kods')?>:</label><br>
								<input id="j_bank_acc" type="text" name="j_bank_acc"  value="<?=hsc($current_data['j_bank_acc'])?>" />
							</td>
						</tr>
						<tr>
							<td>
								<label for="j_bank_code" ><?=$this->l('Bankas konts')?>:</label><br>
								<input id="j_bank_code" type="text"  name="j_bank_code"  value="<?=hsc($current_data['j_bank_code'])?>" />
							</td>
						</tr>
						<tr>
							<td>
								<label for="j_epasts"><?=$this->l('E-pasts')?>:</label><br>
								<input id="j_epasts" type="text" name="j_epasts" value="<?=hsc($current_data['j_epasts'])?>" />								
							</td>
						</tr>	
						<tr>
							<td>
								<? if($_SESSION['logged_in']['logoimage']){?>
								<input name="dellogo" id="dellogo" type="checkbox"  /><label for="dellogo"><?=$this->l('Dzēst logo')?></label>
								<? } ?>
							</td>
						</tr>	
					</table>
				</div>    
				<div class="cl"></div>												
				
						
				<input type="submit" name="changecredentials" value="<?=$this->l('Atjaunot informāciju')?>" class="button"/>
			</form>
			
			<?
			}
		}
	}


  function SetProperties(){
    $this->properties = Array(
      "name"        => Array(
        "label"     => "Name:",
        "type"      => "str"
      ),
      "users" => Array(
        "label"       => "Kolekcija users:",
        "type"        => "collection",
        "collectiontype" => "shopusercollection",
        "lookup"      => GetCollections($this->site_id, "shopusercollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

    );
		$this->PostSetProperties();
  }



}