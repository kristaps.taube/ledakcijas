<?

include_once("class.component.php");

class title extends component
{

  //Class initialization
  function title($name)
  {
    parent::__construct($name);
    $this->SetProperties();
    $this->registerAsService('title');
  }

  function getLanguageStrings()
  {
    return array(
      'change' => 'Mainīt',
      'titleprop' => 'Virsraksta īpašības',
      'showpgtitle' => 'Rādīt lapas virsrakstu',
      'showcustomtitle' => 'Rādīt/rediģēt ievadīto virsrakstu'
    );
  }

  function visibleService(){

    return true;
    
  }

  function Output($isdesign=false)
  {
    $displayitem = $this->getProperty("displayitem");
    $target = $this->getProperty("target");
    if($target == -1)
    {
      $target = sqlQueryValue("SELECT parent FROM " . $this->site_id . "_pages WHERE page_id=". $this->page_id);
    }
    if(!$target)
    {
      //Default title is this page
      $target = $this->page_id;
    }

    if (!empty($this->srv_title))
    {
      $data = $this->srv_title;
    }else if($displayitem == 0)
    {
      $data = sqlQueryValue("SELECT title FROM " . $this->site_id . "_pages WHERE page_id=$target");
    }else if($displayitem == 1)
    {
      $data = sqlQueryValue("SELECT name FROM " . $this->site_id . "_pages WHERE page_id=$target");
    }else if($displayitem == 2)
    {
      $data = "/" . PagePathById($target, $this->site_id);
    }else if($displayitem == 3)
    {
      $data = $this->page_id;
    }else if($displayitem == 4)
    {
      $data = sqlQueryValue("SELECT description FROM " . $this->site_id . "_pages WHERE page_id=$target");
    }else if($displayitem == 5)
    {
      $data = $this->getProperty('customtext');
    }

    $format = $this->getProperty('format');
    if ($format)
      $data = sprintf($format, $data);

    if ($isdesign)
    {
      $url = $this->getComponentDialogLink('dialogParams', array(), $this->LS('titleprop'));
      $js = $this->getDesignIconScript($url, 400, 400);
?>
  <div onmouseover="<?=$this->name ?>_cmpShowTitle()" onmouseout="<?=$this->name ?>_cmpHideTitle()" style="background:url(/backend/gui/images/1x1.gif)">
    <?=$data ?> &nbsp;        
    <input id="title_btn_<?=$this->name ?>" type="button" value="<?=$this->LS('change') ?>" onclick="<?=$js ?>" style="font-size:11px;display:none"/>
  </div>
  <script type="text/javascript">

    function <?=$this->name ?>_cmpHideTitle()
    {
      document.getElementById('title_btn_<?=$this->name ?>').style.display = 'none';
    }
    function <?=$this->name ?>_cmpShowTitle()
    {
      document.getElementById('title_btn_<?=$this->name ?>').style.display = '';
    }

  </script>
<?
    }
    else
      echo $data;
  }

  function Design()
  {
    $displayitem = $this->getProperty("displayitem");

/*    if($displayitem == 0)
    {
      echo "Title: ";
    }else if($displayitem == 1)
    {
      echo "Name: ";
    }else if($displayitem == 2)
    {
      echo "Path: ";
    }else if($displayitem == 3)
    {
      echo "ID: ";
    }else if($displayitem == 4)
    {
      echo "";
    }else if($displayitem == 5)
    {
      echo "";
    } */
    $this->Output(1);

  }

  function dialogParams()
  {
    $value = ($this->getProperty('displayitem') == 5);
    $txt = $this->getProperty('customtext');
    
    $form = array(
      'show_pg' => array(
        'label' => '',
        'type' => 'code',
        'value' => '<label for="pg"><input type="radio" name="customtitle" value="0" id="pg"'.($value ? '' : ' checked="checked"').' onclick="document.getElementById(\'txt\').style.display=\'none\'"/> '.$this->LS('showpgtitle').'</label>'
      ),
      
      'show_custom' => array(
        'label' => '',
        'type' => 'code',
        'value' => '<label for="cust"><input type="radio" name="customtitle" value="1" id="cust"'.($value ? ' checked="checked"' : '').' onclick="document.getElementById(\'txt\').style.display=\'\'"/> '.$this->LS('showcustomtitle').'</label>'
      ),

      'customvalue' => array(
        'label'     => '',
        'type'      => 'code',
        'value'     => '<input type="text" class="formEdit" id="txt" name="customvalue" style="display:'.($value ? '' : 'none').'" value="'.htmlspecialchars($txt).'">'
      )
            
    );
    
    echo $this->getDialogFormOutput($form, 'dialogParamsSave', $this->LS('titleprop'));
  }
  
  function dialogParamsSave()
  {
    $this->setProperty('displayitem', $_POST['customtitle'] ? 5 : 0);
    $this->setProperty('customtext', $_POST['customvalue']);
  }

  function srvSetTitle($title)
  {
    $this->srv_title = $title;
  }

  function SetProperties()
  {
    if ($GLOBALS['from_backend'])
    {
      $pages = sqlQueryData("SELECT page_id,name,parent,title FROM " . $this->site_id . "_pages ORDER BY ind");
      $combopages[] = "0:This Page";
      $combopages[] = "-1:Parent";
      ListNodes($pages, $combopages, 0, 0);
    }

    $this->properties = Array(

    "name"        => Array(
      "label"     => "Name:",
      "type"      => "string"
      ),

    "displayitem"   => Array(
        "label"     => "Display:",
        "type"      => "list",
        "lookup"    => Array("0:Title", "1:Name", "2:Full Path", "3:Page ID", "4:Page Description", "5:Custom Text")
      ),

    "target"      => Array(
        "type"      => "list",
        "label"     => "Page:",
        "lookup"    => $combopages
      ),

    "customtext"        => Array(
      "label"     => "Custom text:",
      "type"      => "string"
      ),

    "format"        => Array(
      "label"     => "String format:",
      "type"      => "html"
      ),


    );

  }

  function toolbar(&$script, &$toolbars, &$btnids, &$onclick)
  {
    return true;
  }


}

?>
