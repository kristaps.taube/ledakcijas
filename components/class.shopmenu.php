<?
//Serveris.LV Constructor component
//Created by Aivars Irmejs, 2006, aivars@serveris.lv

include_once("class.shop.php");


class shopmenu extends shop
{

  var $parent;
  var $normalclass;
  var $normalstyle;
  var $normalbullet;
  var $selectedclass;
  var $selectedstyle;
  var $selectedbullet;

  //Class initialization
  function shopmenu($name)
  {
    $this->shop($name);
    $this->SetProperties();

  }

  //=========================================================================//
  // Display routines
  //=========================================================================//
  function Output()
  {
    if (!$this->urlman) return;

    $this->initLanguageStrings();
    if (!$this->initCollections()) return;

    $this->displayMenu($this->urlman->cat_id);
  }

  function Design()
  {
    $this->Output();
  }

}



