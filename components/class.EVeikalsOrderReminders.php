<?php
/**
*
*  Title: Order reminders
*  Author: Kristaps Taube <ktaube@datateks.lv>
*  Date: 04.03.2016
*  Project: E-Veikals
*
*/

require_once("class.component.php");
require_once($GLOBALS['cfgDirRoot']."library/mPDF/mpdf.php");

class EVeikalsOrderReminders extends component{

  function __construct($name){
    parent::__construct($name);
    $this->SetProperties();
  }

  function send($id = false){

    $this->emailfrom_email = option('Shop\\OrderForm\\emailfrom_email', null, 'Send order from e-mail', array(), 'constructor@datateks.lv');
    $this->order_prefix = option('Shop\\OrderForm\\order_prefix', null, 'Order prefix', $params, 'I-NET');
    $this->emailfrom_name = option('Shop\\OrderForm\\emailfrom_name', null, 'Send order from name', array(), 'Constructor');
    $this->subject = option('Shop\\OrderReminders\\reminder_subject', null, 'Reminder subject', array(), 'Rēķina apmaksas atgādinājums');
    $this->interval = option('Shop\\OrderReminders\\reminder_interval', null, 'Reminder interval(min)', array(), 30);
    $this->letter = option('Shop\\OrderReminders\\reminder_letter', null, 'Reminder letter', array("type" => "wysiwyg"), 'Labdien, <br /> Vēlamies atgādināt, ka neesam saņēmuši Jūsu rēķina ar numuru [order_nr] apmaksu. <br /><br />Ar cieņu,<br />E-Veikala administrācija');
    $this->use_reminders = option('Shop\\OrderReminders\\use', null, 'Use reminders', array("type" => 'check'), '0');

    $this->interval = (int)$this->interval;
		$this->interval = ($this->interval > 0) ? $this->interval : 30;

    if(!$this->use_reminders) return;

		if($id){
    	$orders = DB::GetTable("SELECT * FROM `".$this->orders->table."` WHERE item_id = :id", array(":id" => $id));
		}else{
    	$orders = DB::GetTable("SELECT * FROM `".$this->orders->table."` WHERE reminder_sent != 1 AND apmaksats = 0 AND apmaksas_veids != 'cash' AND datums < (NOW() - INTERVAL ".$this->interval." MINUTE)");
		}

		$this->rekini_save_path = option('rekini_save_path', 'files/rekini/', 'Orders save path', array_merge($params, Array('is_advanced' => 1)), 'order');
   	$this->order_savedir = sqlQueryValue("select dirroot from sites where site_id=".$this->site_id).$this->rekini_save_path;
    $sent_count = 0;
		foreach($orders as $order){

      $order_data = unserialize($order['order_data']);
			$order_data['order_id'] = $order['item_id'];

    	$order_text = $order['rekins'];

			$datums = explode(" ", $order['datums']);
			$datums = $datums[0];

      $exported_doc_rekins = $this->order_savedir."rekins_".$datums."_".$order['item_id'].'_'.$order['lang'].".pdf";
      $mpdf = new mPDF('');
      $mpdf->WriteHTML($order_text);
      $mpdf->Output($exported_doc_rekins, "F");

      $text = str_replace("[order_nr]", "<strong>".$this->order_prefix." ".$order['item_id']."</strong>", $this->letter);

      $sent = phpMailerSend(
        $order_text,
        $this->subject,
        $this->emailfrom_name,
       	$this->emailfrom_email,
        $order['epasts'],
        true,
        false,
        Array($exported_doc_rekins)
      );

			if($sent) $sent_count++;
			$this->orders->Update(array("reminder_sent" => 1), array("item_id" => $order['item_id']));

		}

    if(count($orders)){

    	echo "Sent: ".$sent_count."/".count($orders);

    }


  }

  function output(){


  }

  function SetProperties(){
    $this->properties = Array(

      "name"        => Array(
        "label"     => "Name:",
        "type"      => "str"
      ),

      "orders" => Array(
        "label"       => "Order collection:",
        "type"        => "collection",
        "collectiontype" => "EVeikalsOrderCollection",
      ),

    );

		$this->PostSetProperties(); 

  }

}