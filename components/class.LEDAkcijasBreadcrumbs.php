<?php

use Constructor\Url;
use Constructor\WebApp;

class LEDAkcijasBreadcrumbs extends component
{

    public function output($items = [])
    {

        $index = "<a href='".Url::get("/")."'>".WebApp::l("Sākums")."</a>";
        array_unshift($items, $index);


        return $this->render('index', ['items' => $items]);

    }

}