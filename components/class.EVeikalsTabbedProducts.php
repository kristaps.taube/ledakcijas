<?php
/**
*
*  Title: Tabbed products
*  Author: Kristaps Taube <ktaube@datateks.lv>
*  Date: 28.01.2014
*  Project: E-Veikals
*
*/

include_once("class.component.php");

class EVeikalsTabbedProducts extends component{

  function __construct($name){
    parent::__construct($name);
    $this->SetProperties();
    $this->registerAsService("TabbedProducts");
  }

  function execute(){

    $this->manager = $this->getServiceByName('shopmanager', true);
    $this->shop = EVeikalsFrontendShop::getInstance();

    $t = ($this->manager->prod)? "related" : "featured";
    $this->type = $_POST['type']? $_POST['type'] : $t;

    if($_POST['action'] == 'GetTabbedList'){
      $prods = $this->getProducts();
      echo $this->outputList($prods);
      die();
    }

  }

  function output(){

    if(!$this->initCollections()){
      echo $this->name.": Can't init collections.<br />";
      return;
    }

    if(!$this->manager || !$this->shop) return;

    if($this->manager->mobile_session) return; // do not output in mobile session

    $prods = $this->getProducts();

    ?>
    <div id='TabbedProducts'>
      <div class='tabs'>
        <? $this->outputTabs(); ?>
        <div class='cb'></div>
      </div>
      <div class='content'>
        <div class='track_wrap'>
          <div class='track'>
          <? $this->outputList($prods) ?>
          </div>
        </div>
        <div class='bullets'>
          <span class='left'></span>
          <ul></ul>
          <span class='right'></span>
        </div>
      </div>
    </div>
    <?

  }

  function getProducts(){

    $cond = array("disabled = 0", "shortcut = 0");

		$disabled_cat_ids = $this->shop->cats->getInactiveCatIds();
		if($disabled_cat_ids){
    	$cond[] = "category_id NOT IN(".implode(",", $disabled_cat_ids).")";
    }

    if($this->type == "featured"){

      $cond[] = "featured = 1";
      $cond[] = "IF(featured_from != '0000-00-00', featured_from < NOW(), 1)";
      $cond[] = "IF(featured_to != '0000-00-00', featured_to > NOW(), 1)";

    }elseif($this->type == "related"){

      return $this->shop->prods->getRelatedProducts($this->manager->prod['item_id'], $this->shop->related_prods);

    }elseif($this->type == "new"){

      $cond[] = "is_new = 1";
      $cond[] = "IF(new_until  != '0000-00-00', new_until > NOW(), 1)";

    }elseif($this->type == 'top'){

      $cond[] = "is_top = 1";

    }

    $prods = $this->shop->prods->getDBData(array("where" => implode(" AND ", $cond), "order" => "RAND()", "assoc" => true, "count" => 100));

    foreach($prods as &$prod){
      list($prod['price'], $prod['old_price'], $prod['undiscounted_price']) = $this->manager->getProductPrice($prod);
    }

    return $prods;

  }

  function outputTabs(){

    if($this->manager->prod){
    ?>
    <div id='tab-related' class='tab active'><?=$this->l("Saistītās preces")?></div>
    <div id='tab-featured' class='tab'><?=$this->l("Akcijas preces")?></div>
    <?
    }else{
    ?>
    <div id='tab-featured' class='tab active'><?=$this->l("Akcijas preces")?></div>
    <div id='tab-new' class='tab'><?=$this->l("Jaunās preces")?></div>
    <?
    }

  }

  function visibleService(){
    return true;
  }

  function outputList($prods){

    if(empty($prods)){
      echo "<div class='empty'>".$this->l("Saraksts ir tukšs")."</div>";
    }

    $i = 0; // item counter
    $count = count($prods);

    ?>
    <div class='group'>
    <? foreach($prods as $prod){ ?>
     <?
        $i += 1;
        $rating = round($this->shop->ratings->getRating($prod['item_id']));
        $featured = $this->manager->isProductFeatured($prod);
        $new = $this->manager->isProductNew($prod);
        $top = $this->manager->isProductTop($prod);
        $image = $this->manager->getProductPicture($prod);
        $image = $image ? $image : $this->shop->default_product_image;
        $can_add = $this->manager->canAddProductFromList($prod);
        $has_discount = ($featured && $prod['sale_price'] && $prod['sale_price'] != '0.00' && $prod['sale_price'] != $prod['price'])? true : false;
        $price = ($has_discount)? $prod['sale_price']  : $prod['price'];
        $discounted_price = $prod['price'];

        if($prod['has_prices']){
            $prices = $this->shop->prices->getPricesByProduct($prod['item_id']);
            if($prices){
                $price_row = $prices[0];
                $has_discount = ($featured && $price_row['saleout_price'] && $price_row['saleout_price'] != '0.00' && $price_row['saleout_price'] != $price_row['price'])? true : false;
                $price = ($has_discount)? $price_row['saleout_price']  : $price_row['price'];
                $discounted_price = $price_row['price'];
            }
        }

      ?>
      <div class='product <?=$this->type?> <?=($i % 4 == 0)? "last" : "" ?>'>
        <div class='image'>
          <div class='<?=$this->type?>'></div>
          <div class='overlay'></div>
          <div class='ratings'>
            <? if($rating > 0){ ?>
              <? for($j = 1; $j <= 5; $j++){ ?>
                <img src='/images/html/rating_star_<?=($rating >= $j)? 'full' : "empty"?>.png' alt='<?=hsc(str_replace("[rating]", $j, $this->l("Rating: [rating]")))?>' />
              <? } ?>
            <? } ?>
          </div>
          <img src='<?=getThumbUrl($image, 150, 110, 6)?>' alt='<?=hsc($prod['name_'.$this->lang])?>' class='valign'/>
        </div>
        <div class='info'>
          <div class='price <?=($has_discount)? "has_discount" : ""?>'>
            <? if($has_discount){ ?>
            <span class="discounted_price"><?=number_format($discounted_price / $_SESSION['currency']['rate'], 2)?> <?=$_SESSION['currency']['label']?></span><br />
            <? } ?>
            <span><?=number_format($price / $_SESSION['currency']['rate'], 2)?> <?=$_SESSION['currency']['label']?></span>
          </div>
          <div class='name'><a href='<?php echo Constructor\Url::get('product', ['product' => $prod]); ?>'><?=$prod['name_'.$this->lang]?></a></div>
          <div class='actions <?=$can_add? "" : "no_add"?>'>
            <a class='view' href='<?php echo Constructor\Url::get('product', ['product' => $prod]); ?>' title='<?=hsc($this->l("Skatīt"))?>'><?=$this->l("Skatīt")?></a>
            <? if($can_add){ ?>
            <a class='buy' href='<?php echo Constructor\Url::get('product', ['product' => $prod, 'action' => 'buy']); ?>' title='<?=hsc($this->l("Pirkt"))?>'><?=$this->l("Pirkt")?></a>
            <? } ?>
            <div class='cb'></div>
          </div>
        </div>
      </div>
      <? if($i % 4 == 0 && $count != $i){ ?>
      </div><div class='group'>
      <? } ?>
    <? } ?>
    </div>
    <?

  }

  function SetProperties(){
    $this->properties = Array(

      "name"        => Array(
        "label"     => "Name:",
        "type"      => "str"
      ),

    );

  }

}