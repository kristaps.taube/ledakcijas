<?
//Serveris.LV Constructor component
//Created by Aivars Irmejs, 2006, aivars@serveris.lv

include_once("class.shop.php");


class shopspecialoffer extends shop
{

  var $parent;
  var $normalclass;
  var $normalstyle;
  var $normalbullet;
  var $selectedclass;
  var $selectedstyle;
  var $selectedbullet;

  //Class initialization
  function shopspecialoffer($name)
  {
    $this->shop($name);
    $this->SetProperties();
    $this->specialofferspath = 'offers';
    $this->maxcount = 12;
  }


  function getLanguageStrings()
  {
    return array(
      '_aktualaispiedavajums' => 'Aktuālais piedāvājums',
    );
  }

  //=========================================================================//
  // Display routines
  //=========================================================================//
  function Output()
  {
    $this->InitLanguageStrings();
    if($this->initCollections())
    {
      $this->displaySpecialOffers();
    }
  }

  function Design()
  {
    $this->Output();
  }

  function outputSpecialOffers($p)
  {
    extract($p);
?>
  <h1><?=$this->LS('_aktualaispiedavajums') ?></h1>
  <div class="content_text">
    <? foreach ($offers as $o) { extract($o) ?>
      <div class="offer">
        <? if ($thumbpic) { ?>
          <a href="<?=$url ?>"><img src="<?=$thumbpic ?>" class="img" /></a>
        <? } ?>
        <div class="offer_text">
          <p><a href="<?=$url ?>"><b><?=$o['name_' . $this->lang] ?></b></a></p>
          <?=$o['shortdescription_' . $this->lang] ?>
          <p><?=$this->LS('_cena') ?>: <b><?=number_format($price, 2) ?> EUR</b></p>
        </div>
      </div>
    <? } ?>
    <? if($count > $maxcount) { ?>
      <p style="clear: both;"><a href="<?=$offerspath ?>"><?=$this->LS('visiaktualie') ?></a></p>
    <? } ?>
  </div>
<?
  }

  function displaySpecialOffers()
  {
    $priceman = $this->getServiceByName('shopmanager', true);
    $urlman = $priceman;
    if (!$urlman) return;

    $this->addToBasket();

    $requiredfields = $this->shoprequiredfields;
    list($data, $count) = $this->productcollection->getFeaturedProducts(1, $this->maxcount, 'date_added+0 DESC', false, $requiredfields);

    $tpl = Array(
      'offers' => Array(),
      'count' => $count,
      'maxcount' => $this->maxcount,
      'offerspath' => '/'.$this->lang.'/'.$urlman->catwebpath.'/get/cataloglist/specialoffers/'
    );

    foreach($data as $row)
    {
      if($row['disabled']) continue;

      $row = $urlman->getProductInfo($row, shopmanager::INFO_PRICE | shopmanager::INFO_URLS);

      $row['name'] = $row['name_'.$this->lang];

      $tpl['offers'][] = $row;
    }

    $this->outputSpecialOffers($tpl);
  }

}




?>
