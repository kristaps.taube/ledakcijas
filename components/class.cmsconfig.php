<?

require_once("class.component.php");

class cmsconfig extends component{

  function __construct($name){
    parent::__construct($name);
    $this->SetProperties();
  }

  static function processOptionBeforeDisplay($path, $opt, &$data){



    switch ($path){
      case 'CMS\\language':
        $data['lookupassoc'] = self::getLanguages();
        break;

    }

  }

  public static function getLanguages(){

		$langs = array();
		foreach(getLanguages() as $lang){
    	$langs[$lang['shortname']] = $lang['fullname'];
		}
		return $langs;

  }

}

