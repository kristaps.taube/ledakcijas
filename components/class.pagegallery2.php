<?
//Datateks.LV Constructor component
//Created by Raivo Fismeisters, 2008, raivo@datateks.lv

include_once("class.component.php");

class pagegallery2 extends component
{
  function pagegallery2($name)
  {
    parent::__construct($name);
    $this->SetProperties();
  }

  function getLanguageStrings()
  {
    return Array(
      'addimage' => 'Pievienot attēlu',
      'editimage' => 'Rediģēt attēlu'
    );
  }

  function output($isdesign=false)
  {
    if (!$this->initCollections()) return;

    $data = $this->galcol->getImagesByPage($this->page_id);
    if (!$data) return;

    $this->includeLibrary('thickbox');

    $thumbwidth = intval($this->getProperty('thumbwidth'));
    if (!$thumbwidth) $thumbwidth = 160;

    $thumbheight = intval($this->getProperty('thumbheight'));
    if (!$thumbheight) $thumbheight = 120;

    echo '<div class="pagegallery2">';
    foreach ($data as $row)
    {
      if (!$isdesign && !$row['image']) continue;

      echo '<a href="'.$row['image'].'" class="thickbox" rel="g"><img src="'.getThumbURL($row['image'], $thumbwidth, $thumbheight, 6).'" alt="'.htmlspecialchars($row['name']).'"/></a> ';

      if ($isdesign)
      {
        $edit_url = $this->getComponentDialogLink('imageEdit', Array('image_id' => $row['item_id']), $this->LS('editimage'));

        $params = Array('image_id' => $row['item_id']);
        $del_url = $this->getComponentModifierLink('imageDelete', $params);
        $down_url = $this->getComponentModifierLink('imageDown', $params);
        $up_url = $this->getComponentModifierLink('imageUp', $params);

        echo
         $this->getDesignIconOutput('down', $down_url, 150, 100) .
         $this->getDesignIconOutput('up', $up_url, 150, 100) .
         $this->getDesignIconOutput('edit', $edit_url, 450, 700) .
         $this->getDesignIconOutput('del', $del_url, 150, 100 );

      }
    }
    echo '</div>';
  }

  function design()
  {
    $addurl = $this->getComponentDialogLink('imageEdit', Array(), $this->LS('addimage'));
    $addjs = $this->getDesignIconScript($addurl, 400, 500);
    echo '<input type="button" onclick="'.$addjs.'" value="'.$this->LS('addimage').'"/>';
    $this->output(true);
  }

  function imageEdit()
  {
    if (!$this->initCollections()) return;
    $id = intval($_GET['image_id']);

    $form_data = $this->galcol->getDBProperties();

    if ($id)
    {
      $item = $this->galcol->getItemByIdAssoc($id);
      if (!$item) return;

      foreach ($form_data as $key => $field)
      {
        $form_data[$key]['value'] = $item[$key];
      }

    }

    $this->galcol->processFormBeforeDisplay($form_data, !$id);
    echo $this->getDialogFormOutput($form_data, 'imageEditSave', $_GET['title'], Array('image_id' => $id));
  }

  function imageEditSave()
  {
    if (!$this->initCollections()) return;
    $id = intval($_GET['image_id']);

    $item = $this->galcol->getItemAssocFromPost();

    if ($_FILES['image']['size'] > 0)
    {
      $p = getFilePathFromLink($this->site_id, $item['image']);
      $type = strtolower( substr($p, strrpos($p, '.') + 1) );
      resizeKeepAspectUploadedImage($p, $type, 1280, 1280, $p, 6);
    }

    $this->galcol->processItemBeforeSave($id, $item, !$id, $msg);
    if ($id)
    {
      $this->galcol->changeItemByIdAssoc($id, $item);
    }
    else
    {
      $item['page_id'] = $this->page_id;
      $ind = $this->galcol->addItemAssoc($item);
    }

  }

  function imageDelete()
  {
    if (!$this->initCollections()) return;
    $id = intval($_GET['image_id']);

    $this->galcol->delDBItem($id);
  }

  function imageDown()
  {
    if (!$this->initCollections()) return;
    $id = intval($_GET['image_id']);
    $this->galcol->moveItemDown($this->galcol->getItemInd($id));
  }

  function imageUp()
  {
    if (!$this->initCollections()) return;
    $id = intval($_GET['image_id']);
    $this->galcol->moveItemUp($this->galcol->getItemInd($id));
  }


  function initCollections()
  {
    if (!($this->galcol = $this->initPropertyCollection('galcol') )) return false;

    return true;
  }

  function Toolbar()
  {
    return true;
  }

  function SetProperties()
  {
    $this->properties = Array(
      "name"        => Array(
        "label"     => "Name:",
        "type"      => "str"
      ),

      "thumbwidth"        => Array(
        "label"     => "Thumb width:",
        "type"      => "str",
        "value"     => 160
      ),

      "thumbheight"        => Array(
        "label"     => "Thumb height:",
        "type"      => "str",
        "value"     => 120
      ),

      "galcol" => Array(
        "label"       => "Page gallery collection:",
        "type"        => "collection",
        "collectiontype" => "pagegallery2collection",
        "lookup"      => GetCollections($this->site_id, "pagegallery2collection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

    );
  }
}


?>