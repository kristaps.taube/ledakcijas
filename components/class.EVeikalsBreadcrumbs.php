<?php
/**
*
*  Title: Breadcrumbs
*  Author: Kristaps Taube <ktaube@datateks.lv>
*  Date: 24.01.2014
*  Project: E-Veikals
*
*/

include_once("class.component.php");

class EVeikalsBreadcrumbs extends component{

  function __construct($name){
    parent::__construct($name);
    $this->SetProperties();

    $this->registerAsBackendService("Breadcrumbs");

  }

  function output(){

    $this->manager = $this->getServiceByName("shopmanager");
    if($this->manager->mobile_session && !$this->manager->prod) return;

    $cache_name_parts = array();
    $cache_name_parts[] = $this->page_id;
		if($this->manager->cat_id) $cache_name_parts[] = "c".$this->manager->cat_id;
		if($this->manager->prod) $cache_name_parts[] = "p".$this->manager->prod['item_id'];

  	if($this->start_cache(implode("_", $cache_name_parts))) return;

    if(!$this->initCollections()){
      echo $this->name.": Can't init collections.<br />";
      return;
    }

    $this->news = $this->getServiceByName("news");

    if($this->manager->mobile_session && !$this->manager->prod) return;
    #if(!$this->manager->cat) return;

    $data = array();

    $path = PagePathRowsById($this->page_id, $this->site_id);
    foreach($path as $page){
      if($page['level'] == 0) continue; // no first level pages
      $data[] = array("name" => $page['title'], "url" => "/".PagePathById($page['page_id'], $this->site_id));
    }

    if($this->manager->cat_id){
      foreach($this->manager->cat_path as $cat){
        $data[] = array("name" => $cat['title_'.$this->lang], "url" => $this->manager->getCategoryURL($cat['item_id']));
      }
    }

    if($this->manager->prod){
      $data[] = array("name" => $this->manager->prod['name_'.$this->lang], "url" => $this->manager->getProductURL($this->manager->prod));
    }

    if($this->news->item){

      $data[] = array("name" => $this->news->item['title'], "url" => $this->news->getURL('news', $this->news->item['item_id']));

    }

    $count = count($data);
    ?>
    <div id='BreadCrumbs'>
    <? foreach($data as $link){ ?>
      <a href="<?=$link['url']?>" class='<?=(++$i == $count)? "active" : ""?>' title='<?=hsc($link['name'])?>'><?=$link['name']?></a>
    <? } ?>
    </div>
    <?

    $this->end_cache();

  }

  function SetProperties(){
    $this->properties = Array(

      "name"        => Array(
        "label"     => "Name:",
        "type"      => "str"
      ),


    );
  }



}



?>