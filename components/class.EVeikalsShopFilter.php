<?php
/**
*
*  Title: Shop filter
*  Author: Kristaps Taube <ktaube@datateks.lv>
*  Date: 23.01.2013
*  Project: E-Veikals
*
*/

include_once("class.component.php");

class EVeikalsShopFilter extends component{

    function __construct($name)
    {

        parent::__construct($name);
        $this->SetProperties();

        $this->registerAsService("ShopFilter");
        $this->registerAsBackendService("ShopFilter");

        $this->cat = isset(Constructor\WebApp::$app) && Constructor\WebApp::$app->request->rule instanceof UrlRules\CatalogUrlRule && Constructor\WebApp::$app->request->rule->getCategory() ? Constructor\WebApp::$app->request->rule->getCategory() : false;
        $this->prod = isset(Constructor\WebApp::$app) && Constructor\WebApp::$app->request->rule instanceof UrlRules\CatalogUrlRule ?  Constructor\WebApp::$app->request->rule->getProduct() : false;
        $cat_id = $this->cat ? $this->cat['item_id'] : -1;

        $this->active_filters = $_SESSION['shop']['filter'][$cat_id]? $_SESSION['shop']['filter'][$cat_id] : array();
        $this->active_brands = $_SESSION['shop']['brandfilter'][$cat_id]? $_SESSION['shop']['brandfilter'][$cat_id] : array();

        $this->price_filter['starting_to'] = $_SESSION['shop']['pricefilter'][$cat_id]['to'];
        $this->price_filter['starting_from'] = $_SESSION['shop']['pricefilter'][$cat_id]['from'];

    }

  function filterProducts($cats){

    if(empty($this->active_filters) && empty($this->active_brands) && !$this->price_filter['starting_to'] && !$this->price_filter['starting_from']) return false; // no restrictions

    $grouped_filters = $this->Filters->getGroupedByIds($this->active_filters);

    $groups = array();

    $prods = $this->manager->prodscol->getDBData(array("where" => "category_id IN (".implode(",", $cats).")"));

    $pids = array();
    foreach($prods as $prod){
        $pids[] = $prod['item_id'];
    }
    $prod_filter_ids = $this->ProdFilterRel->getFilterIdsByProdIds($pids);

    /*
      Same group filters are "OR-ed" with each other
      Whole "OR-ed" filter group is being "AND-ed" with other group "OR-ed" filters
      Doesn't that blow your mind? It surely does mine!
    */
    if(!empty($grouped_filters)){
      foreach($prods as $key => $prod){

        //$prod_filters = $this->ProdFilterRel->getFilterIds($prod['item_id']);
        $prod_filters = isset($prod_filter_ids[$prod['item_id']]) ? $prod_filter_ids[$prod['item_id']] : [];

        foreach($grouped_filters as $group => $filters){

          $ok = false;

          foreach($filters as $filter){
            if(in_array($filter, $prod_filters)){ // or-ing
              $ok = true;
              break;
            }
          }

          $groups[$group] = $ok;

        }

        foreach($groups as $check){

          if(!$check){ // and-ing
            unset($prods[$key]);
            break;
          }

        }

      }
    }

    $ids = array();
    foreach($prods as $prod){

      if(!empty($this->active_brands) && !in_array($prod['brand_id'], $this->active_brands)){ // filter out by brands
        continue;
      }

      if($this->price_filter['starting_to']){
        if(round($prod['price']/$_SESSION['currency']['rate'],2) > $this->price_filter['starting_to']){
          continue;
        }
      }

      if($this->price_filter['starting_from']){
        if(round($prod['price']/$_SESSION['currency']['rate'],2) < $this->price_filter['starting_from']){
          continue;
        }
      }

      $ids[] = $prod['item_id'];
    }


    return $ids;

  }

  function ClearCategoryFilter($cat = false){

    if(!$cat){
        $cat = isset($this->cat) && $this->cat ? $this->cat['item_id'] : -1;
    }

    $_SESSION['shop']['filter'][$cat] = array();
    unset($_SESSION['shop']['filter'][$cat]);

  }

  function init(){

    if($this->inited === true) return true;

    $this->manager = shopmanager::getInstance();

    if($this->manager){
        $this->manager->initCollections();
    }else{
        echo "<pre>".print_r(debug_backtrace(),true)."</pre>";
        die("No ShopManager in ShopFilter init() method");
    }

    $this->cat_output_type = option('Shop\\cat_output_type', null, 'Category output type', array(
      'type' => 'list',
      'value_on_create' => 'categories',
      'fieldparams' => array(
        'lookupassoc' => array(
          'categories' => 'Categories',
          'products' => 'Products'
        )
      )
    ));

    $this->inited = $this->manager ? true : false;

    return $this->inited;

  }

    public function getFilterGroupsForCat($cat)
    {

        static $cache;

        $catIds = [];

        if(!$cat) return [];
        $cat = (is_array($cat))? $cat : $this->manager->catscol->getFromCache($cat);

        if(isset($cache[$cat['item_id']])) return $cache[$cat['item_id']];
        $catIds[] = $cat['item_id'];

        while($cat['parent']){
            $cat = $this->manager->catscol->getFromCache($cat['parent']);
            $catIds[] = $cat['item_id'];
        };

        $ancestor_cats = $this->manager->catscol->getAncestorIds($cat);
        $catIds = array_merge($catIds, $ancestor_cats);

        $data = $this->CatFilterGroupRel->getByCatIds($catIds);
        $ids = [];
        foreach($data as $row){
            if(!in_array($row['filter_group_id'], $ids)){
                $ids[] = $row['filter_group_id'];
            }
        }

        if($ids){
            $cache[$cat['item_id']] = $this->FilterGroups->getByIds($ids);
            return $cache[$cat['item_id']];
        }

        $cache[$cat['item_id']] = Array();
        return Array();

    }

  function hasActiveFilter(){
    return (!empty($this->active_filters) || !empty($this->active_brands) || $this->price_filter['starting_to'] || $this->price_filter['starting_from'])? true : false;
  }

  function outputMobileFilters(){

    if(!$this->init()){
      echo $this->name.": Can't init collections.<br />";
      return;
    }

    // do not output filters if we aren't in any category or viewing product
    if(!$this->manager->cat || $this->manager->prod) return;

    $ancestor_cats = $this->manager->catscol->getAncestorIds($this->manager->cat_id);
    $ancestor_cats[] = $this->manager->cat_id;

    $groups = $this->getFilterGroupsForCat();

    $predecessor_cats = $this->manager->catscol->getPredecessorIds($this->manager->cat_id);
    $predecessor_cats[] = $this->manager->cat_id;

    $max_price = ceil($this->manager->prodscol->getHighestProductPrice($predecessor_cats)/$_SESSION['currency']['rate']);

    $starting_to = ($this->price_filter['starting_to'])? $this->price_filter['starting_to'] : $max_price;
    $starting_from = ($this->price_filter['starting_from'])? $this->price_filter['starting_from'] : 0;

    $price_slider_open = ($this->price_filter['starting_to'] || $this->price_filter['starting_from']);

    ?>
    <form method='post' action=''>
      <div class="filter_groups">
        <div class='group <?=($price_slider_open)? "open active" : "" ?>'>
          <div class='head'><div><?=$this->l("Cena")?></div></div>
          <div class='content center'>
            <div class='inner_wrap'>
              <?=$this->l("no")?>: <input type='text' class='price' name='price_from' value='<?=$starting_from?>' />
              <?=$this->l("līdz")?>: <input type='text' class='price' name='price_to' value='<?=$starting_to?>' />
              <div id='priceslider'></div>
              <input type='hidden' name='pricesliderchanged' value="<?=($price_slider_open? 1 : 0)?>" />
            </div>
          </div>
        </div>
        <? if(!empty($groups)){ ?>
          <? foreach($groups as $group){ ?>
            <?
              $filters = $this->Filters->getByGroup($group['item_id'], $this->ProdFilterRel, $this->manager->prodscol, $ancestor_cats);
              $open = false;
              foreach($filters as $filter){
                if(in_array($filter['item_id'], $this->active_filters)){
                  $open = true;
                  break;
                }
              }
            ?>
            <div class="group <?=($open)? "open active" : ""?>">
              <div class='head'><div><?=$group['title_'.$this->lang]?></div></div>
              <div class='content'>
                <div class='inner_wrap'>
                  <? foreach($filters as $filter){ ?>
                    <div class='line'>
                      <input id='filter<?=$filter['item_id']?>' type='checkbox' name='filter[]' value='<?=$filter['item_id']?>' <?=(in_array($filter['item_id'], $this->active_filters))? "checked='checked'" : "" ?> />
                      <label for='filter<?=$filter['item_id']?>'><?=$filter['param_title_'.$this->lang]?> (<?=$filter['count']?>)</label>
                    </div>
                  <? } ?>
                </div>
              </div>
            </div>
          <? } ?>
        <? } ?>
        <div class="group">
          <div class='head'><div><?=$this->l("Kārtošana")?></div></div>
          <div class='content'>
            <div class='inner_wrap'>
              <div class='line'>
                <select name='product_order' class='styled'>
                  <option value='auto'><?=$this->l("Automātiski")?></option>
                  <option value='cheapest' <?=($_SESSION['shop']['product_order'] == 'cheapest')? "selected='selected'" : ""?>><?=$this->l("Lētākās vispirms")?></option>
                  <option value='expensive' <?=($_SESSION['shop']['product_order'] == 'expensive')? "selected='selected'" : ""?>><?=$this->l("Dārgākās vispirms")?></option>
                  <option value='alphabet' <?=($_SESSION['shop']['product_order'] == 'alphabet')? "selected='selected'" : ""?>><?=$this->l("Pēc alfabēta")?></option>
                </select>
                <div class='cb'></div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class='clear'>
        <input type='submit' name='Filter' value='<?=$this->l("Filtrēt")?>' />
        <? if(!empty($this->active_filters)){ ?>
        <input type='submit' name='ClearCatalogFilter' value='<?=$this->l("Atcelt filtrēšanu")?>' />
        <? } ?>
      </div>
      <input type='hidden' name='CatalogFilter' value='1' />
    </form>
    <script type='text/javascript'>

      var slider_conf = [];
      slider_conf.max_price = <?=json_encode((float)$max_price)?>;
      slider_conf.starting_to = <?=json_encode($starting_to)?>;
      slider_conf.starting_from = <?=json_encode($starting_from)?>;

    </script>
    <?

  }

  public function output($category = false){

    if($this->prod) return;

    if(!$this->init()){
      echo $this->name.": Can't init collections.<br />";
      return;
    }

    $groups = $this->getFilterGroupsForCat($category);

    $ancestor_cats = $this->manager->catscol->getAncestorIds($category['item_id']);
    $ancestor_cats[] = $category['item_id'];

    $predecessor_cats = $this->manager->catscol->getPredecessorIds($category['item_id']);
    $predecessor_cats[] = $category['item_id'];

    // do not output filters if there is category list in the middle
    if($this->cat_output_type == 'categories' && count($predecessor_cats) > 1){
      return;
    }

    $brands = $this->manager->prodscol->getBrandsByCategoryIds($ancestor_cats, $this->manager->brandscol);

    $max_price = ceil($this->manager->prodscol->getHighestProductPrice($predecessor_cats)/$_SESSION['currency']['rate']);

    $starting_to = ($this->price_filter['starting_to'])? $this->price_filter['starting_to'] : $max_price;
    $starting_from = ($this->price_filter['starting_from'])? $this->price_filter['starting_from'] : 0;

    $price_slider_open = ($this->price_filter['starting_to'] || $this->price_filter['starting_from']);

    if($groups){
        $group_ids = array();
        $group_filters = array();
        foreach($groups as $group){
            $group_ids[] = $group['item_id'];
        }

        if(!empty($group_ids)){
            $group_filters = $this->Filters->getByGroups($group_ids, $this->ProdFilterRel, $this->manager->prodscol, $predecessor_cats);
        }

    }

    ?>
    <div class='box'>
      <div class='title'><?=$this->l("Filtrs")?></div>
      <div id="CatalogFilter">
        <form method='post' action='<?php echo Constructor\Url::get('category', ['category' => $category, 'action' => 'filter'])?>'>
          <ul class='menu'>
            <li <?=($price_slider_open)? "class='open'" : "" ?>>
              <div class='head'><div class='valign_p'><?=$this->l("Cena")?></div></div>
              <div class='content center'>
                <?=$this->l("no")?>: <input type='text' class='price' name='price_from' value='<?=$starting_from?>' />
                <?=$this->l("līdz")?>: <input type='text' class='price' name='price_to' value='<?=$starting_to?>' />
                <div id='priceslider'></div>
                <input type='hidden' name='pricesliderchanged' value="<?=($price_slider_open? 1 : 0)?>" />
              </div>
            </li>
            <? if(!empty($brands)){ ?>
              <li <?=(!empty($this->active_brands))? "class='open'" : ""?>>
                <div class='head'><div class='valign_p'><?=$this->l("Ražotājs")?></div></div>
                <div class='content'>
                <? foreach($brands as $brand){ ?>
                <div class='line'>
                        <input id='brand_filter_<?=$brand['id']?>' type='checkbox' name='brands[]' value='<?=$brand['id']?>' <?=(in_array($brand['id'], $this->active_brands))? "checked='checked'" : "" ?> />
                        <label for='brand_filter_<?=$brand['id']?>'><?=$brand["brandname"]?> (<?=$brand['count']?>)</label>
                </div>
                <? } ?>
                </div>
              </li>
            <? } ?>
            <? foreach($groups as $group){ ?>
              <?
                $filters = $group_filters[$group['item_id']];
                $open = false;
                foreach($filters as $filter){
                  if(in_array($filter['item_id'], $this->active_filters)){
                    $open = true;
                    break;
                  }
                }
              ?>
              <li <?=($open)? "class='open'" : ""?>>
                <div class='head'><div class='valign_p'><?=$group['title_'.$this->lang]?></div></div>
                <div class='content'>
                <? foreach($filters as $filter){ ?>
                    <div class='line'>
                        <input id='filter_<?=$filter['item_id']?>' type='checkbox' name='filter[]' value='<?=$filter['item_id']?>' <?=(in_array($filter['item_id'], $this->active_filters))? "checked='checked'" : "" ?> />
                        <label for='filter_<?=$filter['item_id']?>'><?=$filter['param_title_'.$this->lang]?> (<?=$filter['count']?>)</label>
                    </div>
                <? } ?>
                </div>
              </li>
            <? } ?>
          </ul>
          <div class='clear'>
            <input type='submit' name='Filter' value='<?=$this->l("Filtrēt")?>' />
            <? if(!empty($this->active_filters) || !empty($this->active_brands) || $this->price_filter['starting_to'] || $this->price_filter['starting_from']){ ?>
            <input type='submit' name='ClearCatalogFilter' value='<?=$this->l("Atcelt filtrēšanu")?>' />
            <? } ?>
          </div>
          <input type='hidden' name='CatalogFilter' value='1' />
        </form>
      </div>
    </div>
    <script type='text/javascript'>

      var slider_conf = [];
      slider_conf.max_price = <?=json_encode((float)$max_price)?>;
      slider_conf.starting_to = <?=json_encode($starting_to)?>;
      slider_conf.starting_from = <?=json_encode($starting_from)?>;

    </script>
    <?

  }

  function SetProperties(){

    $this->properties = Array(

      "name"        => Array(
        "label"     => "Name:",
        "type"      => "str"
      ),

      "FilterGroups" => Array(
        "label"       => "Filter group collection:",
        "type"        => "collection",
        "collectiontype" => "EVeikalsFilterGroups",
        "lookup"      => GetCollections($this->site_id, "EVeikalsFilterGroups"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "Filters" => Array(
        "label"       => "Filters collection:",
        "type"        => "collection",
        "collectiontype" => "EVeikalsFilters",
        "lookup"      => GetCollections($this->site_id, "EVeikalsFilters"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "ProdFilterRel" => Array(
        "label"       => "Prod. filter rel. collection:",
        "type"        => "collection",
        "collectiontype" => "EVeikalsProdFilterRel",
        "lookup"      => GetCollections($this->site_id, "EVeikalsProdFilterRel"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "CatFilterGroupRel" => Array(
        "label"       => "Cat. filter group rel. collection:",
        "type"        => "collection",
        "collectiontype" => "EVeikalsCatFilterGroupRel",
        "lookup"      => GetCollections($this->site_id, "EVeikalsCatFilterGroupRel"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

    );

  }

}

?>