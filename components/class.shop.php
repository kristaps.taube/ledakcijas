<?php

use Constructor\WebApp;
use LEDAkcijas\CategoryAmountStepCollection;
use LEDAkcijas\ProductAmountPriceCollection;

include_once("class.component.php");

class shop extends component
{
  var $parent;
  var $normalclass;
  var $normalstyle;
  var $normalbullet;
  var $selectedclass;
  var $selectedstyle;
  var $selectedbullet;
  var $defaultsortcol;
  var $catwebpath;

  //Class initialization
  function shop($name)
  {

    parent::__construct($name);
    $this->SetProperties();

    //constants
    //$this->products_in_page = option('Shop\\products_in_page', null, 'Products in page', array('value_on_create' => 2) );
    $this->daysnew = option('Shop\\daysnew', null, 'Product days as new', array('value_on_create' => 30) );

    $this->custom_product_list_collection = 'customimages_collection';
    $this->shoprequiredfields = Array(); //set in InitLanguageStrings()

    $this->Filters = Array('searchq', 'search', 'price_min', 'price_max', 'brand_id', 'flag', 'toppurchased', 'topsearches');

  }

  //========================================================================//
  // Language strings
  //========================================================================//
  function InitLanguageStrings()
  {

    $this->lang = WebApp::$app->getLanguage();
    $this->shoprequiredfields = Array('name_' . $this->lang);

  }

  function getLanguageStrings()
  {
    $ls = Array(
      '_sakums'  => 'Sākums',
      '_jaunakieprodukti' => 'Jaunākie produkti',
      '_meklesanasrezultati' => 'Meklēšanas rezultāti',
      '_new' => 'NEW',
      '_cenaveikalos' => 'Vecā cena',
      '_cena' => 'Cena',
      '_cena_iepak' => 'Cena iepakojumā',
      '_pievienotgrozam' => 'Pievienot grozam',
      '_lapas' => 'Lapas',
      '_nekasnetikaatrasts' => 'Nekas netika atrasts.',
      '_aktualaispiedavajums' => 'Aktuālais piedāvājums',
      '_krasas' => 'Krāsas',
      'visiaktualie' => 'Visi aktuālie piedāvājumi',
      'filtresana' => 'Filtrēšana',
      'cena_no' => 'No',
      'cena_lidz' => 'Līdz',
      'brand' => 'Ražotājs',
      'filtret' => 'Filtrēt',
      'cena_gab' => 'Cena gabalā',
      'cena_iepak' => 'Cena iepakojumā',
      'krasa' => 'Preces krāsa',
      'kods' => 'Preces kods',
      'skatitpreci' => 'Skatīt preci',
      'navprece' => 'Prece nav atrasta.'
    );
    return $ls;
  }

  function Execute(){
             
    $this->shopman = $this->getServiceByName('shopmanager');
    if(!$this->shopman) $this->shopman = $this->getBackendServiceByName('shopmanager');
    $this->urlman = $this->shopman;

    $this->shopman->initCollections();
    $this->shopman->processShopPath();

    if($this->shopman->prod_id){
      $GLOBALS['force_title'] = $this->shopman->prod['name_'.$this->lang];
    }elseif($this->shopman->cat){
      $GLOBALS['force_title'] = $this->shopman->cat['title_'.$this->lang];
    }

    if (get_class($this) != 'shop') return;

    $this->InitLanguageStrings();
    if (!$this->initCollections()) return;

    $this->sort_products_by = option('Shop\\sort_products_by', null, 'Sort products by', array(
      'type' => 'list',
      'fieldparams' => array(
        'lookupassoc' => array(
          'auto' => 'Manually',
          'alphabet' => 'Name',
          'cheapest' => 'Price ascending',
          'expensive' => 'Price descending',
        )
      )
    ));

  }

  //=========================================================================//
  // Display routines
  //=========================================================================//
  function Output()
  {
    if (!$this->shopman) { echo 'shop: Service shopmanager not found!'; return; }

    if ($this->shopman->prod_id)
    {
      $prod = $this->productcollection->getItemByIdAssoc($this->shopman->prod_id);
      if ($prod)
      {
        $GLOBALS['meta_keywords'] .= $prod['keywords_' . $this->lang];
        $GLOBALS['meta_description'] .= strip_tags($prod['meta_description_'.$this->lang]);
        $GLOBALS['additional_header_tags'] .= $prod['additional_header_tags_'.$this->lang];
      }
    }
    else if ($this->shopman->cat_id)
    {
      $path = $this->categorycollection->getCategoryPath($this->shopman->cat_id);
      if (!count($path)) return;

      $GLOBALS['meta_description'] .= $path[count($path)-1]['description_' . $this->lang];
      foreach ($path as $cat)
      {
        $GLOBALS['meta_keywords'] .= $cat['keywords_' . $this->lang].' ';
      }

      $cat = $path[count($path)-1];
      if($cat['additional_header_tags_'.$this->lang]){
        $GLOBALS['additional_header_tags'] = $cat['additional_header_tags_'.$this->lang];
      }

    }

    $this->addToBasket();
    $shopman = $this->shopman;

    foreach ($shopman->cat_path as $cat)
    {
      if ($cat['disabled'])
      {
        header('Location: /');
        die;
      }
    }

    if ($prod)
    {
      $this->displaySingleProduct($prod);
    }else
    {
/*
      $show_prods = !empty($_GET['searchq']) || !empty($_GET['cataloglist']);
      if (!$show_prods)
      {
        $subcats = $this->categorycollection->getAllSubCategoriesRows($shopman->cat_id);
        $show_prods = empty($subcats);
      }
      */
      if (1 || $show_prods)
        $this->displayProducts($shopman->cat_id);
      else
        $this->displaySubcategories($subcats);

    }
  }

  function addToBasket()
  {
    if(isset($_GET['addtobasket']))
    {
      $prodid = intval($_GET['addtobasket']);
      $count = intval($_GET['count']);
      if (!$count) $count = 1;

      if($prodid)
      {
        include_once("class.shopbigcart.php");
        shopbigcart::addCartItem(Array('id' => $prodid, 'price_id' => intval($_GET['price_id']) ), $count);

        header('Location: ?page='.intval($_GET['page']) );
        die();
      }
    }

  }

  function output_cat_tree_option($cat){

    $pre = str_repeat("&nbsp;&nbsp;", $cat['lvl']);

    echo "<option value='".$cat['item_id']."'>".$pre.$cat['title_'.$this->lang]."</option>";

    if($cat['kids']){
      foreach($cat['kids'] as $kid){
        $this->output_cat_tree_option($kid);
      }
    }

  }

  function outputCategoryPath($p)
  {
    extract($p);
?>
  <? if(count($catlinks)) { ?>
    <h1>
    <? for($f = 0; $f < count($catlinks); $f++) { ?>
      <?=($f > 0) ? ' &raquo; ' : '' ?>
      <? if ($this->shopman->prod_id || $f < count($catlinks)-1) { ?>
        <a href="<?=$catlinks[$f]['href'] ?>"><?=$catlinks[$f]['title'] ?></a>
      <? } else { ?>
        <?=$catlinks[$f]['title'] ?>
      <? } ?>
    <? } ?>
    </h1>
  <? } ?>
<?
  }

  function ajax_copyProduct(){

    $prod_id = intval($_GET['prod_id']);

    if (!$this->initDesign()) return;

    $prod = $this->productcollection->getItemByIDAssoc($prod_id, true);
    if(!$prod) return;

    unset($prod['item_id']);
    unset($prod['ind']);
    unset($prod['cat_ind']);

		$i = 0;
		foreach(getLanguages() as $key => $data){
     	if(++$i == 1){ // only first
     		$prod['name_'.$key] = $prod['name_'.$key]." COPY";
     	}
      $prod['url_'.$key] = $this->shopman->createProductUrl($prod, $kry, false);
    }

    $id = $this->productcollection->add($prod);

    // copying images
    foreach($this->imagescol->getImagesForProduct($prod_id) as $image){
      unset($image['item_id']);
      unset($image['ind']);
      $image['product_id'] = $id;
      $this->imagescol->addItemAssoc($image);
    }

    // copying volumes
    foreach($this->pricescol->getPricesByProduct($prod_id) as $price){
      unset($price['item_id']);
      unset($price['ind']);
      $price['product_id'] = $id;
      $this->pricescol->addItemAssoc($price);
    }

    // copying related prods
    foreach($this->relatedprodscol->getProductRelations($prod_id) as $relation){
      $this->relatedprodscol->addItemAssoc(array("prod_id" => $id, "related_id" => $relation['related_id']));
    }

    // copying filter relations
    foreach($this->ProdFilterRel->getByProduct($prod_id) as $rel){
      $this->ProdFilterRel->AddItemAssoc(array("product_id" => $id, "filter_id" => $rel['filter_id']));
    }

    // copying sizes
    foreach($this->product_sizes->getSizeIdsForProduct($prod_id) as $size){
      $this->product_sizes->addItemAssoc(array("prod_id" => $id, "size_id" => $size));
    }

    // copying colors
    foreach($this->product_colors->getColorIdsForProduct($prod_id) as $color){
      $this->product_colors->addItemAssoc(array("prod_id" => $id, "color_id" => $color));
    }

    $_GET['prod_id'] = $id;
    $this->ajax_getSingleProductRow();

  }

  function displayCategoryPath($cat_id, $prod_id)
  {
    $cattitle = '';
    $catlinks = Array();
    if($_GET['searchq'] || $_GET['search'])
    {
      $cattitle = $this->LS('_meklesanasrezultati');
      //$catlinks[] = Array('href' => '/'.$this->lang.'/?searchq='.$_GET['searchq'], 'title' => $cattitle);
    }else if($_GET['cataloglist'] == 'specialoffers')
    {
      $cattitle = $this->LS('_aktualaispiedavajums');
    }else if($cat_id)
    {
      $cattitle = $this->LS('_sakums');
      $catlinks[] = Array('href' => '/'.$this->lang.'/', 'title' => $cattitle);
      $data = $this->categorycollection->getCategoryPath($cat_id);
      $this->cat_path = $data;
      foreach($data as $row)
      {
        $cattitle = $row['title_' . $this->lang];
        $catlinks[] = Array('href' => $this->shopman->getCategoryURL($row['item_id']), 'title' => $cattitle);
      }
      if($prod_id)
      {
        $product = $this->productcollection->getItemByIdAssoc($prod_id);
        if($product)
        {
          $catlinks[] = Array('href' => $this->shopman->getProductURL($product), 'title' => $product['name_' . $this->lang]);
          $cattitle = $product['name_' . $this->lang];
        }
      }
    }else
    {
      //first page - nothing here
    }
    $this->outputCategoryPath(Array(
      'catlinks' => $catlinks,
      'cattitle' => $cattitle
    ));

  }

  function displaySubcategories($subcats)
  {
    foreach($subcats as $row)
    {
      echo '<a href="'.$this->shopman->getCategoryURL($row['item_id']).'" style="margin-left:'.($row['lvl'] * 15).'px">' . $row['title_' . $this->lang] . '</a><br/> ';
    }
  }


  function displayMenu($cat_id)
  {
    $data = $this->categorycollection->getAllCategories();
    $lastlevel = 0;
    //build parent reference tree
    foreach($data as $key => $row)
    {
      if($row['parent'] != 0)
      {
        $item = $row;
        $parentkey = $key;
        for($f = $key; $item['lvl'] >= $row['lvl']; $f--)
        {
          $item = $data[$f];
          $parentkey = $f;
        }
        $data[$key]['parent'] = $parentkey;
      }else
      {
        $data[$key]['parent'] = -1;
      }
    }

    //build open branch reference
    foreach($data as $key => $row)
    {
      if($cat_id == $row['item_id'])
      {
        $activeitem = $key;
      }
    }
    //open all branches from active item up
    $f = $activeitem;
    while($f > -1)
    {
      if($data[$f]['title_' . $this->lang])
        $data[$f]['open'] = true;
      $f = $data[$f]['parent'];
    }
    //delete unneeded branches
    foreach($data as $key => $row)
    {
      if($row['lvl'] > 0)
      {
        if(!$data[$row['parent']]['open'])
        {
          $data[$key]['disabled'] = true;
        }
      }
    }
    //draw menu
    echo '<ul>';
    foreach($data as $row)
    {
      $disabled = (boolean)$row['disabled'];
      $parent = $row['parent'];
      while ($parent > 0)
      {
        $cat = $data[$parent];
        if ($cat['disabled'])
        {
          $disabled = true;
          break;
        }
        $parent = $cat['parent'];
      }

      if(!$disabled && $row['title_' . $this->lang])
      {
        $row['lvl'] += 1;
        if($lastlvl < $row['lvl'] && $lastlvl != 0)
        {
          echo '
          <ul>';
        }else if($lastlvl == $row['lvl'])
        {
          echo '</li>';
        }else if($lastlvl != 0)
        {
          echo '</li>';
          for($f = $row['lvl']; $f < $lastlvl; $f++)
            echo '</ul></li>';
        }
        echo '
        <li><a'.($cat_id == $row['item_id']?' id="active"':'').' href="'.$this->shopman->getCategoryURL($row['item_id']).'">' . $row['title_' . $this->lang] . '</a>';
        $lastlvl = $row['lvl'];
      }
    }
    if($lastlvl != 0)
    {
      echo '</li>';
      for($f = 1; $f < $lastlvl; $f++)
        echo '</ul></li>';
    }
    echo '</ul>';
  }

  function getProductsList(&$data, &$count, $cat_id, $pg, &$brands=null)
  {
    $orderby = $this->sort_products_by;
    if ($orderby == 'name')
      $orderby = 'name_'.$this->lang;
    else if ($orderby)
      $orderby = 'p.'.$orderby;

    if ($orderby == 'price')
      $orderby = ' CASE
      WHEN
        featured = 1 AND sale_price > 0 AND (
          (featured_from = "0000-00-00" AND featured_to = "0000-00-00") OR
          IF (featured_from != "0000-00-00" AND featured_to != "0000-00-00",
            NOW() >= featured_from AND NOW() < featured_to,
            IF (featured_from != "0000-00-00",
              NOW() >= featured_from,
              NOW() < featured_to
            )
          )
        )
      THEN sale_price
      ELSE price
      END
      ';


    $cond = Array();

    $_SESSION['shopfilter'] = Array();
    foreach ($this->Filters as $key)
    {
      $_SESSION['shopfilter'][$key] = $_GET[$key];
    }

    $use_filter = false;
    foreach ($_SESSION['shopfilter'] as $key => $val)
    {
      if ($key == 'search') continue;
      $use_filter |= (bool)$val;
    }

    $brandscondkey = null;
    if ($use_filter)
    {
      $flt = $_SESSION['shopfilter'];
      $pricefield = 'price';
      if (!empty($flt['price_min'])) $cond[] = '('.$pricefield.'+0 >= '.floatval($flt['price_min']).')';
      if (!empty($flt['price_max'])) $cond[] = '('.$pricefield.'+0 <= '.floatval($flt['price_max']).')';
      if ($flt['brand_id'])
      {
        $brandscondkey = count($cond);
        $cond[] = Array('brand_id', '="'.$flt['brand_id'].'"');
      }

      if ($flt['flag']) switch ($flt['flag'])
      {
        case 'new': $cond[] = Array('is_new', '= 1'); break;
        case 'featured': $cond[] = Array('featured', '= 1'); break;
        case 'freeship': $cond[] = Array('is_freeship', '= 1'); break;
        case 'seasonal': $cond[] = Array('is_seasonal', '= 1'); break;
        case 'recommended': $cond[] = Array('is_recommended', '= 1'); break;
        default: $use_filter = false; break;
      }

      if ($flt['toppurchased'])
      {
        $orderby = 'times_purchased DESC';
        $cond[] = Array('times_purchased', '> 0');
      }

      if ($flt['topsearches'])
      {
        $orderby = 'times_searched DESC';
        $cond[] = Array('times_searched', '> 0');
      }
    }

    foreach ($this->shoprequiredfields as $name)
    {
      $cond[] = Array($name, '<> ""');
    }

    $cond[] = Array('disabled', '= 0');

    if ($_GET['searchq'])
    {
      $cond[] = Array('shortcut', '= 0');
      $searchcols = Array('name_'.$this->lang, 'description_'.$this->lang, 'long_description_'.$this->lang, 'name_lit_'.$this->lang, 'p.code');
      if ($this->pricescol)
      {
        $searchcols[] = 'pr.code';
        $searchcols[] = 'pr.name';
      }

      if (!$this->productcollection->condSearchProducts($cond, $_GET['searchq'], $searchcols))
        return true;
    }
    else if ($_GET['cataloglist'] == 'specialoffers')
    {
      $cond[] = '( (featured=1 AND (
        (featured_from = "0000-00-00" AND featured_to = "0000-00-00") OR
        IF (featured_from != "0000-00-00" AND featured_to != "0000-00-00",
          NOW() >= featured_from AND NOW() < featured_to,
          IF (featured_from != "0000-00-00",
            NOW() >= featured_from,
            NOW() < featured_to
          )
        )
      )) OR is_new=1)';
      $cond[] = Array('shortcut', '= 0');
    }
    else if ($cat_id)
    {
      $catcondkey = count($cond);
      $cond[] = Array('category_id', '='.$cat_id);
    }
    else if ($use_filter)
    {
    }
    else
      return false;

    list($data, $count) = $this->productcollection->queryProducts($cond, $pg, $this->products_in_page, $orderby, '*', QUERYPROD_DATACOUNT, $this->pricescol);

    if ($_GET['searchq'] && !$count)
      return true;

    if ($cat_id && !$count)
    {
      //$this->displaySubcategories($cat_id);
      $cat_ids = $this->categorycollection->getAllSubCategories($cat_id);

      if ($cat_ids)
      {
        unset($cond[$catcondkey]);
        $cond[] = Array('category_id', ' IN ('.implode(',', $cat_ids).')');

        list($data, $count) = $this->productcollection->queryProducts($cond, $pg, $this->products_in_page, $orderby, '*', QUERYPROD_DATACOUNT, $this->pricescol);
      }
    }

    $_SESSION['searched_products'] = Array();
    if ($_GET['searchq'])
    {
      foreach ($data as $key => $row)
      {
        $_SESSION['searched_products'][] = (int)$row['item_id'];
      }
    }

    if ($this->brandscol && is_array($brands))
    {
      if ($brandscondkey !== null)
        unset($cond[$brandscondkey]);

      $bdata = $this->productcollection->queryProducts($cond, 0, 0, '', 'DISTINCT brand_id',  QUERYPROD_DATA, $this->pricescol);
      foreach ($bdata as $b)
      {
        $brands[] = $b['brand_id'];
      }
    }

    return true;
  }

  function outputProductsList($p)
  {
    extract($p);
?>
  <?=$msg ?>
  <div id="offers">
    <? foreach ($prods as $prod) { extract($prod); ?>
    <div class="akcija offer">
      <? if ($old_price) { ?>
        <div class="old_price"><?=$this->LS('_cena') ?><br/><?=number_format($old_price, 2) ?></div>
        <div class="price"><?=$this->LS('_cena') ?><br/><?=number_format($price, 2) ?></div>
      <? } ?>
      <a href="<?=$url ?>" class="pic"><img src="<?=getThumbURL($picture ? $picture : '/images/logo_gray.png', 138, 67, 6) ?>" alt="<?=$name ?>"/></a>
      <div class="over">
        <p><a href="<?=$url ?>"><?=$this->LS('skatitpreci') ?></a></p>
        <div class="price2"><?=$this->LS('_cena') ?> <?=number_format($price, 2) ?> Ls</div>
      </div>
      <div class="title2"><a href="<?=$url ?>">
        <h3><?=$name ?></h3>
        <p><?=$description ?></p>
      </a></div>
    </div>
    <? } ?>
  </div>
  <? if ($pages) { ?>
  <div id="pages">
    <?=$pages ?>
  </div>
  <? } ?>

<? /*     <a href="<?=$addbasket_url ?>" class="btn2" onclick="return addToBasketLink(this)">      */ ?>
  <script type="text/javascript">

  function addToBasketLink(a)
  {
    window.location = $(a).attr('href') + '&page=' + shop.page;
    return false;
  }

  </script>

<?
  }

  function displayProductsList($data, $count, $cat_id, $pg)
  {
    if(!$data) return;

    $tpl = Array(
      'prods' => Array(),
      'lang' => $this->lang,
      'brandopts' => $brands,
    );

    $pagecount = ceil(intval($count) / intval($this->products_in_page));
    if ($pg > $pagecount) $pg = $pagecount;

    //display pages
    if($pagecount > 1)
    {
      $this->onAjaxQueryChange(Array('page'), '
        var n = parseInt(query.page);
        if (isNaN(n) || n < 1) n = 1
        else if (n > shop.pagecount) n = shop.pagecount;

        cmpPost("'.$this->name.'", "displayProducts", "catcontent", shop.filter, { cat_id: '.$cat_id.', page: n }, function(data){
          shop.page = n;
          return true;
        })

      ');

      $g = $_GET;
      unset($g['page']);
      unset($g['shopcat']);
      unset($g['shopprod']);
      unset($g['_component']);
      unset($g['_method']);
      $params = http_build_query($g);
      $cat_url = $this->shopman->cat_url.'?'.$params;

      $flt = Array();
      foreach ($_SESSION['shopfilter'] as $key => $val)
      {
        if (!empty($val)) $flt[$key] = $val;
      }

      $this->addScript('
        var shop = {
          page: '.intval($pg).',
          pagecount: '.$pagecount.',
          cat_url: '.json_encode($cat_url).',
          filter: '.json_encode($flt).'
        }
     ');

      $vis = ($pg > 1) ? 'visible' : 'hidden';
      $tpl['pages'] .= '<a id="shopprevpg" href="'.$cat_url.'&page='.($pg - 1).'"' . ' onclick="return cmpQuery({page: shop.page-1})" style="visibility:'.$vis.'">&nbsp;&nbsp;&laquo;</a> ';
      $tpl['pages'] .= '<span id="pages_numbers">';
      for($f = max(1, $pg - 7); $f < min($pagecount + 1, $pg + 7); $f++)
      {
        $tpl['pages'] .= '<a id="pg'.$f.'"  href="'.$cat_url.'&page='.$f.'"' . ($pg == $f ? ' class="active"' : '') . ' onclick="return cmpQuery({page:'.$f.'})">'.$f.'</a> ';
      }
      $tpl['pages'] .= '</span>';
      $vis = ($pg < $pagecount) ? 'visible' : 'hidden';
      $tpl['pages'] .= '<a id="shopnextpg" href="'.$cat_url.'&page='.($pg + 1).'"' . ' onclick="return cmpQuery({page: shop.page+1})" style="visibility:'.$vis.'">&raquo;&nbsp;&nbsp;</a> ';

    }

    $n = 0;
    foreach($data as $row){
      if ($row['disabled']) continue;

      $prod = $this->shopman->getProductInfo($row, shopmanager::INFO_PRICE | shopmanager::INFO_URLS);

      $prod['name'] = &$prod['name_'.$this->lang];
      $prod['description'] = &$prod['description_'.$this->lang];
      $prod['long_description'] = &$prod['long_description_'.$this->lang];
      $prod['n'] = $n;

      $tpl['prods'][] = $prod;
      $n++;
    }

    ob_start();
    $this->outputProductsList($tpl);
    $c = ob_get_contents();
    ob_end_clean();
    return $c;

  }

  function ajax_displayProducts()
  {
    $pg = max($_POST['page'], 1);

    $tpl = Array('msg' => '');
    if($this->getProductsList($data, $count, intval($_POST['cat_id']), $pg))
    {
      if($_GET['searchq'] || $_GET['search'])
      {
        if(!$data)
          echo $this->LS('_nekasnetikaatrasts');
      }
    }else
      return;

    echo $this->displayProductsList($data, $count, intval($_POST['cat_id']), $pg);
  }

  function outputProducts($p)
  {
    extract($p);
     if (!$prodlist) { ?>
    <h3><?=$msg ?></h3>
<? }
?>
  <div id="catalog">
    <? $this->displayCategoryPath($this->shopman->cat_id, $this->shopman->prod_id) ?>

  <div id="filter">
    <form action="" method="get">
    <? foreach ($this->Filters as $key) { if (empty($_GET[$key])) continue; ?>
    <input type="hidden" name="<?=$key ?>" value="<?=htmlspecialchars($_GET[$key]) ?>"/>
    <? } ?>
    <span class="arrow"><?=$this->LS('filtresana') ?></span>
    <div class="bg"><table>
      <tr>
        <td class="first"> </td>
        <td class="second"><?=$this->LS('_cena') ?></td>
        <td class="third"><?=$this->LS('brand') ?></td>
      </tr>
      <tr>
        <td class="first"><?=$this->LS('cena_no') ?></td>
        <td class="second">
          <input type="text" class="text" name="price_min" value="<?=$_SESSION['shopfilter']['price_min'] ?>"/>
          &nbsp; Līdz:
          <input type="text" class="text" name="price_max" value="<?=$_SESSION['shopfilter']['price_max'] ?>"/>
        </td>
        <td class="third">
          <select name="brand_id">
            <?=$brandopts ?>
          </select>
          &nbsp;
          <input type="submit" value="<?=$this->LS('filtret') ?>" class="filter_submit"/>
        </td>
      </tr>
    </table></div>
    </form>
  </div>
  <div id="catcontent"><?=$prodlist ?></div>

  </div>
<?
  }

  function displayProducts($cat_id)
  {
    $this->includeLibrary('jquery');

    $pg = max($_GET['page'], 1);
    $tpl = Array(
      'msg' => '',
      'pages' => ''
    );

    $brand_ids = Array();
    if($this->getProductsList($data, $count, $cat_id, $pg, $brand_ids))
    {
      if($_GET['searchq'] || $_GET['search'])
      {
        if(!$data)
          $tpl['msg'] = $this->LS('_nekasnetikaatrasts');
      }
    }else
    {
      //kataloga saakums, redirekteesimies uz indeksa lapu
      if(!$this->output_plain)
      {
        header('Location: /' . $this->lang . '/');
        die();
      }
    }

    $brandsdata = $this->brandscol ? $this->brandscol->getBrandnames($brand_ids) : Array();
    $brands = '<option value="0">-</option>';
    foreach ($brandsdata as $brow)
    {
      $sel = ($_SESSION['shopfilter']['brand_id'] == $brow['item_id']) ? ' selected="selected"' : '';
      $brands .= '<option value="'.$brow['item_id'].'"'.$sel.'>'.$brow['brandname'].'</option>';
    }
    $tpl['brandopts'] = $brands;

    $tpl['prodlist'] = $this->displayProductsList($data, $count, $cat_id, $pg);

    $this->outputProducts($tpl);
  }

  function unitsText($prod, $longname=false)
  {
    static $cache = Array();


//    $count = empty($prod['skaits_iepakojuma']) ? 1 : $prod['skaits_iepakojuma'];
    if (!$prod['units'])
      return null;

    if (!$this->unitscol)
    {
      $this->unitscol = $this->initPropertyCollection('units_collection');
      if (!$this->unitscol)
        return $count;

    }

    $ckey = $this->site_id.'_'.$this->unitscol->collection_id.'_'.$prod['units'];
    if (!isset($cache[$ckey]))
      $cache[$ckey] = $this->unitscol->getItemByIdAssoc($prod['units']);

    if ($longname)
      return $cache[$ckey]['unit_'.$this->lang];

    return $count.' '.$cache[$ckey]['shortname_'.$this->lang];
  }

  function outputSingleProduct($p)
  {
    extract($p);

    if ($p['disabled'])
    {
      echo '<p>'.$this->LS('navprece').'</p>';
      return;
    }

    $this->includeLibrary('jquery');

    $this->addScript('

    $(function()
    {
      $("#addtocart").click(function()
      {
        var prices = $("#prodform input[name=price]");
        if (prices.length == 0)
        {
          window.location = "?addtobasket='.$p['item_id'].'";
          return false;
        }

        var price_id = prices.filter(":checked").val();
        if (price_id == undefined) return false;

        window.location = "?addtobasket='.$p['item_id'].'&price_id="+price_id;

        return false;
      });

    });

    ');

?>
  <div id="catalog">
    <? $this->displayCategoryPath($this->shopman->cat_id, $this->shopman->prod_id) ?>

  <div id="product">
    <form action="" method="post" id="prodform">
    <div class="panel">
      <div class="mid">
        <div class="left">
          <h3><?=$p['name_'.$lang] ?></h3>
          <a  href="<?=$picture ?>" class="bigthumb thickbox" rel="g"><img src="<?=getThumbURL($picture ? $picture : '/images/logo_gray.png', 202, 151, 3) ?>" alt="<?=$p['name_'.$lang] ?>"/></a>
    <? if(count($pictures)) { ?>
          <div class="images">
      <? $i = 0; foreach($pictures as $pic) { ?>
        <? if ($i % 3 == 0) { ?><div><? } ?>
        <a title="<?=$pic['title'] ?>" href="<?=$pic['image'] ?>" class="thickbox" rel="g"><img src="<?=getThumbURL($pic['image'], 67, 43, 6) ?>" alt="<?=$pic['title'] ?>" /></a>
        <? if ($i % 3 == 2) { ?></div><? } ?>
      <? $i++; } ?>
      <? if ($i % 3 > 0) { ?></div><? } ?>
            <div class="clear"></div>
          </div>
    <? } ?>
          <table>
            <? if ($prices) { for ($i=0; $i<count($prices); $i++) { $pr = $prices[$i]; ?>
              <tr>
                <td id="price<?=$i ?>"><div><label><input type="radio" name="price" value="<?=$pr['item_id'] ?>"/> <?=$pr['name'] ?></label></div></td>
                <td class="value"><div><?=number_format($pr['price'], 2) ?> Ls</div></td>
              </tr>
            <? } } else { ?>
              <tr>
                <td id="price0"><div><a href="javascript:selectPrice(0)" class="active"><?=$this->LS('cena_gab') ?></a></div></td>
                <td class="value"><div><?=number_format($price, 2) ?> Ls</div></td>
              </tr>
              <? if ($pack_price) { ?>
              <tr>
                <td id="price1"><div><a href="javascript:selectPrice(1)"><?=$this->LS('cena_iepak') ?></a></div></td>
                <td class="value"><div><?=number_format($pack_price, 2) ?> Ls</div></td>
              </tr>
              <? } ?>
            <? } ?>
            <? if ($colorsdata) { ?>
              <tr>
                <td><div><?=$this->LS('krasa') ?></div></td>
                <td class="value"><div>
                  <select name="color" id="prodcolor">
                  <? foreach ($colorsdata as $c) { ?>
                    <option value="<?=$c['colorcode'] ?>"><?=$c['color_'.$lang] ?></option>
                  <? } ?>
                  </select>
                  </div>
                </td>
              </tr>
            <? } ?>
            <tr>
              <td><div><?=$this->LS('kods') ?></div></td>
              <td class="value"><div id="prodcode"><?=$code ?></div></td>
            </tr>
          </table>
        </div>
        <div class="right">
          <?=$p['description_' . $this->lang] ?>
          <? if ($old_price) { ?>
          <div class="akcija">
            <div class="old_price"><?=$this->LS('_cena') ?><br/><?=number_format($old_price, 2) ?></div>
            <div class="price"><?=$this->LS('_cena') ?><br/><?=number_format($price, 2) ?></div>
          </div>
          <? } ?>
        </div>
        <div class="clear"></div>
      </div>
      <div class="bottom">
        <a href="?addtobasket=<?=$p['item_id'] ?>" class="arrow" id="addtocart"><?=$this->LS('_pievienotgrozam') ?></a>
      </div>
      </form>
    </div>
  </div>
  </div>
<?
  }
  function displaySingleProduct($prod_id)
  {
    $prod = $this->shopman->getProductInfo($prod_id, shopmanager::INFO_ALL);
    if (!$prod) return;
    $prod_id = (int)$prod['item_id'];

    if ($_SESSION['searched_products'] && in_array($prod_id, $_SESSION['searched_products']))
      $this->productcollection->addTimesSearched($prod_id, 1);

    $prod['lang'] = $this->lang;
    $this->outputSingleProduct($prod);
  }


  function indexData($lastindextime){

    $data = array();
    $check = $this->initCollections();

    if($this->name == 'shop'){

      $products = $this->prods->getDBData(array("where" => "disabled = 0"));

      foreach($products as $p){

        $data_text = $p['name_'.$this->lang];
        $data_text .= " ".strip_tags($p['description_'.$this->lang]);
        $data_text .= " ".strip_tags($p['long_description_'.$this->lang]);

        $data[$this->prods->collection_id][$p['item_id']] = array(
          'query' => $this->manager->getProductURL($p, '', true),
          'title' => $p['name_'.$this->lang],
          'data' => $data_text
        );

      }

      $categories = $this->cats->getDBData(array("where" => "disabled = 0"));

      foreach($categories as $c){
        $data[$this->cats->collection_id][$c['item_id']] = array(
          'query' => $this->manager->getCategoryURL($c['item_id'], '', true),
          'title' => $c['title_'.$this->lang],
          'data' => $c['name_'.$this->lang].' '.strip_tags($c['description_'.$this->lang])
        );
      }

    }

    return $data;

  }

  //=========================================================================//
  // Design routines
  //=========================================================================//

  function initDesign(){



    $this->InitLanguageStrings();

    if (!$this->initCollections()){
      echo "Collections haven't been specified!";
      return false;
    }

    $this->shopman = shopmanager::getInstance();
    $this->shopman->initCollections();
    $this->Breadcrumbs = EVeikalsBreadcrumbs::getInstance();

    return true;

  }

  function Design()
  {
    if (!$this->initDesign()){

        echo "Can't init design";
        return;

    }

    $this->designCategories();

  }

  function ajax()
  {
    if (empty($_GET['ajaxmethod'])) die('Method not defined!');

    $method = 'ajax_' . $_GET['ajaxmethod'];

    if (method_exists($this, $method))
    {
      $this->$method();
    }
    else
      die('Method not found!');

    die;
  }

  function ajax_sortableChange(){

    $product_id = intval($_GET['product_id']);
    $ind = intval($_GET['ind']);

    if (!$this->initDesign()) return;

    $prod = $this->productcollection->getItemByIDAssoc($product_id);

    if(!$prod) return;

    if($prod['cat_ind'] > $ind){ // moving our product up

      // move prev items down by one
      sqlQuery("UPDATE `".$this->productcollection->table."` SET cat_ind = cat_ind + 1 WHERE category_id = ".$prod['category_id']." AND cat_ind >= ".$ind." AND cat_ind < ".$prod['cat_ind']);

    }else{ // moving our product down

      // move next items up by one
      sqlQuery("UPDATE `".$this->productcollection->table."` SET cat_ind = cat_ind - 1 WHERE category_id = ".$prod['category_id']." AND cat_ind <= ".$ind." AND cat_ind > ".$prod['cat_ind']);

    }

    // place our product
    sqlQuery("UPDATE `".$this->productcollection->table."` SET cat_ind = ".$ind." WHERE item_id = ".$prod['item_id']);

    die("1");
  }

  function ajax_changeDisabled()
  {
    $product_id = intval($_GET['product_id']);
    if (!$product_id || !isset($_GET['value'])) die('no params');

    if (!$this->initDesign()) return;

    $this->productcollection->changeDisabled($product_id, $_GET['value'] ? 1 : 0);
		
		$user = $_SESSION['userdata']['username'];
		$now = date('Y-m-d H:i:s');
		$log_data = array(
			'prod'=>$product_id,
			'change_type'=>'Update',
			'user'=>$user,
			'change_comment'=>'Product "'.$prod['name_lv'].'"  '.($_GET['value'] ? 'disabled' : 'enabled'),
			'change_time'=>$now);
		$this->ProdLogger->change_log->Insert($log_data);
  }

  function ajax_moveProductUp()
  {
    $product_id = intval($_GET['product_id']);
    if (!$product_id) die('no params');

    if (!$this->initDesign()) return;

  	$this->productcollection->moveUpProduct($product_id);
  }

  function ajax_moveProductDown()
  {
    $product_id = intval($_GET['product_id']);
    if (!$product_id) die('no params');

    if (!$this->initDesign()) return;

  	$this->productcollection->moveDownProduct($product_id);
  }

  function ajax_deleteProduct()
  {
    $product_id = intval($_GET['product_id']);
    if (!$product_id) die('no params');

    if (!$this->initDesign()) return;


    $dirs = array();
    $shopman = shopmanager::getInstance();
    $prod = $this->productcollection->getItemByIdAssoc($product_id);
    $images = $shopman->getProductPictures($prod);
    foreach ($images as $p)
    {
      $dir = getFilePathFromLink($this->site_id, dirname($p['picture']));
      $id = file_get_contents($dir.'/product_id.txt');

      if ($id && $id == $product_id)
      {
        $this->deleteImage($p['picture']);
      }

      if (!in_array($dir, $dirs))
        $dirs[] = $dir;

    }

    foreach ($dirs as $dir)
    {
      @unlink($dir.'/product_id.txt');
      @rmdir($dir.'/th');
      @rmdir($dir);
    }

    $this->ProdFilterRel->DeleteByProd($product_id);

    $this->productcollection->deleteProduct($product_id);
		
		$user = $_SESSION['userdata']['username'];
		$now = date('m.d.Y H:i:s');
		$log_data = array(
			'prod'=>$product_id,			
			'change_type'=>'Delete',
			'user'=>$user,
			'change_comment'=>'Product "'.$prod['name_lv'].'" deleted',
			'change_time'=>$now);
		$this->ProdLogger->change_log->Insert($log_data);
    
		echo '1';
  }

  function ajax_saveProduct(){
		$this->initCollections();
    $prod_id = intval($_GET['prod_id']);
    if (!$prod_id){
      if ($_GET['prod_id'] != 'new') return;
    }

    if (!$this->initDesign()) return;

    $props = $this->productcollection->getDBProperties();
    $changes = array();
    foreach ($props as $key => $f){
      if (isset($_POST[$key]))
        $changes[$key] = $_POST[$key];
    }

    if (empty($changes))
      die('0');


    foreach ($props as $key => $prop){
      if ($key != 'disabled' && $prop['type'] == 'check'){
        $changes[$key] = (int)(boolean)$_POST[$key];
      }else if (isset($changes[$key]) && $prop['column_type'] == 'date'){
        $t = strtotime($changes[$key]);
        $changes[$key] = $t > 0 ? date('Y-m-d', $t) : NULL;
      }
    }

    $changes['price'] = strtr($changes['price'], ',', '.');
    $changes['price'] = $changes['price'] ? $changes['price'] : "0.00";
    $changes['sale_price'] = strtr($changes['sale_price'], ',', '.');
    $changes['sale_price'] = $changes['sale_price'] ? $changes['sale_price'] : "0.00";

    $prod = $this->productcollection->getItemByIdAssoc($prod_id);
    if ($prod['category_id'] != $changes['category_id']){
      $changes['cat_ind'] = 1 + $this->productcollection->getMaxCatInd($changes['category_id']);
    }

    if ($prod_id){
        $changes['date_modified'] = time();
        $changes = $this->productcollection->processPropertyDataTypes($changes);
        $this->productcollection->Update($changes, array("item_id" => $prod_id));
        $this->Breadcrumbs->deleteAllCache();
        $user = $_SESSION['userdata']['username'];
        $now = date('Y-m-d H:i:s');
        $log_data = array(
        'prod'=>$prod_id,
        'change_type'=>'Update',
        'user'=>$user,
        'change_comment'=>'Product "'.$prod['name_lv'].'"  changed data',
        'change_time'=>$now);

        $this->ProdLogger->change_log->Insert($log_data);
		}else{

      $prod_id = $this->productcollection->add($changes);
			$user = $_SESSION['userdata']['username'];
			$now = date('Y-m-d H:i:s');
			$log_data = array(
				'prod'=>$prod_id,
				'change_type'=>'Insert',
				'user'=>$user,
				'change_comment'=>'Product "'.$changes['name_lv'].'"  added',
				'change_time'=>$now);
			$this->ProdLogger->change_log->Insert($log_data);
      $pid = -SessionManager::$session_row_id;
      $this->imagescol->replaceProductId($pid, $prod_id);

      if ($this->pricescol)
        $this->pricescol->replaceProductId($pid, $prod_id);

      $prices = $this->pricescol->getPricesByProduct($prod_id);
      if($prices){
        $this->productcollection->changeProduct($prod_id, array("has_prices" => 1));
      }

      $images = $this->imagescol->getImagesForProduct($prod_id);
      if ($images){
      	
        $prodpath = $this->getProductFolder(
          $this->categorycollection->getCategoryPath($prod['category_id']),
          $prod
        );

        $prodpath = 'images/products/' . $prodpath;
        forceDirectories($this->site_id, $prodpath);

        $id_path = getFilePathFromLink($this->site_id, $prodpath.'/product_id.txt');
        if (!file_exists($id_path)){
          file_put_contents($id_path, $prod_id);
        }

        $tmp_dir = trim(getFilePathFromLink($this->site_id, 'images/products/tmp'), '/');

        foreach ($images as $key => $p){
          $path = getFilePathFromLink($this->site_id, $p['image']);

          if (trim(dirname($path), '/') == $tmp_dir){
            deleteThumbnails($p['image']);

            $newurl = $prodpath.basename($p['image']);
            $destpath = getFilePathFromLink($this->site_id, $newurl);
            rename($path, $destpath);

            $images[$key]['image'] = '/'.$newurl;
          }
        }

        foreach ($images as $i => $p){
        	if(!$i){
						$this->productcollection->Update(array("picture" => $p['image']), array("item_id" => $prod_id));
						$user = $_SESSION['userdata']['username'];
						$now = date('m.d.Y H:i:s');
						$log_data = array(
							'prod'=>$prod_id,
							'change_type'=>'Update',
							'user'=>$user,
							'change_comment'=>'Product "'.$prod['name_lv'].'" picture '.$p['image'].' update',
							'change_time'=>$now);
						$this->ProdLogger->change_log->Insert($log_data);
					}


          $this->imagescol->changeItemByIdAssoc($p['item_id'], array(
            'image' => $p['image']
          ));

					if($i){
						$user = $_SESSION['userdata']['username'];
						$now = date('m.d.Y H:i:s');
						$log_data = array(
							'prod'=>$prod_id,
							'change_type'=>'Update',
							'user'=>$user,
							'change_comment'=>'Product "'.$prod['name_lv'].'"  image '.$p['image'].' update',
							'change_time'=>$now);
						$this->ProdLogger->change_log->Insert($log_data);
					}
        }

      }

    }

    $prod = $this->productcollection->getItemByIdAssoc($prod_id);
    $prod_changes = array();

    // amount prices
    if(isset($_POST['amount_prices']) && is_array($_POST['amount_prices'])){

        $amountprices = ProductAmountPriceCollection::getInstance();
        $prod_amountprices = $amountprices->getByProduct($prod_id);

        foreach($_POST['amount_prices'] as $step_id => $price){

            if(isset($prod_amountprices[$step_id])){
                $amountprices->Update(['price' => $price], $prod_amountprices[$step_id]['item_id']);
            }else{
                $amountprices->Insert(['price' => $price, 'category_step_id' => $step_id, 'product_id' => $prod_id]);
            }

        }

    }


    // process custom fields
	  $prod_custom_fields = $this->ProdCustomFields->getProdValues($prod_id);
		$processed_custom_field_ids = array();

    if(isset($_POST['customfield_texts']) && is_array($_POST['customfield_texts'])){

        foreach($_POST['customfield_texts'] as $field_id => $data){
            $field_data = array();
            $processed_custom_field_ids[] = $field_id;
            foreach($data as $lang => $value){
                $field_data['value_'.$lang] = $value;
            }

            if(isset($prod_custom_fields[$field_id])){
                $this->ProdCustomFields->Update($field_data, array("item_id" => $prod_custom_fields[$field_id]['item_id']));
            }else{
                $field_data['prod_id'] = $prod_id;
                $field_data['field_id'] = $field_id;
                $this->ProdCustomFields->Insert($field_data);
            }

        }

    }

    if(isset($_POST['customfields']) && is_array($_POST['customfields'])){

        foreach($_POST['customfields'] as $field_id => $value){

            $field_data = array();
            $processed_custom_field_ids[] = $field_id;

            foreach(getLanguages() as $lang => $ldata){
                $field_data['value_'.$lang] = $value;
            }

            if(isset($prod_custom_fields[$field_id])){

                $this->ProdCustomFields->Update($field_data, array("item_id" => $prod_custom_fields[$field_id]['item_id']));

            }else{

                $field_data['prod_id'] = $prod_id;
                $field_data['field_id'] = $field_id;
                $this->ProdCustomFields->Insert($field_data);

            }

        }
    }
		// process custom fields END

    // process filters
    $prod_filters = $this->ProdFilterRel->getFilterIds($prod_id);

    // adding new
    if(isset($_POST['filters']) && is_array($_POST['filters'])){
        foreach($_POST['filters'] as $filter){
          if(!in_array($filter, $prod_filters)){
            $this->ProdFilterRel->AddItemAssoc(Array("product_id" => $prod_id, "filter_id" => $filter));
    				$filtername = $this->Filters->getValue(array('what'=>'param_title_lv','where'=>'item_id='.$filter));
    				$user = $_SESSION['userdata']['username'];
    				$now = date('m.d.Y H:i:s');
    				$log_data = array(
    					'prod'=>$prod_id,
    					'change_type'=>'Insert',
    					'user'=>$user,
    					'change_comment'=>'Product "'.$prod['name_lv'].'"  added filter '.$filtername,
    					'change_time'=>$now);
    				$this->ProdLogger->change_log->Insert($log_data);
    			}
        }

        // remove missing
        foreach($prod_filters as $filter){
          if(!in_array($filter, $_POST['filters'])){
            $this->ProdFilterRel->DeleteByProdAndFilter($prod_id, $filter);
    				$filtername = $this->Filters->getValue(array('what'=>'param_title_lv','where'=>'item_id='.$filter));
    				$user = $_SESSION['userdata']['username'];
    				$now = date('m.d.Y H:i:s');
    				$log_data = array(
    					'prod'=>$prod_id,
    					'change_type'=>'Delete',
    					'user'=>$user,
    					'change_comment'=>'Product "'.$prod['name_lv'].'"  removed filter '.$filtername,
    					'change_time'=>$now);
    				$this->ProdLogger->change_log->Insert($log_data);
          }
        }
    }
    // process filters end

    /* COLORS START */
    $prod_colors = $this->product_colors->getColorIdsForProduct($prod_id);

    // adding new
    $colors = isset($_POST['colors']) ? $_POST['colors'] : [];
    foreach($colors as $color){
      if(!in_array($color, $prod_colors)){
        $this->product_colors->AddItemAssoc(Array("prod_id" => $prod_id, "color_id" => $color));
				$colorname = $this->colors->getValue(array('what'=>'title_lv','where'=>'item_id='.$color));
				$user = $_SESSION['userdata']['username'];
				$now = date('m.d.Y H:i:s');
				$log_data = array(
					'prod'=>$prod_id,			
					'change_type'=>'Insert',
					'user'=>$user,
					'change_comment'=>'Product "'.$prod['name_lv'].'"  added color '.$colorname,
					'change_time'=>$now);
				$this->ProdLogger->change_log->Insert($log_data);
      }
    }

    // remove missing
    foreach($prod_colors as $color){
      if(!in_array($color, $colors)){
        $this->product_colors->DeleteByProdAndColor($prod_id, $color);
				$colorname = $this->colors->getValue(array('what'=>'title_lv','where'=>'item_id='.$color));
				$user = $_SESSION['userdata']['username'];
				$now = date('m.d.Y H:i:s');
				$log_data = array(
					'prod'=>$prod_id,			
					'change_type'=>'Delete',
					'user'=>$user,
					'change_comment'=>'Product "'.$prod['name_lv'].'"  removed color '.$colorname,
					'change_time'=>$now);
				$this->ProdLogger->change_log->Insert($log_data);
      }
    }

    if(empty($colors) && $prod['has_colors']){
    	$prod_changes['has_colors'] = 0;
		}elseif(!empty($colors) && !$prod['has_colors']){
			$prod_changes['has_colors'] = 1;
		}

		/* COLORS END */

    /* SIZES START */
    $prod_sizes = $this->product_sizes->getSizeIdsForProduct($prod_id);

    // adding new
    $sizes = isset($_POST['sizes']) ? $_POST['sizes'] : [];
    foreach($sizes as $size){
      if(!in_array($size, $prod_sizes)){
        $this->product_sizes->AddItemAssoc(Array("prod_id" => $prod_id, "size_id" => $size));
				$sizename = $this->sizes->getValue(array('what'=>'title_lv','where'=>'item_id='.$size));
				$user = $_SESSION['userdata']['username'];
				$now = date('m.d.Y H:i:s');
				$log_data = array(
					'prod'=>$prod_id,			
					'change_type'=>'Insert',
					'user'=>$user,
					'change_comment'=>'Product "'.$prod['name_lv'].'"  added size '.$sizename,
					'change_time'=>$now);
				$this->ProdLogger->change_log->Insert($log_data);
      }
    }

    // remove missing
    foreach($prod_sizes as $size){
      if(!in_array($size, $sizes)){
        $this->product_sizes->DeleteByProdAndSize($prod_id, $size);
				$sizename = $this->sizes->getValue(array('what'=>'title_lv','where'=>'item_id='.$size));
				$user = $_SESSION['userdata']['username'];
				$now = date('m.d.Y H:i:s');
				$log_data = array(
					'prod'=>$prod_id,			
					'change_type'=>'Delete',
					'user'=>$user,
					'change_comment'=>'Product "'.$prod['name_lv'].'"  removed size '.$sizename,
					'change_time'=>$now);
				$this->ProdLogger->change_log->Insert($log_data);
      }
    }

		// prod data
    if(empty($sizes) && $prod['has_sizes']){
    	$prod_changes['has_sizes'] = 0;
		}elseif(!empty($sizes) && !$prod['has_sizes']){
			$prod_changes['has_sizes'] = 1;
		}

    /* SIZES END */

    // chekojam, vai URL jau eksiste un galaa kabinam id, lidz neatrodam vairs tadu
    $item = $this->productcollection->getItemByIdAssoc($prod_id);

    $langs = getLanguages($this->site_id);
    foreach ($langs as $lang => $lrow)
    {
      if (empty($item['url_'.$lang])) continue;

      $url = $item['url_'.$lang];
      do {
        $dup_prod = $this->productcollection->getCategoryProductByURL($item['category_id'], $lang, $url, $prod_id);
        if ($dup_prod)
        {
          $url .= '-'.$prod_id;
        }

      } while ($dup_prod);

      if ($url != $item['url_'.$lang])
        $prod_changes['url_'.$lang] = $url;

    }

    if ($prod_changes){
      $this->productcollection->changeItemByIdAssoc($prod_id, $prod_changes);
			
			$user = $_SESSION['userdata']['username'];
			$now = date('m.d.Y H:i:s');
			$log_data = array(
				'prod'=>$prod_id,
				'change_type'=>'Update',
				'user'=>$user,
				'change_comment'=>'Product "'.$prod['name_lv'].'"  changed data',
				'change_time'=>$now);
			$this->ProdLogger->change_log->Insert($log_data);
		}

    echo $prod_id;
  }

  function ajax_uploadImage()
  {
    $prod_id = intval($_GET['prod_id']);

    if (!$this->initDesign()) return;

    $prod = null;
    if ($prod_id)
    {
      $prod = $this->productcollection->getItemByIdAssoc($prod_id);

      $prodpath = $this->getProductFolder(
        $this->categorycollection->getCategoryPath($prod['category_id']),
        $prod
      );
    }
    else
    {
      $prodpath = 'tmp/';
    }


    $path = '/images/products/' . $prodpath;
    forceDirectories($this->site_id, $path);

    if ($prod_id){

      $id_path = getFilePathFromLink($this->site_id, $path.'product_id.txt');

      if (!file_exists($id_path)){
      	file_put_contents($id_path, (string)$prod_id);
      }

    }

    $imagetypes = array('gif', 'jpg', 'jpeg', 'png');

    foreach ($_FILES as $name => $f){
      if (!in_array(strtolower(pathinfo($f['name'], PATHINFO_EXTENSION)), $imagetypes)) continue;

      $info = uploadSiteFile($this->site_id, $f, $path);
      if (!$info['url']) continue;

      $pid = $prod_id;
      if (!$pid) $pid = -SessionManager::$session_row_id;

      if($pid){
          $this->imagescol->addItemAssoc(array(
            'product_id' => $pid,
            'image' => $info['url']
          ));
      }

      $images = $this->imagescol->getImagesForProduct($pid);
      $this->productcollection->Update(array('picture' => $images[0]['image']), array("item_id" => $prod['item_id']));

    }

    die('1');

  }

  function ajax_addImageURL(){
		$this->initCollections();
    $prod_id = intval($_GET['prod_id']);

    if (!$this->initDesign()) return;

    $image_url = $_GET['image_url'];

    $imagetypes = array('gif', 'jpg', 'jpeg', 'png');
    if (!in_array(strtolower(pathinfo($image_url, PATHINFO_EXTENSION)), $imagetypes)){
      echo 'Invalid file type!';
      die;
    }

    $prod = null;
    if ($prod_id){
      $prod = $this->productcollection->getItemByIdAssoc($prod_id);
    }

    if ($prod && empty($prod['picture'])){
      $this->productcollection->changeItemByIdAssoc($prod['item_id'], array('picture' => $image_url));
			$user = $_SESSION['userdata']['username'];
			$now = date('m.d.Y H:i:s');
			$log_data = array(
				'prod'=>$prod['item_id'],
				'change_type'=>'Update',
				'user'=>$user,
				'change_comment'=>'Product "'.$prod['name_lv'].'" picture add '.$image_url,
				'change_time'=>$now);
			$this->ProdLogger->change_log->Insert($log_data);
		}

    $pid = $prod_id ? $prod_id : -SessionManager::$session_row_id;
		
    $this->imagescol->addItemAssoc(array(
      'product_id' => $pid,
      'image' => $image_url
    ));
		
		$user = $_SESSION['userdata']['username'];
		$now = date('m.d.Y H:i:s');
		$log_data = array(
			'prod'=>$pid,			
			'change_type'=>'Insert',
			'user'=>$user,
			'change_comment'=>'Product "'.$prod['name_lv'].'" image add '.$image_url,
			'change_time'=>$now);
		$this->ProdLogger->change_log->Insert($log_data);
		
    die('1');
  }

	function ajax_addVideoURL(){

  	$prod_id = intval($_GET['prod_id']);
		$url = $_GET['url'];

    if (!$this->initDesign() || !$url) return;

    $parts = parse_url($url);
		parse_str($parts['query'], $query_parts);

		if(!$query_parts['v']){
			echo "Nekorekts Youtube video url formāts";
			return;
		}

		$thumb = addvideoarrow(getYoutubeVideoThumb($url));

    $prod = null;
    if ($prod_id){
      $prod = $this->productcollection->getItemByIdAssoc($prod_id);
    }

    if ($prod && empty($prod['picture'])){
      $this->productcollection->changeItemByIdAssoc($prod['item_id'], array('picture' => $thumb));
				$user = $_SESSION['userdata']['username'];
			$now = date('m.d.Y H:i:s');
			$log_data = array(
				'prod'=>$prod['item_id'],			
				'change_type'=>'Insert',
				'user'=>$user,
				'change_comment'=>'Product "'.$prod['name_lv'].'" picture added '.$thumb,
				'change_time'=>$now);
			$this->ProdLogger->change_log->Insert($log_data);
    }

		$pid = $prod_id ? $prod_id : -SessionManager::$session_row_id;

    $this->imagescol->addItemAssoc(array(
      'product_id' => $pid,
      'image' => $thumb,
			'video_url' => $url,
			'is_video' => 1
    ));
		
		$user = $_SESSION['userdata']['username'];
		$now = date('m.d.Y H:i:s');
		$log_data = array(
			'prod'=>$pid,			
			'change_type'=>'Insert',
			'user'=>$user,
			'change_comment'=>'Product "'.$prod['name_lv'].'" video added '.$url,
			'change_time'=>$now);
		$this->ProdLogger->change_log->Insert($log_data);
		
		die('1');

	}

  function ajax_loadImages()
  {
    $prod_id = intval($_GET['prod_id']);

    if (!$this->initDesign()) return;

    $shopman = shopmanager::getInstance();

    if ($prod_id > 0)
    {
      $prod = $this->productcollection->getItemByIdAssoc($prod_id);

      $pictures = $shopman->getProductPictures($prod);
    }
    else
    {
      $pictures = $shopman->getProductPictures(array('item_id' => -SessionManager::$session_row_id));
    }

    $this->designPictures($_GET['prod_id'], $pictures, false);
  }

  function autocomplete(){

    if (!$this->initDesign()) return;

    $cond = array("shortcut = 0"); // no shortcuts please!
    $results = array();

    $key = ($_GET['by'] == 'name')? "name_".$this->lang : "code";
    $what = ($_GET['by'] == 'name')? "name_".$this->lang : "code";
    $cond[] = '`'.$key.'` LIKE :needle';

    $params = array(":needle" => '%'.$_GET['query'].'%');

    $prods = $this->productcollection->getDBData(array(
      "what" => $what,
      "where" => implode(" AND ", $cond),
      "assoc" => true,
      "count" => 100,
      "order" => "name_".$this->lang,
			"params" => $params
    ));

    foreach($prods as $prod){
      $results[] = $prod[$what];
    }

    echo json_encode(array("suggestions" => $results));
    die();

  }

  function deleteImage($url)
  {
    deleteThumbnails($url);
    @unlink(getFilePathFromLink($this->site_id, $url));
  }

  function ajax_deleteImage()
  {
    $prod_id = intval($_GET['prod_id']);

    if (!$this->initDesign()) return;

    $shopman = shopmanager::getInstance();

    $pic_id = intval($_GET['pic_id']);
    if ($prod_id){
      $prod = $this->productcollection->getItemByIdAssoc($prod_id);
      $pictures = $shopman->getProductPictures($prod);
    }else{
      $pictures = $shopman->getProductPictures(array('item_id' => -SessionManager::$session_row_id));
    }

    if ($pic_id == 0 && $prod_id){
      $dir = getFilePathFromLink($this->site_id, dirname($prod['picture']));
      $id = file_get_contents($dir.'/product_id.txt');

      if ($id && $id == $prod_id){
        $this->deleteImage($prod['picture']);
				$user = $_SESSION['userdata']['username'];
				$now = date('m.d.Y H:i:s');
				$log_data = array(
					'prod'=>$prod_id,			
					'change_type'=>'Delete',
					'user'=>$user,
					'change_comment'=>'Product "'.$prod['name_lv'].'" deleted picture '.$prod['picture'],
					'change_time'=>$now);
				$this->ProdLogger->change_log->Insert($log_data);
      }

      $pictures = $this->imagescol->getImagesForProduct($prod_id);
			$this->productcollection->changeItemByIdAssoc($prod_id, array(
        'picture' => $pictures[0]['image']
      ));
			if($prod_id){
				$user = $_SESSION['userdata']['username'];
				$now = date('m.d.Y H:i:s');
				$log_data = array(
					'prod'=>$prod_id,			
					'change_type'=>'Update',
					'user'=>$user,
					'change_comment'=>'Product "'.$prod['name_lv'].'" changed picture '.$pictures[0]['image'],
					'change_time'=>$now);
				$this->ProdLogger->change_log->Insert($log_data);
      }
			$this->imagescol->delDBItem($pictures[1]['id']);

      die('1');

		}else{

      $tmp_dir = trim(getFilePathFromLink($this->site_id, 'images/products/tmp'), '/');

      foreach ($pictures as $pic){
        if ($pic['id'] == $pic_id){


          $this->deleteImage($pic['picture']);

          $this->imagescol->delDBItem($pic_id);
					if($prod_id){
						$user = $_SESSION['userdata']['username'];
						$now = date('m.d.Y H:i:s');
						$log_data = array(
							'prod'=>$prod_id,			
							'change_type'=>'Delete',
							'user'=>$user,
							'change_comment'=>'Product "'.$prod['name_lv'].'" deleted image '.$pic['picture'],
							'change_time'=>$now);
						$this->ProdLogger->change_log->Insert($log_data);
          }
					$pictures = $this->imagescol->getImagesForProduct($prod_id);
					$this->productcollection->changeItemByIdAssoc($prod_id, array(
		        'picture' => $pictures[0]['image']
		      ));
					if($prod_id){
						$user = $_SESSION['userdata']['username'];
						$now = date('m.d.Y H:i:s');
						$log_data = array(
							'prod'=>$prod_id,			
							'change_type'=>'Update',
							'user'=>$user,
							'change_comment'=>'Product "'.$prod['name_lv'].'" changed picture '.$pictures[0]['image'],
							'change_time'=>$now);
						$this->ProdLogger->change_log->Insert($log_data);
					}
          die('1');
        }
      }
    }

    die('0');
  }

  function ajax_getProduct()
  {

    $prod_id = $_GET['prod_id'];
    if (!$prod_id) return;

    if (!$this->initDesign()) return;

    $pvn = option('Shop\\pvn', null, 'PVN (%)', null, 22);

    if ($prod_id > 0)
      $prod = $this->productcollection->getItemByIdAssoc($prod_id);
    else
    {
      $prod = array();
      $props = $this->productcollection->getDBProperties();
      foreach ($props as $key => $prop)
      {
        $prod[$key] = '';
      }
    }

    $data = sqlQueryData('SELECT shortname FROM '.$this->site_id.'_languages');
    $langs = array();
    foreach ($data as $row)
    {
      $langs[] = $row['shortname'];
    }

    $data = $this->categorycollection->getDBData();
    $cats = array();
    foreach ($data as $row)
    {
      $cats[$row['item_id']] = $row;
    }

    $catslist = array();
    foreach ($cats as $cid => $row)
    {
      $pid = $cid;
      while ($pid)
      {
        if ($pid == $id) continue 2;
        $pid = $cats[$pid]['parent'];
      }

      $catslist[$cid] = str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;', $row['lvl']).htmlspecialchars($row['title_'.$this->lang]);
    }

    foreach ($this->productcollection->properties_assoc as $key => $f)
    {
      if ($f['column_type'] != 'date') continue;

      if (!$prod[$key] || $prod[$key] == '0000-00-00')
        $prod[$key] = '';
      else
        $prod[$key] = date('d.m.Y', strtotime($prod[$key]));

    }

    if ($prod['price'] == 0) $prod['price'] = '';
    if ($prod['sale_price'] == 0) $prod['sale_price'] = '';

    $shopman = shopmanager::getInstance();
    $steps = CategoryAmountStepCollection::getInstance();
    $amountprices = ProductAmountPriceCollection::getInstance();

    $p = $prod;
    if (!$p['item_id'])
    {
      $p['item_id'] = -SessionManager::$session_row_id;
    }

    $pictures = $shopman->getProductPictures($p);

    $col1_fields = array();
    $col2_fields = array();
    foreach ($this->productcollection->properties_assoc as $key => $prop)
    {
      if ($prop['on_panel'] == 'col1')
        $col1_fields[$key] = $prop;
      else if ($prop['on_panel'] == 'col2')
        $col2_fields[$key] = $prop;

    }

?>
<div id="openprod<?=$prod_id ?>" class="panel" data-shortcut='<?=$prod['shortcut']?1:0?>'>
  <div id="prodpanel<?=$prod_id ?>" class="">
	<? if($prod['shortcut']){ ?>
		<? $prod = $this->productcollection->getItemByIDAssoc($prod['shortcut']); ?>
		<? $cat_path = $this->categorycollection->getCategoryPath($prod['category_id']) ?>
		<? $cats = array(); ?>
		<? foreach($cat_path as $c){ $cats[] = $c['title_'.$this->lang]; } ?>
		<div class="panel_cnt"><div class="panel_inner">
		This is just a shortcut. Please edit original product at <?=implode(" / ", $cats) ?>
		</div></div>
	<? }else{ ?>
  <form action="" method="post">
<!--  <input type="hidden" name="prod_id" value="<?=$prod_id ?>"/> -->
  <div class="panel_cnt"><div class="panel_inner">

    <table class="wrap"><tr>
    <td class="leftwrap"><div class="left">
      <table>
        <tr>
          <td class="label"><?=lcms('Category')?>:</td>
          <td class="val">
            <select class="inp input1 field" name="category_id">
            <? foreach ($catslist as $id => $label) { ?><option value="<?=$id ?>"<?=($id == $prod['category_id'] ? ' selected="selected"' : '' )?>><?=$label ?></option><? echo "\n"; } ?>
            </select>
          </td>
        </tr>

        <tr>
          <td><?=lcms('Name')?>:</td>
          <td class="val" id="wrapname<?=$prod_id ?>">
            <div class="input_head">
              <ul class="langs">
              <? foreach ($langs as $lang) { ?>
                <li class="<?=($lang == CMS_LANG ? 'active' : (!empty($prod['name_'.$lang]) ? ' filled' : '') ) ?>"><a href="#" rel="<?=$lang ?>"><?=strtoupper($lang) ?></a></li>
              <? } ?>
              </ul>
            </div>
            <? foreach ($prod as $key => $val) {
               if (substr($key, 0, 5) != 'name_' || strlen($key) != 7) continue;
            ?>
              <input type="hidden" name="<?=$key ?>" value="<?=htmlspecialchars($val) ?>"/>
            <? } ?>
            <input type="hidden" class="active_lang" value="<?=$this->lang ?>"/>
            <div class="input"><input type="text" class="lang_val" value="<?=htmlspecialchars($prod['name_'.$this->lang]) ?>"/></div>
          </td>
        </tr>

        <tr>
          <td><?=lcms('Code')?>:</td>
          <td class="val">
            <div class="input"><input type="text" class="field" name="code" value="<?=htmlspecialchars($prod['code']) ?>"/></div>
          </td>
        </tr>
        <tr>
          <td><?=lcms('EAN')?>:</td>
          <td class="val">
            <div class="input"><input type="text" class="field" name="ean" value="<?=htmlspecialchars($prod['ean']) ?>"/></div>
          </td>
        </tr>
        <tr>
            <td></td>
            <td class="val">
            <div class="fields1">
              <label><?=lcms('Featured')?> <input type="checkbox" class="field" name="featured"<?=($prod['featured'] ? ' checked="checked"' : '') ?>/></label> &nbsp;

              <? if ($this->productcollection->properties_assoc['is_new']['type'] != 'hidden') { ?>
                <label><?=lcms('New')?> <input type="checkbox" class="field" name="is_new"<?=($prod['is_new'] ? ' checked="checked"' : '') ?>/></label>
              <? } ?>

              <? if ($this->productcollection->properties_assoc['is_top']['type'] != 'hidden') { ?>
                <label><?=lcms('Top')?> <input type="checkbox" class="field" name="is_top"<?=($prod['is_top'] ? ' checked="checked"' : '') ?>/></label>
              <? } ?>


              <br/>

              <table class="featured"<?=$prod['featured'] ? ' style="display:block"' : '' ?>>
                <tr>
                    <td><?=lcms('Featured on count')?></td>
                    <td><input type="checkbox" class="field" name="featured_count"<?=($prod['featured_count'] ? ' checked="checked"' : '') ?>/></td>
                </tr>
                <tr>
                  <td><?=lcms('Featured from')?>: &nbsp; </td>
                  <td>
                    <input type="text" name="featured_from" class="inp date field" value="<?=htmlspecialchars($prod['featured_from']) ?>"/>
                    <a href="#" class="datebutton" rel="featured_from"><img src="/scr/components/shop/images/icon_calendar.png" alt=""/></a>
                  </td>
                </tr>

                <tr>
                  <td><?=lcms('Featured to')?>: &nbsp; </td>
                  <td>
                    <input type="text" name="featured_to" class="inp date field" value="<?=htmlspecialchars($prod['featured_to']) ?>"/>
                    <a href="#" class="datebutton" rel="featured_to"><img src="/scr/components/shop/images/icon_calendar.png" alt=""/></a>
                  </td>
                </tr>

                <tr>
                  <td><?=lcms('Featured price')?>:&nbsp;</td>
                  <td> <input type="text" class="inp price field" name="sale_price" value="<?=htmlspecialchars($prod['sale_price']) ?>"/></td>
                </tr>

              </table>

              <div style="display:<?=$prod['is_new'] ? 'block' : 'none' ?>" class='new_date_wrap'>
                <?=lcms('New until')?>: &nbsp; <input type="text" name="new_until" class="inp date field" value="<?=htmlspecialchars($prod['new_until']) ?>"/>
                <a href="#" class="datebutton" rel="new_until"><img src="/scr/components/shop/images/icon_calendar.png" alt=""/></a>
                <br/>
              </div>

            </div>
          </td>
        </tr>

        <tr>
          <td><?=lcms('Price')?>:</td>
          <td class="val">

            <table class="subtab"><tr>
              <td><input type="text" class="inp price field" name="price" value="<?=htmlspecialchars($prod['price']) ?>"/></td>
              <td class="saleout_price">&nbsp;</td>
            </tr></table>

          </td>
        </tr>

        <?php // amount prices ?>
        <?php if(isset($prod['category_id']) && $prod['item_id']) { ?>
            <?php $prod_amount_prices = $amountprices->getByProduct($prod['item_id']) ?>
            <?php foreach($steps->getByCategory($prod['category_id']) as $step){ ?>
            <tr>
                <td>Cena(Pērkot <?php echo $step['count_from'].'-'.$step['count_to'] ?> gab.)</td>
                <td><input type='text' name='amount_prices[<?php echo $step['item_id']?>]' value='<?php echo number_format(isset($prod_amount_prices[$step['item_id']]) ? $prod_amount_prices[$step['item_id']]['price'] : 0, 2)?>' /></td>
            </tr>
            <?php } ?>
        <?php } ?>

        <tr>
            <td><?php echo lcms("Pieejamība")?>:</td>
            <td>
                <label><input type='checkbox' name='available_sample' value='1' <?php echo $prod['available_sample'] ? 'checked' : '' ?> /> Pieejams paraugs</label><br />
                <label><input type='checkbox' name='available_in_shop' value='1' <?php echo $prod['available_in_shop'] ? 'checked' : '' ?> /> Pieejams veikalā</label><br />
                <label><input type='checkbox' name='not_available' value='1' <?php echo $prod['not_available'] ? 'checked' : '' ?> /> Nav pieejama</label><br />
            </td>
        </tr>

        <tr>
          <td><?=lcms('PVN ( % )')?>:</td>
          <td class="val">
            <input type="text" class="inp number field" name="pvn" value="<?=htmlspecialchars($prod['pvn'] != 0 ? $prod['pvn'] : '') ?>"/>
          </td>
        </tr>

        <? $this->outputDesignCustomFields($col1_fields, $langs, $lang, $prod) ?>

        <?
        $custom_field_group_id = $this->categorycollection->getCustomFieldGroupId($prod['category_id']);
        $custom_fields_group = $custom_field_group_id ? $this->CustomFieldGroups->getById($custom_field_group_id) : false;
        ?>

        <? /* Custom fields */ ?>
        <?  // CustomFieldGroups CustomFields ProdCustomFields ?>
        <? if($custom_fields_group){ ?>
        <tr>
        <td><?=lcms('Custom fields')?>:</td>
        <td></td>
        </tr>
        <? foreach($this->CustomFields->getFieldsByGroup($custom_field_group_id, $prod['item_id']) as $field){ ?>
        <tr class='customfield <?=$field['type']?>' data-id='<?=$field['item_id']?>'>
            <td><?=$field['fieldname_lv']?>:</td>
            <td id="wrap<?=$field['item_id'].$prod['item_id'] ?>">
            <? if($field['type'] == 'text'){ ?>

            <div class="input_head">
                <ul class="langs">
                <? foreach ($langs as $lang) { ?>
                <li class="<?=($lang == $this->lang ? 'active' : (!empty($field['value_'.$lang]) ? ' filled' : '') ) ?>"><a href="#" rel="<?=$lang ?>"><?=strtoupper($lang) ?></a></li>
                <? } ?>
                </ul>
            </div>
            <div class="input">
                <input type="text" class="lang_val" value="<?=htmlspecialchars($field['value_'.$this->lang]) ?>"/>
                <? foreach($langs as $lang){ ?>
                <input type="hidden" class="customfields_text_<?=$field['item_id']?>_<?=$lang?>" name="customfield_texts[<?=$field['item_id']?>][<?=$lang?>]" value="<?=htmlspecialchars($field['value_'.$lang]) ?>"/>
                <? } ?>
                <input type="hidden" class="active_lang" value="<?=$this->lang ?>"/>
            </div>

            <? }elseif($field['type'] == 'select'){ ?>
            <? $field_values = $this->CustomFields->values->getByField($field['item_id']); ?>
            <select name='customfields[<?=$field['item_id']?>]'>
            <option value=''><?=lcms("Izvēlieties")?></option>
                <? foreach($field_values as $value){ ?>
                <option value='<?=$value['item_id']?>' <?=($value['item_id'] == $field['value_id'] ? "selected='selected'" : "")?>><?=$value['value_lv']?></option>
                <? } ?>
            </select>
            <? } ?>

            </td>
        </tr>
        <? } ?>
        <? } ?>



        <?

          if(!$prod['category_id']) $prod['category_id'] = $_GET['cat_id'];
          $groups = $this->ShopFilter->getFilterGroupsForCat($prod['category_id']);

          if(is_numeric($prod['item_id'])){
            $prod_filters = $this->ProdFilterRel->getFilterIds($prod['item_id']);
          }else{
            $prod_filters = Array();
          }

        ?>
        <? foreach($groups as $group){ ?>
        <? $params = $this->Filters->getByGroup($group['item_id']) ?>
        <tr>
          <td><?=$group['title_lv']?>:</td>
          <td>
            <? foreach($params as $param){ ?>
            <input <?=(in_array($param['item_id'], $prod_filters))? "checked='checked'" : "" ?> type='checkbox' name='filters[<?=$param['item_id']?>]' value='<?=$param['item_id']?>' /> <?=$param['param_title_lv']?> <br />
            <? } ?>
          </td>
        </tr>
        <? } ?>

      </table>

    </div></td>
    <td class="rightwrap"><div class="right">
      <table><tbody>
        <tr>
          <td class="label2"><?=lcms('Images')?>:</td>
          <td>
            <table class="pic_actions">
              <tr>
                <td style="width: 154px">
                  <img src="/scr/components/shop/images/pic_actions.png" alt="" usemap="#actions<?=$prod_id ?>"/>
                  <map name="actions<?=$prod_id ?>" id="picicons<?=$prod_id ?>">
                    <area class="edit" shape="rect" coords="0,0,16,20" href="#" title="<?=lcms('Edit')?>"/>
                    <area class="move_left" shape="rect" coords="24,0,41,20" href="#" title="<?=lcms('Move left')?>"/>
                    <area class="move_right" shape="rect" coords="47,0,64,20" href="#" title="<?=lcms('Move right')?>"/>
                    <area class="delete" shape="rect" coords="73,0,88,20" href="#" title="<?=lcms('Delete')?>"/>
                    <area class="zoom_in" shape="rect" coords="100,0,121,20" href="#" title="<?=lcms('Zoom in')?>"/>
                    <area class="print" shape="rect" coords="125,0,144,20" href="javascript:void(0)" title="<?=lcms('Print image')?>"/>
                  </map>
                </td>
                <td class="upload_tools">
                  <div id="debug<?=$prod_id ?>"></div>
                  <span class="uploadwrap"><span id="upload<?=$prod_id ?>"></span></span>
                  <span class="btn4"><a href="#" class="addfromserver"><?=lcms('Add from server')?></a></span>
                  <span class="btn4"><a href="#" class="addvideo"><?=lcms('Add youtube video')?></a></span>
                  <div id="debug<?=$prod_id ?>"></div>
                </td>
                <td class="corner_tools">
                  <? if ($prod_id > 0) { ?>
                    <a href="javascript:void(0)" id="printbtn<?=$prod_id ?>"><img src="/scr/components/shop/images/print.png" alt="<?=lcms('Print product')?>"/></a>
                  <? } ?>
                </td>
              </tr>
            </table>

            <div class="pics2" id="pics<?=$prod_id ?>">
              <? $this->designPictures($prod_id, $pictures, true) ?>
            </div>

            <table><tbody>


               <tr>
                <td><?=lcms('Brand')?>:</td>
                <td>
                  <select name="brand_id" class="field">
                    <option value="0"<?=$prod['brand_id'] == 0 ? ' selected="selected"' : '' ?>></option>
                  <? $brands = $this->brandscol->getDBData(Array('order' => 'brandname'));
                  foreach ($brands as $brand) { ?>
                    <option value="<?=$brand['item_id'] ?>"<?=$prod['brand_id'] == $brand['item_id'] ? ' selected="selected"' : '' ?>><?=$brand['brandname'] ?></option>
                  <? } ?>
                  </select>
                </td>
              </tr>


              <tr>
                <td style="width:100px"><?=lcms('Colors')?>:</td>
                <td style="white-space:nowrap">
                  <? $color_group = $this->categorycollection->getColorGroupId($prod['category_id']); ?>
                  <? if($color_group){ ?>
                    <? $prod_colors = $this->product_colors->getColorIdsForProduct($prod['item_id']); ?>
                    <? foreach($this->colors->getDBData(array("where" => "`group` = ".$color_group, "order" => "ind")) as $color){ ?>
                    <div style='margin-bottom: 3px'>
                      <input style='float: left;' type='checkbox' name='colors[<?=$color['item_id']?>]' <?=(in_array($color['item_id'], $prod_colors))? "checked='checked'" : ""?>  value='<?=$color['item_id']?>' />
                      <? if($color['image']){ ?>
                      <div style='float: left;margin: 0px 10px;border: 1px solid black;'><img src="<?=$color['image']?>" style="width: 20px;height:20px" alt="" /></div>
                      <? }else{ ?>
                      <div style='float: left;margin: 0px 10px;border: 1px solid black;width: 20px;height:20px;background:<?=$color['color']?>'></div>
                      <? } ?>
                      <div style='line-height: 22px'><?=$color['title_lv']?></div>
                      <div style='clear:both'></div>
                    </div>
                    <? } ?>
                  <? }else{ ?>
                    <?=lcms('Color group not specified for category')?>
                  <? } ?>
                </td>
              </tr>
              <tr>
                <td style="width:100px"><?=lcms('Sizes')?>:</td>
                <td>
                  <? $size_group = $this->categorycollection->getSizeGroupId($prod['category_id']); ?>
                  <? if($size_group){ ?>
                    <? $prod_sizes = $this->product_sizes->getSizeIdsForProduct($prod['item_id']) ?>
                    <? foreach($this->sizes->getDBData(array("where" => "`group` =".$size_group, "order" => "ind")) as $size){ ?>
                        <input type='checkbox' name='sizes[<?=$size['item_id']?>]' <?=(in_array($size['item_id'], $prod_sizes))? "checked='checked'" : ""?>  value='<?=$size['item_id']?>' />
                        <?=$size['title_lv']?> <br />
                    <? } ?>
                  <? }else{ ?>
                    <?=lcms('Size group not specified for category')?>
                  <? } ?>
                </td>
              </tr>
              <tr>
                <td><?=lcms('Product count')?>:</td>
                <td><input type="text" class="inp number field" name="prod_count" value="<?=$prod['prod_count'] ?>"/></td>
              </tr>


            </tbody></table>

          </td>
        </tr>
        <tr>
          <td><?=lcms('URL')?>:</td>
          <td class="val" id="wrapurl<?=$prod_id ?>">
            <div class="input_head">
              <ul class="langs">
              <? foreach ($langs as $lang) { ?>
                <li class="<?=($lang == $this->lang ? 'active' : (!empty($prod['url_'.$lang]) ? ' filled' : '') ) ?>"><a href="#" rel="<?=$lang ?>"><?=strtoupper($lang) ?></a></li>
              <? } ?>
              </ul>
            </div>
            <? foreach ($prod as $key => $val) {
               if (substr($key, 0, 4) != 'url_' || strlen($key) != 6) continue;
            ?>
              <input type="hidden" name="<?=$key ?>" value="<?=htmlspecialchars($val) ?>"/>
            <? } ?>
            <input type="hidden" class="active_lang" value="<?=$this->lang ?>"/>
            <div class="input"><input type="text" class="lang_val" value="<?=htmlspecialchars($prod['url_'.$this->lang]) ?>"/></div>
          </td>
        </tr>
        <tr>
          <td></td>
          <td class="val">
            <a href="#" class="btn3" id="editprices<?=$prod_id ?>"><?=lcms('Volumes...')?></a>
            <a href="#" class="btn3" id="editrelatedprods<?=$prod_id ?>"><?=lcms('Related...')?></a>
          </td>
        </tr>
        <? $this->outputDesignCustomFields($col2_fields, $langs, $lang, $prod) ?>
      </tbody></table>

    </div></td></tr></table>
    <div class="clear"></div>


  </div></div>

  <div class="footer" id="footer<?=$prod_id ?>">

    <table>
      <tr>
        <td style="width:30px"><img src="/scr/components/shop/images/comments.png" alt=""/></td>
        <td style="width:370px;">
          <?=lcms('Comments')?> <input type="text" class="input1bg field" name="comment" value="<?=htmlspecialchars($prod['comment']) ?>"/>
        </td>
        <td class="marks">
          <input type="hidden" name="status" value="<?=$prod['status'] ?>"/>
          <a href="#" class="m1<?=($prod['status'] == 1) ? ' active' : '' ?>" title="<?=lcms('Change product status to yellow')?>"></a>
          <a href="#" class="m2<?=($prod['status'] == 2) ? ' active' : '' ?>" title="<?=lcms('Change product status to red')?>"></a>
          <a href="#" class="m3<?=($prod['status'] == 3) ? ' active' : '' ?>" title="<?=lcms('Change product status to green')?>"></a>
          <a href="#" class="m4<?=($prod['status'] == 4) ? ' active' : '' ?>" title="<?=lcms('Change product status to blue')?>"></a>
        </td>
        <td class="prevnext"><div class="inner">
          <a href="#" class="prev" title="<?=lcms('Previous product')?>"><img src="/scr/components/shop/images/arrow_prev.png" alt=""/></a>
          <a href="#" class="next" title="<?=lcms('Next product')?>"><img src="/scr/components/shop/images/arrow_next.png" alt=""/></a>
        </div></td>
        <td style="text-align:right">
          <span class="ajaxload" style="display:none"><img src="<?=$GLOBALS['cfgWebRoot'] ?>gui/ajaxload.gif" alt=""/></span>
          <span class="okmsg" style="display:none"><?=lcms('Changes saved.')?></span>
          <div id="prodpanelbtns<?=$prod_id ?>" class="panel_btns">
            <a href="#" class="btn3" id="savebtn<?=$prod_id ?>"><?=lcms('Save')?></a>
            <a href="#" class="btn3" id="closebtn<?=$prod_id ?>"><?=lcms('Close')?></a>
          </div>
        </td>
      </tr>
    </table>

  </div>

  </form>
  <? } ?>
	</div>
</div>
  <script type="text/javascript"> $(function() { init_prodpanel('<?=$prod_id ?>'); });  </script>
<?
  }

  function outputDesignCustomField($key, $prop, $langs, $lang, $prod){

    ?>
    <tr>
      <td><?=$prop['label'] ?></td>
      <td class="val" id="wrap<?=$key.$prod['item_id'] ?>">

      <? if ($prop['multilang']) { ?>
        <div class="input_head">
          <ul class="langs">
          <? foreach ($langs as $lang) { ?>
            <li class="<?=($lang == $this->lang ? 'active' : (!empty($prod[$key.'_'.$lang]) ? ' filled' : '') ) ?>"><a href="#" rel="<?=$lang ?>"><?=strtoupper($lang) ?></a></li>
          <? } ?>
          </ul>
		  <? if($prop['type'] == 'wysiwyg'){ ?>
		  <a href="#" class="edit"><img src="/scr/components/shop/images/edit.png" alt=""/></a>
		  <? } ?>
        </div>
        <? foreach ($prod as $key2 => $val) {
           if (substr($key2, 0, strlen($key)+1) != $key.'_' || strlen($key2) != strlen($key)+3) continue;
        ?>
          <input type="hidden" name="<?=$key2 ?>" value="<?=htmlspecialchars($val) ?>"/>
        <? } ?>
        <input type="hidden" class="active_lang" value="<?=$this->lang ?>"/>

        <? if($prop['type'] == 'html'){ ?>
        	<div class="input">
            <textarea class="lang_val" name=''><?=htmlspecialchars($prod[$key.'_'.$this->lang]) ?></textarea>
          </div>
        <? }elseif($prop['type'] == 'wysiwyg'){ ?>
        <style>
        .toolbar_bg{display:none;}
        </style>
          <!-- START -->
          <input type="hidden" name="<?=$key ?>" value="<?=htmlspecialchars($val) ?>"/>
          <div class="toolbar_bg">
              <table class="toolbar">
                <tr>
                <td><a href="#" class="ico_italic"><img src="/scr/components/shop/images/ico_italic.png" alt=""/></a></td>
                <td><a href="#" class="ico_bold"><img src="/scr/components/shop/images/ico_bold.png" alt=""/></a></td>
                <td><a href="#" class="ico_underline"><img src="/scr/components/shop/images/ico_underline.png" alt=""/></a></td>
                <td><a href="#" class="ico_paste_word"><img src="/scr/components/shop/images/ico_paste_word.png" alt=""/></a></td>
                <td><a href="#" class="ico_link"><img src="/scr/components/shop/images/ico_link.png" alt=""/></a></td>
                <td><a href="#" class="ico_unlink"><img src="/scr/components/shop/images/ico_unlink.png" alt=""/></a></td>
                <td><a href="#" class="ico_image"><img src="/scr/components/shop/images/ico_image.png" alt=""/></a></td>
                <td><a href="#" class="ico_html"><img src="/scr/components/shop/images/ico_html.png" alt=""/></a></td>
              </tr>
            </table>
          </div>
          <div class="input"><div><textarea class="lang_val" rows="5" cols="40" id="<?=$key."-".$prod['item_id']?>"><?=htmlspecialchars($prod[$key."_".$this->lang])?></textarea></div></div>

          <!-- END -->

          <? }else{ ?>
          <div class="input">
            <input type="text" class="lang_val" value="<?=htmlspecialchars($prod[$key.'_'.$this->lang]) ?>"/>
          </div>
          <? } ?>

      <? } else { ?>

        <? if ($prop['type'] == 'file') { ?>
          <div class="file_field" id="file-<?=$prod['item_id'].'-'.$key ?>">
            <div>
              <label><input type="radio" checked="checked" name="_type_<?=$key ?>" value="upload"/> <?=lcms('Upload')?></label> &nbsp;
              <label><input type="radio" name="_type_<?=$key ?>" value="url"/> <?=lcms('Choose from server')?></label>
            </div>

            <div class="upload"><div id="fuploader-<?=$prod['item_id'].'-'.$key ?>"></div></div>

            <table class="url" style="display:none"><tr>
              <td class="col1"><div class="input"><input type="text" class="field" name="_url_<?=$key ?>"/></div></td>
              <td class="col2"><button class="btn">...</button></td>
            </tr></table>
          </div>
          <script type="text/javascript"> init_file_field('<?=$prod["item_id"] ?>', '<?=$key ?>')  </script>
        <? } else if ($prop['dialog']) { ?>
          <?
          if (!isset($prop['dialogw'])) $w = 400; else $w = $prop['dialogw'];
          if (!isset($prop['dialogh'])) $h = 450; else $h = $prop['dialogh'];
          ?>
          <table cellpadding="0" cellspacing="0"><tr>
          <td width="*" style="padding: 0px;"><div class="input"><input type="text" class="field" name="<?=$key ?>" id="<?=$key ?>" value="<?=htmlspecialchars($prod[$key]) ?>"/></div></td>
          <td width="40" style="padding: 0px;"><input class="formButton smallButton" type="button" value="..."
           OnClick="var val=openModalWin('<?= $prop['dialog']."', " . (!isset($prop['jsarguments']) ? "document.getElementById('".$key."').value" : $prop['jsarguments']) . ", ".$w.", ".$h."); if (val) document.getElementById('".$key."').value=val;"  ?>"></td>
          </tr></table>
        <? } else if ($prop['type'] == 'check') { ?>
          <label><input type="checkbox" name="<?=$key ?>" value="1"<?=$prod[$key] ? ' checked="checked"' : '' ?>/></label>
        <? } else if ($prop['type'] == 'list') { ?>
          <select name="<?=$key ?>">
            <? foreach ($prop['lookupassoc'] as $lkey => $lval) { ?>
              <option value="<?=$lkey ?>"<?=($prod[$key] == $lkey) ? ' selected="selected"' : '' ?>><?=$lval ?></option>
            <? } ?>
          </select>
        <? } else if ($prop['type'] == 'code') { echo $prop['code']; ?>
        <? } else { ?>
          <div class="input"><input type="text" class="field" name="<?=$key ?>" value="<?=htmlspecialchars($prod[$key]) ?>"/></div>
        <? } ?>
      <? } ?>
      </td>
    </tr>
    <?

  }
 function outputDesignCustomFields($fields, $langs, $lang, $prod){

    if (!$prod['item_id']) $prod['item_id'] = 'new';
    if ($fields) {
      echo '<tr><td colspan="2">&nbsp;</td></tr>';
      foreach ($fields as $key => $prop) {
        $this->outputDesignCustomField($key, $prop, $langs, $lang, $prod);
      }
    }

  }

  function designPictures($prod_id, $pictures)
  {
?>
  <? $j=0; foreach ($pictures as $pic) { ?>
    <a<?=($j == 0 ? ' class="active"' : '') ?> href="<?=$pic['picture'] ?>" rel="pic-<?=$pic['id'] ?>" title="<?=htmlspecialchars($pic['title']) ?>"><img src="<?=getThumbURL($pic['picture'], 50, 38, 7) ?>" width="50" height="38" alt=""/></a>
  <? $j++; } ?>
<?
  }

  function ajax_searchProducts(){

		if (!$this->initDesign()) return;

    $cond = array();

    if($_POST['enabled'])$cond[] = 'disabled = 0';
    if($_POST['disabled'])$cond[] = 'disabled = 1';

    $pricefield = 'price';
    if (!empty($_POST['price_min']) || is_numeric($_POST['price_min'])) $cond[] = '(p.'.$pricefield.'+0 >= '.floatval($_POST['price_min']).')';
    if (!empty($_POST['price_max']) || is_numeric($_POST['price_max'])) $cond[] = '(p.'.$pricefield.'+0 <= '.floatval($_POST['price_max']).')';

    $code = trim($_POST['code']);
    if (!empty($code)){
      $searchcols = array('p.code');
      if ($this->pricescol)
      {
        $searchcols[] = 'pr.code';
      }
      $this->productcollection->condSearchProducts($cond, $code, $searchcols);
    }

    $cat = (int)$_POST['category'];
    if($cat){

      $cat_ids = $this->categorycollection->getActivePredecessorIds($cat);
      $cat_ids[] = $cat;
      $cond[] = "category_id IN (".implode(", ", $cat_ids).")";
    }

    $name = trim($_POST['name']);
    if (!empty($name))
    {
      $searchcols = Array('p.name_'.$this->lang, 'description_'.$this->lang);
      $this->productcollection->condSearchProducts($cond, $name, $searchcols);
    }

    if($_POST['brand']){

      $brand = $this->brandscol->getDBData(array("where" => "brandname = :name", "params" => array(":name" => $_POST['brand'])));
      if($brand){
          $cond[] = "brand_id = ".$brand[0]['item_id'];
      }else{
        $cond[] = 0; // do not find anything
      }

    }

    list($products, $count) = $this->productcollection->queryProducts($cond, $pg, $this->products_in_page, $orderby, '*', QUERYPROD_DATACOUNT, $this->pricescol);

    if($_POST['with_image'] || $_POST['without_image']){
      foreach($products as $key => $product){

        $images = $this->imagescol->getImagesForProduct($product['item_id']);
        $image_count = count($image_count);

        if($_POST['with_image'] && !$images){
          unset($products[$key]);
          $count--;
        }
        if($_POST['without_image'] && $images){
          unset($products[$key]);
          $count--;
        }

      }
    }


    $this->outputDesignProductsList($products);

  }

  function ajax_getCategoryProducts()
  {
    $cat_id = intval($_GET['cat_id']);
    if (!$cat_id) return;

    if (!$this->initDesign()) return;

    $products = $this->productcollection->getCategoryProducts($cat_id, 0, 0, 'cat_ind', true);

    $this->outputDesignProductsList($products);
  }

  function outputDesignProductsList($products)
  {
    foreach ($products as $key => $p)
    {
      $p['brand_name'] = '';
      if ($p['brand_id'])
      {
        $brand = $this->brandscol->getItemByIdAssoc($p['brand_id']);
        $p['brand_name'] = $brand['brandname'];
      }

      $products[$key]['brand_name'] = $p['brand_name'];
    }

?>
  <div class="prodtable">
    <ul class='sortable'>
      <li id="emptyprod"<?=$products ? ' style="display:none"' : '' ?>>
        <div class='head'>
          <div class='edit fl'></div>
          <div class='pic fl'></div>
          <div class='up fl'></div>
          <div class='down fl'></div>
          <div class='del fl'></div>
          <div class='name fl'><?=lcms('Nothing found.')?></div>
          <div class='code fl'></div>
          <div class='brand fl'></div>
          <div class='price fl'></div>
          <div class='tmp fl'></div>
          <div class='disabled fl'></div>
          <div class='cb'></div>
        </div>
        <div class='data'></div>
      </li>
      <? foreach ($products as $p) { ?>
       <? $this->outputDesignProductRow($p); ?>
      <? } ?>
    </ul>
  </div>
<?
  }

  function outputDesignProductRow($p){

    $c = $this->shopman->getProductPicture($p);
    if($c){
      $picture = $c;
    }

?>
    <li id="prod<?=$p['item_id'] ?>"<?=$p['status'] ? ' class="mark'.$p['status'].'"' : '' ?>>
      <div class='head'>
        <div class='edit fl'>
          <a href="#" title="<?=lcms('Edit')?>" class="edit"><img src="/scr/components/shop/images/prod_edit.png" alt=""/></a>
          <a href="<?=$this->shopman->getProductURL($p) ?>" target="_blank" title="Apskatīt lapā" class="preview"><img src="/scr/components/shop/images/magnify2.png" alt=""/></a>
        </div>
        <div class='pic fl'><a href="<?=$picture ?>" target="_blank" rel="shadowbox[prods]"<?=$picture ? '' : ' style="display:none"' ?>><img src="<?=getThumbURL($picture, 49, 35, 7) ?>" alt="" width="49" height="37"/></a></div>
        <div class='up fl'><a href="#" title="<?=lcms('Move up')?>"><img src="/scr/components/shop/images/arrow_up.png" alt=""/></a></div>
        <div class='down fl'><a href="#" title="<?=lcms('Move down')?>"><img src="/scr/components/shop/images/arrow_down.png" alt=""/></a></div>
        <div class='del fl'><a href="#" title="<?=lcms('Delete product')?>"><img src="/scr/components/shop/images/delete.png" alt=""/></a></div>
        <div class="copy fl"><a href="#" title="<?=lcms('Copy product')?>"><img src="/scr/components/shop/images/copy.png" alt=""/></a></div>
        <div class='name fl <?=$p['comment']? "has_commet" : "" ?>'>
          <?=$p['shortcut'] ? '<img src="/cms/backend/gui/images/shortcut.gif" alt="" style="position:relative;top:2px"/>' : '' ?>
          <a href="#" <?=$p['disabled'] ? 'class="disabled"' : '' ?> title="<?=htmlspecialchars($p['name_'.$this->lang]) ?>"><?=$p['name_'.$this->lang] ?></a>
          <? if($p['comment']){ ?><span><?=htmlspecialchars($p['comment'])?></span><? } ?>
        </div>
        <div class='code fl'><?=$p['code'] ?></div>
        <div class='brand fl'><?=$p['brand_name'] ?></div>
        <div class='price fl' style='<?=($this->shopman->isProductFeatured($p) && $p['sale_price'])? "line-height:17px" : ""?>'>
         <?=lcms('Price')?>: <span><?=number_format($p['price'], 2, '.', '') ?></span>
          <? if($this->shopman->isProductFeatured($p) && $p['sale_price']){ ?>
          <br /> <?=lcms('Sale price')?>: <?=number_format($p['sale_price'], 2, '.', '') ?>
          <? } ?>
        </div>
        <div class='tmp fl'></div>
        <div class='disabled fl'>
          <label for="ds<?=$p['item_id'] ?>">
            <?=lcms('Disabled')?><br/>
            <input type="checkbox" id="ds<?=$p['item_id'] ?>" <?=$p['disabled'] ? 'checked="checked"' : '' ?>/>
          </label>
        </div>
        <? /* <div class="status fl"><?=htmlspecialchars($p['comment']) ?></div> */ ?>
        <div class='cb'></div>
      </div>
      <div class='edit_wrap'></div>
    </li>
    <?

  }

  function ajax_addNewProduct()
  {
    if (!$this->initDesign()) return;

    if ($this->imagescol)
      $this->imagescol->dropDeadSessionItems();

    if ($this->pricescol)
      $this->pricescol->dropDeadSessionItems();

    $_GET['prod_id'] = 'new';

?>
  <li id="prodnew" class='active'>
    <div class='head'>
      <div class='edit fl'></div>
      <div class='pic fl'></div>
      <div class='up fl'></div>
      <div class='down fl'></div>
      <div class='del fl'></div>
      <div class='name fl'><?=lcms('New product')?></div>
      <div class='code fl'></div>
      <div class='brand fl'></div>
      <div class='price fl'></div>
      <div class='tmp fl'></div>
      <div class='disabled fl'></div>
      <div class='cb'></div>
    </div>
    <div class='edit_wrap'><? $this->ajax_getProduct();  ?></div>
  </li>
<?



  }

  function ajax_getSingleProductRow()
  {
    $prod_id = intval($_GET['prod_id']);
    if (!$prod_id) return;

    if (!$this->initDesign()) return;

    $p = $this->productcollection->getItemByIdAssoc($prod_id);
    $p['brand_name'] = '';
    if ($p['brand_id'])
    {
      $brand = $this->brandscol->getItemByIdAssoc($p['brand_id']);
      $p['brand_name'] = $brand['brandname'];
    }

    $this->outputDesignProductRow($p);

//    $this->ajax_getProduct();
  }

  function ajax_loadProductThumb(){

    if (!$this->initDesign()) return;

    $c = $this->shopman->getProductPicture((int)$_GET['product_id']);
    if($c){
      $picture = $c;
    }

    echo json_encode(array(
      'thumb' => $picture ? getThumbURL($picture , 49, 37, 7) : "",
      'picture' => $picture
    ));
    die;

  }

  function ajax_upProductImage(){
    $this->upProductImage();
  }

  function ajax_downProductImage(){
    $this->downProductImage();
  }


  function dialogPrintImage()
  {
    if (!$this->initDesign()) return;

    $pic_id = intval($_GET['pic_id']);
    if ($pic_id > 0)
    {
      $row = $this->imagescol->getItemByIdAssoc($pic_id);
      $image = $row['image'];
    }
    else
    {
      $prod = $this->productcollection->getItemByIdAssoc($_GET['product_id']);
      $image = $prod['picture'];
    }

?>
  <img src="<?=htmlspecialchars($image) ?>" alt=""/>

  <script type="text/javascript">

  window.parent.print();

  </script>
<?

  }

  function dialogPrintProduct()
  {
    if (!$this->initDesign()) return;

    $lang = $_GET['lang'];
    $prod_id = intval($_GET['product_id']);
    $prod = $this->productcollection->getItemByIdAssoc($prod_id);

    $shopman = $this->getBackendServiceByName('shopmanager');

    $cat = $this->categorycollection->getItemByIdAssoc($prod['category_id']);
    $fields = array(
      'Name' => $prod['name_'.$lang],
      'Category' => $cat['title_'.$lang],
      'Price' => $prod['price'],
      'Saleout price' => $prod['sale_price'],
      'URL' => 'http://'.$_SERVER['SERVER_NAME'].$shopman->getProductURL($prod),
      'Picture' => $prod['picture'] ? '<img src="'.getThumbURL($prod['picture'], 250, 188, 6).'" alt=""/>' : '',
      'Code' => $prod['code'],
      'Is featured' => $prod['featured'] ? 'yes' : 'no',
      'Is new' => $prod['is_new'] ? 'yes' : 'no',
      'New until' => $prod['new_until'] && $prod['new_until'] != '0000-00-00' ? date('d.m.Y', strtotime($prod['new_until'])) : '',
      'Description' => $prod['description_'.$lang],
      'Long description' => $prod['long_description_'.$lang]
    );

?>
  <style type="text/css">

  .tbl {
    border-collapse: collapse;
  }

  .tbl td {
    font-size: 14px;
    vertical-align: top;
    padding-right: 20px;
    padding-top: 3px;
    padding-bottom: 3px;
    border-bottom: 1px solid #dddddd;
  }

  </style>
  <table class="tbl">
  <? foreach ($fields as $label => $val) { ?>
  <tr>
    <td><?=$label ?></td>
    <td><?=$val ?></td>
  </tr>
  <? } ?>
  </table>
  <script type="text/javascript">

  window.parent.print();

  </script>
<?
  }

  function dialogAddShorcutSelectProduct(){

    $prod_id = intval($_POST['prod_id']);
    $this->initCollections();

    do{
			$properties = $this->productcollection->GetRow(array("where" => "item_id = :id", "params" => array(":id" => $prod_id)));
			$prod_id = $properties['shortcut'] ? $properties['shortcut'] : $prod_id; // for next iteration
    }while($properties['shortcut']);

		$properties['shortcut'] = $properties['item_id'];
    $properties['category_id'] = $_GET['category_id'];

		unset($properties['item_id']);
		unset($properties['ind']);

    $id = $this->productcollection->add($properties);

		die($id);

  }

  function designCategories(){

    $ajax_url = $this->getComponentModifierLink('ajax');
    $autocomplete_url = $this->getComponentModifierLink('autocomplete');
    $editpic_url = $this->getComponentDialogLink('editProductImage');
    $printpic_url = $this->getComponentDialogLink('dialogPrintImage', null, '', 'close');
    $printprod_url = $this->getComponentDialogLink('dialogPrintProduct', null, '', 'close');
    $addsubcat_url = $this->getComponentDialogLink('editCategory', null, lcms('Add subcategory'));
    $editcat_url = $this->getComponentDialogLink('editCategory', null, lcms('Edit category'));
    $add_video_url = $this->getComponentDialogLink('addVideoUrl', null, lcms('Add video'));

//Array('div' => 1, 'product_id' => $id)
    $editprices_url = $this->getComponentDialogLink('editProductPrices', null, '', 'close');
    $editrelatedprods_url = $this->getComponentDialogLink('editRelatedProducts', null, lcms('Edit related products'), 'close');

    $dialogselectprod = shopordereditor::getInstance();

    $addshortcut_url = $dialogselectprod->getSelectProductDialogUrl(array('category_id'), lcms('Add shortcut'), 'dialogAddShorcutSelectProduct', $this->name, get_class($this), false);

    $cats = $this->categorycollection->getAllCategories($this->productcollection);

    $multilang_fields = array('name', 'url');

    $wysiwyg_fields = Array();

    foreach ($this->productcollection->properties_assoc as $key => $prop)
    {
      if ($prop['multilang'] && ($prop['on_panel'] == 'col1' || $prop['on_panel'] == 'col2') && $prop['type'] != 'wysiwyg' )
        $multilang_fields[] = $key;

      if($prop['type'] == 'wysiwyg'){
        $wysiwyg_fields[] = $key;
      }

    }

    $prodcount = $this->productcollection->itemCount();
    $brands = $this->brandscol->getDBData(array("order" => "brandname ASC"));
    $cat_tree = $this->categorycollection->getCategoryTree();

?>
  <!-- jquery -->
  <script type="text/javascript" src="/scr/js/jquery.js"></script>

  <!-- jquery ui + autocomplete -->
  <script src="/scr/js/jquery-plugins.js" type="text/javascript"></script>
  <script src="/scr/js/jquery.autocomplete.js" type="text/javascript"></script>

  <!-- jquery LocalScroll -->
  <script type="text/javascript" src="/scr/components/jquery.scrollTo-1.4.2-min.js"></script>
  <script type="text/javascript" src="/scr/components/jquery.localscroll-1.2.7-min.js"></script>

  <!-- jquery labelify -->
  <script type="text/javascript" src="/scr/components/jquery.labelify.js"></script>

  <!-- ckeditor -->
  <script type="text/javascript" src="<?=$GLOBALS['cfgWebRoot'] ?>gui/ckeditor/ckeditor.js"></script>

  <!-- shop editor -->
  <link rel="stylesheet" type="text/css" href="/scr/components/shop/editor.css" />
  <script type="text/javascript" src="/scr/components/shop/editor.js"></script>

  <!-- misc functions -->
  <script type="text/javascript" src="/scr/components/misc.js"></script>

  <!-- datepicker -->
  <link rel="stylesheet" type="text/css" href="/scr/components/jquery.datepick/jquery.datepick.css" />
  <link rel="stylesheet" type="text/css" href="/scr/components/jquery.datepick/redmond.datepick.css" />
  <script type="text/javascript" src="/scr/components/jquery.datepick/jquery.datepick.pack.js"></script>
  <script type="text/javascript" src="/scr/components/jquery.datepick/jquery.datepick-<?=$this->lang ?>.js"></script>

  <!-- uploadify -->
  <link type="text/css" rel="stylesheet" href="/scr/components/uploadify/uploadify.css" />
  <script type="text/javascript" src="/scr/components/uploadify/jquery.uploadify.v2.1.0.min.js"></script>

  <!-- swfobject -->
  <script type="text/javascript" src="/scr/components/swfobject/swfobject.js"></script>

  <!-- shadowbox -->
  <link rel="stylesheet" type="text/css" href="/scr/components/shadowbox/shadowbox.css" />
  <script type="text/javascript" src="/scr/components/shadowbox/shadowbox.js"></script>

  <!-- Google Map -->
  <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>

  <script type="text/javascript">

  var texts = {
    prodchanged: '<?=lcms('Product data changed. Save changes ?')?>',//Produkta dati mainīti. Vai saglabāt izmaiņas ?
    delimage: '<?=lcms('Do you want to delete this image ?')?>',//Vai vēlies dzēst šo bildi ?
    cantsave: '<?=lcms('Product data failed to save, reason server error! Data')?>: {!data}',//Produkta datus neizdevās saglabāt servera kļūdas dēļ! Dati
    confirmdel: '<?=lcms('Do you really want to delete this product ?')?> "{!name}" ?',//Vai tiešām vēlies dzēst preci
    confirmdelcat: '<?=lcms('Do you really want to delete this category ?')?> "{!name}" ?'//Vai tiešām vēlies dzēst kategoriju
  };

  var cfgWebRoot = <?=json_encode($GLOBALS['cfgWebRoot']) ?>;
  var site_id = <?=$this->site_id ?>;
  var page_id = <?=$this->page_id ?>;
  var session_name = <?=json_encode(session_name()) ?>;
  var session_id = <?=json_encode(session_id()) ?>;

  var autocomplete_url = <?=json_encode($autocomplete_url) ?>;
  var catalog_ajax_url = <?=json_encode($ajax_url) ?>;
  var editpic_url = <?=json_encode($editpic_url) ?>;
  var printpic_url = <?=json_encode($printpic_url) ?>;
  var printprod_url = <?=json_encode($printprod_url) ?>;
  var addshortcut_url = <?=json_encode($addshortcut_url) ?>;
  var editprices_url = <?=json_encode($editprices_url) ?>;
  var addsubcat_url = <?=json_encode($addsubcat_url) ?>;
  var editcat_url = <?=json_encode($editcat_url) ?>;
  var browse_images_url = '?module=dialogs&action=filex&site_id=<?=$this->site_id ?>&view=thumbs';
  var add_video_url = <?=json_encode($add_video_url)?>;
  var editrelatedprods_url = <?=json_encode($editrelatedprods_url) ?>;

  var proddata = {};
  var current_lang = '<?=$this->lang ?>';
  var selected_lang = '<?=$this->lang ?>';
  var multilang_fields = <?=json_encode($multilang_fields) ?>;
  var wysiwyg_fields = <?=json_encode($wysiwyg_fields) ?>;

  </script>

  <style type="text/css">

  select.datepick-month-year {
    font-size: 11px !important;
  }
  .datepick {
    width: 200px !important;
  }
  .datepick-month {
    width: 100% !important;
  }


  </style>

  <div id="shopeditorwrap">
  <table style="width:100%;table-layout:fixed"><tr>
    <td>
      <?=lcms('Category list')?>:<br />
      <?=$this->designAddButtons() ?>
      <span id="shopajaxload" style="display:none"><img src="<?=$GLOBALS['cfgWebRoot'] ?>gui/ajaxload.gif" alt=""/></span>
      <br/>
    </td>
    <td style='width: 75%'>
      <div id="prodpanel">

        <div class="ptop">
          <form action="" method="post" id="prodpanelsearch">
            <input type="hidden" name="search" value="1"/>
            <select name="category" style="margin-bottom: 5px;">
              <option value='0'><?=lcms('In all categories')?></option>
              <? foreach($cat_tree as $cat){ $this->output_cat_tree_option($cat); } ?>
            </select>  <br />

            <input type="text" class="name labelify" name="name" value="" title="<?=lcms('Name ...')?>"/>
            <input type="text" class="code labelify" name="code" value="" title="<?=lcms('Product code ...')?>"/>
            <select name="brand">
              <option value=""><?=lcms('Manufacture')?></option>
              <? foreach($brands as $brand){ ?>
                <option value="<?=$brand['brandname']?>"><?=$brand['brandname']?></option>
              <? } ?>
            </select>
            <br />
            <input type="checkbox" name='with_image' id='with_image' /> <label for="with_image"><?=lcms('With image')?></label>
            <input type="checkbox" name='without_image' id='without_image' /> <label for="without_image"><?=lcms('Without image')?></label>
            <br />
            <input type="checkbox" name='enabled' id='enabled' /> <label for="enabled"><?=lcms('Enabled')?></label>
            <input type="checkbox" name='disabled' id='disabled' /><label for="disabled"><?=lcms('Disabled')?></label>
            <br />
            <span class="price"><?=lcms('Price')?> : </span>
            <input type="text" class="pricefrom labelify" name="price_min" value="" title="<?=lcms('From ...')?>"/>
            <input type="text" class="priceto labelify" name="price_max" value="" title="<?=lcms('To ...')?>"/>

            <button type="submit" class="submit"><?=lcms('Filter')?></button>
            <div class='total'>
              <?=$prodcount ?> <?=lcms('products in catalog')?>
            </div>
          </form>
        </div>

      </div>
      <div class="clear"></div>
    </td>
  </tr></table>

  <table id="shopcats" style="table-layout: fixed">
    <tbody>
      <tr id="catsearch" style="display:none" bgcolor="#E3E3E3">
        <td style="width:130px;height:18px"></td>
        <td><?=lcms('Search results')?></td>
      </tr>
    </tbody>
    <tbody id="shopcatrows">
<?
  $this->designOutputCategories($cats);
?>
    </tbody>
  </table>
  </div>
<?

    //$this->designCategoriesCtrls();
  }

  function ajax_designJustCategories()
  {
    if (!$this->initDesign()) return;

    $cats = $this->categorycollection->getAllCategories($this->productcollection);
    $this->designOutputCategories($cats);
  }

  function designOutputCategories($cats)
  {
?>
  <? $i = 0; $j = array(); foreach ($cats as $row) { ?>
  <tr id="cat<?=$row['item_id'] ?>" bgcolor="#<?=($j[$row['lvl']] & 1) ? 'F4F4F4' : 'E3E3E3' ?>" class="cp-<?=(int)$row['parent'] ?><?=$row['subcatcount'] ? ' has_subcats' : '' ?><?=$row['lvl'] > 0 ? '' : ' open' ?>">
    <td style="width:130px" class="actions">
      <a href="#" class="add"><img src="gui/images/new/plus.png" alt=""/></a>&nbsp;

      <a href="#" class="down"><img src="gui/images/new/down.png" alt=""/></a>
      <a href="#" class="up"><img src="gui/images/new/up.png" alt=""/></a>
      <a href="#" class="edit"><img src="gui/images/new/edit.png" alt=""/></a>&nbsp;

      <a href="#" class="delete"><img src="gui/images/new/delete.png" alt=""/></a>
    </td>
    <td class="label">
      <a href="#" onclick="return false" class="name" style="margin-left:<?=$row['lvl'] * 15 ?>px;<?=$row['disabled'] ? 'text-decoration: line-through' : '' ?>"><?=$row['title_' . $this->lang] ?></a> &nbsp; <? if (!$row['subcatcount']) { ?>(<span id="catprodcount<?=$row['item_id'] ?>"><?=$row['prodcount'] ?></span>)<? }  else echo '&raquo;' ?>
    </td>
  </tr>
  <? $i++;

    if (!isset($j[$row['lvl']])) $j[$row['lvl']] = 0;
    $j[$row['lvl']]++;

  } ?>
<?
  }

  function getJsCategoryNodes($die = true)
  {
    $this->InitLanguageStrings();
    if($this->initCollections())
    {
      $data = $this->categorycollection->getDBData();

      if($_POST['node'] == 'source')
      {
        $parent = 0;
      }else
      {
        $parent = intval(substr($_POST['node'], 7));
      }

      if($data)
      {
        echo '[';
        for($f = 0; $f < count($data); $f++)
        {
          $row = $data[$f];
          if(intval($row['parent']) == $parent)
          {

            echo '
            {';
            echo 'id: \'catnode' . $row['item_id'] . '\',';
            echo 'singleClickExpand: true,';
            echo 'text: \'' . parseForJScript($row['title_' . $this->lang], true) . '\'';
            if($f == count($data) - 1 || $row['lvl'] >= $data[$f+1]['lvl'])
            {
              echo ',leaf: true';
            }
            echo '},
            ';
          }
        }
        echo ']';
      }
    }

    if($die)
      die(); //prevent templates
  }

  function designCategoriesCtrls()
  {
    echo "
    <script>
    Ext.onReady(function(){
        // shorthand
        var Tree = Ext.tree;

        var tree = new Tree.TreePanel({
            el:'".$this->name."-tree-div',
            useArrows:true,
            autoScroll:true,
            animate:true,
            enableDD:true,
            rootVisible: false,
            containerScroll: true,
            loader: new Tree.TreeLoader({
                dataUrl:'?module=dialogs&action=component&site_id=".$this->site_id."&page_id=".$this->page_id."&component_name=".$this->name."&component_type=".get_class($this)."&refresh=0&method_name=getJsCategoryNodes'
            })
        });

        // set the root node
        var root = new Tree.AsyncTreeNode({
            text: 'Products',
            draggable:false,
            id:'source'
        });
        tree.setRootNode(root);

        // render the tree
        tree.render();
        root.expand();
    });
    </script>
    ";
    echo '
    <div id="'.$this->name.'-tree-div" style="overflow:auto; height:400px;width:250px;border:1px solid #c3daf9;"></div>';
  }

  function editProductsContainer()
  {
    echo '<div id="productslist" style="margin: 10px;">';
    $this->editProducts();
    echo '</div>';
  }

  function editProducts()
  {
    $this->InitLanguageStrings();
    if($this->initCollections())
    {
      $category = $this->categorycollection->getItemByIdAssoc($_GET['category']);
      echo lcms('Products in category').': ' . $category['title_' . $this->lang] . '<br /><br />';
      echo $this->designAddProductButton($_GET['category']);

      $dialogselectprod = $this->getBackendServiceByName('DialogSelectProduct');
      if (!$dialogselectprod) die('DialogSelectProduct backendService not found!');

      $add_js = $dialogselectprod->srvGetSelectProductDialogJs(Array('category'), 'Add shortcut', 'dialogAddShortcutSubmit', 'editProducts', $this->name, get_class($this), 'productslist', '&category='.$_GET['category']);

      echo '&nbsp;&nbsp;&nbsp;<input type="button" value="'.lcms('Add shortcut').'" onclick="'.$add_js.'"/>';
      echo '<br />';
      $products = $this->productcollection->getCategoryProducts($_GET['category'], $page, $pagesize, 'cat_ind', true);
      if($products)
      {
        echo '<table>';
        $i = 0;
        foreach($products as $product)
        {
          $i++;
          echo '<tr><td>';
          if($product['shortcut'])
          {
            echo ' <img src="'.$GLOBALS['cfgWebRoot'].'gui/images/shortcut.gif" /> ';
            $shortcut = $product;
            $realproduct = $this->productcollection->getProductInfo($product['shortcut']);
            $product = $realproduct;
          }else
          {
            $shortcut = false;
          }
          if($product['picture'])
            echo $i . '<img src="' . getThumbURL($product['picture'], 27, 20, 6) . '" alt=""/> ';
          echo '</td><td>';
          if($product['disabled'])
            echo '<strike>';
          echo '<a href="#" onclick="' . $this->editProductLink($product['item_id'], $product['category_id']) . '; return false;">';
          echo $product['name_' . $this->lang];
          echo '</a>';
          if($product['disabled'])
            echo '</strike>';
          echo '</td><td>';
          if(!$shortcut)
            echo $this->editProductButtons($product['item_id'], $product['category_id']);
          else
            echo $this->editShortcutButtons($product['item_id'], $product['category_id'], $shortcut);

          echo '</td></tr>';
        }
        echo '</table>';
      }
    }
  }

  function dialogAddShortcutSubmit()
  {
    $prod_id = intval($_POST['prod_id']);
    $this->initCollections();
//    $id = $_GET['index'];
    $properties = $this->productcollection->getProductInfo($prod_id);
    $properties['shortcut'] = $properties['item_id'];
//    $properties['cat_ind'] = $_POST['cat_ind'];
    $properties['category_id'] = $_GET['category'];
    $properties = $this->productcollection->AddItemSlashes($properties);

/*
    if($id)
    {
      $this->productcollection->changeProduct($id, $properties);
    }else
    {*/
      $ind = $this->productcollection->addProduct($properties, false);
      $id = $this->productcollection->GetItemId($ind);
//    }

  }

  //=========================================================================//
  // Forms and form processing for editing, adding, deleting
  //=========================================================================//

  //=========================================================================//
  // Product editing
  //=========================================================================//

  //display form for both - add and edit windows
  function editProduct()
  {
    $this->InitLanguageStrings();
    $langs = sqlQueryColumn('SELECT shortname FROM '.$this->site_id.'_languages ORDER BY is_default DESC');
    //initialize collection class
    if($this->initCollections())
    {
      $form_data = $this->productcollection->GetDBProperties();
      $id = $_GET['index'];
      if (!$id)
      {
        $prod1 = $this->productcollection->getProdByCategory(0, session_id() );
        $id = $prod1 ? $prod1['item_id'] : $this->productcollection->getItemID($this->productcollection->addItemAssoc( array('code' => session_id() ) ));
        $_GET['index'] = $id;
      }

      $paramvalues = Array();


      if($id)
      {
        //edit product
        if ($_POST)
        {
          $item = Array();
          $it = $this->productcollection->getItemAssocFromPost();
          foreach ($it as $key => $val)
          {
            $item[] = $val;
          }
        }
        else
          $item = $this->productcollection->getItemByID($id);

        if (!$item['category_id'])
        {
          $item['code'] = '';
        }

        $this->productcollection->LoadItemInDBPorperties($form_data, $item);

        $url = $this->getComponentDialogLink('editProductPrices', Array('div' => 1, 'product_id' => $id), 'Edit prices', 'close');
        $form_data['edit_prices_btn'] = Array(
          "label"     => "Prices",
          "type"      => "button",
          "width"     => "50",
          "text"      => "Edit",
          "onClick"   => $this->getDesignIconScript($url, 400, 600, '', 'null')
        );

        $form_data['edit_images_codes_btn'] = Array(
          "label"     => "Images",
          "type"      => "button",
          "width"     => "50",
          "text"      => "Edit",
          "onClick"   => 'var args = new Array(window, null);
  							 openModalWin(\'?module=dialogs&action=componentframe&site_id='.$this->site_id.'&page_id='.$this->page_id.'&component_name='.$this->name.'&component_type='.get_class($this).'&method_name=editproductimagescontainer&product_id='.$id.'&refresh=0&okaction=close&session_id=' . session_id() . '&session_name=' . session_name() . '\', args, 600, 400, 0, 0, 0);'
        );

        $url = $this->getComponentDialogLink('editproductkeywordscontainer', Array('product_id' => $id));
        $form_data['edit_keywords_btn'] = Array(
          "label"     => "Keywords",
          "type"      => "button",
          "width"     => "50",
          "text"      => "Edit",
          "onClick"   => $this->getDesignIconScript($url, 400, 400, '', 'null')//'editproduct')
        );

        if ($this->relatedprodscol)
        {
          $url = $this->getComponentDialogLink('editRelatedProducts', Array('div' => 1, 'product_id' => $id), 'Edit related products', 'close');
          $form_data['edit_related_btn'] = Array(
            "label"     => "Related products",
            "type"      => "button",
            "width"     => "50",
            "text"      => "Edit",
            "onClick"   => $this->getDesignIconScript($url, 600, 500, '', 'null')
          );
        }

        $paramvalues = $this->parametercollection->getProductParameterValues($id, true);
      }else
      {
        $form_data['date_added']['value'] = time();
        $paramvalues = $this->parametercollection->getProductParameterValues(0, true);
      }

      //add subtitle above product properties
//      $form_data = array_merge(Array('title_details' => Array('label' => 'Details', 'type' => 'subtitle')), $form_data);

      foreach (array_keys($form_data) as $key)
      {
        if ($key == 'category_id')
        {
          $c = $form_data[$key];
          unset($form_data[$key]);
          $c['type'] = 'list';
          $data = $this->categorycollection->getDBData();
          $c['lookupassoc'] = Array();
          foreach ($data as $row)
          {
            $c['lookupassoc'][$row['item_id']] = str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;', $row['lvl']).$row['title_'.$this->lang];
          }
          $form_data = array_merge(Array($key => $c), $form_data);
        }
        else if (substr($key, 0, strlen('keywords_')) == 'keywords_')
        {
          unset($form_data[$key]);
        }


      }

      $form_data['days_new'] = Array(
        'label'    => 'Days left as new:',
        'type'     => 'str',
        'value'    => max(0, intval(($this->daysnew*86400 + $form_data['date_added']['value'] - time()) / 86400))
      );

      $form_data['category_id']['value'] = $_GET['category'];

      $form_data['units']['lookup'] = $this->createLookupFromCollection('units_collection', '-', 'unit_' . $this->lang);


//      $this->createMultiCheckFromCollection($form_data['piegadeslaiki'], 'piegadescol', false, 'item_id', 'name_' . $this->lang);

      $form_data['spacer'] = Array(
        "label"     => "Properties",
        "type"      => "title",
      );

      if ($this->brandscol)
      {
        $a = Array('0:(none)');
        $data = $this->brandscol->getDBData(Array('order' => 'brandname'));
        foreach ($data as $row)
        {
          $a[] = $row['item_id'].': '.$row['brandname'];
        }
        $form_data['brand_id']['lookup'] = $a;
      }

      //parametri
      //$catparams = $this->parametercollection->getCategoryParameters($_GET['category']);

      $params = $this->parametercollection->getGroupedData($this->lang);


      $lastgroup = '[unset]';
      $groupcount = 0;

      foreach($params as $p)
      {
        //if($catparams[$p['item_id']])
        //{

          if($p['group_' . $this->lang] != $lastgroup)
          {
            $form_data['groupspacer' . $groupcount] = Array(
              "label"     => "Group: " . ( $p['group_' . $this->lang] ? $p['group_' . $this->lang] : '&lt;untitled&gt;'),
              "type"      => "subtitle",
            );

            $groupcount++;
            $lastgroup = $p['group_' . $this->lang];
          }

          if($p['type'] == 0)
          {
            $form_data['params_' . $p['item_id']] = Array(
              "label"       =>  $p['title_' . $this->lang],
              //"type"        =>  "check",
              "type"        => "list",
              "lookup"      => Array("0:Hidden", "on:+", "off:-"),
              "value"       => $paramvalues[$p['item_id']]['value']
            );
          }else if($p['type'] == 1)
          {
            //
            $title = $p['title_' . $this->lang];
            if(!$title)
            {
              for($f = 0; !$title && $f < count($langs); $f++)
              {
                if($p['title_' . $langs[$f]])
                {
                  $title = $p['title_' . $langs[$f]] . ' (' . $langs[$f] . ')';
                }
              }
            }

            $list = Array('' => '');
            foreach($paramvalues[$p['item_id']]['list'] as $listitem)
            {
              $list[$listitem] = $listitem;
            }
            $form_data['params_' . $p['item_id']] = Array(
              "label"       =>  $title,
              "type"        =>  "list",
              "value"       => $paramvalues[$p['item_id']]['value'],
              "lookupassoc"      => $list,
              "combo"       => true
            );
          }
        //}
      }

      //$this->productcollection->FilterDBProperties($form_data, Array('name', 'price', 'description', 'sale_price', 'category_id', 'cat_ind'));
echo '<div id="editproduct">';
      $this->displayForm($form_data, 'editProductSave', $id, 'Edit Product', 'editProducts', '&category=' . $_GET['category']);
echo '</div>';

    }
  }

  function editProductPrices()
  {
    $prod_id = intval($_GET['product_id']);
    $this->initCollections();
    if (!$this->pricescol) return;

    if (!$prod_id)
      $prod_id = -SessionManager::$session_row_id;

    $data = $this->pricescol->getPricesByProduct($prod_id);
    foreach ($data as $key => $row)
    {
      $edit_url = $this->getComponentDialogLink('editPrice', Array('price_id' => $row['item_id']), lcms('Edit price'));
      $del_url = $this->getComponentModifierLink('deletePrice', Array('product_id' => $prod_id, 'price_id' => $row['item_id']), '1', 'editProductPrices');
      $down_url = $this->getComponentModifierLink('downPrice', Array('product_id' => $prod_id, 'price_id' => $row['item_id']), '1', 'editProductPrices');
      $up_url = $this->getComponentModifierLink('upPrice', Array('product_id' => $prod_id, 'price_id' => $row['item_id']), '1', 'editProductPrices');

      $data[$key]['icons_col'] =
        $this->getDesignIconOutput('edit', $edit_url, 400, 400, '', 'editproductprices') .
        $this->getDesignIconOutput('down', $down_url, 100, 100, '', 'editproductprices') .
        $this->getDesignIconOutput('up', $up_url, 100, 100, '', 'editproductprices') .
        $this->getDesignIconOutput('del', $del_url, 100, 100, '', 'editproductprices');

    }

    require_once($GLOBALS['cfgDirRoot']."library/"."class.datagrid.php");

    $table = new DataGrid();
    $table->cols = $this->pricescol->columns;
    $table->cols['icons_col'] = Array("title" => '');

    $table->data = $data;
    $table->footer = false;


    $edit_url = $this->getComponentDialogLink('editPrice', Array('product_id' => $prod_id, 'price_id' => 0), lcms('Add price'));
    $addjs = $this->getDesignIconScript($edit_url, 400, 400, '', 'editproductprices');

    if ($_GET['div']) echo '<div id="editproductprices">';
    echo $table->output();
    echo '<input type="button" onclick="'.$addjs.'" value="'.lcms('Add price').'"/>';

    if ($_GET['div']) echo '</div>';
  }

  function editPrice()
  {
    $this->initCollections();
    $price_id = intval($_GET['price_id']);

    $form_data = $this->pricescol->getDBProperties();
    if ($price_id)
    {
      $item = $this->pricescol->getItemByIdAssoc($price_id);
      foreach ($form_data as $key => $v)
      {
        $form_data[$key]['value'] = $item[$key];
      }
    }
    else
    {
      $prod_id = intval($_GET['product_id']);
      if (!$prod_id)
        $prod_id = -SessionManager::$session_row_id;

      $form_data['product_id']['value'] = $prod_id;
    }

    echo $this->getDialogFormOutput($form_data, 'editPriceSave', lcms('Edit price'), Array('product_id' => $form_data['product_id']['value'], 'price_id' => $price_id, 'afterrefreshmethod' => 'editProductPrices') );
  }

  function editPriceSave(){
    $this->initCollections();
    $price_id = intval($_GET['price_id']);

    $item = $this->pricescol->GetItemAssocFromPost();
    $item = $this->pricescol->processPropertyDataTypes($item);
		if ($price_id){
    	$this->pricescol->changeItemByIdAssoc($price_id, $item);
			$prodname = $this->productcollection->GetValue(array('what'=>'name_lv','where'=>'item_id='.$item['product_id']));
			$user = $_SESSION['userdata']['username'];
			$now = date('m.d.Y H:i:s');
			$log_data = array(
				'prod'=>$item['product_id'],
				//'volume'=>$price_id,
				'change_type'=>'Update',
				'user'=>$user,
				'change_comment'=>'Product "'.$prodname.'" volume '.($item['name_lv'] ? '"'.$item['name_lv'].'"' : ($item['code'] ? '"'.$item['code'].'"' : '')).' changed',
				'change_time'=>$now);
			$this->ProdLogger->change_log->Insert($log_data);
		}else{
    	$price_id = $this->pricescol->getItemID($this->pricescol->addItemAssoc($item));
			$prodname = $this->productcollection->GetValue(array('what'=>'name_lv','where'=>'item_id='.$item['product_id']));
			$user = $_SESSION['userdata']['username'];
			$now = date('m.d.Y H:i:s');
			$log_data = array(
				'prod'=>$item['product_id'],
				//'volume'=>$price_id,
				'change_type'=>'Insert',
				'user'=>$user,
				'change_comment'=>'Product "'.$prodname.'" volume '.($item['name_lv'] ? '"'.$item['name_lv'].'"' : ($item['code'] ? '"'.$item['code'].'"' : '')).'  added',
				'change_time'=>$now);
			$this->ProdLogger->change_log->Insert($log_data);
			$this->productcollection->Update(array("has_prices" => 1), array("item_id" => $item['product_id']));
    }

  }

  function deletePrice(){
    $this->initCollections();
    $price_id = intval($_GET['price_id']);

		$price = $this->pricescol->getItemByIdAssoc($price_id);
    $prod_prices = $this->pricescol->getPricesByProduct($price['product_id']);

		if(count($prod_prices) <= 1){// we have one or less before delete
    	$this->productcollection->Update(array("has_prices" => 0), array("item_id" => $price['product_id']));
		}
    $this->pricescol->delDBitem($price_id);
		$prodname = $this->productcollection->GetValue(array('what'=>'name_lv','where'=>'item_id='.$price['product_id']));
		$user = $_SESSION['userdata']['username'];
		$now = date('m.d.Y H:i:s');
		$log_data = array(
			'prod'=>$price['product_id'],
			//'volume'=>$price_id,
			'change_type'=>'Delete',
			'user'=>$user,
			'change_comment'=>'Product "'.$prodname.'" volume '.($price['name_lv'] ? '"'.$price['name_lv'].'"' : ($price['code'] ? '"'.$price['code'].'"' : '')).' deleted' ,
			'change_time'=>$now,			
			);
		$this->ProdLogger->change_log->Insert($log_data);
  }

  function upPrice()
  {
    $this->initCollections();
    $price_id = intval($_GET['price_id']);
    $prod_id = intval($_GET['product_id']);
    $this->pricescol->moveItemUp($this->pricescol->getItemInd($price_id), 'product_id='.$prod_id);
  }

  function downPrice()
  {
    $this->initCollections();
    $price_id = intval($_GET['price_id']);
    $prod_id = intval($_GET['product_id']);
    $this->pricescol->moveItemDown($this->pricescol->getItemInd($price_id), 'product_id='.$prod_id);
  }

  function editRelatedProducts()
  {
    $prod_id = intval($_GET['product_id']);
    $this->initCollections();
    if (!$prod_id || !$this->relatedprodscol) return;

    $dialogselectprod = $this->getBackendServiceByName('DialogSelectProduct');
		
    if (!$dialogselectprod) die('DialogSelectProduct backendService not found!');

    $add_js = $dialogselectprod->srvGetSelectProductDialogJs(Array('product_id'), lcms('Add related product'), 'dialogAddRelatedProductSubmit', 'editRelatedProducts', $this->name, get_class($this), 'editrelatedproducts', '&product_id='.$_GET['product_id']);

    $prod = $this->productcollection->getItemByIdAssoc($prod_id);
    $lang = $this->GetLanguageName();
    $title = lcms('Related products to').' "'.htmlspecialchars($prod['name_'.$lang]).'"';

    $data = $this->productcollection->getRelatedProducts($prod_id, $this->relatedprodscol);
    foreach ($data as $key => $row)
    {
      $del_url = $this->getComponentModifierLink('deleteRelatedProduct', Array('product_id' => $prod_id, 'related_id' => $row['item_id']), '1', 'editRelatedProducts');
      $down_url = $this->getComponentModifierLink('downRelatedProduct', Array('product_id' => $prod_id, 'relation_id' => $row['relation_id']), '1', 'editRelatedProducts');
      $up_url = $this->getComponentModifierLink('upRelatedProduct', Array('product_id' => $prod_id, 'relation_id' => $row['relation_id']), '1', 'editRelatedProducts');

      $data[$key]['icons'] =
        $this->getDesignIconOutput('down', $down_url, 100, 100, '', 'editrelatedproducts') .
        $this->getDesignIconOutput('up', $up_url, 100, 100, '', 'editrelatedproducts') .
        $this->getDesignIconOutput('del', $del_url, 100, 100, '', 'editrelatedproducts');
    }

    if ($_GET['div']) echo '<div id="editrelatedproducts">';
?>
  <h5><?=$title ?></h5>
  <input type="button" onclick="<?=$add_js ?>" value="<?=lcms('Add product')?>"/><br/>
  <table>
  <? foreach ($data as $row) { ?>
    <tr>
      <td><? if ($row['picture']) { ?><a href="<?=$row['picture'] ?>" target="_blank"><img src="<?=getThumbURL($row['picture'], 30, 23, 6) ?>" alt=""/></a><? } ?></td>
      <td><?=htmlspecialchars($row['name_'.$lang]) ?></td>
      <td><?=$row['icons'] ?></td>
    </tr>
  <? } ?>
  </table>
<?
    if ($_GET['div']) echo '</div>';
  }

  function dialogAddRelatedProductSubmit()
  {
    $prod_id = intval($_GET['product_id']);
    $related_id = intval($_POST['prod_id']);
    if (!$prod_id || !$related_id) return;

    if ($prod_id == $related_id)
    {
      echo '<script type="text/javascript">alert("Product can\'t be related to itself!")</script>';
      return;
    }

    $this->initCollections();

    $rel = $this->relatedprodscol->getRelation($prod_id, $related_id);
    if ($rel) return;

    $this->relatedprodscol->addItemAssoc(Array(
      'prod_id' => $prod_id,
      'related_id' => $related_id
    ));
  }

  function deleteRelatedProduct()
  {
    $prod_id = intval($_GET['product_id']);
    $related_id = intval($_GET['related_id']);

    $this->initCollections();

    $this->relatedprodscol->deleteRelation($prod_id, $related_id);
  }

  function downRelatedProduct()
  {
    $prod_id = intval($_GET['product_id']);
    $related_id = intval($_GET['relation_id']);

    $this->initCollections();

    $this->relatedprodscol->moveItemDown($this->relatedprodscol->getItemInd($related_id), 'prod_id='.$prod_id);
  }

  function upRelatedProduct()
  {
    $prod_id = intval($_GET['product_id']);
    $related_id = intval($_GET['relation_id']);

    $this->initCollections();

    $this->relatedprodscol->moveItemUp($this->relatedprodscol->getItemInd($related_id), 'prod_id='.$prod_id);
  }

  function editShortcut()
  {

    echo 'ok';
    return;

    $this->InitLanguageStrings();
    $langs = sqlQueryColumn('SELECT shortname FROM '.$this->site_id.'_languages ORDER BY is_default DESC');

    //initialize collection class
    if($this->initCollections())
    {

      $form_data = $this->productcollection->GetDBProperties();

      $form_data['category_id']['value'] = $_GET['category'];

      $id = $_GET['index'];

      if($id)
      {
        //edit product
        $item = $this->productcollection->getItemByID($id);
        $this->productcollection->LoadItemInDBPorperties($form_data, $item);

      }

      $this->categorycollection->FilterDBProperties($form_data, Array('shortcut', 'cat_ind', 'category_id'));

      $data = $this->productcollection->getFullProductList($this->categorycollection, $this->lang);
      $prod_array = Array();
      foreach($data as $row)
      {
        if(!$row['shortcut'])
          $prod_array[] = $row['item_id'] . ':' . str_replace(':', '', $row['name_' . $this->lang] . ' / '. $row['title_' . $this->lang]);
      }
      $form_data['shortcut']['type'] = 'list';
      $form_data['shortcut']['lookup'] = $prod_array;

      //$this->productcollection->FilterDBProperties($form_data, Array('name', 'price', 'description', 'sale_price', 'category_id', 'cat_ind'));
      $this->displayForm($form_data, 'editShortcutSave', $id, lcms('Edit Shortcut'), 'editProducts', '&category=' . $_GET['category']);

    }
  }


  function ValidateProductProperties(&$properties, $id)
  {
    if($properties['name_' . $this->lang])
    {
      if($id)
      {
        $currentprod = $this->productcollection->GetItemByIdAssoc($id);
      }else
      {
        $currentprod = false;
      }

      if ($_FILES['picture']['name'] && $_POST['picture_file_select_mode'] == 'upload')
      {
        $prod = $currentprod;
        if (!$prod)
        {
          $prod = Array();
          foreach ($properties as $key => $val)
          {
            $prod[$key] = $val;
          }
        }
        $cat_id = intval($_GET['category']);
        if (!$cat_id) $cat_id = $prod['category_id'];
        $prodpath = $this->getProductFolder(
          $this->categorycollection->getCategoryPath($cat_id),
          $prod
        );

        if($this->upload($image, 'picture', $prodpath ))
        {
          $properties['picture'] = $image;
        }else
        {
          return false;
        }
      }else
      if($_POST['picture_file_select_mode'] == 'select' && $_POST['picture_link'] != $currentprod['picture'])
      {
        $properties['picture'] = $_POST['picture_link'];
        $imagepath = getFilePathFromLink($this->site_id, $properties['picture']);
      }else
      {
        unset($properties['picture']);
      }
      return true;
    }else
    {
      return false;
    }
  }


  function editProductSave(){

    require_once($GLOBALS['cfgDirRoot'].'components/class.shopmanager.php');

    $this->InitLanguageStrings();
    //initialize collection class
    if($this->initCollections()){

      $id = $_GET['index'];
      $item = $this->productcollection->getItemByIdAssoc($id);
      $dbproperties = $this->productcollection->getDBProperties();
      $properties = Array();
      foreach($dbproperties as $key => $val){
        if ($val['type'] != 'check' && !isset($_POST[$key])) continue;
        $properties[$key] = ($val['type'] == 'check') ? (int)(bool)$_POST[$key] : $_POST[$key];
      }

      if ($item['category_id'] != $_POST['category_id']){
        $properties['cat_ind'] = 1 + sqlQueryValue('SELECT MAX(cat_ind) FROM `' . $this->productcollection->table . '` WHERE category_id = ' . intval($_POST['category_id']));
      }

      if($this->ValidateProductProperties($properties, $id)){
        if($id){
          $this->productcollection->changeProduct($id, $properties);
        }else{
          $ind = $this->productcollection->addProduct($properties);
          $id = $this->productcollection->GetItemId($ind);
        }

        $props = $properties;
        $props['item_id'] = $id;
        $p = $this->productcollection->GetItemByIdAssoc($id);
        $props['category_id'] = $p['category_id'];

        $dup = $this->productcollection->getDuplicateURLProducts($props);
        if ($dup)
        {
          foreach ($dup as $d)
          {
            $u = array();
            foreach ($d as $key => $val)
            {
              if (substr($key, 0, 4) != 'url_') continue;
              $u[$key] = $val.'-'.$d['item_id'];
            }

            $this->productcollection->changeItemByIdAssoc($d['item_id'], $u);
          }
        }

        //save parameter values
        $params = Array();
        foreach($_POST as $key => $val)
        {
          if(substr($key, 0, 7) == 'params_')
          {
            $param_id = substr($key, 7);
            $params[$param_id] = $val;
          }
        }
        $this->parametercollection->setProductParameterValues($id, $params);
      }else
      {
        echo lcms('Please fill in all required fields');
        $_GET['refresh'] = 0;
        $this->editProduct();
      }

//      $_GET['refresh'] = 0;

    }
  }

  function editShortcutSave()
  {
    $this->InitLanguageStrings();
    //initialize collection class
    if($this->initCollections())
    {
      $id = $_GET['index'];
      $properties = $this->productcollection->getProductInfo($_POST['shortcut']);
      $properties['shortcut'] = $_POST['shortcut'];
      $properties['cat_ind'] = $_POST['cat_ind'];
      $properties['category_id'] = $_POST['category_id'];
      $properties = $this->productcollection->AddItemSlashes($properties);


      if($id)
      {
        $this->productcollection->changeProduct($id, $properties);
      }else
      {
        $ind = $this->productcollection->addProduct($properties, false);
        $id = $this->productcollection->GetItemId($ind);
      }

      //reindex
//      $this->SearchIndexProducts();
    }
  }

  function removeProduct()
  {
    if($this->initCollections())
    {
  	  $ind=$_GET["index"];
  	  $this->productcollection->deleteProduct($ind);
    }
  }


  function upProduct()
  {
    if($this->initCollections())
    {
  	  $id=$_GET["index"];
  	  $this->productcollection->moveUpProduct($id);
    }
  }

  function downProduct()
  {
    if($this->initCollections())
    {
  	  $id=$_GET["index"];
  	  $this->productcollection->moveDownProduct($id);
    }
  }


  //=========================================================================//
  // Category editing
  //=========================================================================//

  function editCategory()
  {
    $this->InitLanguageStrings();
    //initialize collection class
    if($this->initCollections())
    {
      $form_data = $this->categorycollection->GetDBProperties();
      $id = $_GET['index'];

      $pvalues = Array();

      if($id)
      {
        $item = $this->categorycollection->getItemByID($id);
        $this->categorycollection->LoadItemInDBPorperties($form_data, $item);
      }

      $c = $form_data['parent'];
      unset($form_data['parent']);
      $c['type'] = 'list';


      $data = $this->categorycollection->getDBData();
      $cats = array();
      foreach ($data as $row)
      {
        $cats[$row['item_id']] = $row;
      }

      $c['lookupassoc'] = Array(0 => '(root)');
      foreach ($cats as $cid => $row)
      {
        $pid = $cid;
        while ($pid)
        {
          if ($pid == $id) continue 2;
          $pid = $cats[$pid]['parent'];
        }

        $c['lookupassoc'][$cid] = str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;', $row['lvl']).$row['title_'.$this->lang];
      }

      $form_data = array_merge(Array('parent' => $c), $form_data);

      $first_title_field = '';

      foreach (array_keys($form_data) as $key)
      {
        if (substr($key, 0, strlen('keywords_')) == 'keywords_')
          $form_data[$key]['type'] = 'hidden';

        if (substr($key, 0, strlen('description_')) == 'description_')
          $form_data[$key]['type'] = 'hidden';

        if (substr($key, 0, strlen('additional_header_tags_')) == 'additional_header_tags_')
          $form_data[$key]['type'] = 'hidden';

        if (!$first_title_field && substr($key, 0, strlen('title_')) == 'title_')
        {
          $first_title_field = $key;
        }

      }

      $active_filters = $id ? $this->CatFilterGroupRel->getGroupIds($id) : array();
      $FilterGroups = $this->FilterGroups->GetDBData();

      $form_data['FilterGroups'] = Array(
        "label" => lcms("Filter groups"),
        "type" => "code",
        "value" => $this->display_filters_html($FilterGroups, $active_filters)
      );


      $url = $this->getComponentDialogLink('editCategoryMeta', Array('category_id' => $id));
      $form_data['metabtn'] = Array(
          "label"     => lcms("Meta description"),//lcms("Meta keywords & description"),
          "type"      => "button",
          "width"     => "50",
          "text"      => "Edit",
          "onClick"   => $this->getDesignIconScript($url, 400, 600, '', 'null')
        );

      $this->displayForm($form_data, 'editCategorySave', $id, lcms('Edit Category'));
?>
  <script type="text/javascript">

  	var files = {};

  	$(document).ready(function(){

    	$("#Form input[type='file']").change(function(){

      	files[$(this).attr("name")] = $(this)[0].files[0];


    	});

  	});

  document.getElementById('<?=$first_title_field ?>').focus();

  </script>
<?
    }
  }

  function editCategoryMeta()
  {
//    if (!$_GET['category_id']) return;

    $this->initCollections();
    $fdata = $this->categorycollection->GetDBProperties();

    if ($_POST)
    {
//      var_dump($_POST);
      $item = Array();
      foreach (array_keys($fdata) as $key)
      {
        if (substr($key, 0, strlen('keywords_')) == 'keywords_')
          $item[$key] = $_POST[$key];

        if (substr($key, 0, strlen('description_')) == 'description_')
          $item[$key] = $_POST[$key];

        if (substr($key, 0, strlen('additional_header_tags_')) == 'additional_header_tags_')
          $item[$key] = $_POST[$key];


      }

      $this->categorycollection->changeItemByIdAssoc(intval($_GET['category_id']), $item);
      return;
    }


    $item = $this->categorycollection->getItemByID(intval($_GET['category_id']));
    $this->categorycollection->LoadItemInDBPorperties($fdata, $item);

    $form_data = Array();
    foreach (array_keys($fdata) as $key)
    {
      if (substr($key, 0, strlen('keywords_')) == 'keywords_')
        $form_data[$key] = $fdata[$key];

      if (substr($key, 0, strlen('description_')) == 'description_')
        $form_data[$key] = $fdata[$key];

      if (substr($key, 0, strlen('additional_header_tags_')) == 'additional_header_tags_')
        $form_data[$key] = $fdata[$key];

    }

    echo $this->getDialogFormOutput($form_data, 'editCategoryMeta', lcms('Edit'), Array('category_id' => $_GET['category_id']));

?>
<script type="text/javascript">

var fields = <?=json_encode(array_keys($form_data)) ?>;
for (var i=0; i<fields.length; i++)
{
  var name = fields[i];
  document.Form[name].value = window.parent.dialogArguments[0].document.Form[name].value;
}

var win = window;
window.parent.document.Form.Ok.onclick = function()
{
  for (var i=0; i<fields.length; i++)
  {
    var name = fields[i];
    window.parent.dialogArguments[0].document.Form[name].value = document.Form[name].value;
    window.parent.close();
  }
  return false;
}

</script>
<?

  }

  function ValidateCategoryProperties($properties)
  {
		$errors = array();

    if(!$properties['title_' . $this->lang]) $errors[] = "Kategorijas nosaukums ir obligāts lauks";

		if($properties['parent']){

			$products = $this->productcollection->GetValue(array("what" => "count(*)", "where" => "category_id = :cid", "params" => array(":cid" => $properties['parent'])));

			if($products){
				$errors[] = "Izvēlētajā virskategorijā atrodas produkti. Izvēlieties citu virskategoriju";
			}

		}

		return $errors;

  }

  function editCategorySave(){

    $this->InitLanguageStrings();
    //initialize collection class
    if($this->initCollections()){
      $id = $_GET['index'];
      $properties = $this->categorycollection->getDBProperties();
      foreach($properties as $key => $val){
        $properties[$key] = $_POST[$key];
      }

			$properties = $this->categorycollection->processPropertyDataTypes($properties);

			// process urls
			$langs = getLanguages();
			$short_names = $my_urls = array();

        foreach($langs as $lang){
            $short_names[] = $lang['shortname'];
        }

			$i = 0;
      foreach($short_names as $lang){

        if(!$properties['url_'.$lang]){ // we dont have URL, lets make it

        $url = ($properties['title_'.$lang]) ? ($properties['title_'.$lang]) : strtolower("category-".$lang); // starting url from title or default
        $t_url = $url = transliterateURL($url);
        $check = in_array($url, $my_urls) || !$this->categorycollection->checkIfUrlIsUnique($url, $short_names);

          while($check){ // while we are not unique
	        	$t_url = transliterateURL($url."-".(++$i));
	          $check = in_array($t_url, $my_urls) || !$this->categorycollection->checkIfUrlIsUnique($t_url, $short_names);
	        }

          // here we should be unique
					$url = $t_url;
          $properties['url_'.$lang] = $url;
					$my_urls[] = $url;

				}

			}
			// process urls end

      $properties['featured_image'] = getFormFile('featured_image', $this->site_id, 'images/cats/');
      $properties['image'] = getFormFile('image', $this->site_id, 'images/cats/');
      $properties['icon'] = getFormFile('icon', $this->site_id, 'images/cats/');
      $properties['mob_icon'] = getFormFile('mob_icon', $this->site_id, 'images/cats/');

      $properties['show_in_menu'] = (int)(boolean)$_POST['show_in_menu'];
      $properties['featured'] = (int)(boolean)$_POST['featured'];
      $properties['disabled'] = (int)(boolean)$_POST['disabled'];
      $properties['uncategorized_item_cat'] = (int)(boolean)$_POST['uncategorized_item_cat'];

      if($properties['uncategorized_item_cat'] == 1){
        sqlQuery("UPDATE `".$this->categorycollection->table."` SET uncategorized_item_cat = 0"); // clear other
      }

      $errors = $this->ValidateCategoryProperties($properties);

      if(empty($errors)){

				$lvl = 0;
	      if ($properties['parent'])
	      {
	        $catpath = $this->categorycollection->getCategoryPath($properties['parent']);
	        $lvl = count($catpath);
	      }
	      $properties['lvl'] = $lvl;

				if($id){
		      $row = $this->categorycollection->getItemByIdAssoc($id);
		      if ($row['parent'] != $properties['parent']){
		        $this->categorycollection->moveSubcategory($properties['parent'], $row['item_id']);
		      }
				}

				if($id){ // existing category
          $this->categorycollection->changeCategory($id, $properties);
        }else{ // new category
          $ind = $this->categorycollection->addCategory($properties);
          $id = $this->categorycollection->GetItemId($ind);
        }

        // process filter groups
	      $cat_filter_groups = $this->CatFilterGroupRel->getGroupIds($id);

	      // adding new
          if(isset($_POST['FilterGroups']) && is_array($_POST['FilterGroups'])){
    	      foreach($_POST['FilterGroups'] as $group){
    	        if(!in_array($group, $cat_filter_groups)){
    	          $this->CatFilterGroupRel->AddItemAssoc(Array("category_id" => $id, "filter_group_id" => $group));
    	        }
    	      }

    	      // remove missing
    	      foreach($cat_filter_groups as $group){
    	        if(!in_array($group, $_POST['FilterGroups'])){
    	          $this->CatFilterGroupRel->DeleteByCatAndFilter($id, $group);
    	        }
    	      }
          }
	      // process filter groups end

        echo 1;

      }else{

				echo json_encode(array("errors" => implode("\n", $errors)));

				/*
				echo "Kļūdas:<br/ > ".implode("<br />", $errors);
        $_GET['refresh'] = 0;
        $this->editCategory();
				*/
			}
			die();

    }
  }

  function ajax_upCategory()
  {
    $this->upCategory();
  }

  function upCategory()
  {
    if($this->getCategoryCollection())
    {
  	  $id=$_GET["index"];
  	  $this->categorycollection->moveUpCategory($id);
    }
  }


  function ajax_downCategory()
  {
    $this->downCategory();
  }

  function downCategory()
  {
    if($this->getCategoryCollection())
    {
  	  $id=$_GET["index"];
  	  $this->categorycollection->moveDownCategory($id);
    }
  }



  function ajax_removeCategory()
  {
    $this->removeCategory();
  }

  function removeCategory()
  {
    if($this->initCollections())
    {
  	  $ind=$_GET["index"];
  	  $this->categorycollection->deleteCategory($ind, $this->productcollection);
    }
  }


  //=========================================================================//
  // Custom product lists
  // for catalogs that require specific lists per product, like several images
  // modify required collection and methods accordingly
  //=========================================================================//

  //validate form fields here (are required ones filled, are they filled correctly)
  //and process file uploads here
  function ValidateProductImageProperties(&$properties, $prodpath)
  {
    if ($_FILES['image']['name'])
    {
      if($this->uploadProductImage($image, 'image', $prodpath))
      {
        $properties['image'] = $image;
      }else
      {
        return false;
      }
    }else
    {
      unset($properties['image']);
    }
    return true;
  }

  function uploadProductImage(&$image, $inputname = 'image', $prodpath)
  {
    global $_FILES;
    //file upload
    if ($_FILES[$inputname]["type"] == "image/gif" || $_FILES[$inputname]["type"] == "image/jpeg" || $_FILES[$inputname]["type"] == "image/pjpeg" || $_FILES[$inputname]["type"] == "image/x-png")
    {
        $format_ok = true;
    }
    else
    {
        $format_ok = false;
        echo '<div align="center" style="color:red;">'.lcms('Invalid file format!').'</div>';
        return false;
    }

    $path = 'upload/products/' . $prodpath . '_customimages/';
    forceDirectories($this->site_id, $path);
    $up_data = uploadSiteFile($this->site_id, $_FILES[$inputname], $path);

    if($up_data)
    {
      $type = strtolower( substr($up_data['path'], strrpos($up_data['path'], '.') + 1) );
      resizeKeepAspectUploadedImage($up_data['path'], $type, 1024, 1024, $up_data['path'], 6);

      $image = $up_data['url'];
      return true;
    }else
    {
      print lcms("File was not uploaded");
      return false;
    }
  }

  //process edit/add form results
  function editProductImageSave()
  {
    if (!$this->initCollections()) return;

    $productimages = $this->InitPropertyCollection($this->custom_product_list_collection);
    if (!$productimages) return;

    $id = intval($_GET['index']);

    $properties = $productimages->getDBProperties();
    foreach($properties as $key => $val)
    {
      $properties[$key] = $_POST[$key];
    }

    $catcol = $this->InitPropertyCollection('cat_collection');
    $prodscol = $this->InitPropertyCollection('prod_collection');
    $prod = $prodscol ? $prodscol->getItemByIdAssoc(intval($_GET['product_id'])) : null;

    $prodpath = $this->getProductFolder(
      $catcol->getCategoryPath($prod['category_id']),
      $prod
    );

    if($this->ValidateProductImageProperties($properties, $prodpath))
    {
      if($id)
      {
        $productimages->ChangeItemByIDAsoc($id, $properties);
      }else
      {
        if ($properties['image'])
        {
          $this->productcollection->changeItemByIdAssoc($_GET['product_id'], array(
            'picture' => $properties['image']
          ));
        }

      }
    }else
    {
      echo lcms('Please fill in all required fields');
      $_GET['refresh'] = 0;
      $this->editProductImages();
    }

  }

  //display add form
  function addproductimage()
  {
    if($_GET['product_id'])
    {
      $productimages = $this->InitPropertyCollection($this->custom_product_list_collection);
      if($productimages)
      {
        $form_data = $productimages->GetDBProperties();
        $form_data['product_id']['value'] = $_GET['product_id'];
        $this->displayForm($form_data, 'editProductImageSave', 0, lcms('Add Item'), 'editproductimages', '&product_id=' . $_GET['product_id']);
      }
    }
  }

  //display edit form
  function editProductImage()
  {
    if (!$this->initCollections()) return;

    $prod = $this->productcollection->getItemByIdAssoc($_GET['product_id']);

    $productimages = $this->InitPropertyCollection($this->custom_product_list_collection);
    if($productimages)
    {

      $form_data = $productimages->GetDBProperties();
      $id = $_GET['index'];

      if($id)
      {
        //edit product
        $item = $productimages->getItemByID($id);
        $productimages->LoadItemInDBPorperties($form_data, $item);
      }
      else
      {
        $form_data['image']['value'] = $prod['picture'];
        foreach ($form_data as $key => $f)
        {
          if ($key != 'image')
            $form_data[$key]['type'] = 'hidden';

        }
      }

      if($form_data['image']['value'])
      {
        $form_data['thumb'] = Array(
          "type" => 'code',
          "value" => '<img src="'.getThumbURL($form_data['image']['value'], 120, 90, 6).'">',
        );
      }

      $this->displayForm($form_data, 'editProductImageSave', $id, lcms('Edit Item'), 'editproductimages', '&product_id=' . $_GET['product_id']);
    }
  }

  function downProductImage(){

    if (!$this->initDesign()) return;

    $productimages = $this->InitPropertyCollection($this->custom_product_list_collection);
    $prod_id = intval($_GET['product_id']);
    if (!$productimages) return;

    if (!$prod_id)
      $prod_id = -SessionManager::$session_row_id;

    if ($_GET['index'] > 0){

      $productimages->moveItemDownForProduct($_GET['index'], $prod_id);
      $first = $productimages->getFirstImage($prod_id);
			$this->productcollection->changeItemByIdAssoc($prod_id, array(
        'picture' => $first['image']
      ));

    }else{

      $img = $productimages->getFirstImage($prod_id);
      $prod = $this->productcollection->getItemByIdAssoc($prod_id);

      $this->productcollection->changeItemByIdAssoc($prod_id, array(
        'picture' => $img['image']
      ));

      $productimages->changeItemByIdAssoc($img['item_id'], array(
        'image' => $prod['picture']
      ));

		}

  }

  function upProductImage()
  {
    if (!$this->initDesign()) return;

    $productimages = $this->InitPropertyCollection($this->custom_product_list_collection);
    $prod_id = intval($_GET['product_id']);
    if (!$productimages) return;

    if (!$prod_id)
      $prod_id = -SessionManager::$session_row_id;

    if ($productimages->getUpInd($_GET['index'], $prod_id) > 0){

			$productimages->moveItemUpForProduct($_GET['index'], $prod_id);
			$first = $productimages->getFirstImage($prod_id);
			$this->productcollection->changeItemByIdAssoc($prod_id, array(
        'picture' => $first['image']
      ));

    }else{
      $img = $productimages->getFirstImage($prod_id);
      $prod = $this->productcollection->getItemByIdAssoc($prod_id);

      $this->productcollection->changeItemByIdAssoc($prod_id, array(
        'picture' => $img['image']
      ));

      $productimages->changeItemByIdAssoc($img['item_id'], array(
        'image' => $prod['picture']
      ));
    }

  }

  function removeProductImage(){
    $productimages = $this->InitPropertyCollection($this->custom_product_list_collection);

    if($productimages){
      $productimages->DelDBItem($_GET['index']);
    }

		$first = $productimages->getFirstImage($prod_id);
		$this->productcollection->changeItemByIdAssoc($prod_id, array(
       'picture' => $first['image'] ? $first['image'] : ''
    ));
  }

  function geteditbuttonsformated()
  {
    $params = Array(
        'index' => '{%item_id%}',
        'product_id' => $_GET['product_id']
    );

    $link = $this->getComponentModifierLink('downProductImage', $params, 1, 'editproductimages');
    $imgdown = $this->getDesignIconOutput('down', $link, 1, 1, '', 'productimageslist');

    $link = $this->getComponentModifierLink('upProductImage', $params, 1, 'editproductimages');
    $imgup = $this->getDesignIconOutput('up', $link, 1, 1, '', 'productimageslist');

    $link = $this->getComponentDialogLink('editProductImage', $params, lcms('Change image'));
    $imgedit = $this->getDesignIconOutput('edit', $link, 400, 500, '', 'productimageslist');

    $link = $this->getComponentModifierLink('removeProductImage', $params, 1, 'editproductimages');
    $imgdel = $this->getDesignIconOutput('del', $link, 1, 1, lcms('Are you sure you want to delete this item?'), 'productimageslist');

    $data = $imgdown . $imgup . $imgedit . $imgdel;

    //urldecode index field
    $data = str_replace('%7B%25item_id%25%7D', '{%item_id%}', $data);

    return $data;
  }

  function editproductimagescontainer()
  {
    echo '<div id="productimageslist">';
    $this->editproductimages();
    echo '</div>';
  }

  function editproductkeywordscontainer()
  {
    echo '<div id="productkeywords">';
    $this->editproductkeywords();
    echo '</div>';
  }

  function editproductkeywords()
  {
    if (!$_GET['product_id']) return;

    $this->initCollections();
    $fdata = $this->productcollection->GetDBProperties();

    if ($_POST)
    {
      $item = Array();
      foreach (array_keys($fdata) as $key)
      {
        if (substr($key, 0, strlen('keywords_')) == 'keywords_')
          $item[$key] = $_POST[$key];

      }

      $this->productcollection->changeItemByIdAssoc(intval($_GET['product_id']), $item);
      return;
    }


    $item = $this->productcollection->getItemByID(intval($_GET['product_id']));
    $this->productcollection->LoadItemInDBPorperties($fdata, $item);

    $form_data = Array();
    foreach (array_keys($fdata) as $key)
    {
      if (substr($key, 0, strlen('keywords_')) == 'keywords_')
        $form_data[$key] = $fdata[$key];

    }

    echo $this->getDialogFormOutput($form_data, 'editproductkeywords', lcms('Edit'), Array('product_id' => $_GET['product_id']));
  }

  function editproductimages()
  {
    if($_GET['product_id'])
    {
      $productimages = $this->InitPropertyCollection($this->custom_product_list_collection);
      if($productimages)
      {
        $data = $productimages->getImagesForProduct($_GET['product_id']);
        foreach ($data as $key => $row)
        {
          if (!$row['image']) continue;
          $data[$key]['imagethumb'] = getThumbURL($row['image'], 120, 90, 6);
        }

        //output table
        require_once($GLOBALS['cfgDirRoot']."library/"."class.datagrid.php");

        $Table = new DataGrid();
        $Table->cols = $productimages->columns;
        $Table->cols['control_column'] = Array(
          "width"   => "80px",
          "title"   => "",
          "format"  => $this->geteditbuttonsformated()
        );
        $Table->data = $data;
        $Table->footer = false;

        echo $Table->output();

        echo '<input value="Pievienot papildus attēlu produktam" type="button" onClick="javascript:
        var args = new Array(window, productimageslist);
        openModalWin(\'?module=dialogs&action=componentframe&site_id='.$this->site_id.'&page_id='.$this->page_id.'&component_name='.$this->name.'&component_type='.get_class($this).'&method_name=addproductimage&refresh=0&okaction=submit&product_id='.$_GET['product_id'].'&session_id=' . session_id() . '&session_name=' . session_name() . '\', args, 400, 500, 0, 0, 0);">';
      }
    }
  }

  //=========================================================================//
  // Misc. utilities
  //=========================================================================//

  function upload(&$image, $inputname = 'picture', $prodpath)
  {
    global $_FILES;
    //file upload
    if ($_FILES[$inputname]["type"] == "image/gif" || $_FILES[$inputname]["type"] == "image/jpeg" || $_FILES[$inputname]["type"] == "image/pjpeg" || $_FILES[$inputname]["type"] == "image/x-png")
    {
        $format_ok = true;
    }
    else
    {
        $format_ok = false;
        echo '<div align="center" style="color:red;">'.lcms('Invalid file format!').'</div>';
        return false;
    }

    $path = 'images/products/' . $prodpath;

    forceDirectories($this->site_id, $path);
    $up_data = uploadSiteFile($this->site_id, $_FILES[$inputname], $path);

    if($up_data)
    {
      $type = strtolower( substr($up_data['path'], strrpos($up_data['path'], '.') + 1) );
      resizeKeepAspectUploadedImage($up_data['path'], $type, 1024, 1024, $up_data['path'], 6);

      $image = $up_data['url'];
      return true;
    }else
    {
      print lcms("File was not uploaded to: ").$path;
      return false;
    }
  }


  function createLookupFromCollection($property, $addnull = '-', $colname = 'col0')
  {
    //initialize collection
    $colid = $this->getProperty($property);
    $s = Array();
    if($addnull)
      $s[] = '0:' . $addnull;
    if($colid)
    {
      $type = $this->properties[$property]['collectiontype'];
      include_once($GLOBALS['cfgDirRoot'] . "collections/class.".$type.".php");
      $collectionname = '';
      $collection = new $type($collectionname,$colid);
      if($collection->isValidCollection())
      {
        $data = $collection->getDBData();
        foreach($data as $row)
        {
          $s[] = $row['item_id'] . ':' . $row[$colname];
        }
      }
    }
    return $s;
  }

  function getMultiCheckFromCollectionURL($property, $useid, $idcolname, $colname, $params='')
  {
    //initialize collection
    $colid = $this->getProperty($property);
    if($colid)
    {
      $type = $this->properties[$property]['collectiontype'];
      include_once($GLOBALS['cfgDirRoot'] . "collections/class.".$type.".php");
      $collectionname = '';
      $collection = new $type($collectionname,$colid);
      if($collection->isValidCollection())
      {
        return "?module=dialogs&action=multichecklist&site_id=" . $this->site_id . "&coltype=" . $type . "&categoryid=" . $colid . "&useid=" . ($useid?'1':'0') . "&columnname=" . $colname . "&idcolumnname=" . $idcolname;
      }
    }

    return '';
  }

  function createMultiCheckFromCollection(&$formfield, $property, $useid, $idcolname, $colname)
  {
    $url = $this->getMultiCheckFromCollectionURL($property, $useid, $idcolname, $colname);
    if (!empty($url))
    {
      $formfield['type'] = 'dialog';
      $formfield['dialog'] = $url;
      return true;
    }

    return false;
  }


  function displayForm($form_data, $methodname, $index, $title, $afterrefreshmethod = '', $additionalgetdata = '', $showbuttons = false)
  {
    require_once($GLOBALS['cfgDirRoot']."library/"."class.formgen.php");
    $FormGen = new FormGen();
    $FormGen->action = '?module=dialogs&action=component&site_id='.
		$this->site_id.'&page_id='.$this->page_id.'&component_name='.
		$this->name.'&component_type='.
		get_class($this).'&method_name='.$methodname.'&afterrefreshmethod='.$afterrefreshmethod.'&refresh=1&index=' . $index . '&p_pagedev_id='.$this->pagedev_id . $additionalgetdata;
    $FormGen->title = $title;
    $FormGen->properties = $form_data;
    $FormGen->buttons = $showbuttons;
    echo $FormGen->output();
  }


  //=========================================================================//
  // Delete, Edit, Add Buttons
  //=========================================================================//
  function designAddButtons()
  {
    $data = '<div class="buttons">';

    $data .= '<a href="#" class="btn3" id="btn_add_cat">'.lcms('Add Category').'</a><br />';

    $data .= '<a href="#" class="btn3" id="btn_expand_cats">'.lcms('Expand all').'</a>&nbsp;';
    $data .= '<a href="#" class="btn3" id="btn_collapse_cats">'.lcms('Collapse all').'</a><br />';

    $data .= '<a href="#" class="btn3" id="btn_add_prod" style="display:none">'.lcms('Add product').'</a>&nbsp;';
    $data .= '<a href="#" class="btn3" id="btn_add_shortcut" style="display:none">'.lcms('Add shortcut').'</a><br />';

    $data .= '</div>';

    return $data;
  }


  function designEditCollectionButton($property, $title)
  {
    $categoryID = $this->getProperty($property);
    $coltype = $this->properties[$property]['collectiontype'];
    return '<input type="button" style="width: auto" value="'.$title.'" OnClick="javascript:
       							     var args = new Array(window, \'div_'.$this->name.'\');
                                                             var val=openModalWin(\'?module=dialogs&action=collection&site_id='.$this->site_id.'&page_id='.$this->page_id.'&category_id='.$categoryID.'&newname=&justedit=1&coltype='.$coltype.'&session_id=' . session_id() . '&session_name=' . session_name() . '\', args, 600, 450);

                                                             try {
                                                               PWin.close();
                                                             } catch(e) { }
                                                             ">';
  }



  function designAddProductButton($category)
  {
//    $url = $this->getComponentDialogLink('editProduct', Array('category' => $category), 'Add product');
//    return $this->getDesignIconScript($url, 400, 500, 'productslist');

    $data = '';
      $data .=  '<input type="button" value="Add Product"
      OnClick="javascript:
                   try {
                     PWin.close();
                   } catch(e) { }
                     var args = new Array(window, productslist);
                     openModalWin(\'?module=dialogs&action=componentframe&site_id='.$this->site_id.'&page_id='.$this->page_id.'&component_name='.$this->name.'&component_type='.get_class($this).'&method_name=editProduct&refresh=0&okaction=submit&session_id=' . session_id() . '&session_name=' . session_name() . '&category='.$category.'\', args, 400, 500, 0, 0, 0);"
      >&nbsp;&nbsp;';


    return $data;
  }

  function editCategoryProductsLink($item_id)
  {
    return 'javascript:
                       var args = new Array(window, \'div_'.$this->name.'\');
                       openModalWin(\'?module=dialogs&action=componentframe&site_id='.$this->site_id.'&page_id='.$this->page_id.'&component_name='.$this->name.'&component_type='.get_class($this).'&method_name=editProductsContainer&refresh=0&okaction=close&session_id=' . session_id() . '&session_name=' . session_name() . '&category='.$item_id.'\', args, 700, 600, 0, 0, 0);
                       ';
  }


  function getDesignIconOutput2($img, $link, $width=1, $height=1, $confirm='', $targetdiv='')
  {
    $imgsrc = $GLOBALS['cfgWebRoot'].'gui/images/'.$img;
    $r = '<img src="'.$imgsrc.'"
			onclick="'.$this->getDesignIconScript($link, $width, $height, $confirm, $targetdiv).'"
			>';
    return $r;
  }


  function editProductLink($item_id, $category)
  {
    return 'javascript:var args = new Array(window, productslist);
                       openModalWin(\'?module=dialogs&action=componentframe&site_id='.$this->site_id.'&page_id='.$this->page_id.'&component_name='.$this->name.'&component_type='.get_class($this).'&method_name=editProduct&afterrefreshmethod=editProducts&refresh=0&okaction=submit&session_id=' . session_id() . '&session_name=' . session_name() . '&index='.$item_id.'&category='.$category.'\', args, 400, 800, 0, 0, 0);';
  }


  function editProductButtons($item_id, $category)
  {
    $data = '';
    if ($item_id)
    {

        $data .=  '<img src="'.$GLOBALS['cfgWebRoot'].'gui/images/edit.gif" alt="Edit product"
        OnClick="javascript:
                     try {
                       PWin.close();
                     } catch(e) { }
                       var args = new Array(window, productslist);
                       openModalWin(\'?module=dialogs&action=componentframe&site_id='.$this->site_id.'&page_id='.$this->page_id.'&component_name='.$this->name.'&component_type='.get_class($this).'&method_name=editProduct&afterrefreshmethod=editProducts&refresh=0&okaction=submit&session_id=' . session_id() . '&session_name=' . session_name() . '&index='.$item_id.'&category='.$category.'\', args, 400, 800, 0, 0, 0);"
        >';


        $data .=  '<img src="'.$GLOBALS['cfgWebRoot'].'gui/images/down.gif"

  		OnClick="javascript:
         							     var args = new Array(window, productslist);
  									 openModalWin(\'?module=dialogs&action=component&site_id='.$this->site_id.'&page_id='.$this->page_id.'&component_name='.$this->name.'&component_type='.get_class($this).'&method_name=downProduct&afterrefreshmethod=editProducts&refresh=1&session_id=' . session_id() . '&session_name=' . session_name() . '&index='.$item_id.'&category='.$category.'\', args, 1, 1, 0, 0, 0);"
  		 alt="Move item down">';


  		$data .=  '<img src="'.$GLOBALS['cfgWebRoot'].'gui/images/up.gif"
  		OnClick="javascript:
         							     var args = new Array(window, productslist);
  									 openModalWin(\'?module=dialogs&action=component&site_id='.$this->site_id.'&page_id='.$this->page_id.'&component_name='.$this->name.'&component_type='.get_class($this).'&method_name=upProduct&afterrefreshmethod=editProducts&refresh=1&session_id=' . session_id() . '&session_name=' . session_name() . '&index='.$item_id.'&category='.$category.'\', args, 1, 1, 0, 0, 0);"
  		alt="Move item up">';


        $data .= '<img src="'.$GLOBALS['cfgWebRoot'].'gui/images/del.gif" alt="Delete product"
  			OnClick="javascript:
  										 if (confirm(\'Are you sure you want to delete this product?\')) {
                                         var args = new Array(window, productslist);
  										 openModalWin(\'?module=dialogs&action=component&site_id='.$this->site_id.'&page_id='.$this->page_id.'&component_name='.$this->name.'&component_type='.get_class($this).'&method_name=removeProduct&afterrefreshmethod=editProducts&refresh=1&session_id=' . session_id() . '&session_name=' . session_name() . '&index='.$item_id.'&category='.$category.'\', args, 1, 1, 0, 0, 0); }"
  			>';
    }
    return $data;
  }

  function editShortcutButtons($item_id, $category, $shortcut)
  {
    $data = '';
    if ($item_id)
    {

        $data .=  '<img src="'.$GLOBALS['cfgWebRoot'].'gui/images/edit.gif" alt="Edit product"
        OnClick="javascript:
                     try {
                       PWin.close();
                     } catch(e) { }
                       var args = new Array(window, productslist);
                       openModalWin(\'?module=dialogs&action=componentframe&site_id='.$this->site_id.'&page_id='.$this->page_id.'&component_name='.$this->name.'&component_type='.get_class($this).'&method_name=editProduct&afterrefreshmethod=editProducts&refresh=0&okaction=submit&session_id=' . session_id() . '&session_name=' . session_name() . '&index='.$item_id.'&category='.$category.'\', args, 400, 800, 0, 0, 0);"
        >';

        $data .=  '<img src="'.$GLOBALS['cfgWebRoot'].'gui/images/down.gif" alt="Move shortcut down"

  		  OnClick="javascript:
         							     var args = new Array(window, productslist);
  									 openModalWin(\'?module=dialogs&action=component&site_id='.$this->site_id.'&page_id='.$this->page_id.'&component_name='.$this->name.'&component_type='.get_class($this).'&method_name=downProduct&afterrefreshmethod=editProducts&refresh=1&session_id=' . session_id() . '&session_name=' . session_name() . '&index='.$shortcut['item_id'].'&category='.$shortcut['category_id'].'\', args, 1, 1, 1, 0, 0);"
        >';


  		  $data .=  '<img src="'.$GLOBALS['cfgWebRoot'].'gui/images/up.gif" alt="Move shortcut up"
  		  OnClick="javascript:
         							     var args = new Array(window, productslist);
  									 openModalWin(\'?module=dialogs&action=component&site_id='.$this->site_id.'&page_id='.$this->page_id.'&component_name='.$this->name.'&component_type='.get_class($this).'&method_name=upProduct&afterrefreshmethod=editProducts&refresh=1&session_id=' . session_id() . '&session_name=' . session_name() . '&index='.$shortcut['item_id'].'&category='.$shortcut['category_id'].'\', args, 1, 1, 0, 0, 0);"
  		  >';


        $data .= '<img src="'.$GLOBALS['cfgWebRoot'].'gui/images/del.gif" alt="Delete shortcut"
  			OnClick="javascript:
  										 if (confirm(\'Are you sure you want to delete this shortcut?\')) {
                                         var args = new Array(window, productslist);
  										 openModalWin(\'?module=dialogs&action=component&site_id='.$this->site_id.'&page_id='.$this->page_id.'&component_name='.$this->name.'&component_type='.get_class($this).'&method_name=removeProduct&afterrefreshmethod=editProducts&refresh=1&session_id=' . session_id() . '&session_name=' . session_name() . '&index='.$shortcut['item_id'].'&category='.$shortcut['category_id'].'\', args, 1, 1, 0, 0, 0); }"
  			>';
    }
    return $data;
  }


  function getProductFolder($catpath, $prod)
  {
    $path = '';
    foreach ($catpath as $row)
    {
      $c = $row['title_'.$this->GetLanguageName() ];
      if (!$c)
      {
        $langs = getLanguages($this->site_id);
        foreach ($langs as $key => $lang)
        {
          if (!empty($row['title_'.$key]))
          {
            $c = $row['title_'.$key];
            break;
          }
        }
      }
      if (!$c) $c = $row['item_id'];

      $path .= transliterateURL($c, '-_0123456789abcdefghijklmnopqrstuvwxyz') . '/';
    }

    if ($prod)
    {
      $c = $prod['name_'.$this->GetLanguageName()];
      if (!$c)
      {
        $langs = getLanguages($this->site_id);
        foreach ($langs as $key => $lang)
        {
          if (!empty($prod['name_'.$key]))
          {
            $c = $prod['name_'.$key];
            break;
          }
        }
      }
      if (!$c) $c = $prod['item_id'];
    }
    $path .= transliterateURL($c, '-_0123456789abcdefghijklmnopqrstuvwxyz') . '/';

    return $path;
  }

  //=========================================================================//
  // Collection initialization routines
  //=========================================================================//
  function initCollections(){

    $ok = true;
    $ok = $ok && $this->getCategoryCollection();
    $this->collections['categories'] = $this->categorycollection;
    $ok = $ok && $this->getProductCollection();
    $this->collections['products'] = $this->productcollection;

    $this->usercollection = $this->initPropertyCollection('user_collection');
    $ok = $ok && ($this->usercollection !== false);

    $this->relatedprodscol = $this->initPropertyCollection('relatedprodscol');
    $this->brandscol = $this->initPropertyCollection('brandscol');
    $this->pricescol = $this->initPropertyCollection('pricescol');

    $this->imagescol = $this->InitPropertyCollection($this->custom_product_list_collection);
    $this->unitscol = $this->initPropertyCollection('units_collection');

    option("shop\\category_table", $this->categorycollection->table, "Kategoriju tabula", Array("is_advanced" => true), $this->categorycollection->table, $this->site_id);
    option("shop\\product_table", $this->productcollection->table, "Produktu tabula", Array("is_advanced" => true), $this->productcollection->table, $this->site_id);
    $path = $this->getProperty("shop_url");
    if($path){
      $page_id = PageIdByPath($path, $this->site_id);
      option("shop\\catalog_page_id_".$this->lang, $page_id, "Kataloga(".$this->lang.") page id", Array("is_advanced" => true), $page_id, $this->site_id);
    }

    $this->FilterGroups = $this->initPropertyCollection('FilterGroups');
    $this->Filters = $this->initPropertyCollection('Filters');
    $this->ProdFilterRel = $this->initPropertyCollection('ProdFilterRel');
    $this->CatFilterGroupRel = $this->initPropertyCollection('CatFilterGroupRel');
    $this->colors = $this->initPropertyCollection('colors');
    $this->product_colors = $this->initPropertyCollection('product_colors');
    $this->sizes = $this->initPropertyCollection('sizes');
    $this->product_sizes = $this->initPropertyCollection('product_sizes');

    $this->CustomFields = $this->initPropertyCollection('CustomFields');
    $this->CustomFieldGroups = $this->initPropertyCollection('CustomFieldGroups');
    $this->ProdCustomFields = $this->initPropertyCollection('ProdCustomFields');


    $this->ShopFilter = EVeikalsShopFilter::getInstance();
    if($this->ShopFilter){
      $c = $this->ShopFilter->init();
      if(!$c){
        die("Can't init ShopFilter in class.shop.php initCollections() method");
      }
    }

    $this->ProdLogger = EVeikalsProdLogging::getInstance();
    return $ok;
  }

  function clearRelatedCaches(){

     $this->Breadcrumbs->deleteAllCache();

  }

  function display_filters_html($filters,$active){

    foreach($filters as $filter)
    {
      $checked = (in_array($filter['item_id'],$active))? 'checked="checked"': '';
      $str .= "<input type='checkbox' name='FilterGroups[]' value='".$filter['item_id']."' ".$checked." />".($filter['sys_title'] ? $filter['sys_title'] : $filter['title_lv'])."<br />";
    }

    return $str;

  }

  function getCategoryCollection()
  {
    if ($this->categorycollection) return true;

    //init collection and return if success
    $colid = $this->getProperty("cat_collection");
    include_once($GLOBALS['cfgDirRoot'] . "collections/class.shopcatcollection.php");
    $colname = '';
    $col = new shopcatcollection($colname,$colid);
    if ($col->isValidCollection())
    {
      $this->categorycollection = $col;
      return true;
    }
    return false;
  }

  function getProductCollection()
  {
    if ($this->productcollection) return true;
    //init collection and return if success
    $colid = $this->getProperty("prod_collection");
    include_once($GLOBALS['cfgDirRoot'] . "collections/class.shopprodcollection.php");
    $colname = '';
    $col = new shopprodcollection($colname,$colid);
    if ($col->isValidCollection())
    {
      $this->productcollection = $col;
      return true;
    }
    return false;
  }

  //=========================================================================//
  // Default properties
  //=========================================================================//

  function toolbar(&$script, &$toolbars, &$btnids, &$onclick)
  {
    return true;
  }

  function CanLoadDesignDynamically()
  {
    return true;
  }

  function CanBeCompiled()
  {
    return False;
  }

  function SetProperties()
  {
    $this->properties = Array(

      "name"        => Array(
        "type"      => "string",
        "label"     => "Name:"
      ),

      "cat_collection" => Array(
        "label"       => "Category collection:",
        "type"        => "collection",
        "collectiontype" => "shopcatcollection",
        "lookup"      => GetCollections($this->site_id, "shopcatcollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "prod_collection" => Array(
        "label"       => "Product collection:",
        "type"        => "collection",
        "collectiontype" => "shopprodcollection",
        "lookup"      => GetCollections($this->site_id, "shopprodcollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "parameters_collection" => Array(
        "label"       => "Parameters collection:",
        "type"        => "collection",
        "collectiontype" => "shopparameterscollection",
        "lookup"      => GetCollections($this->site_id, "shopparameterscollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "user_collection" => Array(
        "label"       => "User collection:",
        "type"        => "collection",
        "collectiontype" => "shopusercollection",
        "lookup"      => GetCollections($this->site_id, "shopusercollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "units_collection" => Array(
        "label"       => "Units collection:",
        "type"        => "collection",
        "collectiontype" => "shopunitscollection",
        "lookup"      => GetCollections($this->site_id, "shopunitscollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "customimages_collection" => Array(
        "label"       => "Product images collection:",
        "type"        => "collection",
        "collectiontype" => "shopcustomimagescollection",
        "lookup"      => GetCollections($this->site_id, "shopcustomimagescollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "relatedprodscol" => Array(
        "label"       => "Related products collection:",
        "type"        => "collection",
        "collectiontype" => "shoprelatedprodscollection",
        "lookup"      => GetCollections($this->site_id, "shoprelatedprodscollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "brandscol" => Array(
        "label"       => "Brendi:",
        "type"        => "collection",
        "collectiontype" => "shopbrandscollection",
        "lookup"      => GetCollections($this->site_id, "shopbrandscollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "pricescol" => Array(
        "label"       => "Prices collection:",
        "type"        => "collection",
        "collectiontype" => "shoppricescollection",
        "lookup"      => GetCollections($this->site_id, "shoppricescollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "FilterGroups" => Array(
        "label"       => "Filter group collection:",
        "type"        => "collection",
        "collectiontype" => "EVeikalsFilterGroups",
        "lookup"      => GetCollections($this->site_id, "EVeikalsFilterGroups"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "Filters" => Array(
        "label"       => "Filters collection:",
        "type"        => "collection",
        "collectiontype" => "EVeikalsFilters",
        "lookup"      => GetCollections($this->site_id, "EVeikalsFilters"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "CustomFieldGroups" => Array(
        "label"       => "Custom field groups collection:",
        "type"        => "collection",
        "collectiontype" => "EVeikalsCustomFieldGroupCollection",
        "lookup"      => GetCollections($this->site_id, "EVeikalsCustomFieldGroupCollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

			"CustomFields" => Array(
        "label"       => "Custom field collection:",
        "type"        => "collection",
        "collectiontype" => "EVeikalsCustomFieldCollection",
        "lookup"      => GetCollections($this->site_id, "EVeikalsCustomFieldCollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "ProdCustomFields" => Array(
        "label"       => "Prod custom field collection:",
        "type"        => "collection",
        "collectiontype" => "EVeikalsCustomFieldProdValueCollection",
        "lookup"      => GetCollections($this->site_id, "EVeikalsCustomFieldProdValueCollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "ProdFilterRel" => Array(
        "label"       => "Prod. filter rel. collection:",
        "type"        => "collection",
        "collectiontype" => "EVeikalsProdFilterRel",
        "lookup"      => GetCollections($this->site_id, "EVeikalsProdFilterRel"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "CatFilterGroupRel" => Array(
        "label"       => "Cat. filter group rel. collection:",
        "type"        => "collection",
        "collectiontype" => "EVeikalsCatFilterGroupRel",
        "lookup"      => GetCollections($this->site_id, "EVeikalsCatFilterGroupRel"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "colors" => Array(
        "label"       => "Color collection:",
        "type"        => "collection",
        "collectiontype" => "EVeikalsColorCollection",
        "lookup"      => GetCollections($this->site_id, "EVeikalsColorCollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "product_colors" => Array(
        "label"       => "Product color collection:",
        "type"        => "collection",
        "collectiontype" => "EVeikalsProductColorCollection",
        "lookup"      => GetCollections($this->site_id, "EVeikalsProductColorCollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "sizes" => Array(
        "label"       => "Size collection:",
        "type"        => "collection",
        "collectiontype" => "EVeikalsSizeCollection",
        "lookup"      => GetCollections($this->site_id, "EVeikalsSizeCollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "product_sizes" => Array(
        "label"       => "Product size collection:",
        "type"        => "collection",
        "collectiontype" => "EVeikalsProductSizeCollection",
        "lookup"      => GetCollections($this->site_id, "EVeikalsProductSizeCollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      'shop_url' => Array(
        'label'     => 'Kataloga lapa:',
        'type'      => 'dialog',
        'dialog'    => '?module=dialogs&action=links&site_id='.$this->site_id,
      ),

    );
  }

}


?>