<?php
/**
*
*  Title: Order viewer
*  Author: Kristaps Taube <ktaube@datateks.lv>
*  Date: 01.03.2016
*  Project: EVeikals
*
*/

include_once("class.component.php");
require_once($GLOBALS['cfgDirRoot']."library/mPDF/mpdf.php");

class EVeikalsOrderViewer extends component{

    function __construct($name){
        parent::__construct($name);
    }

    function execute(){

        if($_GET['action'] == 'add'){
            $this->add((int)$_GET['id']);
            return;
        }

        if($_GET['action'] == 'download'){
            $this->download((int)$_GET['id']);
            return;
        }

    }

  function download($id){

  	$order = $this->orders->GetRow(array("where" => "item_id = :id", "params" => array(":id" => $id)));

    if($order && $order['user_id'] == $this->shopmanager->user['item_id']){

     	$mpdf = new mPDF();
      $mpdf->WriteHTML($order['rekins']."!!");
      $mpdf->Output($this->l("INET_").$order['item_id'].".pdf", "D");

		}

  }

	function add($id){

  	$order = $this->orders->GetRow(array("where" => "item_id = :id", "params" => array(":id" => $id)));

    if($order && $order['user_id'] == $this->shopmanager->user['item_id']){

    	$order_data = unserialize($order['order_data']);
			$prods = $order_data['products'];

      $_SESSION['cartitems'] = array(); // clear current basket
			foreach($prods as $prod){

      	if(!$prod['giftcard']){
          $item = array("id" => $prod['id']);
					$item['price_id'] = $prod['price_id'];

	      	$this->shopmanager->smallcart->addCartItem($item, $prod['count']);
      	}else{

        	$this->shopmanager->addGiftCard($prod);

      	}

			}

			header("location: ?action=view&id=".$order['item_id']."&added=1");
			die();

    }

	}

  function output($user){

    $i = 0;

    $orders = $this->orders->getDataForUser($user['item_id']);
    ?>
    <div class='padding'>
        <table id='OrderViewer'>
            <tr>
                <th>#</th>
                <th><?=$this->l("Numurs")?></th>
                <th><?=$this->l("Datums")?></th>
                <th><?=$this->l("Apmaksāts")?></th>
                <th><?=$this->l("Summa")?></th>
                <th><?=$this->l("Darbība")?></th>
            </tr>
            <? foreach($orders as $order){ ?>
            <tr>
                <td class='ind'><?=(++$i)?>.</td>
                <td><?=$this->l("I-NET.").$order['item_id']?></td>
                <td><?=date("d.m.Y", strtotime($order['datums']))?></td>
                <td class='center'><?=($order['apmaksats'])? $this->l("Jā") : $this->l("Nē") ?></td>
                <td class='sum'><?=number_format($order['kopeja_summa'], 2) ?> <?=$this->CurrencyMenu->default['label']?></td>
                <td class='actions'>
                <a href='?action=view&id=<?=$order['item_id']?>' class='button'><?=$this->l("Skatīt")?></a>
                <a href='?action=download&id=<?=$order['item_id']?>' class='button'><?=$this->l("Lejupielādēt")?></a>
                </td>
            </tr>
            <? } ?>
        </table>
    </div>
    <?

  }

    function addProperties(){

        return [
            "orders" => Array(
                "label"       => "Order collection:",
                "type"        => "collection",
                "collectiontype" => "EVeikalsOrderCollection",
            ),

            "orderprods" => Array(
                "label"       => "Order prod collection:",
                "type"        => "collection",
                "collectiontype" => "shoporderprodcollection",
            ),
        ];

    }

}
