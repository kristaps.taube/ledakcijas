<?php
/**
*
*  Title: Currency menu
*  Author: Kristaps Taube <ktaube@datateks.lv>
*  Date: 16.01.2014
*  Project: E-Veikals
*
*/

include_once("class.component.php");

class EVeikalsCurrencyMenu extends component{

    private $inited = false;

    function __construct($name = false){

        parent::__construct($name);
        $this->SetProperties();

        $this->registerAsService("CurrencyMenu");
        $this->registerAsBackendService("CurrencyMenuBackend");

    }

    function execute(){

        if(!$this->initCollections()){
            return;
        }

        if($_GET['set_currency']){

            foreach($this->getCurrencies() as $curr){

                if($curr['item_id'] == $_GET['set_currency']){
                    $_SESSION['currency'] = $curr;
                    break;
                }

            }

            header("location: ?");
            die();

        }

        $this->init();

        if(!$_SESSION['currency']){
            $_SESSION['currency'] = $this->default;
        }else{

            foreach($this->getCurrencies() as $curr){

                // grab actual data from DB
                if($curr['item_id'] == $_SESSION['currency']['item_id']){
                    $_SESSION['currency'] = $curr;
                    break;
                }

            }

        }

    }

    public function init()
    {

        if($this->inited){
            return;
        }

        $this->secondary = $this->getSecondary();
        $this->default = $this->getDefault();

        $this->inited = true;

	}

  function getSecondary(){

  	foreach($this->getCurrencies() as $curr){
  		if($curr['secondary']) return $curr;
  	}

	}

	function getDefault(){

  	foreach($this->getCurrencies() as $curr){
  		if($curr['default']) return $curr;
  	}

	}

  // used as a service method
  function getCurrencies(){

  	static $cache = false;

		if(!$cache) $cache = $this->currencies->getDBData(array("order" => "ind"));

		return $cache;

  }

  function afterGetService(){
    $this->execute();
  }

  function output(){

    if(!$this->initCollections()){
      echo $this->name.": Can't init collections.<br />";
      return;
    }

    $currencies = $this->getCurrencies();
    if(count($currencies) < 2) return;

    ?>
    <?=$this->l("Valūta")?>:
    <? foreach($currencies as $cur){ ?>
    <a href='?set_currency=<?=$cur['item_id']?>' class='<?=($_SESSION['currency']['item_id'] == $cur['item_id'])? "active" : ""?>' title='<?=hsc($cur['label'])?>'><?=$cur['label']?></a>
    <? } ?>
    <?

  }

  function SetProperties(){
    $this->properties = Array(

      "name"        => Array(
        "label"     => "Name:",
        "type"      => "str"
      ),

      "currencies" => Array(
        "label"       => "Valūtas:",
        "type"        => "collection",
        "collectiontype" => "EVeikalsCurrencyCollection",
        "lookup"      => GetCollections($this->site_id, "EVeikalsCurrencyCollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

    );
  }



}



?>