<?
//Serveris.LV Constructor component
//Created by Aivars Irmejs, 2006, aivars@serveris.lv

include_once("class.component.php");


class shopprocesslogin extends component
{

  var $parent;
  var $normalclass;
  var $normalstyle;
  var $normalbullet;
  var $selectedclass;
  var $selectedstyle;
  var $selectedbullet;
  var $orderreciever;

  //Class initialization
  function shopprocesslogin($name)
  {
    parent::__construct($name);
    $this->SetProperties();
  }

  //========================================================================//
  // Language strings
  //========================================================================//
  function InitLanguageStrings()
  {
    if ($this->site_id && $this->page_id)
    {
        $language_id = sqlQueryValue("SELECT language FROM " . $this->site_id . "_pages WHERE page_id=". $this->page_id);
    	  $lang = sqlQueryValue("SELECT shortname FROM ".$this->site_id."_languages WHERE language_id=$language_id");
    }
    $this->lang = $lang;
  }

  function getLanguageStrings()
  {
    $ls = Array(
      'xmleksports' => 'XML eksports',
      'pieslegties' => 'Pieslēgties',
      '_lietotajvards' => 'Lietotājvārds',
      '_parole' => 'Parole',
      'neesiregistrejies' => 'Reģistrēties',
      'nepareizaparole' => 'Nepareizs lietotāja vārds vai parole!',
      'piesledzieska' => 'Pieslēdzies kā',
      'beigtdarbu' => 'Beigt darbu',
      'cenasnoraditasaratlaidi' => 'Visas cenas norādītas ar Jums piešķirto atlaidi',
      'nomainitparoli' => 'Nomainīt paroli',
      'registrejies' => '%s Piereģistrējies %s, lai varētu iepirkties.',
      'aizmirsi' => 'Ja esi aizmirsis paroli, %s spied šeit %s!',
      'lietotajs' => 'Lietotājs'
    );
    return $ls;
  }

  function outputLogin($p)
  {
    extract($p);
?>
  <div id="loginbox">  sdf
  <? if(!$_SESSION['logged_in']) { ?>
    <form action="" method="post">
      <? if($form_err) { ?>
        <div class="err"><?=$form_err ?></div>
      <? } ?>
      <p><?=$this->LS('_lietotajvards') ?>:</p>
      <p><input type="text" name="username" value="" class="text"/></p>
      <p><?=$this->LS('_parole') ?>:</p>
      <p><input type="password" name="password" value="" class="text"/></p>
      <p><input type="submit" value="<?=$this->LS('pieslegties') ?>" value="" class="submit" name="submit_login"/></p>
      <div class="clear"></div>

      <p><?=$registrejies ?></p>
      <p><?=$aizmirsi ?></p>
    </form>
  <? } else { ?>
    <p>
      <?=$this->LS('piesledzieska') ?> <b><?=$_SESSION['logged_in']['username'] ?></b><br />
      <? if($_SESSION['logged_in']['discount']) { ?>
        <?=$this->LS('cenasnoraditasaratlaidi') ?> <b>-<?=$_SESSION['logged_in']['discount'] ?>%</b><br />
      <? } ?>
      <a href="?userlogout=1"><?=$this->LS('beigtdarbu') ?></a><br/>
      <a href="<?=$chpassurl ?>"><?=$this->LS('nomainitparoli') ?></a><br/>
      <? if ($_SESSION['logged_in']['xml_export_enabled']) { ?>
        <a href="/<?=$this->lang ?>/xml-export-info"><?=$this->LS('xmleksports') ?></a>
      <? } ?>
    </p>
  <? } ?>
  </div>
<?
  }

  function execute()
  {
    if ($GLOBALS['FreeShop'])
    {
      $this->use_reg = option('Free Shop\\use_reg', null, 'Use registration', array('type' => 'check'), 'on');
      if (!$this->use_reg) return;
    }

    $this->InitLanguageStrings();
    $this->initCollections();

    $this->form_err = '';
    if($_POST['submit_login'])
    {
      if($_POST['username'] && $_POST['password'])
      {
        $user = $this->usercollection->getUserPassId($_POST['username'], $_POST['password']);
        if($user)
        {
           $user = $this->usercollection->GetItemByIDAssoc($user);
           if($user['savedfields'])
           {
             $_SESSION['buyers_information'] = unserialize($user['savedfields']);
           }
           $_SESSION['logged_in'] = $user;
           header('Location: '.$_SERVER["REQUEST_URI"].'');
           die();

        }else
        {
          $this->form_err = ''.$this->LS('nepareizaparole').'';
          $GLOBALS['unsuccessfulloginattempt'] = true;
        }
      }
    }

  }

  function Output()
  {
    if ($GLOBALS['FreeShop'] && !$this->use_reg) return;

    if($_GET['userlogout'])
    {
      $_SESSION['logged_in'] = false;
      $_SESSION['buyers_information'] = false;
      header('Location: ?');
      die();
    }

    $reg_url = '/'.$this->lang.'/registration/';
    $getpass_url = '/'.$this->lang.'/getpass/';

    $tpl = Array(
      'reg_url' => $reg_url,
      'chpassurl' => '/'.$this->lang.'/changepass/',
      'getpass_url' => $getpass_url,
      'registrejies' => sprintf($this->LS('registrejies'), '<a href="'.$reg_url.'">', '</a>'),
      'aizmirsi' => sprintf($this->LS('aizmirsi'), '<a href="'.$getpass_url.'">', '</a>'),
      'form_err' => $this->form_err
    );
    $this->outputLogin($tpl);
  }

  function Design()
  {
    $this->Output();
  }

  //=========================================================================//
  // Collection initialization routines
  //=========================================================================//
  function initCollections()
  {
    $ok = true;
    $ok = $ok && $this->getUserCollection();
    $this->collections['users'] = $this->usercollection;
    return $ok;
  }

  function getUserCollection()
  {
    //init collection and return if success
    $colid = $this->getProperty("user_collection");
    include_once($GLOBALS['cfgDirRoot'] . "collections/class.shopusercollection.php");
    $colname = '';
    $this->usercollection = new shopusercollection($colname,$colid);
    return $this->usercollection->isValidCollection();
  }


  //=========================================================================//
  // Default properties
  //=========================================================================//

  function CanBeCompiled()
  {
    return False;
  }

  function SetProperties()
  {
    $this->properties = Array(

      "name"        => Array(
        "type"      => "string",
        "label"     => "Name:"
      ),

      "user_collection" => Array(
        "label"       => "User collection:",
        "type"        => "collection",
        "collectiontype" => "shopusercollection",
        "lookup"      => GetCollections($this->site_id, "shopusercollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      )


    );
  }

}




?>
