<?php
/**
*
*  Title: Login
*  Author: Kristaps Taube <ktaube@datateks.lv>
*  Date: 16.01.2014
*  Project: E-Veikals
*
*/

require_once("class.component.php");
require_once($GLOBALS['cfgDirRoot']."library/facebook/facebook.php");
require_once($GLOBALS['cfgDirRoot']."library/google/Google_Client.php");
require_once($GLOBALS['cfgDirRoot']."library/google/contrib/Google_PlusService.php");
require_once($GLOBALS['cfgDirRoot']."library/google/contrib/Google_Oauth2Service.php");
require_once($GLOBALS['cfgDirRoot']."library/draugiem/DraugiemApi.php");

use Constructor\Url;

class EVeikalsLogin extends component{

  public $login_error = false;
  private $options_inited = false;

  public function __construct($name)
  {

    parent::__construct($name);
    $this->initOptions();
    $this->login_error = $this->getError();

  }

  function execute(){

    $this->initOptions();

    if($this->use_facebook_login && $_GET['state'] && $_GET['code']){ // check Facebook login
      $this->processFacebookLogin();
    }

    if($this->use_google_login && isset($_GET['code']) && isset($_GET['prompt'])) { // check Google login
      $this->processGoogleLogin();
    }

    if($_GET['dr_auth_status'] == 'ok'){
      $this->processDraugiemLogin();
    }

    if($_GET['action'] == 'removeSoc'){
      $this->removeSoc($_GET['type']);
      header("location: ?");
      die();
    }

    // we will redirect user to this page after google login
    $_SESSION['LAST_URL'] = $GLOBALS['cur_url'];

  }

  function getLoggedInMsg(){
    $msg = $_SESSION['LoggedInMsg']? $_SESSION['LoggedInMsg'] : false;
    unset($_SESSION['LoggedInMsg']);
    return $msg;
  }

  function setLoggedInMsg($msg){
    $_SESSION['LoggedInMsg'] = $msg;
  }

  public function setError($error)
  {
      $_SESSION['login_error'] = $error;
  }

  public function getError()
  {
    $error = $_SESSION['login_error'];
    $_SESSION['login_error'] = '';
    return $error;
  }

  function getLoggedInError(){
    $error = $_SESSION['LoggedInError']? $_SESSION['LoggedInError'] : false;
    unset($_SESSION['LoggedInError']);
    return $error;
  }

  function setLoggedInError($error){
    $_SESSION['LoggedInError'] = $error;
  }

  function removeSoc($type){

    $types = array("facebook", "draugiem", "google");

    if(in_array($type, $types) && $_SESSION['logged_in']){

      $login_method_count = 0;

      $login_method_count += ($_SESSION['logged_in']['google_id'])? 1 : 0;
      $login_method_count += ($_SESSION['logged_in']['fb_id'])? 1 : 0;
      $login_method_count += ($_SESSION['logged_in']['draugiem_id'])? 1 : 0;
      $login_method_count += ($_SESSION['logged_in']['password'] && $_SESSION['logged_in']['email'])? 1 : 0;

      if($login_method_count >= 2){ // we will have at least one login method left

        if($type == 'facebook'){

          $update = array(
            "fb_id" => "",
            "fb_name" => ""
          );

        }elseif($type == 'draugiem'){

          $update = array(
            "draugiem_id" => "",
            "draugiem_name" => ""
          );

        }elseif($type == 'google'){

          $update = array(
            "google_id" => "",
            "google_name" => ""
          );

        }

        if($update){
          $_SESSION['logged_in'] = array_merge($_SESSION['logged_in'], $update);
          $this->users->changeItemByIDAssoc($_SESSION['logged_in']['item_id'], $update);
          $this->setLoggedInMsg($this->l("Sociālais tīkls noņemts"));
        }

      }else{ // Do not remove last login method

        $this->setLoggedInError($this->l("Nevar noņemt pēdējo autorizācijas iespēju."));

      }

    }

  }

    public function authorizeUserEntry($user)
    {

        $_SESSION['logged_in'] = $user;
        $smallcart = shopsmallcart::getInstance();


        if($user['savedfields']){
            $_SESSION['buyers_information'] = unserialize($user['savedfields']);
        }

        if(!empty($_SESSION['cartitems'])){ // we have some items in cart.. lets save 'em!

            $_SESSION['logged_in']['saved_cart_id'] = $smallcart->saveCart();

        }else{ // cart empty, maybe we have something saved?

            $cart = $this->carts->getByUser($user['item_id']);

            if($cart){
                $smallcart->loadCart(unserialize($cart['data']));
                $_SESSION['logged_in']['saved_cart_id'] = $cart['item_id'];
            }

        }

    }

  function processDraugiemLogin(){

    $session = $this->draugiem->getSession();

    if($session){

      $info = $this->draugiem->getUserData();//Get user info

      $user = $this->users->getUserByDraugiemId($info['uid']);

      if($_SESSION['logged_in']){ // already logged in? Trying to add soc. network

        if($user){ // social network ID exists. ERROR!

          $this->setLoggedInError($this->l("Šis sociālā tīkla konts ir jau pievienots sistēmai"));

        }else{ // social network ID not found. Add to this account!

          $update = array(
            "draugiem_id" => $info['uid'],
            "draugiem_name" => $info['name']." ".$info['surname']
          );

          $_SESSION['logged_in'] = array_merge($_SESSION['logged_in'], $update);
          $this->users->changeItemByIDAssoc($_SESSION['logged_in']['item_id'], $update);
          $this->setLoggedInMsg($this->l("Sociālais tīkls pievienots"));

          header('location: '.$_SERVER["REQUEST_URI"].'');
          die();

        }

      }else{ // trying to login

        if($user){ // existing user

          $this->authorizeUserEntry($user);
          header('location: '.$_SERVER["REQUEST_URI"].'');
          die();

        }else{ // ohh.. new user.. we need to ask for email

          $user = array(
            "draugiem_id" => $info['uid'],
            "name" => $info['name'],
            "surname" => $info['surname'],
            "active" => 1,
            "draugiem_name" => $info['name']." ".$info['surname'],
          );

          $_SESSION['dr_user_data'] = $user;
          $_SESSION['dr_reg_form'] = true;
          header("location: /".PagePathById($this->register_page_id));
          die();

        }

      }

    }

  }

  function processGoogleLogin(){

    $this->google->authenticate($_GET['code']);

    $_SESSION['google_access_token'] = $this->google->getAccessToken();

    $me = $this->google_pluss->people->get('me');

    if($me){

      $user = $this->users->getUserByGoogleId($me['id']);

      if($_SESSION['logged_in']){  // already logged in? Trying to add soc. network

        if($user){ // social network ID exists. ERROR!

          $this->setLoggedInError($this->l("Šis sociālā tīkla konts ir jau pievienots sistēmai"));

        }else{ // social network ID not found. Add to this account!

          $update = array(
            "google_id" => $me['id'],
            "google_name" => $me['name']['givenName']." ".$me['name']['familyName']
          );

          $_SESSION['logged_in'] = array_merge($_SESSION['logged_in'], $update);
          $this->users->changeItemByIDAssoc($_SESSION['logged_in']['item_id'], $update);
          $this->setLoggedInMsg($this->l("Sociālais tīkls pievienots"));

          header('location: '.$_SERVER["REQUEST_URI"].'');
          die();

        }

      }else{ // trying to login

        if($user){ // existing user

          $this->authorizeUserEntry($user);

        }else{ // new user

          $info = $this->googleoauth2Service->userinfo->get();

          $check = $this->users->getUserByEmail($info["email"]);

          if(!$check){

            $user = array(
              "google_id" => $me['id'],
              "email" => $info["email"],
              "active" => 1,
              "name" => $me['name']['givenName'],
              "google_name" => $me['name']['givenName']." ".$me['name']['familyName'],
              "surname" => $me['name']['familyName'] ,
            );

            $ind = $this->users->addItemAssoc($user);
            $id = $this->users->getItemId($ind);
            $user = $this->users->getItemByIDAssoc($id);

            $this->authorizeUserEntry($user);

          }else{

            $_SESSION['SOCIAL_REG_EMAIL_ERROR'] = true;
            header("location: /".PagePathById($this->register_page_id));
            die();

          }

        }

      }

    }

    header('location: '. $_SESSION['LAST_URL']);
    die();

  }

  function processFacebookLogin(){

    $fb_user = $this->facebook->getUser();

    if($fb_user){

      $user = $this->users->getUserByFBId($fb_user);

      if($_SESSION['logged_in']){ // already logged in? Trying to add soc. network

        if($user){ // social network ID exists. ERROR!

          $this->setLoggedInError($this->l("Šis sociālā tīkla konts ir jau pievienots sistēmai"));

        }else{ // social network ID not found. Add to this account!

          $user_profile = $this->facebook->api('/me');

          $update = array(
            "fb_id" => $fb_user,
            "fb_name" => $user_profile['name']." ".$user_profile['surname']
          );

          $_SESSION['logged_in'] = array_merge($_SESSION['logged_in'], $update);
          $this->users->changeItemByIDAssoc($_SESSION['logged_in']['item_id'], $update);
          $this->setLoggedInMsg($this->l("Sociālais tīkls pievienots"));

          header('location: '.$_SERVER["REQUEST_URI"].'');
          die();

        }

      }else{ // trying to login

        if($user){ // existing user

          $this->authorizeUserEntry($user);

          header('location: '.$_SERVER["REQUEST_URI"]);
          die();

        }else{ // new user add

          $user_profile = $this->facebook->api('/me');

          $check = $this->users->getUserByEmail($user_profile["email"]);

          if(!$check){
            $user = array(
              "fb_id" => $fb_user,
              "email" => $user_profile['email'],
              "name" => $user_profile['name'],
              "active" => 1,
              "fb_name" => $user_profile['name']." ".$user_profile['surname'],
              "surname" => $user_profile['surname'],
            );

            $ind = $this->users->addItemAssoc($user);
            $id = $this->users->getItemId($ind);
            $user = $this->users->getItemByIDAssoc($id);

            $this->authorizeUserEntry($user);

            header('location: '.$_SERVER["REQUEST_URI"].'');
            die();

          }else{

            $_SESSION['SOCIAL_REG_EMAIL_ERROR'] = true;
            header("location: /".PagePathById($this->register_page_id));
            die();

          }

        }

      }

    }

  }

    public function initOptions()
    {



        if($this->options_inited === true) return;

        $this->use_facebook_login = option('Login\\fb_use', null, 'Use Facebook login', Array("is_advanced" => true, 'type' => 'check'));

        if($this->use_facebook_login){
            // facebook conf.
            $this->facebook_conf = array();
            $this->facebook_conf['app_id'] = option('Login\\fb_app_id', null, 'Facebook App Id', array("is_advanced" => true), '');
            $this->facebook_conf['app_secret'] = option('Login\\fb_app_secret', null, 'Facebook App Secret', array("is_advanced" => true), '');

            $this->facebook = new Facebook(array(
                'appId'  => $this->facebook_conf['app_id'],
                'secret' => $this->facebook_conf['app_secret'],
                'cookie' => true
            ));

        }

        $this->use_google_login = option('Login\\g_use', null, 'Use Google login', Array("is_advanced" => true, 'type' => 'check'));

        if($this->use_google_login){

            // google conf.
            $this->google_conf = array();
            $this->google_conf['client_id'] = option('Login\\g_client_id', null, 'Google Client Id', array("is_advanced" => true), '');
            $this->google_conf['client_secret'] = option('Login\\g_client_secret', null, 'Google Client Secret', array("is_advanced" => true), '');
            $this->google_conf['redirect_uri'] = option('Login\\g_redirect_uri', null, 'Google Redirect URI', array("is_advanced" => true), '');

            $this->google = new Google_Client();
            $this->google->setClientId($this->google_conf['client_id']);
            $this->google->setClientSecret($this->google_conf['client_secret']);
            $this->google->setRedirectUri($this->google_conf['redirect_uri']);
            $this->google->setApprovalPrompt('auto');
            $this->google_pluss = new Google_PlusService($this->google);
            $this->googleoauth2Service = new Google_Oauth2Service($this->google);

            if(isset($_SESSION['google_access_token'])){
                $this->google->setAccessToken($_SESSION['google_access_token']);
            }

        }

        $this->use_draugiem_login = option('Login\\dr_use', null, 'Use Draugiem.lv login', Array("is_advanced" => true, 'type' => 'check'));

        if($this->use_draugiem_login){

            // draugiem.lv conf.
            $this->draugiem_conf = array();
            $this->draugiem_conf['app_id'] = option('Login\\dr_app_id', null, 'Draugiem.lv App Id', array("is_advanced" => true), '');
            $this->draugiem_conf['app_key'] = option('Login\\dr_app_key', null, 'Draugiem.lv API key', array("is_advanced" => true), '');

            $this->draugiem = new DraugiemApi($this->draugiem_conf['app_id'], $this->draugiem_conf['app_key']);

            $s = $this->draugiem->getSession();

        }

        $this->use_soc_logins = ($this->use_google_login || $this->use_facebook_login || $this->use_draugiem_login)? true : false;

        option('Shop\\OrderForm', '', 'Order form');
        $optcat = getOptionByPath('Shop\\OrderForm');
        $params = Array('parent_id' => $optcat['id']);
        $this->use_user_offers = option('use_user_offers', null, 'Lietot lietotāju piedāvājumus', array_merge($params, Array('type' => 'check','is_advanced' => true)), null);

        $this->options_inited = true;



    }

  function outputLoggedIn(){
	
    $error = $this->getLoggedInError();
    $msg = $this->getLoggedInMsg();

  ?>
  <div class='login_wrap logged_in'>
    <div class='fl'><?=str_replace("[name]", $_SESSION['logged_in']['name']." ".$_SESSION['logged_in']['surname'], $this->l("Sveicināts, [name]!"))?> </div>
  </div>
  <div class='box <?=($error || $msg)? "open" : "" ?>'>
    <?=($error)? "<div class='error'>".$error."</div>" : ""?>
    <?=($msg)? "<div class='msg'>".$msg."</div>" : ""?>
    <div class='sections'>
        <ul class='menu'>
            <li><a href='<?php echo Constructor\Url::get('profile/order-history')?>'><?=$this->l("Rēķinu vēsture")?></a></li>
            <li><a href='<?php echo Constructor\Url::get('profile/change-password')?>'><?=$this->l("Mainīt paroli")?></a></li>
            <?if($this->use_user_offers){?>
            <li><a href='/<?=PagePathById($this->user_offers_page_id)?>'><?=$this->l("Mani piedāvājumi")?></a></li>
            <li><a href='/<?=PagePathById($this->user_crendentialchange_page_id)?>'><?=$this->l("Mainīt rekivizītus")?></a></li>
            <? } ?>
        </ul>
        </div>
    <? if($this->use_soc_logins){ ?>
    <div class='soc'>
      <? if($this->use_facebook_login){ ?>
        <div>
        <? if($_SESSION['logged_in']['fb_id']){ ?>
          <img src='/images/html/login_facebook.png' alt='' /> <?=$_SESSION['logged_in']['fb_name']?> (<a href='?action=removeSoc&type=facebook'><?=$this->l("Noņemt")?></a>)
        <? }else{ ?>
          <a class='fb_login' href='<?=str_replace("&", "&amp;", $this->facebook->getLoginUrl(array("scope" => "email")))?>'><img src='/images/html/login_facebook.png' alt='' /> <?=$this->l("Pievienot")?></a>
        <? } ?>
        </div>
      <? } ?>
      <? if($this->use_google_login){ ?>
        <div>
        <? if($_SESSION['logged_in']['google_id']){ ?>
          <img src='/images/html/login_google.png' alt='' /> <?=$_SESSION['logged_in']['google_name']?> (<a href='?action=removeSoc&type=google'><?=$this->l("Noņemt")?></a>)
        <? }else{ ?>
          <a class='google_login' href='<?=str_replace("&", "&amp;", $this->google->createAuthUrl())?>'><img src='/images/html/login_google.png' alt='' /> <?=$this->l("Pievienot")?></a>
        <? } ?>
        </div>
      <? } ?>
      <? if($this->use_draugiem_login){ ?>
        <div>
        <? if($_SESSION['logged_in']['draugiem_id']){ ?>
          <img src='/images/html/login_dr.png' alt='' /> <?=$_SESSION['logged_in']['draugiem_name']?> (<a href='?action=removeSoc&type=draugiem'><?=$this->l("Noņemt")?></a>)
        <? }else{ ?>
          <a class='draugiem_login' href='<?=str_replace("&", "&amp;", $this->draugiem->getLoginURL($_SERVER['REQUEST_URI']))?>'><img src='/images/html/login_dr.png' alt='' /> <?=$this->l("Pievienot")?></a>
        <? } ?>
        </div>
      <? } ?>
    </div>
    <? } ?>
    <a href='<?php echo Constructor\Url::get('/user/logout')?>' class='logout'><?=$this->l("Atvienoties")?></a>
  </div>
  <?

  }

  public function output()
  {

    ?>
    <script>

    <? if($this->use_facebook_login){ ?>
        window.fbAsyncInit = function() {
            FB.init({
                appId      : <?=json_encode($this->facebook->getAppId())?>,
                xfbml      : true,
                version    : 'v2.5'
            });
        };

        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'))

    <? } ?>

      var logged_in = <?=json_encode($_SESSION['logged_in']? true : false)?>;
      var updating = <?=json_encode($this->l("Atjaunojam..."))?>;

    </script>
    <?

    if($this->getProperty("login_form")){
      $this->outputLoginForm();
      return;
    }

    if($_SESSION['logged_in']){
      $this->outputLoggedIn();
      return;
    }

    ?>

    <div class='login_wrap'>
      <div class='trigger_wrap'>
        <a href='#' class='trigger' title='<?=hsc($this->l("Ielogoties"))?>'><?=$this->l("Ielogoties")?></a>
      </div>
      <div class='form <?=($this->login_error)? "active has_error" : "" ?>'>
        <div class='top'></div>
        <div class='middle'>
        	<? if(($this->login_error)){ echo "<div class='error'><span>".$this->login_error."</span></div>";} ?>
	        <form action="<?php echo Constructor\SystemUrl::get('login')?>" method="post">
	          <label for="login-email"><?=$this->l("E-pasts")?>:</label>
	          <input type="text" name="email" id="login-email" value="" />
	          <label for="login-password"><?=$this->l("Parole")?>:</label>
	          <input type="password" name="password" id="login-password" value="" />
	          <a href='<?php echo Url::get('password_recovery')?>' class='recover' title='<?=hsc($this->l("Aizmirsi paroli?"))?>'><?=$this->l("Aizmirsi paroli?")?></a>
	          <input type='submit' name='login' value='<?=$this->l("Ieiet")?>' />
	        </form>
	        <div class='cb'></div>
	        <? if($this->use_soc_logins){ ?>
            <div class='sep'></div>
	        <div id="SocLoginWrap">
	          <div class='title'><?=$this->l("Ielogoties ar")?>: </div>
	          <div class='icons'>
	          <? if($this->use_facebook_login){ ?>
	          <a class='fb_login' href='<?=str_replace("&", "&amp;", $this->facebook->getLoginUrl(array("scope" => "email")))?>' title='<?=hsc("Facebook")?>'><img src='/images/html/login_facebook.png' alt='<?=hsc("Facebook")?>' /></a>
	          <? } ?>
	          <? if($this->use_google_login){ ?>
	          <a class='google_login' href='<?=str_replace("&", "&amp;", $this->google->createAuthUrl())?>' title='<?=hsc("Google")?>'><img src='/images/html/login_google.png' alt='<?=hsc("Google")?>' /></a>
	          <? } ?>
	          <? if($this->use_draugiem_login){ ?>
	          <a class='draugiem_login' href='<?=str_replace("&", "&amp;", $this->draugiem->getLoginURL($_SERVER['REQUEST_URI']))?>' title='<?=hsc("Draugiem.lv")?>'><img src='/images/html/login_dr.png' alt='<?=hsc("Draugiem.lv")?>' /></a>
	          <? } ?>
	          </div>
	          <div class='cb'></div>
	        </div>
        	<? } ?>
        </div>
        <div class='bottom'></div>
      </div>
    </div>
    <a class='register_link' href="<?php echo Url::get('registration')?>" title="<?=hsc($this->l("Reģistrēties"))?>"><?=$this->l("Reģistrēties")?></a>
    <?

  }

    function outputLoginForm(){

    if($_SESSION['logged_in']){ ?>
        <div class='padding'>
            <?=str_replace("[name]", $_SESSION['logged_in']['name'], $this->l("Jūs esat pievienojies kā [name]!"))?>
            <a href='?action=logout' class='logout' title='<?=hsc($this->l("Atvienoties"))?>'><?=$this->l("Atvienoties")?></a>
        </div>
        <?
        return;
    }

    ?>
    <?php if($this->login_error){ ?>
    <ul class='error'>
        <li>
        <?=$this->login_error?>
        </li>
    </ul>
    <?php } ?>
    <form action="<?php echo Url::get('login')?>" method="post" name="" class="loginform padding" id="LoginForm">
        <table>
            <tr>
                <td class='label'><?=$this->l('E-pasts') ?>asd:</td>
                <td><input type="text" name="email" class="form_box" value="" /></td>
            </tr>
            <tr>
                <td class='label'><?=$this->l('Parole') ?>:</td>
                <td><input type="password" name="password" class="form_box" value="" /></td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <a href='<?php echo Url::get('password_recovery')?>' class='recover' title='<?=hsc($this->l("Aizmirsi paroli?"))?>'><?=$this->l("Aizmirsi paroli?")?></a>
                    <input type="submit" name="login" value="<?=$this->l('Ieiet') ?>"/>
                </td>
            </tr>
        </table>
        <?php if($this->use_soc_logins){ ?>
        <div class='login_sep'></div>
        <table>
            <tr>
                <td class='label'><?=$this->l("Ielogoties ar")?>:</td>
                <td>
                    <? if($this->use_facebook_login){ ?>
                    <a class='fb_login' href='<?=urlencode($this->facebook->getLoginUrl(array("scope" => "email")))?>' title='<?=hsc("Facebook")?>'><img src='/images/html/login_facebook.png' alt='<?=hsc("Facebook")?>' /></a>
                    <? } ?>
                    <? if($this->use_google_login){ ?>
                    <a class='google_login' href='<?=urlencode($this->google->createAuthUrl())?>' title='<?=hsc("Google")?>'><img src='/images/html/login_google.png' alt='<?=hsc("Google")?>' /></a>
                    <? } ?>
                    <? if($this->use_draugiem_login){ ?>
                    <a class='draugiem_login' href='<?=urlencode($this->draugiem->getLoginURL($_SERVER['REQUEST_URI']))?>' title='<?=hsc("Draugiem.lv")?>'><img src='/images/html/login_dr.png' alt='<?=hsc("Draugiem.lv")?>' /></a>
                    <? } ?>
                </td>
            </tr>
        </table>
        <? } ?>
    </form>
    <?

    }

    protected function addProperties()
    {

        return  [
            "name"        => Array(
                "label"     => "Name:",
                "type"      => "str"
            ),

            "users" => Array(
                "label"       => "User collection:",
                "type"        => "collection",
                "collectiontype" => "shopusercollection",
            ),

            "carts" => Array(
                "label"       => "User saved carts:",
                "type"        => "collection",
                "collectiontype" => "EVeikalsSavedCarts",
            ),

            'login_url' => Array(
                'label'     => 'Links uz ielogošanos:',
                'type'      => 'dialog',
                'dialog'    => '?module=dialogs&action=links&site_id='.$this->site_id,
            ),

            'register_page_id' => Array(
                'label'     => 'Links uz reģistrāciju:',
                'type'      => 'page_id',
            ),

            'recover_page_id' => Array(
                'label'     => 'Links uz paroles atjaunošanu:',
                'type'      => 'page_id',
            ),

            'order_page_id' => Array(
                'label'     => 'Links uz rēķinu vēsturi:',
                'type'      => 'page_id',
            ),

            'psw_change_page_id' => Array(
                'label'     => 'Links uz paroles maiņu:',
                'type'      => 'page_id',
            ),

            'user_offers_page_id' => Array(
                'label'     => 'Links uz piedāvājumu lapu:',
                'type'      => 'page_id',
            ),

            'user_crendentialchange_page_id' => Array(
                'label'     => 'Links uz rekvizītu maiņu:',
                'type'      => 'page_id',
            ),

            'login_form' => Array(
                'label'     => 'Forma:',
                'type'      => 'check',
            ),
        ];

    }

}

