<?php
/**
*
*  Title: Moblie menu
*  Author: Kristaps Taube <ktaube@datateks.lv>
*  Date: 10.02.2014
*  Project: E-Veikals
*
*/

include_once("class.component.php");

class EVeikalsMobileMenu extends component{

  function __construct($name){
    parent::__construct($name);
    $this->SetProperties();
  }

  function getServices(){

    $this->manager = $this->getServiceByName('shopmanager');

    if(!$this->manager || $this->manager->mobile_session === false) return;

    $this->login = $this->getServiceByName("Login");
    $this->SocIcons = $this->getServiceByName("SocIcons");
    $this->CurrencyMenu = $this->getServiceByName("CurrencyMenu");
    if(!$this->login || !$this->SocIcons || !$this->CurrencyMenu) return false;

    return true;

  }

  function output(){

    if(!$this->getServices()) return;

    $this->shown_counts = array();
    $this->total_counts = array();

    $cats = $this->manager->catscol->getDBData(array("where" => "disabled = 0 AND lvl = 0", "order" => "ind", "assoc" => true));
    $total = count($cats);
    $i = 0;

    $currencies = $this->CurrencyMenu->getCurrencies();
    $cur_count = count($currencies);
    if($cur_count >= 2){
      $cur_width = round(1 / $cur_count * 100, 2);
      $cur_width = ($cur_width <= 50)? $cur_width : 49;
    }

    $last_width = 100 - ($cur_width * ($cur_count - 1));

    $languages = getLanguages();
    $lang_count = count($languages);
    if($lang_count >= 2){
      $lang_width = round(1 / $lang_count * 100, 2);
      $lang_width = ($lang_width <= 50)? $lang_width : 49;
    }

    $last_lang_width = 100 - ($lang_width * ($lang_count - 1));

    ?>
      <div id='MobileMenu'>
        <div class='inner_wrap' id='MIW'>
          <nav id="mobile-menu">
            <a href='#' class='closeall' title='<?=hsc($this->l("Aizvērt"))?>'><?=$this->l("Aizvērt")?></a>
            <? if($cur_count >= 2){ $i = 0; ?>
              <div class='Currencies'>
              <? foreach($currencies as $currency){ ?>
                <a title='<?=hsc($currency['label'])?>' href='?set_currency=<?=$currency['item_id']?>' style='width:<?=((++$i == $cur_count)? $last_width : $cur_width)?>%' class='<?=($_SESSION['currency']['item_id'] == $currency['item_id'])? "active" : ""?> <?=($i == 1)? "first" : ""?> <?=($i == $cur_count)? "last" : ""?>'>
                  <span><?=$currency['label']?></span>
                </a>
              <? } ?>
              <div class='cb'></div>
              </div>
            <? } ?>
            <? if($lang_count >= 2){ $i = 0; ?>
            <div class='Languages'>
            <? foreach($languages as $language){ ?>
              <a title='<?=hsc($language['shortname'])?>' href='/<?=urlencode($language['shortname'])?>' style='width:<?=((++$i == $lang_count)? $last_lang_width : $lang_width)?>%' class='<?=($language['shortname'] == $this->lang)? "active" : ""?> <?=($i == 1)? "first" : ""?> <?=($i == $lang_count)? "last" : ""?>'>
                <span><?=$language['shortname']?></span>
              </a>
            <? } ?>
            <div class='cb'></div>
            </div>
          <? } ?>
            <ul class="menu level0">
            <? foreach($cats as $cat){ ?>
              <?=$this->output_cat($cat, (++$i == 1), ($i == $total))?>
            <? } ?>
            </ul>
          </nav>
          <div class='buttons'>
          <? if($_SESSION['logged_in']){ ?>
            <div class='first'><span class='username'><?=$_SESSION['logged_in']['name']?></span></div>
            <div class='last'><a href='?action=logout' class='logout'><?=$this->l("Atvienoties")?></a> </div>
          <? }else{ ?>
            <div class='first'><a href='<?=$this->login->getProperty("login_url")?>'><?=$this->l("Ielogoties")?></a></div>
            <div class='last'><a href='<?=$this->login->getProperty("register_url")?>'><?=$this->l("Reģistrēties")?></a></div>
          <? } ?>
          </div>

          <div class='SocIcons'>
            <? foreach($this->SocIcons->icons->getMobileIcons() as $icon){ ?>
              <a href='<?=$icon['link']?>' target='_blank'><img src='<?=$icon['icon_mobile']?>' alt='' /></a>
            <? } ?>
          </div>
        </div>
      </div>
    <?

  }

  function output_cat($cat, $first = false, $last = false){

    $kids = $this->manager->catscol->getDBData(array("where" => "disabled = 0 AND parent = ".$cat['item_id'], "order" => "ind"));

    $this->shown_counts[$cat['lvl']] += 1;

    $total = count($kids);
    $i = 0;

    ?>
    <li class='<?=($first)? "first" : "" ?> <?=($last)? "last" : "" ?> <?=($kids)? "hasKids" : ""?>'>
      <a title='<?=hsc($cat['title_'.$this->lang])?>' class='valign' href="<?=$this->manager->getCategoryURL($cat['item_id'])?>"><?=($cat['lvl'] == 2)? " - " : ""?><?=$cat['title_'.$this->lang]?>  (<?=$this->manager->catscol->getCategoryProductCount($cat['item_id'], $this->manager->prodscol)?>)</a>
      <? if($kids){ ?>
      <div class='level<?=($cat['lvl']+1)?>'>
        <? if($cat['lvl'] == 0){ ?>
        <a href='#' class='close' title='<?=hsc($this->l("Aizvērt"))?>'><?=$this->l("Aizvērt")?></a>
        <? } ?>
        <ul class='menu'>
          <? foreach($kids as $kid){ ?>
          <?=$this->output_cat($kid, (++$i == 1), ($i == $total))?>
          <? } ?>
        </ul>
      </div>
      <? } ?>
    </li>
    <?


  }

  function SetProperties(){
    $this->properties = Array(

      "name"        => Array(
        "label"     => "Name:",
        "type"      => "str"
      ),

    );
  }



}
