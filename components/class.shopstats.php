<?

include_once("class.component.php");
require_once($GLOBALS['cfgDirRoot']."library/"."class.datagrid.php");

class shopstats extends component{

  function shopstats($name){
    parent::__construct($name);
    $this->SetProperties();
    $this->prod_cache = array();
  }

  function ajax(){
    if (empty($_GET['ajaxmethod'])) die('Method not defined!');

    $method = 'ajax_' . $_GET['ajaxmethod'];

    if (!method_exists($this, $method))
      die('Method not found!');

    ob_start();
    $this->$method();
    $html = ob_get_contents();
    ob_end_clean();

    echo parseVariables($html);
    die;

  }

  function ajax_getOrders(){
    if (!$this->init()) return;

    $orders = $this->shopman->orderscol->getData(array(
      'datums_from' => $_POST['from'],
      'datums_to' => $_POST['to']
    ));

?>
  <div class="response-orders">
    <? $this->outputOrders($orders, $_POST['from'], $_POST['to'], $_POST['interval']) ?>
  </div>
  <div class="response-stats">
    <? $this->outputProductStats($_POST['from'], $_POST['to']) ?>
  </div>
<?
  }

  function ajax_getSingleOrder()
  {
    if (!$this->init()) return;

    $order = $this->shopman->orderscol->getItemByIdAssoc(intval($_GET['order_id']));
    if (!$order) return;

?>
  <tr id="openorder-<?=$order['item_id'] ?>">
    <td colspan="20">
      <div style="border: 1px solid black;padding:3px">
        <?=$order['rekins'] ?>
      </div>
    </td>
  </tr>
<?
  }

  function init()
  {
    $this->shopman = $this->getBackendServiceByName('shopmanager');
    if (!$this->shopman) { echo 'shopmanager service not found!'; return false; }

    if (!$this->shopman->initCollections()) return false;

    if (!$this->shopman->orderscol || !$this->shopman->orderprodscol) return false;

    return true;
  }

  function design()
  {
    if (!$this->init()) return;

    $jsparams = array(
      'ajax_url' => $this->getComponentModifierLink('ajax')
    );

    $date_from = date('d.m.Y');
    $date_to = date('d.m.Y');

    $orders = $this->shopman->orderscol->getData(array(
      'datums_from' => $date_from,
      'datums_to' => $date_to
    ));

?>
  <!-- jquery ui -->
  <link rel="stylesheet" type="text/css" href="/scr/components/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css" />
  <script type="text/javascript" src="/scr/components/jquery-ui/js/jquery-ui-1.10.4.custom.js"></script>

  <!-- jq-plot -->
  <link rel="stylesheet" type="text/css" href="/scr/components/jq-plot/jquery.jqplot.css" />
  <script type="text/javascript" src="/scr/components/jq-plot/jquery.jqplot.js"></script>

  <!-- jq-plot plugins -->
  <script type="text/javascript" src="/scr/components/jq-plot/plugins/jqplot.highlighter.js"></script>
  <script type="text/javascript" src="/scr/components/jq-plot/plugins/jqplot.enhancedLegendRenderer.min.js"></script>

  <!-- date.js -->
  <script type="text/javascript" src="/scr/components/date.js"></script>

  <!-- shop stats -->
  <link rel="stylesheet" type="text/css" href="/scr/components/shop/stats.css" />
  <script type="text/javascript" src="/scr/components/shop/new.stats.js"></script>

<div id="ShopStats">

  <div class="filter"><form action="" method="post">
    <div class="shortcuts">
      <a href="#" class="today"><?=lcms('today')?></a> |
      <a href="#" class="yesterday"><?=lcms('yesterday')?></a> |
      <a href="#" class="this_week"><?=lcms('this week')?></a> |
      <a href="#" class="prev_week"><?=lcms('previous month')?></a> |
      <a href="#" class="this_month"><?=lcms('this month')?></a> |
      <a href="#" class="prev_month"><?=lcms('previous month')?></a>
    </div>
    <?=lcms('Date from')?>: <input type="text" class="date from" name="from" value="<?=$date_from ?>"/>
    <?=lcms('to')?>: <input type="text" class="date to" name="to" value="<?=$date_to ?>"/>
    <?=lcms('show interval')?>:
    <select name="interval">
      <option value="days"><?=lcms('days')?></option>
      <option value="weeks"><?=lcms('weeks')?></option>
      <option value="months"><?=lcms('months')?></option>
    </select>
    <button class="submit"><?=lcms('Filter')?></button>
    <span class="ajaxload" style="display:none"><img src="<?=$GLOBALS['cfgWebRoot'] ?>gui/ajaxload.gif" alt=""/></span>
  </form></div>

  <div class="tabs">
  	<ul>
  		<li><a href="#tabs-1" class="tab-link-1"><?=lcms('Orders')?></a></li>
  		<li><a href="#tabs-2" class="tab-link-2"><?=lcms('Bestsellers')?></a></li>
  	</ul>
  	<div id="tabs-1">
      <div class="info-empty" style="display:<?=$orders ? 'none' : '' ?>"><?=$this->l('Nothing found')?></div>
      <div class="info" style="display:<?=$orders ? '' : 'none' ?>">
        <div><?=lcms('Invoice count')?>: <strong class="orders_count"><?=$orders ? count($orders) : 0 ?></strong> (<?=lcms('registered')?>: <strong class="reg_count">0</strong>, <?=lcms('not registered')?>: <strong class="unreg_count">0</strong>)</div>
        <div><?=lcms('Total sum')?>: <strong class="sum"><?=number_format($sum, 2) ?></strong></div>
      </div>

      <div class="chart-wrap" style="display:block">
        <div id="stats-chart1"></div>
      </div>

      <div class="chart-wrap2" style="display:block">
        <div id="stats-chart2"></div>
      </div>

      <p><a href="#" class="details"><?=lcms('Details')?> &raquo;</a></p>
      <div class="orders" style="display:none"><? $this->outputOrders($orders, $date_from, $date_to, 'days') ?></div>
  	</div>
  	<div id="tabs-2" class="prodstats">
      <form action="" method="post" id="prodstats_form">
        <table>
          <tr>
            <td style="width:100px"><?=lcms('Show in chart')?>:</td>
            <td>
              <label><input type="radio" name="show_products" checked="checked" value="popular"/> <?=lcms('product which bought more than')?> &nbsp; <input type="text" name="min_times" value="2" class="num"/> &nbsp; <?=lcms('times')?></label><br/>
              <label><input type="radio" name="show_products" value="most_popular"/> <input type="text" name="num_popular" class="num" value="10"/> <?=lcms('most popular products')?></label>
            </td>
          </tr>
          <tr>
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="2">
              <div class="chart-wrap3" style="display:block">
                <div class="title"></div>
                <div id="stats-chart3"></div>
              </div>
            </td>
          </tr>
          <tr>
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr>
            <td><?=lcms('Details')?></td>
            <td>
            <? /*
              <label><input type="radio" name="show_detail" value="chart" checked="checked"/> rādīt to, kas grafikā</label>
              <label><input type="radio" name="show_detail" value="all"/> rādīt visu</label>
              */
            ?>
            </td>
          </tr>
          <tr>
            <td colspan="2">&nbsp;</td>
          </tr>
        </table>
      </form>

      <div class="stats_wrap">
        <? $this->outputProductStats($date_from, $date_to) ?>
      </div>

  	</div>
  </div>

</div>
<script>
  init_stats(<?=json_encode($jsparams)?>);
  //var s = new ShopStats(<?=json_encode($jsparams)?>);
</script>
<?
  }

  function getProd($id)
  {
    if (isset($this->prod_cache[$id]))
      return $this->prod_cache[$id];

    $prod = $this->shopman->prodscol->getItemByIdAssoc($id);
    if (!$prod) return null;

    $this->prod_cache[$id] = $prod;

    return $prod;
  }

  function ajax_outputProductStats()
  {
    if (!$this->init()) return;

    $this->outputProductStats();
  }

  function outputProductStats($from, $to){
    $params = array();

    if ($_POST['show_detail'] == 'chart' && $_POST['show_products'] == 'most_popular')
    {
      $limit = intval($_POST['num_popular']);
      if ($limit < 1)
        $limit = 1;
      else if ($limit > 20)
        $limit = 20;

      $params['prod_limit'] = $limit;
    }

    if ($_POST['show_products'] == 'most_popular'){
      $params['type'] = 'most_popular';
    }else{
      $params['min_times'] = $_POST['min_times'];
    }

    $prodstats = $this->shopman->orderscol->getProductStats($this->shopman->orderprodscol, $this->shopman->prodscol, $from, $to, $params);

    $data = array();
    $table = new DataGrid();
    $table->cols = Array(
      'datums' => Array('title' => lcms('Date / Products')),
    );

    $datumi = array();
    foreach ($prodstats as $row){
      $dat = $row['date'];
      if (!isset($datumi[$dat])) $datumi[$dat] = array();

      $datumi[$dat][] = $row;
    }

    $totals = array();

    $setprods = array();
    foreach ($datumi as $dat => $prods)
    {
      $row = array(
        'datums' => date('d.m.Y', strtotime($dat))
      );

      foreach ($prods as $p)
      {
        $row['count'.$p['prod_id']] = $p['count'];


        if (!$setprods[$p['prod_id']])
        {
          $prod = $this->getProd($p['prod_id']);
          $html = '<div style="margin-left:4px;vertical-align:middle;display:inline-block;width:15px;height:15px;background:'.$this->getProductColor($p['prod_id']).'"></div>';
          $table->cols['count'.$p['prod_id']] = array('title' => $prod['name_lv'].$html);

          $setprods[$p['prod_id']] = true;

        }

        if (!isset($totals[$p['prod_id']])) $totals[$p['prod_id']] = 0;

        $totals[$p['prod_id']] += intval($p['count']);
      }


      $data[] = $row;
    }


    $total = array(
      'datums' => '<strong>'.lcms('Total').':</strong>',
    );
    foreach ($totals as $prod_id => $count)
    {
      $total['count'.$prod_id] = '<strong>'.$count.'</strong>';
    }


    $data[] = $total;

    $table->data = $data;
    $table->footer = false;

?>
  <div class="chart-data3" style="display:none">
    <? $chart_debug3 = $this->outputStatsChartData3($prodstats, $from, $to) ?>
  </div>


  <div class='chart-debug3'>
    <div style='background: lightgrey; padding: 10px; display: none'>
      <pre>
      <?
        #print_r($prodstats);
      ?>
      <br /><br /><br />

      <?
       print_r($chart_debug3)
      ?>
      </pre>
    </div>
  </div>
  <div class='chart3_details'>
  <?=$table->output() ?>
  </div>

  <?

  }

  function outputOrders($orders, $date_from, $date_to, $interval)
  {
    $table = new DataGrid();
    $table->cols = Array(
      'datums' => Array('title' => lcms('Date')),
      'prod_count' => Array('title' => lcms('Product count')),
      'pasutitajs' => Array('title' => lcms('Customer')),
      'kopeja_summa' => Array('title' => lcms('Total sum')),
      'rekins' => Array('title' => ''),
    );

    $reg_count = 0;
    $unreg_count = 0;
    $sum = 0;
    $count_sum = 0;
    foreach ($orders as $key => $row)
    {
      $data = unserialize($row['order_data']);

      $orders[$key]['datums'] = date('d.m.Y, H:i', strtotime($row['datums']));
      $orders[$key]['prod_count'] = count($data['products']);
      $orders[$key]['rekins'] = '<a href="#" class="show_order order-'.$row['item_id'].'">Rēķins</a>';

      if ($row['user_id'])
        $reg_count += 1;
      else
        $unreg_count += 1;

      $sum += $row['kopeja_summa'];

      $count_sum += count($data['products']);
    }


    $orders[] = array(
      'datums' => '<strong>'.lcms('Total').':</strong>',
      'prod_count' => '<strong>'.$count_sum.'</strong>',
      'pasutitajs' => '',
      'kopeja_summa' => '<strong>'.number_format($sum, 2, '.', ' ').'</strong>',
      'rekins' => ''
    );

    $table->data = $orders;
    $table->footer = false;

?>
  <input type="hidden" class="reg_count" value="<?=$reg_count ?>"/>
  <input type="hidden" class="unreg_count" value="<?=$unreg_count ?>"/>
  <input type="hidden" class="sum" value="<?=number_format($sum, 2) ?>"/>
  <div class="chart-data" style='display: none'>
    <? $chart_debug = $this->outputChartData($orders, $date_from, $date_to, $interval) ?>
  </div>
  <div class="chart-data2" style='display: none'>
    <? $chart_debug2 = $this->outputChartData2($orders, $date_from, $date_to, $interval) ?>
  </div>

  <input type="hidden" class="data_length" value="<?=$chart_debug['data_length'] ?>"/>

  <?=$table->output() ?>
<?

  }

  function outputChartData($orders, $date_from, $date_to, $interval){


    $date_from = strtotime($date_from);
    $date_to = strtotime($date_to);

    $debug = array();

    $orders_by_date = array();
    foreach ($orders as $row)
    {
      $t = strtotime($row['datums']);
      $date = date('d.m.Y', $t);

      if (!isset($orders_by_date[$date])) $orders_by_date[$date] = array();

      $orders_by_date[$date][] = $row;
    }

    $min_date = null;
    $max_date = null;
    $y_max = 0;
    foreach ($orders_by_date as $date => $os)
    {
      if (count($os) > $y_max)
        $y_max = count($os);

      $t = strtotime($date);
      if (!$min_date || $t < $min_date)
        $min_date = $t;

      if (!$max_date || $t > $max_date)
        $max_date = $t;

    }

    $data = array();
    $d = $date_from;
    $orders_days = $this->countDays($min_date, $max_date);
    $days = $this->countDays($date_from, $date_to);
    $day_labels = array();

    if ($interval == 'days')
      $label_steps = 3;
    else
      $label_steps = 1;

    $i = 0;
    while ($i <= $days)
    {
      if ($interval == 'months')
      {
        $last_md = $this->lastDayOfMonth(date('m', $d), date('Y', $d));
        $i2 = min($i + $last_md, $days);

        $day_labels[] = date('m.Y.', $d);

        $ocount = 0;
        for ($j=$i; $j<=$i2; $j++)
        {
          $dkey = date('d.m.Y', $d);
          $ocount += isset($orders_by_date[$dkey]) ? count($orders_by_date[$dkey]) : 0;
          $d += 86400;
          $i++;
        }

        if ($ocount > $y_max)
          $y_max = $ocount;

        $data[] = $ocount;
      }
      else if ($interval == 'weeks')
      {
        $i2 = min($i + 6, $days);

        $wd = date('w', $d);
        $eow = 7 - $wd;
        $i2 = min($i2, $i + $eow);

        $day_labels[] = date('d.m.', $d).' - '.date('d.m.', $d+($i2 - $i) * 86400);

        $ocount = 0;
        for ($j=$i; $j<=$i2; $j++)
        {
          $dkey = date('d.m.Y', $d);
          $ocount += isset($orders_by_date[$dkey]) ? count($orders_by_date[$dkey]) : 0;
          $d += 86400;
          $i++;
        }

        if ($ocount > $y_max)
          $y_max = $ocount;

        $data[] = $ocount;
      }
      else
      {
        $day_labels[] = date('d.m.', $d);

        $dkey = date('d.m.Y', $d);
        $data[] = isset($orders_by_date[$dkey]) ? count($orders_by_date[$dkey]) : 0;

        $d += 86400;
        $i++;
      }
    }

    $output = array();

    $i = 0;
    foreach($data as $day => $sum){
      $output["data"][] = array($day+1, $sum);
      $output['options']['x_ticks'][] = array($day+1, $day_labels[$day]);
      $i++;
    }

    $output['options']['y_max'] = round(max($data) + 1); // +1
    for($i = 0; $i <= $output['options']['y_max']; $i++){
      $output['options']['y_ticks'][] = $i;
    }
    $output['options']['y_min'] = min($data);
    $output['options']['x_min'] = 0;
    $output['options']['x_max'] = count($data);
		$output['options']['title'] = lcms('The number of orders by date');
    echo json_encode($output);


    $debug['days'] = $orders_days;
    $debug['data_length'] = count($data);
    $debug['day_labels'] = $day_labels;
    $debug['data'] = $data;

    return $debug;

  }

  function lastDayOfMonth($m, $y){
    $nm = $m + 1;
    if ($nm > 12) $nm = 1;

    return date('d', mktime(0,0,0, $nm, 1, $y) - 86400);
  }

  function outputChartData2($orders, $date_from, $date_to, $interval){

    $date_from = strtotime($date_from);
    $date_to = strtotime($date_to);

    $debug = array();

    $orders_by_date = array();
    foreach ($orders as $row){
      $t = strtotime($row['datums']);
      $date = date('d.m.Y', $t);

      if (!isset($orders_by_date[$date])) $orders_by_date[$date] = array();

      $orders_by_date[$date][] = $row;
    }

    $min_date = null;
    $max_date = null;
    $y_max = 0;
    foreach ($orders_by_date as $date => $os){
      $t = strtotime($date);
      if (!$min_date || $t < $min_date)
        $min_date = $t;

      if (!$max_date || $t > $max_date)
        $max_date = $t;

    }

    if ($interval == 'days'){
      $label_steps = 3;
    }else{
      $label_steps = 1;
    }

    $data = array();
    $d = $date_from;
    $orders_days = $this->countDays($min_date, $max_date);
    $days = $this->countDays($date_from, $date_to);
    $day_labels = array();


    $i = 0;
    while ($i <= $days){

      if ($interval == 'months'){
        $last_md = $this->lastDayOfMonth(date('m', $d), date('Y', $d));
        $i2 = min($i + $last_md, $days);

        $day_labels[] = date('m.Y.', $d);

        $sum = 0;
        for ($j=$i; $j<=$i2; $j++){

          $dkey = date('d.m.Y', $d);
          if (isset($orders_by_date[$dkey])){
            foreach ($orders_by_date[$dkey] as $os){
              $sum += $os['kopeja_summa'];
            }
          }
          $d += 86400;
          $i++;
        }

        if ($sum > $y_max)
          $y_max = $sum;

        $data[] = $sum;
      }
      else if ($interval == 'weeks')
      {
        $i2 = min($i + 6, $days);

        $wd = date('w', $d);
        $eow = 7 - $wd;
        $i2 = min($i2, $i + $eow);

        $day_labels[] = date('d.m.', $d).' - '.date('d.m.', $d+($i2 - $i) * 86400);

        $sum = 0;
        for ($j=$i; $j<=$i2; $j++)
        {
          $dkey = date('d.m.Y', $d);
          if (isset($orders_by_date[$dkey]))
          {
            foreach ($orders_by_date[$dkey] as $os)
            {
              $sum += $os['kopeja_summa'];
            }
          }

          $d += 86400;
          $i++;
        }

        if ($sum > $y_max)
          $y_max = $sum;

        $data[] = $sum;
      }
      else
      {
        $day_labels[] = date('d.m.', $d);

        $dkey = date('d.m.Y', $d);
        if (isset($orders_by_date[$dkey]))
        {
          $sum = 0;
          foreach ($orders_by_date[$dkey] as $os)
          {
            $sum += $os['kopeja_summa'];
          }
          $data[] = $sum;

          if ($sum > $y_max)
            $y_max = $sum;

        }
        else
          $data[] = 0;

        $d += 86400;
        $i++;
      }
    }

    $debug['orders_by_date'] = $orders_by_date;

    $output = array();

    $i = 0;
    foreach($data as $day => $sum){
      $output["data"][] = array($day+1, $sum);
      $output['options']['x_ticks'][] = array($day+1, $day_labels[$day]);
      $i++;
    }

    $output['options']['y_max'] = round(max($data) * 1.1); // +10%
    $output['options']['y_min'] = min($data);
    $output['options']['x_min'] = 0;
    $output['options']['x_max'] = count($data);		
		$output['options']['title'] = lcms('Invoice totals by date');
    echo json_encode($output);

    return $debug;
  }

  function outputStatsChartData3($prodstats, $date_from, $date_to){

    $from = strtotime($date_from);
    $to = strtotime($date_to);
    #$to = ($to < time())? $to : time();
    $interval = $_POST['interval'];

    $debug = array();

    $current = $from;

    if($interval == 'days'){
      $int = ' + 1 day';
    }elseif($interval == 'weeks'){
      $int = ' + 1 week';
    }else{ // months
      $int = ' + 1 month';
    }


    $dates = array(date("Y-m-d", $current));
    while($current < $to){
      $current = strtotime(date('Y-m-d', strtotime(date("Y-m-d", $current) . $int)));
      $dates[] = date("Y-m-d", $current);
    }

    // reorder prod data
    $tmp = array();
    foreach($prodstats as $entry){
      $tmp[$entry['prod_id']][strtotime($entry['date'])] = $entry;
    }
    foreach($tmp as &$t){
      ksort($t);
      $t = array_values($t);
    }
    $prodstats = $tmp;

    $y_max = 0;

    $prod_lines = array();
    $i = 0;
    if($interval == 'days'){
      foreach($dates as $date){ // check in all dates

        foreach($prodstats as $key => $entries){ // go through each product
          $date_found = false;
          foreach($entries as $entry){ // check each product entry
            if($entry['date'] == $date){
              #echo "I are even ".$entry['date']." == ".$date."\n ";
              $prod_lines[$key][$i] = array($i, $entry['count']);
              $y_max = ($y_max >= $entry['count'])? $y_max : $entry['count']; // counts has index 1
              $date_found = true;
              break;
            }else{
              #echo "Not even ".$entry['date']." == ".$date."\n ";
            }
          }

          if($date_found === false){ // not found
            $prod_lines[$key][$i] = array($i, 0);
          }

          if(!$prod_series[$key]){
            $prod_series[$key] = array(
              "label" => $entries[0]["name_lv"],
              "color" => $this->getProductColor($entries[0]['prod_id']),
              "showLabel" => true

            );
          }

        }

        $i++;

      }

      $x_max = count($dates);
    }else{ // weeks and months

      $prev_date = 0;
      foreach($dates as $date){ // check in all dates

        foreach($prodstats as $key => $entries){ // go through each product
          $date_found = false;
          foreach($entries as $entry){ // check each product entry
            if($entry['date'] <= $date && $entry['date'] > $prev_date){
              #echo "I are even ".$entry['date']." == ".$date."\n ";
              $prod_lines[$key][$i] = array($i, $entry['count']);
              $y_max = ($y_max >= $entry['count'])? $y_max : $entry['count']; // counts has index 1
              $date_found = true;
              break;
            }else{
              #echo "Not even ".$entry['date']." == ".$date."\n ";
            }
          }

          if($date_found === false){ // not found
            $prod_lines[$key][$i] = array($i, 0);
          }

          if(!$prod_series[$key]){
            $prod_series[$key] = array(
              "label" => $entries[0]["name_lv"],
              "color" => $this->getProductColor($entries[0]['prod_id']),
              "showLabel" => true

            );
          }

        }

        $i++;
        $prev_date = $date;

      }


    }

    $stop_date = date('Y-m-d H:i:s', strtotime($stop_date . ' + 1 day'));

    $y_max = ceil($y_max * 1.1); // + 10%

    $debug['dates'] = $dates;
    $debug['days'] = $orders_days;
    $debug['data_length'] = $data_len;
    $debug['y_max'] = $y_max;
    $debug['orders_by_date'] = $orders_by_date;
    $debug['prods'] = $prods;
    $debug['prodstats'] = $prodstats;
    $debug['prod_lines'] = $prod_lines;
    $debug['prod_series'] = $prod_series;

    $this->prod_series = $prod_series; // so we can grab colors later

    $output = array();

    $i = 0;

    $output["data"] = array_values($prod_lines);
    $output["series"] = array();

    $debug['y_max'] = $y_max;

    for($i = 0; $i <= $y_max; $i++){
      $output['options']['y_ticks'][] = $i;
    }

    foreach($dates as $i => $date){
      $output['options']['x_ticks'][] = array($i, date("d.m.Y", strtotime($date)));
    }

    $output['options']['prod_series'] = array_values($prod_series);
    $output['options']['y_max'] = $y_max + 5;
    $output['options']['y_min'] = 0;
    $output['options']['x_min'] = 0;
    $output['options']['x_max'] = $x_max;
		$output['options']['title'] = lcms('Ordered product counts by dates');
    $output['debug'] = $debug;

    echo json_encode($output);

    return $debug;
  }

  function getProductColor($id){

    static $colors;

    if($colors[$id]) return $colors[$id];

    $colors[$id] = "#".$this->random_color();

    return $colors[$id];

  }

  function random_color_part() {
    return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
  }

  function random_color() {
      return $this->random_color_part() . $this->random_color_part() . $this->random_color_part();
  }


  function countDays( $a, $b )
  {
    $gd_a = getdate( $a );
    $gd_b = getdate( $b );

    $a_new = mktime( 12, 0, 0, $gd_a['mon'], $gd_a['mday'], $gd_a['year'] );
    $b_new = mktime( 12, 0, 0, $gd_b['mon'], $gd_b['mday'], $gd_b['year'] );

    return round( abs( $a_new - $b_new ) / 86400 );
  }

  function toolbar(&$script, &$toolbars, &$btnids, &$onclick){
    return true;
  }

  function SetProperties(){
    $this->properties = Array(
      'name' => Array(
        'label'     => 'Name:',
        'type'      => 'str'
      ),

    );
  }
}
