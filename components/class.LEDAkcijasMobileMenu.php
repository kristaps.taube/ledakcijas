<?php

class LEDAkcijasMobileMenu extends component
{

    public function output()
    {

        $catcol = \shopcatcollection::getInstance();

        return $this->render('index', [
            'languages' => getLanguages(),
            'categories' => $catcol->getForMenu()
        ]);

    }

}