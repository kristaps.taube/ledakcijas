<?php
/**
*
*  Title: Shop menu
*  Author: Kristaps Taube <ktaube@datateks.lv>
*  Date: 23.01.2014
*  Project: E-Veikals
*
*/

include_once("class.component.php");

class EVeikalsShopMenu extends component{

  function __construct($name){
    parent::__construct($name);
    $this->SetProperties();
  }

  function output($category){

    $this->cat_id = $category ? $category['item_id'] : 0;
    $this->open_cats = $this->makeOpenCats($this->cat_id);
    $this->shopmanager = shopmanager::getInstance();

    $menu_type = option('Shop\\menu_type', null, 'Menu type', array(
      'type' => 'list',
      'fieldparams' => array(
        'lookupassoc' => array(
          'full' => 'Full',
          'compact' => 'Compact',
        )
      )
    ), 'full');

    if($menu_type == 'full'){

        $parent = 0;
        $cats = $this->cats->getAllCatsByParent($parent);

    }elseif($menu_type == 'compact'){

        $parent = $cat_id;

        if(!$cat_id){
            $cats = $this->cats->getAllCatsByParent(0);
        }else{
            $cats = array($this->cats->getFromCache($cat_id));
        }

    }

    $cat_count = count($cats);

    $i = 0;
    ?>
    <div class='box'>
      <div class='title'><?=$this->l("Katalogs")?></div>
      <div id='CatalogMenu'>
        <? if(!empty($cats)){ ?>
        <ul class='menu'>
          <? foreach($cats as $cat){ ?>
            <? $this->output_menu_item($cat, (++$i == $cat_count)); ?>
          <? } ?>
        </ul>
        <? } ?>
      </div>
    </div>
    <?

  }

  private function makeOpenCats($cat_id)
  {

    $cats = [];

    do{
        $cat = $this->cats->getFromCache($cat_id);
        if($cat){
            $cats[] = $cat['item_id'];
            $cat_id = $cat['parent'];
        }
    }while($cat);

    return $cats;

  }

  function output_menu_item($cat, $last = false){

    $open = in_array($cat['item_id'], $this->open_cats);

    $prod_count = $this->cats->getCategoryProductCount($cat['item_id'], $this->prods);

    ?>
    <li class='<?=($last)? "last" : "" ?> <?=($open)? "active" : "" ?>'>
        <a href='<?php echo Constructor\Url::get('category', ['category' => $cat])?>' title='<?=hsc($cat['title_'.$this->lang])?>'><?=($cat['lvl']>1) ? "- ": ""?> <?=hsc($cat['title_'.$this->lang])?> <?=($prod_count ? "(".$prod_count.")" : "")?></a>
        <?php
            if($open){
                $kids = $this->cats->getAllCatsByParent($cat['item_id']);
                $cat_count = count($kids);
                $i = 0;
                if(!empty($kids)){
                    echo "<ul class='menu'>";
                    foreach($kids as $kid){
                        $this->output_menu_item($kid, (++$i == $cat_count));
                    }
                    echo "</ul>";
                }
            }
        ?>
    </li>
    <?

  }

  function SetProperties(){
    $this->properties = Array(

      "name"        => Array(
        "label"     => "Name:",
        "type"      => "str"
      ),

      "cats" => Array(
        "label"       => "Category collection:",
        "type"        => "collection",
        "collectiontype" => "shopcatcollection",
        "lookup"      => GetCollections($this->site_id, "shopcatcollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "prods" => Array(
        "label"       => "Product collection:",
        "type"        => "collection",
        "collectiontype" => "shopprodcollection",
        "lookup"      => GetCollections($this->site_id, "shopprodcollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

    );
  }

}
