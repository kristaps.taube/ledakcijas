<?
//Serveris.LV Constructor component
//Created by Kristaps Grinbergs, 2006, kristaps@datateks.lv
//Project: Constructor Gallery Component

include_once("class.component.php");

class gallerytemplateex extends component
{

//Class initialization
function gallerytemplateex($name)
{
    parent::__construct($name);
    $this->SetProperties();
    $this->getLanguageStrings();
}

  function getLanguageStrings()
  {
    $ls = Array(
    );
    $this->parseTemplatePropertiesForLanguageStrings($ls);
    return $ls;
  }
function output($isdesign=0)
{
  $type = $this->getProperty('type');
  if ($type==1)
  {
    $this->SingleGallery();
  }
  else if($type==0)
  {
    $this->ListGalleries();
  }else
  {
    if($_GET['galid'])
    {
      $this->SingleGallery();
    }else
    {
      $this->ListGalleries();
    }
  }

}

function SinglePicture($gallery, $pic_id, $ajax=false)
{
  if (!is_array($gallery))
  {
    $gallery = $this->galleries->getItemByIdAssoc($gallery);
    if (!$gallery) return;  
  }

  $multilang = $this->getProperty('multilingual');
  if($multilang)
    $lang = $this->getLanguageName();
    
  $gallery_id = $gallery['item_id'];
  
  $picture = $this->pictures->getItemByIDAssoc($pic_id);
  if($picture['disabled'])
  {
    $picture = false;
  }
  //ja bilde nav noraadiita vai ir nekorekta, atteelot pirmo bildi galerijaa
  if(!$picture)
  {
    $picture = $this->pictures->getFirstPictureForGallery($gallery_id);
    $pic_id = $picture['item_id'];
  }

  //set up general template variables
  $this->outvars['galleryrow'] = $gallery;
  $this->outvars['gal_id'] = $gallery['item_id'];
  if(!$multilang)
  {
    $this->outvars['gallerytitle'] = $gallery['title'];
  }else
  {
    $outtitle = unserialize($gallery['title']);
    $this->outvars['gallerytitle'] = $outtitle[$lang];
  }
  $this->outvars['picturerow'] = $picture;
  $this->outvars['pic_id'] = $picture['item_id'];
  $this->outvars['imglink'] = $picture['image'];
  $this->outvars['photographer'] = $picture['photographer'];
  $this->outvars['telephone'] = $picture['phone'];
  if(!$multilang)
  {
    $this->outvars['text'] = $picture['text'];
  }else
  {
    $outtext = unserialize($picture['text']);
    $this->outvars['text'] = $outtext[$lang];
  }
  $this->outvars['galleriesindex'] = $this->getProperty('galleries_list_path');
  $this->outvars['galleryindex'] = $this->getProperty('gallery_path').'?galid='.$gallery_id;

  //get next and previous image
  $prevpic = $this->pictures->getPreviousPicture($gallery_id, $picture['ind']);
  $nextpic = $this->pictures->getNextPicture($gallery_id, $picture['ind']);
  $this->outvars['prevpicrow'] = $prevpic;
  $this->outvars['first'] = (!$prevpic);
  $this->outvars['nextpicrow'] = $nextpic;
  $this->outvars['last'] = (!$nextpic);

  //get link to next and previous image
  if($prevpic)
    $prev = $prevpic['item_id'];
  else
    $prev = $pic_id;
  if($nextpic)
    $next = $nextpic['item_id'];
  else
    $next = $pic_id;
  $this->outvars['nextpath'] = $this->getProperty('gallery_path').'?galid='.$gallery_id.'&picture_id='.$next;
  $this->outvars['prevpath'] = $this->getProperty('gallery_path').'?galid='.$gallery_id.'&picture_id='.$prev;
  
  $this->outvars['nextonclick'] = 'return cmpQuery({picture_id:gallery.next})';
  $this->outvars['prevonclick'] = 'return cmpQuery({picture_id:gallery.prev})';

  //comments
  if($comments)
  {
    if($_GET['action'] == 'addcomment')
      $this->AddComment($comments, $gallery_id, $pic_id);

    $commenthtml = '';
    $commentlist = $comments->getNewsComments($pic_id);
    foreach($commentlist as $comment)
    {
      $this->outvars['commentauthor'] = $comment['col1'];
      $this->outvars['commenttext'] = $comment['col2'];
      $commentdate = strtotime($comment['col3']);
      $dateformat = $this->getProperty('dateformat');
      if (!$dateformat)
      {
        $commentdate = $this->FormatDate($commentdate);
      } else
      {
        $commentdate = $this->date_($dateformat,$commentdate);
      }

      $this->outvars['commenttime'] = $commentdate;
      $commenthtml .= $this->ReturnTemplate('commentlistitem');
    }

    $this->outvars['commentslist'] = $commenthtml;
    $this->outvars['hiddenfield'] = '<input type="hidden" name="commenttime" value="' . time() . '">';

    $this->outvars['comments'] = $this->ReturnTemplate('galcomments');
    $this->outcalls['comments_count'] = '$this->commentscol->getNewsCommentsCount(' . intval($pic_id) . ')';
  }else
  {
    $this->outvars['comments'] = '';
    $this->outvars['comments_count'] = 0;
  }
  
  if (!$ajax)
  {
    $this->includeLibrary('jquery');
  
    $pictures = $this->pictures->getPicturesForGallery($gallery_id, true);
    $pic_ids = Array();
    foreach ($pictures as $pic)
    {
      $pic_ids[] = $pic['item_id'];
    }
  
    $this->addScript('
      var gallery = {
        gal_id: '.$gallery_id.',
        pic_id: '.$picture['item_id'].',
        pics: '.json_encode($pic_ids).',
        prev: '.$prev.',
        next: '.$next.'
      }
    ');
    
    $this->onAjaxQueryChange(Array('picture_id'), '
      var pic_id = parseInt(query.picture_id);
      gallery.pic_id = pic_id;
      for (var i in gallery.pics)
      {
        if (gallery.pics[i] == pic_id)
        {
          gallery.prev = (i > 0) ? gallery.pics[i-1] : 0;
          gallery.next = (i < (gallery.pics.length-1)) ? gallery.pics[Number(i)+1] : 0;
        }
      }
      cmpPost("'.$this->name.'", "SinglePicture", "'.$this->name.'_single", {galid:gallery.gal_id, picture_id:pic_id});
    ');   
  }
  if (!$ajax) echo '<div id="'.$this->name.'_single">';
  $this->ProcessTemplate('singlegallery');
  if (!$ajax) echo '</div>';
}

function ajax_SinglePicture()
{
  $this->init_collections();
  $comments = $this->initpropertyCollection('commentcollection');
  $this->commentscol = $comments;

  $gal_id = intval($_GET['galid']);
  $pic_id = intval($_GET['picture_id']);
  if ($gal_id && $pic_id)
    $this->SinglePicture($gal_id, $pic_id, true);
                     
}

#-- S : Output Single Gallery (pictures list or single picture depending on GET) --#
function SingleGallery ()
{
  $galstep = $this->getProperty('galstep');

  $this->includeLibrary('fancybox');

  $multilang = $this->getProperty('multilingual');
  if($multilang)
    $lang = $this->getLanguageName();

  $this->init_collections();
  $comments = $this->initpropertyCollection('commentcollection');
  $this->commentscol = $comments;

  $gallery_id = intval($_GET['galid']);
  if (!$gallery_id)
  {
    //ja galerija nav noraadiita, atteelojam pirmo galeriju sarakstaa
    $gallery = $this->galleries->getFirstGallery();
    if($galleries)
    {
      $gallery_id = $galleries['item_id'];
    }
  }
  $gallery = $this->galleries->getItemByIdAssoc($gallery_id);

  if(!$gallery['disabled'])
  {

    if ((isset($_GET['picture_id']) && !empty($_GET['picture_id']))||($_GET['firstpicture']))
    {
      //************************** single pic ****
      $pic_id = intval($_GET['picture_id']);

      $this->SinglePicture($gallery, $pic_id);                
    }
    else //********************** pictures list ****
    {
      $pictures = $this->pictures->getPicturesForGallery($gallery_id, true);
      $this->outvars['galleryrow'] = $gallery;
      $this->outvars['gal_id'] = $gallery_id;
      if(!$multilang)
      {
        $this->outvars['gallerytitle'] = $gallery['title'];
      }else
      {
        $outtitle = unserialize($gallery['title']);
        $this->outvars['gallerytitle'] = $outtitle[$lang];
      }

      $dateformat = $this->getProperty('dateformat');
      $date = strtotime($gallery['date']);
      if (!$dateformat)
      {
        $date = $this->FormatDate($date);
      } else
      {
        $d = $this->date_($dateformat,$date);
        $date = $d;
      }
      $this->outvars['gallerydate'] = $date;

      $this->outvars['allpictures'] = $pictures;
      $i = 0;
      for ($j=0; $j<count($pictures); $j+=$galstep)
      {
        $i += $galstep;
        $pic = $pictures[$j];
        $this->outvars['pic_id'] = $pic['item_id'];
        $this->outvars['picturerow'] = $picture;
        $this->outvars['num'] = $i;
        $this->outvars['first'] = ($i == $galstep);
        $this->outvars['last'] = ($i == count($pictures));

        if(!$multilang)
        {
          $this->outvars['text'] = $pic['text'];
        }else
        {
          $outtext = unserialize($pic['text']);
          $this->outvars['text'] = $outtext[$lang];
        }
        $this->outvars['bigpic'] = $pic['image'];
        if ($galstep > 1)
        {
          $this->outvars['thumbnail'] = array();
          $this->outvars['link'] = array();
          $this->outvars['origpic'] = array();
          for ($k=0; $k<$galstep; $k++)
          {
            $this->outvars['link'][$k] = $this->getProperty('gallery_path').'?galid='.$gallery_id.'&picture_id='.$pictures[$j+$k]['item_id'];
            $this->outvars['thumbnail'][$k] = $pictures[$j+$k]['thumb'];
            $this->outvars['origpic'][$k] = $pictures[$j+$k]['origimage'];
          }

        }
        else
        {
          $this->outvars['origpic'] = $pic['origimage'];
          $this->outvars['thumbnail'] = $pic['thumb'];
          $this->outvars['link'] = $this->getProperty('gallery_path').'?galid='.$gallery_id.'&picture_id='.$pic['item_id'];
        }



        if($comments)
          $this->outcalls['comments_count'] = '$this->commentscol->getNewsCommentsCount(' . intval($pic_id) . ')';
        else
          $this->outvars['comments_count'] = 0;

        $this->outvars['pictureslist'] .= $this->ReturnTemplate('piclistitem');
      }
      $this->outvars['gallerieslistpath'] = $this->getProperty('galleries_list_path');
      if(!$this->outvars['gallerieslistpath'])
        $this->outvars['gallerieslistpath'] = '?';
      $this->ProcessTemplate('piclistcontainer');
    }
  }

  $this->addScript('$(function()
  {
    $(".gal_pictures a.pic").fancybox();
  }); ');
}
#-- B : Output Single Gallery --#


#-- S : Output Galleries List --#
function ListGalleries ()
{
  $multilang = $this->getProperty('multilingual');
  if($multilang)
    $lang = $this->getLanguageName();

  $this->init_collections();
  $dateformat = $this->getProperty('dateformat');

  $i = 0;
  $galleries = $this->galleries->getEnabledGalleries();
  foreach ($galleries as $gal)
  {
    $i++;

    $date = strtotime($gal['date']);
    if (!$dateformat)
    {
      $date = $this->FormatDate($date);
    } else
    {
      $d = $this->date_($dateformat,$date);
      $date = $d;
    }

    $this->outvars['id'] = $gal['item_id'];
    $this->outvars['galleryrow'] = $gal;

    //use either the specified gallery thumbnail or thumbnail of first gallery picture
    if($gal['thumb'])
    {
      $this->outvars['thumbnail'] = $gal['thumb'];
      $this->outvars['hasthumb'] = true;
    }else
    {
      $firstpic = $this->pictures->getFirstPictureForGallery($gal['item_id']);
      $this->outvars['thumbnail'] = $firstpic['thumb'];
      $this->outvars['hasthumb'] = false;
    }
    if(!$multilang)
    {
      $this->outvars['title'] = $gal['title'];
      $this->outvars['description'] = $gal['description'];
    }else
    {
      $title = unserialize($gal['title']);
      $this->outvars['title'] = $title[$lang];
      $description = unserialize($gal['description']);
      $this->outvars['description'] = $description[$lang];
    }

    $this->outvars['date'] = $date;
    $this->outcalls['pictures_count'] = '$this->pictures->countPicturesForGallery(' . intval($gal['item_id']) . ', true)';
    $this->outvars['path'] = $this->getProperty('gallery_path').'?galid='.$gal['item_id'];
    $this->outvars['num'] = $i;

    $this->outvars['first'] = ($i == 1);
    $this->outvars['last'] = ($i == count($galleries));

    $this->outvars['gallerieslist'] .= $this->ReturnTemplate('listitem');
  }
  $this->ProcessTemplate('listcontainer');
}
#-- B : Output Galleries List --#


function AddComment($comments, $gal_id, $pic_id)
{
  $ok = true;
  if(!$_POST['commenttime'] || time() - $_POST['commenttime'] < 3 || time() - $_POST['commenttime'] > 86400)
  {
    $ok = false;
  }
  if (isset($_POST) && !empty($_POST['author']) && !empty($_POST['comment']))
  {
    if(strpos(strip_tags(strtoupper($_POST['comment'])), '[URL=') !== false)
    {
      $ok = false;
      header ('Location: ?galid='.$gal_id.'&picture_id='.$pic_id);
    }
    if ($ok)
    {
      $ind=$comments->AddNewItem();
      $properties = array(intval($pic_id),strip_tags($_POST['author']),nl2br(strip_tags($_POST['comment'])),date("Y-m-d H:i:s", time()),getenv("REMOTE_ADDR"));
      $comments->ChangeItem($ind,$properties);
    }
  }
  if ($ok)
    @header ('Location: ?galid='.$gal_id.'&picture_id='.$pic_id);
}


//=============================== DESIGN =========================================

function open_gallery()
{
  if($_SESSION['opengalerybranch'] != $_GET['galerijas_id'])
    $_SESSION['opengalerybranch'] = $_GET['galerijas_id'];
  else
    $_SESSION['opengalerybranch'] = 0;
}

function design ()
{
  $this->init_collections(); //init collections

  $multilang = $this->getProperty('multilingual');
  $langs = $this->ListLanguages();
  $lang = $this->getLanguageName();
?>
<!-- JavaScript for galleries divs show/hide -->
<script type="text/javascript">
  window.blockVisibility = function(block_id)
  {
    try {
                      PWin.close();
                     } catch(e) { }
                 var args = new Array(window, document.getElementById('div_<?= $this->name ?>'));
                 openModalWin('?module=dialogs&action=component&site_id=<?= $this->site_id ?>&page_id=<?= $this->page_id ?>&component_name=<?= $this->name ?>&component_type=<?= get_class($this) ?>&method_name=open_gallery&refresh=1&okaction=submit&session_id=<?= session_id() ?>&session_name=<?= session_name() ?>&galerijas_id='+block_id+'&nochngetime=1', args, 1, 1, 0, 0, 0);
  }

  window.setCookie = function (name, value, expires)
  {
    if (!expires)
    {
      expires = new Date();
    }
    document.cookie = name + "=" + escape (value) + "; expires=" + expires.toGMTString() +  "; path=/";
  }

    window.editpic = function(ind, galid)
  {
    try {
                      PWin.close();
                     } catch(e) { }
                 var args = new Array(window, document.getElementById('div_<?= $this->name ?>'));
                 openModalWin('?module=dialogs&action=componentframe&site_id=<?= $this->site_id ?>&page_id=<?= $this->page_id ?>&component_name=<?= $this->name ?>&component_type=<?= get_class($this) ?>&method_name=edit_pic&refresh=0&okaction=submit&session_id=<?= session_id() ?>&session_name=<?= session_name() ?>&pic_id='+ind+'&galerijas_id='+galid+'', args, 400, 400, 0, 0, 0);
  }

  window.uppic = function(ind)
  {
    try {
                      PWin.close();
                     } catch(e) { }
                 var args = new Array(window, document.getElementById('div_<?= $this->name ?>'));
                 openModalWin('?module=dialogs&action=componentframe&site_id=<?= $this->site_id ?>&page_id=<?= $this->page_id ?>&component_name=<?= $this->name ?>&component_type=<?= get_class($this) ?>&method_name=up_pic&refresh=1&okaction=submit&session_id=<?= session_id() ?>&session_name=<?= session_name() ?>&pic_id='+ind+'', args, 1, 1, 0, 0, 0);
  }

  window.downpic = function(ind)
  {
    try {
                      PWin.close();
                     } catch(e) { }
                 var args = new Array(window, document.getElementById('div_<?= $this->name ?>'));
                 openModalWin('?module=dialogs&action=componentframe&site_id=<?= $this->site_id ?>&page_id=<?= $this->page_id ?>&component_name=<?= $this->name ?>&component_type=<?= get_class($this) ?>&method_name=down_pic&refresh=1&okaction=submit&session_id=<?= session_id() ?>&session_name=<?= session_name() ?>&pic_id='+ind+'', args, 1, 1, 0, 0, 0);
  }

  window.delpic = function(ind)
  {
    try {
                      PWin.close();
                     } catch(e) { }
                 var args = new Array(window, document.getElementById('div_<?= $this->name ?>'));
                 openModalWin('?module=dialogs&action=componentframe&site_id=<?= $this->site_id ?>&page_id=<?= $this->page_id ?>&component_name=<?= $this->name ?>&component_type=<?= get_class($this) ?>&method_name=del_pic&refresh=1&okaction=submit&session_id=<?= session_id() ?>&session_name=<?= session_name() ?>&pic_id='+ind+'', args, 1, 1, 0, 0, 0);
  }

</script>

<?php

    $data = $this->galleries->getDBData(); //get galleries data

    echo '<b>Galerijas</b>';
    echo '<div padding-left:20px;">';

    $n = 1;
    foreach ($data as $key=>$row)
    {
      $pictures = $this->pictures->getPicturesForGallery($row['item_id']);

      echo '<div style="padding-left:15px; margin-bottom: 10px;">';
      echo '<span style="margin-bottom: 5px; display: block;">';

      if (count($pictures)>0 && isset($pictures[0]['thumb']) && !empty($pictures[0]['thumb']))
      {
         echo '<a href="javascript:blockVisibility(\''.$row['item_id'].'\')" style="cursor:default; background: Red;"><img id="galerija'.$row['item_id'].'_bult" src="/cms/backend/gui/images/'.($_SESSION['opengalerybranch'] != $row['item_id'] ? 'plus' : 'minus').'.gif" style="border:0;" /></a> ';
      }

      if(!$multilang)
        $title = $row['title'];
      else
      {
        $title = unserialize($row['title']);
        $title = $title[$lang];
      }
      if($row['disabled'])
        echo '<strike>';
      echo $title.' ('.$row['date'].')';
      if($row['disabled'])
        echo '</strike>';

      echo '
          <a href="javascript:void(0);" onClick="javascript:
                 try {
                      PWin.close();
                     } catch(e) { }
                 var args = new Array(window, document.getElementById(\'div_'.$this->name.'\'));
                 openModalWin(\'?module=dialogs&action=componentframe&site_id='.$this->site_id.'&page_id='.$this->page_id.'&component_name='.$this->name.'&component_type='.get_class($this).'&method_name=edit_gallery&refresh=0&okaction=submit&session_id=' . session_id() . '&session_name=' . session_name() . '&galerijas_id='.$row['ind'].'\', args, 400, 400, 0, 0, 0);"><img src="/cms/backend/gui/images/edit.gif" style="border: 0;" /></a>';


          if ($n!=1)
          echo '<a href="javascript:void(0);" onClick="javascript:
                 try {
                      PWin.close();
                     } catch(e) { }
                 var args = new Array(window, document.getElementById(\'div_'.$this->name.'\'));
                 openModalWin(\'?module=dialogs&action=componentframe&site_id='.$this->site_id.'&page_id='.$this->page_id.'&component_name='.$this->name.'&component_type='.get_class($this).'&method_name=up_gallery&refresh=1&okaction=submit&session_id=' . session_id() . '&session_name=' . session_name() . '&galerijas_id='.$row['ind'].'\', args, 1, 1, 0, 0, 0);"><img src="/cms/backend/gui/images/up.gif" style="border: 0;" /></a>';

          if ($n!=count($data))
          echo '<a href="javascript:void(0);" onClick="javascript:
                 try {
                      PWin.close();
                     } catch(e) { }
                 var args = new Array(window, document.getElementById(\'div_'.$this->name.'\'));
                 openModalWin(\'?module=dialogs&action=componentframe&site_id='.$this->site_id.'&page_id='.$this->page_id.'&component_name='.$this->name.'&component_type='.get_class($this).'&method_name=down_gallery&refresh=1&okaction=submit&session_id=' . session_id() . '&session_name=' . session_name() . '&galerijas_id='.$row['ind'].'\', args, 1, 1, 0, 0, 0);"><img src="/cms/backend/gui/images/down.gif" style="border: 0;" /></a>';

          echo '<a href="javascript:void(0);" onClick="javascript:
                 try {
                      PWin.close();
                     } catch(e) { }
                 if(!confirm(\'Are you sure you want to delete this gallery?\')) return false;
                 var args = new Array(window, document.getElementById(\'div_'.$this->name.'\'));
                 openModalWin(\'?module=dialogs&action=componentframe&site_id='.$this->site_id.'&page_id='.$this->page_id.'&component_name='.$this->name.'&component_type='.get_class($this).'&method_name=del_gallery&refresh=1&okaction=submit&session_id=' . session_id() . '&session_name=' . session_name() . '&galerijas_id='.$row['item_id'].'\', args, 1, 1, 0, 0, 0);"><img src="/cms/backend/gui/images/del.gif" style="border: 0;" /></a></span>';

      if (count($pictures)>0 && isset($pictures[0]['thumb']) && !empty($pictures[0]['thumb']))
      {
        echo '<div id="galerija'.$row['item_id'].'" style="'.($_SESSION['opengalerybranch'] ?'display:block;':'display:none;').' padding-left:25px;">';

        if($_SESSION['opengalerybranch'] == $row['item_id'])
        {
          $i = 1;
          foreach ($pictures as $pic)
          {

            if($pic['disabled'])
              echo '<strike>';
            echo '<span '.($i != count($pictures) ? 'style="margin-bottom: 9px; display: block;"' : '').'><img src="'.$pic['thumb'].'" />';
            if($pic['disabled'])
              echo 'disabled</strike>';
            echo '
            <a href="javascript:void(0);" onClick="javascript:
                   editpic('.$pic['ind'].', '.$row['item_id'].');"><img src="/cms/backend/gui/images/edit.gif" style="border: 0;" /></a>';
            if ($i!=1)
            echo '<a href="javascript:void(0);" onClick="javascript:
                   uppic('.$pic['ind'].')"><img src="/cms/backend/gui/images/up.gif" style="border: 0;" /></a>';



            if ($i!=count($pictures))
            echo '<a href="javascript:void(0);" onClick="javascript:
                   downpic('.$pic['ind'].')"><img src="/cms/backend/gui/images/down.gif" style="border: 0;" /></a>';

            echo '<a href="javascript:void(0);" onClick="javascript:
                   delpic('.$pic['item_id'].')"><img src="/cms/backend/gui/images/del.gif" style="border: 0;" /></a></span></span>';

          $i++;
          }
        }
        echo '</div>';
      }

      echo '<a href="javascript:void(0);" onClick="javascript:
                 try {
                      PWin.close();
                     } catch(e) { }
                 var args = new Array(window, document.getElementById(\'div_'.$this->name.'\'));
                 openModalWin(\'?module=dialogs&action=componentframe&site_id='.$this->site_id.'&page_id='.$this->page_id.'&component_name='.$this->name.'&component_type='.get_class($this).'&method_name=add_pic&refresh=0&okaction=submit&session_id=' . session_id() . '&session_name=' . session_name() . '&galerijas_id='.$row['item_id'].'\', args, 400, 400, 0, 0, 0);
                                                                 ">[pievienot attēlu]</a><br/>
                 <a href="javascript:void(0);" onClick="javascript:
                 try {
                      PWin.close();
                     } catch(e) { }
                 var args = new Array(window, document.getElementById(\'div_'.$this->name.'\'));
                 openModalWin(\'?module=dialogs&action=componentframe&site_id='.$this->site_id.'&page_id='.$this->page_id.'&component_name='.$this->name.'&component_type='.get_class($this).'&method_name=add_pics&refresh=0&okaction=submit&session_id=' . session_id() . '&session_name=' . session_name() . '&galerijas_id='.$row['item_id'].'\', args, 400, 280, 0, 0, 0);
                                                                 ">[pievienot attēlus]</a>';

        echo '</div>';
      $n++;
    }
    echo '</div>';
    echo '<br/><a href="javascript:void(0);" onClick="javascript:
                 try {
                      PWin.close();
                     } catch(e) { }
                 var args = new Array(window, document.getElementById(\'div_'.$this->name.'\'));
                 openModalWin(\'?module=dialogs&action=componentframe&site_id='.$this->site_id.'&page_id='.$this->page_id.'&component_name='.$this->name.'&component_type='.get_class($this).'&method_name=add_gallery&refresh=0&okaction=submit&session_id=' . session_id() . '&session_name=' . session_name() . '\', args, 400, 380, 0, 0, 0);">[pievienot galeriju]</a>';


}

/*  GALLERY FUNCTIONS :
*     * Add gallery
*     * Delete gallery
*     * Move Up gallery
*     * Move Down gallery
*     * Edit gallery
*     * Save Edited gallery
*/

#-- S : ADD GALLERY --#
function add_gallery()
{
  $multilang = $this->getProperty('multilingual');
  $langs = $this->ListLanguages();

  echo '<table align="center">';
    echo '<form name=Form enctype="multipart/form-data" action="?module=dialogs&action=component&site_id='.
		$this->site_id.'&page_id='.$this->page_id.'&component_name='.
		$this->name.'&component_type='.
		get_class($this).'&method_name=save_gallery&refresh=1&session_id=' . session_id() . '&session_name=' . session_name() . '" method="post">
    ';
    if(!$multilang)
      echo '
      <tr><td class="formLabel">Title: </td><td><input type="text" name="title" size="45"></td></tr>';
    else
    {
      foreach($langs as $lang)
      {
        echo '
      <tr><td class="formLabel">Title ('.$lang.'): </td><td><input type="text" name="title_'.$lang.'" size="45"></td></tr>';

      }
    }
    echo '
    <tr><td class="formLabel">Date: </td><td><input type="text" name="date" value="'.date('Y-m-d H:i:s',time()).'">&nbsp;<input class="formButton" type="button" value="..." OnClick="
        var val=openModalWin(\'?module=dialogs&action=date&site_id=4&time=1\', document.Form.date.value, 400, 450, 0);
                             if (val) {
                               document.Form.date.value=val;
                             }" ></td></tr>
    <tr><td class="formLabel">Upload from file: </td><td><input type="radio" name="imgtype" value="file" checked></td></tr>
    <tr><td class="formLabel">Gallery thumbnail: </td><td><input style="font-size:12px;" type="file" name="userfile"></td></tr>
            <tr><td class="formLabel">Link to image: </td><td><input type="radio" name="imgtype" value="link"></td></tr>
            <tr><td class="formLabel">Thumbnail link [optional]: </td><td><input style="font-size:12px;" type="text" name="thumblink"><INPUT class=formButton onclick="var val=openModalWin(\'?module=dialogs&action=filex&site_id='.$this->site_id.'&view=thumbs\', document.Form.thumblink.value, 600, 450); if (val) document.Form.thumblink.value=val;" type=button value=...></td></tr>';
    if($this->getProperty('showgaldesc'))
    {
      if(!$multilang)
        echo '
            <tr><td class="formLabel">Description: </td><td>
            <textarea class="formEdit" style="width: 200px" name="description" rows="6" cols="30"></textarea>&nbsp;<input class="formButton" type="button" value="..." OnClick="
                             var val=openModalWin(\'?module=wysiwyg&canpreview=0&site_id='.$this->site_id.'&page_id='.$this->page_id.'\', document.Form.description.value, 720, 450, 1);
                             if (val) {
                               document.Form.description.value=val;
                             }">
            </td></tr>';
      else
      {
        foreach($langs as $lang)
        {
          echo '
            <tr><td class="formLabel">Description ('.$lang.'): </td><td>
            <textarea class="formEdit" style="width: 200px" name="description_'.$lang.'" rows="6" cols="30"></textarea>&nbsp;<input class="formButton" type="button" value="..." OnClick="
                             var val=openModalWin(\'?module=wysiwyg&canpreview=0&site_id='.$this->site_id.'&page_id='.$this->page_id.'\', document.Form.description_'.$lang.'.value, 720, 450, 1);
                             if (val) {
                               document.Form.description_'.$lang.'.value=val;
                             }">
            </td></tr>';
        }
      }
    }
    echo '
    <tr><td class="formLabel">Disabled: </td><td><input type="checkbox" name="disabled"></td></tr>
            <tr>
</tr>
            <tr><td></td><td align="right"><!--<input style="font-size:12px;" type="submit" name="submit" value="Upload" onClick="javascript:window.close();">--></td></tr>
          </form>';
    echo '</table>';
}
#-- B : ADD GALLERY --#

#-- S : SAVE GALLERY --#
function save_gallery ($edit=false)
{
  $multilang = $this->getProperty('multilingual');
  $langs = $this->ListLanguages();

  $this->init_collections();

  if(!$multilang)
  {
    $title = $_POST["title"];
  }else
  {
    $title = Array();
    foreach($langs as $lang)
    {
      $title[$lang] = $_POST['title_' . $lang];
    }
    $title = serialize($title);
  }
  $date = $_POST["date"];
  if(!$multilang)
  {
    $description = $_POST['description'];
  }else
  {
    $description = Array();
    foreach($langs as $lang)
    {
      $description[$lang] = $_POST['description_' . $lang];
    }
    $description = serialize($description);
  }
  $thumblink = $_POST['thumblink'];
  $disabled = $_POST['disabled'];

  if ($_POST['imgtype']=="file")
  {
    $imgdir = $this->galleries->GetName();
    $path = 'images/'.$imgdir.'/thumbs/';

    forceDirectories($this->site_id, $path);
    $up_data = uploadSiteFile($this->site_id, $_FILES['userfile'], $path);

    if($up_data)
    { //successful upload
      echo $up_data['url']."<br>";

      $galthumbwidth  = $this->getProperty('galthumbwidth');
      $galthumbheight = $this->getProperty('galthumbheight');
      $galresizemode  = $this->getproperty('galthumbresizemode');
      resizeImage($up_data['path'], $galthumbwidth, $galthumbheight, $galresizemode);

      //save changes to collection
      $properties = array($up_data['url'],$title,$date,$description, $disabled);
      $ind = $this->addChangeGallery($edit, $_GET['galerijas_id']);
      $this->galleries->ChangeItem($ind,$properties);

    }else {
      print "File was not uploaded";
      $_GET['refresh'] = 0;
    }
  }
  else if ($_POST['imgtype']=="link")
  {
    $properties=array($thumblink,$title,$date, $description, $disabled);
    $ind = $this->addChangeGallery($edit, $_GET['galerijas_id']);
    $this->galleries->ChangeItem($ind,$properties);
  }
}
#-- B : SAVE GALLERY --#

function addChangeGallery($edit, $gal_id)
{
  if($edit){
        $ind = $gal_id;
  }else
  {
    if(!$this->getProperty('galleryorder'))
    {
      $ind = $this->galleries->AddNewItemTop();
    }else
    {
      $ind = $this->galleries->AddNewItem();
    }
  }
  return $ind;
}

#-- S : MOVE GALLERY UP --#
function up_gallery ()
{
  if (isset($_GET['galerijas_id']) && !empty($_GET['galerijas_id']))
  {
    $this->init_collections();
    //die($_GET['galerijas_id']);
    $this->galleries->SwapItems($_GET['galerijas_id'],$_GET['galerijas_id']-1);
  }
}
#-- B : MOVE GALLERY UP --#

#-- S : MOVE GALLERY DOWN --#
function down_gallery()
{
  if (isset($_GET['galerijas_id']) && !empty($_GET['galerijas_id']))
  {
    $this->init_collections();
    $this->galleries->SwapItems($_GET['galerijas_id']+1,$_GET['galerijas_id']);
  }
}
#-- B : MOVE GALLERY DOWN --#

#-- S : EDIT GALLERY --#
function edit_gallery ()
{
  $multilang = $this->getProperty('multilingual');
  $langs = $this->ListLanguages();

  $this->init_collections();
  $row = $this->galleries->GetItem($_GET['galerijas_id']);

  echo '<table align="center">';
    echo '<form name=Form enctype="multipart/form-data" action="?module=dialogs&action=component&site_id='.
		$this->site_id.'&page_id='.$this->page_id.'&component_name='.
		$this->name.'&component_type='.
		get_class($this).'&method_name=save_edited_gallery&refresh=1&session_id=' . session_id() . '&session_name=' . session_name() . '&galerijas_id='.$_GET['galerijas_id'].'" method="post">
    ';
    if(!$multilang)
    {
      echo '
      <tr><td class="formLabel">Title: </td><td><input type="text" name="title" size="45" value="'.$row[1].'"></td></tr>';
    }else
    {
      $title_uns = unserialize($row[1]);
      foreach($langs as $lang)
      {
        echo '
        <tr><td class="formLabel">Title ('.$lang.'): </td><td><input type="text" name="title_'.$lang.'" size="45" value="'.$title_uns[$lang].'"></td></tr>';
      }
    }
    echo '
    <tr><td class="formLabel">Date: </td><td><input type="text" name="date" value="'.$row[2].'">&nbsp;<input class="formButton" type="button" value="..." OnClick="
        var val=openModalWin(\'?module=dialogs&action=date&site_id=4&time=1\', document.Form.date.value, 400, 450, 0);
                             if (val) {
                               document.Form.date.value=val;
                             }" ></td></tr>
    <tr><td class="formLabel">Thumbnail:</td><td><img src="'.$row[0].'" alt="" /></td></tr>
    <tr><td class="formLabel" colspan="2" align="center" style="padding-top: 8px;"><b>New thumbnail:</b></td></tr>
    <tr><td class="formLabel">Upload from file: </td><td><input type="radio" name="imgtype" value="file"></td></tr>
    <tr><td class="formLabel">Gallery thumbnail: </td><td><input style="font-size:12px;" type="file" name="userfile"></td></tr>
            <tr><td class="formLabel">Link to image: </td><td><input type="radio" name="imgtype" value="link" checked="checked"></td></tr>
            <tr><td class="formLabel">Thumbnail link [optional]: </td><td><input style="font-size:12px;" type="text" name="thumblink" value="'.$row[0].'"><INPUT class=formButton onclick="var val=openModalWin(\'?module=dialogs&action=filex&site_id='.$this->site_id.'&view=thumbs\', document.Form.thumblink.value, 600, 450); if (val) document.Form.thumblink.value=val;" type=button value=...></td></tr>';
    if($this->getProperty('showgaldesc'))
    {
      if(!$multilang)
      {
        echo '
            <tr><td class="formLabel">Description: </td><td>
            <textarea class="formEdit" style="width: 200px" name="description" rows="6" cols="30">'.$row[3].'</textarea>&nbsp;<input class="formButton" type="button" value="..." OnClick="
                             var val=openModalWin(\'?module=wysiwyg&canpreview=0&site_id='.$this->site_id.'&page_id='.$this->page_id.'\', document.Form.description.value, 720, 450, 1);
                             if (val) {
                               document.Form.description.value=val;
                             }">
            </td></tr>';
      }else
      {
        $description_uns = unserialize($row[3]);
        foreach($langs as $lang)
        {
          echo '
            <tr><td class="formLabel">Description ('.$lang.'): </td><td>
            <textarea class="formEdit" style="width: 200px" name="description_'.$lang.'" rows="6" cols="30">'.$description_uns[$lang].'</textarea>&nbsp;<input class="formButton" type="button" value="..." OnClick="
                             var val=openModalWin(\'?module=wysiwyg&canpreview=0&site_id='.$this->site_id.'&page_id='.$this->page_id.'\', document.Form.description_'.$lang.'.value, 720, 450, 1);
                             if (val) {
                               document.Form.description_'.$lang.'.value=val;
                             }">
            </td></tr>';
        }
      }
    }
    echo '
    <tr><td class="formLabel">Disabled: </td><td><input type="checkbox" name="disabled"'.($row[4]?' checked':'').'></td></tr>
            <tr>
</tr>
            <tr><td></td><td align="right"><!--<input style="font-size:12px;" type="submit" name="submit" value="Upload" onClick="javascript:window.close();">--></td></tr>
          </form>';
    echo '</table>';
}
#-- B : EDIT GALLERY --#

#-- S : SAVE EDITED GALLERY --#
function save_edited_gallery ()
{
  $this->save_gallery(true);
}
#-- B : SAVE EDITED GALLERY --#

#-- S : DELETE GALLERY --#
function del_gallery ()
{
  if (isset($_GET['galerijas_id']) && !empty($_GET['galerijas_id']))
  {
    $this->init_collections();
    $gal = $this->galleries->getItemByIdAssoc($_GET['galerijas_id']);
    //delete all pictures
    $pictures = $this->pictures->getPicturesForGallery($gal['item_id']);
    foreach($pictures as $pic)
    {
      $this->del_pic_files($pic);
      $this->pictures->DelDBItem($pic['item_id']);
    }

    //delete gallery thumb
    $imgdir = $this->galleries->GetName();
    $path = 'images/'.$imgdir.'/thumbs/';
    $this->del_pic_file($gal['thumb'], $path);

    $this->galleries->DelDBItem($_GET['galerijas_id']);
  }
}
#-- B : DELETE GALLERY --#


/*  PICTURE FUNCTIONS
*     * Add picture
*     * Add pictures
*     * Save picture
*     * Save pictures
*     * Move Up picture
*     * Move Down picture
*     * Delete picture
*     * Edit picture
*     * Save Edited gallery
*/

#-- S : ADD PICTURE --#
function add_pic()
{
  $multilang = $this->getProperty('multilingual');
  $langs = $this->ListLanguages();

    echo '<table align="center">';
    echo '<form name=Form enctype="multipart/form-data" action="?module=dialogs&action=component&site_id='.
		$this->site_id.'&page_id='.$this->page_id.'&component_name='.
		$this->name.'&component_type='.
		get_class($this).'&method_name=save_pic&galerijas_id='.$_GET['galerijas_id'].'&refresh=1&session_id=' . session_id() . '&session_name=' . session_name() . '" method="post">';
        if($this->getProperty('showphotographer'))
          echo '
            <tr><td class="formLabel">Photographer: </td><td><input type="text" name="photo"></td></tr>';
        if($this->getProperty('showphone'))
          echo '
            <tr><td class="formLabel">Phone: </td><td><input type="text" name="phone"></td></tr>';
        echo '
            <tr><td class="formLabel">Upload from file: </td><td><input type="radio" name="imgtype" value="file" checked></td></tr>
            <tr><td class="formLabel">Picture: </td><td><input style="font-size:12px;" type="file" name="userfile"></td></tr>
            <tr><td class="formLabel">Link to image: </td><td><input type="radio" name="imgtype" value="link"></td></tr>
            <tr><td class="formLabel">Image link: </td><td><input style="font-size:12px;" type="text" name="imglink"><INPUT class=formButton onclick="var val=openModalWin(\'?module=dialogs&action=filex&site_id='.$this->site_id.'&view=thumbs\', document.Form.imglink.value, 600, 450); if (val) document.Form.imglink.value=val;" type=button value=...></td></tr>
            <tr><td class="formLabel">Thumbnail link [optional]: </td><td><input style="font-size:12px;" type="text" name="thumblink"><INPUT class=formButton onclick="var val=openModalWin(\'?module=dialogs&action=filex&site_id='.$this->site_id.'&view=thumbs\', document.Form.thumblink.value, 600, 450); if (val) document.Form.thumblink.value=val;" type=button value=...></td></tr>
            <tr><td class="formLabel">Original image link [optional]: </td><td><input style="font-size:12px;" type="text" name="origlink"><INPUT class=formButton onclick="var val=openModalWin(\'?module=dialogs&action=filex&site_id='.$this->site_id.'&view=thumbs\', document.Form.origlink.value, 600, 450); if (val) document.Form.origlink.value=val;" type=button value=...></td></tr>';

        if($this->getProperty('showpictext'))
        {
          if(!$multilang)
          {
            echo '

              <tr>
    <td  width="30%" class="formLabel" style="white-space: nowrap;">
      Text:
    </td>
    <td  width="70%"class="formControl" style="white-space: nowrap;">
      <textarea class="formEdit" style="width: 200px" name="text" rows="6" cols="30"
                                      ></textarea>&nbsp;<input class="formButton" type="button" value="..." OnClick="
                               var val=openModalWin(\'?module=wysiwyg&canpreview=0&site_id='.$this->site_id.'&page_id='.$this->page_id.'\', document.Form.text.value, 720, 450, 1);
                               if (val) {
                                 document.Form.text.value=val;
                               }">
    </td>
  </tr>';
          }else
          {
            foreach($langs as $lang)
            {
              echo '

              <tr>
    <td  width="30%" class="formLabel" style="white-space: nowrap;">
      Text ('.$lang.'):
    </td>
    <td  width="70%"class="formControl" style="white-space: nowrap;">
      <textarea class="formEdit" style="width: 200px" name="text_'.$lang.'" rows="6" cols="30"
                                      ></textarea>&nbsp;<input class="formButton" type="button" value="..." OnClick="
                               var val=openModalWin(\'?module=wysiwyg&canpreview=0&site_id='.$this->site_id.'&page_id='.$this->page_id.'\', document.Form.text_'.$lang.'.value, 720, 450, 1);
                               if (val) {
                                 document.Form.text_'.$lang.'.value=val;
                               }">
    </td>
  </tr>';
            }
          }
    }
    echo '
<tr><td class="formLabel">Disabled: </td><td><input type="checkbox" name="disabled"></td></tr>
';
    echo '
            <tr><td></td><td align="right"><!--<input style="font-size:12px;" type="submit" name="submit" value="Upload" onClick="javascript:window.close();">--></td></tr>
          </form>';
    echo '</table>';
}
#-- B : ADD PICTURE --#


function ajax()
{
  if (empty($_GET['ajaxmethod'])) die('Method not defined!');

  $method = 'ajax_' . $_GET['ajaxmethod'];

  if (method_exists($this, $method))
  {
    $this->$method();
  }
  else
    die('Method not found!');

  die;
}

function ajax_uploadImage()
{
  $gal_id = intval($_GET['galerijas_id']);
  if (!$gal_id) return;

  $this->init_collections();

  foreach ($_FILES as $key => $f)
  {
    if (!$f['size']) continue;

    $this->save_pic(false, $f);
  }


}

#-- S : ADD PICTURES --#
function add_pics ()
{
?>
  <!-- jquery -->
  <script type="text/javascript" src="<?=$GLOBALS['cfgWebRoot'] ?>gui/jquery.min.js"></script>

  <!-- uploadify -->
  <link type="text/css" rel="stylesheet" href="/scr/components/uploadify/uploadify.css" />
  <script type="text/javascript" src="/scr/components/uploadify/jquery.uploadify.v2.1.0.min.js"></script>

  <!-- swfobject -->
  <script type="text/javascript" src="/scr/components/swfobject/swfobject.js"></script>

  <script type="text/javascript">


  var ajax_url = <?=json_encode('/cms/backend/'.$this->getComponentModifierLink('ajax')) ?>;
  var session_name = <?=json_encode(session_name()) ?>;
  var session_id = <?=json_encode(session_id()) ?>;

  $(function()
  {
    if (swfobject.hasFlashPlayerVersion("9.0.24"))
    {
      $('#upload_wrap').hide();

      var post = {};
      post[session_name] = session_id;

      $('#fupload').uploadify({
        'uploader' : '/scr/components/uploadify/uploadify-cbtn.swf',
        'script' : escape(ajax_url+'&ajaxmethod=uploadImage&galerijas_id=<?=$_GET["galerijas_id"] ?>'),
        'scriptData' : post,
        'cancelImg' : '/scr/components/uploadify/cancel.png',
        'auto' : true,
        'buttonText' : 'Add images',
        'width' : 142,
        'height' : 22,
        'rollover' : false,
        'wmode' : 'transparent',
        'multi' : true,
        'folder' : '/upload',

        'onComplete' : function(event, queueID, fileObj, response, data)
        {
//          $('#fdebug').html(response);
        },

        'onAllComplete' : function(e, data)
        {
          parent.onOk();
        }

      });

    }

  });

  </script>

  <div id="fdebug"></div>
  <div id="fupload"></div>
<?

  echo '<table cellpadding="2" cellspacing="2" id="upload_wrap">';
    echo '<form name=Form enctype="multipart/form-data" action="?module=dialogs&action=component&site_id='.
		$this->site_id.'&page_id='.$this->page_id.'&component_name='.
		$this->name.'&component_type='.
		get_class($this).'&method_name=save_pics&galerijas_id='.$_GET['galerijas_id'].'&refresh=1&session_id=' . session_id() . '&session_name=' . session_name() . '" method="post">
            <tr><td class="formLabel">Picture 1: </td><td><input style="font-size:12px; width: 260px" type="file" name="userfile1"></td></tr>
            <tr><td class="formLabel">Picture 2: </td><td><input style="font-size:12px; width: 260px" type="file" name="userfile2"></td></tr>
            <tr><td class="formLabel">Picture 3: </td><td><input style="font-size:12px; width: 260px" type="file" name="userfile3"></td></tr>
            <tr><td class="formLabel">Picture 4: </td><td><input style="font-size:12px; width: 260px" type="file" name="userfile4"></td></tr>
            <tr><td class="formLabel">Picture 5: </td><td><input style="font-size:12px; width: 260px" type="file" name="userfile5"></td></tr>
            <tr><td class="formLabel">Picture 6: </td><td><input style="font-size:12px; width: 260px" type="file" name="userfile6"></td></tr>
            <tr><td></td><td align="right"><!--<input style="font-size:12px;" type="submit" name="submit" value="Upload" onClick="javascript:window.close();">--></td></tr>
          </form>';
    echo '</table>';


}
#-- B : ADD PICTURES --#


#-- S : SAVE PICTURES --#
function save_pics ()
{
  //echo '<pre>';
  //print_r($_FILES);
  foreach ($_FILES as $file)
  {
    if (!empty($file['name']))
    {
      $this->save_pic(false,$file);
    }
  }
  //die();
}
#-- B : SAVE PICTURES --#

#-- S : SAVE PICTURE --#
function save_pic($edit=false,$pic=null)
{
  $multilang = $this->getProperty('multilingual');
  $langs = $this->ListLanguages();

  $this->init_collections();

  $photo = $_POST["photo"];
  $phone = $_POST["phone"];

  if(!$multilang)
  {
    $text =  $_POST["text"];
  }else
  {
    $text = Array();
    foreach($langs as $lang)
    {
      $text[$lang] = $_POST['text_' . $lang];
    }
    $text = serialize($text);
  }

  $imglink = $_POST['imglink'];
  $thumblink = $_POST['thumblink'];
  $origlink = $_POST['origlink'];

  $disabled = $_POST['disabled'];

  if ($_POST['imgtype']=="file" || $pic)
  {
    if ($pic)
    {
      $_FILES['userfile'] = $pic;
    }

    $imgdir = $this->pictures->GetName();
    $path = 'images/'.$imgdir.'/';

    forceDirectories($this->site_id, $path . 'thumbs/');
    $up_data = uploadSiteFile($this->site_id, $_FILES['userfile'], $path);

    if($up_data)
    {
      echo $up_data['url']."<br>";

      //create picture thumbnail
      $thumbdata = fileDuplicate($this->site_id, $up_data['path'], '', $path . 'thumbs/');
      $picthumbwidth  = $this->getProperty('picthumbwidth');
      $picthumbheight = $this->getProperty('picthumbheight');
      $picresizemode  = $this->getproperty('picthumbresizemode');
      resizeImage($thumbdata['path'], $picthumbwidth, $picthumbheight, $picresizemode);

      //copy original image image
      if($this->getProperty('keeporiginalpics'))
      {
        $origdata = fileDuplicate($this->site_id, $up_data['path'], 'big_');
      }else
      {
        $origdata = '';
      }

      //create resized image
      $imgdata = $up_data; //work with originally uploaded image
      $imgwidth  = $this->getProperty('imgwidth');
      $imgheight = $this->getProperty('imgheight');
      $imgresizemode  = $this->getproperty('imgresizemode');
      resizeImage($imgdata['path'], $imgwidth, $imgheight, $imgresizemode);

      //save changes to collection
      $properties=array($_GET['galerijas_id'], $thumbdata['url'], $imgdata['url'] , $photo,$phone,$text, $origdata['url'], $disabled);
      $ind = $this->addChangePicture($edit, $_GET['pic_id']);
      $this->pictures->ChangeItem($ind,$properties);
      print "File is valid, and was successfully uploaded. ";
    }else
    {
      print "File was not uploaded";
      $_GET['refresh'] = 0;
    }
  }
  else if ($_POST['imgtype']=="link")
  {
    if(!$thumblink)
      $thumblink = $imglink;
    $properties=array($_GET['galerijas_id'],$thumblink,$imglink,$photo,$phone,$text,$origlink,$disabled);
    $ind = $this->addChangePicture($edit, $_GET['pic_id']);
    $this->pictures->ChangeItem($ind,$properties);
  }
}
#-- B : SAVE PICTURE --#

function addChangePicture($edit, $pic_id)
{
  if($edit){
        $ind = $pic_id;
  }else
  {
    if(!$this->getProperty('pictureorder'))
    {
      $ind = $this->pictures->AddNewItemTop();
    }else
    {
      $ind = $this->pictures->AddNewItem();
    }
  }
  return $ind;
}



#-- S : MOVE PICTURE UP --#
function up_pic ()
{
  if (isset($_GET['pic_id']) && !empty($_GET['pic_id']))
  {
    $this->init_collections();
    $this->pictures->movePicUp($_GET['pic_id']);
  }
}
#-- B : MOVE PICTURE UP --#

#-- S : MOVE PICTURE DOWN --#
function down_pic()
{
  if (isset($_GET['pic_id']) && !empty($_GET['pic_id']))
  {
    $this->init_collections();
    $this->pictures->movePicDown($_GET['pic_id']);
  }
}
#-- B : MOVE PICTURE DOWN --#

#-- S : DELETE PICTURE --#
function del_pic ()
{
  //pic_id
   if (isset($_GET['pic_id']) && !empty($_GET['pic_id']))
  {
    $this->init_collections();
    $pic = $this->pictures->getItemByIdAssoc($_GET['pic_id']);
    $this->del_pic_files($pic);
    $this->pictures->DelDBItem($_GET['pic_id']);
  }
}
#-- B : DELETE PICTURE --#

function del_pic_files($pic)
{
  $imgdir = $this->pictures->GetName();
  $path = 'images/'.$imgdir.'/thumbs/';
  $this->del_pic_file($pic['thumb'], $path);
  $path = 'images/'.$imgdir.'/';
  $this->del_pic_file($pic['image'], $path);
  $this->del_pic_file($pic['origimage'], $path);
}

function del_pic_file($pic, $path)
{
  if(substr($pic, 1, strlen($path)) == $path)
  { //it is autouploaded image

    $count = $this->pictures->countTimesPictureUsed(AddSlashes($pic));
    $count = $count + $this->galleries->countTimesPictureUsed(AddSlashes($pic));

    if($count == 1)
    {
      $uploaddir = sqlQueryValue("select dirroot from sites where site_id=".$this->site_id);
      $picpath = $uploaddir . substr($pic, 1);
      unlink($picpath);
    }
  }
}

#-- S : EDIT PICTURE --#
function edit_pic ()
{
  $multilang = $this->getProperty('multilingual');
  $langs = $this->ListLanguages();
  $this->init_collections();
  $pic = $this->pictures->getItem($_GET['pic_id']);
    echo '<table align="center">';
    echo '<form name=Form enctype="multipart/form-data" action="?module=dialogs&action=component&site_id='.
		$this->site_id.'&page_id='.$this->page_id.'&component_name='.
		$this->name.'&component_type='.
		get_class($this).'&method_name=save_edited_pic&pic_id='.$_GET['pic_id'].'&galerijas_id='.$_GET['galerijas_id'].'&refresh=1&session_id=' . session_id() . '&session_name=' . session_name() . '" method="post">';
        if($this->getProperty('showphotographer'))
          echo '
            <tr><td class="formLabel">Photographer: </td><td><input type="text" name="photo" value="'.$pic[3].'"></td></tr>';
        if($this->getProperty('showphone'))
          echo '
            <tr><td class="formLabel">Phone: </td><td><input type="text" name="phone" value="'.$pic[4].'"></td></tr>';
        echo '
            <tr><td class="formLabel">Upload from file: </td><td><input type="radio" name="imgtype" value="file" checked></td></tr>
            <tr><td class="formLabel">Picture: </td><td><input style="font-size:12px;" type="file" name="userfile"></td></tr>
            <tr><td class="formLabel">Link to image: </td><td><input type="radio" checked="checked" name="imgtype" value="link"></td></tr>
            <tr><td class="formLabel">Image link: </td><td><input style="font-size:12px;" type="text" value="'.$pic[2].'" name="imglink"><INPUT class=formButton onclick="var val=openModalWin(\'?module=dialogs&action=filex&site_id='.$this->site_id.'&view=thumbs\', document.Form.imglink.value, 600, 450); if (val) document.Form.imglink.value=val;" type=button value=...></td></tr>
            <tr><td class="formLabel">Thumbnail link [optional]: </td><td><input style="font-size:12px;" type="text" name="thumblink" value="'.$pic[1].'"><INPUT class=formButton onclick="var val=openModalWin(\'?module=dialogs&action=filex&site_id='.$this->site_id.'&view=thumbs\', document.Form.thumblink.value, 600, 450); if (val) document.Form.thumblink.value=val;" type=button value=...></td></tr>
            <tr><td class="formLabel">Original image link [optional]: </td><td><input style="font-size:12px;" type="text" name="origlink" value="'.$pic[6].'"><INPUT class=formButton onclick="var val=openModalWin(\'?module=dialogs&action=filex&site_id='.$this->site_id.'&view=thumbs\', document.Form.origlink.value, 600, 450); if (val) document.Form.origlink.value=val;" type=button value=...></td></tr>';

        if($this->getProperty('showpictext'))
          if(!$multilang)
          {
            echo '
              <tr>
    <td  width="30%" class="formLabel" style="white-space: nowrap;">
      Text:
    </td>
    <td  width="70%"class="formControl" style="white-space: nowrap;">
      <textarea class="formEdit" style="width: 200px" name="text" rows="6" cols="30"
                                      >'.$pic[5].'</textarea>&nbsp;<input class="formButton" type="button" value="..." OnClick="
                               var val=openModalWin(\'?module=wysiwyg&canpreview=0&site_id='.$this->site_id.'&page_id='.$this->page_id.'\', document.Form.text.value, 720, 450, 1);
                               if (val) {
                                 document.Form.text.value=val;
                               }">
    </td>
  </tr>';
          }else
          {
            $text_uns = unserialize($pic[5]);
            foreach($langs as $lang)
            {
              echo '
              <tr>
    <td  width="30%" class="formLabel" style="white-space: nowrap;">
      Text ('.$lang.'):
    </td>
    <td  width="70%"class="formControl" style="white-space: nowrap;">
      <textarea class="formEdit" style="width: 200px" name="text_'.$lang.'" rows="6" cols="30"
                                      >'.$text_uns[$lang].'</textarea>&nbsp;<input class="formButton" type="button" value="..." OnClick="
                               var val=openModalWin(\'?module=wysiwyg&canpreview=0&site_id='.$this->site_id.'&page_id='.$this->page_id.'\', document.Form.text_'.$lang.'.value, 720, 450, 1);
                               if (val) {
                                 document.Form.text_'.$lang.'.value=val;
                               }">
    </td>
  </tr>';
            }
          }
    echo '
<tr><td class="formLabel">Disabled: </td><td><input type="checkbox" name="disabled"'.($pic[7] ? ' checked' : '').'></td></tr>
';
    echo '
            <tr><td></td><td align="right"><!--<input style="font-size:12px;" type="submit" name="submit" value="Upload" onClick="javascript:window.close();">--></td></tr>
          </form>';
    echo '</table>';
}
#-- B : SAVE GALLERY --#

#-- S : SAVE EDITED PICTURE --#
function save_edited_pic ()
{
  $this->save_pic(true);
}
#-- B : SAVE EDITED PICTURE --#

function ListLanguages()
{
  return sqlQueryColumn('SELECT shortname FROM `'.$this->site_id.'_languages` ORDER BY is_default DESC');
}

function FormatDate($date, $extended = 0)
  {
    if ($extended) return date("d.m.Y H:i:s",$date);
    else return date("d.m.Y",$date);
  }

//works like default php date() function, except this allows html tags to be used in dateforma
//for an example, it accepts 'd.<b>m</b>.Y'
function date_($dateformat,$date)
{
  preg_match_all ("/(<([\w]+)[^>]*>)(.*)(<\/\\2>)/", $dateformat, $matches);
  $d = $dateformat;
  if (count($matches))
  {
    $terms = preg_split('/(<([\w]+)[^>]*>)(.*)(<\/\\2>)/', $dateformat, -1, PREG_SPLIT_NO_EMPTY);
    foreach ($terms as $term)
    {
      $d = str_replace($term,date($term,$date),$d);
    }

    for ($i=0; $i< count($matches[0]); $i++) {
      if (!preg_match_all("/(<([\w]+)[^>]*>)(.*)(<\/\\2>)/", $matches[3][$i], $matches2))
      {
        $d = str_replace($matches[3][$i], date($matches[3][$i], $date),$d);
      } else
      {
        $d = str_replace($matches[3][$i],$this->date_($matches[3][$i], $date),$d);
      }
    }
  }
   return $d;
}

function init_collections()
{
    $this->galleries = $this->initPropertyCollection('galleriescollectionex');
    $this->pictures  = $this->initPropertyCollection('picturescollectionex');
}

  function toolbar(&$script, &$toolbars, &$btnids, &$onclick)
  {
    return True;
  }

  function CanLoadDesignDynamically()
  {
    return true;
  }

function SetProperties()
{
    $this->properties = Array(

    "name"        => Array(
      "label"     => "Name:",
      "type"      => "string"
      ),

    "type"      => Array(
      "label"     => "List type:",
      "type"      => "list",
      "lookup"    => Array("0:List galleries", "1:Single gallery", "3:Determine automatically"),
      "value"     => 3
      ),

    "listcontainer"    => Array(
        "label"   => "Gallery List Container template:",
        "type"    => "template",
        "rows"      => "8",
        "cols"      => "30",
        "tags"    => "{!gallerieslist}",
        "samples" => Array(
          "Default" => '<div class="galleries">{!gallerieslist}</div>'
        ),
        "value"   => '<div class="galleries">{!gallerieslist}</div>'
      ),

    "listitem"    => Array(
        "label"   => "Gallery List item template:",
        "type"    => "template",
        "rows"      => "8",
        "cols"      => "30",
        "tags"    => "{!id};{!galleryrow};{!thumbnail};{!hasthumb};{!title};{!description};{!date};{!pictures_count};{!path};{!num};{first};{last}",
        "samples" => Array(
          "Default" => '<div style="float:left;margin-right:20px">
  <div style="height:84px"><a href="{!path}"><img src="{!thumbnail}" alt=""/></a></div>
  <div>{!title} <span style="font-size:smaller">({!pictures_count})</span></div>
</div>'
        ),
        "value"   => '<div style="float:left;margin-right:20px">
  <div style="height:84px"><a href="{!path}"><img src="{!thumbnail}" alt=""/></a></div>
  <div>{!title} <span style="font-size:smaller">({!pictures_count})</span></div>
</div>'
    ),

    "dateformat"    => Array(
        "label"     => "{%langstr:date_format%}:",
        "type"      => "string"
      ),

    "gallery_path"    => Array(
        "label"     => "Gallery target link:",
        "type"      => "dialog",
        "dialog"    => "?module=dialogs&action=links&site_id=".$GLOBALS['site_id'],
    ),

    "piclistcontainer"    => Array(
        "label"   => "Pictures List Container template:",
        "type"    => "template",
        "rows"      => "8",
        "cols"      => "30",
        "tags"    => "{!pictureslist};{!gallerieslistpath};{!galleryrow};{!gal_id};{!gallerytitle};{!gallerydate};{!allpictures}",
        "samples" => Array(
                       "Default" => '<div class="gal_pictures">
  {!pictureslist}
  <div class="galpiclist_footer" style="padding-top:10px;clear:both"><a href="{!gallerieslistpath}">{%ls:uzgalsarakstu:&laquo; Uz galeriju sarakstu%}</a></div>
</div>'
        ),
        "value"   => '<div class="gal_pictures">
  {!pictureslist}
  <div class="galpiclist_footer" style="padding-top:10px;clear:both"><a href="{!gallerieslistpath}">{%ls:uzgalsarakstu:&laquo; Uz galeriju sarakstu%}</a></div>
</div>
'
      ),

    "piclistitem"    => Array(
        "label"   => "Pictures List item template:",
        "type"    => "template",
        "rows"      => "8",
        "cols"      => "30",
        "tags"    => "{!pic_id};{!thumbnail};{!link};{!text};{!bigpic};{!origpic};{!picturerow};{!num};{!first};{!last};{!comments_count}",
        "samples" => Array(
          'Default' => '<div style="float:left;margin-right:20px">
  <a href="{!bigpic}" rel="gallery" class="pic"><img src="{!thumbnail}" alt=""/></a>
</div>'
        ),
        "value"   => '<div style="float:left;margin-right:20px">
  <a href="{!bigpic}" rel="gallery" class="pic"><img src="{!thumbnail}" alt=""/></a>
</div>'
    ),

    "singlegallery"    => Array(
        "label"   => "Single Gallery template:",
        "type"    => "template",
        "rows"      => "8",
        "cols"      => "30",
        "tags"    => "{!gallerytitle};{!gallerydate};{!imglink};{!photographer};{!telephone};{!text};{!galleriesindex};{!galleryindex};{!nextpath};{!prevpath};{!galleryrow};{!gal_id};{!picturerow};{!pic_id};{!prevpicrow};{!first};{!nextpicrow};{!last};{!comments};{!comments_count}",
        "samples" => Array(
                       "Default" => "<p>{!gallerytitle}</p>
[if !prevpicrow]<a href=\"{!prevpath}\" onclick=\"{!prevonclick}\">[/if]Iepriekseja[if !prevpicrow]</a>[/if] [if !nextpicrow]<a href=\"{!nextpath}\" onclick=\"{!nextonclick}\">[/if]Nakama[if !nextpicrow]</a>[/if]<br/>
<img src=\"{!imglink}\"/>
<a href=\"{!galleryindex}\">Uz galerijas saakumu</a>
<a href=\"{!galleriesindex}\">Uz galeriju sarakstu</a>"),
        "value"   => "<p>{!gallerytitle}</p>
[if !prevpicrow]<a href=\"{!prevpath}\">[/if]Iepriekseja[if !prevpicrow]</a>[/if] [if !nextpicrow]<a href=\"{!nextpath}\">[/if]Nakama[if !nextpicrow]</a>[/if]<br/>
<img src=\"{!imglink}\"/>
<a href=\"{!galleryindex}\">Uz galerijas saakumu</a>
<a href=\"{!galleriesindex}\">Uz galeriju sarakstu</a>
[if !comments]<br /><br />{!comments}[/if]"
    ),

    "commentlistitem"    => Array(
        "label"   => "Comments single item template:",
        "type"    => "template",
        "rows"      => "8",
        "cols"      => "30",
        "tags"    => "{!pic_id};{!commentauthor};{!commenttext};{!commenttime}",
        "samples" => Array(
                       "Default" => "<b>{!commentauthor}</b> - {!commenttime}<br />{!commenttext}<br /><br />"),
        "value"   => "<b>{!commentauthor}</b> - {!commenttime}<br />{!commenttext}<br /><br />"
    ),



    "galcomments"    => Array(
        "label"   => "Comments wrapper template:",
        "type"    => "template",
        "rows"      => "8",
        "cols"      => "30",
        "tags"    => "{!pic_id};{!gal_id};{!commentslist};{!gallerytitle};{!hiddenfield}",
        "samples" => Array(
                      ),
        "value"   => '{!commentslist}
        <form method="post" action="?galid={!gal_id}&picture_id={!pic_id}&action=addcomment" name="gallerycommentform" class="form">
            <table>
            <tr>
              <td><b>Autors:</b></td>
              <td><input type="text" name="author" value=""></td>
            </tr>
            <tr>
              <td valign="top"><b>Komentārs:</b></td>
              <td><textarea name="comment"></textarea></td>
            </tr>
            <tr>
              <td colspan="2" align="right"><input type="submit" value="Pievienot"></td>
            </tr>
            </table>{!hiddenfield}
          </form>'
    ),


    "galleries_list_path"    => Array(
        "label"     => "Galleries list target link:",
        "type"      => "dialog",
        "dialog"    => "?module=dialogs&action=links&site_id=".$GLOBALS['site_id'],
    ),

    "galthumbwidth"      => Array(
      "label"     => "Gallery thumbnail width:",
      "type"      => "string",
      "value"     => "100",
      ),

    "galthumbheight"      => Array(
      "label"     => "Gallery thumbnail height:",
      "type"      => "string",
      "value"     => "100",
      ),

    "galthumbresizemode" => Array(
      "label"     => "Gal thumb resize mode:",
      "type"      => "list",
      "lookup"    => Array("1:By width", "2:By height", "3:By width if too big", "4:By height if too big", "5:By width and height", "6:By width and height if too big", "7:Crop by width and height"),
      "value"     => "1",
    ),

    "picthumbwidth"      => Array(
      "label"     => "Picture thumbnail width:",
      "type"      => "string",
      "value"     => "100",
      ),

    "picthumbheight"      => Array(
      "label"     => "Picture thumbnail height:",
      "type"      => "string",
      "value"     => "100",
      ),

    "picthumbresizemode" => Array(
      "label"     => "Pic thumb resize mode:",
      "type"      => "list",
      "lookup"    => Array("1:By width", "2:By height", "3:By width if too big", "4:By height if too big", "5:By width and height", "6:By width and height if too big", "7:Crop by width and height"),
      "value"     => "1",
    ),

    "imgwidth"      => Array(
      "label"     => "Picture width:",
      "type"      => "string",
      "value"     => "1280",
      ),

    "imgheight"      => Array(
      "label"     => "Picture height:",
      "type"      => "string",
      "value"     => "960",
      ),

    "imgresizemode" => Array(
      "label"     => "Picture resize mode:",
      "type"      => "list",
      "lookup"    => Array("1:By width", "2:By height", "3:By width if too big", "4:By height if too big", "5:By width and height", "6:By width and height if too big", "7:Crop by width and height"),
      "value"     => "1",
    ),

    "galleriescollectionex" => Array(
      "label"       => "Galleries collection:",
      "type"        => "collection",
      "collectiontype" => "galleriescollectionex",
      "lookup"      => GetCollections($this->site_id, "galleriescollectionex"),
      "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
      "dialogw"     => "600"
    ),

    "picturescollectionex" => Array(
      "label"       => "Galleries Pictures collection:",
      "type"        => "collection",
      "collectiontype" => "picturescollectionex",
      "lookup"      => GetCollections($this->site_id, "picturescollectionex"),
      "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
      "dialogw"     => "600"
    ),

    "commentcollection" => Array(
      "label"       => "Comments collection:",
      "type"        => "collection",
      "collectiontype" => "newscommentscollection",
      "lookup"      => GetCollections($this->site_id, "newscommentscollection"),
      "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
      "dialogw"     => "600"
    ),

    "showgaldesc"    => Array(
        "label"     => "Show gallery description:",
        "type"      => "list",
        "lookup"    => Array("0:hidden", "1:visible")
      ),

    "showphotographer"    => Array(
        "label"     => "Show photographer:",
        "type"      => "list",
        "lookup"    => Array("0:hidden", "1:visible")
      ),

    "showphone"    => Array(
        "label"     => "Show phone:",
        "type"      => "list",
        "lookup"    => Array("0:hidden", "1:visible")
      ),

    "showpictext"    => Array(
        "label"     => "Show picture text:",
        "type"      => "list",
        "lookup"    => Array("0:hidden", "1:visible")
      ),

    "keeporiginalpics"    => Array(
        "label"     => "Keep original images:",
        "type"      => "list",
        "lookup"    => Array("0:No", "1:Yes")
      ),

    "galleryorder"    => Array(
        "label"     => "Gallery order:",
        "type"      => "list",
        "lookup"    => Array("0:Newest first", "1:Newest last")
      ),

    "pictureorder"    => Array(
        "label"     => "Picture order:",
        "type"      => "list",
        "lookup"    => Array("0:Newest first", "1:Newest last"),
        "value"     => 1
      ),

    "multilingual"    => Array(
        "label"     => "Languages:",
        "type"      => "list",
        "lookup"    => Array("0:Single language", "1:Multiple languages"),
      ),

      'galstep' => array(
        'label' => 'Gallery item template step',
        'type' => 'str',
        'value' => '1'
      )


    );

}

}
?>
