<?php
/**
*
*  Title: Soc. icons
*  Author: Kristaps Taube <ktaube@datateks.lv>
*  Date: 15.01.2014
*  Project: E-Veikals
*
*/

include_once("class.component.php");

class EVeikalsSocIcons extends component{

  function __construct($name){
    parent::__construct($name);
    $this->SetProperties();
    $this->registerAsService("SocIcons");
  }

  function output(){

    if(!$this->initCollections()){
      echo $this->name.": Can't init collections.<br />";
      return;
    }

    if($this->getProperty("position") == 0){
      $this->output_top();
    }else{
      $this->output_bottom();
    }

  }

  function output_bottom(){

    $icons = $this->icons->getBottomIcons();

    if(!$icons) return;

    ?>
      <div class='title'><?=$this->l("Seko mums")?>:</div>
      <? foreach($icons as $icon){ ?>
        <a href='<?=$icon['link']?>' target='_blank' title="<?=hsc($icon['title'])?>"><img src='<?=$icon['icon_bottom']?>' alt='<?=hsc($icon['title'])?>' /></a>
      <? } ?>
    <?

  }

  function output_top(){

    $icons = $this->icons->getTopIcons();

    if(!$icons) return;

    foreach($icons as $icon){
    ?>
      <a href='<?=$icon['link']?>' target='_blank' title="<?=hsc($icon['title'])?>"><img src='<?=$icon['icon_top']?>' alt='<?=hsc($icon['title'])?>' /></a>
    <?
    }

  }

  function SetProperties(){
    $this->properties = Array(

      "name"        => Array(
        "label"     => "Name:",
        "type"      => "str"
      ),

      "icons" => Array(
        "label"       => "Ikonas:",
        "type"        => "collection",
        "collectiontype" => "EVeikalsSocIconCollection",
        "lookup"      => GetCollections($this->site_id, "EVeikalsSocIconCollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      'position' => Array(
        'label'     => 'Novietojums:',
        'type'      => 'list',
        'lookup'    => Array('0:Augšā', '1:Apakšā'),
      ),

    );
  }



}



?>