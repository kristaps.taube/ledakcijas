<?php
/**
*
*  Title: Auth. checker
*  Author: Kristaps Taube <ktaube@datateks.lv>
*  Date: 05.02.2015
*  Project: EVeikals
*
*/

include_once("class.component.php");

class EVeikalsAuthChecker extends component{

    public function __construct($name)
    {

        parent::__construct($name);

        if($this->secured && !$_SESSION['logged_in']){
            header("location: /".$this->lang);
            die();
        }

    }

    function output(){

    }

    function SetProperties(){

        $this->properties = Array(

            "name"        => Array(
                "label"     => "Name:",
                "type"      => "str"
            ),

            "secured" => array(
                "label" => "Secure",
                "type" => "check"
            )

        );

    }

}
