<?php
/**
*
*  Title: Catalog Search
*  Author: Kristaps Taube <ktaube@datateks.lv>
*  Date: 24.01.2013
*  Project: E-Veikals
*
*/


class EVeikalsCatalogSearch extends component{

  function __construct($name){
    parent::__construct($name);
    $this->SetProperties();
    $this->registerAsService("CatalogSearch");

    $this->needle = isset($_GET['search']) && $_GET['search']? $_GET['search'] : null;
    $this->search = isset($_GET['search']) ? true : false;

  }

  function getAutoCompleteData($query, $category){

    $cond = array("shortcut = 0 AND disabled = 0"); // no shortcuts please!
    $results = array();

    $disabled_cat_ids = $this->cats->getInactiveCatIds();
        if(!empty($disabled_cat_ids)){
        $cond[] = "category_id NOT IN(".implode(",", $disabled_cat_ids).")";
    }

    if($category){

      $ids = $this->cats->getAllSubCategories($category);
      $ids[] = $category;

      if(!empty($ids)){
        $cond[] = "category_id IN(".implode(",", $ids).")";
      }

    }

    $search_cond = $params = array();
    foreach($this->Shop->autocomplete_search_in as $in){
      $search_cond[] = "lower(".$in.") LIKE :".$in;
      $params[":".$in] = '%'.strtolower($query).'%';
    }

    if(!empty($search_cond)){
      $cond[] = "(".implode(" OR ", $search_cond).")";
    }

    $prods = DB::GetTable("
      SELECT ".implode(",", $this->Shop->search_in)."
      FROM `".$this->prods->table."`
      WHERE ".implode(" AND ", $cond)."
      ORDER BY name_".$this->lang."
      LIMIT 100
    ", $params);

    foreach($prods as $prod){

    	foreach($prod as $field){

      	if(strpos(strtolower($field), strtolower($query)) !== false){
      		$results[] = $field;
      	}

    	}

    }

    return array("suggestions" => $results);

  }

  function output_cat_tree_option($cat){

    $pre = str_repeat("&nbsp;&nbsp;", $cat['lvl']);

    echo "<option value='".$cat['item_id']."' ".(($cat['item_id'] == $this->manager->cat_id)? "selected='selected'" : "").">".$pre.$cat['title_'.$this->lang]."</option>";

    if($cat['kids']){
      foreach($cat['kids'] as $kid){
        $this->output_cat_tree_option($kid);
      }
    }

  }

  function output(){

    $tree = $this->cats->getCategoryTree();

    ?>
    <form method='post' action='<?php echo Constructor\Url::get('search') ?>' data-autocomplete-url='<?php echo Constructor\Url::get('search') ?>?action=autocomplete' id='SearchForm'>
      <select name='category' class='styled' title="<?=$this->l("Visas kategorijas")?>">
        <option value='0'><?=$this->l("Visas kategorijas")?></option>
        <? foreach($tree as $cat){ $this->output_cat_tree_option($cat); } ?>
      </select> 
      <label for='search_needle'><?=$this->l("Meklēt")?>:</label>
      <div class='fl' id="search_needle_wrap"><input type='text' name='needle' value='<?=htmlspecialchars($this->needle, ENT_QUOTES)?>' id='search_needle' /></div>
      <input type='submit' name='CatalogSearch' value='<?=$this->l("Meklēt")?>' />
      <input type='hidden' name='CatalogSearchTrigger' value='' />
    </form>
    <script>
        intCustomSelects();
    </script>
    <?
  }

  function SetProperties(){
    $this->properties = Array(

      "name"        => Array(
        "label"     => "Name:",
        "type"      => "str"
      ),

      "cats" => Array(
        "label"       => "Category collection:",
        "type"        => "collection",
        "collectiontype" => "shopcatcollection",
        "lookup"      => GetCollections($this->site_id, "shopcatcollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "prods" => Array(
        "label"       => "Product collection:",
        "type"        => "collection",
        "collectiontype" => "shopprodcollection",
        "lookup"      => GetCollections($this->site_id, "shopprodcollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

    );
  }



}

