<?php
/**
*
*  Title: Import
*  Author: Kristaps Taube <ktaube@datateks.lv>
*  Date: 19.02.2017
*  Project: LEDAkcijas
*
*/

use Constructor\WebApp;
use LEDAkcijas\CategoryAmountStepCollection;
use LEDAkcijas\ProductAmountPriceCollection;

require_once($GLOBALS['cfgDirRoot'].'library/PHPExcel.php');
require_once($GLOBALS['cfgDirRoot'].'library/PHPExcel/IOFactory.php');
require_once($GLOBALS['cfgDirRoot'].'library/class.excelChunkReadFilter.php');

class LEDAkcijasImport extends component
{

    public function design()
    {

        if(isset($_POST['import'])){
            $response = $this->processImport($_POST['category']);
            echo $response;
        }

        ?>
        <form method='post' action='' enctype="multipart/form-data">
            <div style='padding: 3px 0px;'>
                Kategorija:
                <select name='category'>
                    <?php foreach($this->categories->getDBData() as $cat){ ?>
                    <option value='<?=$cat['item_id']?>'><?php echo $cat['title_lv']?></option>
                    <?php } ?>
                </select>
            </div>
            <div style='padding: 3px 0px;'>
                Fails: <input type='file' name='bulbs' />
            </div>
            <div style='padding: 3px 0px;'>
                <input type='submit' name='import' value='Importēt' />
            </div>
        </form>
        <?php

    }

    private function processImport($category)
    {

        if(isset($_FILES['bulbs']) && !$_FILES['bulbs']['error'] && $_FILES['bulbs']['size']){

            $destination = WebApp::$app->getConfigValue('temp_folder')."bulbs_".$_FILES['bulbs']['name'];
            move_uploaded_file($_FILES['bulbs']['tmp_name'], $destination);

            $data = $this->getReaderData($destination);

            foreach($data as $key => $arr){
                if(!array_filter(array_values($arr))){// remove empty lines
                    unset($data[$key]);
                }
            }
            $data = array_values($data); // re-index

            $this->processData($data, $category);

            #echo "<pre>".print_r($data, true)."</pre>";

        }else{

            return 'Kļūda mēģiniet vēlreiz.(CODE 1)';

        }

    }

    private function clearData()
    {

        $this->products->Truncate();
        $this->product_filter_rel->Truncate();
        $this->amount_prices->Truncate();
        $this->images->Truncate();

    }

    private function clearFilterData()
    {

        $this->product_filter_rel->Truncate();
        $this->cat_filter_rel->Truncate();
        $this->filter_groups->Truncate();
        $this->filters->Truncate();
        $this->link_filter_rel->Truncate();

    }

    private function processData($data, $category)
    {

        $site = WebApp::$app->getSite();

        $data = $this->prepareData($data);

        #echo "<pre>".print_r($data, true)."</pre>";
        #return;

        // clear data
        # $this->clearData();
        # $this->clearFilterData();

        $category_id = $category;

        $langs = getLanguages();

        $step_ids = [];

        foreach($data as $entry){

            $check = $this->products->getByCode($entry['Nosaukums']['SKU kods']);

            $prod_data = [];

            // common changes
            $prod_data['price'] = $entry['Cenas']['Akcijas cena ar PVN'];
            if($entry['Cenas']['Superakcijas cena ar PVN'] > 0){
                $prod_data['featured'] = 1;
                $prod_data['sale_price'] = $entry['Cenas']['Superakcijas cena ar PVN'];
            }

            $prod_data['ean'] = isset($entry['Apraksts']['EAN kods']) ? $entry['Apraksts']['EAN kods'] : '';

            $prod_data['disabled'] = 0;
            // status
            if($entry['Preces statuss'] == 1){
                $prod_data['available_in_shop'] = 1;
                $prod_data['available_sample'] = 0;
            }elseif($entry['Preces statuss'] == 2){
                $prod_data['available_in_shop'] = 0;
                $prod_data['available_sample'] = 1;
            }elseif($entry['Preces statuss'] == 3){
                $prod_data['available_in_shop'] = 0;
                $prod_data['available_sample'] = 0;
            }elseif($entry['Preces statuss'] == 4){
                $prod_data['available_in_shop'] = 0;
                $prod_data['available_sample'] = 0;
                $prod_data['disabled'] = 1;
            }

            if($check){ // existing product

                $this->products->Update($prod_data, $check['item_id']);
                $id = $check['item_id'];

            }else{ // new product

                $prod_data['code'] = $entry['Nosaukums']['SKU kods'];
                foreach($langs as $short => $lang){
                    $prod_data['name_'.$short] = $entry['Nosaukums']['Nosaukums'];

                    $descriptions = [];
                    if(isset($entry['Specifiskie filtri']) && is_array($entry['Specifiskie filtri'])){
                        foreach(array_merge($entry['Specifiskie filtri'], $entry['Apraksts']) as $key => $value){
                            $descriptions[] = $key.": ".$value;
                        }
                    }

                    $prod_data['description_'.$short] = implode("<br />", $descriptions);
                }

                $prod_data['category_id'] = $category_id;

                $id = $this->products->Insert($prod_data);

                // images
                $image_web_path = '/images/import/'.$prod_data['code'].'.jpg';
                $image_path = str_replace('//', '/', $site['dirroot'].$image_web_path);
                if(is_file($image_path)){

                    $this->products->Update(['picture' => $image_web_path], $id);
                    $this->images->Insert([
                        'product_id' => $id,
                        'image' =>  $image_web_path
                    ]);

                }

            }

            $this->processProductAmountPrices($id, $entry, $category_id); // amount prices
            $this->processFilters($id, $entry, $category_id); // filters

        }

        #echo "<pre>".print_r($data, true)."</pre>";

    }

    private function getCategoryFilterGroup($category)
    {
        #
        static $cache;

        if(!$cache){

            $result = $this->cat_filter_rel->getImportedFilterGroup($category);

            if(!$result){

                $data = ['imported' => 1];
                $cat = $this->categories->getById($category);

                foreach(getLanguages() as $short => $lang){
                    $data['title_'.$short] = $cat['title_'.$short].' filtri';
                }

                $cache = $this->filter_groups->Insert($data);
                $this->cat_filter_rel->Insert([
                    'category_id' => $category,
                    'filter_group_id' => $cache
                ]);

            }else{
                $cache = $result;
            }

        }

        return $cache;

    }

    private function processFilters($id, $entry, $category)
    {

        static $cats;

        if(!isset($cats[$category])){
            $cat = $this->categories->getById($category);
            $cats[$category] = $cat;
        }else{
            $cat = $cats[$category];
        }

        static $cat_filter_groups;

        if(!isset($cat_filter_groups[$category])){
            $cat_filter_groups[$category] = $this->cat_filter_rel->getGroupIds($category);
        }

        $general_filter_group = 1;

        $prod_filter_ids = $this->product_filter_rel->getFilterIds($id);

        $filters = [];
        if(isset($entry['Filtri'])){
            $filters = array_merge($filters, $entry['Filtri']);
        }
        if(isset($entry['filtri'])){
            $filters = array_merge($filters, $entry['filtri']);
        }

        /*if(isset($entry['Specifiskie filtri'])){
            $filters = array_merge($filters, $entry['Specifiskie filtri']);
        }*/

        if($filters){
            foreach($filters as $key => $value){

                if(!$value) continue;

                $filter_group_id = $this->filter_groups->addIfNotExist($key, $key."(".$cat['title_lv'].")", $cat['item_id']);

                if(!in_array($filter_group_id, $cat_filter_groups[$category])){
                    $this->cat_filter_rel->Insert([
                        'category_id' => $category,
                        'filter_group_id' => $filter_group_id
                    ]);
                    $cat_filter_groups[$category][] = $filter_group_id;
                }

                $filter_id = $this->getFilterId($value, $filter_group_id);

                if(!in_array($filter_id, $prod_filter_ids)){
                    $this->product_filter_rel->Insert(['product_id' => $id, 'filter_id' => $filter_id]);
                }

            }
        }

    }

    private function getFilterId($filter, $category)
    {

        static $cache;

        if(!isset($cache[$filter][$category])){
            $cache[$filter][$category] = $this->filters->addIfNotExist($filter, $category);
        }

        return $cache[$filter][$category];
    }

    private function processProductAmountPrices($id, $entry, $category_id)
    {

        static $deleted_missing;

        $steps = [];
        $step_prices = [];
        foreach($entry['Cenas'] as $cena => $value){
            if(mb_substr($cena, 0, mb_strlen('Pērkot')) == 'Pērkot'){ // a step

                $parts = explode("-", $cena);

                $from = filter_var($parts[0], FILTER_SANITIZE_NUMBER_INT);
                $to = filter_var($parts[1], FILTER_SANITIZE_NUMBER_INT);

                $steps[] = [$from, $to];

                $step_id = isset($step_ids[$from.'-'.$to]) ? $step_ids[$from.'-'.$to] : $this->steps->getStepId($from, $to, $category_id);
                $step_ids[$from.'-'.$to] = $step_id;
                $step_prices[$step_id] = $value;
            }
        }

        if(!$deleted_missing){ // only once per import. ty
            $this->steps->deleteMissing($steps, $category_id);
            $deleted_missing = true;
        }

        $prod_amount_prices = $this->amount_prices->getByProduct($id);
        foreach($step_prices as $step_id => $price){

            if(isset($prod_amount_prices[$step_id])){ // existing price entry
                if($prod_amount_prices[$step_id]['price'] != $price){
                    $this->amount_prices->Update(['price' => $price], $prod_amount_prices[$step_id]['item_id']);
                }
            }else{ // new price entry
                $eid = $this->amount_prices->Insert(['category_step_id' => $step_id, 'price' => $price, 'product_id' => $id]);
            }

        }

    }

    private function prepareData($data)
    {

        $header1 = $data[0];
        $header2 = $data[1];
        unset($data[0]);
        unset($data[1]);

        $result = [];

        foreach($data as $row){

            $h1 = $header1['A'];
            $h2 = $header2['A'];

            $result_row = [];

            foreach($row as $key => $value){

                $h1 = isset($header1[$key]) ? ucfirst($header1[$key]) : $h1;
                $h2 = ucfirst(trim($header2[$key]));

                if(!isset($result_row[$h1]) && $h1) $result_row[$h1] = [];

                if($h1 && $h2){
                    $result_row[$h1][$h2] = $value;
                }elseif($h2){
                    $result_row[$h2] = $value;
                }

            }

            $result[] = $result_row;

        }

        return $result;

    }

    private function getReaderData($file, $from = false, $chunk = false)
    {

        $objReader = PHPExcel_IOFactory::createReader('Excel2007');
        $objReader->setReadDataOnly(true);

        if($from && $chunk){
            $chunkFilter = new excelChunkReadFilter();
            $objReader->setReadFilter($chunkFilter);
            $chunkFilter->setRows($from, $chunk);
        }

        try{
            $objPHPExcel = $objReader->load($file);
            $data = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
        } catch (Exception $e) {
            $this->endWithError("Can't read file: ".$file.". Error: ".$e->getMessage());
        }

        // Free up some of the memory
        $objPHPExcel->disconnectWorksheets();
        unset($objPHPExcel);

        return $data;

    }

    public function addProperties()
    {

        return [
            'categories' => [
                'label'       => 'Categories:',
                'type'        => 'collection',
                'collectiontype' => 'shopcatcollection',
            ],
            'products' => [
                'label'       => 'Products:',
                'type'        => 'collection',
                'collectiontype' => 'shopprodcollection',
            ],
            'filter_groups' => [
                'label'       => 'Filter groups:',
                'type'        => 'collection',
                'collectiontype' => 'EVeikalsFilterGroups',
            ],
            'filters' => [
                'label'       => 'Filters:',
                'type'        => 'collection',
                'collectiontype' => 'EVeikalsFilters',
            ],
            'product_filter_rel' => [
                'label'       => 'Prod. filter relations:',
                'type'        => 'collection',
                'collectiontype' => 'EVeikalsProdFilterRel',
            ],
            'steps' => [
                'label'       => 'Steps:',
                'type'        => 'collection',
                'collectiontype' => 'LEDAkcijas\\CategoryAmountStepCollection',
            ],
            'amount_prices' => [
                'label'       => 'Amount prices:',
                'type'        => 'collection',
                'collectiontype' => 'LEDAkcijas\\ProductAmountPriceCollection',
            ],
            'images' => [
                'label'       => 'Images:',
                'type'        => 'collection',
                'collectiontype' => 'shopcustomimagescollection',
            ],
            'cat_filter_rel' => [
                'label'       => 'Cat filter rel:',
                'type'        => 'collection',
                'collectiontype' => 'EVeikalsCatFilterGroupRel',
            ],
            'link_filter_rel' => [
                'label'       => 'Link filter rel:',
                'type'        => 'collection',
                'collectiontype' => 'LedAkcijasLinkFiltersCollection',
            ],
        ];

    }

}