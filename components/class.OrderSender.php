<?php
/**
*
*  Title: OrderSender
*  Author: Kristaps Taube <ktaube@datateks.lv>
*  Date: 08.03.2018
*  Project: LEDAkcijas.lv
*
*/

use Constructor\WebApp;

class OrderSender extends component
{

    private $_fields;

    public function __construct($name = false)
    {

        parent::__construct($name);
        $this->init();

    }

    public function sendToClient($order)
    {

        $body = str_replace('[ORDER_NUM]', $order['num'], $this->mail_body);

        $path = $this->createPDF($order); // do we really have to create PDF twice?
        $attachs = [$path];

        phpMailerSend($body, $this->client_subject, $this->sender_name, $this->sender_email, $order['epasts'], true, false, $attachs);

    }

    public function sendToAdmin($order)
    {

        $body = str_replace('[ORDER_NUM]', $order['num'], $this->admin_body);

        $path = $this->createPDF($order); // do we really have to create PDF twice?
        $attachs = [$path];

        foreach(explode(",", $this->orderreciever) as $admin_email){
            phpMailerSend($body, $this->admin_subject, $this->sender_name, $this->sender_email, $admin_email, true, false, $attachs);
        }

    }

    private function createPDF($order)
    {

        require_once(WebApp::$app->getConfigValue('dir_root')."/library/mPDF/mpdf.php");
        $mpdf = new \mPDF();
        $mpdf->WriteHTML($order['rekins']);

        $path = $this->order_path.transliterateURL($order['num']).'.pdf';

        $mpdf->Output($path, "F");

        return $path;

    }

    private function init()
    {

        option('Shop\\OrderForm', '', 'Order form');
        $optcat = getOptionByPath('Shop\\OrderForm');
        $params = Array('parent_id' => $optcat['id']);

        $this->sender_email = option('emailfrom_email', null, 'Send order from e-mail', $params, 'constructor@datateks.lv');
        $this->sender_name = option('emailfrom_name', null, 'Send order from name', $params, 'Constructor');

        $this->client_subject = option('mail_subject', null, 'Client mail subject', $params, 'Rekins');
        $this->client_body = option('mail_body', null, 'Client mail body', array_merge(
            $params, [
                "type" => "wysiwyg",
                "value_on_create" => "Labdien,<br />Jūsu pasūtījums ar numuru [ORDER_NUM] ir saņemts. Rēķins pievienots vēstulei.<br /><br />Ar cieņu,<br />Administrācija",
            ]
        ));

        $this->orderreciever = $this->orderemail = option('orderemail', null, 'Send order to e-mail(seperated by ,)', $params, 'constructor@datateks.lv');
        $this->admin_subject = option('admin_subject', null, 'Admin mail subject', $params, 'Jauns rekins');
        $this->admin_body = option('admin_body', null, 'Admin mail body', array_merge(
            $params, [
                "type" => "wysiwyg",
                "value_on_create" => "Labdien,<br />Saņemts jauns pasūtījums ar numuru [ORDER_NUM]. Rēķins pievienots vēstulei.<br /><br />Ar cieņu,<br />Administrācija",
            ]
        ));

        $this->order_path = WebApp::$app->getSite()['dirroot'].'files/rekini/';

    }

    public function addProperties()
    {

        return [
            "orders" => [
                "label"       => "Order collection:",
                "type"        => "collection",
                "collectiontype" => "EVeikalsOrderCollection",
            ],
            "order_products" => [
                "label"       => "Order product collection:",
                "type"        => "collection",
                "collectiontype" => "shoporderprodcollection",
            ],
            "statuses" => [
                "label"       => "Order status collection:",
                "type"        => "collection",
                "collectiontype" => "EVeikalsOrderStatuses",
            ]
        ];

    }

}