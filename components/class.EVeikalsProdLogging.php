<?php
/**
*
*  Title: [EVeikals product change logger]
*  Author: Jānis Polis <j.polis@datateks.lv>
*  Date: [05.07.2016]
*  Project: [EVeikals]
*
*/

class EVeikalsProdLogging extends component
{

    function __construct($name)
    {
        parent::__construct($name);
    }

    function output(){}
    function design(){}

    function addProperties()
    {

        return [
            "change_log" => [
                "label"       => "Product change log collection:",
                "type"        => "collection",
                "collectiontype" => "EVeikalsProdChangeLog",
            ]
        ];

    }

}