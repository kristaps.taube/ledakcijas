<?
/**
*
*  Title: Contact form
*  Author: Kristaps Taube <ktaube@datateks.lv>
*  Date: 15.01.2014
*  Projet: E-Veikals
*
*/

include_once("class.component.php");

class EVeikalsContactForm extends component{

  function __construct($name){
    parent::__construct($name);
    $this->SetProperties();
  }

  function initFields(){

    $this->fields = Array(
      Array(
        "name" => "name",
        "type" => "text",
        "label" => $this->l("Vārds, uzvārds"),
        "required" => true,
      ),
      Array(
        "name" => "email",
        "type" => "text",
        "label" => $this->l("E-pasts"),
        "required" => true,
				"email" => true
      ),
      Array(
        "name" => "comment",
        "type" => "textarea",
        "label" => $this->l("Tavs jautājums vai komentārs"),
        "required" => true,
      ),
    );

  }

  function getFields(){

    return $this->fields;

  }

  function execute(){

    $this->initCollections();

    if(!empty($_POST['SendContactForm'])){
      $errors = $this->processForm($_POST);

      if(empty($errors)){

        echo json_encode(array("status" => 1, "msg" => "<div class='msg'>".$this->l("Vēstule nosūtīta")."</div>"));

      }else{

        echo json_encode(array("status" => 0, "msg" => "<div class='errors'>" . print_r($errors['msg'], true) . "</div>", "fields" => $errors['fields']));

      }

      die();

    }

  }

  function validateForm($data){

    $fields = $this->getFields();
    $errors = Array();

    foreach($fields as $field){

      if($field['required'] && empty($data[$field['name']])){
        $errors['msg'][] = $this->l("Empty field").": ".ucfirst($this->l($field['name']));
				$errors['fields'][] = $field['name'];
      }elseif($field['email'] && !filter_var($data[$field['name']], FILTER_VALIDATE_EMAIL)){
       	$errors['msg'][] = $this->l("Incorrect e-mail");
				$errors['fields'][] = $field['name'];
     	}

    }

    if(!$_SESSION['captcha'] || $_SESSION['captcha'] != $data['security_code']){
      $errors['msg'][] = $this->l("Invalide security code");
      $errors['fields'][] = "security_code";
    }

    return $errors;

  }

  function processForm($data){

    $errors = $this->validateForm($data);

    if(empty($errors)){

      $text = "<h1>No kontakformas</h1>";

      $fields = $this->getFields();
      foreach($fields as $field){
        $text .= "<b>".$field['label']."</b>: ".$data[$field['name']]."<br />";
      }

      $text .= "<br />======================<br/>";
      $text .= date("d.m.Y H:i:s")."<br />";
      $text .= $_SERVER['REMOTE_ADDR']."<br/>";

      $email = $this->getProperty("email");
      $email = $email? $email : option("admin_emailaddress", null, "Admin e-mail", array("value_on_create" => "ktaube@datateks.lv"));

      $domain = (substr($_SERVER['SERVER_NAME'], 0, 4) == "www.")? substr($_SERVER['SERVER_NAME'], 4) : $_SERVER['SERVER_NAME'];

      $subject = option("ContactForm\\subject", null, "Subject", array("value_on_create" => "Jautājumu forma"));
      $from = option("ContactForm\\from", null, "From", array("value_on_create" => "Web"));
      $from_email = option("ContactForm\\from_email", null, "From e-mail", array("value_on_create" => 'web@'.$domain));
        
      $sent = phpMailerSend(
        $text,
        $subject,
        $from,
        $from_email,
        $email,
        true,
        false,
        Array()
      );

      if(!$sent) $errors['msg'][] = $this->l("Sistēmas kļūda. Neizdevās nosūtīt");

      $_SESSION['captcha'] = '';

    }

    return $errors;

  }

  function output(){

    $fields = $this->getFields();

    if(!empty($errors)){
      echo "<ul class='errors'>";
      foreach($errors as $error){
        echo "<li>".$error."</li>";
      }
      echo "</ul>";
    }

    ?>
    <div id="ContactFormWrap">
      <div class='content'>
        <form method='post' action='<?=$_SERVER["REQUEST_URI"]?>'>
          <label for='contact_name'><?=$this->l("Vārds, uzvārds")?>:</label>
          <input type='text' name='name' value='<?=($_SESSION['logged_in'])? $_SESSION['logged_in']['name']." ".$_SESSION['logged_in']['surname'] : ""?>' id='contact_name' />
          <label for='contact_email'><?=$this->l("E-pasts")?>:</label>
          <input type='text' name='email' value='<?=(isset($_SESSION['logged_in']))? $_SESSION['logged_in']['email'] : ""?>' id='contact_email' />
          <label for='contact_comment'><?=$this->l("Tavs jautājums vai komentārs")?>:</label>
          <textarea name='comment' id='contact_comment'></textarea>
          <label for='security_code'><?=$this->l("Drošības kods")?>:</label>
          <div class='security_code_wrap'>
            <a href='#' title='<?=hsc($this->l("Drošības kods"))?>'><img src='http://<?=$_SERVER["HTTP_HOST"]?>/cms/library/captcha/captcha.php' alt='<?=hsc($this->l("Drošības kods"))?>' /></a>
            <input type='text' name='security_code' value='' id='security_code' />
            <input type='submit' name='send' value='<?=$this->l("Sūtīt")?>' />
            <div class='cb'></div>
          </div>
          <input type='hidden' name='SendContactForm' value='1' />
        </form>
      </div>
      <div class='label'><div class='text <?=$this->lang?>'></div></div>
      <div class='cb'></div>
    </div>
    <script>

      var captcha_url = <?=json_encode("http://".$_SERVER["HTTP_HOST"]."/cms/library/captcha/captcha.php")?>

      var wrap = $("#ContactFormWrap");
      $(".security_code_wrap a", wrap).click(function(e){

        var captcha = $(this).find("img");
        captcha.attr("src", captcha_url + "?" + Math.random());

        e.preventDefault();

      });

    </script>
    <?

  }

  function initCollections(){

    $this->initFields();

  }

  function SetProperties()
  {
    $this->properties = Array(
      "name"        => Array(
        "label"     => "Name:",
        "type"      => "str"
      ),

    );
  }



}



?>