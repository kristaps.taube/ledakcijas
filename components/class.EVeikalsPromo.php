<?
/**
*
*  Title: Promo codes
*  Author: Kristaps Taube <ktaube@datateks.lv>
*  Date: 17.02.2014
*  Project: E-Veikals
*
*/

include_once("class.component.php");
require_once($GLOBALS['cfgDirRoot']."library/mPDF/mpdf.php");

class EVeikalsPromo extends component{

  function __construct($name){
    parent::__construct($name);
    $this->SetProperties();

    $this->registerAsService("Promo");

  }

  function execute(){

    $this->init();

    if($_GET['action'] == 'GeneratePromoCode' && $_GET['id']){
      $this->GeneratePDF($_GET['id'], true);
      die();
    }

    if($_POST['action'] == 'ClearPromoCode'){
    	$_SESSION['promo_code'] = '';
			unset($_SESSION['promo_code']);
    	die();
    }

    if($_POST['action'] == 'ValidatePromoCode'){

      $response = $this->ValidatePromoCode($_POST['code']);
			if($response['status']){
				$_SESSION['promo_code'] = $_POST['code'];
			}else{
      	$_SESSION['promo_code'] = '';
				unset($_SESSION['promo_code']);
			}

      die(json_encode($response));

    }

  }

  function init(){

    $this->initCollections();

    $this->shopman = $this->getServiceByName("shopmanager");

    $this->CurrencyMenu = $this->getServiceByName('CurrencyMenu', true);

    if($_SESSION['promo_code']){
      $this->active_promocode = $this->codes->getByCode($_SESSION['promo_code']);
    }

    $this->promo_logo = option('GiftCard\\logo', null, 'Logo link', Array("is_advanced" => true, 'type' => 'dialog', 'componenttype' => get_class($this) ) );
    $this->promo_products = option('GiftCard\\products', null, 'Product image link', Array("is_advanced" => true, 'type' => 'dialog', 'componenttype' => get_class($this) ) );

  }

  static function processOptionBeforeDisplay($path, $opt, &$data){

    if($path == 'GiftCard\\logo' || $path == 'GiftCard\\products'){
      $data['dialog'] = '?module=dialogs&action=filex&site_id='.$GLOBALS['site_id'].'&view=thumbs';
    }

  }

  function GeneratePDF($code, $output = false){

    $code = is_numeric($code)? $this->codes->getItemByIDAssoc($code) : $code;

    if(!$code){
      echo $this->l("Promo code not found");
      return;
    }

    ob_start()
    ?>
      <style type="text/css">
        .number, .for{
          font: 22px Verdana;
          color: #316385;
        }
        .sum{
          font: bold 24px Verdana;
          color: #f1930b;
          margin-bottom: 20px;
        }
        .valid_until{
          font: 22px Verdana;
          color: #316385;
        }
        .wrap{
          padding: 50px 75px 0px;
          position: relative;
        }

        .column2{ font: 14px verdana; }
        .title{
          color: #f1930b;
          margin-bottom: 20px;
          font-weight: bold;
        }

        table{ width: 100%; }
        table td{ vertical-align: top; }

        .fc{
          width: 70%;
          padding-right: 10px;
        }

        .ft{
          padding: 15px 10px;
        }

        .footer{
          border-top: 3px solid #CCCCCC;
          padding-top: 10px;
          margin-top: 10px;
          font-size: 18px;
        }

        .logo{
          width: 200px;
          margin: 20px 75px 0px;
        }

        .logosep{
          width: 100%;
          border-bottom: 3px solid #7cbd42;
        }

        .productback_wrap{
          position: absolute;
          right: 0px;
          top: 0px;
        }

        .products_wrap{
          position: absolute;
          right: 40px;
          top: 20px;
        }

        .t{
          font-weight: bold;
          font-style: italic;
          color: #7cbd42;
          margin: 0px 80px 10px;
          font: italic bold 23px Verdana
        }

      </style>
      <?

        if($code['type'] == 1){
          $for = $this->l("piegādei");
        }elseif($code['type'] == 2){
          $p = $this->prods->getItemByIDAssoc($code['product']);
          $for = $this->l("produktam")." ".$p['name_lv'];
        }elseif($code['type'] == 3){
          $c = $this->cats->getItemByIDAssoc($code['category']);
          $for = $this->l("kategorijai")." ".$c['title_lv'];
        }elseif($code['type'] == 4){
          $for = $this->l("gala summai");
        }
      ?>
      <img src='http://<?=$_SERVER['SERVER_NAME']?><?=$this->promo_logo?>' alt='' class='logo' />
      <div class='t'><?=$this->l("Atlaides kupons")?></div>
      <div class='products_wrap'>
        <img src='http://<?=$_SERVER['SERVER_NAME']?><?=$this->promo_products?>' alt='' class='logo' />
      </div>
      <div class='logosep'></div>
      <div class='wrap'>
        <table>
          <tr>
            <td class='fc'>
              <div class='column1'>
                <div class='number'><?=$this->l("Kupona kods")?> <b><?=$code['code']?></b></div>
                <div class='for'><?=str_replace("[for]", $for, $this->l("Atlaide [for]"))?></div>
                <div class='sum'><?=$this->l("Atlaides apjoms")?>: <?=$code['discount_size']?> <?=($code['discount_type'] == 'precentage')? "%" : $this->CurrencyMenu->default['label']?></div> <br />
                <div class='valid_until'><?=$this->l("Derīguma termiņš")?>: <?=$code['onetime']? $this->l("Vienreiz lietojams") : $this->l("Bez ierobežojumiem")?></div>
              </div>
            </td>
          </tr>
        </table>
        <div class='footer'>
          <?=$this->l('Šī atlaižu kupona īpašnieks to var izmantot produktu iegādei tiešsaistē e-veikals.lv mājas lapā.')?>
        </div>
       </div>

    <?
    $html = ob_get_contents();
    ob_end_clean();

    $dir = sqlQueryValue("select dirroot from sites where site_id=".$this->site_id);
    $filename = sha1("promo".$code['item_id'].rand(1,10000)).".pdf"; // so noone can read
    $web_path = DIRECTORY_SEPARATOR."files".DIRECTORY_SEPARATOR ."promocodes".DIRECTORY_SEPARATOR . $filename;
    $path = str_replace(DIRECTORY_SEPARATOR.DIRECTORY_SEPARATOR, DIRECTORY_SEPARATOR, $dir.$web_path);

    $mpdf = new mPDF();
    $mpdf->AddPage('P', '', '', '', '', 0,0,0,0);
    $mpdf->WriteHTML($html);
    $mpdf->Output($path); // save pdf

    if($output){
      $mpdf->Output(); // output pdf
    }

    return array("web" => $web_path, "full" => $path);

  }

  function ValidatePromoCode($code){

    $entry = (is_array($code))? $code : $this->codes->getByCode($code);

    $return = array("status" => 0);

    if($entry && !$entry['used']){

      $type = ($entry['discount_type'] == 'precentage')? "%" : " ".$_SESSION['currency']['label'];

      $amount = "<span class='highlight'>".(number_format($entry['discount_size'] / $_SESSION['currency']['rate'])).$type."</span>";

      $text = $this->l("[amount] atlaide [discount_for]!");

      if($entry['type'] == 1){
        $for = $this->l("piegādei");
      }elseif($entry['type'] == 2){

        $prod = $this->prods->getItemByIDAssoc($entry['product']);
        if($prod){ // no product found
          $for = str_replace("[product]", $prod['name_'.$this->lang], $this->l("produktam [product]"));
        }else{
          die("Error. No product found");
        }

      }elseif($entry['type'] == 3){

        $cat = $this->cats->getItemByIDAssoc($entry['category']);
        if($cat){
          $for = str_replace("[category]", $cat['title_'.$this->lang], $this->l("kategorijai [category]"));
        }else{
          die("Error. Category not found");
        }

      }elseif($entry['type'] == 4){

        $for = $this->l("gala summai");

      }

      $text = str_replace("[discount_for]", $for, $text);
      $text = str_replace("[amount]", $amount, $text);

			$return['type'] = $entry['discount_type'];
      $return['amount'] = $entry['discount_size'] / $_SESSION['currency']['rate'];
      $return['msg'] = $text;
      $return['status'] = 1;

      $discount = 0;

      if($entry['type'] == 3 || $entry['type'] == 2){ // product or category

        $this->shopman->getSumAndGoodsList($goods, $sum, $hasgoods);

        foreach($goods as $p){
          $discount += $this->getExactProductDiscountForCode($p, $entry);
        }

      }elseif($entry['type'] == 4){ // gala summai
        $this->shopman->getSumAndGoodsList($goods, $sum, $hasgoods);
        $discount = ($entry['discount_type'] == 'sum')? $entry['discount_size'] : $sum * ($entry['discount_size'] / 100);
      }

      $return['discount'] = $discount;

    }elseif($entry && $entry['used']){
      $return['msg'] =  $this->l("Šis promo kods ir jau izlietots");
    }else{
      $return['msg'] =  $this->l("Promo kods netika atrasts");
    }

    return $return;

  }

  function loadOptions(){

    $check = $this->initCollections();
    if(!$check){
      echo $this->name.": Can't init collections.<br />";
      die;
    }

    if($_GET['type'] == 2){

      $prods = $this->prods->getDBData();

      foreach($prods as $prod){
        $data[$prod['item_id']] = $prod['name_lv'];
      }

    }elseif($_GET['type'] == 3){

      $cats = $this->cats->getDBData();
      $data[-1] = "Visām";
      foreach($cats as $cat){
        $pre = str_repeat("&nbsp;&nbsp;", $cat['lvl']);
        $data[$cat['item_id']] = $pre.$cat['title_lv'];
      }

    }else{
      die;
    }
    ?>

    <tr id='addition'>
      <td>
        <?=($_GET['type'] == 2)? "Produkts" : "Kategorija" ?>:
      </td>
      <td>
        <select name='elem'>
          <option value='0'><?=$this->l('Izvēlieties')?> <?=($_GET['type'] == 2)? "produktu" : "kategoriju" ?>..</option>
          <? foreach($data as $key => $value){ ?>
          <option value='<?=$key?>' <?=($_GET['active'] == $key)? "selected='selected'" : "" ?>><?=$value?></option>
          <? } ?>
        </select>
      </td>
    </tr>
    <?

    die();

  }

  function getSumDiscountForCode($sum, $code){

    $code = is_array($code)? $code : $this->codes->getByCode($code);
    if(!$code || $code['type'] != 4) return 0;

    if($code['discount_type'] == 'sum'){
      $discount = ($sum > $sum)? $delivery_sum : $code['discount_size'];
    }else{
      $discount = $sum * $code['discount_size'] / 100;
    }

    return $discount;

  }

  function getDeliveryDiscountForCode($delivery_sum, $code){

    $code = is_array($code)? $code : $this->codes->getByCode($code);
    if(!$code || $code['type'] != 1) return 0;

    if($code['discount_type'] == 'sum'){
      $discount = ($delivery_sum > $sum)? $delivery_sum : $code['discount_size'];
    }else{
      $discount = $delivery_sum * $code['discount_size'] / 100;
    }

    return $discount;

  }

  function getCategoryDiscountForCode($code){

    $sum = 0;

    foreach($_SESSION['cartitems'] as $item){

      $prod = $this->prods->getItemByIDAssoc($item['id']);

      if($prod['category_id'] == $code['category'] || $code['category'] == -1){

        if($item['price_id']){

          $price = $this->prices->getItemByIDAssoc($item['price_id']);
          $sum += $price['price'] * $item['count'];

        }else{

          $sum += $prod['price'] * $item['count'];

        }

      }

    }

    if(!$sum) return 0;

    if($code['discount_type'] == 'sum'){

      return ($code['discount_size'] > $sum)? $sum : $code['discount_size'];

    }else{

      return $sum * $code['discount_size'] / 100;

    }

  }

  function registerUsage($code){

    $code = is_array($code)? $code : $this->codes->getByCode($code);

    if($code['onetime']){
      $this->codes->changeItemByIDAssoc($code['item_id'], array('used' => 1));
    }

  }

  // check exact product
  function getExactProductDiscountForCode($item, $code){

    $code = is_array($code)? $code : $this->codes->getByCode($code);
    if(!$code || $code['used']) return 0;

    if($code['type'] == 2){ // discount for single product

      if($item['id'] == $code['product']){ // yep, we found guilty one

        $featured = $this->shopman->isProductFeatured($item['product']);

        // we do not sum discount for this code
        if($featured && !$code['sum_discount']){
          return 0;
        }

        if($item['price_id']){

          $price = $this->prices->getItemByIDAssoc($item['price_id']);
          $price['price'] = ($featured && $price['saleout_price'])? $price['saleout_price'] : $price['price'];
          $sum = $price['price'] * $item['count'];

        }else{

          $prod = $item['product']? $item['product'] : $this->prods->getItemByIDAssoc($item['id']);
          $prod['price'] = ($featured && $prod['sale_price'])? $prod['sale_price'] : $prod['price'];
          $sum = $prod['price'] * $item['count'];

        }

        if(!$sum) return 0;

        if($code['discount_type'] == 'sum'){

          return ($code['discount_size'] > $sum)? $sum : $code['discount_size'];

        }else{

          return $sum * $code['discount_size'] / 100;

        }

      }else{
        return 0;
      }

    }elseif($code['type'] == 3){ // discount for category

      $cat = $this->cats->getItemByIDAssoc($code['category']);
      if(!$cat) return;
      $ids = $this->cats->getPredecessorIds($cat['item_id']);
      $ids[] = $cat['item_id'];

      if(!in_array($item['product']['category_id'], $ids)){
        return 0;
      }

      $featured = $this->shopman->isProductFeatured($item['product']);

      // we do not sum discount for this code
      if($featured && !$code['sum_discount']){
        return 0;
      }

      if($item['price_id']){

        $price = $this->prices->getItemByIDAssoc($item['price_id']);
        $price['price'] = ($featured && $price['saleout_price'])? $price['saleout_price'] : $price['price'];
        $sum = $price['price'] * $item['count'];

      }else{

        $prod = $item['product']? $item['product'] : $this->prods->getItemByIDAssoc($item['id']);
        $prod['price'] = ($featured && $prod['sale_price'])? $prod['sale_price'] : $prod['price'];
        $sum = $prod['price'] * $item['count'];

      }

      if(!$sum) return 0;

      if($code['discount_type'] == 'sum'){

        return ($code['discount_size'] > $sum)? $price['price'] : $price['price'] * ($code['discount_size'] / $sum); 

      }else{

        return $sum * $code['discount_size'] / 100;

      }


    }else{ // other types
      return 0;
    }

  }

  // search in all basket items for product
  function getProdDiscountForCode($code){

    $sum = 0;

    foreach($_SESSION['cartitems'] as $item){

      $sum = $this->getExactProductDiscountForCode($item, $code);
      if($sum) break; // we found him!

    }

    return $sum;

  }

  function getPromoCodeDiscount($code){

    $code = $this->codes->getByCode($code);

    if(!$code || ($code['onetime'] && $code['used'])){
      $response = false;
    }else{

      $response['discount_type'] = $code['discount_type'];
      $response['type'] = $code['type'];

      switch($code['type']){
        case 1: $response['discount'] = $code['discount_size']; break; // piegādei
        case 2: $response['discount'] = $this->getProdDiscountForCode($code); break; // produktam
        case 3: $response['discount'] = $this->getCategoryDiscountForCode($code); break; // kategorijai
        case 4: $response['discount'] = $code['discount_size']; break; // gala summai
      }

    }

    return $response;

  }

  function generateCode($length = 8){

    $code = "";
    $possible = "012346789abcdfghjkmnpqrtvwxyzABCDFGHJKLMNPQRTVWXYZ";
    $maxlength = strlen($possible);

    if ($length > $maxlength) {
      $length = $maxlength;
    }

    $i = 0;

    while ($i < $length) {

      $char = substr($possible, mt_rand(0, $maxlength-1), 1);

      if (!strstr($code, $char)) {
        $code .= $char;
        $i++;
      }

    }

    return $code;

  }

  function design(){

    $this->init();
    $check = $this->initCollections();
    if(!$check){
      echo $this->name.": Can't init collections.<br />";
      return;
    }

    $ajax_url = $this->getComponentModifierLink('loadOptions');

    ?>
    <h3 style='margin-bottom: 0px;'><?=lcms('Promo code creation')?></h3>
    <div style='margin-bottom: 11px;font-size:10px'><?=lcms('Promo kodi tiek ģenerēti pamata valūtā. Ja pamata valūta tiek mainīta, jāatceras nomainīt uzģenerētos promo kodus atbilstoši valūtas kursam!')?></div>
    <form method='post' action='' id='PromoGenerator'>
      <table style='width: 320px;'>
        <tr>
          <td style='width: 150px;'><?=lcms('Discount apply to')?>:</td>
          <td>
            <select name='type'>
              <option value='1' <?=($_POST['type'] == '1')? "selected='selected'" : "" ?>><?=lcms('Piegādei')?></option>
              <option value='2' <?=($_POST['type'] == '2')? "selected='selected'" : "" ?>><?=lcms('Produktam')?></option>
              <option value='3' <?=($_POST['type'] == '3')? "selected='selected'" : "" ?>><?=lcms('Kategorijai')?></option>
              <option value='4' <?=($_POST['type'] == '4')? "selected='selected'" : "" ?>><?=lcms('Gala summai')?></option>
            </select>
          </td>
        </tr>
        <tr>
          <td><?=lcms('Discount type')?>:</td>
          <td>
            <input type='radio' value='sum' name='discount_type' <?=($_POST['discount_type'] == 'sum' || !$_POST['discount_type'])? "checked='checked'" : "" ?> /> <?=lcms('Sum')?>&nbsp;&nbsp;&nbsp;
            <input type='radio' value='precentage' name='discount_type' <?=($_POST['discount_type'] == 'precentage')? "checked='checked'" : "" ?> /> <?=lcms('Percentage')?><br />
          </td>
        </tr>
        <tr>
          <td><?=lcms('Discount size')?>:</td>
          <td><input name='discount_size' value='<?=$_POST['discount_size']? $_POST['discount_size'] : 0 ?>'></td>
        </tr>
        <tr>
          <td><?=lcms('Promo code count')?>:</td>
          <td><input name='count' value='<?=$_POST['count']? $_POST['count'] : 1 ?>' /></td>
        </tr>
        <tr>
          <td><?=lcms('Var pielietot akcijas produktiem')?>:</td>
          <td><input type='checkbox' <?=($_POST['sum_discount'])? "checked='checked'" : "" ?> name='sum_discount' value='1' /></td>
        </tr>
        <tr>
          <td><?=lcms('Vienreizējai lietošanai')?>:</td>
          <td><input type='checkbox' <?=($_POST['onetimer'])? "checked='checked'" : "" ?> name='onetimer' value='1' /></td>
        </tr>
        <tr>
          <td><?=lcms('Comments')?>:</td>
          <td><textarea name='comments'><?=($_POST['comments'])? $_POST['comments'] : "" ?></textarea></td>
        </tr>
        <tr>
          <td colspan='2' class='center'>
            <input type='submit' value='<?=lcms("Generate")?>' name='GeneratePromoCodes' />
          </td>
        </tr>
      </table>
    </form>

    <script type='text/javascript'>
      $("select[name='type']").change(function(){

        var val = $(this).val();
        var tr = $(this).closest("tr");
        $("#addition").remove();

        var ajax_url = <?=json_encode($ajax_url)?>;

        $.get(ajax_url + "&type=" + val + "&active=<?=$_POST['elem']?>",function(response) {
            tr.after(response);
        });
      }).trigger("change");
    </script>
    <style type="text/css">
    #PromoGenerator table td{
      padding: 3px 0px;
    }
    .center{
      text-align: center;
    }
    </style>
    <?

    if($_POST['GeneratePromoCodes']){

      $errors = Array();

      if(empty($_POST['discount_size'])){
        $errors[] = lcms("Tukšs atlaides apjoms");
      }

      if($_POST['type'] == 2 && empty($_POST['elem'])){
        $errors[] = lcms("Nav izvēlēts produkts");
      }

      if($_POST['type'] == 3 && empty($_POST['elem'])){
        $errors[] = lcms("Nav izvēlēta kategorija");
      }

      if($_POST['count'] <= 0){
        $errors[] = lcms("Nepareizs kodu skaits");
      }

      if(empty($errors)){

        for($i = (int)$_POST['count']; $i != 0; $i--){

          $crow = true;

          // make me unique!
          while($crow){
            $code = $this->generateCode(5);
            $crow = $this->codes->getByCode($code);
          }

          $data = Array(
            "code" => $code,
            "type" => $_POST['type'],
            "comments" => $_POST['comments'],
            "discount_type" => $_POST['discount_type'],
            "discount_size" => str_replace(",", ".", (float)$_POST['discount_size']),
            "category" => ($_POST['type'] == 3)? $_POST['elem'] : "",
            "product" => ($_POST['type'] == 2)? $_POST['elem'] : "",
            "onetime" => ($_POST['onetimer'])? 1 : 0,
            "sum_discount" => ($_POST['sum_discount'])? 1 : 0,
          );

          // we need id for file generation
          $id = $this->codes->Insert($data);
          $data['item_id'] = $id;

          $pdf = $this->GeneratePDF($data);
          $data['pdf'] = $pdf['web'];
          $this->codes->Update(array("pdf" => $data['pdf']), array("item_id" => $id));

          $codes[] = $code." (<a href='".$pdf['web']."' target='_blank'>PDF</a>)";

        }

        echo "<b>Kodi:</b> <br />".implode(" <br /> ", $codes);

      }else{

        ?>
        <ul class='errors'>
        <? foreach($errors as $error){ ?>
          <li><?=$error?></li>
        <? } ?>
        </ul>
        <?

      }


    }

  }

  function output(){

    if(!$this->initCollections()){
      echo $this->name.": Can't init collections.<br />";
      return;
    }

  }

  function SetProperties(){
    $this->properties = Array(

      "name"        => Array(
        "label"     => "Name:",
        "type"      => "str"
      ),

      "cats" => Array(
        "label"       => "Category collection:",
        "type"        => "collection",
        "collectiontype" => "shopcatcollection",
        "lookup"      => GetCollections($this->site_id, "shopcatcollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "prods" => Array(
        "label"       => "Product collection:",
        "type"        => "collection",
        "collectiontype" => "shopprodcollection",
        "lookup"      => GetCollections($this->site_id, "shopprodcollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "codes" => Array(
        "label"       => "Promo codes collection:",
        "type"        => "collection",
        "collectiontype" => "EVeikalsPromoCodes",
        "lookup"      => GetCollections($this->site_id, "EVeikalsPromoCodes"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "prices" => Array(
        "label"       => "Prices collection:",
        "type"        => "collection",
        "collectiontype" => "shoppricescollection",
        "lookup"      => GetCollections($this->site_id, "shoppricescollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

    );
  }

}
