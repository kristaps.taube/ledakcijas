<?php

use Constructor\WebApp;

class LEDAkcijasSlideshow extends component
{

    public function output()
    {

        $lang = WebApp::$app->getLanguage();
        $banners_col = bannerlistdbcollection::getInstance('banners_'.$lang);

        $cond = [];
        $cond[] = "position = 'top'";
        $cond[] = "IF(hit_limit, hit_limit > hits, 1)";
        $cond[] = "IF(view_limit, view_limit > views, 1)";
        $cond[] = "IF(date_start, date_start <= NOW(), 1)";
        $cond[] = "IF(date_end, date_end >= NOW(), 1)";

        $banners = $banners_col->GetDBData(['where' => implode(" AND ", $cond)]);

        echo $this->render('index', ['banners' => $banners]);

    }


}