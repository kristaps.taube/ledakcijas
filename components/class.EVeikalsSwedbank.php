<?php
/**
*
*  Title: Swedbank
*  Author: Kristaps Taube <ktaube@datateks.lv>
*  Date: 19.04.2017
*  Project: EVeikals
*
*/

require_once("class.component.php");

class EVeikalsSwedbank extends component
{

    private $use_Swedbank;
    private $mode;
    private $Swedbank_url;
    private $Swedbank_vtid;
    private $Swedbank_password;
    private $Swedbank_hps_page;
    private $curlRawResponse;
    private $error;
    private $optionsInited = false;

    public function __construct($name)
    {
        parent::__construct($name);
        $this->SetProperties();
    }

    public function execute()
    {

        $this->initOptions();

        if($_SERVER["REMOTE_ADDR"] == '213.21.219.21' || $_SERVER["REMOTE_ADDR"] == '213.21.219.28' || $_SERVER['REMOTE_ADDR'] == '127.0.0.1'){ // me, us or localhost

            if($_GET['action'] == 'makepayment' && $_GET['id']){
                $this->makePayment($_GET['id']);
                die();
            }

            #$entry = $this->log->getById(5);
            #$this->processGetPaymentDetails($entry);

        }

        if(isset($_GET['action']) && isset($_GET['dts_reference']) && $_GET['action'] == 'card_return'){
            $this->processCardReturn($_GET['dts_reference']);
        }

            $this->processNotification();

    }

    public function output()
    {

        if(isset($_GET['view']) && $_GET['view'] == 'create_payment_error'){
            $this->outputSwedbankError();
            return;
        }elseif(isset($_GET['view']) && $_GET['view'] == 'payment_error'){
            $this->outputSwedbankError('payment_error');
            return;
        }elseif(isset($_GET['view']) && $_GET['view'] == 'payment_expired'){
            $this->outputSwedbankError('expired');
            return;
        }elseif(isset($_GET['view']) && $_GET['view'] == 'payment_cancelled'){
            $this->outputSwedbankError('cancelled');
            return;
        }elseif(isset($_GET['view']) && $_GET['view'] == 'payment_successful'){
            $this->outputSwedbankSuccess();
            return;
        }elseif(isset($_GET['action']) && $_GET['action'] == 'sw_return_success'){
            $this->outputSwedbankSuccess();
            return;
        }elseif(isset($_GET['action']) && $_GET['action'] == 'sw_return_failure'){
            $this->outputSwedbankError('banklink_failure');
            return;
        }

    }

    function outputSwedbankSuccess()
    {

        ?>
        <div class='padding' id='SwedbankSuccessWrap'>
            <h3><?=$this->l("Swedbank apmaksa veiksmīga")?></h3>
            <p><?=$this->l("Apmaksa saņemta")?></p>
        </div>
        <?php

    }

    function outputSwedbankError($type)
    {

        ?>
        <div class='padding' id='SwedbankErrorWrap'>
            <h3><?=$this->l("Swedbank apmaksas kļūda")?></h3>
            <?php
                switch($type){
                    case 'cancelled': $text = $this->l("Maksājums atcelts"); break;
                    case 'expired': $text = $this->l("Maksājuma apmaksas izpildes laiks beidzies"); break;
                    case 'payment_error': $text = str_replace("[REF]", htmlspecialchars($_GET['dts_reference']), $this->l("Maksājuma apmaksas kļūda. REF: [REF]")); break;
                    case 'banklink_failure': $text = $this->l("Maksājuma apmaksas kļūda."); break;
                    default: $text = '';
                }
            ?>
            <p><?=$text?></p>
        </div>
        <?php

    }

    public function makePayment($order, $test = false)
    {

        $order = is_array($order) ? $order : $this->orders->GetById($order);
        if(!$order){
            $this->addError(array("code" => 1, "error" => $this->l("Order not found")));
            return false;
        }

        if($order['apmaksats']){
            $this->addError(array("code" => 2, "error" => $this->l("Order already paid")));
            return false;
        }

        switch($order['apmaksas_veids']){
            case 'card': $this->payWithCard($order, $test); break;
            case 'ibank_swed': $this->payWithBankLink($order, $test); break;
            default:
                $this->addError(array("code" => 3, "error" => $this->l("Incorrect payment method")));
                return false;
        }

    }

    private function payWithBankLink($order, $test = false)
    {

        $reference = $this->log->getNewMerchantReference("ORDER".$order['item_id']);

        /**
        * SW (Swedbank)
        * NL (Nordea)
        * SL (SEB Lithuania)
        * SV (SEB Latvia)
        * DN (DNB)
        * DL (Danske)
        * CA (Citadele)
        */
        switch($order['apmaksas_veids']){
            case 'ibank_swed': $payment_method = "SW"; break; // currently only swedbank
        }

        $request_xml = $this->makeBanklinkPaymentRequestXML($order, $reference, $payment_method, $test);
        $response = $this->makeCURLRequest($request_xml);

        $this->log->Insert(array(
            'order_id' => $order['item_id'],
            'merchant_reference' => $reference,
            'datacash_reference' => $response['datacash_reference'],
            'event' => 'banklink_create_request',
            'result' => $response['reason'],
            'request' => $request_xml,
            'response' => $this->curlRawResponse
        ));

        if($response['HpsTxn']['hps_url'] && $response['HpsTxn']['session_id']){
            $redirect_url = $response['HpsTxn']['hps_url'].'?HPS_SessionID='.$response['HpsTxn']['session_id'];
        }else{
            $redirect_url = $this->return_page."?view=payment_error&ref=".$reference;
        }

        header('location:'.$redirect_url);
        die();

    }

    private function makeBanklinkPaymentRequestXML($order, $reference, $payment_method, $test = false)
    {

        $sum = $test ? 0.01 : $order['kopeja_summa']*100;
        $description = $this->getPaymentDescription($order, $test);
        $return_url = 'http://'.$GLOBALS['cfgDomain'].$this->return_page;
        $lang = $order['lang'] ? $order['lang'] : 'en';

    ob_start();
    ?>
    <Request version="2">
        <Authentication>
            <password><?=$this->Swedbank_password?></password>
            <client><?=$this->Swedbank_vtid?></client>
        </Authentication>
        <Transaction>
            <TxnDetails>
                <merchantreference><?=$reference?></merchantreference>
            </TxnDetails>
            <HpsTxn>
                <page_set_id><?=$this->Swedbank_hps_page?></page_set_id>
                <method>setup_full</method>
            </HpsTxn>
            <APMTxns>
                <APMTxn>
                    <method>purchase</method>
                    <payment_method><?=$payment_method?></payment_method>
                    <AlternativePayment version="2">
                    <TransactionDetails>
                        <Description><?=$description?></Description>
                        <SuccessURL><?=$return_url?>?action=sw_return_success</SuccessURL>
                        <FailureURL><?=$return_url?>?action=sw_return_failure</FailureURL>
                        <Language><?=$lang?></Language>
                        <PersonalDetails>
                            <Email><?=$order['epasts']?></Email>
                        </PersonalDetails>
                        <BillingDetails>
                            <AmountDetails>
                                <Amount><?=$sum?></Amount>
                                <Exponent>2</Exponent>
                                <CurrencyCode>978</CurrencyCode>
                            </AmountDetails>
                        </BillingDetails>
                    </TransactionDetails>
                    <MethodDetails>
                        <?php if($payment_method == 'SW'){ ?>
                        <ServiceType>LTV_BANK</ServiceType>
                        <?php } ?>
                        </MethodDetails>
                    </AlternativePayment>
                </APMTxn>
            </APMTxns>
        </Transaction>
    </Request>
    <?php

    $xml = ob_get_clean();

    return $xml;

    }

    private function payWithCard($order, $test = false)
    {

        $reference = $this->log->getNewMerchantReference("ORDER".$order['item_id']);
        $request_xml = $this->makeCardPaymentRequestXML($order, $reference, $test);
        $response = $this->makeCURLRequest($request_xml);

        $this->log->Insert(array(
            'order_id' => $order['item_id'],
            'merchant_reference' => $reference,
            'datacash_reference' => $response['datacash_reference'],
            'event' => 'card_create_request',
            'result' => $response['reason'],
            'request' => $request_xml,
            'response' => $this->curlRawResponse
        ));

        if($response['reason'] == 'ACCEPTED'){
            $redirect_url = $response['HpsTxn']['hps_url'].'?HPS_SessionID='.$response['HpsTxn']['session_id'];
        }else{
            $redirect_url = $this->return_page."?view=payment_error&ref=".$reference;
        }

        header("location: ".$redirect_url);
        die();

    }

    private function makeCURLRequest($xml)
    {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->Swedbank_url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300);

        // not cool
        if($this->mode == 'sandbox'){
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        }

        $data = curl_exec($ch);

        $this->curlRawResponse = $data;

        //convert the XML result into array
        $array_data = json_decode(json_encode(simplexml_load_string($data)), true);

        return $array_data;

    }

    private function getPaymentDescription($order, $test = false)
    {

        $description = str_replace("[ORDER_ID]", $this->order_prefix." ".$order['item_id'], $this->l("Payment for invoice [ORDER_ID]"));
        if($test){
            $description .= " (TEST)";
        }
        return $description;

    }

    private function makeCardPaymentRequestXML($order, $merch_reference, $test = false)
    {

        $sum = $test ? 0.01 : $order['kopeja_summa'];

        $expiry_url = "http://".$GLOBALS['cfgDomain'].$this->return_page."?view=payment_expired";
        $cancel_url = "http://".$GLOBALS['cfgDomain'].$this->return_page."?view=payment_cancelled";
        $error_url = "http://".$GLOBALS['cfgDomain'].$this->return_page."?view=payment_error";
        $return_url = "http://".$GLOBALS['cfgDomain'].$this->return_page."?action=card_return";

        $description = $this->getPaymentDescription($order, $test);

        if($order['user_id']){
            $user = $this->users->getById($order['user_id']);
            $card_data = json_decode($user['card_data'], true);
            $card_data = $card_data ? $card_data : false;

            $name = $user['name'];
            $surname = $user['surname'];
        }else{
            $card_data = false;

            $order_data = unserialize($order['order_data']);
            $allfields = $order_data['allfields'];
            $type = $allfields['maksatajs']['value'];

            $buyer = $order['pasutitajs'];
            if($type == 'fiziska'){
                $explode = explode(' ', $buyer);
                if(count($explode) > 0){
                    $name = array_shift($explode); // removes the first element, and returns it

                    if(count($explode) > 0){
                        $surname = implode(' ', $explode); // glue the remaining pieces back together
                    }
                }
            }
            else{
                $name = $buyer;
            }
        }

        ob_start();
        ?>
        <Request version="2">
            <Authentication>
                <password><?=$this->Swedbank_password?></password>
                <client><?=$this->Swedbank_vtid?></client>
            </Authentication>
            <Transaction>
                <TxnDetails>
                    <merchantreference><?=$merch_reference?></merchantreference>
                    <amount currency="EUR"><?=$sum?></amount>
                    <capturemethod>ecomm</capturemethod>
                    <ThreeDSecure>
                        <purchase_datetime><?=date('Ymd H:i:s')?></purchase_datetime>
                        <verify>yes</verify>
                        <merchant_url>http://<?=$GLOBALS['cfgDomain']?></merchant_url>
                        <purchase_desc><?=$description?></purchase_desc>
                    </ThreeDSecure>
                    <Risk>
                        <Action service="1">
                            <MerchantConfiguration>
                                <channel>W</channel>
                            </MerchantConfiguration>
                            <CustomerDetails>
                                <OrderDetails>
                                    <BillingDetails>
                                    </BillingDetails>
                                </OrderDetails>
                                <PersonalDetails>
                                    <first_name><?=$name;?></first_name>
                                    <surname><?=$surname;?></surname>
                                    <telephone></telephone>
                                </PersonalDetails>
                                <ShippingDetails>
                                </ShippingDetails>
                                <PaymentDetails>
                                    <payment_method>CC</payment_method>
                                </PaymentDetails>
                                <RiskDetails>
                                    <email_address><?=$order['epasts']?></email_address>
                                    <ip_address><?=$_SERVER['REMOTE_ADDR']?></ip_address>
                                </RiskDetails>
                            </CustomerDetails>
                        </Action>
                    </Risk>
                </TxnDetails>
                <HpsTxn>
                    <method>setup_full</method>
                    <page_set_id><?=$this->Swedbank_hps_page?></page_set_id>
                    <?php if($card_data){ ?>
                    <cvv_only>1</cvv_only>
                    <Card>
                        <pan type="hps_token"><?=$card_data['token']?></pan>
                    </Card>
                    <?php } ?>
                    <return_url><?=$return_url?></return_url>
                    <expiry_url><?=$expiry_url?></expiry_url>
                    <error_url><?=$error_url?></error_url>
                    <DynamicData>
                        <?php if($card_data){ ?>
                        <?php $expirydate_parts = explode("/", $card_data['expirydate']); ?>
                        <dyn_data_1><?=$card_data['pan']?></dyn_data_1>
                        <dyn_data_6><?=$expirydate_parts[0]?></dyn_data_6>
                        <?php /* because Swedbank returns expiry year in format "17", but requires in format "2017" */ ?>
                        <dyn_data_7><?=(2000 + $expirydate_parts[1])?></dyn_data_7>
                        <?php } ?>
                        <dyn_data_4><?=$return_url?></dyn_data_4>
                    </DynamicData>
                </HpsTxn>
                <CardTxn>
                    <method>auth</method>
                </CardTxn>
            </Transaction>
        </Request>
        <?php

        $xml = ob_get_clean();

        return $xml;

    }

    public function processCron()
    {

        $this->initOptions();

        if(!$this->useSwedbank()) return;

        $entries = $this->log->getEntriesForPaymentCheck();

        foreach($entries as $entry){
            $this->processPaymentStatusCheck($entry);
        }

    }

    private function processCardReturn($reference)
    {

        $entry = $this->log->getRow(array("where" => 'datacash_reference = :ref AND event = "card_create_request"', 'params' => array(':ref' => $reference)));

        $result = $this->processPaymentStatusCheck($entry);

        $url = $result ? $this->return_page."?view=payment_successful" : $this->return_page."?view=payment_error&dts_reference=".$reference;

        header("location: ".$url);
        die();

    }

    private function processPaymentStatusCheck($entry)
    {

        $order = $this->orders->GetById($entry['order_id']);

        if($order['apmaksats']) return true; // this should never happen

        $response = $this->queryPaymentStatus($entry);

        if($response['HpsTxn']['AuthAttempts']['Attempt']['dc_response'] == 1){ // ir apmaksāts

            $order_update_data = array("apmaksats" => 1, "apmaksas_datums" => date("Y-m-d H:i:s"));

            // if it was card payment, get card data
            if($entry['event'] == 'card_create_request'){

                $log_entry = $this->log->getByDatacashReference($response['HpsTxn']['datacash_reference']);
                $card_data = $this->processGetPaymentDetails($log_entry);
                if($card_data){
                    $order_update_data['card_data'] = json_encode($card_data);
                }

            }

            $paid_status = $this->statuses->getPaidStatusRow();
            if($paid_status){

                $order_update_data['status_id'] = $paid_status['item_id'];

                // adding history entry
                $this->order_history->Insert(array(
                    "order_id" => $order['item_id'],
                    "status_from_id" => $order['status_id'],
                    "status_to_id" => $paid_status['item_id'],
                    "comments" => "Rēķina apmaksa saņemta",
                    "date" => date('Y-m-d H:i:s')
                ));

            }

            foreach($order_update_data as $key => $value){
                $order[$key] = $value;
            }

            // recreate order html
            $html = $this->orderform->getOrderById($order);

            $order['rekins'] = $order_update_data['rekins'] = $html;

            $this->orders->Update($order_update_data, $order['item_id']);

            $this->orderform->sendPaymentConfirmationMail($order);

            return true;

        }

        return false;

    }

    private function queryPaymentStatus($entry)
    {

        $request_xml = '
        <Request version="2">
            <Authentication>
                <password>'.$this->Swedbank_password.'</password>
                <client>'.$this->Swedbank_vtid.'</client>
            </Authentication>
            <Transaction>
                <HistoricTxn>
                    <reference>'.$entry['datacash_reference'].'</reference>
                    <method>query</method>
                </HistoricTxn>
            </Transaction>
        </Request>';

        $response = $this->makeCURLRequest($request_xml);

        $this->log->Insert(array(
            'order_id' => $entry['order_id'],
            'merchant_reference' => $entry['merchant_reference'],
            'datacash_reference' => $response['HpsTxn']['datacash_reference'],
            'event' => 'payment_status_query',
            'result' => $response['reason'],
            'request' => $request_xml,
            'response' => $this->curlRawResponse
        ));

        return $response;

    }

    private function processGetPaymentDetails($entry)
    {

        $card_data = false;

        $request_xml = '
        <Request version="2">
            <Authentication>
                <password>'.$this->Swedbank_password.'</password>
                <client>'.$this->Swedbank_vtid.'</client>
            </Authentication>
            <Transaction>
                <HistoricTxn>
                    <reference>'.$entry['datacash_reference'].'</reference>
                    <method>query</method>
                </HistoricTxn>
            </Transaction>
        </Request>
        ';

        $response = $this->makeCURLRequest($request_xml);

        $this->log->Insert(array(
            'order_id' => $entry['order_id'],
            'merchant_reference' => $entry['merchant_reference'],
            'event' => 'get_payment_details',
            'result' => $response['reason'],
            'request' => $request_xml,
            'response' => $this->curlRawResponse
        ));

        if($response['QueryTxnResult']['Card']){

            $card = $response['QueryTxnResult']['Card'];
            $order = $this->orders->getById($entry['order_id']);

            $card_data = array(
                "expirydate" => $card['expirydate'],
                "country" => $card['country'],
                "pan" => $card['pan'],
                "scheme" => $card['scheme'],
                "issuer" => $card['issuer'],
                "token" => $card['token']
            );

            $this->orders->Update(array("card_data" => json_encode($card_data)), $entry['order_id']);

            if($order['user_id']){

                $user = $this->users->getById($order['user_id']);
                if($user){
                    $this->users->Update(array("card_data" => json_encode($card_data)), $user['item_id']);
                }

            }

        }

        return $card_data;

    }

    private function processNotification()
    {

            $post = file_get_contents("php://input");
        $notification = json_decode(json_encode(simplexml_load_string($post)), true);

            if(empty($notification)){
                $this->logNotification("Empty notification");
                return;
            }

            $this->logNotification("Notification: ".print_r($notification, true));
            $this->logNotification("Notification serialized: ".serialize($notification));

            $ref = $notification['Event']['Purchase']['@attributes']['TransactionId'];
            $log_entries = $this->log->getByMerchantReference($ref);
                $log_entry = $log_entries[0];

        if(!$log_entry){
                $this->logNotification("No log entry found by: ".$ref);
            return; // not ok
        }

            $this->logNotification("Log entry: ".print_r($log_entry, true));

            $status = $notification['Event']['Purchase']['Status'];

            $this->log->Insert(array(
                'order_id' => $log_entry['order_id'],
                'event' => 'process_notification',
                'result' => $status,
                'merchant_reference' => $ref,
                'date' => date('Y-m-d H:i:s'),
                'response' => print_r($notification)
            )
        );

            $this->logNotification("Status: ".$status);

            if($status == 'AUTHORISED'){
            $this->logNotification("Order paid");

            $order = $this->orders->GetRow(array('where'=> 'item_id='.$log_entry['order_id']));
                        $paid_status = $this->statuses->getPaidStatusRow();

            if($paid_status){
                $this->order_history->Insert(array(
                    "order_id" => $order['item_id'],
                    "status_from_id" => $order['status_id'],
                    "status_to_id" => $paid_status['item_id'],
                    "comments" => "Rēķina apmaksa saņemta",
                    "date" => date('Y-m-d H:i:s')
                ));
            }

                $order_update_data = array(
                    "apmaksats" => 1,
                    "apmaksas_datums" => date("Y-m-d H:i:s"),
                    "status_id" => $paid_status['item_id']
                );

            $this->orders->Update($order_update_data, $log_entry['order_id']);
            $this->orderform->sendPaymentConfirmationMail($log_entry['order_id']);

            }else{
                $this->logNotification("Order already paid");
            }

        echo "<Response>OK</Response>";
        die();

    }

    function logNotification($text)
    {

        file_put_contents(LOG_FOLDER.swedbank_notification_log.txt, PHP_EOL . "----- ".date("d.m.Y H:i:s")." (".$_SERVER['REMOTE_ADDR'].") -----" . PHP_EOL . $text . PHP_EOL, FILE_APPEND);

    }

    public function useSwedbank()
    {
        return $this->use_Swedbank ? true : false;
    }

    public function getError()
    {
        return $this->error;
    }

    private function addError($error)
    {
        $this->error = $error;
    }

    private function initOptions()
    {

        if($this->optionsInited) return;

        if($this->orderform){
            $this->orderform->initOptions();
        }

        $this->use_Swedbank = option('Swedbank\\use', null, 'Use Swedbank', Array('type' => 'check'));
        $this->mode = option('Swedbank\\mode', null, 'Mode', Array('type' => 'list', 'fieldparams' => array('lookupassoc' => array('sandbox' => 'Sandbox', 'production' => 'production'))));

        $this->Swedbank_url = ($this->mode == 'production') ? "https://mars.transaction.datacash.com/Transaction" : "https://accreditation.datacash.com/Transaction/acq_a";

        $this->Swedbank_vtid = option('Swedbank\\merchant_id', null, 'vTID', null, '');
        $this->Swedbank_password = option('Swedbank\\password', null, 'Password', null, '');

        foreach(getLanguages() as $lang => $lang_data){
            option('Swedbank\\hps_page_'.$lang, null, 'HPS page('.$lang.')', null, 1403);
            option('Swedbank\\hps_page_sandbox_'.$lang, null, 'HPS page sandbox('.$lang.')', null, 168);
        }

        $this->Swedbank_hps_page = ($this->mode == 'production') ? readOption('Swedbank\\hps_page_'.$this->lang) : readOption('Swedbank\\hps_page_sandbox_'.$this->lang);

        $this->order_prefix = readOption('Shop\\OrderForm\\order_prefix');

        // orderform_page
        $this->return_page = "/".PagePathById($this->return_page);

        $this->optionsInited = true;

    }

    public function SetProperties()
    {

        $this->properties = Array(

            "name" => Array(
                "label" => "Name:",
                "type" => "str"
            ),

            "orders" => Array(
                "label" => "Order collection:",
                "type" => "collection",
                "collectiontype" => "EVeikalsOrderCollection",
            ),

            "users" => Array(
                "label" => "User collection:",
                "type" => "collection",
                "collectiontype" => "shopusercollection",
            ),

            "statuses" => Array(
                "label" => "Statuses collection:",
                "type" => "collection",
                "collectiontype" => "EVeikalsOrderStatuses",
            ),

            "order_history" => Array(
                "label" => "Order history collection:",
                "type" => "collection",
                "collectiontype" => "EVeikalsOrderHistory",
            ),

            "log" => Array(
                "label" => "Order Log collection:",
                "type" => "collection",
                "collectiontype" => "EVeikalsSwedbankLog",
            ),

            'return_page' => Array(
                'label' => 'Atgriešanās lapa:',
                'type' => 'page_id',
            ),

        );

        $this->PostSetProperties();

    }

}