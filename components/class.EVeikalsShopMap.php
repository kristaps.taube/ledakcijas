<?php
/**
*
*  Title: Shop map
*  Author: Kristaps Taube <ktaube@datateks.lv>
*  Date: 06.03.2014
*  Project: EVeikals
*
*/

include_once("class.component.php");

class EVeikalsShopMap extends component{

  function __construct($name){
    parent::__construct($name);
    $this->SetProperties();
  }

  function execute(){

    $this->marker = option('GoogleMap\\logo', null, 'Logo link', Array('type' => 'dialog', 'componenttype' => get_class($this) ) );
    $this->marker = getThumbUrl($this->marker, 25, 25, 6);

    if($_GET['action'] == 'popup_map'){
      $this->output_popup($_GET['shop']);
      die();
    }

  }

  static function processOptionBeforeDisplay($path, $opt, &$data){

    if($path == 'GoogleMap\\logo'){
      $data['dialog'] = '?module=dialogs&action=filex&site_id='.$GLOBALS['site_id'].'&view=thumbs';
    }

  }

  function output_popup($shop){

    if(!$this->initCollections()){
      echo $this->name.": Can't init collections.<br />";
      return;
    }

    $shop = $this->shops->getItemByIDAssoc((int)$shop);

    if(!$shop){
      echo "Shop not found";
      return;
    }

    $a = array();
    $c = explode(',', $shop['coords']);
    if (count($c) > 1){
      $a[0] = (float)$c[0];
      $a[1] = (float)$c[1];
    }

    $shops[] = array(
      'lat' => $a[0],
      'lng' => $a[1],
      'content' => "<div class='ShopContent'><div class='title'>".$shop['title_'.$this->lang]."</div>".$shop['desc_'.$this->lang]."</div>"
    );

    if($this->marker){
      $shops[0]['icon'] = $this->marker;
    }
                           
    ?>

      <div style='max-width: 80%;width: 600px;height: 450px;'>
      <div id="map_canvas"></div>
      <script type="text/javascript">
        var $gm;
        var map_shops;
        function initPopupMap(){
          $gm = google.maps;
          map_shops = <?=json_encode($shops)?>;
          var latlng = new $gm.LatLng(<?=$a[0]?>, <?=$a[1]?>);
          initGoogleMap(latlng, 13);
        }
      </script>
      </div>

    <?


  }

  function output(){

    if(!$this->initCollections()){
      echo $this->name.": Can't init collections.<br />";
      return;
    }

    $map_center = option("GoogleMap\\map_center", NULL, "Kartes centra koord.", NULL, "56.949433403348394, 24.105334563171482", $this->site_id);
    $zoom = option("GoogleMap\\zoom", NULL, "Zoom", NULL, "12", $this->site_id);
    $map_center = explode(",", $map_center);

    $shops = array();
    $cities = array();

    foreach($this->shops->getDBData() as $shop){

      $cities[$chop['city_id']][] = $shop;

      $a = array();
      $c = explode(',', $shop['coords']);
      if (count($c) > 1){
        $a[0] = (float)$c[0];
        $a[1] = (float)$c[1];
      }
      if (count($a) != 2) continue;

      $shop = array(
        'lat' => $a[0],
        'lng' => $a[1],
        'content' => "<div class='ShopContent'><div class='title'>".$shop['title_'.$this->lang]."</div>".$shop['desc_'.$this->lang]."</div>"
      );

      if($this->marker){
        $shop['icon'] = $this->marker;
      }

      $shops[] = $shop;

    }

    ?>
    <div class='title'></div>
    <div class="map">
      <div id="map_canvas"></div>
    </div>
    <div class='cb'></div>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <script type="text/javascript">
      var $gm = google.maps;
      var map_shops = <?=json_encode($shops)?>;
      var latlng = new $gm.LatLng(<?=$map_center[0]?>, <?=$map_center[1]?>);
      initGoogleMap(latlng, <?=$zoom?>);
    </script>
    <?


  }

  function SetProperties(){
    $this->properties = Array(

      "name"        => Array(
        "label"     => "Name:",
        "type"      => "str"
      ),

      "shops" => Array(
        "label"       => "Veikali:",
        "type"        => "collection",
        "collectiontype" => "EVeikalsShopCollection",
        "lookup"      => GetCollections($this->site_id, "EVeikalsShopCollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

    );
  }



}
