<?php
/**
*
*  Title: Best Excel Export ever... ...and import!
*  Author: Kristaps Taube <ktaube@datateks.lv>
*  Date: 27.02.2014
*  Project: E-Veikals
*  added volume import/export by Jānis Polis 01.08.2017
*
*/

require_once("class.component.php");
require_once($GLOBALS['cfgDirRoot'].'library/PHPExcel.php');
require_once($GLOBALS['cfgDirRoot'].'library/PHPExcel/IOFactory.php');

class EVeikalsExcelExport extends component{

  function __construct($name){
    parent::__construct($name);
    $this->SetProperties();

    $this->product_fields = array("ID" => "item_id");

  }

  function increaseResources(){

    ini_set('memory_limit','1024M'); // 1GB
    set_time_limit(60 * 5); // 5min

  }

  function init(){

    // make prod. prop and excel column relation array.
    // array("excel column" => "Prod prop", "excel column" => "Prod prop", ...) <- like this

    foreach($this->excel_prod_rel->getDBData() as $relation){
      $this->product_fields[$relation['excel_column']] = $relation['prod_prop'];
    }
    
    $this->volumes_prod_fields = $this->volumes_fields = array();    
    $this->volumes_prod_fields["Produkta nosaukums"] = 'prodname';
    $this->volumes_fields["Produkta ID"] = 'product_id';
    $this->volumes_fields["ID"] = 'item_id';
    $this->langs = getLanguages();
    foreach($this->langs  as $lang => $val){
        $this->volumes_fields["Nosaukums ".$lang] = 'name_'.$lang;
    }
    $this->volumes_fields["Cena"] = 'price';
    $this->volumes_fields["Akcijas cena"] = 'saleout_price';
    $this->volumes_fields["Artikuls"] =  'code';
    
    $this->partial_import_tmp_file = $GLOBALS['cfgDirRoot']."temp/excel_import.xls";
    $this->partial_export_tmp_file = $GLOBALS['cfgDirRoot']."temp/excel_export.xls";

    $this->partial_export_page_size = 1000; // how many products process in one iteration
    $this->partial_import_page_size = 2000; // how many products process in one iteration

  }

  function includeJs(){

  ?>
    <script type="text/javascript" src="/scr/components/EVeikalsExcelExporter.js"></script>
  <?

  }

  function design(){

    if(!$this->initCollections()){
      echo $this->name.": Can't init collections.<br />";
      return;
    }

    $this->init();

    if($_GET['partialexport'] == 1 && isset($_SESSION['DO_PARTIAL_EXPORT']) && !empty($_SESSION['DO_PARTIAL_EXPORT'])){ // ooo! we need to do partial export
      $this->ExportProducts($_SESSION['DO_PARTIAL_EXPORT']['category_ids']);
      die();
    }
    
    if($_GET['partialvolumesexport'] == 1 && isset($_SESSION['DO_PARTIAL_EXPORT']) && !empty($_SESSION['DO_PARTIAL_EXPORT'])){ // ooo! we need to do partial export
      $this->ExportVolumes($_SESSION['DO_PARTIAL_EXPORT']['category_ids']);
      die();
    }

    if($_GET['partialimport'] == 1){
      $this->ImportProducts();
    }
    
    if($_GET['partialvolumesimport'] == 1){
      $this->ImportVolumes();
    }

    if($_GET['StartExportProducts']){
      $this->StartExportProducts($_GET['category']);
    }
    
    if($_GET['StartExportVolumes']){
      $this->StartExportVolumes($_GET['category']);
    }

    if($_POST['ImportProducts']){
      $this->StartImportProducts($_FILES['file']['tmp_name']);
    }
    
    if($_POST['ImportVolumes']){
      $this->StartImportVolumes($_FILES['file']['tmp_name']);
    }

    if($_POST['ExportCategories']){
      $this->ExportCategories();
    }

    if($_GET['importdone'] == 1){
      echo "Imports pabeigts!";
    }

    $tree = $this->cats->getCategoryTree(0, false);
    $this->includeJs();
    ?>
    <h1><?=lcms('Export')?></h1>
    <hr />
    <form method='post' action='<?=$this->getComponentModifierLink('design')?>' id="EVeikalsExport">
      <?=lcms('Category')?>:
      <select name='category'>
        <option value='0'><?=lcms('All')?></option>
        <? foreach($tree as $cat){ $this->outputCatTreeOption($cat); } ?>
      </select>
      <br />
      <input type='submit' name='ExportProducts' value='<?=lcms('Export products')?>' /> <br />
      <input type='submit' name='ExportVolumes' value='<?=lcms('Export volumes')?>' /> <br />
      <input type='submit' name='ExportCategories' value='<?=lcms('Export categories')?>' />
    </form>
    <h1><?=lcms('Import')?></h1>
    <hr />
    <form method='post' action='#EVeikalsExport' enctype="multipart/form-data" id="EVeikalsExport">
      <?=lcms('File')?>:  <input type="file" name="file" /> <input type='submit' name='ImportProducts' value='<?=lcms('Import products')?>' />
    </form>
    <h1><?=lcms('Import')?></h1>
    <hr />
    <form method='post' action='#EVeikalsVolumes' enctype="multipart/form-data" id="EVeikalsVolumes">
      <?=lcms('File')?>:  <input type="file" name="file" /> <input type='submit' name='ImportVolumes' value='<?=lcms('Import volumes')?>' />
    </form>
    <script>
      var ExportProducts_url = <?=json_encode($this->getComponentModifierLink('design')."&StartExportProducts=1")?>;
      var ExportVolumes_url = <?=json_encode($this->getComponentModifierLink('design')."&StartExportVolumes=1")?>;      
    </script>

    <?

  }

  
  function ExportCategories(){

    $excel = new PHPExcel();
    $excel->setActiveSheetIndex(0);
    $sheet = $excel->getActiveSheet();

    $style = array('font' => array('bold' => true));

    $col = 0;
    $row = 1;

    $category_fields = array("ID" => "item_id", "Nosaukums" => "title_lv", "Vecāks" => "parent");

    // writing headers
    foreach($category_fields as $column => $prop){
      $letter = PHPExcel_Cell::stringFromColumnIndex($col++);
      $sheet->setCellValue($letter.$row, $column);
      $sheet->getStyle($letter.$row)->applyFromArray($style);
    }

    $row++;

    foreach($this->cats->getCategoryTreeFlat(0, false) as $cat){
      $col = 0;
      foreach($category_fields as $column => $prop){        
        $letter = PHPExcel_Cell::stringFromColumnIndex($col++);
        $sheet->setCellValue($letter.$row, $cat[$prop]);        
      }
      $row++;
    }

    header("Content-type: application/octet-stream");
    header('Content-Disposition: attachment; filename="kategorijas.xls"');
    header("Content-Transfer-Encoding: binary");

    $objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
    $objWriter->save('php://output');

    die();
  }

  function ImportProducts(){

    $this->increaseResources();

    // are we done?
    if($_SESSION['DO_PARTIAL_IMPORT']["page"] > $_SESSION['DO_PARTIAL_IMPORT']["page_count"]){
      $_SESSION['DO_PARTIAL_IMPORT'] = array();
      unset($_SESSION['DO_PARTIAL_IMPORT']);
      unlink($this->partial_import_tmp_file);
      header("location: ?module=lists&site_id=4&importdone=1");
      die();
    }

    ?>
      <?=lcms('Importing page')?> <?=$_SESSION['DO_PARTIAL_IMPORT']["page"]?>/<?=$_SESSION['DO_PARTIAL_IMPORT']["page_count"]?>(<?=number_format($this->partial_import_page_size)?> <?=lcms('products per page')?>)...
    <?

    $excel = PHPExcel_IOFactory::load($this->partial_import_tmp_file);
    $sheet = $excel->getActiveSheet();

    $col = 0;
    $row = 1;

    $headers = array();

    $column_count = PHPExcel_Cell::columnIndexFromString($sheet->getHighestColumn());

    // reading headers
    for($i = 0; $i < $column_count; $i++){
      $col_name = $sheet->getCell(PHPExcel_Cell::stringFromColumnIndex($i)."1")->getCalculatedValue();
      foreach($this->product_fields as $column => $prod_prop){
        if($column == $col_name){
          $headers[$i] = $prod_prop; // remember which column it was, so we can later grab data from it
          break;
        }
      }
    }

    $prods = array();

    $start = ($_SESSION['DO_PARTIAL_IMPORT']["page"] - 1) * $this->partial_import_page_size + 2;
    $end = $start + $this->partial_import_page_size;
    $rows = $sheet->getHighestRow();
    $end = ($end > $rows)? $rows : $end;

    for ($i = $start; $i <= $end; $i++){

      foreach($headers as $num => $prop){

        $value = trim($sheet->getCell(PHPExcel_Cell::stringFromColumnIndex($num).$i)->getCalculatedValue());
        $prod[$prop] = $value;

      }

      $id = $prod['item_id'];
      unset($prod['item_id']); // do not update/add me

      if($id){ // existing prod

        $this->prods->changeItemByIDAssoc($id, $prod);

      }else{ // new prod

       $cat_id = $this->cats->getUncategorizedItemCatId();
       if($cat_id){

        //$prod['category_id'] = $cat_id;
        //$this->prods->addProduct($prod);

       }

      }

    }

    $_SESSION['DO_PARTIAL_IMPORT']["page"]++;

    // reload
    ?>

    <script>
      document.location.reload();
    </script>
    <?

  }
  
  function colNumber($c){
    return ord(strtolower($c)) - 96;
  }
    function ImportVolumes()
    {

    $this->increaseResources();

    // are we done?
    if($_SESSION['DO_PARTIAL_IMPORT']["page"] > $_SESSION['DO_PARTIAL_IMPORT']["page_count"]){
      $_SESSION['DO_PARTIAL_IMPORT'] = array();
      unset($_SESSION['DO_PARTIAL_IMPORT']);
      unlink($this->partial_import_tmp_file);
      header("location: ?module=lists&site_id=4&importdone=1");
      die();
    }

    ?>
      <?=lcms('Importing page')?> <?=$_SESSION['DO_PARTIAL_IMPORT']["page"]?>/<?=$_SESSION['DO_PARTIAL_IMPORT']["page_count"]?>(<?=number_format($this->partial_import_page_size)?> <?=lcms('products per page')?>)...
    <?

    $excel = PHPExcel_IOFactory::load($this->partial_import_tmp_file);
    $sheet = $excel->getActiveSheet();

    $col = 0;
    $row = 1;

    $headers = array();

    $column_count = PHPExcel_Cell::columnIndexFromString($sheet->getHighestColumn());

    // reading headers
    for($i = 0; $i < $column_count; $i++){
      $col_name = $sheet->getCell(PHPExcel_Cell::stringFromColumnIndex($i)."1")->getCalculatedValue();
      foreach($this->volumes_fields as $column => $prod_prop){
        if($column == $col_name){
          $headers[$i] = $prod_prop; // remember which column it was, so we can later grab data from it
          break;
        }
      }
    }

    $prods = array();

    $start = ($_SESSION['DO_PARTIAL_IMPORT']["page"] - 1) * $this->partial_import_page_size + 2;
    $end = $start + $this->partial_import_page_size;
    $rows = $sheet->getHighestRow();
    $end = ($end > $rows)? $rows : $end;

    for ($i = $start; $i <= $end; $i++){

      foreach($headers as $num => $prop){

        $value = trim($sheet->getCell(PHPExcel_Cell::stringFromColumnIndex($num).$i)->getCalculatedValue());
        $prod[$prop] = $value;

      }

      $id = $prod['item_id'];
      
      if($id AND $prod['product_id']){ // existing prod

        $this->prices->Update($prod,$id);

      }elseif($prod['product_id'] AND $prod['price']){ // new prod

        $this->prices->Insert($prod);
        $this->prods->Update(array('has_prices'=>1),$prod['product_id']);
      }

    }

    $_SESSION['DO_PARTIAL_IMPORT']["page"]++;

    // reload
    ?>

    <script>
      document.location.reload();
    </script>
    <?

  }  
  
  function StartImportProducts($file){

    // move file
    rename($file, $this->partial_import_tmp_file);

    $excel = PHPExcel_IOFactory::load($this->partial_import_tmp_file);
    $sheet = $excel->getActiveSheet();
    $rows = $sheet->getHighestRow();

    $_SESSION['DO_PARTIAL_IMPORT'] = array();
    $_SESSION['DO_PARTIAL_IMPORT']["page"] = 1;
    $_SESSION['DO_PARTIAL_IMPORT']["page_count"] = ceil($rows / $this->partial_import_page_size);

    unset($_GET['importdone']);

    header("location: ".$_SERVER['SCRIPT_URL']."?".http_build_query($_GET)."&partialimport=1#EVeikalsExport");
    die();

  }
  
  function StartImportVolumes($file)
  {

    // move file
    rename($file, $this->partial_import_tmp_file);

    $excel = PHPExcel_IOFactory::load($this->partial_import_tmp_file);
    $sheet = $excel->getActiveSheet();
    $rows = $sheet->getHighestRow();

    $_SESSION['DO_PARTIAL_IMPORT'] = array();
    $_SESSION['DO_PARTIAL_IMPORT']["page"] = 1;
    $_SESSION['DO_PARTIAL_IMPORT']["page_count"] = ceil($rows / $this->partial_import_page_size);

    unset($_GET['importdone']);

    header("location: ".$_SERVER['SCRIPT_URL']."?".http_build_query($_GET)."&partialvolumesimport=1#EVeikalsVolumes");
    die();

  }

  function StartExportProducts($cat){

    $cat_ids = $this->cats->getActivePredecessorIds($cat);
    $cat_ids[] = $cat;

    $cond = array();
    $cond[] = "category_id IN(".implode(",", $cat_ids).")";

    $count = $this->prods->GetDBData(array(
      "what" => "count(*) AS count",
      "where" => implode(" AND ", $cond),
      "assoc" => true
    ));

    $count = $count[0]["count"];

    $_SESSION['DO_PARTIAL_EXPORT'] = array();
    $_SESSION['DO_PARTIAL_EXPORT']["page"] = 1;
    $_SESSION['DO_PARTIAL_EXPORT']["page_count"] = ceil($count / $this->partial_export_page_size);
    $_SESSION['DO_PARTIAL_EXPORT']['category_ids'] = $cat_ids;

    unset($_GET['StartExportProducts']);

    // reload
    header('Location: '.$_SERVER['SCRIPT_URL']."?".http_build_query($_GET)."&partialexport=1");
    die();

  }
  
  function StartExportVolumes($cat){

    $cat_ids = $this->cats->getActivePredecessorIds($cat);
    $cat_ids[] = $cat;
    
    $cond[] = "category_id IN(".implode(",", $cat_ids).")";
   
    if($GLOBALS['CMS_LANG'])
        $what = 'item_id,name_'.$GLOBALS['CMS_LANG'];
    else
        $what = 'item_id,name_lv';
    $prods = $this->prods->GetTable(array(
      "what" => $what,
      "where" => implode(" AND ", $cond),     
    ));
    $tmp = $prodids = array();
    foreach($prods as $prod){
        $tmp[$prod['item_id']] = $GLOBALS['CMS_LANG'] ? $prod['name_'.$GLOBALS['CMS_LANG']] : $prod['name_lv'];
        $prodids[] = $prod['item_id'];
    }
    $prods = $tmp;
    $cond = array();
    $cond[] = "product_id IN(".implode(",", $prodids).")";
    
    $count = $this->prices->GetValue(array(
      "what" => 'COUNT(*)',
      "where" => implode(" AND ", $cond),      
    ));
    if(!$count){ 
        echo 'There is no volumes';
    die;
    }
    $_SESSION['DO_PARTIAL_EXPORT'] = array();
    $_SESSION['DO_PARTIAL_EXPORT']["page"] = 1;
    $_SESSION['DO_PARTIAL_EXPORT']["page_count"] = ceil($count / $this->partial_export_page_size);
    $_SESSION['DO_PARTIAL_EXPORT']['category_ids'] = $cat_ids;

    unset($_GET['StartExportVolumes']);

    // reload
    header('Location: '.$_SERVER['SCRIPT_URL']."?".http_build_query($_GET)."&partialvolumesexport=1");
    die();

  }  
  
  function ExportProducts($cat_ids){

    $this->increaseResources();

    // are we done?
    if($_SESSION['DO_PARTIAL_EXPORT']["page"] > $_SESSION['DO_PARTIAL_EXPORT']["page_count"]){
      $_SESSION['DO_PARTIAL_EXPORT'] = array();
      unset($_SESSION['DO_PARTIAL_EXPORT']);
      $this->sendExportedProductsToUser();
    }

    ?>
      <?=lcms('Exporting page')?> <?=$_SESSION['DO_PARTIAL_EXPORT']["page"]?>/<?=$_SESSION['DO_PARTIAL_EXPORT']["page_count"]?>(<?=number_format($this->partial_export_page_size)?> <?=lcms('products per page')?>)...
    <?

    if($_SESSION['DO_PARTIAL_EXPORT']["page"] == 1){ // we need to start new document

      $excel = new PHPExcel();
      $excel->setActiveSheetIndex(0);
      $sheet = $excel->getActiveSheet();

      $style = array('font' => array('bold' => true));

      $col = 0;
      $row = 1;

      // writing headers
      foreach($this->product_fields as $column => $prod_prop){
        $letter = PHPExcel_Cell::stringFromColumnIndex($col++);
        $sheet->setCellValue($letter.$row, $column);
        $sheet->getStyle($letter.$row)->applyFromArray($style);
      }

      $row++;

    }else{ // we need to continue started document

      $excel = PHPExcel_IOFactory::load($this->partial_export_tmp_file);
      $sheet = $excel->getActiveSheet();
      $row = ($_SESSION['DO_PARTIAL_EXPORT']["page"] - 1) * $this->partial_export_page_size + 2;

    }

    $cond = array();
    $cond[] = "category_id IN(".implode(",", $cat_ids).")";

    $prods = $this->prods->GetDBData(array(
      "what" => implode(",", $this->product_fields),
      "where" => implode(" AND ", $cond),
      "count" => $this->partial_export_page_size,
      "offset" => ($_SESSION['DO_PARTIAL_EXPORT']["page"] - 1) * $this->partial_export_page_size,
      "assoc" => true
    ));

    foreach($prods as $prod){
      $col = 0;
      foreach($this->product_fields as $column => $prod_prop){      
        $letter = PHPExcel_Cell::stringFromColumnIndex($col++);
        $sheet->setCellValue($letter.$row, $prod[$prod_prop]);
      }
      $row++;
    }

    $_SESSION['DO_PARTIAL_EXPORT']["page"]++;

    $objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
    $objWriter->save($this->partial_export_tmp_file); // writing to temp file

    // reload
    ?>
    <br /><?=lcms('Memory using')?>: <?=round(memory_get_usage(true) / (1024 * 1024), 2)?> MB / 1024 MB
    <script>
      document.location.reload();
    </script>
    <?
    die();

  }
  
  function ExportVolumes($cat_ids)
  {

    $this->increaseResources();

    // are we done?
    if($_SESSION['DO_PARTIAL_EXPORT']["page"] > $_SESSION['DO_PARTIAL_EXPORT']["page_count"]){
      $_SESSION['DO_PARTIAL_EXPORT'] = array();
      unset($_SESSION['DO_PARTIAL_EXPORT']);
      $this->sendExportedVolumesToUser();
    }

    ?>
      <?=lcms('Exporting page')?> <?=$_SESSION['DO_PARTIAL_EXPORT']["page"]?>/<?=$_SESSION['DO_PARTIAL_EXPORT']["page_count"]?>(<?=number_format($this->partial_export_page_size)?> <?=lcms('products per page')?>)...
    <?

    if($_SESSION['DO_PARTIAL_EXPORT']["page"] == 1){ // we need to start new document

      $excel = new PHPExcel();
      $excel->setActiveSheetIndex(0);
      $sheet = $excel->getActiveSheet();

      $style = array('font' => array('bold' => true));

      $col = 0;
      $row = 1;

      // writing headers      
      foreach($this->volumes_prod_fields as $column => $prod_prop){
        $letter = PHPExcel_Cell::stringFromColumnIndex($col++);
        $sheet->setCellValue($letter.$row, $column);
        $sheet->getStyle($letter.$row)->applyFromArray($style);
      }
      foreach($this->volumes_fields as $column => $prod_prop){
        $letter = PHPExcel_Cell::stringFromColumnIndex($col++);
        $sheet->setCellValue($letter.$row, $column);        
        $sheet->getStyle($letter.$row)->applyFromArray($style);
      }

      $row++;

    }else{ // we need to continue started document

      $excel = PHPExcel_IOFactory::load($this->partial_export_tmp_file);
      $sheet = $excel->getActiveSheet();
      $row = ($_SESSION['DO_PARTIAL_EXPORT']["page"] - 1) * $this->partial_export_page_size + 2;

    }

    $cond = array();
    $cond[] = "category_id IN(".implode(",", $cat_ids).")";
    if($GLOBALS['CMS_LANG'])
        $what = 'item_id,name_'.$GLOBALS['CMS_LANG'];
    else
        $what = 'item_id,name_lv';
    $prods = $this->prods->GetTable(array(
      "what" => $what,
      "where" => implode(" AND ", $cond),     
    ));
    $tmp = $prodids = array();
    foreach($prods as $prod){
        $tmp[$prod['item_id']] = $GLOBALS['CMS_LANG'] ? $prod['name_'.$GLOBALS['CMS_LANG']] : $prod['name_lv'];
        $prodids[] = $prod['item_id'];
    }
    $prods = $tmp;
    $cond = array();
    $cond[] = "product_id IN(".implode(",", $prodids).")";
    
    $prices = $this->prices->GetDBData(array(
      "what" => implode(",", $this->volumes_fields),
      "where" => implode(" AND ", $cond),
      "count" => $this->partial_export_page_size,
      "offset" => ($_SESSION['DO_PARTIAL_EXPORT']["page"] - 1) * $this->partial_export_page_size,
      "assoc" => true
    ));
    
    foreach($prices as $price){
      $col = 0;
      foreach($this->volumes_prod_fields as $column => $price_prop){
        $letter = PHPExcel_Cell::stringFromColumnIndex($col++);
        $sheet->setCellValue($letter.$row, $prods[$price['product_id']]);        
      }
      foreach($this->volumes_fields as $column => $price_prop){
        $letter = PHPExcel_Cell::stringFromColumnIndex($col++);
        $sheet->setCellValue($letter.$row, $price[$price_prop]);
      }
      $row++;
    }

    $_SESSION['DO_PARTIAL_EXPORT']["page"]++;

    $objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
    $objWriter->save($this->partial_export_tmp_file); // writing to temp file

    // reload
    ?>
    <br /><?=lcms('Memory using')?>: <?=round(memory_get_usage(true) / (1024 * 1024), 2)?> MB / 1024 MB
    <script>
      document.location.reload();
    </script>
    <?
    die();

  }
  
  function sendExportedProductsToUser(){

    if(file_exists($this->partial_export_tmp_file)){

      header("Content-type: application/octet-stream");
      header('Content-Disposition: attachment; filename="preces.xls"');
      header("Content-Transfer-Encoding: binary");

      readfile($this->partial_export_tmp_file);
      unlink($this->partial_export_tmp_file); // deleting file

      die();

    }

  }
  
  function sendExportedVolumesToUser(){

    if(file_exists($this->partial_export_tmp_file)){

      header("Content-type: application/octet-stream");
      header('Content-Disposition: attachment; filename="tilpumi.xls"');
      header("Content-Transfer-Encoding: binary");

      readfile($this->partial_export_tmp_file);
      unlink($this->partial_export_tmp_file); // deleting file

      die();

    }

  }

  function outputCatTreeOption($cat){

    $pre = str_repeat("&nbsp;&nbsp;", $cat['lvl']);

    echo "<option value='".$cat['item_id']."'>".$pre.$cat['title_'.$this->lang]."</option>";
    if($cat['kids']){
      foreach($cat['kids'] as $kid){
        $this->outputCatTreeOption($kid);
      }
    }

  }

  function output(){

    if(!$this->initCollections()){
      echo $this->name.": Can't init collections.<br />";
      return;
    }

  }

  function SetProperties(){
    $this->properties = Array(

      "name"        => Array(
        "label"     => "Name:",
        "type"      => "str"
      ),

      "cats" => Array(
        "label"       => "Category collection:",
        "type"        => "collection",
        "collectiontype" => "shopcatcollection",
        "lookup"      => GetCollections($this->site_id, "shopcatcollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "prods" => Array(
        "label"       => "Product collection:",
        "type"        => "collection",
        "collectiontype" => "shopprodcollection",
        "lookup"      => GetCollections($this->site_id, "shopprodcollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "excel_prod_rel" => Array(
        "label"       => "Excel prod. rel. collection:",
        "type"        => "collection",
        "collectiontype" => "EVeikalsExcelProdrelation",
        "lookup"      => GetCollections($this->site_id, "EVeikalsExcelProdrelation"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),
      
      "prices" => Array(
        "label"       => "Prices collection:",
        "type"        => "collection",
        "collectiontype" => "shoppricescollection",
         "lookup"      => GetCollections($this->site_id, "shoppricescollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),
    
    );
  }



}
