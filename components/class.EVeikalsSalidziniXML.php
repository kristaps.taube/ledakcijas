<?
/**
*
*  Title: XML priekš Salidzini.lv / kurpirkt.lv
*  Author: Kristaps Taube <ktaube@datateks.lv>
*  Date: 28.02.2014
*  Project: E-Veikals
*
*/

include_once("class.component.php");

class EVeikalsSalidziniXML extends component{

  function __construct($name){
    parent::__construct($name);
    $this->SetProperties();
  }

  function design(){
    return;
  }

  function increaseResources(){

    ini_set('memory_limit','1024M'); // 1GB
    set_time_limit(60 * 10); // 10min

  }

  function init(){

    $this->manager = $this->getServiceByName('shopmanager', true);
    $this->shop = $this->getServiceByName('FrontendShop', true);

  }

  function output(){

    $this->init();
    $this->increaseResources();
    if($_GET['action'] == 'components_form_frame'){ return; }

    $GLOBALS['content_type'] = "text/xml";

    $this->shopman = $this->getServiceByName('shopmanager');
    if($this->shopman) $this->shopman->initCollections();

    $prods = $this->products->getActive();
    //$prods = $this->products->getDBData(array("count" => 10));

    $cats = array();
    $brands = array();

    foreach($this->brands->getDBData(array("assoc" => true)) as $elem){
    	$brands[$elem['item_id']] = $elem;
    }

    foreach($this->categories->getDBData(array("assoc" => true)) as $elem){
    	$cats[$elem['item_id']] = $elem;
    }

    echo '<?xml version="1.0" encoding="UTF-8" ?>';
    ?>
    <root>
      <? foreach($prods as $prod){ ?>

      <?
				$brand = $cats[$prod['brand_id']];
        $cat = $cats[$prod['category_id']];
        $image = $prod['picture'];
 				$image = $image ? $image : $this->shop->default_product_image;
				$featured = $this->manager->isProductFeatured($prod);
    		$price = ($featured && $prod['sale_price'] && $prod['sale_price'] != '0.00')? $prod['sale_price'] : $prod['price'];
      ?>
      <item>
        <name><![CDATA[<?=$prod['name_lv']?>]]></name>
        <model><![CDATA[<?=$prod['name_lv']?>]]></model>
        <link><![CDATA[http://<?=$_SERVER['HTTP_HOST']?><?=$this->manager->getProductURL($prod)?>]]></link>
        <price><?=number_format($price,2)?></price>
        <manufacturer><![CDATA[<?=$brand['brandname']?>]]></manufacturer>
        <image><![CDATA[http://<?=$_SERVER['HTTP_HOST']?><?=$image?>]]></image>
        <category><![CDATA[<?=$cat['title_lv']?>]]></category>
        <category_full><![CDATA[<?=$cat['title_lv']?>]]></category_full>
        <category_link><![CDATA[http://<?=$_SERVER['HTTP_HOST']?>/<?=$cat['url_lv']?>]]></category_link>
        <in_stock><?=((int)$prod['prod_count'] > 0)? 1 : 0?></in_stock>
      </item>
    <? } ?>
    </root><?

  }

  function SetProperties(){
    $this->properties = Array(
      "name"        => Array(
        "label"     => "Name:",
        "type"      => "str"
      ),

      "products" => Array(
        "label"       => "Product collection:",
        "type"        => "collection",
        "collectiontype" => "shopprodcollection",
        "lookup"      => GetCollections($this->site_id, "shopprodcollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "categories" => Array(
        "label"       => "Category collection:",
        "type"        => "collection",
        "collectiontype" => "shopcatcollection",
        "lookup"      => GetCollections($this->site_id, "shopcatcollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "brands" => Array(
        "label"       => "Brendi:",
        "type"        => "collection",
        "collectiontype" => "shopbrandscollection",
        "lookup"      => GetCollections($this->site_id, "shopbrandscollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

    );
  }

}