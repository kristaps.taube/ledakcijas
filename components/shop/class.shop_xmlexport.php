<?

require_once($GLOBALS['cfgDirRoot']."components/class.component.php");
require_once($GLOBALS['cfgDirRoot'].'library/class.xmlnode.php');

class shop_xmlexport extends component
{
  function shop_xmlexport($name)
  {
    parent::__construct($name);
    $this->SetProperties();
  }

  function output()
  {
    if (empty($_GET['u']) || empty($_GET['key'])) die;

    $shopman = $this->shopman = $this->getServiceByName('shopmanager', true);
    if (!$shopman) die;

    $user = $shopman->userscol->getItemByIdAssoc(intval($_GET['u']));
    if (!$user) die;

    $allowed_ips = explode(';', $user['xml_allowed_ip']);
    foreach ($allowed_ips as $k => $ip)
    {
      $allowed_ips[$k] = trim($ip);
    }
    if (!in_array($_SERVER['REMOTE_ADDR'], $allowed_ips)) die('Your IP has no access.');


    ini_set('memory_limit', '2048M');
    set_time_limit(0);

    $langs = getLanguages();

    $orderby = 'cat_ind';
    $cond = array();
    $cond[] = Array('disabled', '= 0');
//    $cond[] = 'category_id = 308';

    $shop_xml = new XMLNode('shop');

    $categories_xml = new XMLNode('categories');
    $shop_xml->addChild($categories_xml);

    $products_xml = new XMLNode('products');
    $shop_xml->addChild($products_xml);

    $cats = array();
    $catdata = $shopman->catscol->getAllCategories();
    foreach ($catdata as $cat)
    {
      $cats[$cat['item_id']] = $cat;
    }

    $data = $shopman->prodscol->queryProducts($cond, 0, 0, $orderby, '*', QUERYPROD_DATA);

    $cat_ids = array();
    foreach ($data as $row)
    {
      $cat_id = $row['category_id'];
      do {
        if (!isset($cat_ids[$cat_id]))
          $cat_ids[$cat_id] = 1;

        $cat_id = $cats[$cat_id]['parent'];

      } while ($cat_id > 0);

    }

    foreach ($cat_ids as $cat_id => $val)
    {
      $cat = $cats[$cat_id];
      if (!$cat['item_id']) continue;

      $cat_xml = new XMLNode('category');
      $cat_xml->attribs = array(
        'id' => $cat['item_id'],
        'parent_id' => $cat['parent']
      );
      foreach ($langs as $lang => $lrow)
      {
        if (!empty($cat['title_'.$lang]))
          $cat_xml->attribs['title_'.$lang] = $cat['title_'.$lang];

      }
      $categories_xml->addChild($cat_xml);
    }

    foreach ($data as $prod)
    {
      $prod_xml = new XMLNode('product');
      $prod_xml->attribs = array(
        'id' => $prod['item_id'],
        'category_id' => $prod['category_id'],
        'code' => $prod['code'],
      );

      list($price, $old_price, $undiscounted_price) = $this->shopman->srvGetProductPrice($prod, $user, $prod['price'], $prod['sale_price']);
      if ($price == $undiscounted_price)
      {
        $prod_xml->attribs['price'] = $price;
      }
      else
      {
        $prod_xml->attribs['price'] = $undiscounted_price;
        $prod_xml->attribs['featured_price'] = $price;
      }

      if ($this->shopman->prodscol->isFeaturedProduct($prod))
        $prod_xml->attribs['is_featured'] = 1;

      if ($this->shopman->prodscol->isNewProduct($prod))
        $prod_xml->attribs['is_new'] = 1;

      foreach ($langs as $lang => $lrow)
      {
        if (!empty($prod['name_'.$lang]))
          $prod_xml->attribs['name_'.$lang] = $prod['name_'.$lang];

        if (!empty($prod['description_'.$lang]))
        {
          $desc = new XMLNode('description');
          $desc->attribs['lang'] = $lang;
          $desc->text = $prod['description_'.$lang];
          $desc->cdata = true;
          $prod_xml->addChild($desc);
        }

        if (!empty($prod['long_description_'.$lang]))
        {
          $desc = new XMLNode('long_description');
          $desc->attribs['lang'] = $lang;
          $desc->text = $prod['long_description_'.$lang];
          $desc->cdata = true;
          $prod_xml->addChild($desc);
        }

      }

      $products_xml->addChild($prod_xml);
    }

    header('Content-type: text/xml; charset=utf-8');
    echo $shop_xml->output(0, true, true);

    die;
  }

  function SetProperties()
  {
    $this->properties = Array(
      'name' => Array(
        'label'     => 'Name:',
        'type'      => 'str'
      ),

    );
  }
}




?>
