<?

require_once($GLOBALS['cfgDirRoot']."components/class.component.php");


class shop_exporter extends component
{
  function shop_exporter($name)
  {
    parent::__construct($name);
    $this->SetProperties();
  }

  function init()
  {
    $this->shopman = $this->getBackendServiceByName('shopmanager');
    if (!$this->shopman) return false;

    $this->lang = $this->getLanguageName();

    if (!$this->shopman->initCollections()) return false;

    return true;
  }

  function design()
  {
    if (!$this->init()) return;

    $data = $this->shopman->catscol->getAllCategories(null, 0);

    $jsparams = array(
      'wrapper' => '#shopexporter',
      'export_xls_url' => $this->getComponentModifierLink('exportXLS'),
      'export_xml_url' => $this->getComponentModifierLink('exportXML'),
      'texts' => array(
        'vismaz1' => 'Atzīmējiet vismaz vienu kategoriju, kuru vēlaties eksportēt!'
      )
    );

?>
  <script type="text/javascript" src="/scr/components/component.js"></script>
  <script type="text/javascript" src="/scr/components/shop/exporter.js"></script>

  <script type="text/javascript">

  var shopexporter = new ShopExporter(<?=json_encode($jsparams) ?>);

  </script>
  <style type="text/css">

  #shopexporter table.grid {
    width: 100%;
  }
  #shopexporter table.grid td {
    vertical-align: top;
  }

  </style>
  <div id="shopexporter">

  <table class="grid">
    <tr>
      <td>
        <button class="select_all">Iezīmēt visas</button>
        <button class="select_none">Noņemt visas</button><br/>
        <br/>
      </td>
      <td>
      </td>
    </tr>
    <tr>
      <td style="width:30%">
        <p style="margin-top:0;margin-bottom:10px">Kategorijas:</p>
        <div class="cats">
      <? foreach ($data as $row) { ?>
        <label><input type="checkbox" name="cat-<?=$row['item_id'] ?>" checked="checked"/> <span style="margin-left:<?=$row['lvl'] * 15 ?>px"><?=$row['title_'.$this->lang] ?></span></label><br/>
      <? } ?>
        </div>
      </td>
      <td class="options">
        <table>
          <tr>
            <td style="vertical-align:middle">Eksportēt:</td>
            <td>
              <button class="export_xls">Excel</button>
              <button class="export_xml">XML</button>
            </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td></td>
          </tr>
          <tr>
            <td style="vertical-align:middle">Importēt:</td>
            <td>
<?

    if ($_FILES['file1']['size'])
    {
      $this->importXLS();
    }

?>
              <form action="" method="post" enctype="multipart/form-data">
                <input type="file" name="file1"/>
                <button type="submit" class="import_xls" >Importēt no Excel</button>
              </form>
              ! importēt var tikai tos xls failus, kas eksportēti ar maināmajiem laukiem
            </td>
          </tr>
        </table>

      </td>
    </tr>
  </table>

  </div>
<?
  }

  function importXLS()
  {
    require_once($GLOBALS['cfgDirRoot'].'library/PHPExcel.php');

    $excel = PHPExcel_IOFactory::load($_FILES['file1']['tmp_name']);
    $sheet = $excel->getActiveSheet();

    if ($sheet->getCell('A1')->getCalculatedValue() != 'ID')
    {
      echo '<span style="color:red">Nekorekts XLS formāts.</span>';
      return;
    }

    $langs = getLanguages();

    $changed_count = 0;
    $rows = $sheet->getHighestRow();
    for ($y=3; $y<=$rows; $y++)
    {
      $id = trim($sheet->getCell('A'.$y)->getCalculatedValue());
      if ($id == '') continue;

      $prod = $this->shopman->prodscol->getItemByIdAssoc($id);
      if ($prod)
      {
        $col = 1;

        $changes = array();
        foreach ($langs as $lang => $lrow)
        {
          $changes['name_'.$lang] = $sheet->getCell($this->colLetter($col++).$y)->getCalculatedValue();
        }
        $changes['code'] = $sheet->getCell($this->colLetter($col++).$y)->getCalculatedValue();
        $changes['price'] = floatval($sheet->getCell($this->colLetter($col++).$y)->getCalculatedValue());
        $changes['sale_price'] = floatval($sheet->getCell($this->colLetter($col++).$y)->getCalculatedValue());
        if ($this->shopman->prodscol->properties_assoc['prod_count']['type'] != 'hidden')
        {
          $changes['prod_count'] = floatval($sheet->getCell($this->colLetter($col++).$y)->getCalculatedValue());
        }

        $changes['disabled'] = (int)(boolean)$sheet->getCell($this->colLetter($col++).$y)->getCalculatedValue();

        foreach ($changes as $chkey => $chval)
        {
          $changes[$chkey] = db_escape($chval);
        }

        $this->shopman->prodscol->changeItemByIdAssoc($prod['item_id'], $changes);

        $changed_count++;
      }

    }

    echo '<span style="color:green">'.$changed_count.' precēm nomainīti dati.</span><br/><br/>';
  }

  function colLetter($i)
  {
    return chr(ord('A')+$i);
  }

  function exportXLS()
  {
    if (!$this->init()) return;

    require_once($GLOBALS['cfgDirRoot'].'library/PHPExcel.php');

    $excel = new PHPExcel();
    $excel->setActiveSheetIndex(0);
    $sheet = $excel->getActiveSheet();

    $style = array(
      'font' => array(
        'bold' => true
      )
    );
    $langs = getLanguages();
    $col = 0;

    $letter = $this->colLetter($col++);
    $sheet->setCellValue($letter.'1', 'ID');
    $sheet->getColumnDimension($letter)->setWidth(10);
    $sheet->getStyle($letter.'1')->applyFromArray($style);

    foreach ($langs as $lang => $row)
    {
      $letter = $this->colLetter($col++);
      $sheet->setCellValue($letter.'1', 'Preces nosaukums ('.$lang.')');
      $sheet->getColumnDimension($letter)->setWidth(25);
      $sheet->getStyle($letter.'1')->applyFromArray($style);
    }

    $letter = $this->colLetter($col++);
    $sheet->setCellValue($letter.'1', 'Kods');
    $sheet->getColumnDimension($letter)->setWidth(20);
    $sheet->getStyle($letter.'1')->applyFromArray($style);

    $letter = $this->colLetter($col++);
    $sheet->setCellValue($letter.'1', 'Cena');
    $sheet->getColumnDimension($letter)->setWidth(15);
    $sheet->getStyle($letter.'1')->applyFromArray($style);

    $letter = $this->colLetter($col++);
    $sheet->setCellValue($letter.'1', 'Akcijas cena');
    $sheet->getColumnDimension($letter)->setWidth(15);
    $sheet->getStyle($letter.'1')->applyFromArray($style);

    if ($this->shopman->prodscol->properties_assoc['prod_count']['type'] != 'hidden')
    {
      $letter = $this->colLetter($col++);
      $sheet->setCellValue($letter.'1', 'Skaits');
      $sheet->getColumnDimension($letter)->setWidth(15);
      $sheet->getStyle($letter.'1')->applyFromArray($style);
    }

    $letter = $this->colLetter($col++);
    $sheet->setCellValue($letter.'1', 'Slēpts?');
    $sheet->getColumnDimension($letter)->setWidth(15);
    $sheet->getStyle($letter.'1')->applyFromArray($style);

    $row = 3;
    $ids = explode(',', $_GET['ids']);
    foreach ($ids as $cat_id)
    {
      $cat = $this->shopman->catscol->getItemByIdAssoc($cat_id);

      $cond = array(
        'category_id = '.$cat_id
      );
      $prods = $this->shopman->prodscol->queryProducts($cond, 0, 0, 'cat_ind');

      $col = 0;
      $lk = array_keys($langs);

      $letter = $this->colLetter($col++);
      $sheet->setCellValue($letter.$row, 'Kategorija:');
      $sheet->getStyle($letter.$row)->applyFromArray($style);

      $letter = $this->colLetter($col++);
      $sheet->setCellValue($letter.$row, $cat['title_'.$lk[0]]);
      $sheet->getStyle($letter.$row)->applyFromArray($style);

      $row++;

      foreach ($prods as $prod)
      {
        $col = 0;
        $sheet->setCellValue($this->colLetter($col++).$row, $prod['item_id']);

        foreach ($langs as $lang => $lrow)
        {
          $sheet->setCellValue($this->colLetter($col++).$row, $prod['name_'.$lang]);
        }

        $sheet->setCellValue($this->colLetter($col++).$row, $prod['code']);

        $sheet->setCellValue($this->colLetter($col++).$row, $prod['price']);
        $sheet->setCellValue($this->colLetter($col++).$row, $prod['sale_price'] > 0 ? $prod['sale_price'] : '');
        if ($this->shopman->prodscol->properties_assoc['prod_count']['type'] != 'hidden')
        {
          $sheet->setCellValue($this->colLetter($col++).$row, $prod['prod_count']);
        }

        $sheet->setCellValue($this->colLetter($col++).$row, $prod['disabled'] ? 1 : 0);

        $row++;
      }
      $row++;

    }

    require_once($GLOBALS['cfgDirRoot'].'library/PHPExcel/IOFactory.php');

    header("Content-type: application/octet-stream");
    header('Content-Disposition: attachment; filename="veikala_preces_importam.xls"');
    header("Content-Transfer-Encoding: binary");

    $objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
    $objWriter->save('php://output');

    die;
  }


  function exportXML()
  {
    if (!$this->init()) return;

    require_once($GLOBALS['cfgDirRoot'].'library/class.xmlnode.php');
    header('Content-type: text/xml; charset=utf-8');

    $langs = getLanguages();

    $shop = new XMLNode('shop');

    $categories = new XMLNode('categories');
    $shop->addChild($categories);

    $products = new XMLNode('products');
    $shop->addChild($products);

    $ids = explode(',', $_GET['ids']);
    foreach ($ids as $cat_id)
    {
      $cat = $this->shopman->catscol->getItemByIdAssoc($cat_id);


      $catnode = new XMLNode('category');
      $catnode->attribs = array(
        'id' => $cat['item_id'],
      );
      foreach ($langs as $lang => $lrow)
      {
        $catnode->attribs['title_'.$lang] = $cat['title_'.$lang];
      }
      $categories->addChild($catnode);

      $cond = array(
        'disabled = 0',
        'category_id = '.$cat_id
      );
      $prods = $this->shopman->prodscol->queryProducts($cond, 0, 0, 'cat_ind');
      foreach ($prods as $prod)
      {
        $prodnode = new XMLNode('product');
        $prodnode->attribs = array(
          'id' => $prod['item_id'],
          'category_id' => $prod['category_id'],
          'code' => $prod['code'],
        );

        list($price, $old_price, $undiscounted_price) = $this->shopman->srvGetProductPrice($prod, null, $prod['price'], $prod['sale_price']);
        if ($price == $undiscounted_price)
        {
          $prodnode->attribs['price'] = $price;
        }
        else
        {
          $prodnode->attribs['price'] = $undiscounted_price;
          $prodnode->attribs['featured_price'] = $price;
        }

        foreach ($langs as $lang => $lrow)
        {
          $prodnode->attribs['name_'.$lang] = $prod['name_'.$lang];

          $desc = new XMLNode('description_'.$lang);
          $desc->text = $prod['description_'.$lang];
          $desc->cdata = true;
          $prodnode->addChild($desc);

          $desc = new XMLNode('long_description_'.$lang);
          $desc->text = $prod['long_description_'.$lang];
          $desc->cdata = true;
          $prodnode->addChild($desc);
        }


        $products->addChild($prodnode);
      }

    }
/*
    header("Content-type: application/octet-stream");
    header('Content-Disposition: attachment; filename="veikala_preces.xml"');
    header("Content-Transfer-Encoding: binary");
*/
    echo $shop->output(0, true, true);
    die;
  }

  function SetProperties()
  {
    $this->properties = Array(
      "name"        => Array(
        "type"      => "string",
        "label"     => "Name:"
      ),

    );
  }

}




?>
