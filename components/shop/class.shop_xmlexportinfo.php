<?

require_once($GLOBALS['cfgDirRoot']."components/class.component.php");

class shop_xmlexportinfo extends component
{
  function shop_xmlexportinfo($name)
  {
    parent::__construct($name);
    $this->SetProperties();
  }

  function getLanguageStrings()
  {
    return Array(
      'navatlauts' => 'XML eksports jūsu profilam nav ieslēgts.',
      'ip_adrese' => 'Atļautās IP adreses',
      'xml_links' => 'XML links',

    );
  }

  function output()
  {
    $shopman = $this->getServiceByName('shopmanager', true);
    if (!$shopman) return;

    if (!$_SESSION['logged_in']) return;

    if (!$_SESSION['logged_in']['xml_export_enabled'])
    {
      echo $this->LS('navatlauts');
      return;
    }

    if (!$_SESSION['logged_in']['xml_key'])
    {
      $key = generate_random_string(30, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMOPQRSTUVWXYZ0123456789');
      $shopman->userscol->changeItemByIdAssoc($_SESSION['logged_in']['item_id'], array(
        'xml_key' => $key
      ));
      $_SESSION['logged_in']['xml_key'] = $key;
    }

    $xml_url = 'http://'.$_SERVER['SERVER_NAME'].'/products-xml?u='.$_SESSION['logged_in']['item_id'].'&key='.$_SESSION['logged_in']['xml_key'];
?>
  <table>
    <tr>
      <td><?=$this->LS('ip_adrese') ?>:</td>
      <td><?=$_SESSION['logged_in']['xml_allowed_ip'] ?></td>
    </tr>
    <tr>
      <td><?=$this->LS('xml_links') ?>:</td>
      <td><a href="<?=$xml_url ?>" target="_blank"><?=$xml_url ?></a></td>
    </tr>
  </table>
<?
  }

  function SetProperties()
  {
    $this->properties = Array(
      'name' => Array(
        'label'     => 'Name:',
        'type'      => 'str'
      ),

    );
  }
}




?>
