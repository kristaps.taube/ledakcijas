<?php

class LEDAkcijasSearch extends component
{

    public function output()
    {

        $query = isset($_GET['q']) ? $_GET['q'] : '';

        echo $this->render('index', ['query' => $query]);

    }

}