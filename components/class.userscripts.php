<?

include_once("class.component.php");

class userscripts extends component
{
  function userscripts($name)
  {
    parent::__construct($name);
    $this->SetProperties();
  }

  function execute()
  {
    $GLOBALS['head_user_scripts'] = readOption('Free CMS\\head_scripts');
    $GLOBALS['body_user_scripts'] = readOption('Free CMS\\body_scripts');
  }

  function design()
  {
    $url = $this->getComponentDialogLink('dialogScripts', Array(), 'Ziema', 'submit');
    $js = $this->getDesignIconScript($url, 400, 500);
    echo '<input type="button" value="Mainīt" onclick="'.$js.'" />';
  }

  function dialogScripts()
  {
    $form_data = Array(
      'head_scripts' => $this->properties['head_scripts'],
      'body_scripts' => $this->properties['body_scripts'],
    );

    $form_data['head_scripts']['value'] = readOption('Free CMS\\head_scripts');
    $form_data['body_scripts']['value'] = readOption('Free CMS\\body_scripts');

    echo $this->getDialogFormOutput($form_data, 'dialogScriptsSave', 'Mainīt skriptus');
  }

  function dialogScriptsSave()
  {
    $params = array('type' => 'customtext');
    option('Free CMS\\head_scripts', $_POST['head_scripts'], 'Head scripts', $params);
    option('Free CMS\\body_scripts', $_POST['body_scripts'], 'Body scripts', $params);
/*
    $this->setDefaultProperty('head_scripts', $_POST['head_scripts']);
    $this->setDefaultProperty('body_scripts', $_POST['body_scripts']);
*/
  }

  function SetProperties()
  {
    $this->properties = Array(
      'name'            => Array(
        'label'     => 'Name:',
        'type'      => 'str'
      ),

      'head_scripts' => Array(
        'label'     => 'Skripti pirms &lt;/head&gt; taga:',
        "type"      => "customtext",
        "rows"      => "10",
        "cols"      => "50",
      ),

      'body_scripts' => Array(
        'label'     => 'Skripti pirms &lt;/body&gt; taga:',
        "type"      => "customtext",
        "rows"      => "10",
        "cols"      => "50",
      ),

    );
  }
}


?>