<?
//this class extends menuonelevelsimple.
//methods Output and Design are not redeclared, thus methods from menuonelevel will be used

include_once("class.menuoneleveltemplate.php");

class menutwoleveltemplate extends menuoneleveltemplate{

  //Class initialization
  function menutwoleveltemplate($name){
    $this->menuoneleveltemplate($name);
    $this->SetProperties();
  }


  function DisplayMenuItem2($name, $title, $path, $active, $row, $first, $last){
    $this->outvars['name'] = $name;
    $this->outvars['title'] = $title;
    $this->outvars['path'] = $path;
    $this->outvars['active'] = $active;
    $this->outvars['row'] = $row;
    $this->outvars['first'] = $first;
    $this->outvars['last'] = $last;

    return $this->ReturnTemplate("l2template");
  }

  //override OutputSubmenu, which is empty function in menuonelevel class
  function OutputSubmenu($parent, $lev, $p_name, $p_title, $p_path, $p_active, $p_row = Array(), $p_openbranch = false, $p_first=false,$p_last=false){

		#$data = sqlQueryData("SELECT * FROM " . $this->site_id . "_pages WHERE parent=$parent AND enabled=1 AND visible=1 AND in_trash=0 ORDER BY ind");
		$data = $this->getVisibleByParent($parent);

		$first = 1;
		$i = 0;
		$this->outvars['submenuinside'] = '';

		foreach($data as $row){
			$i++;

			if ($first){
				$pageparent = $this->getParent($row['page_id']);
				#$pageparent = sqlQueryValue("SELECT parent FROM  " . $this->site_id . "_pages WHERE page_id=" . $row['page_id']);
				if (!$pageparent){
					$parentpath = "/";
				}else{
					$parentpath = PagePathByID($pageparent, $this->site_id);
				}
			}

      $path = "";
			$name = $row['name'];
			$title = $row['title'];

			if (!$this->design){
				$path = ($name == 'index') ? $parentpath : $parentpath . $name;
			}else{
				$path = 'edit.php?module=pages&site_id='.$this->site_id.'&action=inline_frame_page&page_id='.$row["page_id"];
			}

			$active = ($row['page_id'] == $this->page_id);
			$s = $this->DisplayMenuItem2($name, $title, $path, $active, $row, $first, ($i == count($data)));

			$this->outvars['submenuinside'] .= $s;
			$first = 0;
		}

		#echo $this->outvars['submenuinside'];
		$this->outvars['name'] = $p_name;
		$this->outvars['title'] = $p_title;
		$this->outvars['path'] = $p_path;
		$this->outvars['active'] = $p_active;
		$this->outvars['row'] = $p_row;
		$this->outvars['openbranch'] = $p_openbranch;
		$this->outvars['last'] = $p_last;
		$this->outvars['first'] = $p_first;

		$this->outvars['submenu'] = $this->ReturnTemplate('submenutemplate');

		return $this->outvars['submenu'];
  }

  function CanBeCompiled(){
    if($GLOBALS['developmode'])
      return False;
    else
      return True;
  }

  function CanLoadDesignDynamically(){
    return true;
  }

  function InitFormData(){
    $pages = sqlQueryData("SELECT page_id,name,parent,title FROM " . $this->site_id . "_pages ORDER BY ind");
    ListNodes($pages, $combopages, 0, 0);
    $combopages[count($combopages)] = "-1:(Active page parent)";
    $combopages[count($combopages)] = "-2:(Active page)";
    $combopages[count($combopages)] = "-3:(Specify Level)";
    $this->properties['parent']['lookup'] = $combopages;
  }

  function SetProperties(){
    $this->properties = Array(

      "name"        => Array(
        "type"      => "string",
        "label"     => "Name:"
      ),

      "parent"      => Array(
        "type"      => "list",
        "label"     => "Root:",
        "lookup"    => $combopages
      ),

      "level"     => Array(
          "label"     => "Menu level:",
          "type"      => "string"
        ),

      "menutemplate"    => Array(
        "label"   => "Menu Template:",
        "type"    => "template",
        "rows"      => "8",
        "cols"      => "30",
        "tags"    => "{!menuinside}",
        "samples" => Array(
                       "UL List" => "<ul>{!menuinside}</ul>"),
        "value"   => "[if !menuinside]<ul>{!menuinside}</ul>[/if]"
      ),

      "submenutemplate"    => Array(
        "label"   => "Submenu Template:",
        "type"    => "template",
        "rows"      => "8",
        "cols"      => "30",
        "tags"    => "{!submenuinside}",
        "samples" => Array(
                       "UL List" => "<ul>{!submenuinside}</ul>"),
        "value"   => "[if !submenuinside]<ul>{!submenuinside}</ul>[/if]"
      ),

      "l1template"    => Array(
        "label"   => "L1 item template:",
        "type"    => "template",
        "rows"      => "8",
        "cols"      => "30",
        "tags"    => "{!path};{!name};{!title};{active};{!openbranch};{!first};{!row};{!submenu}",
        "samples" => Array(
                       "UL List" => "<li><a href=\"/{!path}\"[if !openbranch] class=\"active\"[/if]>{!title}</a>[if !openbranch]{!submenu}[/if]</li>"),
        "value"   => "<li><a href=\"/{!path}\"[if !openbranch] class=\"active\"[/if]>{!title}</a>[if !openbranch]{!submenu}[/if]</li>"
      ),

      "l2template"    => Array(
        "label"   => "L2 item template:",
        "type"    => "template",
        "rows"      => "8",
        "cols"      => "30",
        "tags"    => "{!path};{!name};{!title};{!active};{!row};{!first}",
        "samples" => Array(
                       "UL List" => "<li><a href=\"/{!path}\"[if !active] class=\"active\"[/if]>{!title}</a></li>"),
        "value"   => "<li><a href=\"/{!path}\"[if !active] class=\"active\"[/if]>{!title}</a></li>"
      ),

    );

  }

}
