<?
//Serveris.LV Constructor component
//Created by Aivars Irmejs, 2003, aivars@serveris.lv
//Modified by Kristaps Taube, 2014, ktaube@datateks.lv

include_once("class.colitemlist.php");

class bannerlisttemplate extends colitemlist{

  //Class initialization
  function bannerlisttemplate($name){
    parent::__construct($name);
    $this->collectiontype = 'bannerlistdbcollection';
    $this->SetProperties();
  }

  function execute(){

    parent::execute();

    $this->manager = $this->getServiceByName('shopmanager', true);

    if(isset($_GET['showbanner'])){

      $banner = $this->collection->getItemByIDAssoc((int)$_GET['showbanner']);
      if($banner){

        $this->collection->changeItemByIdAssoc($banner['item_id'], array("hits" => $banner['hits'] + 1));
        if($banner['link']){
          header("location: ".$banner['link']);
          die();
        }

      }

    }

  }

  function GetTableData($isdesign = false){

    if(!$this->getProperty("position")) return array();

    $cond = array("position = '".$this->getProperty("position")."'");
    $cond[] = "IF(hit_limit, hit_limit > hits, 1)";
    $cond[] = "IF(view_limit, view_limit > views, 1)";
    $cond[] = "IF(date_start, date_start <= NOW(), 1)";
    $cond[] = "IF(date_end, date_end >= NOW(), 1)";

    $randlimit = intval($this->getProperty("randlimit"));
    if(!$isdesign && $randlimit){
      $banners = $this->collection->GetDBData(array(
        'where' => implode(" AND ", $cond),
        'count' => $randlimit,
        'order' => 'rand()'
      ));
    }else{
      $banners = $this->collection->GetDBData(array('where' => implode(" AND ", $cond)));
    }

    if($isdesign === false){
      $ids = array();
      foreach($banners as $banner){
        $ids[] = $banner['item_id'];
      }

      if(!empty($ids)){
        $this->collection->updateViews($ids);
      }

    }

    return $banners;

  }

  function OutputTable($tdata, $design){

    if($this->name == "Slideshow"){

        if(!$tdata){ // no top banners
          $GLOBALS['slideshow'] = "no_slideshow";
          return;
        }else{
          $GLOBALS['slideshow'] = "has_slideshow";
        }

    }

    $d = '';
    $i = 0;
    foreach($tdata as $key => $row){
      $this->outvars['first'] = ($i == 0);
      $this->outvars['last'] = ($i == count($tdata)-1);
      $d .= $this->OutputRow($row, $design);
      $i++;
    }

    $this->outvars['tableinside'] = $d;

    if($design){
      $this->outvars['addbutton'] = $this->OutputAddButton();
    }
    $this->outvars['design'] = $design;
    return $this->ReturnTemplate('tabletemplate');
  }

  function OutputRow($row, $design, &$lastletter=''){
    if (!$row['image']) return '';

    if($design){
      $this->outvars['editbuttons'] = $this->OutputEditButtons($row['item_id']);
    }

    if (!$row['width'] || !$row['height']){
      list($fw, $fh) = getimagesize(GetFilePathFromLink($this->site_id, $row['image']));

      if ($fw && $fh){
        if (!$row['width'] && !$row['height']){
          $row['width'] = $fw;
          $row['height'] = $fh;
        }else if (!$row['width']){
          $row['width'] = round($fw * $row['height'] / $fh);
        }else if (!$row['height']){
          $row['height'] = round($fh * $row['width'] / $fw);
        }

        $this->collection->changeItemByIdAssoc($row['item_id'], Array(
          'width' => $row['width'],
          'height' => $row['height']
        ));
      }
    }

    $row['link'] = "?showbanner=".$row['item_id'];

    $this->outvars['row'] = $row;
    $filename = $row['image'];
    $path_info = pathinfo($filename);
    $ext = $path_info['extension'];
    $script = trim($row['script']);

    if (!empty($script)){
      $this->outvars['flash'] = $script;
    }else if($ext == 'swf'){
      $this->outvars['flash'] = '
      <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
      codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab
      #version=7,0,0,0" width="'.$row['width'].'" height="'.$row['height'].'">
        <param name=movie value="'.$filename.'?clickTAG='.htmlspecialchars($row['link']).'">
        <param name=play VALUE=true>
        <param name=loop VALUE=true>
        <param name=quality value=high>
        <embed src="'.$filename.'?clickTAG='.htmlspecialchars($row['link']).'" quality=High
      pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-
      shockwave-flash" width="'.$row['width'].'" height="'.$row['height'].'">
        </embed>
      </object>

      ';
    }else
    {
      $this->outvars['flash'] = '';
    }

    return $this->ReturnTemplate('rowtemplate');

  }


  function CanBeCompiled()
  {
    if(!$this->getProperty("randlimit"))
      return true;
    else
      return false;
  }


  function CanLoadDesignDynamically()
  {
    return true;
  }

  function SetProperties()
  {
    $this->properties = Array(

      "name"        => Array(
        "label"     => "Name:",
        "type"      => "string"
      ),

    "collectionID" => Array(
      "label"       => "Collection:",
      "type"        => "collection",
      "collectiontype" => $this->collectiontype,
      "lookup"      => GetCollections($this->site_id, $this->collectiontype),
      "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
      "dialogw"     => "600"
    ),

    "tabletemplate"    => Array(
        "label"   => "Table Template:",
        "type"    => "template",
        "rows"      => "8",
        "cols"      => "30",
        "tags"    => "{!tableinside};{!addbutton}",
        "samples" => Array(
                       "Normal" => "<div id=\"banners\">{!tableinside}{!addbutton}</div>"),
        "value"   => "<div>{!tableinside}{!addbutton}</div>"
      ),

    "rowtemplate"    => Array(
        "label"   => "Row Template:",
        "type"    => "template",
        "rows"      => "8",
        "cols"      => "30",
        "tags"    => "{!row};{!row.link};{!row.target};{!row.description};{!row.image};{!editbuttons}{!first};{!last}",
        "samples" => Array(
                       "Normal" => '<p>[if !flash]{!flash}[else]<a href="{!row.link}" target="{!row.target}"><img alt="{!row.description}" src="{!row.image}" /></a>[/if]{!editbuttons}</p>'),
        "value"   => '<p>[if !flash]{!flash}[else]<a href="{!row.link}" target="{!row.target}"><img alt="{!row.description}" src="{!row.image}" /></a>[/if]{!editbuttons}</p>'
      ),


      "randlimit"    => Array(
        "label"     => "Limit (random banners):",
        "type"      => "string"
      ),

      "width"    => Array(
        "label"     => "width:",
        "type"      => "string"
      ),

      "height"    => Array(
        "label"     => "height:",
        "type"      => "string"
      ),

      "position"        => Array(
        "label"     => "Position:",
        "type"      => "list",
        'lookupassoc' => array('left' => 'Left', 'top' => 'Top'),
      ),

    );

  }

}
