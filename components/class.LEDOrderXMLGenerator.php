<?php

use Constructor\WebApp;

class LEDOrderXMLGenerator extends component
{

    public function __construct($name)
    {

        parent::__construct($name);
        $this->init();

    }

    public function init()
    {

        $this->vat = option('Shop\\pvn', null, 'PVN (%)', null, 22) / 100;
        $this->site = WebApp::$app->getSite();
        $this->save_path = $this->site['dirroot'].'files/rekini_xml/';

    }

    public function save($filename, $xml)
    {

        file_put_contents($this->save_path.$filename, $xml);

    }

    public function generate($order_id)
    {

        $order = $this->orders->getById($order_id);
        if(!$order) return false;

        $xml_order = $this->getXMLOrder($order);

        $xml = new \SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><AccrualPZ></AccrualPZ>');
        $header_node = $xml->addChild('PZHeader');

        foreach($xml_order['PZHeader'] as $key => $value){
            $header_node->addChild($key, htmlspecialchars($value));
        }

        $Ieraksti = $xml->addChild("Ieraksti");
        foreach($xml_order['Ieraksti'] as $item){

            $Ieraksts = $Ieraksti->addChild("Ieraksts");
            foreach($item as $key => $value){
                $Ieraksts->addChild($key, htmlspecialchars($value));
            }

        }

        return $xml->asXML();

    }

    private function getXMLOrder($order)
    {

        $orderdata = unserialize($order['order_data']);
        $allfields = $orderdata['allfields'];

        $xml_order = [];
        $header = [];

        $header['WEB'] = $order['item_id'];
        $header['Type'] = 6;
        $header['Nr'] = $order['num'];
        $header['Datums'] = $order['datums'];
        $header['PartnNosaukums'] = $order['pasutitajs'];
        $header['PartnKods'] = $allfields['payer'] == 'legal' ? $allfields['registration_nr'] : '';
        $header['Adrese'] = '';

        $header['Summa'] = round($order['kopeja_summa'] / (1 + $this->vat), 2);
        $header['PVNSumma'] = $order['kopeja_summa'] - $header['Summa'];
        $header['GalaSumma'] = $header['Summa'] + $header['PVNSumma'];
        $header['Valuta'] = 'EUR';

        $xml_order['PZHeader'] = $header;

        $items = [];
        foreach($this->order_products->getOrderProducts($order['item_id']) as $product){

            $item = [];

            $item['ArticleId'] = $product['code'];
            $item['Artikuls'] = $product['code'];
            $item['EAN'] = $product['ean'];
            $item['Nosaukums'] = $product['name_lv'];
            $item['Mervieniba'] = 'gab.';
            $item['PrecuCena'] = (float)round($product['base_price'] / (1 + $this->vat), 2);
            $item['CenaArPVN'] = (float)$product['base_price'];
            $item['Atlaide'] = (float)($product['base_price'] - $product['price']);
            $item['Cena'] = (float)$product['price'];
            $item['Daudzums'] = $product['count'];
            $item['Summa'] = (float)($product['count'] * $product['price']);
            $item['Nodoklis'] = 'PVN '.number_format($this->vat * 100).'%';
            $item['Likme'] = $this->vat * 100;

            $items[] = $item;

        }

        $xml_order['Ieraksti'] = $items;

        return $xml_order;

    }

    public function addProperties()
    {

        return [
            "orders" => [
                "label"       => "Order collection:",
                "type"        => "collection",
                "collectiontype" => "EVeikalsOrderCollection",
            ],
            "order_products" => [
                "label"       => "Order product collection:",
                "type"        => "collection",
                "collectiontype" => "shoporderprodcollection",
            ],
        ];

    }

}