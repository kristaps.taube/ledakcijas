<?php
/**
*
*  Title: E-Veikals Frontend Shop
*  Author: Kristaps Taube <ktaube@datateks.lv>
*  Date: 14.11.2013
*  Project: E-Veikals
*
*/

include_once("class.component.php");

class EVeikalsFrontendShop extends component{

    private $manager;
    private $product_page_size;
    private $product_page;
    private $product_order = "auto";
    public $default_product_image = "/images/html/blank_product.png";
    public $default_category_image = "/images/html/blank_product.png";
    public $search_in;
    public $autocomplete_search_in;

    function __construct($name = false)
    {

        parent::__construct($name);
        $this->search_in = array("name_".$this->lang, "code");
        $this->autocomplete_search_in = array("name_".$this->lang, "code");
        $this->initOptions();
        $this->getServices();

    }

    function visibleService(){
        return true;
    }

  function getServices(){

    if($this->gotServices === true) return true;

    $this->CatalogSearch = EVeikalsCatalogSearch::getInstance();
    $this->manager = shopmanager::getInstance();

    $this->ShopFilter = EVeikalsShopFilter::getInstance();
    if($this->ShopFilter){
      $this->ShopFilter->init();
    }

    $this->CurrencyMenu = EVeikalsCurrencyMenu::getInstance();
    $this->gotServices = true;

    return true;

  }

  function execute(){

    if($this->use_soc_discount){
      if($_GET['action'] == 'applySocialDiscount'){
        $_SESSION['soc_discount'] = true;
        die();
      }

      if($_GET['action'] == 'removeSocialDiscount'){
        $_SESSION['soc_discount'] = false;
        die();
      }
    }

    $this->product_page = (isset($_GET['p']) && (int)$_GET['p'] > 1)? (int)$_GET['p'] : 1;

    if($_GET['youtube']){
      ?>
        <iframe width="560" height="315" src="http://www.youtube.com/embed/<?=$_GET['youtube']?>" frameborder="0" allowfullscreen></iframe>
      <?
      die();

    }

    if($_POST['action'] == 'rate' && $this->manager->user){
      $this->ratings->rateProduct($this->manager->user['item_id'], $this->manager->prod['item_id'], (int)$_POST['score']);
      $rating = $this->ratings->getRating($this->manager->prod['item_id']);
      die(round($rating['rating']));
    }

    $this->product_page_size = $_SESSION['shop']['product_page_size']? $_SESSION['shop']['product_page_size'] : $this->product_page_size;
    $this->product_order = $_SESSION['shop']['product_order']? $_SESSION['shop']['product_order'] : $this->sort_products_by;
    $this->list_type = $_SESSION['shop']['list_type']? $_SESSION['shop']['list_type'] : $this->default_product_layout;

  }


    function processMetaTags(){
        $title_tag_suffix = readOption('title_tag_suffix');

        if($this->manager->prod){

            $GLOBALS['meta_keywords'] .= $this->manager->prod['keywords_' . $this->lang];
            $GLOBALS['meta_description'] .= strip_tags($this->manager->prod['meta_description_'.$this->lang]);
            $GLOBALS['additional_header_tags'] .= $this->manager->prod['additional_header_tags_'.$this->lang];
            $GLOBALS['page_title'] = $this->manager->prod['name_'.$this->lang];
            $GLOBALS['force_title'] = $this->manager->prod['name_'.$this->lang].$title_tag_suffix;

        }elseif($this->manager->cat){

            $GLOBALS['meta_description'] .= strip_tags($this->manager->cat['description_'.$this->lang]);
            $GLOBALS['meta_keywords'] .= $this->manager->cat['keywords_' . $this->lang];
            $GLOBALS['additional_header_tags'] = $this->manager->cat['additional_header_tags_'.$this->lang];
            $GLOBALS['force_title'] = $this->manager->cat['title_tag_'.$this->lang] ? $this->manager->cat['title_tag_'.$this->lang].$title_tag_suffix : $this->manager->cat['title_'.$this->lang].$title_tag_suffix;

        }

    }

  
  function indexData($lastindextime){

    $data = array();
    $check = $this->initCollections();

    if($this->name == 'shop'){

      $products = $this->prods->getDBData(array("where" => "disabled = 0"));

      foreach($products as $p){

        $data_text = $p['name_'.$this->lang];
        $data_text .= " ".strip_tags($p['description_'.$this->lang]);
        $data_text .= " ".strip_tags($p['long_description_'.$this->lang]);

        $data[$this->prods->collection_id][$p['item_id']] = array(
          'query' => $this->manager->getProductURL($p, '', true),
          'title' => $p['name_'.$this->lang],
          'data' => $data_text
        );

      }

      $categories = $this->cats->getDBData(array("where" => "disabled = 0"));

      foreach($categories as $c){
        $data[$this->cats->collection_id][$c['item_id']] = array(
          'query' => $this->manager->getCategoryURL($c['item_id'], '', true),
          'title' => $c['title_'.$this->lang],
          'data' => $c['name_'.$this->lang].' '.strip_tags($c['description_'.$this->lang])
        );
      }

    }

    return $data;

  }

  function initOptions(){

    $this->product_page_size = option('Shop\\products_in_page', null, 'Products in page', array(
      'type' => 'list',
      'value_on_create' => true,
      'fieldparams' => array(
        'lookupassoc' => array('12' => '12', '24' => '24', '36' => '36', '48' => '48', '96' => '96')
      )
    ));

    $this->sort_products_by = option('Shop\\sort_products_by', null, 'Sort products by', array(
      'type' => 'list',
      'fieldparams' => array(
        'lookupassoc' => array(
          'auto' => 'Manually',
          'alphabet' => 'Name',
          'cheapest' => 'Price ascending',
          'expensive' => 'Price descending',
        )
      )
    ));

    $this->use_watermark = option("Shop\\use_watermark", null, "Lietot ūdenszīmi", Array("is_advanced" => true, "type" => 'check'), null, $this->site_id);
    $this->watermark_logo = option('Shop\\watermark', null, 'Ūdenszīme', Array("is_advanced" => true, 'type' => 'dialog', 'componenttype' => get_class($this) ) );

    $this->use_product_ratings = option("Shop\\use_product_ratings", null, "Lietot produktu vērtējumus", Array("is_advanced" => true, "type" => 'check'), null, $this->site_id);
    $this->show_in_stock = option("Shop\\show_in_stock", null, "Rādīt produktu atlikumus", Array("is_advanced" => false, "type" => 'check'), null, $this->site_id);

    $this->use_soc_discount = option('Shop\\use_soc_discount', null, 'Use Soc. discount', Array("is_advanced" => true, 'type' => 'check'));
    $this->soc_discount = option("Shop\\soc_discount", NULL, "Soc. discount", array("is_advanced" => true), 10, $this->site_id);

    $this->cat_output_type = option('Shop\\cat_output_type', null, 'Category output type', array(
      'type' => 'list',
      'value_on_create' => 'categories',
      'fieldparams' => array(
        'lookupassoc' => array(
          'categories' => 'Categories',
          'products' => 'Products'
        )
      )
    ));

    $this->default_product_layout = option('Shop\\default_product_layout', null, 'Default product layout', array(
      'type' => 'list',
      'value_on_create' => 'grid',
      'fieldparams' => array(
        'lookupassoc' => array(
          'grid' => 'Grid',
          'list' => 'List'
        )
      )
    ));

  }

  static function processOptionBeforeDisplay($path, $opt, &$data){

    switch ($path){
      case 'Shop\watermark':
        $data['dialog'] = '?module=dialogs&action=filex&site_id='.$GLOBALS['site_id'].'&view=thumbs';
        break;
    }
  }

  // this function checks if none of the parent categories arent disabled
  function validateShopPath(){

    foreach ($this->manager->cat_path as $cat){
      if ($cat['disabled'] || $cat['uncategorized_item_cat']){
        header('Location: /');
        die;
      }
    }

  }

  function design(){}

  function output(){

    $this->manager = $this->manager ? $this->manager : shopmanager::getInstance();

    if(!$this->manager){
      echo $this->name.": Can't get manager. Where is he?<br />";
      return;
    }

    if($this->manager->prod && !$this->manager->prod['disabled']){

      $this->outputProduct($this->manager->prod);

    }elseif($this->manager->cat && $this->cat_output_type == 'categories' && !$this->CatalogSearch->search){

      $this->outputSubCategories($this->manager->cat);

    }elseif($this->manager->cat && $this->cat_output_type == 'products' || $this->CatalogSearch->search){

      $this->outputProductList($this->manager->cat);

    }else{  // start

      if($this->cat_output_type == 'categories'){
        $this->outputSubCategories(0);
      }else{
        $this->outputProductList();
      }

    }

  }

  function outputProduct($prod){

  }

  function outputSubCategories($cat = false){

    /** HERE YOU SHOULD PUT CATEGORY LIST OUTPUT **/
    $sub_categories = $this->cats->getDBData(array("where" => "disabled = 0 AND uncategorized_item_cat != 1 AND parent = ".(($cat)? $cat['item_id'] : 0), "order" => "ind", "assoc" => true));

    if($sub_categories === false || empty($sub_categories)){
      $this->outputProductList($cat);
      return;
    }

    $title = $this->manager->cat ? $this->manager->cat['title_'.$this->lang] : $this->l("Katalogs");

    $i = 0;
    ?>
    <div id='Catalog'>
			<? if($this->manager->cat['desc_'.$this->lang]){ ?>
      <div id='CatDescription'>
      <?=$this->manager->cat['desc_'.$this->lang]?>
			</div>
			<? } ?>
      <div class='box'>
        <div id='CategoryList'>
          <div class='header'>
            <div class='currency_menu'>
              <?=$this->CurrencyMenu->output() ?>
            </div>
          </div>
          <h1><?=$title?></h1>
          <div class='list'>
            <? foreach($sub_categories as $cat){ ?>
            <div class='category <?=(((++$i) % 4 == 0))? "last" : "" ?>'>
              <div class='image'>
                <a href='<?=$this->manager->getCategoryURL($cat['item_id'])?>'><img class='valign_p' src='<?=getThumbUrl($cat['image']? $cat['image'] : $this->default_category_image, 150, 110, 6)?>' /></a>
              </div>
              <div class='link'>
                <a href='<?=$this->manager->getCategoryURL($cat['item_id'])?>'><span class='valign_p'><?=$cat['title_'.$this->lang]?> (<?=$this->cats->getCategoryProductCount($cat['item_id'], $this->prods)?>)</span></a>
              </div>
            </div>
            <? } ?>
            <div class='cb'></div>
          </div>
        </div>
      </div>
    </div>
    <?

  }

  function outputProductsGridList($prods){

    ?>
    <div class='grid'>
    <? foreach($prods as $prod){ ?>
      <?
        if($prod['shortcut']){
            $prod = $this->prods->getItemByIDAssoc($prod['shortcut']);
        }

        if($this->use_product_ratings) $rating = round($this->ratings->getRating($prod['item_id']));
        $featured = $this->manager->isProductFeatured($prod);
        $new = $this->manager->isProductNew($prod);
        $top = $this->manager->isProductTop($prod);
        $image = $this->manager->getProductPicture($prod);
        $image = $image? $image : $this->default_product_image;
        $can_add = $this->manager->canAddProductFromList($prod);
        $has_discount = ($featured && $prod['sale_price'] && $prod['sale_price'] != '0.00' && $prod['sale_price'] != $prod['price'])? true : false;
        $price = ($has_discount)? $prod['sale_price']  : $prod['price'];
        $discounted_price = $prod['price'];

        if($prod['has_prices']){
            $prices = $this->prices->getPricesByProduct($prod['item_id']);
            if($prices){
                $min_price_row = array();

                foreach($prices as $price_row){

                    $has_discount = ($featured && $price_row['saleout_price'] && $price_row['saleout_price'] != '0.00' && $price_row['saleout_price'] != $price_row['price'])? true : false;
                    $price = ($has_discount)? $price_row['saleout_price']  : $price_row['price'];
                    $price_row['final_price'] = $price;

                    if(empty($min_price_row) || $min_price_row['final_price'] > $price_row['final_price']){
                        $min_price_row = $price_row;
                    }

                }

                $price = $min_price_row['final_price'];
                $discounted_price = $min_price_row['price'];

            }
        }

      ?>
      <div class='product <?=$featured? "featured" : "" ?> <?=$top? "top" : "" ?> <?=$new? "new" : "" ?> <?=(((++$i) % 4 == 0))? "last" : "" ?>'>
        <div class='image'>
          <div class='top'></div>
          <div class='new'></div>
          <div class='featured'></div>
          <div class='overlay'></div>
          <? if($this->use_product_ratings){ ?>
          <div class='ratings'>
            <? if($rating > 0){ ?>
              <? for($j = 1; $j <= 5; $j++){ ?>
                <img src='/images/html/rating_star_<?=($rating >= $j)? 'full' : "empty"?>.png' alt='<?=hsc(str_replace("[rating]", $j, $this->l("Rating: [rating]")))?>' />
              <? } ?>
            <? } ?>
          </div>
          <? } ?>
          <img src='<?=getThumbUrl($image, 150, 110, 6)?>' alt='<?=hsc($prod['name_'.$this->lang])?>' class='valign'/>
        </div>
        <div class='info'>
          <div class='price <?=($has_discount)? "has_discount" : ""?>'>
            <? if($has_discount){ ?>
            <span class="discounted_price"><?=number_format($discounted_price / $_SESSION['currency']['rate'], 2)?> <?=$_SESSION['currency']['label']?></span><br />
            <? } ?>
            <span><?=number_format($price / $_SESSION['currency']['rate'], 2)?> <?=$_SESSION['currency']['label']?></span>
          </div>
          <div class='name'><a href='<?=$this->manager->getProductURL($prod)?>'><?=$prod['name_'.$this->lang]?></a></div>
          <div class='actions <?=$can_add? "" : "no_add"?>'>
            <a class='view' href='<?=$this->manager->getProductURL($prod)?>' title='<?=hsc($this->l("Skatīt"))?>'><?=$this->l("Skatīt")?></a>
            <? if($can_add){ ?>
            <a class='buy' href='<?=$this->manager->getProductURL($prod)?>?action=buy&amp;product=<?=$prod['item_id']?>' title='<?=hsc($this->l("Pirkt"))?>'><?=$this->l("Pirkt")?></a>
            <? } ?>
            <div class='cb'></div>
          </div>
        </div>
      </div>
    <? } ?>
    <div class='cb'></div>
    </div>
    <?

  }

  function outputProductsListList($prods){

    ?>
    <div class='list'>
    <? foreach($prods as $prod){ ?>
      <?

      	if($prod['shortcut']){
					$prod = $this->prods->getItemByIDAssoc($prod['shortcut']);
				}

        if($this->use_product_ratings) $rating = round($this->ratings->getRating($prod['item_id']));
        $featured = $this->manager->isProductFeatured($prod);
        $new = $this->manager->isProductNew($prod);
        $top = $this->manager->isProductTop($prod);
        $image = $this->manager->getProductPicture($prod);
        $image = $image? $image : $this->default_product_image;
        $can_add = $this->manager->canAddProductFromList($prod);
        $has_discount = ($featured && $prod['sale_price'] && $prod['sale_price'] != '0.00' && $prod['sale_price'] != $prod['price'])? true : false;
        $price = ($has_discount)? $prod['sale_price']  : $prod['price'];
        $discounted_price = $prod['price'];

				if($prod['has_prices']){
					$prices = $this->prices->getPricesByProduct($prod['item_id']);
        	if($prices){
        		$min_price_row = array();
						foreach($prices as $price_row){

              $has_discount = ($featured && $price_row['saleout_price'] && $price_row['saleout_price'] != '0.00' && $price_row['saleout_price'] != $price_row['price'])? true : false;
							$price = ($has_discount)? $price_row['saleout_price']  : $price_row['price'];
              $price_row['final_price'] = $price;

            	if(empty($min_price_row) || $min_price_row['final_price'] > $price_row['final_price']){
            		$min_price_row = $price_row;
            	}

						}

						$price = $min_price_row['final_price'];
						$discounted_price = $min_price_row['price'];

	        }
				}

			?>
      <div class='product <?=$featured? "featured" : "" ?> <?=$top? "top" : "" ?> <?=$new? "new" : "" ?>'>
        <div class='image'>
          <a href='<?=$this->manager->getProductURL($prod)?>' class='valign' title='<?=hsc($prod['name_'.$this->lang])?>'><img src='<?=getThumbUrl($image, 130, 105, 6)?>' alt='<?=hsc($prod['name_'.$this->lang])?>' /></a>
        </div>
        <div class='info'>
          <div class='featured'></div>
          <div class='top'></div>
          <div class='new'></div>
					<? if($this->use_product_ratings){ ?>
          <div class='ratings'>
            <? if($rating > 0){ ?>
              <? for($j = 1; $j <= 5; $j++){ ?>
                <img src='/images/html/rating_star_<?=($rating >= $j)? 'full' : "empty"?>.png' alt='<?=hsc(str_replace("[rating]", $j, $this->l("Rating: [rating]")))?>' />
              <? } ?>
            <? } ?>
          </div>
					<? } ?>
          <div class='name'><a href='<?=$this->manager->getProductURL($prod)?>' title='<?=hsc($prod['name_'.$this->lang])?>'><?=$prod['name_'.$this->lang]?></a></div>
          <div class="small_text"><?=$prod['small_text_'.$this->lang]?></div>
        </div>
        <div class='actions <?=$can_add? "" : "no_add"?>'>
          <div class='old_price'>
            <? if($has_discount){ ?>
              <?=number_format($discounted_price / $_SESSION['currency']['rate'], 2)?> <?=$_SESSION['currency']['label']?>
            <? } ?>
          </div>
          <div class='price <?=$can_add? "" : "no_add"?>'>
            <?=number_format($price / $_SESSION['currency']['rate'], 2)?> <?=$_SESSION['currency']['label']?>
          </div>
          <div class='buttons'>
            <a class='view' href='<?=$this->manager->getProductURL($prod)?>' title='<?=hsc($this->l("Skatīt"))?>'><?=$this->l("Skatīt")?></a>
            <? if($can_add){ ?>
            <a class='buy' href='?action=buy&amp;product=<?=$prod['item_id']?>' title='<?=hsc($this->l("Pirkt"))?>'><?=$this->l("Pirkt")?></a>
            <? } ?>
          </div>
        </div>
        <div class='cb'></div>
      </div>
    <? } ?>

    </div>
    <?

  }

  function MobileMenu(){

    $path = $this->manager->cat_path;

    if(empty($path)) return;

    ?>
    <div id="MobileCatalogMenu">
      <ul class='menu'>
        <li class='level0'>
          <a href='<?=$this->manager->getCategoryURL($this->manager->cat['item_id'])?>' title='<?=hsc($this->manager->cat['title_'.$this->lang])?>'><?=$this->manager->cat['title_'.$this->lang]?></a>
        </li>
      </ul>
    </div>
    <?

    return;

    ?>
    <div id="MobileCatalogMenu">
      <ul class='menu'>
        <? foreach($path as $item){ ?>
        <li class='level<?=$item['lvl']?>'>
          <a href="<?=$this->manager->getCategoryURL($item['item_id'])?>">
            <?=$item['title_'.$this->lang]?>
            <? if($item['lvl']){ ?>
            (<?=$this->cats->getCategoryProductCount($item['item_id'], $this->prods)?>)
            <? } ?>
          </a>
        </li>
        <? } ?>
      </ul>
    </div>
    <?

  }

  function MobileFilters(){

    $active = $this->ShopFilter->hasActiveFilter();

    ?>
    <div id="MobileFilters" class="<?=$active? "active" : ""?>">
      <div class="title"><?=$this->l("Filtrs")?></div>
      <div class="filters">
        <div class='inner_wrap'>
        <? $this->ShopFilter->outputMobileFilters(); ?>
        </div>
      </div>
    </div>
    <?

  }

  function outputProductList($cat = false){

    $cat_id = $cat? $cat['item_id'] : 0;

    $cats = $this->cats->getActivePredecessorIds($cat_id);
    $cats[] = $cat_id; // adding current category

    /** HERE YOU SHOULD PUT PRODUCT LIST OUTPUT **/
    $prods = $this->getProducts($cat_id ? $cats : false);

    // if there is one product in search results, open it!
    if($this->CatalogSearch->needle && $prods && count($prods) == 1){
      header("location:".$this->manager->getProductURL($prods[0]));
      die();
    }

    $params = array();
    if($_GET['p']){ $params[] = "p=".urlencode($_GET['p']); }
    if($_GET['search']){ $params[] = "search=".urlencode($_GET['search']); }
    $url = (!empty($params))? "&".implode("&", $params) : "";

    $i = 0;
    ?>
    <div id='Catalog'>
    <? if($this->manager->cat['desc_'.$this->lang]){ ?>
      <div id='CatDescription'>
      <?=$this->manager->cat['desc_'.$this->lang]?>
        </div>
        <? } ?>
      <div class='box'>
        <div id='ProductList'>
          <? if($this->manager->mobile_session === true){ ?>
            <? $this->MobileMenu(); ?>
            <? $this->MobileFilters() ?>
          <? } ?>
          <div class='header'>
            <div class='currency_menu'>
              <?=$this->CurrencyMenu->output() ?>
            </div>
            <div class='options'>
              <span class='fl'><?=$this->l("Skatīt vienlaicīgi")?>:</span>
              <div class='perpage_wrap'>
                <select name='per_page' class='styled'>
                  <option value='12' <?=($this->product_page_size == 12)? "selected='selected'" : ''?>>12</option>
                  <option value='24' <?=($this->product_page_size == 24)? "selected='selected'" : ''?>>24</option>
                  <option value='36' <?=($this->product_page_size == 36)? "selected='selected'" : ''?>>36</option>
                  <option value='48' <?=($this->product_page_size == 48)? "selected='selected'" : ''?>>48</option>
                  <option value='96' <?=($this->product_page_size == 96)? "selected='selected'" : ''?>>96</option>
                </select>
              </div>
              <div class='order_wrap'>
                <select name='product_order' class='styled'>
                  <option value='auto'><?=$this->l("Automātiski")?></option>
                  <option value='cheapest' <?=($this->product_order == 'cheapest')? "selected='selected'" : ""?>><?=$this->l("Lētākās vispirms")?></option>
                  <option value='expensive' <?=($this->product_order == 'expensive')? "selected='selected'" : ""?>><?=$this->l("Dārgākās vispirms")?></option>
                  <option value='alphabet' <?=($this->product_order == 'alphabet')? "selected='selected'" : ""?>><?=$this->l("Pēc alfabēta")?></option>
                </select>
                <div class='cb'></div>
              </div>
              <div class='list_type'>
                <a href='?action=change_list_type&amp;type=grid<?=$url?>' class='grid <?=$this->list_type == 'grid'? "active" : ""?>' title='<?=hsc($this->l("Režģis"))?>'><span></span></a>
                <a href='?action=change_list_type&amp;type=list<?=$url?>' class='list <?=$this->list_type == 'list'? "active" : ""?>' title='<?=hsc($this->l("Saraksts"))?>'><span></span></a>
                <div class='cb'></div>
              </div>
            </div>
            <div class='cb'></div>
          </div>
          <div class='pagination'>
            <?=$this->outputPagination()?>
          </div>

          <? if($prods && count($prods)){ ?>
            <? if($this->list_type == 'grid'){ ?>
              <? $this->outputProductsGridList($prods) ?>
            <? }else{ ?>
              <? $this->outputProductsListList($prods) ?>
            <? } ?>
          <? }else{ ?>
            <div class='no_products'><?=$this->l("Nav produktu ko attēlot")?></div>
          <? } ?>
          <div class='pagination bottom'>
            <?=$this->outputPagination(false, false, true)?>
          </div>

        </div>
      </div>
    </div>
    
    <? if($this->manager->cat['desc2_'.$this->lang]){ ?>
    <div id='CatDescription2'>
      <?=$this->manager->cat['desc2_'.$this->lang]?>
    </div>
	<? } 
  }

  // $cat should be integer or array of integers
  function getProducts($cat){

    $cond = array("disabled = 0");
    $params = array();

    $disabled_cat_ids = $this->cats->getInactiveCatIds();
        if($disabled_cat_ids){
        $cond[] = "category_id NOT IN(".implode(",", $disabled_cat_ids).")";
    }

    if(is_array($cat)){
      $cond[] = "category_id IN (".implode(", ", $cat).")";
      $cat_for_filter = $cat;
    }elseif($cat){
      $cond[] = "category_id = ".$cat;
      $cat_for_filter = array($cat);
    }

    $filtered_ids = EVeikalsShopFilter::getInstance()->filterProducts($cat_for_filter);
    if($filtered_ids !== false){
      if(!empty($filtered_ids)){
        $cond[] = "item_id IN(".implode(", ", $filtered_ids).")";
      }else{ // no match
        $cond[] = "0";
      }
    }

    if(EVeikalsCatalogSearch::getInstance()->needle){

      $search_cond;
      foreach($this->search_in as $in){
        $search_cond[] = $in." LIKE :needle";
      }

      $params[':needle'] = '%'.EVeikalsCatalogSearch::getInstance()->needle.'%';

      if(!empty($search_cond)){
        $cond[] = "(".implode(" OR ", $search_cond).")";
        $cond[] = "shortcut = 0"; // no shortcuts in search results
      }

    }

    switch($_SESSION['shop']['product_order']){
      case 'cheapest': $order = "IF(lowest_volume_price, lowest_volume_price, p.price) ASC"; break;
      case 'expensive': $order = "IF(lowest_volume_price, lowest_volume_price, p.price) DESC"; break;
      case 'alphabet': $order = "name_".$this->lang; break;
      default: $order = "cat_ind";
    }

    $this->total_product_count = DB::GetValue("SELECT COUNT(*) FROM `".$this->prods->table."` WHERE ".implode(" AND ", $cond), $params);
    $this->product_pages = ceil($this->total_product_count / $this->product_page_size);

    // if page size has been changed and now we are out of borders
    $this->product_page = ($this->product_page <= $this->product_pages)? $this->product_page : 1;

    $prods = DB::GetTable("
        SELECT p.*, IF(p.has_prices, (SELECT min(IF(p.featured AND pr.saleout_price, saleout_price, price)) FROM `".$this->prices->table."` AS pr WHERE pr.product_id = if(p.shortcut, p.shortcut, p.item_id)), 0) AS lowest_volume_price
        FROM `".$this->prods->table."` AS p
        WHERE ".implode(" AND ", $cond)."
        ORDER BY ".$order."
        LIMIT ".$this->product_page_size * ($this->product_page - 1).",".$this->product_page_size."
    ", $params);

    foreach($prods as &$prod){
      list($prod['price'], $prod['old_price'], $prod['undiscounted_price']) = shopmanager::getInstance()->getProductPrice($prod);
    }

    return $prods;

  }

  function outputPagination($count = false, $active = false, $bottom = false){

    if($this->total_product_count <= 0) return;

    $count = $count ? $count : $this->product_pages;
    $active = $active ? $active : $this->product_page;

    if($count == 1) return;

    $url = $_GET['search']? "search=".$_GET['search']."&" : "";

    $prev_link = "?".$url."p=".($this->product_page - 1);
    $next_link = "?".$url."p=".($this->product_page + 1);

    $end = ($active-1)*$this->product_page_size+$this->product_page_size;
    $end = ($end < $this->total_product_count)? $end : $this->total_product_count;

    $text = $this->l("Uzrādās [from]-[to] no [total] precēm");
    $text = str_replace("[from]", ($active - 1)*$this->product_page_size + 1, $text);
    $text = str_replace("[to]", $end, $text);
    $text = str_replace("[total]", $this->total_product_count, $text);

    ?>
    <? if(!$bottom){ ?>
    <div class='text'><?=$text?></div>
    <? } ?>
    <div class='pages'>
      <? if($active != 1){ ?><a href='<?=$prev_link?>' class='prev' title='<?=hsc($this->l("Iepriekšējā lapa"))?>'>&lt;</a><? } ?>

      <? if($count >= 10){ ?>

        <? for($i = 1; $i != 4 ; $i++){ ?>
        <a href='?p=<?=$i?>' class='num <?=($active == $i)? "active" : ""?>' title='<?=hsc(str_replace("[page]", $i, $this->l("Lapa: [page]")))?>'><?=$i?></a>
        <? } ?>

        <? if($active >= 4 && $active < ($count - 3)){ ?>
          <? if($active >4){ ?><span class='plenty'>...</span> <? } ?>
          <? for($i = $active; $i == $active; $i++){ ?>
          <a href='?<?=$url?>p=<?=$i?>' class='num <?=($active == $i)? "active" : ""?>' title='<?=hsc(str_replace("[page]", $i, $this->l("Lapa: [page]")))?>'><?=$i?></a>
          <? } ?>

        <? } ?>
        <? if($i < $count-3){ ?><span class='plenty'>...</span><? } ?>
        <? for($i = $count - 2; $i <= $count ; $i++){ ?>
        <a href='?<?=$url?>p=<?=$i?>' class='num <?=($active == $i)? "active" : ""?>' title='<?=hsc(str_replace("[page]", $i, $this->l("Lapa: [page]")))?>'><?=$i?></a>
        <? } ?>

      <? }else{ ?>
        <? for($i = 1; $i <= $count; $i++){ ?>
        <a href='?<?=$url?>p=<?=$i?>' class='num <?=($active == $i)? "active" : ""?>' title='<?=hsc(str_replace("[page]", $i, $this->l("Lapa: [page]")))?>'><?=$i?></a>
        <? } ?>
      <? } ?>

      <? if($active <  $count){ ?><a href='<?=$next_link?>' class='next' title='<?=hsc($this->l("Nākamā lapa"))?>'>&gt;</a><? } ?>
      <div class='cb'></div>
    </div>
    <div class='cb'></div>
    <?

  }


  function addProperties(){

    return [

        "name"        => Array(
            "label"     => "Name:",
            "type"      => "str"
        ),

        "cats" => Array(
            "label"       => "Category collection:",
            "type"        => "collection",
            "collectiontype" => "shopcatcollection",
        ),

        "prods" => Array(
            "label"       => "Product collection:",
            "type"        => "collection",
            "collectiontype" => "shopprodcollection",
        ),

        "users" => Array(
            "label"       => "User collection:",
            "type"        => "collection",
            "collectiontype" => "shopusercollection",
        ),

        "units" => Array(
            "label"       => "Units collection:",
            "type"        => "collection",
            "collectiontype" => "shopunitscollection",
        ),

        "prod_images" => Array(
            "label"       => "Product images collection:",
            "type"        => "collection",
            "collectiontype" => "shopcustomimagescollection",
        ),

        "related_prods" => Array(
            "label"       => "Related products collection:",
            "type"        => "collection",
            "collectiontype" => "shoprelatedprodscollection",
        ),

        "brandscol" => Array(
            "label"       => "Brands collection:",
            "type"        => "collection",
            "collectiontype" => "shopbrandscollection",
        ),

        "prices" => Array(
            "label"       => "Prices collection:",
            "type"        => "collection",
            "collectiontype" => "shoppricescollection",
        ),

        "ratings" => Array(
            "label"       => "Rating collection:",
            "type"        => "collection",
            "collectiontype" => "EVeikalsProductRatingCollection",
        ),

        "colors" => Array(
            "label"       => "Color collection:",
            "type"        => "collection",
            "collectiontype" => "EVeikalsColorCollection",
        ),

        "product_colors" => Array(
            "label"       => "Product color collection:",
            "type"        => "collection",
            "collectiontype" => "EVeikalsProductColorCollection",
        ),

        "sizes" => Array(
            "label"       => "Size collection:",
            "type"        => "collection",
            "collectiontype" => "EVeikalsSizeCollection",
        ),

        "product_sizes" => Array(
            "label"       => "Product size collection:",
            "type"        => "collection",
            "collectiontype" => "EVeikalsProductSizeCollection",
        ),

        "custom_field_groups" => Array(
            "label"       => "Custom field groups collection:",
            "type"        => "collection",
            "collectiontype" => "EVeikalsCustomFieldGroupCollection",
        ),

        "custom_fields" => Array(
            "label"       => "Custom field collection:",
            "type"        => "collection",
            "collectiontype" => "EVeikalsCustomFieldCollection",
        ),

    ];

  }

}
