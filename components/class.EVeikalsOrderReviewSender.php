<?php
/**
*
*  Title: Order review sender
*  Author: Jānis Polis <j.polis@datateks.lv>
*  Date: 12.08.2016
*  Project: E-Veikals
*
*/

require_once("class.component.php");

class EVeikalsOrderReviewSender extends component{

  function __construct($name){
    parent::__construct($name);
    $this->SetProperties();
  }
	
	function check_email($x) {
		 if (strpos($x, "@") !== false) {
			$split = explode("@", $x);
			if (strpos($split['1'], ".") !== false) 
				return true; 
			else
				return false; 
		 }
		 else 
			return false; 

	}
	
  function send($id = false){

    $this->emailfrom_email = option('Shop\\OrderForm\\emailfrom_email', null, 'Send order from e-mail', array(), 'constructor@datateks.lv');
    $this->emailfrom_name = option('Shop\\OrderForm\\emailfrom_name', null, 'Send order from name', array(), 'Constructor');
    $this->subject = option('Shop\\OrderReviews\\review_subject', null, 'Review subject', array(), 'Tava atsauksme par pasūtījumu');
    $this->interval = option('Shop\\OrderReviews\\review_interval', null, 'Review interval(min)', array(), 30);
		$this->link = option('Shop\\OrderReviews\\review_link', null, 'Review link', array(), '/lv/pasutijuma-noformesana/');
    $this->letter = option('Shop\\OrderReviews\\reminder_letter', null, 'Reminder letter', array("type" => "wysiwyg"), 'Labdien, <br /> Vēlamies zināt, Jūsu viedokli par pasūtījumu ar numuru [order_nr] atveriet šo saiti [link] ,lai novērtētu Mūsu servisu. <br /><br />Ar cieņu,<br />E-Veikala administrācija');
    $this->use_reviews = option('Shop\\OrderReviews\\use', null, 'Use reviews', array("type" => 'check','is_advanced'=>true), '0');

    $this->interval = (int)$this->interval;
		$this->interval = ($this->interval > 0) ? $this->interval : 30;

    if(!$this->use_reviews) return;

		if($id){
    	$orders = DB::GetTable("SELECT item_id,epasts FROM `".$this->orders->table."` WHERE item_id = :id", array(":id" => $id));
		}else{
    	$orders = DB::GetTable("SELECT item_id,epasts FROM `".$this->orders->table."` WHERE review_sent != 1 AND apmaksats = 1 AND piegadats = 1 AND datums < (NOW() - INTERVAL ".$this->interval." MINUTE)");
		}
		
    $sent_count = 0;
		foreach($orders as $order){
      if(!$this->check_email($order['epasts']))
				continue;
			$text = str_replace("[order_nr]", "<strong>".$order['item_id']."</strong>", $this->letter);
			$order['review_hash'] = md5($order['epasts'].date('Y-m-d H:i:s'));
			$this->orders->Update(array('review_hash' => $order['review_hash']),$order['item_id']);
			$link = $GLOBALS['cur_url'].$this->link.'?reviewhash='.$order['review_hash'];
			$text = str_replace("[link]", "<strong><a href='".$link."'>".$link."</a></strong>", $text);

      $sent = phpMailerSend(
        $text,
        $this->subject,
        $this->emailfrom_name,
       	$this->emailfrom_email,
        $order['epasts'],
        true,
        false,
        false
      );

			if($sent){ $sent_count++;
				$this->orders->Update(array("review_sent" => 1), array("item_id" => $order['item_id']));
			}
		}

    if(count($orders)){

    	echo "Sent: ".$sent_count."/".count($orders);

    }


  }

  function output(){


  }

  function SetProperties(){
    $this->properties = Array(

      "name"        => Array(
        "label"     => "Name:",
        "type"      => "str"
      ),

      "orders" => Array(
        "label"       => "Order collection:",
        "type"        => "collection",
        "collectiontype" => "EVeikalsOrderCollection",
      ),

    );

		$this->PostSetProperties(); 

  }

}