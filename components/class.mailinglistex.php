<?php

use Constructor\WebApp;
use Constructor\Url;

class mailinglistex extends component
{

  var $emailcatrelationcollection;
  var $emailcollection;
  var $catcollection;

  //Class initialization
  function mailinglistex($name)
  {
    parent::__construct($name);
    $this->SetProperties();
    $this->emails_per_request = 1;
  }


  function getLanguageStrings()
  {
    $ls = Array(
      'confirm_empty_list' => 'Do you want delete all emails from this list?',
      'submit' => 'Submit',
      'name' => 'Your name',
      'readmore'  => 'Read More',
      'click_to_unsubscribe' => 'Click here to unsubscribe from mailing list',
      'subscribetoall' => 'Subscribe to all',
      'emailnotsubscribed' => 'Your e-mail is not subscribed to this mailing list.',
      'subscriptionstatuswasupdated' => 'Your subscription status was updated successfully!',
      'successfulsubscriptionnotification' => 'Thank you for your subscription!<br />To finish subscription you must confirm your e-mail address. Please check your e-mail for further instructions.',
      'subscriptionconfirmationnotification' => 'Your subscription was successfully confirmed. Thank you for subscribing to our mailing list!',
      'email' => 'E-mail',
      'selectcategories' => 'Select categories you want to subscribe to',
      'subscribe' => 'Subscribe',
      'emailaddress' => 'E-mail address',
      'changesubscriptionstatus' => 'You can change your subscription status below',
      'unconfirmedemail' => '<b>Your e-mail address has not been confirmed.</b> Please check your e-mail for confirmation instructions',
      'unsubscribedfrommailinglist' => 'Unsubscribed from mailinglist',
      'subscribedtomailinglist' => 'Subscribed to mailinglist',
      'emailcouldnotbeconfirmed' => 'The e-mail address could not be confirmed. Please make sure you have subscribed to our mailing list and that you\'re using a valid confirmation link.',
      'emailisalreadyconfirmed' => 'The e-mail address has already been confirmed.',
      'pleaseenteremail' => 'Please enter your e-mail address!',
      'pleaseentervalidemail' => 'Please enter a valid e-mail address!',
      'emailisalreadysubscribed' => 'This e-mail address is already subscribed.',

    );
    return $ls;
  }


  function Execute()
  {
    if(!$this->InitCollections()) {
      echo ' collection Initialization failed';
      die();
    }
  }

    function output()
    {

        $substatus = $confirmed = false;
        if(isset($_POST['maillist_email'])){
            $substatus = $this->validateAndSubscribe();
        }elseif(isset($_GET['maillist_confirm'])){

            $hash = $_GET['maillist_confirm'];
            //get corrseponding e-mail by hash
            $subscriber = $this->emailcollection->GetItemByHash($hash);
            $confirmed = $this->confirmSubscriber($subscriber);
        }

        if(isset($_GET['maillist_unsubscribe'])){
            $unsub = $this->processUnSub($_GET['maillist_unsubscribe']);
        }

        if($unsub === true){
            echo WebApp::l("Jūs esat veiksmīgi atteicies no jaunumu saņemšanas e-pastā").'<br />';
        }elseif($substatus === true){
            echo WebApp::l("Paldies par pieteikšanos jaunumiem. Lūdzu sekojiet informācijai e-pastā");
        }elseif($confirmed){
            if($confirmed === true){
                echo WebApp::l("E-pasta adrese apstiprināta");
            }else{
                echo $confirmed;
            }
        }else{
            $this->displaySubscribeForm($substatus);
        }

    }

    private function processUnSub($hash)
    {

        $email = $this->emailcollection->GetItemByHash($hash);

        $response = false;

        if($email){
            $this->emailcollection->ChangeItemByIDAssoc($email['item_id'], ['maillist_status' => 2]);
            $response = true;
        }


        return $response;
    }


  function Output2(){

    $sh = $this->getServiceByName("shopmanager");
    if(!$sh) return;


    if(!$this->InitCollections()){
        echo $this->name.": Can't init collections.<br />";
      return;
    }

    if(!$this->use_mailinglist) return;

    ?>
    <div class='box' id="MailingListBox">
    <div class='title'><?=$this->l("Ziņas tavā e-pastā")?></div>
    <div class='content'>
    <?
    $output = false;
    if($_GET['maillist_unsubscribe']){

			$hash = $_GET['maillist_unsubscribe'];
      $subscriber = $this->emailcollection->GetItemByHash($hash);

      if($subscriber){
        //e-mail is subscribed; display form for unsubscription
        $this->displayUnsubscribeForm($subscriber);
      }else{
        echo $this->LS('emailnotsubscribed');
      }
      $output = true;

		}

    if($_GET['change_subscription_status'] && $_POST['maillist_hash']){

      $hash = $_POST['maillist_hash'];
      $subscriber = $this->emailcollection->GetItemByHash($hash);

      if($subscriber){
        $subscriber = $this->updateSubscriptionStatus($subscriber, $_POST['maillist_set_subscribe'], $_POST['maillist_cat']);
        if($subscriber){
          echo ''.$this->LS('subscriptionstatuswasupdated').'';
          echo '<br /><br />';
          $this->displayUnsubscribeForm($subscriber);
        }
      }
      $output = true;

    }

    if($_GET['maillist_confirm']){

      $hash = $_GET['maillist_confirm'];
      //get corrseponding e-mail by hash
      $subscriber = $this->emailcollection->GetItemByHash($hash);

      $confirmstatus = $this->confirmSubscriber($subscriber);
      if($confirmstatus === true)
        echo ''.$this->LS('subscriptionconfirmationnotification').'';
      else
        echo $confirmstatus;

      $output = true;

    }

    if(!$output){

      $substatus = false;
      if(isset($_POST['maillist_email'])){
        $substatus = $this->validateAndSubscribe();
      }
      if($substatus === true)
        echo ''.$this->LS('successfulsubscriptionnotification').'';
      else
        $this->displaySubscribeForm($substatus);
    }

    ?>
    </div>
    </div>
    <?

  } //end of output


  function displaySubscribeForm($status)
  {

    ?>
    <? if($status){ ?>
    <div class='error'><?=$status?></div>
    <? } ?>
    <form action="#subscribe-wrap" method="POST" id="subscribe-form">
        <input type="email" name="maillist_email" placeholder="<?php echo WebApp::l("Jūsu e-pasta adrese")?>">
        <button type="submit"><?php echo WebApp::l("Pieteikties")?></button>
    </form>
    <?

    return;

    echo '<div class="mailinglist">';

    if($status)
      echo '<b style="color: red;">' . $status . '</b><br /><br />';
    echo '<form action="?mailinglist_subscribe=1" method="post">';
    echo ''.$this->LS('name').':<br/><input name="maillist_name" value="'.stripslashes($_POST['maillist_name']).'" /><br />';
    echo ''.$this->LS('email').':<br/><input name="maillist_email" value="'.stripslashes($_POST['maillist_email']).'" /><br /><br />';

    if($cats)
    {
      echo ''.$this->LS('selectcategories').':<br />';
      echo '<blockquote><p>';
      echo '<input type="hidden" name="maillist_hascats" id="maillist_hascats" value="1" />';
      echo '<input type="checkbox" name="maillist_cat[-1]" id="maillist_cat_all" value="-1"';
      if($_POST['maillist_cat'][-1])
        echo ' checked="checked"';
      echo ' /> <label for="maillist_cat_all"><b>'.$this->LS('subscribetoall').'</b></label><br />';
      foreach($cats as $cat)
      {
        echo '<input type="checkbox" name="maillist_cat['.$cat['item_id'].']" id="maillist_cat'.$cat['item_id'].'" value="'.$cat['item_id'].'"';
        if($_POST['maillist_cat'][$cat['item_id']])
          echo ' checked="checked"';
        echo ' /> <label for="maillist_cat'.$cat['item_id'].'">' . $cat['mailcat_title'] . '</label><br />';
      }
      echo '</p></blockquote>';
    }
    else
      echo '<input type="hidden" name="maillist_cat[]" value="-1"/>';

    echo '<input type="submit" name="maillist_submit" value="'.$this->LS('subscribe').'" />';

    echo '</form>';
    echo '</div>';
  }

  function displayUnsubscribeForm($subscriber)
  {
    $cats = $this->getSubscriberCategories($subscriber['item_id']);

    echo '<form action="?change_subscription_status=1" method="post" id="unsub_form">';
    echo '<input type="hidden" name="maillist_hash" value="'.$subscriber['maillist_hash'].'" />';
    echo ''.$this->LS('emailaddress').': <b>' . $subscriber['maillist_email'] . '</b><br /><br />';

    echo ''.$this->LS('changesubscriptionstatus').':<br />';

    if($subscriber['maillist_status'] == 0)
    {
      echo ''.$this->LS('unconfirmedemail').'<br />';
    }else
    {
      echo '<blockquote><p>';
      echo '<input type="radio" name="maillist_set_subscribe" id="maillist_select_unsubscribe" value="unsubscribe"';
      if($subscriber['maillist_status'] != 1)
        echo ' checked="checked"';
      echo ' onchange="changeSubscriptionStatus()"/> <label for="maillist_select_unsubscribe">'.$this->LS('unsubscribedfrommailinglist').'</label><br />';

      echo '<input type="radio" name="maillist_set_subscribe" id="maillist_select_subscribe" value="subscribe"';
      if($subscriber['maillist_status'] == 1)
        echo ' checked="checked"';
      echo '  onchange="changeSubscriptionStatus()" /> <label for="maillist_select_subscribe">'.$this->LS('subscribedtomailinglist').'</label>';
      echo '</p></blockquote>';

      if($cats)
      {
        echo '<div id="maillist_cats"'.($subscriber['maillist_status'] == 1 ? '' : ' style="display:none"').'>';
        echo ''.$this->LS('selectcategories').':<br />';
        echo '<blockquote><p>';
        foreach($cats as $cat)
        {
          echo '<input type="checkbox" name="maillist_cat['.$cat['item_id'].']" id="maillist_cat'.$cat['item_id'].'" value="'.$cat['item_id'].'"';
          if($cat['subscribed'])
            echo ' checked="checked"';
          echo ' /> <label for="maillist_cat'.$cat['item_id'].'">' . ($cat['item_id'] == -1 ? '<b>' : '') . $cat['mailcat_title'] . ($cat['item_id'] == -1 ? '</b>' : '') . '</label><br />';
        }
        echo '</p></blockquote>';
        echo '</div>';
      }
      echo '<input type="submit" name="maillist_submit" value="'.$this->LS('submit').'" />';
    }


    echo '</form>';
?>
  <script type="text/javascript">

  function changeSubscriptionStatus()
  {
    var value = null;
    var nodes = document.getElementById('unsub_form').maillist_set_subscribe;
    for (var i=0; i<nodes.length; i++)
    {
      if (nodes[i].checked)
      {
        value = nodes[i].value;
        break;
      }
    }

    document.getElementById('maillist_cats').style.display = (value == 'subscribe') ? '' : 'none';
  }


  </script>
<?
  }




  function Design()
  {

    if($this->InitCollections())
    {
      echo '<input type="button" value="Send" onClick="window.open(\'?module=dialogs&action=component&site_id='.$this->site_id.'&page_id='.$this->page_id.'&component_name='.$this->name.'&component_type='.get_class($this).'&method_name=send&refresh=0&okaction=submit&session_id=' . session_id() . '&session_name=' . session_name() . '\',\'ad\',\'width=1000,height=650,toolbar=no,menubar=no,location=no,directories=0,status=0,scrollbar=0,resize=1\'); ">';
    }else
    {
      echo 'This component has no collections assigned. You can click the button below to create collections automatically.<br />';
      echo '<input type="button" value="Create Collections" onClick="var cname = prompt(\'Enter identifier for collections\', \'\'); if(!cname) return false; window.open(\'?module=dialogs&action=component&site_id='.$this->site_id.'&page_id='.$this->page_id.'&component_name='.$this->name.'&component_type='.get_class($this).'&method_name=autocreatecollections&refresh=0&okaction=submit&colname=\'+cname+\'&session_id=' . session_id() . '&session_name=' . session_name() . '\',\'ad\',\'width=1000,height=650,toolbar=no,menubar=no,location=no,directories=0,status=0,scrollbar=0,resize=1\'); ">';
    }
  }


  function send()
  {

    if(!$this->InitCollections())
    {
      echo 'Component not configured properly';
      return false;
    }

    $templates = $this->list_available_templates();

    ?>

    <link rel="stylesheet" type="text/css" href="<?= $GLOBALS['cfgWebRoot']; ?>gui/extjs/resources/css/ext-all.css" />
    <script language="JavaScript" src="<?= $GLOBALS['cfgWebRoot']; ?>gui/extjs/PagingMemoryProxy.js" type="text/javascript"></script>
    <script language="JavaScript" src="<?= $GLOBALS['cfgWebRoot']; ?>gui/extjs/HtmlEditorImageInsert.js" type="text/javascript"></script>

    <style>
    .sendinglistitem {
      border-bottom: 1px solid #6699CC;
      padding: 5px;
    }

    .sendinglistitem_ready {
      border-bottom: 1px solid #6699CC;
      padding: 5px;
      background-color: #D9ECFF;
    }

    .sendinglistitem_sending {
      border-bottom: 1px solid #FF8C1A;
      padding: 5px;
      background-color: #FFB871;
    }

    .sendinglistitem_success {
      border-bottom: 1px solid #97CB63;
      padding: 5px;
      background-color: #CEE7B6;
    }

    .sendinglistitem_failure {
      border-bottom: 1px solid #D24848;
      padding: 5px;
      background-color: #E69999;
    }

    #page-sending-div {
      overflow: auto;
      height: 100%;
    }

    #page-sending-progress-div {
      overflow: auto;
      height: 100%;
    }

    .send-node .x-tree-node-icon{background:url('<?= $GLOBALS['cfgWebRoot']; ?>gui/img/extra/email_edit.png') no-repeat; padding-right: 3px; }
    .lists-node .x-tree-node-icon{background:url('<?= $GLOBALS['cfgWebRoot']; ?>gui/img/extra/group.png') no-repeat; padding-right: 3px; }
    .templates-node .x-tree-node-icon{background:url('<?= $GLOBALS['cfgWebRoot']; ?>gui/img/extra/page_white_horizontal.png') no-repeat; padding-right: 3px; }
    .settings-node .x-tree-node-icon{background:url('<?= $GLOBALS['cfgWebRoot']; ?>gui/img/extra/cog.png') no-repeat; padding-right: 3px; }

    .x-html-editor-tb .x-edit-insertimage .x-btn-text {
    		background:transparent url('<?= $GLOBALS['cfgWebRoot']; ?>gui/img/extra/insert_image.gif') no-repeat;
    		background-position: 0 0;
  	}

    .x-html-editor-tb .x-edit-bigedit .x-btn-text {
    		background:transparent url('<?= $GLOBALS['cfgWebRoot']; ?>gui/img/extra/pencil.png') no-repeat;
    		background-position: 0 0;
  	}


    </style>

    <script>

    Ext.BLANK_IMAGE_URL = '<?= $GLOBALS['cfgWebRoot']; ?>gui/extjs/resources/images/default/s.gif';

    Ext.EventManager.onDocumentReady(function(){

    Ext.override(Ext.data.Connection, {timeout:60000});
    /*Ext.override(Ext.data.proxy.Ajax, { timeout:60000 });*/

<?
        $this->scriptInitialization();
?>
      var total_email_send_count = 0;
      var Tree = Ext.tree;

      var tree = new Tree.TreePanel({
          useArrows:true,
          border: false,
          autoScroll:true,
          animate:true,
          enableDD:true,
          containerScroll: true,
          rootVisible: false,
          ddGroup: 'treeDDE',

          //layout properties
          border: true,
  	      title: '',
  		  collapsible: false,
          region:'west',
  		  floatable: false,
  		  margins: '0 0 0 0',
          width: 175,
          minSize: 100,
          maxSize: 250
        })

        // set the root node
        var myroot1 = new Tree.TreeNode({
            text: 'Actions',
            draggable:false,
            id:'node-source'
        });
        tree.setRootNode(myroot1);

        var myleaf_send = new Tree.TreeNode({
            text: 'Send',
            draggable:false,
            id:'leaf-send',
            leaf: true,
            cls: 'send-node'
        });
        myleaf_send.addListener('click', function(){
          pagelist.layout.setActiveItem('page-send');
        });

        var myleaf_lists = new Tree.TreeNode({
            text: 'Lists',
            draggable:false,
            id:'leaf-lists',
            leaf: true,
            cls: 'lists-node'
        });
        myleaf_lists.addListener('click', function(){
          pagelist.layout.setActiveItem('page-lists');
          store_log.load({params:{start:0, limit:25}});
        });

        var myleaf_templates = new Tree.TreeNode({
            text: 'Templates',
            draggable:false,
            id:'leaf-templates',
            leaf: true,
            cls: 'templates-node'
        });
        myleaf_templates.addListener('click', function(){
          pagelist.layout.setActiveItem('page-templates');
        });

        var myleaf_sent = new Tree.TreeNode({
            text: 'Sent',
            draggable:false,
            id:'leaf-sent',
            leaf: true,
            cls: 'sent-node'
        });
        myleaf_sent.addListener('click', function(){
          pagelist.layout.setActiveItem('page-sent');
          store_sent.load({params:{start:0, limit:25}});
        });

        var myleaf_log = new Tree.TreeNode({
            text: 'Log',
            draggable:false,
            id:'leaf-log',
            leaf: true,
            cls: 'log-node'
        });
        myleaf_log.addListener('click', function(){
          pagelist.layout.setActiveItem('page-log');
          store_log.load({params:{start:0, limit:25}});
        });



        var myleaf_settings = new Tree.TreeNode({
            text: 'Settings',
            draggable:false,
            id:'leaf-settings',
            leaf: true,
            cls: 'settings-node'
        });
        myleaf_settings.addListener('click', function(){
          pagelist.layout.setActiveItem('page-settings');
        });

        myroot1.beginUpdate();
        myroot1.appendChild(new Array(myleaf_send, myleaf_lists, myleaf_templates, myleaf_sent, myleaf_log, myleaf_settings));
        myroot1.endUpdate();

        Ext.QuickTips.init();

        var composeform = new Ext.form.FormPanel({
            labelWidth: 75,
            id: 'compose-message-form',
            title: 'Message contents',
            bodyStyle:'padding:15px',
            width: 400,
            labelPad: 10,
            defaultType: 'textfield',
            defaults: {
                // applied to each contained item
                width: 280,
                msgTarget: 'side'
            },
            layoutConfig: {
                // layout-specific configs go here
                labelSeparator: ''
            },
            items: [{
                    fieldLabel: 'From Name',
                    name: 'from',
                    id: 'composeform-fromname',
                    allowBlank: false,
                },{
                    fieldLabel: 'From E-mail',
                    name: 'email',
                    id: 'composeform-fromemail',
                    vtype:'email',
                    allowBlank: false
                },{
                    fieldLabel: 'Subject',
                    name: 'subject',
                    id: 'composeform-subject',
                    allowBlank: false
                },{
                    xtype: 'htmleditor',
                    fieldLabel: 'Text',
                    name: 'msgtext',
                    id: 'composeform-text',
                    width: 600,
                    height: 250,
                    plugins: new Ext.ux.plugins.HtmlEditorImageInsert({
                        popTitle: 'Image url?',
                        popMsg: 'Please insert an image URL...',
                        popWidth: 400,
                        popValue: 'http://www.google.gr/intl/en_com/images/logo_plain.png',
                        site_id: '<?= $this->site_id; ?>',
                    })
                },{
                    xtype: 'combo',
                    fieldLabel: 'Template',
                    name: 'template',
                    id: 'composeform-template',
                    mode: 'local',
                    hiddenName: 'templatehid',
                    editable: false,
                    forceSelection: true,
                    lazyRender: true,
                    triggerAction: 'all',
                    valueField: 'value',
                    displayField: 'text',
                    disableKeyFilter: true,
                    emptyText: '--use default--',
                    store: new Ext.data.SimpleStore({
                      id: 0,
                      fields: ['value', 'text'],
                      data : [<?
                      $first = true;
                      foreach($templates as $template)
                      {
                        if(!$first)
                        {
                          echo ',';
                        }else
                          $first = false;
                        echo '[\'' . $template . '\', \'' . $template . '\']';
                      }
                      ?>
                      ]
                    })
                }
            ]
        });


        var attachmentform = new Ext.form.FormPanel({
            labelWidth: 75,
            id: 'compose-attachment-form',
            title: 'Attachments',
            bodyStyle:'padding:15px',
            width: 400,
            labelPad: 10,
            defaultType: 'textfield',
            defaults: {
                // applied to each contained item
                width: 280,
                msgTarget: 'side'
            },
            layoutConfig: {
                // layout-specific configs go here
                labelSeparator: ''
            }
        });

        for(i = 0; i < 5; i++)
        {
          var tf = new Ext.form.TriggerField({
                      xtype: 'trigger',
                      fieldLabel: 'Select file',
                      name: 'file' + i,
                      id: 'attachmentsform-file' + i,
                      mode: 'local',
                      hiddenName: 'file1hid',
                      editable: true,
                      emptyText: '',
                      triggerClass: 'x-form-search-trigger',
                      triggerNumber: i
          });
          attachmentform.add(tf);
          tf.onTriggerClick = function()
          {
          	var me = this;

						openDlg("?module=dialogs&action=filex&site_id=<?= $GLOBALS['site_id']; ?>&view=thumbs", this.getValue(), {w: 400, h: 450}, function(val){

            	if(val) me.setValue(val);

						});
            //var val = openModalWin("?module=dialogs&action=filex&site_id=<?= $GLOBALS['site_id']; ?>&view=thumbs", this.getValue(), 400, 450);

          }
        }

        var rcptlists = new Ext.form.CheckboxGroup(
        {
            fieldLabel: 'Subscriber lists',
            id: 'rcptlistform-rcptlists',
            disabled: true,
            items: [
            {
              xtype: 'hidden' //to prevent javascript error when items = empty
            }
            <?php
            $cats = $this->catcollection->GetDBData();
            if($cats)
            {
              $first = true;
              foreach($cats as $cat)
              {
                echo '
                ,{
                  boxLabel: \''.$cat['mailcat_title'].'\',
                  id: \'rcptlistform-check-list' . $cat['item_id'] . '\',
                  width: 350,
                  inputValue: ' . $cat['item_id'] . '
                }
                ';
              }
            }
            ?>
            ],
            width: 350,
            columns: 1,
            msgTarget: 'side'
        }
        );

        rcptlisttarget = new Ext.form.RadioGroup({
            fieldLabel: 'Send to',
            id: 'rcptlistform-target',
            items: [
            {
                boxLabel: 'Send to all subscribers',
                id: 'rcptlistform-all',
                name: 'radio-sendto',
                xtype: 'radio',
                checked: true,
                handler: EnableDisableRcptLists
            },
            {
                boxLabel: 'Send to selected list subscribers',
                id: 'rcptlistform-selected',
                name: 'radio-sendto',
                xtype: 'radio',
                handler: EnableDisableRcptLists
            }
            ],
            width: 350,
            columns: 1
        });

        var rcptlistform = new Ext.form.FormPanel({
            labelWidth: 75,
            id: 'compose-recipientlists-form',
            title: 'Recipient lists',
            bodyStyle:'padding:15px',
            width: 400,
            labelPad: 10,
            defaultType: 'textfield',
            defaults: {
                // applied to each contained item
                width: 280,
                msgTarget: 'side'
            },
            layoutConfig: {
                // layout-specific configs go here
                labelSeparator: ''
            },
            items: [
            rcptlisttarget,
            rcptlists
            ]
        });

        var initextracontentformprogress = new Ext.ProgressBar({
                id: 'progress-extracontentform-init',
                width: 350
        });

        var extracontentform = new Ext.form.FormPanel({
            labelWidth: 75,
            id: 'compose-extracontent-form',
            title: 'Extra content',
            bodyStyle:'padding:15px',
            width: 400,
            labelPad: 10,
            defaultType: 'textfield',
            defaults: {
                // applied to each contained item
                width: 280,
                msgTarget: 'side'
            },
            layoutConfig: {
                // layout-specific configs go here
                labelSeparator: ''
            },
            items: [
            {
                xtype: 'label'
            }
            ],
            bbar: [
                initextracontentformprogress
            ]
        });
        extracontentform.on('render', function() { InitializeExtraContentForm(); });

        var sendpanels = new Ext.Panel({
          layout: 'accordion',
          id: 'page-send',
          autoScroll: true,
          title: 'Compose Message',
          layoutConfig: {
              // layout-specific configs go here
              titleCollapse: true,
              animate: false,
              activeOnTop: false,
              fill: false
          },
          items:[
              composeform,
              attachmentform,
              extracontentform,
              rcptlistform
          ],

          buttons: [
          {
              id: 'send-test',
              text: 'Send Test Message',
              handler: function() { SendTestMessage_ShowForm(); }
          },
          {
              id: 'send-all',
              text: 'Send All Message',
              handler: function() { SendAllMessages_ShowForm(); }
          },
          //' ',
          {
              id: 'send-start',
              text: 'Send Messages',
              handler: function() { Sending_ScreenShow(); }
          }
          ],


          region:'center'

        });


        var sendprogress = new Ext.ProgressBar({
            id: 'progressbar-send-progress',
            width: 200
        });


        var rcptlists_tree = new Tree.TreePanel({
          useArrows: true,
          border: true,
          autoScroll: true,
          animate: true,
          rootVisible: false,

          //layout properties
          border: true,
  	      title: '',
  		  collapsible: false,
          margins: '0 0 0 0',
          region: 'west',
          width: 270,
          tbar: [
              {
                text: 'Add',
                tooltip: 'Add new subscribers list',
                handler: function() { AddSubscribersList(); }
              },
              {
                text: 'Edit',
                tooltip: 'Edit selected subscribers list',
                handler: function() { EditSubscribersList(); }
              },
              {
                text: 'Delete',
                tooltip: 'Delete selected subscribers list',
                handler: function() { DeleteSubscribersList(); }
              }
          ]
        });

        // set the root node
        var myroot2 = new Tree.TreeNode({
            text: 'Lists',
            draggable:false,
            id:'node-lists_root'
        });
        rcptlists_tree.setRootNode(myroot2);


        var filterInput = new Ext.form.TextField({
          emptyText: 'Filter by e-mail...',
          id: 'tbar-emailgrid-findinput',
          xtype: 'textfield'
        });


        var Subscriber = Ext.data.Record.create([
            {name: 'id', mapping: 0},
            {name: 'email', mapping: 1},
            {name: 'name', mapping: 2},
            {name: 'sub', mapping: 3}
        ]);
        var SubscribersReader = new Ext.data.ArrayReader({
            id: 0
        }, Subscriber);


        var emailgrid_proxy = new Ext.data.PagingMemoryProxy(Subscriber);
/*
        var checkColumn = new Ext.grid.CheckColumn({
           header: "Subscribed",
           dataIndex: 'sub',
           width: 115,
           hidden: true,
           sortable: true,
           proxy: emailgrid_proxy
        });
*/

        var task_saveemaillistchanges = new Ext.util.DelayedTask(function()
            {
                SaveModifiedSubscribersList();
            }
        );

        var store_Subscribers = new Ext.data.Store({
            reader: SubscribersReader,
            proxy: emailgrid_proxy,
            data: new Array(),
            remoteSort: true
        });

        var listpanel_emailgrid = new Ext.grid.EditorGridPanel({
            store: store_Subscribers,
//            plugins:checkColumn,
            clicksToEdit:1,
            columns: [
                {header: 'id', dataIndex: 'id', hidden: true},
                {id:'column-email', header: "E-mail", sortable: true, dataIndex: 'email'},
                {header: "Name", sortable: true, dataIndex: 'name'}
//                checkColumn
            ] ,
            viewConfig: {
                forceFit: true
            },
            sm: new Ext.grid.RowSelectionModel({singleSelect:true}),
            region: 'center',
            title:'',
            tbar: [
                {
                  text: 'Add',
                  tooltip: 'Add new subscribed e-mail',
                  handler: function() { AddSubscribedEmail(); }
                }
                ,
                {
                  text: 'Edit',
                  tooltip: 'Edit selected e-mail',
                  handler: function() { EditSubscribedEmail(); }
                }
                ,
                {
                  text: 'Delete',
                  tooltip: 'Delete selected e-mail',
                  handler: function() { DeleteSubscribedEmail(); }
                }
                ,
                {
                  text: 'Delete all',
                  tooltip: 'Delete all e-mails from list',
                  handler: function() { DeleteListEmails(); }
                }
                ,
                '->',
                filterInput
            ],
            bbar: new Ext.PagingToolbar({
                store: store_Subscribers,
                pageSize: 24,
                displayInfo: true,
                displayMsg: 'Displaying subscribers {0} - {1} of {2}',
                emptyMsg: "No subscribers to display"
            })
        });


        store_Subscribers.on('update', function()
            {
                task_saveemaillistchanges.delay(500);
            }
        );





        function performEmailFiltering(filterInput)
        {
//          listpanel_emailgrid.getStore().filter('email', filterInput.getValue(), true);
          RedrawFilteredEmailGrid();
        }

        filterInput.on('valid', performEmailFiltering);


        var listspanel_lists = new Ext.Panel({
            id:'page-lists-lists',
            layout:'border',
            title: 'Lists Manager',
            collapsed: true,
            border: false,
            defaults: {
        		collapsible: false,
                split: true,
        		animFloat: false,
        		autoHide: false,
        		useSplitTips: true
        	},
            items: [
                rcptlists_tree,
                listpanel_emailgrid
            ]
        });

        listspanel_lists.on('expand', function() { GetRcptListNames(); });


        listspanel_import = new Ext.form.FormPanel({
            id:'page-lists-import',
            title: 'Import E-mail Addresses',
            collapsed: true,
            border: false,
            autoScroll: true,
            labelWidth: 75,
            bodyStyle:'padding:15px',
            labelPad: 10,
            defaultType: 'textfield',
            defaults: {
                // applied to each contained item
                width: 350,
                msgTarget: 'side'
            },
            layoutConfig: {
                // layout-specific configs go here
                labelSeparator: ''
            },
            items: [
                {
                    fieldLabel: 'E-mail addresses',
                    id: 'lists-import-data',
                    allowBlank: false,
                    xtype: 'textarea',
                    height: 350,
                    emptyText: '--- Paste e-mail addresses (each in new line) here ---'
                }
            ],
            buttonAlign: 'right',
            buttons: [
                {
                    id: 'btn-lists-import-import',
                    text: 'Import',
                    handler: function() { SubmitImportList(); },
                    type: 'submit'
                }
            ]
        });

        listspanel_import.on('expand', function() { GetRcptListNames(); });


        var listspanel = new Ext.Panel({
            title: 'Manage subscriber lists',
            id: 'page-lists',
            autoScroll: true,
            layout: 'accordion',
            layoutConfig: {
                // layout-specific configs go here
                titleCollapse: true,
                animate: false,
                activeOnTop: false,
                fill: true
            },
              items: [
                  listspanel_lists,
                  listspanel_import
              ]
        });



        <?
        #</script> - this useless comment is added to enable syntax highlighting
        //           for the next php code block in webuilder
        ?>

        <?
        //pre-create fields that need their events to be set
        foreach($templates as $template)
        {
          $fields = $this->get_fields_template($template);
          foreach($fields as $fieldname => $field)
          {
            if($field['type'] == 'image')
            {
              echo "
              var templatefield_".$template."_".$fieldname." = new Ext.form.TriggerField({
                          xtype: 'trigger',
                          fieldLabel: '" . parseForJScript($field['label'], true) . "',
                          name: '" . $fieldname . "',
                          id: 'templates-".$template."-field-".$fieldname."',
                          value: '".parseForJScript($field['value'], true)."',
                          mode: 'local',
                          hiddenName: '" . $fieldname . "_hid',
                          editable: true,
                          emptyText: '',
                          triggerClass: 'x-form-search-trigger',
                          triggerNumber: i
              });
              templatefield_".$template."_".$fieldname.".onTriggerClick = function()
              {
                var val = openModalWin(\"?module=dialogs&action=filex&site_id=" . $GLOBALS['site_id'] . "&view=thumbs\", this.getValue(), 400, 450);
                if(val)
                  this.setValue(val);
              }
              ";
            }
          }
        }

        //create forms for all templates
        foreach($templates as $template)
        {
          echo "
          templatespanel_" . $template . " = new Ext.form.FormPanel({
            id:'page-templates-".$template."',
            title: 'Template settings: ".$template."',
            url: '" . '?module=dialogs&action=component&site_id='.$this->site_id.'&page_id='.$this->page_id.'&component_name='.$this->name.'&component_type='.get_class($this).'&method_name=save_template_settings_ajax_handler&refresh=0&okaction=submit&session_id=' . session_id() . '&session_name=' . session_name() . "',
            collapsed: true,
            border: false,
            autoScroll: true,
            labelWidth: 75,
            bodyStyle:'padding:15px',
            labelPad: 10,
            defaultType: 'textfield',
            defaults: {
                // applied to each contained item
                width: 550,
                msgTarget: 'side'
            },
            layoutConfig: {
                // layout-specific configs go here
                labelSeparator: ''
            },
            buttons: [
                {
                    text: 'Save Changes',
                    handler: function()
                    {
                        templatespanel_" . $template . ".form.submit({
                            method: 'POST',
                            waitMsg:'Submitting...',

                            reset : false,
                            success : function(form, action)
                            {
                                //alert('success: ' + action.response.responseText);
                            },
                            failure: function(form, action)
                            {
                                alert('failure: ' + action.response.responseText);
                            }

                        });
                    }
                },
                {
                    text: 'Preview Saved Template',
                    handler: function()
                    {
                        window.open('?module=dialogs&action=component&site_id=".$this->site_id."&page_id=".$this->page_id."&component_name=".$this->name."&component_type=".get_class($this)."&method_name=templatepreview&refresh=0&okaction=submit&template=".$template."&session_id=" . session_id() . "&session_name=" . session_name() . "','tp','width=600,height=650,toolbar=no,menubar=no,location=no,directories=0,status=0,scrollbar=0,resize=1');
                    }
                }
            ],
            items: [
              {
                  xtype: 'label'
              },
              {
                  xtype: 'hidden',
                  id: 'templates-".$template."-templatename',
                  value: '".$template."',
                  name: 'templatename'
              }
            ";

            $fields = $this->get_fields_template($template);
            foreach($fields as $fieldname => $field)
            {
              if($field['type'] == 'image')
              {
                echo ",templatefield_".$template."_".$fieldname."";
              }else
              {
                echo "
                  ,
                  {
                      fieldLabel: '".parseForJScript($field['label'], true)."',
                      id: 'templates-".$template."-field-".$fieldname."',
                      name: '".$fieldname."',
                      value: '".parseForJScript($field['value'], true)."',
                      ";
                switch($field['type'])
                {
                  case 'html':
                    echo "xtype: 'htmleditor'";
                    break;
                  default:
                    echo "xtype: 'textfield'";
                }
                echo "

                  }
                ";
              }
            }


            echo "
            ],
            buttonAlign: 'right'

        }
          );
          ";


        }

        ?>

        //<script> - this useless comment is added to enable syntax highlighting
        //           for the next javascript code block in webuilder

        var templatespanel = new Ext.Panel({
            title: 'Manage e-mail templates',
            id: 'page-templates',
            autoScroll: true,
            layout: 'accordion',
            layoutConfig: {
                // layout-specific configs go here
                titleCollapse: true,
                animate: false,
                activeOnTop: false,
                fill: true
            },
              items: [
                  <?
                  $first = true;
                  foreach($templates as $template)
                  {
                    if(!$first)
                      echo ',';
                    else
                      $first = false;
                    echo 'templatespanel_' . $template;
                  }
                  ?>
              ]
        });


        //sent grid
        // create the Data Store
        var store_sent = new Ext.data.JsonStore({
            root: 'items',
            totalProperty: 'totalCount',
            idProperty: 'item_id',
            remoteSort: true,

            fields: [
                'time', 'subject', 'total_emails_count', 'sent_emails_count'
            ],

            // load using script tags for cross domain, if the data in on the same domain as
            // this page, an HttpProxy would be better
            proxy: new Ext.data.ScriptTagProxy({
                url: '?module=dialogs&action=component&site_id=<?= $this->site_id; ?>&page_id=<?= $this->page_id; ?>&component_name=<?= $this->name; ?>&component_type=<?= get_class($this); ?>&method_name=read_sent_ajax_handler&refresh=0&okaction=submit&session_id=<?= session_id(); ?>&session_name=<?= session_name(); ?>'
            })
        });
        store_sent.setDefaultSort('time', 'desc');

        var pagingBar_sent = new Ext.PagingToolbar({
            pageSize: 25,
            store: store_sent,
            displayInfo: true,
            displayMsg: 'Displaying entries {0} - {1} of {2}',
            emptyMsg: "No entries to display"
        });

        var grid_sent = new Ext.grid.GridPanel({
            id:'page-sent',
            title:'Sent messages',
            store: store_sent,
            trackMouseOver:false,
            disableSelection:false,
            loadMask: true,

            // grid columns
            columns:[
                {header: "Time", sortable: true, dataIndex: 'time'},
                {header: "Subject", sortable: false, dataIndex: 'subject'},
                {header: "Total emails", sortable: false, dataIndex: 'total_emails_count'},
                {header: "Sent emails", sortable: false, dataIndex: 'sent_emails_count'}
            ],

            viewConfig: {
                forceFit: true
            },
            tbar: [
                {
                  text: 'Send as New',
                  tooltip: 'Send new email',
                  handler: function() {
                     //Ext.get('composeform-fromemail').value = 'test@datateks.lv';

                    var sel = grid_sent.getSelectionModel().getSelected();
                    if(sel)
                    {
                      $.ajax({
                        type: "POST",
                        url: '?module=dialogs&action=component&site_id=<?= $this->site_id; ?>&page_id=<?= $this->page_id; ?>&component_name=<?= $this->name; ?>&component_type=<?= get_class($this); ?>&method_name=get_message_by_id&refresh=0&okaction=submit&session_id=<?= session_id(); ?>&session_name=<?= session_name(); ?>&message_id=' + sel.id,
                        success: function(response){
                          data = jQuery.parseJSON(response);
                          console.log(data);
                          composeform.getForm().findField('from').setValue(data.name);
                          composeform.getForm().findField('email').setValue(data.email_from);
                          composeform.getForm().findField('subject').setValue(data.subject);
                          composeform.getForm().findField('msgtext').setValue(data.text);
                          attachmentform.getForm().findField('file0').setValue(data.attachment[0]);
                          attachmentform.getForm().findField('file1').setValue(data.attachment[1]);
                          attachmentform.getForm().findField('file2').setValue(data.attachment[2]);
                          attachmentform.getForm().findField('file3').setValue(data.attachment[3]);
                          attachmentform.getForm().findField('file4').setValue(data.attachment[4]);
                          pagelist.layout.setActiveItem('page-send');

                        }
                      });

                     // $('#composeform-fromemail').val('test@datateks.lv');
                     // alert(Ext.get('composeform-fromemail').getValue());
                    }
                  }
                }
                ,
                {
                  text: 'complete sending',
                  tooltip: 'Complete sending',
                  handler: function() { SendAllUnsentMessages_ShowForm(); }
                }
                ,
                {
                  text: 'preview',
                  tooltip: 'Preview',
                  handler: function() {
                    SendAllUnsentMessages_preview();
                  }
                }
                ,

            ],
            // paging bar on the bottom
            bbar: pagingBar_sent
        });








        //log grid
        // create the Data Store
        var store_log = new Ext.data.JsonStore({
            root: 'items',
            totalProperty: 'totalCount',
            idProperty: 'item_id',
            remoteSort: true,

            fields: [
                'maillog_time', 'maillog_email', 'maillog_subject', 'maillog_status'
            ],

            // load using script tags for cross domain, if the data in on the same domain as
            // this page, an HttpProxy would be better
            proxy: new Ext.data.ScriptTagProxy({
                url: '?module=dialogs&action=component&site_id=<?= $this->site_id; ?>&page_id=<?= $this->page_id; ?>&component_name=<?= $this->name; ?>&component_type=<?= get_class($this); ?>&method_name=read_log_ajax_handler&refresh=0&okaction=submit&session_id=<?= session_id(); ?>&session_name=<?= session_name(); ?>'
            })
        });
        store_log.setDefaultSort('maillog_time', 'desc');

        var pagingBar_log = new Ext.PagingToolbar({
            pageSize: 25,
            store: store_log,
            displayInfo: true,
            displayMsg: 'Displaying entries {0} - {1} of {2}',
            emptyMsg: "No entries to display"
        });

        var grid_log = new Ext.grid.GridPanel({
            id:'page-log',
            title:'Mailing List Log',
            store: store_log,
            trackMouseOver:false,
            disableSelection:true,
            loadMask: true,

            // grid columns
            columns:[
                {header: "Time", sortable: true, dataIndex: 'maillog_time'},
                {header: "E-mail", sortable: true, dataIndex: 'maillog_email'},
                {header: "Subject", sortable: true, dataIndex: 'maillog_subject'},
                {header: "Additional Info", sortable: true, dataIndex: 'maillog_status'}
            ],

            viewConfig: {
                forceFit: true
            },
            // paging bar on the bottom
            bbar: pagingBar_log
        });





        settingsform = new Ext.form.FormPanel({
            id:'page-settings',
            title: 'Mailing List Settings',
            url: '?module=dialogs&action=component&site_id=<?= $this->site_id; ?>&page_id=<?= $this->page_id; ?>&component_name=<?= $this->name; ?>&component_type=<?= get_class($this); ?>&method_name=save_settings_ajax_handler&refresh=0&okaction=submit&session_id=<?= session_id(); ?>&session_name=<?= session_name(); ?>',
            collapsed: false,
            border: true,
            autoScroll: true,
            labelWidth: 75,
            bodyStyle:'padding:15px',
            labelPad: 10,
            defaultType: 'textfield',
            defaults: {
                // applied to each contained item
                width: 350,
                msgTarget: 'side'
            },
            layoutConfig: {
                // layout-specific configs go here
                labelSeparator: ''
            },
            items: [
                {
                    title: 'General',
                    id: 'settings-general',
                    xtype: 'fieldset',
                    items: [
                        {
                            fieldLabel: 'Mass sending interval (sec)',
                            id: 'settings-interval',
                            name: 'sendinterval',
                            allowBlank: false,
                            allowDecimals: false,
                            allowNegative: false,
                            minValue: 1,
                            maxValue: 3600,
                            xtype: 'numberfield',
                            value: '<?= parseForJScript($this->getProperty('sendinterval'), true); ?>'
                        }
                    ],
                    width: 500,
                    autoHeight: true
                },
                {
                    title: 'Subscription confirmation e-mail',
                    id: 'settings-confirmation',
                    xtype: 'fieldset',
                    defaults: {
                        width: 350
                    },
                    items: [
                        {
                          fieldLabel: 'From name',
                          id: 'settings-confirmation-fromname',
                          name: 'confirmationfromname',
                          xtype: 'textfield',
                          allowBlank: false,
                          value: '<?= parseForJScript($this->getProperty('confirmationfrom'), true); ?>'
                        }
                        ,
                        {
                          fieldLabel: 'From email',
                          id: 'settings-confirmation-fromemail',
                          name: 'confirmationfromemail',
                          xtype: 'textfield',
                          allowBlank: false,
                          value: '<?= parseForJScript($this->getProperty('confirmationfromemail'), true); ?>'
                        }
                        ,
                        {
                          fieldLabel: 'Subject',
                          id: 'settings-confirmation-subject',
                          name: 'confirmationsubject',
                          xtype: 'textfield',
                          allowBlank: false,
                          value: '<?= parseForJScript($this->getProperty('confirmationsubject'), true); ?>'
                        }
                        ,
                        {
                          fieldLabel: 'Text',
                          id: 'settings-confirmation-text',
                          name: 'confirmationtext',
                          xtype: 'textarea',
                          allowBlank: false,
                          value: '<?= parseForJScript($this->getProperty('confirmationtext'), true); ?>'
                        }
                    ],
                    width: 500,
                    autoHeight: true
                }
            ],
            buttonAlign: 'right',
            buttons: [
                {
                    id: 'btn-settings-save',
                    text: 'Save Settings',
                    handler: function()
                    {
                        settingsform.form.submit({
                            method: 'POST',
                            waitMsg:'Submitting...',

                            reset : false,
                            success : function(form, action)
                            {
                                //alert('success: ' + action.response.responseText);
                                window.sendinterval = action.result.post.sendinterval * 1000;
                            },
                            failure: function(form, action)
                            {
                                alert('failure: ' + action.response.responseText);
                            }

                        });
                    },
                    type: 'submit'
                }
            ]
        });


        // create the pagelist
        var pagelist = new Ext.Panel({
            title: '',
            layout:'card',
            activeItem: 'page-send', // make sure the active item is set on the container config!
            defaults: {
                // applied to each contained panel
                border:false
            },
            // the panels (or "cards") within the layout
            items: [sendpanels,
                listspanel,
            {
                id: 'page-sending',
                html: '<div id="page-sending-div"></div>',
                title: 'Select e-mails you want to send to',

                buttons: [
                {
                    text: 'Back',
                    handler: function () { pagelist.layout.setActiveItem('page-send'); }
                },
                {
                    text: 'Select All',
                    handler: function () {
                        var i = 0;
                        while(document.getElementById('sendto' + i))
                        {
                            document.getElementById('sendto' + i).checked = true;
                            toggleReceiverSelected(i, true);
                            i++;
                        }
                    }
                },
                {
                    text: 'Unselect All',
                    handler: function () {
                        var i = 0;
                        while(document.getElementById('sendto' + i))
                        {
                            document.getElementById('sendto' + i).checked = false;
                            toggleReceiverSelected(i, false);
                            i++;
                        }
                    }
                },
                {
                    text: 'Start sending',
                    handler: function() { InitializeSendingStart(); }
                }]
            },{
                id: 'page-sending-progress',
                html: '<div id="page-sending-progress-div"></div>',
                title: 'Sending in progress',

                buttons: [
                sendprogress,
                {
                    id: 'btn-sending-progress-back',
                    text: 'Back',
                    handler: function () { Sending_ScreenShow(); },
                    disabled: true
                },
                {
                    id: 'bnt-sending-progress-pause',
                    text: 'Pause',
                    handler: function() { ToggleSendingProgress(); }
                },
                {
                    id: 'bnt-sending-progress-cancel',
                    text: 'Cancel',
                    handler: function () { StopSendingProgress(); }
                }]



            },
            templatespanel,
            grid_sent,
            grid_log,
            settingsform
            ],

            region:'center'

        });



        // create our layout
        var border = {
        	id:'border-panel',
            layout:'border',
            bodyBorder: false,
            height: 295,
        	defaults: {
        		collapsible: false,
                split: true,
        		animFloat: false,
        		autoHide: false,
        		useSplitTips: true
        	},
            items: [
            tree,
            pagelist],
            renderTo: 'main-div'
        };

        mainviewport = new Ext.Viewport(border);



        //======================= Test message sending ==================

        function SendTestMessage_ShowForm()
        {
          if(!SendMessage_ValidateInput())
            return false;

          window.testmsgform = new Ext.form.FormPanel({
            id: 'testmsgform',
            labelWidth: 75,
            title: 'Send Test Message',
            bodyStyle:'padding:15px',
            width: 380,
            labelPad: 10,
            defaultType: 'textfield',
            defaults: {
                // applied to each contained item
                width: 230,
                msgTarget: 'side'
            },
            layoutConfig: {
                // layout-specific configs go here
                labelSeparator: ''
            },
            items: [{
                    fieldLabel: 'To Name',
                    id: 'test-toname',
                    name: 'testtoname',
                    allowBlank: true
                },{
                    fieldLabel: 'To E-mail',
                    name: 'testtoemail',
                    id: 'test-toemail',
                    vtype:'email',
                    allowBlank: false
                }
            ],

            buttons: [
            {
                id: 'send-test-send',
                text: 'Send',
                handler: function() { SendTestMessage_SendMessage(); },
                type: 'submit'
            },
            {
                id: 'send-test-cancel',
                text: 'Cancel',
                handler: function() { SendTestMessage_CancelForm(); }
            }
            ]

          });

          window.testmsgformw = new Ext.Window({
            id: 'testmsgformwindow',
            items:[testmsgform],
            width: 390,
            resizable: false,
            modal: true
          });

          if(window.last_test_name)
            Ext.ComponentMgr.get('test-toname').setValue(window.last_test_name);
          if(window.last_test_email)
            Ext.ComponentMgr.get('test-toemail').setValue(window.last_test_email);

          //line commented because of ?bug? - focus appears and then instantly disappears
          //window.testmsgformw.on('show', function() { Ext.get('test-toname').focus(false, 10); } );
          testmsgformw.show();

          var map = new Ext.KeyMap('testmsgform',
          {
              key : 13,
              fn : function() { SendTestMessage_SendMessage(); }
          });



        }


        function SendTestMessage_CloseForm()
        {
          window.testmsgformw.hide();
          window.testmsgformw.destroy();
          Ext.Msg.hide();
        }


        function SendTestMessage_CancelForm()
        {
            SendTestMessage_CloseForm();
        }


        function SendTestMessage_SuccessHandler(response, options)
        {
            //alert('success: ' + response.responseText);
            SendTestMessage_CloseForm();
        }


        function SendTestMessage_FailureHandler(response, options)
        {
            alert('failure: ' + response.responseText);
            SendTestMessage_CloseForm();
        }


        function SendTestMessage_SendMessage()
        {
            if(window.testmsgform.form.isValid())
            {
                Ext.Msg.wait('Sending Test E-mail');

                var postparams = {
                    sendtest: '1',
                    sendtoemail: Ext.get('test-toemail').getValue(),
                    sendtoname: Ext.get('test-toname').getValue(),
                    sendfromemail: Ext.get('composeform-fromemail').getValue(),
                    sendfromname: Ext.get('composeform-fromname').getValue(),
                    sendsubject: Ext.get('composeform-subject').getValue(),
                    sendtext: Ext.get('composeform-text').getValue(),
                    sendtemplate: Ext.get('composeform-template').getValue(),
                    sendattachments: GetAttachmentsList()
                };
                for(f = 0; f < window.extracontentlist.length; f++)
                {
                    postparams['extracontent_' + window.extracontentlist[f].id] = window.extracontentlist[f].getValue();
                }
                Ext.Ajax.request({
                    url: '<?= '?module=dialogs&action=component&site_id='.$this->site_id.'&page_id='.$this->page_id.'&component_name='.$this->name.'&component_type='.get_class($this).'&method_name=send_mail_ajax_handler&refresh=0&okaction=submit&session_id=' . session_id() . '&session_name=' . session_name() ?>',
                    params: postparams,
                    success: SendTestMessage_SuccessHandler,
                    failure: SendTestMessage_FailureHandler
                });

                window.last_test_name = Ext.ComponentMgr.get('test-toname').getValue();
                window.last_test_email = Ext.ComponentMgr.get('test-toemail').getValue();

            }

        }

        //======================= All messages sending ==================

        function SendAllMessages_ShowForm()
        {
          if(!SendMessage_ValidateInput())
            return false;
          var postparams = {
              sendtoall: Ext.ComponentMgr.get('rcptlistform-all').getValue(),
              selectedlists: Ext.ComponentMgr.get('rcptlistform-rcptlists').getValue(),
          };

          Ext.Ajax.request({
              url: '<?= '?module=dialogs&action=component&site_id='.$this->site_id.'&page_id='.$this->page_id.'&component_name='.$this->name.'&component_type='.get_class($this).'&method_name=get_all_mails_count_ajax_handler&refresh=0&okaction=submit&session_id=' . session_id() . '&session_name=' . session_name() ?>',
              params: postparams,
              success: function(response){
                Ext.MessageBox.confirm('Confirm', 'Are you sure you want to send ' + response.responseText + ' emails?', SendAllMessages_SendMessage);
              },
              failure: function(response){
                Ext.MessageBox.confirm('Confirm', 'Are you sure you want to send ' + response.responseText + ' emails?', SendAllMessages_SendMessage);
              }
          });
        }

        function SendAllUnsentMessages_ShowForm()
        {
          var sel = grid_sent.getSelectionModel().getSelected();
          var postparams = {
              message_id: sel.id,
          };

          Ext.Ajax.request({
              url: '<?= '?module=dialogs&action=component&site_id='.$this->site_id.'&page_id='.$this->page_id.'&component_name='.$this->name.'&component_type='.get_class($this).'&method_name=get_all_unsent_mails_count_ajax_handler&refresh=0&okaction=submit&session_id=' . session_id() . '&session_name=' . session_name() ?>',
              params: postparams,
              success: function(response){
                if(response.responseText == 0 ) {
                  Ext.MessageBox.alert(' All emails already sent! ');
                } else {
                  Ext.MessageBox.confirm('Confirm', 'Are you sure you want to send ' + response.responseText + ' emails?', SendAllUnsentMessages_SendMessage);
                }
              },
              failure: function(response){
                Ext.MessageBox.alert(' Action Failed ');
              }
          });
        }

		function SendAllUnsentMessages_preview()
		{
      var sel = grid_sent.getSelectionModel().getSelected();
      if(sel)
      {
        preview_popup = new Ext.Window({
          modal:true,
          layout:'fit',
          width:550,
          height:450,
          autoScroll: true,
          autoLoad : {
            url : '?module=dialogs&action=component&site_id=<?= $this->site_id; ?>&page_id=<?= $this->page_id; ?>&component_name=<?= $this->name; ?>&component_type=<?= get_class($this); ?>&method_name=get_message_content&refresh=0&okaction=submit&session_id=<?= session_id(); ?>&session_name=<?= session_name(); ?>&message_id=' + sel.id,
          },
        });
        preview_popup.show();
      }

		}


        function SendAllMessages_CloseForm()
        {
          //window.testmsgformw.hide();
         // window.testmsgformw.destroy();
          Ext.Msg.hide();
        }


        function SendAllMessages_CancelForm()
        {
           Ext.MessageBox.hide();
        }


        function SendAllMessages_SuccessHandler(response, options)
        {
            //alert('success: ' + response.responseText);
            SendAllMessages_CloseForm();
        }


        function SendAllMessages_FailureHandler(response, options)
        {
          var  cookie_offset = parseFloat(getCookie('offset'));
          var  message_id = parseFloat(getCookie('message_id'));
          if (cookie_offset!=null && cookie_offset!="")
          {
            var offset = cookie_offset + <?php echo $this->emails_per_request; ?>;
            response = new Array();
            response['offset'] = parseFloat(offset);
            response['message_id'] = parseFloat(message_id);
            send_emails(response);
          }
           /* SendTAllMessages_CloseForm();*/
        }

        function SendAllUnsentMessages_FailureHandler(response, options)
        {
          var  cookie_offset = parseFloat(getCookie('offset'));
          var  message_id = parseFloat(getCookie('message_id'));
          if (cookie_offset!=null && cookie_offset!="")
          {
            var offset = cookie_offset + <?php echo $this->emails_per_request; ?>;
            response = new Array();
            response['offset'] = parseFloat(offset);
            response['message_id'] = parseFloat(message_id);
            send_unsent_emails(response);
          }
           /* SendTAllMessages_CloseForm();*/
        }


        function SendAllMessages_SendMessage(btn)
        {
          if(btn == 'no' )
          {
            return;
          }

          Ext.Msg.wait('Sending E-mails: ');

          var postparams = {
              sendfromemail: Ext.get('composeform-fromemail').getValue(),
              sendfromname: Ext.get('composeform-fromname').getValue(),
              sendsubject: Ext.get('composeform-subject').getValue(),
              sendtext: Ext.get('composeform-text').getValue(),
              sendtemplate: Ext.get('composeform-template').getValue(),
              sendtoall: Ext.ComponentMgr.get('rcptlistform-all').getValue(),
              selectedlists: Ext.ComponentMgr.get('rcptlistform-rcptlists').getValue(),
              sendattachments: GetAttachmentsList(),
              offset: '0',
          };
          for(f = 0; f < window.extracontentlist.length; f++)
          {
              postparams['extracontent_' + window.extracontentlist[f].id] = window.extracontentlist[f].getValue();
          }
          Ext.Ajax.request({
              url: '<?= '?module=dialogs&action=component&site_id='.$this->site_id.'&page_id='.$this->page_id.'&component_name='.$this->name.'&component_type='.get_class($this).'&method_name=send_all_mail_ajax_handler&refresh=0&okaction=submit&session_id=' . session_id() . '&session_name=' . session_name() ?>',
              params: postparams,
              success: send_emails,
              failure: SendAllMessages_FailureHandler
          });

         // window.last_test_name = Ext.ComponentMgr.get('test-toname').getValue();
         // window.last_test_email = Ext.ComponentMgr.get('test-toemail').getValue();
        }

        function send_emails(response)
        {
          console.log(response.responseText);
          if(response.responseText) {
            var response_data = $.parseJSON(response.responseText);
          }

          if(response_data.status == 'ok' )
          {
            Ext.Msg.alert('Email sending finished');
          }
          else
          {
            setCookie('offset',response_data.offset,365);
            setCookie('message_id',response_data.message_id,365);
            Ext.Msg.wait('Sending E-mails: ' + response_data.offset);
            var postparams = {
                sendfromemail: Ext.get('composeform-fromemail').getValue(),
                sendfromname: Ext.get('composeform-fromname').getValue(),
                sendsubject: Ext.get('composeform-subject').getValue(),
                sendtext: Ext.get('composeform-text').getValue(),
                sendtemplate: Ext.get('composeform-template').getValue(),
                sendtoall: Ext.ComponentMgr.get('rcptlistform-all').getValue(),
                selectedlists: Ext.ComponentMgr.get('rcptlistform-rcptlists').getValue(),
                sendattachments: GetAttachmentsList(),
                offset: response_data.offset,
                message_id: response_data.message_id
            };

            for(f = 0; f < window.extracontentlist.length; f++)
            {
                postparams['extracontent_' + window.extracontentlist[f].id] = window.extracontentlist[f].getValue();
            }
            Ext.Ajax.request({
                url: '<?= '?module=dialogs&action=component&site_id='.$this->site_id.'&page_id='.$this->page_id.'&component_name='.$this->name.'&component_type='.get_class($this).'&method_name=send_all_mail_ajax_handler&refresh=0&okaction=submit&session_id=' . session_id() . '&session_name=' . session_name() ?>',
                params: postparams,
                success: send_emails,
                failure: SendAllMessages_FailureHandler
            });
          }
        }


        function SendAllUnsentMessages_SendMessage(btn)
        {
          if(btn == 'no' )
          {
            return;
          }

          Ext.Msg.wait('Sending E-mails: ');

          var sel = grid_sent.getSelectionModel().getSelected();
          var postparams = {
              message_id: sel.id,
              offset: '0',
          };

          Ext.Ajax.request({
              url: '<?= '?module=dialogs&action=component&site_id='.$this->site_id.'&page_id='.$this->page_id.'&component_name='.$this->name.'&component_type='.get_class($this).'&method_name=send_all_unsent_mail_ajax_handler&refresh=0&okaction=submit&session_id=' . session_id() . '&session_name=' . session_name() ?>',
              params: postparams,
              success: send_unsent_emails,
              failure: SendAllUnsentMessages_FailureHandler
          });

        }

        function send_unsent_emails(response)
        {
          console.log(response.responseText);
          if(response.responseText) {
            var response_data = $.parseJSON(response.responseText);
          }

          if(response_data.status == 'ok' )
          {
            Ext.Msg.alert('Email sending finished');
          }
          else
          {
            setCookie('offset',response_data.offset,365);
            setCookie('message_id',response_data.message_id,365);
            Ext.Msg.wait('Sending E-mails: ' + response_data.offset);
            var postparams = {
                offset: response_data.offset,
                message_id: response_data.message_id
            };

            Ext.Ajax.request({
                url: '<?= '?module=dialogs&action=component&site_id='.$this->site_id.'&page_id='.$this->page_id.'&component_name='.$this->name.'&component_type='.get_class($this).'&method_name=send_all_unsent_mail_ajax_handler&refresh=0&okaction=submit&session_id=' . session_id() . '&session_name=' . session_name() ?>',
                params: postparams,
                success: send_unsent_emails,
                failure: SendAllUnsentMessages_FailureHandler
            });
          }
        }

        //========================= Message sending

        function SendMessage_ValidateInput()
        {
          if(!Ext.ComponentMgr.get('compose-message-form').form.isValid())
          {
            Ext.Msg.alert('Send Message', 'Please fill-in the required fields before sending the message!');
            Ext.ComponentMgr.get('compose-message-form').expand();
            return false;
          }

          return true;
        }

        function Sending_ScreenShow()
        {
          if(!SendMessage_ValidateInput())
            return false;

          pagelist.layout.setActiveItem('page-sending');
          GetSubscribersList();

        }

        function GetSubscribersList_SuccessHandler(response, options)
        {
            var a = Ext.decode(response.responseText);
            window.activeSubscribersList = a;
            window.activeSubscribersChecked = new Array();
            document.getElementById('page-sending-div').innerHTML = '';

            var s = new Array();
  //          for(n=0; n < 100; n++)
            for(i = 0; i < a.length; i++)
            {
              window.activeSubscribersChecked[i] = true;
              s[s.length] = '<div class="sendinglistitem"><input type="checkbox" id="sendto'+i+'" checked="checked" onclick="toggleReceiverSelected(' + i + ', this.checked);" /> <label for="sendto'+i+'">' + a[i] + '</label></div>';
            }
            document.getElementById('page-sending-div').innerHTML = s.join('');

            Ext.Msg.hide();
        }

        function GetSubscribersList_FailureHandler(response, options)
        {
            alert('failure: ' + response.responseText);
            Ext.Msg.hide();
        }

        function GetSubscribersList()
        {
          window.activeSubscribersList = new Array();
          window.activeSubscribersChecked = new Array();
          document.getElementById('page-sending-div').innerHTML = '';

          Ext.Msg.wait('Loading recipient e-mail list');

          Ext.Ajax.request({
              url: '<?= '?module=dialogs&action=component&site_id='.$this->site_id.'&page_id='.$this->page_id.'&component_name='.$this->name.'&component_type='.get_class($this).'&method_name=GetSubscribersList_ajax_handler&refresh=0&okaction=submit&session_id=' . session_id() . '&session_name=' . session_name() ?>',
              params: {
                  getemaillist: 1,
                  sendtoall: Ext.ComponentMgr.get('rcptlistform-all').getValue(),
                  selectedlists: Ext.ComponentMgr.get('rcptlistform-rcptlists').getValue()
              },
              success: GetSubscribersList_SuccessHandler,
              failure: GetSubscribersList_FailureHandler
          });
        }


        function InitializeSendingStart()
        {
          Ext.ComponentMgr.get('btn-sending-progress-back').disable();
          Ext.ComponentMgr.get('bnt-sending-progress-pause').enable();
          Ext.ComponentMgr.get('bnt-sending-progress-pause').setText('Pause');
          Ext.ComponentMgr.get('bnt-sending-progress-cancel').enable();
          pagelist.layout.setActiveItem('page-sending-progress');
          Ext.ComponentMgr.get('page-sending-progress').setTitle('Sending in progress');

          var a = window.activeSubscribersList;

          document.getElementById('page-sending-progress-div').innerHTML = '';

          var s = new Array();
          for(i = 0; i < a.length; i++)
          {
            if(window.activeSubscribersChecked[i])
            {
              s[s.length] = '<div class="sendinglistitem_ready" id="emailitem'+i+'">' + a[i] + '</div>';
            }
          }
          s = s.join('');
          document.getElementById('page-sending-progress-div').innerHTML = s;

          StartSendingProgress();

        }


        function ToggleSendingProgress()
        {
          if(!window.cancelSendingProgress)
          {
            window.cancelSendingProgress = true;
            Ext.ComponentMgr.get('bnt-sending-progress-pause').setText('Resume');
            sendprogress.reset(true);

          }else
          {
            StartSendingProgress();
          }
        }

        //=============== Recipient lists ===========

        function RedrawFilteredEmailGrid()
        {
          listpanel_emailgrid.getStore().filter('email', filterInput.getValue());

/*
          listpanel_emailgrid.getStore().load({
                params: {
                    start:0,
                    limit:24,
                    filterCol:'email',
                    filter: filterInput.getValue()
                }
            });
*/

        }

        function EnableDisableRcptLists()
        {
          if(Ext.ComponentMgr.get('rcptlistform-all').getValue())
          {
              Ext.ComponentMgr.get('rcptlistform-rcptlists').disable();
          }else
          {
              Ext.ComponentMgr.get('rcptlistform-rcptlists').enable();
          }
        }

        function GetRcptListNames_SuccessHandler(response, options)
        {
            var a = Ext.decode(response.responseText);

            //remove all nodes
            while(myroot2.firstChild)
            {
                myroot2.firstChild.remove();
            }

            //hide checkbox column
//            listpanel_emailgrid.getColumnModel().setHidden(listpanel_emailgrid.getColumnModel().findColumnIndex('sub'), true);

            //add list titles to tree
            for(f = 0; f < a.categories.length; f++)
            {
                var node = new Tree.TreeNode({
                        text: a.categories[f].title,
                        leaf: true
                });
                node.custom_id = a.categories[f].id;
                node.on('click', function(node) { displayRcptListSelections(node.custom_id); } );
                myroot2.appendChild(
                    node
                );
            }

            //reload list titles in import form
            //destroy if already exyists
            if(Ext.ComponentMgr.get('list-import-cats'))
            {
                //try catch because sometimes .remove() unregisters component from componentmgr and sometimes not (bug?)
                try
                {
                    listspanel_import.remove(Ext.ComponentMgr.get('list-import-cats'));
                } catch (e)
                {
                  //
                }
            }

            //create category checklist
            var chboxes = new Array();
            var node = myroot2.firstChild;
            while(node)
            {
                chboxes[chboxes.length] = new Ext.form.Checkbox({
                    boxLabel: node.text,
                    id: 'list-import-cat-check-' + node.custom_id,
                    width: 350,
                    inputValue: node.custom_id
                });
                node = node.nextSibling;
            }
            importlist_cats = new Ext.form.CheckboxGroup(
            {
                fieldLabel: 'Subscribed to',
                id: 'list-import-cats',
                disabled: false,
                width: 350,
                columns: 1,
                msgTarget: 'side',
                items: chboxes
            });
            listspanel_import.add(importlist_cats);
            //height is changed to force form re-rendering and applying scrollbars if neccessary
            listspanel_import.setHeight(listspanel_import.getSize().height);

            //reload e-mails list
            if(a.emails.length > 0)
            {
                emailgrid_proxy.data = a.emails;
            }

            RedrawFilteredEmailGrid();

            Ext.Msg.hide();
        }

        function GetRcptListNames_FailureHandler(response, options)
        {
            Ext.Msg.hide();
            alert('failure: ' + response.responseText);
        }

        //this method reads subscribers list names and e-mails
        //  to reload contents of tree in Lists Manager and checkgroup of Import E-mails form
        //  and e-mails grid in Lists Manager
        function GetRcptListNames()
        {
          Ext.Msg.wait('Loading subscriber list names');

          Ext.Ajax.request({
              url: '<?= '?module=dialogs&action=component&site_id='.$this->site_id.'&page_id='.$this->page_id.'&component_name='.$this->name.'&component_type='.get_class($this).'&method_name=GetCategoryAndEmailList_ajax_handler&refresh=0&okaction=submit&session_id=' . session_id() . '&session_name=' . session_name() ?>',
              params: {
                  getcategorylist: 1
              },
              success: GetRcptListNames_SuccessHandler,
              failure: GetRcptListNames_FailureHandler
          });

          listpanel_emailgrid.getSelectionModel().clearSelections();

        }

        function displayRcptListSelections_SuccessHandler(response, options)
        {
            Ext.Msg.hide();
            var a = Ext.decode(response.responseText);

            var b = {};
            for(var f = 0; f < a.length; f++)
            {
              b[a[f]] = 0;
            }

            //listpanel_emailgrid.getStore().clearFilter();

            listpanel_emailgrid.hide();

            var data = emailgrid_proxy.data;
            for(var f=0; f < data.length; f++)
            {
              //if(a.inArray(data[f][0]) == -1)
              if(!(data[f][0] in b))
                  data[f][3] = 0;
              else
                  data[f][3] = 1;
            }

            emailgrid_proxy.data = a; //data);

            RedrawFilteredEmailGrid();

            //listpanel_emailgrid.getStore().commitChanges();
            listpanel_emailgrid.show();

            var sst = listpanel_emailgrid.getStore().getSortState();
            if(!sst || sst.field == 'sub')
            {
              listpanel_emailgrid.getStore().sort('sub', 'DESC');
            }

           //performEmailFiltering(filterInput);
        }

        function displayRcptListSelections_FailureHandler(response, options)
        {
            Ext.Msg.hide();
            alert('failure: ' + response.responseText);
        }

      var active_cat_id;

        function displayRcptListSelections(cat_id)
        {
          active_cat_id = cat_id;

          Ext.Msg.wait('Loading subscribers for this list');

          SaveModifiedSubscribersList();

          Ext.Ajax.request({
              url: '<?= '?module=dialogs&action=component&site_id='.$this->site_id.'&page_id='.$this->page_id.'&component_name='.$this->name.'&component_type='.get_class($this).'&method_name=GetSubscribersForCategory_ajax_handler&refresh=0&okaction=submit&session_id=' . session_id() . '&session_name=' . session_name() ?>',
              params: {
                  category: cat_id
              },
              success: displayRcptListSelections_SuccessHandler,
              failure: displayRcptListSelections_FailureHandler
          });

//          listpanel_emailgrid.getColumnModel().setHidden(listpanel_emailgrid.getColumnModel().findColumnIndex('sub'), false);

          listpanel_emailgrid.getSelectionModel().clearSelections();

        }

        function Update_Subscriberlist_SuccessHandler(response, options)
        {
            if(response.responseText != 'ok')
            {
                Update_Subscriberlist_FailureHandler(response, options);
            }
        }

        function Update_Subscriberlist_FailureHandler(response, options)
        {
            alert('failure: ' + response.responseText);
        }

        function SaveModifiedSubscribersList()
        {
            var recs = listpanel_emailgrid.getStore().getModifiedRecords();
            var a = Array();
            for(f = 0; f < recs.length; f++)
            {
                a[a.length] = {id: recs[f].id, on: recs[f].get('sub')};
            }

            //if any records changed and any subscriber list selected
            if(a.length && rcptlists_tree.getSelectionModel().getSelectedNode())
            {
                Ext.Ajax.request({
                    url: '<?= '?module=dialogs&action=component&site_id='.$this->site_id.'&page_id='.$this->page_id.'&component_name='.$this->name.'&component_type='.get_class($this).'&method_name=update_subscriberlist_ajax_handler&refresh=0&okaction=submit&session_id=' . session_id() . '&session_name=' . session_name() ?>',
                    params: {
                        update_changes: Ext.encode(a),
                        list_id: rcptlists_tree.getSelectionModel().getSelectedNode().custom_id
                    },
                    success: Update_Subscriberlist_SuccessHandler,
                    failure: Update_Subscriberlist_FailureHandler
                });
            }

            listpanel_emailgrid.getStore().commitChanges();
        }

        //=== recipient lists manager toolbar - e-mail addresses


        function createaddemailform()
        {
            addemailform = new Ext.form.FormPanel({
                id: 'addemailform',
                labelWidth: 75,
                title: 'Add Subscriber',
                bodyStyle:'padding:15px',
                width: 380,
                labelPad: 10,
                defaultType: 'textfield',
                defaults: {
                    // applied to each contained item
                    width: 230,
                    msgTarget: 'side'
                },
                layoutConfig: {
                    // layout-specific configs go here
                    labelSeparator: ''
                },
                items: [{
                        fieldLabel: 'Name',
                        id: 'add-email-name',
                        name: 'addemailname',
                        allowBlank: true
                    },{
                        fieldLabel: 'E-mail',
                        id: 'add-email-email',
                        vtype:'email',
                        allowBlank: false
                    }
                ],

                buttons: [
                {
                    id: 'btn-add-email-add',
                    text: 'Add',
                    handler: function() { AddSubscribedEmailSave(); },
                    type: 'submit'
                },
                {
                    id: 'btn-add-email-save',
                    text: 'Save',
                    handler: function() { EditSubscribedEmailSave(); },
                    type: 'submit',
                    hidden: true
                },
                {
                    id: 'btn-add-email-cancel',
                    text: 'Cancel',
                    handler: function() { addemailformw.hide(); addemailformw.destroy(); }
                }
                ]

            });

            var chboxes = new Array();
            var node = myroot2.firstChild;
            while(node)
            {
                chboxes[chboxes.length] = new Ext.form.Checkbox({
                    boxLabel: node.text,
                    id: 'add-email-cat-check-' + node.custom_id,
                    width: 350,
                    inputValue: node.custom_id,
                    checked: (node.custom_id == active_cat_id)
                });
                node = node.nextSibling;
            }

            var allcategories = new Ext.form.CheckboxGroup(
            {
                fieldLabel: 'Subscribed to',
                id: 'add-email-cats',
                disabled: false,
                width: 350,
                columns: 1,
                msgTarget: 'side',
                items: chboxes
            });

            addemailform.add(allcategories);

            addemailformw = new Ext.Window({
                id: 'addemailformwindow',
                items:[addemailform],
                width: 390,
                resizable: false,
                modal: true
            });
        }


        function AddSubscribedEmail()
        {
            createaddemailform();

            addemailformw.show();

            var map = new Ext.KeyMap('addemailform',
            {
                key : 13,
                fn : function() { AddSubscribedEmailSave(); }
            });
        }

        function AddSubscribedEmailSave_SuccessHandler(response, options)
        {
            if(response.responseText != 'ok')
            {
              alert('Error while saving e-mail.\nMake sure specified E-mail address is not already listed!');
            }
            Ext.Msg.hide();
            displayRcptListSelections(active_cat_id);
//            GetRcptListNames();
        }

        function AddSubscribedEmailSave_FailureHandler(response, options)
        {
            alert('failure: ' + response.responseText);
            Ext.Msg.hide();
        }

        function AddSubscribedEmailSave()
        {
            if(addemailform.form.isValid())
            {
                Ext.Msg.wait('Saving E-mail...');

                Ext.Ajax.request({
                    url: '<?= '?module=dialogs&action=component&site_id='.$this->site_id.'&page_id='.$this->page_id.'&component_name='.$this->name.'&component_type='.get_class($this).'&method_name=add_email_ajax_handler&refresh=0&okaction=submit&session_id=' . session_id() . '&session_name=' . session_name() ?>',
                    params: {
                        name: Ext.get('add-email-name').getValue(),
                        email: Ext.get('add-email-email').getValue(),
                        lists: Ext.ComponentMgr.get('add-email-cats').getValue()
                    },
                    success: AddSubscribedEmailSave_SuccessHandler,
                    failure: AddSubscribedEmailSave_FailureHandler
                });


                addemailformw.hide();
                addemailformw.destroy();
            }
        }


        function EditSubscribedEmail()
        {
            var sel = listpanel_emailgrid.getSelectionModel().getSelected();
            if(sel)
            {
                createaddemailform();

                Ext.ComponentMgr.get('btn-add-email-add').hide();
                Ext.ComponentMgr.get('btn-add-email-save').show();

                Ext.ComponentMgr.get('add-email-name').setValue(sel.get('name'));
                Ext.ComponentMgr.get('add-email-email').setValue(sel.get('email'));
                addemailform.remove('add-email-cats');

                addemailform.setTitle('Edit subscriber');
                addemailformw.show();

                var map = new Ext.KeyMap('addemailform',
                {
                    key : 13,
                    fn : function() { EditSubscribedEmailSave(); }
                });
            }
        }


        function EditSubscribedEmailSave_SuccessHandler(response, options)
        {
            Ext.Msg.hide();
            displayRcptListSelections(active_cat_id);
//            GetRcptListNames();
        }

        function EditSubscribedEmailSave_FailureHandler(response, options)
        {
            alert('failure: ' + response.responseText);
            Ext.Msg.hide();
        }

        function EditSubscribedEmailSave()
        {
            if(addemailform.form.isValid())
            {
                Ext.Msg.wait('Saving changes...');

                Ext.Ajax.request({
                    url: '<?= '?module=dialogs&action=component&site_id='.$this->site_id.'&page_id='.$this->page_id.'&component_name='.$this->name.'&component_type='.get_class($this).'&method_name=edit_email_ajax_handler&refresh=0&okaction=submit&session_id=' . session_id() . '&session_name=' . session_name() ?>',
                    params: {
                        name: Ext.get('add-email-name').getValue(),
                        email: Ext.get('add-email-email').getValue(),
                        id: listpanel_emailgrid.getSelectionModel().getSelected().id
                    },
                    success: EditSubscribedEmailSave_SuccessHandler,
                    failure: EditSubscribedEmailSave_FailureHandler
                });


                addemailformw.hide();
                addemailformw.destroy();
            }
        }


        function DeleteSubscribedEmailSave_SuccessHandler(response, options)
        {
            Ext.Msg.hide();

            displayRcptListSelections(active_cat_id);
//            GetRcptListNames();
        }

        function DeleteSubscribedEmailSave_FailureHandler(response, options)
        {
            alert('failure: ' + response.responseText);
            Ext.Msg.hide();
        }

        function DeleteSubscribedEmail()
        {
            var sel = listpanel_emailgrid.getSelectionModel().getSelected();
            if(sel)
            {
                Ext.Msg.wait('Deleting item...');

                Ext.Ajax.request({
                    url: '<?= '?module=dialogs&action=component&site_id='.$this->site_id.'&page_id='.$this->page_id.'&component_name='.$this->name.'&component_type='.get_class($this).'&method_name=delete_email_ajax_handler&refresh=0&okaction=submit&session_id=' . session_id() . '&session_name=' . session_name() ?>',
                    params: {
                        id: sel.id,
                        cat_id: active_cat_id
                    },
                    success: DeleteSubscribedEmailSave_SuccessHandler,
                    failure: DeleteSubscribedEmailSave_FailureHandler
                });
            }
        }


        function DeleteListEmails()
        {
          if (!confirm('<?=$this->LS("confirm_empty_list") ?>')) return;

          Ext.Msg.wait('Deleting list e-mails...');

          Ext.Ajax.request({
              url: '<?= '?module=dialogs&action=component&site_id='.$this->site_id.'&page_id='.$this->page_id.'&component_name='.$this->name.'&component_type='.get_class($this).'&method_name=delete_list_emails_ajax_handler&refresh=0&okaction=submit&session_id=' . session_id() . '&session_name=' . session_name() ?>',
              params: {
                cat_id: active_cat_id
              },
              success: function(response, options)
              {
                Ext.Msg.hide();
                displayRcptListSelections(active_cat_id);
              },
              failure: function(response, options)
              {
                alert('failure: ' + response.responseText);
                Ext.Msg.hide();
              }
          });


        }

        //=== recipient lists manager toolbar: recipient lists

        function createaddlistform()
        {
            addlistform = new Ext.form.FormPanel({
                id: 'addlistform',
                labelWidth: 75,
                title: 'Add Subscribers List',
                bodyStyle:'padding:15px',
                width: 380,
                labelPad: 10,
                defaultType: 'textfield',
                defaults: {
                    // applied to each contained item
                    width: 230,
                    msgTarget: 'side'
                },
                layoutConfig: {
                    // layout-specific configs go here
                    labelSeparator: ''
                },
                items: [{
                        fieldLabel: 'List title',
                        id: 'add-list-title',
                        name: 'addlisttitle',
                        allowBlank: false
                    }
                ],

                buttons: [
                {
                    id: 'btn-add-list-add',
                    text: 'Add',
                    handler: function() { AddSubscribersListSave(); },
                    type: 'submit'
                },
                {
                    id: 'btn-add-list-save',
                    text: 'Save',
                    handler: function() { EditSubscribersListSave(); },
                    type: 'submit',
                    hidden: true
                },
                {
                    id: 'btn-add-list-cancel',
                    text: 'Cancel',
                    handler: function() { addlistformw.hide(); addlistformw.destroy(); }
                }
                ]

            });

            addlistformw = new Ext.Window({
                id: 'addlistformwindow',
                items:[addlistform],
                width: 390,
                resizable: false,
                modal: true
            });
        }


        function AddSubscribersList()
        {
            createaddlistform();

            addlistformw.show();

            var map = new Ext.KeyMap('addlistform',
            {
                key : 13,
                fn : function() { AddSubscribersListSave(); }
            });
        }


        function AddSubscribersListSave_SuccessHandler(response, options)
        {
            Ext.Msg.hide();
            GetRcptListNames();
        }

        function AddSubscribersListSave_FailureHandler(response, options)
        {
            alert('failure: ' + response.responseText);
            Ext.Msg.hide();
        }

        function AddSubscribersListSave()
        {
            if(addlistform.form.isValid())
            {
                Ext.Msg.wait('Creating new list...');

                Ext.Ajax.request({
                    url: '<?= '?module=dialogs&action=component&site_id='.$this->site_id.'&page_id='.$this->page_id.'&component_name='.$this->name.'&component_type='.get_class($this).'&method_name=add_list_ajax_handler&refresh=0&okaction=submit&session_id=' . session_id() . '&session_name=' . session_name() ?>',
                    params: {
                        title: Ext.get('add-list-title').getValue()
                    },
                    success: AddSubscribersListSave_SuccessHandler,
                    failure: AddSubscribersListSave_FailureHandler
                });


                addlistformw.hide();
                addlistformw.destroy();
            }
        }


        function EditSubscribersList()
        {
            var sel = rcptlists_tree.getSelectionModel().getSelectedNode();
            if(sel)
            {
                createaddlistform();

                Ext.ComponentMgr.get('btn-add-list-add').hide();
                Ext.ComponentMgr.get('btn-add-list-save').show();

                Ext.ComponentMgr.get('add-list-title').setValue(sel.text);

                addlistform.setTitle('Edit Subscribers List');
                addlistformw.show();

                var map = new Ext.KeyMap('addlistform',
                {
                    key : 13,
                    fn : function() { EditSubscribersListSave(); }
                });
            }
        }


        function EditSubscribersListSave_SuccessHandler(response, options)
        {
            Ext.Msg.hide();
            GetRcptListNames();
        }

        function EditSubscribersListSave_FailureHandler(response, options)
        {
            alert('failure: ' + response.responseText);
            Ext.Msg.hide();
        }

        function EditSubscribersListSave()
        {
            if(addlistform.form.isValid())
            {
                Ext.Msg.wait('Saving changes...');

                Ext.Ajax.request({
                    url: '<?= '?module=dialogs&action=component&site_id='.$this->site_id.'&page_id='.$this->page_id.'&component_name='.$this->name.'&component_type='.get_class($this).'&method_name=edit_list_ajax_handler&refresh=0&okaction=submit&session_id=' . session_id() . '&session_name=' . session_name() ?>',
                    params: {
                        title: Ext.get('add-list-title').getValue(),
                        id: rcptlists_tree.getSelectionModel().getSelectedNode().custom_id
                    },
                    success: EditSubscribersListSave_SuccessHandler,
                    failure: EditSubscribersListSave_FailureHandler
                });


                addlistformw.hide();
                addlistformw.destroy();
            }
        }


        function DeleteSubscribersList_SuccessHandler(response, options)
        {
            Ext.Msg.hide();
            GetRcptListNames();
        }

        function DeleteSubscribersList_FailureHandler(response, options)
        {
            alert('failure: ' + response.responseText);
            Ext.Msg.hide();
        }

        function DeleteSubscribersList()
        {
            var sel = rcptlists_tree.getSelectionModel().getSelectedNode();
            if(sel)
            {
                if(confirm('Are you sure you want to delete list "' + sel.text + '"?'))
                {
                    Ext.Msg.wait('Deleting item...');

                    Ext.Ajax.request({
                        url: '<?= '?module=dialogs&action=component&site_id='.$this->site_id.'&page_id='.$this->page_id.'&component_name='.$this->name.'&component_type='.get_class($this).'&method_name=delete_list_ajax_handler&refresh=0&okaction=submit&session_id=' . session_id() . '&session_name=' . session_name() ?>',
                        params: {
                            id: sel.custom_id
                        },
                        success: DeleteSubscribersList_SuccessHandler,
                        failure: DeleteSubscribersList_FailureHandler
                    });
                }
            }
        }

        // ======= Recipient lists: import ===

        function SubmitImportList_SuccessHandler(response, options)
        {
            Ext.Msg.alert('Import finished', response.responseText + ' items were added');

            Ext.ComponentMgr.get('lists-import-data').setValue('');
            Ext.ComponentMgr.get('list-import-cats').setValue('');
        }

        function SubmitImportList_FailureHandler(response, options)
        {
            alert('failure: ' + response.responseText);
            Ext.Msg.hide();
        }

        function SubmitImportList()
        {
          Ext.Msg.wait('Uploading e-mail list...');

          Ext.Ajax.request({
              url: '<?= '?module=dialogs&action=component&site_id='.$this->site_id.'&page_id='.$this->page_id.'&component_name='.$this->name.'&component_type='.get_class($this).'&method_name=import_list_ajax_handler&refresh=0&okaction=submit&session_id=' . session_id() . '&session_name=' . session_name() ?>',
              params: {
                  importemaillist: 1,
                  emails: Ext.ComponentMgr.get('lists-import-data').getValue(),
                  selectedlists: Ext.ComponentMgr.get('list-import-cats').getValue()
              },
              success: import_emails,
              failure: SubmitImportList_FailureHandler
          });
        }

        function import_emails(response,importemailslist, selectedlists)
        {
          if(response.responseText && (IsNumeric(response.responseText) || response.responseText == "" ))
          {
            Ext.Msg.alert('Import finished');
            Ext.ComponentMgr.get('lists-import-data').setValue('');
            Ext.ComponentMgr.get('list-import-cats').setValue('');
          }
          else
          {
            Ext.Ajax.request({
                url: '<?= '?module=dialogs&action=component&site_id='.$this->site_id.'&page_id='.$this->page_id.'&component_name='.$this->name.'&component_type='.get_class($this).'&method_name=import_list_ajax_handler&refresh=0&okaction=submit&session_id=' . session_id() . '&session_name=' . session_name() ?>',
                params: {
                    importemaillist: 1,
                    emails: response.responseText,
                    selectedlists: Ext.ComponentMgr.get('list-import-cats').getValue()
                },
                success: import_emails,
                failure: SubmitImportList_FailureHandler
            });
          }
        }

      function IsNumeric(input)
      {
          return (input - 0) == input && input.length > 0;
      }


        //============== Extra content ======================

        function InitializeExtraContentForm_SuccessHandler(response, options)
        {
            //alert(response.responseText);
            window.extracontentlist = new Array();
            var list = Ext.decode(response.responseText);
            for(f = 0; f < list.length; f++)
            {
                if(list[f].items.length > 0)
                {
                    var chboxes = new Array();
                    for(i = 0; i < list[f].items.length; i++)
                    {
                        chboxes[chboxes.length] = new Ext.form.Checkbox({
                            boxLabel: list[f].items[i].title,
                            id: 'extracontent-list-check-' + i,
                            width: 350,
                            inputValue: list[f].items[i].id
                        });
                    }

                    window.extracontentlist[f] = new Ext.form.CheckboxGroup(
                    {
                        fieldLabel: list[f].title,
                        id: 'extracontent-list-' + list[f].name,
                        disabled: false,
                        width: 350,
                        columns: 1,
                        msgTarget: 'side',
                        items: chboxes
                    });

                    extracontentform.add(window.extracontentlist[f]);
                }
            }

            initextracontentformprogress.reset(true);
        }


        function InitializeExtraContentForm_FailureHandler(response, options)
        {
            alert('failure: ' + response.responseText);
        }


        function InitializeExtraContentForm()
        {
            initextracontentformprogress.wait({
                text: 'Loading extra content data...'
            });

            Ext.Ajax.request({
                url: '<?= '?module=dialogs&action=component&site_id='.$this->site_id.'&page_id='.$this->page_id.'&component_name='.$this->name.'&component_type='.get_class($this).'&method_name=get_extra_content_ajax_handler&refresh=0&okaction=submit&session_id=' . session_id() . '&session_name=' . session_name() ?>',
                params: {
                    getextracontent: '1'
                },
                success: InitializeExtraContentForm_SuccessHandler,
                failure: InitializeExtraContentForm_FailureHandler
            });
        }


        //===========================================




      }
    );


    // GLOBAL functions

    function toggleReceiverSelected(i, state)
    {
      window.activeSubscribersChecked[i] = state;
    }


    function StopSendingProgress()
    {
      window.cancelSendingProgress = true;
      Ext.ComponentMgr.get('btn-sending-progress-back').enable();
      Ext.ComponentMgr.get('bnt-sending-progress-pause').disable();
      Ext.ComponentMgr.get('bnt-sending-progress-cancel').disable();

      Ext.ComponentMgr.get('page-sending-progress').setTitle('Sending stopped');

      Ext.ComponentMgr.get('progressbar-send-progress').reset(true);
    }

    function SendMailMessage_SuccessHandler(response, options)
    {
        //alert('success: ' + response.responseText);
        document.getElementById('emailitem' + options.emailindex).className = 'sendinglistitem_success';
    }


    function SendMailMessage_FailureHandler(response, options)
    {
        //alert('failure: ' + response.responseText);
        document.getElementById('emailitem' + options.emailindex).className = 'sendinglistitem_failure';
        document.getElementById('emailitem' + options.emailindex).innerHTML = document.getElementById('emailitem' + options.emailindex).innerHTML + '<br />' + response.responseText;
    }


    function GetAttachmentsList()
    {
      var a = '';
      for(var i = 0; i < 5; i++)
      {
        var field = Ext.ComponentMgr.get('attachmentsform-file' + i);
        if(field && field.getValue())
        {
          a = a + ';' + field.getValue();
        }
      }
      return a;
    }


    function SendMailMessage_SendMessage(num)
    {
        var postparams = {
                sendmsg: '1',
                sendtoemail: window.activeSubscribersList[num],
                //sendtoname: Ext.get('test-toname').getValue(),
                sendfromemail: Ext.get('composeform-fromemail').getValue(),
                sendfromname: Ext.get('composeform-fromname').getValue(),
                sendsubject: Ext.get('composeform-subject').getValue(),
                sendtext: Ext.get('composeform-text').getValue(),
                sendtemplate: Ext.get('composeform-template').getValue(),
                sendattachments: GetAttachmentsList()
        };
        for(f = 0; f < window.extracontentlist.length; f++)
        {
            postparams['extracontent_' + window.extracontentlist[f].id] = window.extracontentlist[f].getValue();
        }
        Ext.Ajax.request({
            url: '<?= '?module=dialogs&action=component&site_id='.$this->site_id.'&page_id='.$this->page_id.'&component_name='.$this->name.'&component_type='.get_class($this).'&method_name=send_mail_ajax_handler&refresh=0&okaction=submit&session_id=' . session_id() . '&session_name=' . session_name() ?>',
            params: postparams,
            success: SendMailMessage_SuccessHandler,
            failure: SendMailMessage_FailureHandler,
            emailindex: num
        });
    }


    function StartSendingProgress()
    {
        window.cancelSendingProgress = false;

        Ext.ComponentMgr.get('bnt-sending-progress-pause').setText('Pause');

        Ext.ComponentMgr.get('progressbar-send-progress').wait({
          text: 'Send in progress...'
        });
        Ext.ComponentMgr.get('progressbar-send-progress').show();

        var postparams = {
          active_subscribers: window.activeSubscribersList,
          sendfromemail: Ext.get('composeform-fromemail').getValue(),
          sendfromname: Ext.get('composeform-fromname').getValue(),
          sendsubject: Ext.get('composeform-subject').getValue(),
          sendtext: Ext.get('composeform-text').getValue(),
          sendtemplate: Ext.get('composeform-template').getValue(),
          sendattachments: GetAttachmentsList()
        };
        Ext.Ajax.request({
          url: '<?= '?module=dialogs&action=component&site_id='.$this->site_id.'&page_id='.$this->page_id.'&component_name='.$this->name.'&component_type='.get_class($this).'&method_name=save_message_ajax_handler&refresh=0&okaction=submit&session_id=' . session_id() . '&session_name=' . session_name() ?>',
          params: postparams,
          success: function() {

            setTimeout('SendNextMail()', window.sendinterval);
          },
          failure: function() {
            Ext.Msg.alert('failed', 'failed');
          }
        });
    }


    function SendNextMail()
    {
      if(window.cancelSendingProgress)
        return false;

      //find next unsent mail
      i = -1;
      var a = window.activeSubscribersList;
      for(f = 0; f < a.length; f++ )
      {
        if(window.activeSubscribersChecked[f])
        {
          i = f;
          break;
        }
      }

      if(i > -1)
      {
        window.activeSubscribersChecked[f] = false;
        document.getElementById('emailitem' + i).className = 'sendinglistitem_sending';

        Ext.get('emailitem' + i).scrollIntoView(Ext.get('page-sending-progress-div'));

        //send sequence
        SendMailMessage_SendMessage(i);

        //keep the sending chain going
        if(!window.sendinterval)
            window.sendinterval = <?= $this->getProperty('sendinterval') * 1000; ?>;
        if(!window.cancelSendingProgress)
          setTimeout('SendNextMail()', window.sendinterval);
      }else
      {
        //finished
        StopSendingProgress();
        Ext.ComponentMgr.get('page-sending-progress').setTitle('Sending finished');

      }

    }

    function getCookie(c_name)
    {
      var i,x,y,ARRcookies=document.cookie.split(";");
      for (i=0;i<ARRcookies.length;i++)
      {
      x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
      y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
      x=x.replace(/^\s+|\s+$/g,"");
      if (x==c_name)
        {
        return unescape(y);
        }
      }
    }

    function setCookie(c_name,value,exdays)
    {
      var exdate=new Date();
      exdate.setDate(exdate.getDate() + exdays);
      var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
      document.cookie=c_name + "=" + c_value;
    }

    function isNumber(n) {
      return !isNaN(parseFloat(n)) && isFinite(n);
    }

    </script>
    <?

    echo '<div id="main-div"></div>';

  }


  function scriptInitialization()
  {
    ?>

        //============== ExtJs functionality modifiers ======
        Ext.override(Ext.form.CheckboxGroup, {
            /**
             * @cfg {String} name The field's HTML name attribute (defaults to "").
             */

            /**
             * @cfg {string} separator String seperator between multiple values
             */
            separator: ';',


            /**
             * @method getValue
             * @hide
             */
            getValue : function() {
                if(!this.rendered) {
                    return this.value;
                }
                var v = [];
                this.items.each(function(i) {
                    if(i.getValue()) v.push(i.inputValue);
                });
                return v.join(this.separator);
            }
        });


        Ext.grid.CheckColumn = function(config){
            Ext.apply(this, config);
            if(!this.id){
                this.id = Ext.id();
            }
            this.renderer = this.renderer.createDelegate(this);
        };

        Ext.grid.CheckColumn.prototype ={
            init : function(grid){
                this.grid = grid;
                this.grid.on('render', function(){
                    var view = this.grid.getView();
                    view.mainBody.on('mousedown', this.onMouseDown, this);
                }, this);
            },

            onMouseDown : function(e, t){
                if(t.className && t.className.indexOf('x-grid3-cc-'+this.id) != -1){
                    e.stopEvent();
                    var index = this.grid.getView().findRowIndex(t);
                    var record = this.grid.store.getAt(index);
                    record.set(this.dataIndex, !record.data[this.dataIndex]);
                    if(this.proxy)
                      this.proxy.setValueById(record.get('id'), this.grid.getColumnModel().findColumnIndex(this.dataIndex), record.data[this.dataIndex]);
                }
            },

            renderer : function(v, p, record){
                p.css += ' x-grid3-check-col-td';
                return '<div class="x-grid3-check-col'+(v?'-on':'')+' x-grid3-cc-'+this.id+'">&#160;</div>';
            }
        };


        Array.prototype.inArray = function (value) {
        	var i;
        	for (i=0; i < this.length; i++) {
        		if (this[i] === value) {
        			return true;
        		}
        	}
        	return -1;
        };


        //Array.filter (by default not supported by IE)
        if (!Array.prototype.filter)
        {
          Array.prototype.filter = function(fun /*, thisp*/)
          {
            var len = this.length;
            if (typeof fun != "function")
              throw new TypeError();

            var res = new Array();
            var thisp = arguments[1];
            for (var i = 0; i < len; i++)
            {
              if (i in this)
              {
                var val = this[i]; // in case fun mutates this
                if (fun.call(thisp, val, i, this))
                  res.push(val);
              }
            }

            return res;
          };
        }



    <?
  }


  function templatepreview()
  {
    $template = $_GET['template'];

    //get site configuration
    list($domain, $dirroot) = sqlQueryRow("select domain, dirroot from sites where site_id=".$this->site_id);

    $unsubhash = 'sample';
    $unsubpath = PagePathById($this->page_id, $this->site_id);
    $unsubpath = 'http://' . $domain . '/' . $unsubpath . '?maillist_unsubscribe=' . $unsubhash;

    $configuration = Array(
      'domain' => $domain,
      'dirroot' => $dirroot,
      'toemail' => 'sample@sample.lv',
      'toname' => 'sample@sample.lv',
      'fromemail' => 'sample@sample.lv',
      'fromname' => 'sample@sample.lv',
      'unsubscribeurl' => $unsubpath
    );

    $mailtext = 'Fusce lobortis purus in orci. In lacus pede, convallis non, sollicitudin eu, sagittis non, orci. Curabitur imperdiet augue ultrices mi. Praesent in risus. Integer sed quam. Etiam lobortis dui eget erat. In pellentesque nisi eget justo vehicula pellentesque. Suspendisse auctor fermentum lorem.';
    $lists = Array(
      'sample' => Array('1', '2'),
    );

    $htmlmail = $this->apply_template($template, $configuration, $mailtext, $lists);

    echo $htmlmail;


  }


  //========================== Ajax request handler methods ====================

  function GetSubscribersList_ajax_handler()
  {
    $emails = $this->getSubscribersList($_POST['sendtoall'], $_POST['selectedlists']);
    echo json_encode($emails);
    die(); //so that no templates are sent
  }

  function getSubscribersList($send_to_all = false, $category = 0, $count = 0, $offset = 0)
  {
    if(!$this->InitCollections())
    die();
    if(strtolower($send_to_all == 'true'))
    {
      $params = array();
      $params['where']  = 'maillist_status = 1';
      if($count != 0 )
      {
        $params['offset'] = $offset;
        $params['count']  = $count;
      }
      $data = $this->emailcollection->GetDBData($params);
    }
    else
    {
      $cat_ids = explode(';', $category);
      //$cat_ids[] = -1; //always send to those subscribed to all lists
      $data = $this->emailcollection->GetEmailsFromCategories($cat_ids, $this->emailcatrelationcollection, $count, $offset);
    }

    $emails = Array();
    if($data)
    {
      foreach($data as $row)
      {
        if(!in_array($row['maillist_email'], $emails))
        {
          $emails[] = $row['maillist_email'];
        }
      }
    }

    return $emails;
  }

  function getUnsentSubscribersList($message_id)
  {
    if(!$this->InitCollections())
    die();
    $message = $this->messagescollection->getMessageById($message_id);

    if(in_array(0,$message['groups']))
    {
      $data = $this->emailcollection->getAllUnsentEmails($message_id,$this->logcollection);
    }
    else
    {
      $data = $this->emailcollection->GetUnsetEmailsFromCategories($message_id, $message['groups'], $this->emailcatrelationcollection, $this->logcollection);
    }

    $emails = Array();
    if($data)
    {
      foreach($data as $row)
      {
        if(!in_array($row['maillist_email'], $emails))
        {
          $emails[] = $row['maillist_email'];
        }
      }
    }

    return $emails;
  }


  function getSubscribersListCount($send_to_all = false, $category = 0)
  {
    if(!$this->InitCollections())
    die();
    if(strtolower($send_to_all == 'true'))
    {
      $data = $this->emailcollection->GetDBData(array(
      'what'  => 'count(*) as count',
      'where' => 'maillist_status = 1',
      ));
    }
    else
    {
      $cat_ids = explode(';', $category);
      //$cat_ids[] = -1; //always send to those subscribed to all lists
      $data = $this->emailcollection->GetEmailsFromCategoriesCount($cat_ids, $this->emailcatrelationcollection);
    }

    $count = 0;
    if($data)
    {
      foreach($data as $row)
      {
        $count = $row['count'];
      }
    }

    return $count;
  }

  function getUnsentSubscribersCount($message_id) {
    if(!$this->InitCollections())
    die();

    $message = $this->messagescollection->getMessageById($message_id);

    if(strtolower(in_array(0,$message['groups'])))
    { // send to all
      $count = $this->emailcollection->getAllUsentEmailsCount($message_id,$this->logcollection);
    }
    else
    { //send to selected groups
      $count = $this->emailcollection->GetUnsetEmailsFromCategoriesCount($message_id, $message['groups'], $this->emailcatrelationcollection, $this->logcollection);
    }

    return $count;

  }



  //returns JSON array in format:
  // [
  //   title => 'List title (Site news)'
  //   name  => 'List unique identifier (news)'
  //   items =>
  //     [
  //       id => item id (item_id)
  //       title => 'Item title (Title of news item)'
  //     ]
  // ]
  // multiple lists are allowed; override this function to add custom lists
  function get_extra_content_ajax_handler()
  {
    $ret = Array();

    $news = $this->get_extra_content_news_list();
    if($news)
      $ret[] = $news;

    $this->AddCustomExtraContent($ret);

    echo json_encode($ret);
    die(); //so that no templates are sent
  }


  function send_mail_ajax_handler()
  {
    foreach($_POST as $key => $val)
    {
      $_POST[$key] = stripslashes($val);
    }

    $lists = Array();
    foreach($_POST as $key => $val)
    {
        if(substr($key, 0, 31) == 'extracontent_extracontent-list-')
        {
            $lists[substr($key, 31)] = explode(';', $val);
        }
    }

    if(($_POST['sendtest'] || $_POST['sendmsg']) && $_POST['sendtoemail'])
    {

      ob_start();
      $this->send_email(
        $_POST['sendfromname'],
        $_POST['sendfromemail'],
        $_POST['sendtoname'],
        $_POST['sendtoemail'],
        $_POST['sendsubject'],
        $_POST['sendtext'],
        $_POST['sendattachments'],
        $_POST['sendtemplate'],
        $lists
      );
      $status = ob_get_contents();
      ob_end_clean();
      $this->logcollection->AddLogItem($_POST['sendtoemail'], $_POST['sendsubject'], $status);
      echo 'ok';
    }else
    {
      echo 'invalid parameters';
    }
    die(); //so that no templates are sent
  }

  /**
    Save message when custom email sending is executed.
  */
  function save_message_ajax_handler() {
    if(!$this->InitCollections()) {
      echo ' collection Initialization failed';
      die();
    }
    $message_id = $this->save_message($_POST, count($_POST['active_subscribers']));

    if(!empty($_POST['active_subscribers'])) {

      if(!is_array($_POST['active_subscribers'])){
          $_POST['active_subscribers'] = [$_POST['active_subscribers']];
      }

      foreach( $_POST['active_subscribers'] as $subscriber ) {
        $properties = array(
          'message_id'  => $message_id,
          'email'       => $subscriber,
        );
        $this->messagesemailscollection->AddItemAssoc($properties);
      }
    }
    echo $message_id;
    die();
  }


  /**
    Send email to group or all emails
  */
  function send_all_mail_ajax_handler()
  {
    if(!$this->InitCollections()) {
      echo ' collection Initialization failed';
      die();
    }
    $count = $this->emails_per_request;

    foreach($_POST as $key => $val)
    {
      $_POST[$key] = stripslashes($val);
    }

    $lists = Array();
    foreach($_POST as $key => $val)
    {
        if(substr($key, 0, 31) == 'extracontent_extracontent-list-')
        {
            $lists[substr($key, 31)] = explode(';', $val);
        }
    }

    $emails = $this->getSubscribersList($_POST['sendtoall'], $_POST['selectedlists'], $count, $_POST['offset']);
    $emails_count = $this->getSubscribersListCount($_POST['sendtoall'], $_POST['selectedlists']);

    if($_POST['offset'] == 0 ) {
      //save message
        $message_id = $this->save_message($_POST, $emails_count);
    } else {
      $message_id = is_numeric($_POST['message_id']) ? $_POST['message_id'] : '';
    }

    if(!empty($emails))
    {
      $i = 0;
      foreach ($emails as $email)
      {
        ob_start();
        $this->send_email(
          $_POST['sendfromname'],
          $_POST['sendfromemail'],
          $email,
          $email,
          $_POST['sendsubject'],
          $_POST['sendtext'],
          $_POST['sendattachments'],
          $_POST['sendtemplate'],
          $lists
        );
        sleep($this->getProperty('sendinterval'));
        $status = ob_get_contents();
        ob_end_clean();
        $message_info = $this->messagescollection->GetItemByIDAssoc($message_id);
        $this->messagescollection->ChangeItemByIDAssoc($message_id, array(
          'sent_emails_count' => ($message_info['sent_emails_count'] + 1),
        ));
        $this->logcollection->AddLogItem($email, $_POST['sendsubject'], $status, $message_id);
        $i++;
      }

      $response = array();
      $response['message_id'] = $message_id;
      if($i+ $_POST['offset'] >=  $emails_count)
      {
        $status = 'ok';
        $response['status'] = $status;
      }
      else
      {
        $offset =  $_POST['offset'] + $i;
        $response['offset'] = $offset;
      }
    }
    else
    {
      $status =  'ok';
      $response['status'] = $status;
    }

    echo json_encode($response);
    die(); //so that no templates are sent
  }

  function save_message($data, $emails_count) {
    $groups = array();
    if($data['sendtoall'] == 'true' ) {
      $groups[] = 0;
    } else {
      $groups= explode(';', $data['selectedlists']);
    }

    $attachments_data = explode(';', $data['sendattachments']);
    $attachments = array();
    foreach($attachments_data as $attachment) {
      if(trim($attachment) != "" ) {
        $attachments[] = $attachment;
      }
    }

    //add message
    $properties = array(
      'time'                => date('Y-m-d H:i:s'),
      'name'                => addslashes($data['sendfromname']),
      'email_from'          => $data['sendfromemail'],
      'subject'             => addslashes($data['sendsubject']),
      'text'                => addslashes($data['sendtext']),
      'attachment'          => addslashes(implode(';',$attachments)),
      'groups'              => addslashes(serialize($groups)),
      'total_emails_count'  => $emails_count,
      'status'              => 1,
    );
    $this->messagescollection->AddItemAssoc($properties);
    $message_id = $this->messagescollection->LastItemById();
    return $message_id;
  }

  function send_all_unsent_mail_ajax_handler()
  {
    if(!$this->InitCollections()) {
      echo ' collection Initialization failed';
      die();
    }
    $count = $this->emails_per_request;

    foreach($_POST as $key => $val)
    {
      $_POST[$key] = stripslashes($val);
    }

    $emails = $this->getUnsentSubscribersList($_POST['message_id']);
    $emails_count = $this->getUnsentSubscribersCount($_POST['message_id']);
    $message_info = $this->messagescollection->GetItemByIDAssoc($_POST['message_id']);
    if(!empty($emails))
    {

      $i = 0;
      foreach ($emails as $email)
      {
        ob_start();
        $this->send_email(
          $message_info['name'],
          $message_info['email_from'],
          $email,
          $email,
          $message_info['subject'],
          $message_info['text'],
          $message_info['attachment'],
          'default',
          array()
        );

        sleep($this->getProperty('sendinterval'));
        $status = ob_get_contents();
        ob_end_clean();

        $this->logcollection->AddLogItem($email, $message_info['subject'], $status, $_POST['message_id']);
        $i++;
      }

      $this->messagescollection->ChangeItemByIDAssoc($_POST['message_id'], array(
        'sent_emails_count' => $message_info['sent_emails_count'] + $i,
      ));

      $response = array();
      $response['message_id'] = $_POST['message_id'];
      if($emails_count <= 0 )
      {
        $status = 'ok';
        $response['status'] = $status;
      }
      else
      {
        $offset =  $_POST['offset'] + $i;
        $response['offset'] = $offset;
      }
    }
    else
    {
      $status =  'ok';
      $response['status'] = $status;
    }

    echo json_encode($response);
    die(); //so that no templates are sent
  }

  function get_all_mails_count_ajax_handler()
  {
    foreach($_POST as $key => $val)
    {
      $_POST[$key] = stripslashes($val);
    }
    $emails_count = $this->getSubscribersListCount($_POST['sendtoall'], $_POST['selectedlists']);
    echo $emails_count;
    die();
  }

  function get_all_unsent_mails_count_ajax_handler() {

    $emails_count = $this->getUnsentSubscribersCount($_POST['message_id']);
    echo $emails_count;
    die();
  }



  function get_message_by_id() {
    if(!$this->InitCollections()) {
      echo ' collection Initialization failed';
      die();
    }
    if(isset($_GET['message_id']) && is_numeric($_GET['message_id'])) {
      $message_id = $_GET['message_id'];
      $message_info = $this->messagescollection->getMessageById($message_id);
      echo json_encode($message_info);
    }
    die();
  }

  function get_message_content() {
    if(!$this->InitCollections()) {
      echo ' collection Initialization failed';
      die();
    }
    if(isset($_GET['message_id']) && is_numeric($_GET['message_id'])) {
      $message_id = $_GET['message_id'];
      $message_info = $this->messagescollection->getMessageById($message_id);
      ?>
      <div style="margin: 10px;" >
      <p>
        <b>From Name:</b><br />
        <?php echo $message_info['name']; ?>
      </p>
      <br />
      <p>
        <b>From E-mail:</b><br />
        <?php echo $message_info['from_email']; ?>
      </p>
      <br />
      <p>
        <b>Subject:</b><br />
        <?php echo $message_info['subject']; ?>
      </p>
      <br />
      <p>
        <b>Subject:</b><br />
        <?php echo $message_info['subject']; ?>
      </p>
      <br />
      <p>
        <b>Text:</b><br />
        <?php echo $message_info['text']; ?>
      </p>
      <br />
      <p>
        <b>Attachment:</b><br />
        <?php
          foreach($message_info['attachment'] as $attachment ) {
            ?>
            <a href="<?php echo $attachment; ?>" ><?php echo $attachment; ?></a><br />
            <?php
          }
        ?>
      </p>
      </div>

      <?php
    }
  }


  function GetCategoryAndEmailList_ajax_handler()
  {
    $cats = Array('categories' => Array(Array('id' => -1, 'title' => '<i>Subscribed to all categories</i>')), 'emails' => Array());

    if($_POST['getcategorylist'])
    {
      if($this->InitCollections())
      {
        $catsdata = $this->catcollection->GetDBData(
          Array(
            'what' => 'item_id, mailcat_title',
            'assoc' => true
          )
        );
        if($catsdata)
        {
          foreach($catsdata as $cat)
          {
            $cats['categories'][] = Array('id' => $cat['item_id'], 'title' => $cat['mailcat_title']);
          }
        }
        $emdata = $this->emailcollection->GetDBData(
          Array(
            'what' => 'item_id, maillist_email, maillist_name',
            'order' => 'ind DESC',
            'assoc' => true
          )
        );
        if($emdata)
        {
          foreach($emdata as $em)
          {
            $cats['emails'][] = Array($em['item_id'], $em['maillist_email'], $em['maillist_name'], 0);
          }
        }
      }
    }
    echo json_encode($cats);
    die(); //no template
  }


  //return array of subscriber IDs
  function GetSubscribersForCategory_ajax_handler()
  {
    $ret = Array();
    if($_POST['category'])
    {
      if($this->InitCollections())
      {
        $data = $this->emailcatrelationcollection->GetSubscribersForCategory2(intval($_POST['category']), $this->emailcollection);
        foreach($data as $row)
        {
          $ret[] = array($row['item_id'], $row['maillist_email'], $row['maillist_name'], 1);
        }
      }
    }

    echo json_encode($ret);
    die(); //no template
  }


  function update_subscriberlist_ajax_handler()
  {
    $list_id = intval($_POST['list_id']);
    if($_POST['update_changes'] && $list_id)
    {
      if($this->InitCollections())
      {
        $data = json_decode(stripslashes($_POST['update_changes']), true);
        foreach($data as $row)
        {
          $id = $row['id'];
          $on = $row['on'];
          if($on)
          {
            $this->emailcatrelationcollection->AddRelation($list_id, intval($id));
          }else
          {
            $this->emailcatrelationcollection->DeleteRelation($list_id, intval($id));
          }
        }
      }
    }
    echo 'ok';
    die(); //no template
  }

  function import_list_ajax_handler()
  {
		set_time_limit(0);
		ini_get('memory_limit','200M');
    if($_POST['importemaillist'] && $_POST['emails'])
    {
      $added = 0;
      if($this->InitCollections())
      {
        if($_POST['selectedlists'])
          $cats = explode(';', $_POST['selectedlists']);
        else
          $cats = Array();
        $emails = explode("\n", $_POST['emails']);
        $unattached_emails = array();
        $i = 1;
        foreach($emails as $contact)
        {
          if( $i > 200 )
          {
            $unattached_emails[] = stripslashes($contact);
          }
          else
          {
            $contact = trim($contact);
            list($email, $name) = explode(';', $contact);
            $email = trim($email);
            $name = trim($name);
            if($email)
            {
              if($this->AddEmail($name, $email, $cats))
              {
                $added++;
              }
            }
          }
          $i++;
        }
      }
    }
    if(!empty($unattached_emails))
    {
      echo implode("\n", $unattached_emails);
    }
    else
    {
      echo $added;
    }
    die(); //no template
  }


  function add_email_ajax_handler()
  {
    if($_POST['email'])
    {
      if($this->InitCollections())
      {
        if($_POST['lists'])
          $cats = explode(';', $_POST['lists']);
        else
          $cats = Array();
        if($this->AddEmail($_POST['name'], $_POST['email'], $cats))
        {
          echo 'ok';
        }
      }
    }
    die(); //no template
  }


  function edit_email_ajax_handler()
  {
    $id = intval($_POST['id']);
    if($_POST['email'] && $id)
    {
      if($this->InitCollections())
      {
        $this->emailcollection->ChangeItemByIdAssoc($id, Array(
          'maillist_name' => $_POST['name'],
          'maillist_email' => $_POST['email']
        ));

        echo 'ok';
      }
    }
    die(); //no template
  }


  function delete_list_emails_ajax_handler()
  {
    $cat_id = intval($_POST['cat_id']);
    if (!$cat_id || !$this->InitCollections()) die;

    $this->emailcatrelationcollection->DeleteCategoryRelations($cat_id);

    echo 'ok';
    die;
  }

  function delete_email_ajax_handler()
  {
    $cat_id = intval($_POST['cat_id']);
    $id = intval($_POST['id']);
    if($cat_id && $id)
    {
      if($this->InitCollections())
      {
        $this->emailcatrelationcollection->DeleteRelation($cat_id, $id);

//        $this->emailcatrelationcollection->DeleteEmailRelations($id);
//        $this->emailcollection->DelDBItem($id);

        echo 'ok';
      }
    }
    die(); //no template
  }


  function add_list_ajax_handler()
  {
    if($_POST['title'])
    {
      if($this->InitCollections())
      {
        $ind = $this->catcollection->AddItemAssoc(
            Array(
                'mailcat_title' => $_POST['title']
            )
        );

        echo 'ok';
      }
    }
    die(); //no template
  }


  function edit_list_ajax_handler()
  {
    $id = intval($_POST['id']);
    if($_POST['title'] && $id > 0)
    {
      if($this->InitCollections())
      {
        $this->catcollection->ChangeItemByIdAssoc($id, Array(
          'mailcat_title' => $_POST['title']
        ));

        echo 'ok';
      }
    }
    die(); //no template
  }


  function delete_list_ajax_handler()
  {
    $id = intval($_POST['id']);
    if($id > 0)
    {
      if($this->InitCollections())
      {
        $this->emailcatrelationcollection->DeleteCategoryRelations($id);
        $this->catcollection->DelDBItem($id);

        echo 'ok';
      }
    }
    die(); //no template
  }


  function save_template_settings_ajax_handler()
  {
    $data = $_POST;

    $template_name = $_POST['templatename'];
    $fields = $this->get_fields_template($template_name);
    $tfields = Array();
    foreach($fields as $fname => $field)
    {
      if(isset($data[$fname]))
      {
        $tfields[$fname] = stripslashes($data[$fname]);
      }
    }

    $templatesettings = unserialize(base64_decode($this->getProperty('templatesettings')));
    $templatesettings[$template_name] = $tfields;
    $this->setProperty('templatesettings', base64_encode(serialize($templatesettings)));

    echo json_encode(Array('success' => true, 'post' => $_POST));
    die(); //no template
  }


  function save_settings_ajax_handler()
  {
    $this->setProperty('sendinterval', $_POST['sendinterval']);
    $this->setProperty('confirmationfrom', $_POST['confirmationfromname']);
    $this->setProperty('confirmationfromemail', $_POST['confirmationfromemail']);
    $this->setProperty('confirmationsubject', $_POST['confirmationsubject']);
    $this->setProperty('confirmationtext', $_POST['confirmationtext']);

    echo json_encode(Array('success' => true, 'post' => $_POST));
    die(); //no template
  }


  function read_sent_ajax_handler()
  {
    if($this->InitCollections())
    {
      $data = $this->messagescollection->GetDBData(
        Array(
          'offset' => intval($_GET['start']),
          'count'  => intval($_GET['limit']),
          'order'  => $_GET['sort'] . ' ' . $_GET['dir'],
          'assoc'  => true
        )
      );
      $ret = Array(
        'totalCount' => $this->messagescollection->ItemCount(),
        'items' => $data
      );

      echo $_GET['callback'] . '(' . json_encode($ret) . ')';
      die(); //no template
    }
  }

  function read_log_ajax_handler()
  {
    if($this->InitCollections())
    {
      $data = $this->logcollection->GetDBData(
        Array(
          'offset' => intval($_GET['start']),
          'count'  => intval($_GET['limit']),
          'order'  => $_GET['sort'] . ' ' . $_GET['dir'],
          'assoc'  => true
        )
      );
      $ret = Array(
        'totalCount' => $this->logcollection->ItemCount(),
        'items' => $data
      );

      echo $_GET['callback'] . '(' . json_encode($ret) . ')';
      die(); //no template
    }
  }


  //=======================================

    function get_extra_content_news_list()
  {
    $newscol = $this->InitPropertyCollection('news');
    if($newscol)
    {
      $retitem = Array(
        'title' => 'Site news',
        'name'  => 'news',
        'items' => Array()
      );

      $data = $newscol->GetDBData();
      if($data)
      {
        foreach($data as $row)
        {
          $retitem['items'][] = Array(
            'id'    => $row['item_id'],
            'title' => $row['col0']
          );
        }
      }

      return $retitem;

    }else
    {
      return false;
    }
  }


  //VIRTUAL method - override to add more extra content lists
  // see get_extra_content_news_list() for formatting example
  function AddCustomExtraContent(&$ret)
  {
    //$ret[] = Array('title' => ..., 'name' => ..., 'items' => ...);
  }


  function strip_selected_tags($text, $tags = array())
  {
       $args = func_get_args();
       $text = array_shift($args);
       $tags = func_num_args() > 2 ? array_diff($args,array($text))  : (array)$tags;
       foreach ($tags as $tag){
           if( preg_match_all( '/<'.$tag.'[^>]*>([^<]*)<\/'.$tag.'>/iu', $text, $found) ){
               $text = str_replace($found[0],$found[1],$text);
           }
       }

       return preg_replace( '/(<('.join('|',$tags).')(\\n|\\r|.)*?(\/)?>)/iu', '', $text);
  }


  //VIRTUAL method - override this method to return data for inclusion in e-mail
  function GetExtraContentListData(&$contentdata, $listname, $item_ids)
  {
    //$contentdata[] = Array('title' => ..., 'text' => ..., 'link' => ...);
  }


  function GetExtraContentNews(&$ret, $ids)
  {
    $newscol = $this->InitPropertyCollection('news');
    if($newscol)
    {
      $data = Array();
      foreach($ids as $id)
      {
        $row = $newscol->GetItemByIdAssoc($id);
        if($row)
          $data[] = $row;
      }

      if($data)
      {
        foreach($data as $row)
        {
          $ret[] = Array(
            'title' => $row['col0'],
            'text' => $row['col1'],
            'date' => date("d.m.Y. H:i:s",$row['col3']),
            'link' => $this->getProperty('newspath') . '?news_id=' . $row['item_id']
          );
        }
      }
    }
  }


  function GetExtraContentSample(&$ret, $ids)
  {
    foreach($ids as $id)
    {
          $ret[] = Array(
            'title' => 'Sample title',
            'text' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nam vel nisl eu erat pretium vehicula. Aliquam vel sapien eget orci feugiat adipiscing. Nullam in mi. Mauris nec enim vel felis vehicula iaculis. <b>Vestibulum vestibulum</b> felis et tortor. Nam feugiat pharetra augue.',
            'date' => date("d.m.Y. H:i:s"),
            'link' => '#'
          );
    }
  }


  function GetListContents($lists)
  {
    $ret = Array();
    foreach($lists as $key => $val)
    {
      if($val)
      {
        if($key == 'news')
        {
          $this->GetExtraContentNews($ret, $val);
        }else
        if($key == 'sample')
        {
          $this->GetExtraContentSample($ret, $val);
        }else
        {
          $this->GetExtraContentListData($ret, $key, $val);
        }
      }
    }
    return $ret;
  }


  function strip_non_email_tags($text)
  {
    return $this->strip_selected_tags($text, Array('form', 'input', 'select', 'option', 'textarea', 'script', 'object', 'embed'));
  }


  function get_fields_template_default(&$fieldsettings)
  {
    $res = Array(
        'signature' => Array('value' => '', 'type' => 'html', 'label' => 'Message footer'),
    );
    return $res;
  }


  function apply_template_default($configuration, $mailtext, $listdata, $fields)
  {

    $html = $this->render('/Mailing/notifications', [
        'unsubscribeurl' => $configuration['unsubscribeurl'],
        'content' => $mailtext,
        'lang' => isset($configuration['lang']) ? $configuration['lang'] : WebApp::$app->getLanguage(),
        'view_link' => isset($configuration['view_url']) ? $configuration['view_url'] : false,
    ]);

    return $html;
  }


  function get_fields_template_newsletter(&$fieldsettings)
  {
    $res = Array(
        'bgcolor' => Array('value' => '#99CC00', 'type' => 'color', 'label' => 'Background color'),
        'footercolor' => Array('value' => '#FFFFCC', 'type' => 'color', 'label' => 'Footer background color'),
        'logo' => Array('value' => '', 'type' => 'image', 'label' => 'Logo image (550px wide)'),
        'description' => Array('value' => 'List description<br />', 'type' => 'html', 'label' => 'List description'),
        'footer' => Array('value' => 'Our mailing address is:<br />
*|LIST:ADDRESS|*<br />
<br />
Our telephone:<br />
*|LIST:PHONE|*<br />
<br />
Copyright (C) 2007 *|LIST:COMPANY|* All rights reserved.', 'type' => 'html', 'label' => 'Message footer'),
    );
    return $res;
  }


  function apply_template_newsletter($configuration, $mailtext, $listdata, $fields)
  {

    $s = '<html>
<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0" bgcolor=\''.$fields['bgcolor'].'\' >


<STYLE>
 .headerTop { background-color:#FFCC66; border-top:0px solid #000000; border-bottom:1px solid #FFFFFF; text-align:center; }
 .adminText { font-size:10px; color:#996600; line-height:200%; font-family:verdana; text-decoration:none; }
 .headerBar { background-color:#FFFFFF; border-top:0px solid #333333; border-bottom:10px solid #FFFFFF; }
 .title { font-size:20px; font-weight:bold; color:#CC6600; font-family:arial; line-height:110%; }
 .subTitle { font-size:11px; font-weight:normal; color:#666666; font-style:italic; font-family:arial; }
 .defaultText { font-size:12px; color:#000000; line-height:150%; font-family:trebuchet ms; }
 .footerRow { background-color:'.$fields['footercolor'].'; border-top:10px solid #FFFFFF; }
 .footerText { font-size:10px; color:#996600; line-height:100%; font-family:verdana; }
 a { color:#FF6600; color:#FF6600; color:#FF6600; }
</STYLE>



<table width="100%" cellpadding="10" cellspacing="0" class="backgroundTable" bgcolor=\''.$fields['bgcolor'].'\' >
<tr>
<td valign="top" align="center">


<table width="550" cellpadding="0" cellspacing="0">

';
/*<tr>

<td style="background-color:#FFCC66;border-top:0px solid #000000;border-bottom:1px solid #FFFFFF;text-align:center;" align="center"><span style="font-size:10px;color:#996600;line-height:200%;font-family:verdana;text-decoration:none;">Email not displaying correctly? <a href="*|ARCHIVE|*" style="font-size:10px;color:#996600;line-height:200%;font-family:verdana;text-decoration:none;">View it in your browser.</a></span></td>
</tr> */

$s .= '

<tr>
<td style="background-color:#FFFFFF;border-top:0px solid #333333;border-bottom:10px solid #FFFFFF;">';
if($fields['logo'])
  $s .= '<center><img src="'.$fields['logo'].'" border="0" title="" alt="" align="center"></center>';
$s .= '</td>
</tr>

</table>


<table width="550" cellpadding="20" cellspacing="0" bgcolor="#FFFFFF">
<tr>
<td bgcolor="#FFFFFF" valign="top" style="font-size:12px;color:#000000;line-height:150%;font-family:trebuchet ms;">

<p>
'.$mailtext.'
</p>

';

foreach($listdata as $item)
{
  $s .= '
<p>
<span style="font-size:20px;font-weight:bold;color:#CC6600;font-family:arial;line-height:110%;">'.$item['title'].'</span><br>
';
  if($item['date'])
    $s .= '
<span style="font-size:11px;font-weight:normal;color:#666666;font-style:italic;font-family:arial;">'.$item['date'].'</span><br>
';
  $s .= '
'.$item['text'].' <a href="'.$item['link'].'">'.$this->LS('readmore').'.</a>
</p>';
}

$s .= '

</td>
</tr>

<tr>
<td style="background-color:'.$fields['footercolor'].';border-top:10px solid #FFFFFF;" valign="top">
<span style="font-size:10px;color:#996600;line-height:100%;font-family:verdana;">
'.$fields['description'].'
<br />

<a href="'.$configuration['unsubscribeurl'].'">'.$this->LS('click_to_unsubscribe').'</a><br />

<br />
'.$fields['footer'].'


</span>
</td>
</tr>

</table>











</td>
</tr>
</table>






</body>
</html>

';
    return $s;
  }


  function get_fields_template($template_name)
  {
    if(!$template_name || strpos($template_name, '-') !== false)
      $template_name = 'default';

    $templatesettings = unserialize(base64_decode($this->getProperty('templatesettings')));
    $fieldsettings = $templatesettings[$template_name];
    switch($template_name)
    {
      case 'default':
          $res = $this->get_fields_template_default($fieldsettings);
          break;
      case 'newsletter':
          $res = $this->get_fields_template_newsletter($fieldsettings);
          break;
      default:
          $res = $this->get_fields_template_default($fieldsettings);
    }

    if(is_array($fieldsettings)){
        foreach($fieldsettings as $field => $val)
        {
          if(isset($res[$field]))
          {
            $res[$field]['value'] = $val;
          }
        }
    }

    return $res;
  }


  function apply_template($template_name, $configuration, $mailtext, $lists)
  {
    //content pre-processing - things that should be done to embedded
    //content only and never to the static template parts
    //===============
    //get list contents
    $listdata = $this->GetListContents($lists);
    //process list contents for display
    foreach($listdata as $key => $item)
    {
      //remove outer <p></p> tags from text for consistency
      $text = preg_replace('/^<p>(.*?)<\/p>$/si', '\1', $item['text']);
      $text = $this->strip_non_email_tags($text);
      $listdata[$key]['text'] = $text;

      $title = trim($item['title']);
      $title = $this->strip_non_email_tags($title);
      $listdata[$key]['title'] = $title;
    }

    $mailtext = preg_replace('/^<p>(.*?)<\/p>$/si', '\1', $mailtext);
    $mailtext = $this->strip_non_email_tags($mailtext);
    //===============

    $fields = $this->get_fields_template($template_name);
    $fvals = Array();
    foreach($fields as $fieldname => $field)
    {
      $fvals[$fieldname] = $field['value'];
    }

    switch($template_name)
    {
      case 'default':
          return $this->apply_template_default($configuration, $mailtext, $listdata, $fvals);
          break;
      case 'newsletter':
          return $this->apply_template_newsletter($configuration, $mailtext, $listdata, $fvals);
          break;
      default:
          return $this->apply_template_default($configuration, $mailtext, $listdata, $fvals);
    }
  }


  function list_available_templates()
  {
    return Array('default', 'newsletter');
  }


  function post_process_html_email($configuration, $mailtext, $mail = null)
  {
    $domain = $configuration['domain'];
    $dirroot = $configuration['dirroot'];

     //replace all relative urls with full urls
     preg_match_all('/<a\s+.*?href="([^"]+)"[^>]*>[^<]+<\/a>/is', $mailtext, $matches);
     for ($i=0; $i< count($matches[0]); $i++) {
        if (!strstr($matches[1][$i],":"))
          {
            $mailtext = str_replace('"' . $matches[1][$i] . '"', '"http://' . $domain . $matches[1][$i] . '"', $mailtext);
          }

     }

     //embed images
     preg_match_all('/<img\s+.*?src="('.preg_quote('http://'.$domain, '/').')?\/([^"]+)"[^>]*>/is', $mailtext, $matches);

     for ($i=0; $i< count($matches[0]); $i++) {
        if (!strstr($matches[2][$i],":"))
          {
            //embed the image
            $imgpath = explode('/', $matches[2][$i]);
            $img = $imgpath[count($imgpath) - 1];
            $imgparts = explode('.', $img);
            $ext = $imgparts[count($imgparts) - 1];

            if($mail)
              $mail->addEmbeddedImage(new fileEmbeddedImage($dirroot . $matches[2][$i], 'image/' . $ext, new Base64Encoding()));
            $mailtext = str_replace('"'.$matches[1][$i].'/' . $matches[2][$i] . '"', '"' . $img . '"', $mailtext);
          }
     }

     return $mailtext;
  }


  function send_email($sendername, $senderemail, $toname, $toemail, $mailsubj, $mailtext, $attachments, $template, $lists)
  {
    require_once($GLOBALS['cfgDirRoot']."library/"."class.mimemail.php");

    //get site configuration
    list($domain, $dirroot) = sqlQueryRow("select domain, dirroot from sites where site_id=".$this->site_id);

    $unsubhash = $toemail;
    if($this->InitCollections())
    {
      $subscriber = $this->emailcollection->GetItemByEmail($toemail);
      if($subscriber && $subscriber['maillist_hash'])
      {
        $unsubhash = $subscriber['maillist_hash'];
      }
    }


    $unsubpath = 'http://' . $domain . '/' . $unsubpath . '?maillist_unsubscribe=' . $unsubhash;

    $unsubpath = Url::get('?maillist_unsubscribe=' . $unsubhash.'#subscribe-wrap', [], true);

    $hash = sha1(time().'|'.rand(1,999999));
    $view_url = Url::get('/emails', ['h' => $hash], true);

    $configuration = Array(
      'domain' => $domain,
      'dirroot' => $dirroot,
      'toemail' => $toemail,
      'toname' => $toname,
      'fromemail' => $senderemail,
      'fromname' => $sendername,
      'unsubscribeurl' => $unsubpath,
      'view_url' => $view_url,
      'lang' => $subscriber['lang']
    );

    $mail = new htmlMimeMail5();
    $mail->setTextCharset('utf-8');
    $mail->setHTMLCharset('utf-8');
    $mail->setHeadCharset('utf-8');
    $mail->setFrom($sendername.' <'.$senderemail.'>');
    $mail->setSubject($mailsubj);

    if(!$toname)
      $rcpt = $toemail;
    else
      $rcpt = $toname . ' <' . $toemail . '>';

    //get html version
    $htmlmail = $this->apply_template($template, $configuration, $mailtext, $lists);
    $htmlmail = $this->post_process_html_email($configuration, $htmlmail, $mail);

    //outlook express has problems with QPrintable, so use something else
    $mail->setHTMLEncoding(new Base64Encoding());
    $mail->setHTML($htmlmail);

    //get plain text version
    $mailtext = $this->apply_template('default', $configuration, $mailtext, $lists);
    $mailtext = $this->post_process_html_email($configuration, $mailtext);

    //newlines
    $mailtext = str_replace("<p>","<p>\n",$mailtext);
    $mailtext = str_replace("<P>","<P>\n",$mailtext);
    $mailtext = str_replace("<br>","\n",$mailtext);
    $mailtext = str_replace("<br />","\n",$mailtext);
    $mailtext = str_replace("<BR>","\n",$mailtext);
    $mailtext = str_replace("<BR />","\n",$mailtext);
    //spaces
    $mailtext = str_replace("&nbsp;"," ",$mailtext);
    //urls
    $mailtext = strip_tags($mailtext,'<a>');
    preg_match_all('/<a\s+.*?href="([^"]+)"[^>]*>[^<]+<\/a>/is', $mailtext, $matches);
    for ($i=0; $i< count($matches[0]); $i++)
    {
      if (strstr($matches[1][$i],"mailto:"))
      {
        $mailtext = str_replace($matches[0][$i], substr($matches[1][$i],7), $mailtext);
      }
      if (preg_match('/^(\/[a-zA-Z_-][^ ]+)/',$matches[1][$i]))
      {
        $mailtext = str_replace($matches[0][$i], "http://".$domain.$matches[1][$i], $mailtext);
      }
    }
    //convert links to format Anchor Text [URL]
    $mailtext = preg_replace('/<a\s+.*?href="([^"]+)"[^>]*>([^<]+)<\/a>/is', '\2 [\1]', $mailtext);


    $mail->setText(strip_tags($mailtext));

    //attach files
    if($attachments)
    {
      $attachments = explode(';', $attachments);
      foreach($attachments as $attachment)
      {
        if($attachment)
        {
          //remove leading slash
          $attachment = preg_replace('/^\//s', '', $attachment);
          $attachment = $dirroot . $attachment;
          $mail->addAttachment(new fileAttachment($attachment));
        }
      }
    }

    $this->notifications->Insert([
        'date' => date('Y-m-d H:i:s'),
        'email' => $toemail,
        'hash' => $hash,
        'subject' => $mailsubj,
        'content' => $htmlmail
    ]);


    //echo $mailtext;
    //send
    //print_r($mail->getRFC822(Array($rcpt)));

    //saglabaa faila
//    $fp = fopen($dirroot . 'e.eml', 'w+');
//    fputs($fp, $mail->getRFC822(Array($rcpt)));
//    fclose($fp);

    $mail->send(Array($rcpt));
  }


  function AddEmail($name, $email, $cats, $status = 1, $lang = '')
  {
    $ind = $this->emailcollection->AddEmail($name, $email, $status, $lang);
    if($ind)
    {
      $email_id = $this->emailcollection->GetItemId($ind);
      $this->emailcollection->changeItemByIdAssoc($email_id, array(
        'maillist_status' => $status
      ));

      if(is_array($cats)){
        foreach($cats as $cat_id){
          $this->emailcatrelationcollection->AddRelation($cat_id, $email_id);
        }
      }elseif(is_numeric($cats)){
      	$this->emailcatrelationcollection->AddRelation($cats, $email_id);
      }

      return $email_id;
    }else
    {
      return false;
    }
  }


  function serviceAddEmail($name, $email, $cats, $status = 1)
  {
    if (!$this->initCollections()) return false;
    return $this->AddEmail($name, $email, $cats, $status);
  }

  function serviceIsEmailSubscribed($email)
  {
    if (!$this->initCollections()) return false;
    return $this->emailcollection->isEmailSubscribed($email);
  }

  //returns all categories, but the ones that are subscribed by subscriber_id
  //will be marked with [subscribed] = true
  function getSubscriberCategories($subscriber_id)
  {
    if($this->InitCollections())
    {
      $subscribed_cats = $this->catcollection->GetCategoriesForEmails(Array($subscriber_id), $this->emailcatrelationcollection);
      //list IDs
      $subscribed_cat_ids = Array();
      foreach($subscribed_cats as $cat)
      {
        $subscribed_cat_ids[] = $cat['item_id'];
      }
      if($this->emailcatrelationcollection->HasRelation(-1, $subscriber_id))
        $subscribed_cat_ids[] = -1;

      $all_cats = $this->catcollection->GetDBData();
      $all_cats = array_merge(Array(Array('item_id' => -1, 'mailcat_title' => $this->LS('subscribetoall'))), $all_cats);
      foreach($all_cats as $key => $cat)
      {
        if(in_array($cat['item_id'], $subscribed_cat_ids))
        {
          $all_cats[$key]['subscribed'] = true;
        }else
        {
          $all_cats[$key]['subscribed'] = false;
        }
      }
      return $all_cats;
    }
  }


  function updateSubscriptionStatus($subscriber, $subscription_mode, $categories)
  {
    if($this->InitCollections())
    {
      //apply only for confirmed e-mail addresses
      if($subscriber['maillist_status'] !== 0)
      {
        if($subscription_mode == 'subscribe')
          $subscriber['maillist_status'] = 1;
        if($subscription_mode == 'unsubscribe')
          $subscriber['maillist_status'] = 2;
      }
      //update
      $subscriber = $this->emailcollection->AddItemSlashes($subscriber);
      $this->emailcollection->ChangeItemByIdAssoc($subscriber['item_id'], $subscriber);

      //update group subscriptions
      $this->emailcatrelationcollection->DeleteEmailRelations($subscriber['item_id']);
      foreach($categories as $cat_id)
      {
        $this->emailcatrelationcollection->AddRelation($cat_id, $subscriber['item_id']);
      }

      return $this->emailcollection->GetItemByIdAssoc($subscriber['item_id']);
    }
  }


  function confirmSubscriber($subscriber)
  {
    if($this->InitCollections())
    {
      if(!$subscriber)
        return ''.$this->LS('emailcouldnotbeconfirmed').'';

      if($subscriber['maillist_status'] != 0)
        return ''.$this->LS('emailisalreadyconfirmed').'';

      $subscriber['maillist_status'] = 1;
      $subscriber = $this->emailcollection->AddItemSlashes($subscriber);

      $this->emailcollection->ChangeItemByIdAssoc($subscriber['item_id'], array('maillist_status' => 1));

      return true;
    }
  }


  function validateAndSubscribe(){
    //does email exist?
    if(!$_POST['maillist_email']){
      return ''.$this->LS('pleaseenteremail').'';
    }

    //is email valid?
    if (!preg_match("/^([a-zA-Z0-9_\.\-]){2,}\@(([a-zA-Z0-9\-]){2,}\.)+([a-zA-Z]{2,4})$/",$_POST['maillist_email']))
		{
		  return ''.$this->LS('pleaseentervalidemail').'';
		}

    //is email new?
    if($this->emailcollection->HasEmail($_POST['maillist_email'])){
      return ''.$this->LS('emailisalreadysubscribed').'';
    }

    //has user seen category checkboxes (assuming that categories exist)?
    if($this->catcollection->ItemCount() && !isset($_POST['maillist_hascats']))
    {
      #return false;
    }


    $cat = $this->catcollection->getAddByName(strtoupper($this->lang));

    //all ok
    $sub = $this->AddEmail($_POST['maillist_name'], $_POST['maillist_email'], $cat['item_id'], 0, $this->lang);
    if($sub)
    {
      $subscriber = $this->emailcollection->getItemByIdAssoc($sub);
      $subhash = $subscriber['maillist_hash'];
      $rcpt = $subscriber['maillist_email'];

      //send confirmation e-mail
      require_once($GLOBALS['cfgDirRoot']."library/"."class.mimemail.php");

      //get site configuration
      list($domain, $dirroot) = sqlQueryRow("select domain, dirroot from sites where site_id=".$this->site_id);

      #$confirmpath = PagePathById($this->page_id, $this->site_id);
      #$confirmpath = 'http://' . $domain . '/' . $confirmpath . '?maillist_confirm=' . $subhash;

      $confirmpath = Url::get('/', ['maillist_confirm' => $subhash], true);

      $subject = option("Mailinglist\\subscribe_subject".$this->lang, null, "Aktivizācijas vēstules tēma(".$this->lang.")", array('value_on_create' => "Pierakstīšanās jaunumiem"));
      $sender_email = option("Mailinglist\\sender_email", null, "Sūtītāja e-pasts", array("value_on_create" => "ktaube@datateks.lv"));
      $sender = option("Mailinglist\\sender", null, "Sūtītājs", array("value_on_create" => "Registration"));

      $mailtext = option("Mailinglist\\activation_mail_text_".$this->lang,
        null,
        "Apstiprinājuma vēstule(".$this->lang.")",
        array(
        "type" => "wysiwyg",
        "value_on_create" => "Labdien,<br />lai apstiprinātu jaunumu saņemšanu, spiežiet uz saites [link]<br /><br />Ar cieņu,<br />Administrācija",
        )
      );

      $mailtext = str_replace("[link]", "<a href='".$confirmpath."' target='_blank'>".$confirmpath."</a>", $mailtext);

      phpMailerSend($mailtext, $subject, $sender, $sender_email, $subscriber['maillist_email'], true, false);

      return true; // ==>
    }

    return 'Unspecified error while subscribing email.';
  }


  function AutoCreateAndAssignCollection($property, $colname, $type = '')
  {
    if($property)
    {
      $colid = $this->getProperty($property);
      $type = $this->properties[$property]['collectiontype'];
    }else
    {
      $colid = 0;
    }

    include_once($GLOBALS['cfgDirRoot'] . "collections/class.".$type.".php");
    $collection = new $type($colname,$colid);

    if(!$collection->isValidCollection())
    {
      $colid = $collection->FindCollection($colname);
      if(!$collection->isValidCollection())
      {
        $colid = $collection->AddNewCollection($colname);
      }
      if($property)
        $this->setProperty($property, $colid);
    }

    return $collection;

  }


  function AutoCreateCollections()
  {
    $colident = $_GET['colname'];

    //email-category relation
    $colname = 'maillist_' . $colident . '_relation';
    $this->emailcatrelationcollection = $this->AutoCreateAndAssignCollection('emailcatrelation', $colname);

    //emails
    $colname = 'maillist_' . $colident . '_subscribers';
    $this->emailcollection = $this->AutoCreateAndAssignCollection('', $colname, 'mailinglistcollectionex');

    //categories
    $colname = 'maillist_' . $colident . '_categories';
    $this->catcollection = $this->AutoCreateAndAssignCollection('', $colname, 'mailingcategoriescollectionex');

    //log
    $colname = 'maillist_' . $colident . '_log';
    $this->logcollection = $this->AutoCreateAndAssignCollection('mailinglog', $colname);

    //create relations
    $this->emailcatrelationcollection->setProperty('relation_' . $this->emailcatrelationcollection->name . '_mailcatemail_email', $this->emailcollection->collection_id);
    $this->emailcatrelationcollection->setProperty('relation_' . $this->emailcatrelationcollection->name . '_mailcatemail_cat', $this->catcollection->collection_id);
    $this->emailcollection->setProperty('relation_' . $this->emailcollection->name . '_categories', $this->emailcatrelationcollection->collection_id);
    $this->catcollection->setProperty('relation_' . $this->catcollection->name . '_subscribers', $this->emailcatrelationcollection->collection_id);
  }


  function InitCollections()
  {
    if(!$this->emailcatrelationcollection)
      $this->emailcatrelationcollection = $this->InitPropertyCollection('emailcatrelation');
    if(!$this->emailcatrelationcollection)
      return false;

   if(!$this->emailcollection)
      $this->emailcollection = $this->emailcatrelationcollection->InitializeRelatedCollection('mailcatemail_email', 'mailinglistcollectionex');
    if(!$this->emailcollection)
      return false;

    if(!$this->catcollection)
      $this->catcollection = $this->emailcatrelationcollection->InitializeRelatedCollection('mailcatemail_cat', 'mailingcategoriescollectionex');
    if(!$this->catcollection)
      return false;

    if(!$this->logcollection)
      $this->logcollection = $this->InitPropertyCollection('mailinglog');
    if(!$this->logcollection)
      return false;

    if(!$this->messagescollection)
      $this->messagescollection = $this->InitPropertyCollection('mailingmessages');
    if(!$this->messagescollection)
      return false;

    if(!$this->messagesemailscollection)
      $this->messagesemailscollection = $this->InitPropertyCollection('mailingmessagesemails');
    if(!$this->messagesemailscollection)
      return false;

    $this->use_mailinglist = option("Mailinglist\\use_mailinglist", null, "Lietot mailinglist komponenti", Array("is_advanced" => true, "type" => 'check'), null, $this->site_id);

    return true;

	}


  function toolbar(&$script, &$toolbars, &$btnids, &$onclick)
  {
    return true;
  }



  function SetProperties()
  {
    //Empty
    $this->properties = Array(

    "name"        => Array(
      "label"     => "Name:",
      "type"      => "string"
      ),

     "sendinterval" => Array(
       "label"     => "Mass sending interval:",
       "type"      => "string",
       "value"     => "3"
      ),

     "confirmationfrom" => Array(
       "label"     => "Confirmation e-mail from (name):",
       "type"      => "string",
       "value"     => "INFO"
      ),

     "confirmationfromemail" => Array(
       "label"     => "Confirmation e-mail from (email):",
       "type"      => "string",
       "value"     => "info@noreply.lv"
      ),

     "confirmationsubject" => Array(
       "label"     => "Confirmation e-mail subject:",
       "type"      => "string",
       "value"     => "Subscription confirmation"
      ),

     "confirmationtext" => Array(
        "label"     => "Confirmation e-mail text:",
        "type"      => "customtext",
        "rows"      => "6",
        "cols"      => "30",
        "value"     => "To confirm your subscription to mailing list, please click the link below:\n"
      ),

     "emailcatrelation" => Array(
          "label"       => "Stored email and category list:",
          "type"        => "collection",
          "collectiontype" => "mailingcategoriesemailscollectionex",
          "lookup"      => GetCollections($this->site_id, "mailingcategoriesemailscollectionex"),
          "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
          "dialogw"     => "600"
     ),

     "mailinglog" => Array(
          "label"       => "Mailing list log:",
          "type"        => "collection",
          "collectiontype" => "mailinglogcollectionex",
          "lookup"      => GetCollections($this->site_id, "mailinglogcollectionex"),
          "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
          "dialogw"     => "600"
     ),
     "mailingmessages" => Array(
          "label"       => "Mailing list messages:",
          "type"        => "collection",
          "collectiontype" => "mailingmessagescollectionex",
          "lookup"      => GetCollections($this->site_id, "mailingmessagescollectionex"),
          "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
          "dialogw"     => "600"
     ),

     "mailingmessagesemails" => Array(
          "label"       => "Mailing list message emails:",
          "type"        => "collection",
          "collectiontype" => "mailingmessagesemailscollectionex",
          "lookup"      => GetCollections($this->site_id, "mailingmessagesemailscollectionex"),
          "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
          "dialogw"     => "600"
     ),

      "news" => Array(
          "label"       => "News collection:",
          "type"        => "collection",
          "collectiontype" => "newscollectionex",
          "lookup"      => GetCollections($this->site_id, "newscollectionex"),
          "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
          "dialogw"     => "600"
        ),

        "notifications" => [
            "label"       => "Notification collection:",
            "type"        => "collection",
            "collectiontype" => "EMailNotificationCollection",
            "lookup"      => GetCollections($this->site_id, "EMailNotificationCollection"),
            "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
            "dialogw"     => "600"
        ],

      "newspath" => Array(
        "label"         => "News path:",
        "type"      => "dialog",
        "dialog"    => "?module=dialogs&action=links&site_id=".$GLOBALS['site_id']
      ),

      "templatesettings" => Array(
        "label"     => "Settings for templates:",
        "type"      => "hidden",
      ),

      "mobile" => Array(
        "label"     => "Mobile:",
        "type"      => "check",
      ),


    );

  }

}

?>