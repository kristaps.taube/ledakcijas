<?php
/**
*
*  Title: Registration form
*  Author: Kristaps Taube <ktaube@datateks.lv>
*  Date: 17.01.2014
*  Project: E-Veikals
*
*/

use Constructor\Url;
use Constructor\WebApp;

class EVeikalsRegistrationForm extends component{

    private $data;

    public function __construct($name){
        parent::__construct($name);
        $this->init();
    }

  function execute(){

    if($_SESSION['SOCIAL_REG_EMAIL_ERROR'] === true){
      unset($_SESSION['SOCIAL_REG_EMAIL_ERROR']);
      $this->errors = array($this->l("Jūsu norādītā e-pasta adrese ir jau aizņemta"));
    }

    if(isset($_POST['dr_register'])){
      $this->errors = $this->dr_register($_POST);
    }

  }

  function output(){

    echo 'im not outputting...';
    return;

    if(!$this->login) return;

    if($_SESSION['logged_in']){

      echo "<div class='padding'>";
      echo str_replace("[name]", $_SESSION['logged_in']['name']." ".$_SESSION['logged_in']['surname'], $this->l("Jūs esat pieslēdzies kā [name]"));
      echo "</div>";

    }elseif($_SESSION['dr_reg_form'] === true || $_POST['dr_register']){ // must ask for email
      $this->output_draugiem_form();
      unset($_SESSION['dr_reg_form']);
    }elseif($_GET['action'] == 'activation_sent'){
    	$this->activation_sent();
    }elseif($_GET['activate']){
    	$this->activate($_GET['activate']);
    }else{
      $this->output_form();
    }

  }

  public function setData($data)
  {
      $this->data = $data;
  }

  function activate($hash){

 		$user = $this->users->GetRow(Array("where" => "activation_hash = :hash", "params" => array(":hash" => $hash)));

    if(!$user){
    	echo "<div class='padding error'>".$this->l("Lietotājs netika atrasts")."</div>";
			return;
    }elseif($user['active'] == 1){
    	echo "<div class='padding error'>".$this->l("Lietotājs jau ir aktīvs")."</div>";
			return;
    }

		$this->users->Update(array("active" => "1"), $user['item_id']);
		$this->login->authorizeUserEntry($user);
    echo "<div class='success padding'>".$this->l("Konts aktivizēts")."</div>";

  }

  function activation_sent(){
  	echo "<div class='padding'>".$this->l("Uz Jūsu norādīto e-pasta adresi ir nosūtīta konta aktivizācijas vēstule. Lūdzu sekojiet informācijai tajā.")."</div>";
  }

  function dr_register($data){

    $errors = array();

    if(empty($data['email'])) $errors[] = $this->l("Tukšs lauks").": ".$this->l('E-pasts');
    if(!empty($data['email']) && !filter_var($data['email'], FILTER_VALIDATE_EMAIL)) $errors[] = $this->l("Nekorekts E-pasts");
    $user = $this->users->getUserByEmail($data['email']);
    if($user) $errors[] = $this->l("E-pasta adrese ir jau aizņemta");

    if(empty($errors)){ // everything ok

      $user = $_SESSION['dr_user_data'];
      $user['email'] = $data['email'];

      $ind = $this->users->addItemAssoc($user);
      $id = $this->users->getItemId($ind);
      $user = $this->users->getItemByIDAssoc($id);

      $this->login->authorizeUserEntry($user);

      unset($_SESSION['dr_user_data']);

      header('Location: '.$GLOBALS['cur_url'].'');
      die();

    }

    return $errors;

  }

  function output_draugiem_form(){

    ?>
    <div class='padding'><?=$this->l("Lai pabeigtu reģistrāciju, ievadiet e-pasta adresi.")?></div>
    <? if(!empty($this->errors)){ ?>
    <ul class='errors'>
      <li><?=implode("</li><li>", $this->errors)?></li>
    </ul>
    <? } ?>
    <form method='post' action='<?=$_SERVER["REQUEST_URI"]?>' id="Registration">
      <table>
        <tr>
          <td><label for='field_email'><?=$this->l('E-Pasts') ?>*</label>:</td>
          <td><input name="email" value="<?=htmlspecialchars($_POST['email'], ENT_QUOTES) ?>" type="text" class="form_box1" id='field_email' /></td>
        </tr>
        <tr><td></td><td><input type="submit" name="dr_register" value="<?=$this->l('Pabeigt reģistrāciju') ?>" /></td></tr>
      </table>
    </form>
    <?



  }

    private function init()
    {

        $this->activation_mail_text = option("Registration\\activation_mail_text_".$this->lang,
            null,
            "Konta aktivizācijas vēstule(".$this->lang.")",
            array(
                "type" => "wysiwyg",
                "value_on_create" => "Labdien,<br />Jūs esat reģistrejies portālā veikals.datateks.lv. Lai aktivizētu kontu, spiežiet uz saites [link]<br /><br />Ar cieņu,<br />Administrācija",
            )
        );

        $this->subject = option("Registration\\activation_mail_subject", null, "Konta aktivizācijas vēstules tēma", array('value_on_create' => "Reģistrācija portālā veikals.datateks.lv"));
        $this->admin_email = option("admin_emailaddress", null, "Admin e-mail", array("value_on_create" => "ktaube@datateks.lv"));
        $this->sender = option("Registration\\activation_mail_sender", null, "Sūtītājs", array("value_on_create" => "Registration"));

    }

    public function validateForm()
    {

        $errors = array();

        $data = &$this->data;

        $data['name'] = preg_replace('~[^\p{L}]++~u', '', $data['name']);
        $data['surname'] = preg_replace('~[^\p{L}]++~u', '', $data['surname']);

        if(empty($data['name'])){
            $errors[] = $this->l("Tukšs lauks").": ".$this->l('Vārds');
        }elseif(strlen($data['name']) < 3){
            $errors[] = replace_all($this->l("Lauka [field] minimālais zīmju skaits ir [count]"), array("[field]" => $this->l('Vārds'), "[count]" => 3));
        }

        if(empty($data['surname'])){
            $errors[] = $this->l("Tukšs lauks").": ".$this->l('Uzvārds');
        }elseif(strlen($data['surname']) < 3){
            $errors[] = replace_all($this->l("Lauka [field] minimālais zīmju skaits ir [count]"), array("[field]" => $this->l('Uzvārds'), "[count]" => 3));
        }

        if(empty($data['email'])){
            $errors[] = $this->l("Tukšs lauks").": ".$this->l('E-pasts');
        }elseif(!filter_var($data['email'], FILTER_VALIDATE_EMAIL)){
            $errors[] = $this->l("Nekorekts E-pasts");
        }

        if(empty($data['password'])){
            $errors[] = $this->l("Tukšs lauks").": ".$this->l('Parole');
        }elseif(strlen($data['password']) < 5){
            $errors[] = str_replace("[count]", 5, $this->l("Parolei jābūt vismaz [count] simbolu garai"));
        }elseif($data['password'] != $data['password2']){
            $errors[] = $this->l("Paroles nesakrīt");
        }

        $user = shopusercollection::getInstance()->getUserByEmail($data['email']);
        if($user) $errors[] = $this->l("E-pasta adrese ir jau aizņemta");

        $this->errors = $errors;

        return ($errors) ? false : true;

    }

    public function processRegistration()
    {

        # $errors = $this->validateForm();

        $user = array();

        // salt
        $salt = generate_random_string(5);

        // hashing password
        $user['password'] = $salt.sha1($salt.$this->data['password']);

        $user['name'] = $this->data['name'];
        $user['surname'] = $this->data['surname'];
        $user['email'] = $this->data['email'];
        $user['lang'] = WebApp::$app->getLanguage();

        $user['activation_hash'] = sha1(time().generate_random_string(5));

        $id = shopusercollection::getInstance()->Insert($user);

        $url = Url::get('/registration/activation', ['hash' => $user['activation_hash']], true);
        $text = str_replace("[link]", $url, $this->activation_mail_text);

        $sent = phpMailerSend($text, $this->subject, $this->sender, $this->admin_email, $user['email'], true, false);

    }

    public function outputForm()
    {

        $data = $this->data;

        if(!empty($this->errors)){ ?>
        <ul class='errors'>
            <li><?=implode("</li><li>", $this->errors)?></li>
        </ul>
        <? } ?>
        <form action="?" method="POST" enctype="multipart/form-data" id="Registration">
            <div class='row'>
                <div class='col-12'>
                    <label for='field_name'><?php echo $this->l('Vārds') ?>:</label>
                    <input id='field_name' name="name" value="<?=htmlspecialchars($data['name'], ENT_QUOTES) ?>" type="text" class="form_box1" />
                </div>
            </div>
            <div class='row'>
                <div class='col-12'>
                    <label for='field_surname'><?php echo $this->l('Uzvārds') ?>:</label>
                    <input id='field_surname' name="surname" value="<?=htmlspecialchars($data['surname'], ENT_QUOTES) ?>" type="text" class="form_box1" />
                </div>
            </div>
            <div class='row'>
                <div class='col-12'>
                    <label for='field_email'><?php echo $this->l('E-pasts') ?>:</label>
                    <input id='field_email' name="email" value="<?=htmlspecialchars($data['email'], ENT_QUOTES) ?>" type="text" class="form_box1" />
                </div>
            </div>
            <div class='row'>
                <div class='col-12'>
                    <label for='field_password'><?php echo $this->l('Parole') ?>:</label>
                    <input id='field_password' name="password" value="<?=htmlspecialchars($data['password'], ENT_QUOTES) ?>" type="password" class="form_box1" />
                </div>
            </div>
            <div class='row'>
                <div class='col-12'>
                    <label for='field_password_again'><?php echo $this->l('Parole atkārtoti') ?>:</label>
                    <input id='field_password_again' name="password2" value="<?=htmlspecialchars($data['password2'], ENT_QUOTES) ?>" type="password" class="form_box1" />
                </div>
            </div>
            <div class='row'>
                <div class='col-12'>
                    <input type="submit" class="btn orange" name="register" value="<?=$this->l('Reģistrēties') ?>" />
                </div>
            </div>
        </form>
        <?

    }

}
