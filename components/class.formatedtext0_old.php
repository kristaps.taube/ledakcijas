<?
//Serveris.LV Constructor component
//Created by Aivars Irmejs, 2003, aivars@serveris.lv

include_once("class.component.php");

class formatedtext0 extends component
{

  //Class initialization
  function formatedtext0($name)
  {
    parent::__construct($name);
    $this->SetProperties();
  }


  function Output()
  {
    $data = $this->getProperty("text");
    $format = $this->getProperty("customformat");
    $text = $format ? sprintf($format, $data) : $data;

    $text = $this->processResizedImages($text);

    $cmptags = $this->parseDynamicComponentsTags($text);
    if ($cmptags)
      $text = $this->outputDynamicComponents($text, $cmptags);

    if($text){ ?><div class='formatedtext'><? }
    echo $text;
    if($text){ ?></div><? }
  }

  function parseAttributes($s)
  {
    $attribs = array();
    preg_match_all('~([a-zA-Z0-9_]+?)\\s*\\=\\s*\\"(.*?)\\"~', $s, $atmatches);
    for ($j=0; $j<count($atmatches[1]); $j++)
    {
      $attribs[$atmatches[1][$j]] = html_entity_decode($atmatches[2][$j]);
    }

    return $attribs;
  }

  function serializeAttributes($attribs)
  {
    $a = array();
    foreach ($attribs as $key => $val)
    {
      $a[] = $key.'="'.htmlspecialchars($val).'"';
    }

    return implode(' ', $a);
  }

  function parseStyles($s)
  {
    $styles = array();
    preg_match_all('~([a-zA-Z0-9_]+?)\\s*\\:\\s*([^\\;]*)~', $s, $matches);
    for ($j=0; $j<count($matches[1]); $j++)
    {
      $styles[$matches[1][$j]] = $matches[2][$j];
    }

    return $styles;
  }

  function processResizedImages($text)
  {
    $outtext = '';
    $pos = 0;

    preg_match_all('~<img\\s*(.*?)/>~', $text, $matches, PREG_OFFSET_CAPTURE);
    for ($i=0; $i<count($matches[1]); $i++)
    {
      $attribs = $this->parseAttributes($matches[1][$i][0]);
      if (empty($attribs['src']) || substr($attribs['src'], 0, 4) == 'http') continue;

      $tpos = $matches[0][$i][1];
      $tlen =  strlen($matches[0][$i][0]);

      if ($tpos > $pos)
        $outtext .= substr($text, $pos, $tpos - $pos);

      $width = $attribs['width'] >= 0 ? $attribs['width'] : null;
      $height = $attribs['height'] >= 0 ? $attribs['height'] : null;

      if ($attribs['style'])
      {
        $styles = $this->parseStyles($attribs['style']);
        if ($styles['width'] && strpos($styles['width'], 'px') !== false)
          $width = intval($styles['width']);

        if ($styles['height'] && strpos($styles['height'], 'px') !== false)
          $height = intval($styles['height']);

      }

      if ($width > 0 || $height > 0)
      {
        if ($width && $height)
          $resizemode = PRM_NOASPECT_IF_LARGER;
        else if ($width)
          $resizemode = PRM_WIDTH;
        else
          $resizemode = PRM_HEIGHT;

        $thumb = getThumbURL($attribs['src'], $width, $height, $resizemode);
        if ($thumb)
          $attribs['src'] = $thumb;

        $outtext .= '<img '.$this->serializeAttributes($attribs).'/>';
      }
      else
        $outtext .= $matches[0][$i][0];


      $pos = $tpos + $tlen;
    }

    if ($pos < strlen($text))
      $outtext .= substr($text, $pos, strlen($text) - $pos);

    return $outtext;
  }

  function outputDynamicComponents($text, $cmptags, $isdesign=false)
  {
    $outtext = '';
    $pos = 0;
    foreach ($cmptags as $cmp)
    {
      if ($cmp['pos'] > $pos)
        $outtext .= substr($text, $pos, $cmp['pos'] - $pos);

      $outtext .= $this->outputDynamicComponent($cmp['attribs']['id'], $isdesign);

      $pos = $cmp['pos'] + $cmp['length'];
    }

    if ($pos < strlen($text))
      $outtext .= substr($text, $pos, strlen($text) - $pos);

    return $outtext;
  }

  function parseDynamicComponentsTags($text)
  {
    $tags = array();

    preg_match_all('~<component\\s*(.*?)>.*?</component>~', $text, $matches, PREG_OFFSET_CAPTURE);
    for ($i=0; $i<count($matches[1]); $i++)
    {
      $tags[] = array(
        'attribs' => $this->parseAttributes($matches[1][$i][0]),
        'pos' => $matches[0][$i][1],
        'length' => strlen($matches[0][$i][0])
      );
    }

    return $tags;
  }

  function getDynamicComponentsIDs()
  {
    $ids = array();
    $tags = $this->parseDynamicComponentsTags($this->getProperty('text'));
    foreach ($tags as $tag)
    {
      if ($tag['attribs']['id'])
        $ids[] = $tag['attribs']['id'];

    }

    return $ids;
  }

  function Design(){
    if ($this->componentParams['aftersave']){
      $this->updateDynamicComponents($this->getComponentPageID());
      option("page_updates\\page".$this->page_id, time(), "Lapas atjaunota(".$this->page_id.")", Array("is_advanced" => true));
    }

    return $this->designCKeditor();

  }

  function designDefaultEditor()
  {
    $html = $this->getProperty("text");
    $showedit = $this->getProperty("showedit");
    echo '<div id="' . $this->name . 'contentcontent" style="cursor: auto;">';

    if(!$html)
    {
      echo '{%langstr:formatedtext_empty%}';
    }else
    {
      echo $html;
    }
    echo "</div>";
    echo "&nbsp;";
    echo '<input type="hidden" id="div_'.$this->name.'_cont" name="div_'.$this->name.'_cont" value="' . htmlspecialchars($html) . '">';
    if($showedit)
    {
      echo ' <input type="button" value="Edit" style="width: auto"  OnClick="javascript:

                 document.getElementById("div_'.$this->name.'").thevalue = document.getElementById("div_'.$this->name.'_cont").value;
                 var args = new Array(window, div_'.$this->name.');
                 var html = openModalWin(\'?module=wysiwyg&canpreview=1&site_id='.$this->site_id.'&page_id='.$this->page_id.'\', document.getElementById("div_'.$this->name.'"), 720, 450, 1);

                 if(html)
                 {
                   openModalWin(\'?module=dialogs&action=refreshsinglecomponentframe&site_id='.$this->site_id.'&page_id='.$this->page_id.'&component_name='.$this->name.'&component_type='.get_class($this).'&newproperty=text&mustrefresh=1&paramstr='.urlencode($this->componentParamStr) . '&session_id=' . session_id() . '&session_name=' . session_name() . '\', args, 1, 1, 0, 0, 0);
                 }else
                 {
                   openModalWin(\'?module=dialogs&action=refreshsinglecomponentframe&site_id='.$this->site_id.'&page_id='.$this->page_id.'&component_name='.$this->name.'&component_type='.get_class($this).'&newproperty=&mustrefresh=1&paramstr='.urlencode($this->componentParamStr) . '&session_id=' . session_id() . '&session_name=' . session_name() . '\', args, 1, 1, 0, 0, 0);
                 }

                 try {
                   PWin.close();
                 } catch(e) { }

                 "><br />';
    }
  }

  function strip_selected_tags($text, $tags = array())
  {
      $args = func_get_args();
      $text = array_shift($args);
      $tags = func_num_args() > 2 ? array_diff($args,array($text))  : (array)$tags;
      foreach ($tags as $tag){
          while(preg_match('/<'.$tag.'(|\W[^>]*)>(.*)<\/'. $tag .'>/iusU', $text, $found)){
              $text = str_replace($found[0],$found[2],$text);
          }
      }

      return preg_replace('/(<('.join('|',$tags).')(|\W.*)\/>)/iusU', '', $text);
  }

  function designCKeditor(){

    $html = $this->getProperty("text");
    $html = $this->strip_selected_tags($html, array('font'));

    $showedit = $this->getProperty("showedit");
    $format = $this->getProperty("customdesignformat");
    if(!$format) $format = $this->getProperty("customformat");
    if($format){
      $format = explode('%s', $format);
      echo $format[0];
    }

    $text = $format ? sprintf($format, $html) : $html;
    $text = strip_only_tags($text, array('script'), true);

    $cmptags = $this->parseDynamicComponentsTags($text);
    if ($cmptags) $text = $this->outputDynamicComponents($text, $cmptags, true);

    $text = $text ? $text : '{%langstr:formatedtext_empty%}';

    ?>
		<div id="<?=$this->name?>contentcontent" style="cursor: auto;">
    <?=$text?>
		</div>
		<div id="<?=$this->name?>fck" style="display:none">
      <form id="<?=$this->name?>form" action="">
        <textarea id="<?=$this->name?>fcktxt" cols="40" style="display:none;" rows="8"></textarea>
      </form>
    </div>
		<input type="hidden" id="div_<?=$this->name?>_cont" name="div_<?=$this->name?>_cont" value="<?=htmlspecialchars($html)?>"/>
		<?

    if($format) echo $format[1];

  }

  function getComponentPageID()
  {
    $savemode = $this->getProperty('savemode');

    $component_page_id = $this->page_id;
    if ($savemode == 1)
      $component_page_id = -1;
    else if ($savemode == 2)
      $component_page_id = 0;

    return $component_page_id;
  }

  function toolbarCKeditor(&$script, &$toolbars, &$btnids, &$onclick){
    $component_page_id = $this->getComponentPageID();

    $script = '

      window.oncomplete'.$this->name.' = function (instance)
      {

      ';

    if($this->getProperty('editorcssmode') == 1){
      $script .= '
        var copyStyles = new Array("backgroundColor", "backgroundImage", "backgroundRepeat", "color", "fontFamily", "fontSize", "fontStretch", "fontStyle", "fontVariant", "fontWeight", "letterSpacing", "lineHeight", "listStyleImage", "listStylePosition", "listStyleType", "textAlign", "textDecoration", "textIndent", "textShadow", "textTransform", "verticalAlign", "whiteSpace", "wordSpacing");
        var curstyle;
        var o = document.getElementById("div_'.$this->name.'");
        for(var f = 0; f < copyStyles.length; f++)
        {
          try {
            var o = document.getElementById("div_'.$this->name.'");

            var a = [];
            while (o)
            {
              curstyle = getCurrentStyle(o, copyStyles[f]);
              if(curstyle != "undefined")
                a.push({name: copyStyles[f], style: curstyle});

              break;
              o = o.parentNode;
            }

//            a.reverse();

            for (var i=0; i<a.length; i++)
            {
              instance.EditorDocument.body.style[a[i].name] = a[i].style;
            }

          } catch(e) {
          }
        }';
    }
    $script .= '
      }



      //initialize ck editor
      includeJsOnce("'.$GLOBALS['cfgWebRoot'].'gui/ckeditor/ckeditor.js");
 			$("body").append("<div style=position:fixed;top:0;z-index:100000;width:100%; id=top_toolbar></div>");

			window.CKEDITOR.on("instanceReady", function ( instance ){
      	//initialize CKEDITOR class
      });

      function onsave'.$this->name.'(){

				var editor = CKEDITOR.instances.'.$this->name.'fcktxt;
        if (!editor) return;

        var data = editor.getData();
        document.getElementById("'.$this->name.'contentcontent").innerHTML = data;
        document.getElementById("div_'.$this->name.'_cont").value = data;

        document.getElementById("div_'.$this->name.'").thevalue = data;

				delete CKEDITOR.instances.editor;

        var args = new Array(window, document.getElementById("div_'.$this->name.'"));
        openDlg(\''.$GLOBALS['cfgWebRoot'].'?module=dialogs&action=refreshsinglecomponentframe&site_id='.$this->site_id.'&page_id='.$this->page_id.'&pagedev_id='.$this->pagedev_id.'&component_name='.$this->name.'&component_type='.get_class($this).'&newproperty=text&savemode='.$this->getProperty('savemode').'&mustrefresh=1&paramstr='.urlencode($this->componentParamStr.' aftersave "1"') . '&session_id=' . session_id() . '&session_name=' . session_name() . '\', args, {w: 1,h: 1}, function(){  });
				location.reload();

      }

      function savebtn'.$this->name.'(){
        SaveComponents(1);
        return false;
      }

      function getStyles(o){
        var copyStyles = ["backgroundColor", "backgroundImage", "backgroundRepeat", "color", "fontFamily", "fontSize", "fontStretch", "fontStyle", "fontVariant", "fontWeight", "letterSpacing", "lineHeight", "listStyleImage", "listStylePosition", "listStyleType", "textAlign", "textDecoration", "textIndent", "textShadow", "textTransform", "verticalAlign", "whiteSpace", "wordSpacing","margin","padding","marginTop","marginBottom","marginLeft","paddingTop","paddingBottom","paddingLeft","paddingRight","position","display","visibility","zIndex","left","top","bottom","right","float","clear"]

        var styles = {};
        for(var f=0; f<copyStyles.length; f++){
          var name = copyStyles[f];
          var s = getCurrentStyle(o, name);
          if (s !== undefined){
            name = name.replace(/([A-Z])/g, "-$1");
            name = name.toLowerCase();
            styles[name] = s;
          }
        }

        return styles;
      }

      function clickedit'.$this->name.'(){

        //if already inited don\'t do anything
        try {
        	var editor = CKeditor.instances.'.$this->name.'fcktxt;
					if (editor) return;
        } catch(e) { }

        document.getElementById("'.$this->name.'fcktxt").value = document.getElementById("div_'.$this->name.'_cont").value;

        var h = document.getElementById("'.$this->name.'contentcontent").offsetHeight + 100;
        var h2 = document.getElementById("'.$this->name.'fcktxt").offsetHeight + 100;
				$("body").css("padding-top",h2);

				var CK'.$this->name.' = CKEDITOR.replace("'.$this->name.'fcktxt",{
					allowedContent: true,
					on: {
						save: function(evt){
							onsave'.$this->name.'();
							return false;
						}
					},
					sharedSpaces :{
						top : "top_toolbar"
					}
				});

        var CK = CK'.$this->name.';

				var langs = new Object();
				langs["Latvian"] = "lv";
				langs["Russian"] = "ru";
				langs["English"] = "en";

				var lang = langs["'.$GLOBALS['cfgLanguage'].'"];
				CK.config.language = lang;
		    CK.basePath = "'.$GLOBALS['cfgWebRoot'].'gui/ckeditor/";
				CK.config.filebrowserBrowseUrl = "'.$GLOBALS['cfgWebRoot'].'?module=dialogs&action=filex&site_id='.$GLOBALS['site_id'].'&view=thumbs";
	      CK.config.height = h;
		';

    if($this->getProperty('enterhandling') == 1){
      $script .= '
	   		fck.config.enterMode = CKEDITOR.ENTER_BR;
        fck.config.shiftEnterMode = CKEDITOR.ENTER_P;
      ';
    }

    $script .= '
        document.getElementById("div_'.$this->name.'").oncomponentsave = onsave'.$this->name.';
        document.getElementById("'.$this->name.'contentcontent").style.display = "none";
        document.getElementById("'.$this->name.'fck").style.display = "";
    }

    ';

    $onclick = 'clickedit'.$this->name.'();';

    return true;
  }

  function toolbarDefaultEditor(&$script, &$toolbars, &$btnids, &$onclick)
  {
    $sitedata = sqlQueryRow("SELECT domain, wwwroot FROM sites WHERE site_id='".$this->site_id."'");
    $base = 'http://' . $sitedata['domain'] . $sitedata['wwwroot'];

    require_once($GLOBALS['cfgDirRoot']."library/"."class.componenttoolbar.php");
    $toolbars="";

    $tlb = new componenttoolbar();
    $tlb_3 = new componenttoolbar();
    $tlb_2 = new componenttoolbar();

    //FIRST TOOLBAR

    $cssclasslist = $this->getProperty('cssclasslist');

    if (trim($cssclasslist)!="")
    {
        $classes = explode(";",$cssclasslist);
        $str_css = ":No Style";
        foreach ($classes as $class)
            $str_css .= ":".$class;
        $tlb_3->addCombo($this->name."_100c_CMB", $str_css, "PutStyle".$this->name."('".$this->name."_100c_CMB');", $style="width:90px;");
        $tlb_3->addCombo($this->name."_101p_CMB", ":Normal:Heading 1:Heading 2:Heading 3:Heading 4:Heading 5:Heading 6:Address:Formatted", "FontProperties".$this->name."('FormatBlock', '".$this->name."_101p_CMB');", $style="width:90px;");
    }
    else
    {
        $tlb_3->addCombo($this->name."_101p_CMB", ":Normal:Heading 1:Heading 2:Heading 3:Heading 4:Heading 5:Heading 6:Address:Formatted", "FontProperties".$this->name."('FormatBlock', '".$this->name."_101p_CMB');", $style="width:90px;");
        $tlb_3->addImage($this->name."_121", "font_delete.gif", "toggle".$this->name."('decreasefontsize');", $alt="{%langstr:font_smaller%}");
        $tlb_3->addImage($this->name."_122", "font_add.gif", "toggle".$this->name."('increasefontsize');", $alt="{%langstr:font_bigger%}");
        $tlb_3->addCombo($this->name."_102f_CMB", ":Arial:Verdana:Courier New:Times New Roman:Tahoma:Comic Sans MS:Georgia", "FontProperties".$this->name."('FontName', '".$this->name."_102f_CMB');", $style="width:140");
        $tlb_3->addCombo($this->name."_103s_CMB", ":1:2:3:4:5:6:7", "FontProperties".$this->name."('FontSize', '".$this->name."_103s_CMB');", $style="width:40", 1);
    }

    $tlb_3->addImage($this->name."_104", "bold.gif", "toggle".$this->name."('Bold',this.id);", $alt="{%langstr:bold%}");
    $tlb_3->addImage($this->name."_105", "italics.gif", "toggle".$this->name."('Italic');", $alt="{%langstr:italics%}");
    $tlb_3->addImage($this->name."_106", "underline.gif", "toggle".$this->name."('Underline');", $alt="{%langstr:underline%}","",1);


    if (trim($cssclasslist)=="")
    {
        $tlb_3->addImage($this->name."_107", "fgcolor.gif", "setforecolor".$this->name."('ForeColor');", $alt="{%langstr:forecolor%}");
        $tlb_3->addImage($this->name."_108", "bgcolor.gif", "setbackcolor".$this->name."('BackColor');", $alt="{%langstr:background_color%}","",1);
    }

    $tlb_3->addImage($this->name."_109", "left.gif", "toggle".$this->name."('JustifyLeft');", $alt="{%langstr:justify_left%}");
    $tlb_3->addImage($this->name."_110", "center.gif", "toggle".$this->name."('JustifyCenter');", $alt="{%langstr:center%}");
    $tlb_3->addImage($this->name."_111", "right.gif", "toggle".$this->name."('JustifyRight');", $alt="{%langstr:justify_right%}","",1);

    $tlb_3->addImage($this->name."_112", "ol.gif", "toggle".$this->name."('InsertOrderedList');", $alt="{%langstr:insert_numbered_list%}");
    $tlb_3->addImage($this->name."_113", "ul.gif", "toggle".$this->name."('InsertUnorderedList');", $alt="{%langstr:insert_bulleted_list%}","",1);

    $tlb_3->addImage($this->name."_114", "sup.gif", "toggle".$this->name."('Superscript');", $alt="{%langstr:superscript%}");
    $tlb_3->addImage($this->name."_115", "sub.gif", "toggle".$this->name."('Subscript');", $alt="{%langstr:subscript%}","",1);

    $tlb_3->addImage($this->name."_116", "move_left.gif", "toggle".$this->name."('Outdent');", $alt="{%langstr:move_left%}");
    $tlb_3->addImage($this->name."_117", "move_right.gif", "toggle".$this->name."('Indent');", $alt="{%langstr:move_right%}","",1);

    $tlb_3->addImage($this->name."_118", "HTML.gif", "edithtml".$this->name."();", $alt="{%langstr:edit_html%}");

    //SECOND TOOLBAR

    $tlb->addImage($this->name."_201", "save.gif", "SaveComponents(1);", $alt="{%langstr:save_changes%}");
//    $tlb->addImage($this->name."_202", "restore.gif", "toggle".$this->name."('Refresh');SetStatusbarHTML('',1);", $alt="{%langstr:erease_all_changes%}","",1);

    $tlb->addImage($this->name."_203", "word_sel.gif", "DeWord".$this->name."('Refresh');", $alt="{%langstr:remove_ms_word_tags%}","",1);

    $tlb->addImage($this->name."_204", "cut.gif", "toggle".$this->name."('Cut');", $alt="{%langstr:cut%}");
    $tlb->addImage($this->name."_205", "copy.gif", "toggle".$this->name."('Copy');", $alt="{%langstr:copy%}");
    $tlb->addImage($this->name."_206", "paste.gif", "PasteFromClipboard".$this->name."();", $alt="{%langstr:paste%}");
    $tlb->addImage($this->name."_207", "text.gif", "instext".$this->name."();", $alt="{%langstr:insert_plain_text%}","",1);

    $tlb->addImage($this->name."_208", "undo.gif", "toggle".$this->name."('Undo');", $alt="{%langstr:undo%}");
    $tlb->addImage($this->name."_209", "redo.gif", "toggle".$this->name."('Redo');", $alt="{%langstr:redo%}","",1);

    //$tlb->addImage($this->name."_210", "find.gif", "findtext".$this->name."('Find');", $alt="find text", "", 1);

    $tlb->addImage($this->name."_210", "glink.gif", "glink".$this->name."();", $alt="{%langstr:insert_global_link%}");
    $tlb->addImage($this->name."_211", "link.gif", "link".$this->name."();", $alt="{%langstr:insert_link%}");
    $tlb->addImage($this->name."_212", "filelink.gif", "filelink".$this->name."();", $alt="{%langstr:link_file%}");
    $tlb->addImage($this->name."_214", "bookmark.gif", "addbookmark".$this->name."();", $alt="{%langstr:insert_bookmark%}","",1);

    $tlb->addImage($this->name."_213", "image.gif", "setimage".$this->name."();", $alt="{%langstr:insert_image%}", "", 2);
    //$tlb->addImage($this->name."_215", "lightningimage.gif", "setlightningimage".$this->name."();", $alt="Insert lightning image", "", 2);

    //THIRD TOOLBAR

    $tlb->addImage($this->name."_301", "instable.gif", "InsertTable".$this->name."();", $alt="{%langstr:insert_table%}", "",1);

    $tlb->addImage($this->name."_302", "insrow.gif", "InsertRowBelow".$this->name."();", $alt="{%langstr:insert_row%}");
    $tlb->addImage($this->name."_303", "delrow.gif", "DeleteRow".$this->name."();", $alt="{%langstr:delete_rows%}", "", 1);
    $tlb->addImage($this->name."_304", "inscol.gif", "InsertColBefore".$this->name."();", $alt="{%langstr:insert_column%}");
    $tlb->addImage($this->name."_305", "delcol.gif", "DeleteCol".$this->name."();", $alt="{%langstr:delete_columns%}","",1);
    $tlb->addImage($this->name."_306", "inscell.gif", "InsertCell".$this->name."();", $alt="{%langstr:insert_cell%}");
    $tlb->addImage($this->name."_307", "delcell.gif", "DeleteCell".$this->name."();", $alt="{%langstr:delete_cells%}","",1);
    $tlb->addImage($this->name."_308", "mrgcell.gif", "IncreaseColspan".$this->name."();", $alt="{%langstr:increase_colspan%}");
    $tlb->addImage($this->name."_309", "spltcell.gif", "DecreaseColspan".$this->name."();", $alt="{%langstr:decrease_colspan%}", "", 1);

    $tlb->addImage($this->name."_310", "tprop.gif", "table_prop".$this->name."();", $alt="{%langstr:table_properties%}");
    $tlb->addImage($this->name."_311", "cellprop.gif", "cell_prop".$this->name."();", $alt="{%langstr:cell_properties%}");
    $tlb->addImage($this->name."_312", "rowprop.gif", "row_prop".$this->name."();", $alt="{%langstr:row_properties%}");

    $script = $tlb->outputJS();
    $script .= $tlb_3->outputJS();
    //$script .= $tlb_2->outputJS();
    $script .= 'function clickedit'.$this->name.'()
          {
            //make sure the time-consuming initialization is only done once
            ////document.getElementById("div_'.$this->name.'").set_tb_cnt = "";

            if(document.getElementById("' . $this->name . 'contentcontent").contentEditable == "inherit" || document.getElementById("' . $this->name . 'contentcontent").contentEditable == "false")
            {
              document.getElementById("' . $this->name . 'contentcontent").contentEditable = true;
              if(document.getElementById("div_'.$this->name.'_cont").value == "")
              {
                document.getElementById("' . $this->name . 'contentcontent").innerHTML = document.getElementById("div_'.$this->name.'_cont").value;
                document.getElementById("div_'.$this->name.'_cont").value = " ";
              }
              if(document.activeElement != document.getElementById("' . $this->name . 'contentcontent"))
              {
                document.getElementById("' . $this->name . 'contentcontent").focus();
              }
              document.getElementById("' . $this->name . 'contentcontent").onkeyup = checkbtns' . $this->name . ';
              document.getElementById("' . $this->name . 'contentcontent").onmouseup = checkbtns' . $this->name . ';
              document.getElementById("' . $this->name . 'contentcontent").onkeydown = CheckForKeys'.$this->name.';
              //setTimeout("checkbtns' . $this->name . '();", 100);
              try
              {
                if(document.getElementById("div_'.$this->name.'").firstChild.className == "borderdiv")
                  document.getElementById("div_'.$this->name.'").firstChild.style.display = "none";
                document.getElementById("div_'.$this->name.'").style.border = "1px solid red";
              }catch(e){}
              document.getElementById("div_'.$this->name.'").oncomponentsave = onsave' . $this->name . ';
            }
            checkbtns' . $this->name . '();
          }

          function PutStyle'.$this->name.'(name)
          {
           if(document.getElementById("' . $this->name . 'contentcontent").contentEditable)
           {
           v = window.parent.window.document.all[name].value

              if(isCursorInEditObject'.$this->name.'() && v!="")
              {
                var arr, oSel, oParent, sType;
                oSel = document.selection;
                sType=oSel.type;
                if(sType=="Control")
                {
                  //image selected
                  var cList = oSel.createRange();

                  var oRange = document.body.createTextRange( );

                  var cObj = cList.item(0);
                  oRange.moveToElementText(cObj);
                  var control = true;
                }else
                {
                  var oRange = oSel.createRange();
                  var control = false;
                }

                if (v=="No Style")
                {

                span = oRange.parentElement();

                //var aStl = newContent.match(/ style=[\'"]*([^"\' ]+)[\'"]*/i);
                //var aSRC = oRange.htmlText.match(/<img.*src=[\'"]*([^"\' ]+)[\'"]*/i);
                var inspan = oRange.htmlText.match(/<span[^>]*>(.*?)<\/span>/i);

                  if (span != null)
                  {
                      if (!/^span$/i.test(span.tagName))
                          span = span.parentElement;

                      if (span != null)
                      {
                          if (/^span$/i.test(span.tagName))
                          {
                                  //a.target = param.f_target.trim();
                                  //a.title = param.f_title.trim();
                                  selectNodeContents(span);
                                  sType=oSel.type;
                                  oRange = oSel.createRange();

                                  if(control)
                                  {
                                    oRange.pasteHTML(inspan[1]);
                                  }else
                                  {
                                    oRange.pasteHTML(oRange.text);
                                  }
                          }else
                          {
                            span = null;
                          }
                      }
                  }
                }
                else
                {

                //if (arr != null){
                  if(control)
                  {
                    oRange.pasteHTML("<span class=\"" + v + "\">" + oRange.htmlText + "</span>");
                  }else
                  {
                    oRange.pasteHTML("<span class=\"" + v + "\">" + oRange.text + "</span>");
                  }

                  document.getElementById("' . $this->name . 'contentcontent").focus();
                }
                //tbContentElement.focus();
              }
           }
          }

          function onsave' . $this->name . '()
          {
            editsave'.$this->name.'();
          }

          function checkbtns' . $this->name . '()
            {

                if (!isCursorInTableCell'.$this->name.'())
                {
                    DisableButton(window.parent.document.getElementById("'.$this->name.'_302"),0);
                    DisableButton(window.parent.document.getElementById("'.$this->name.'_303"),0);
                    DisableButton(window.parent.document.getElementById("'.$this->name.'_304"),0);
                    DisableButton(window.parent.document.getElementById("'.$this->name.'_305"),0);
                    DisableButton(window.parent.document.getElementById("'.$this->name.'_306"),0);
                    DisableButton(window.parent.document.getElementById("'.$this->name.'_307"),0);
                    DisableButton(window.parent.document.getElementById("'.$this->name.'_308"),0);
                    DisableButton(window.parent.document.getElementById("'.$this->name.'_309"),0);

                    DisableButton(window.parent.document.getElementById("'.$this->name.'_311"),0);
                    DisableButton(window.parent.document.getElementById("'.$this->name.'_312"),0);
                }else
                {
                    EnableButton(window.parent.document.getElementById("'.$this->name.'_302"),0);
                    EnableButton(window.parent.document.getElementById("'.$this->name.'_303"),0);
                    EnableButton(window.parent.document.getElementById("'.$this->name.'_304"),0);
                    EnableButton(window.parent.document.getElementById("'.$this->name.'_305"),0);
                    EnableButton(window.parent.document.getElementById("'.$this->name.'_306"),0);
                    EnableButton(window.parent.document.getElementById("'.$this->name.'_307"),0);
                    EnableButton(window.parent.document.getElementById("'.$this->name.'_308"),0);
                    EnableButton(window.parent.document.getElementById("'.$this->name.'_309"),0);

                    EnableButton(window.parent.document.getElementById("'.$this->name.'_311"),0);
                    EnableButton(window.parent.document.getElementById("'.$this->name.'_312"),0);
                }
                if ((!isTableSelected() || cnst.isFF) && !isCursorInTableCell'.$this->name.'())
                {
                    DisableButton(window.parent.document.getElementById("'.$this->name.'_310"),0);
                }else
                {
                    EnableButton(window.parent.document.getElementById("'.$this->name.'_310"),0);
                }

              var oSel, sType;
              if(document.selection)
              {
                oSel = document.selection;
              }else if(window.getSelection)
              {
                oSel = window.getSelection();
              }

              sType=oSel.type;
              if(cnst.isIE)
              {
                if(sType=="Control")
                {
                  //image selected
                  var cList = oSel.createRange();
                  var oRange = document.body.createTextRange( );
                  var cObj = cList.item(0);
                  oRange.moveToElementText(cObj);

                  DisableButton(window.parent.document.getElementById("'.$this->name.'_104"),0);
                  DisableButton(window.parent.document.getElementById("'.$this->name.'_105"),0);
                  DisableButton(window.parent.document.getElementById("'.$this->name.'_106"),0);
                  DisableButton(window.parent.document.getElementById("'.$this->name.'_107"),0);
                  DisableButton(window.parent.document.getElementById("'.$this->name.'_108"),0);
                  DisableButton(window.parent.document.getElementById("'.$this->name.'_112"),0);
                  DisableButton(window.parent.document.getElementById("'.$this->name.'_113"),0);
                  DisableButton(window.parent.document.getElementById("'.$this->name.'_114"),0);
                  DisableButton(window.parent.document.getElementById("'.$this->name.'_115"),0);
                  DisableButton(window.parent.document.getElementById("'.$this->name.'_116"),0);
                  DisableButton(window.parent.document.getElementById("'.$this->name.'_117"),0);
                }else
                {
                  EnableButton(window.parent.document.getElementById("'.$this->name.'_104"),0);
                  EnableButton(window.parent.document.getElementById("'.$this->name.'_105"),0);
                  EnableButton(window.parent.document.getElementById("'.$this->name.'_106"),0);
                  EnableButton(window.parent.document.getElementById("'.$this->name.'_107"),0);
                  EnableButton(window.parent.document.getElementById("'.$this->name.'_108"),0);
                  EnableButton(window.parent.document.getElementById("'.$this->name.'_112"),0);
                  EnableButton(window.parent.document.getElementById("'.$this->name.'_113"),0);
                  EnableButton(window.parent.document.getElementById("'.$this->name.'_114"),0);
                  EnableButton(window.parent.document.getElementById("'.$this->name.'_115"),0);
                  EnableButton(window.parent.document.getElementById("'.$this->name.'_116"),0);
                  EnableButton(window.parent.document.getElementById("'.$this->name.'_117"),0);
                }
              }

              try
              {
                var fontsize = document.queryCommandValue("fontSize");
              } catch(e) { }
              try
              {
                var fontname = document.queryCommandValue("FontName");
              } catch(e) { }
              try
              {
                var formatblock = document.queryCommandValue("FormatBlock");
              } catch(e) { }

              try
              {
                if(cnst.isFF)
                {
                  if(formatblock == "h1")
                    formatblock = "Heading 1";
                  else if(formatblock == "h2")
                    formatblock = "Heading 2";
                  else if(formatblock == "h3")
                    formatblock = "Heading 3";
                  else if(formatblock == "h4")
                    formatblock = "Heading 4";
                  else if(formatblock == "h5")
                    formatblock = "Heading 5";
                  else if(formatblock == "h6")
                    formatblock = "Heading 6";
                  else if(formatblock == "pre")
                    formatblock = "Formatted";
                  else if(formatblock == "p")
                    formatblock = "Normal";
                }
              } catch(e) {}

              if(cnst.isFF)
              {
                window.parent.document.getElementById("'.$this->name.'_103s_CMB").style.display = "none";
              }else
              {
                window.parent.document.getElementById("'.$this->name.'_121").style.display = "none";
                window.parent.document.getElementById("'.$this->name.'_122").style.display = "none";
              }

              try {
                window.parent.document.getElementById("'.$this->name.'_103s_CMB").value = fontsize;
                window.parent.document.getElementById("'.$this->name.'_102f_CMB").value = fontname;
                window.parent.document.getElementById("'.$this->name.'_101p_CMB").value = formatblock;
              } catch(e) {}

              try {
                if (document.queryCommandState("Bold")) dn2(window.parent.document.getElementById("'.$this->name.'_104"), "bold");
                else up2(window.parent.document.getElementById("'.$this->name.'_104"));
              } catch(e) { }
              try {
                if (document.queryCommandState("Italic")) dn2(window.parent.document.getElementById("'.$this->name.'_105"), "italic");
                else up2(window.parent.document.getElementById("'.$this->name.'_105"));
              } catch(e) { }
              try {
                if (document.queryCommandState("Underline")) dn2(window.parent.document.getElementById("'.$this->name.'_106"));
                else up2(window.parent.document.getElementById("'.$this->name.'_106"));
              } catch(e) { }

              try {
                if (document.queryCommandState("JustifyLeft")) dn2(window.parent.document.getElementById("'.$this->name.'_109"));
                else up2(window.parent.document.getElementById("'.$this->name.'_109"));
              } catch(e) { }
              try {
                if (document.queryCommandState("JustifyCenter")) dn2(window.parent.document.getElementById("'.$this->name.'_110"));
                else up2(window.parent.document.getElementById("'.$this->name.'_110"));
              } catch(e) { }
              try {
                if (document.queryCommandState("JustifyRight")) dn2(window.parent.document.getElementById("'.$this->name.'_111"));
                else up2(window.parent.document.getElementById("'.$this->name.'_111"));
              } catch(e) { }

              try {
                if (document.queryCommandState("InsertOrderedList")) dn2(window.parent.document.getElementById("'.$this->name.'_112"));
                else up2(window.parent.document.getElementById("'.$this->name.'_112"));
              } catch(e) { }
              try {
                if (document.queryCommandState("InsertUnorderedList")) dn2(window.parent.document.getElementById("'.$this->name.'_113"));
                else up2(window.parent.document.getElementById("'.$this->name.'_113"));
              } catch(e) { }

              try {
                if (document.queryCommandState("Superscript")) dn2(window.parent.document.getElementById("'.$this->name.'_114"));
                else up2(window.parent.document.getElementById("'.$this->name.'_114"));
              } catch(e) { }
              try {
                if (document.queryCommandState("Subscript")) dn2(window.parent.document.getElementById("'.$this->name.'_115"));
                else up2(window.parent.document.getElementById("'.$this->name.'_115"));
              } catch(e) { }


                if(cnst.isFF)
                {
                  window.parent.document.getElementById("'.$this->name.'_204").onclick = null;
                  DisableButton(window.parent.document.getElementById("'.$this->name.'_204"),0);
                  window.parent.document.getElementById("'.$this->name.'_205").onclick = null;
                  DisableButton(window.parent.document.getElementById("'.$this->name.'_205"),0);
                  window.parent.document.getElementById("'.$this->name.'_206").onclick = null;
                  DisableButton(window.parent.document.getElementById("'.$this->name.'_206"),0);
                  window.parent.document.getElementById("'.$this->name.'_214").onclick = null;
                  DisableButton(window.parent.document.getElementById("'.$this->name.'_214"),0);
                }
            }

          function escaperegexp' . $this->name . '(text)
          {
              var specials = [
                \'/\', \'.\', \'*\', \'+\', \'?\', \'|\',
                \'(\', \')\', \'[\', \']\', \'{\', \'}\', \'\\\\\'
              ];
              sRE = new RegExp(
                \'(\\\\\' + specials.join(\'|\\\\\') + \')\', \'g\'
              );
              return text.replace(sRE, \'\\\\$1\');
          }

          function editsave'.$this->name.'()
          {
            if(document.getElementById("' . $this->name . 'contentcontent").contentEditable)
            {
              MakeAbsolutePathsRelative'.$this->name.'();
              document.getElementById("' . $this->name . 'contentcontent").contentEditable = false;
              var args = new Array(window, document.getElementById("div_'.$this->name.'"));
              //add alt tags to images
              var imgs = document.getElementById("' . $this->name . 'contentcontent").getElementsByTagName("img");
              for(var f = 0; f < imgs.length; f++)
              {
                if(cnst.isFF)
                {
                  if(!imgs[f].hasAttribute("alt"))
                  {
                    imgs[f].setAttribute("alt", "");
                  }
                }
              }
              document.getElementById("div_'.$this->name.'").thevalue = document.getElementById("' . $this->name . 'contentcontent").innerHTML;

              var link = escaperegexp' . $this->name . '(window.location.pathname + window.location.search);
              link = link.replace(/&/g, "&amp;");
              var re = new RegExp(link, "g");
              document.getElementById("div_'.$this->name.'").thevalue = document.getElementById("div_'.$this->name.'").thevalue.replace(re, \'\');

              //add omnitags to <img>
              document.getElementById("div_'.$this->name.'").thevalue = document.getElementById("div_'.$this->name.'").thevalue.replace(/(<img .*?")>/gi, "$1 />");
              //add omnitags to <br>
              document.getElementById("div_'.$this->name.'").thevalue = document.getElementById("div_'.$this->name.'").thevalue.replace(/<br>/gi, "<br />");
              openModalWin(\'?module=dialogs&action=refreshsinglecomponentframe&site_id='.$this->site_id.'&page_id='.$this->page_id.'&pagedev_id='.$this->pagedev_id.'&component_name='.$this->name.'&component_type='.get_class($this).'&newproperty=text&savemode='.$this->getProperty('savemode').'&mustrefresh=1&paramstr='.urlencode($this->componentParamStr.'  aftersave "1"') . '&session_id=' . session_id() . '&session_name=' . session_name() . '\', args, 1, 1, 0, 0, 0);
              // &afterrefreshmethod=onAfterSave
              ' .
              (
                !$this->componentParams['outborder'] ?
                'document.getElementById("div_'.$this->name.'").style.border = "";' :
                'document.getElementById("div_'.$this->name.'").style.border = "1px dashed #808080";'
              ) .
              'MakeAbsolutePathsRelative'.$this->name.'();
            }
          }

          function IsWordContent'.$this->name.'(newContent)
          {
            var aStl = newContent.match(/ style=[\'"]*([^"\' ]+)[\'"]*/i);
            var aIst = newContent.match(/mso-/i);
            if(aIst != null)
              return true;
            else
            {
              var aIst = newContent.match(/msoNormal/i);
              if(aIst != null)
                return true;
              else
                return false;
            }
          }


          function PasteFromClipboard'.$this->name.'()
          {
            if(!cnst.isFF)
            {
              toggle'.$this->name.'("paste");
            }
            newContent = document.getElementById("' . $this->name . 'contentcontent").innerHTML;
            if(IsWordContent'.$this->name.'(newContent))
            {
              var arr;
              arr = showModalDialog( "?module=wysiwyg&action=paste", arr, "font-family:Verdana; font-size:12; dialogWidth:350px; dialogHeight:300px; help:0; status:0; scroll: 0" );
              if (arr != null) {
                if(arr == 1)  //remove word tags
                {
                  DeWord'.$this->name.'("Refresh");
                }else
                if(arr == 3){  //paste as plain text
                  document.getElementById("' . $this->name . 'contentcontent").innerHTML = document.getElementById("' . $this->name . 'contentcontent").innerText;
                }
              }

            }
            return false;
          }


          function CheckForKeys'.$this->name.'(e)
          {
            if(window.event)
              e = window.event;
            if(e.ctrlKey)
            {
              // 86 = V
				      if (e.keyCode == 86 || e.charCode == 86) {
                  PasteFromClipboard'.$this->name.'();
                  if(cnst.isFF)
                  {
                    setTimeout("PasteFromClipboard' . $this->name . '();", 100);
                  }else
                  {
                    e.returnValue = false;
                    e.keyCode = 0;
                  }
				      }
              // 83 = S
				      if (e.keyCode == 83 || e.charCode == 83) {
                  if(cnst.isFF)
                  {
                    //default ctrl+s cannot be cancelled on FF
                    //e.stopPropagation();
                  }else
                  {
                    e.returnValue = false;
                    e.keyCode = 0;
                    SaveComponents(1);
                  }

				      }
            }
  ';
  if($this->getProperty("enterhandling"))
  {
    $script .= '
            if(event.keyCode == 13 && !event.shiftKey)
            { //check if an image isn\'t selected
              var TempTR = document.selection.createRange();
              if(TempTR.pasteHTML)
              {
                //TempTR.pasteHTML("<br><wbr>");
                temporary = unescape("%0D")
                document.execCommand("paste","",temporary);
                return false;
              }

            }   ';
  }
  $script .= '
          }


          function MakeAbsolutePathsRelative'.$this->name.'()
          {
            if(cnst.isFF)
              elems = window.document.getElementsByTagName("*");
            else
              elems = window.document.all;
            for(f=0; f<elems.length;f++)
            {
              if(elems[f].tagName.toLowerCase() == "img")
              {
                if(elems[f].src.indexOf("'.$base.'") == 0)
                {
                  elems[f].src = elems[f].src.substring("'.$base.'".length - 1);
                }
              }
              if(elems[f].tagName.toLowerCase() == "a")
              {
                if(elems[f].href.indexOf("'.$base.'") == 0)
                {
                  elems[f].href = elems[f].href.substring("'.$base.'".length - 1);
                }
              }
            }
          }

        function FontProperties'.$this->name.'(obj, name) {
                    if(document.getElementById("' . $this->name . 'contentcontent").contentEditable)
                    {
                      v = window.parent.window.document.all[name].value

                      if(cnst.isFF)
                      {
                        if(v == "Heading 1")
                          v = "h1";
                        else if(v == "Heading 2")
                          v = "h2";
                        else if(v == "Heading 3")
                          v = "h3";
                        else if(v == "Heading 4")
                          v = "h4";
                        else if(v == "Heading 5")
                          v = "h5";
                        else if(v == "Heading 6")
                          v = "h6";
                        else if(v == "Formatted")
                          v = "pre";
                        else if(v == "Normal")
                          v = "p";

                      }

                      document.execCommand(obj,0,v);
                      document.getElementById("' . $this->name . 'contentcontent").focus();
                      try
                      {
                        document.selection.createRange().scrollIntoView(true);
                      }catch(e) { }
                    }
        }

          function toggle'.$this->name.'(obj)
          {
            if(document.getElementById("' . $this->name . 'contentcontent").contentEditable)
            {
              if(!cnst.isFF)
                document.execCommand(obj);
              else
                document.execCommand(obj, 0, "");
              checkbtns' . $this->name . '();
              if(document.activeElement != document.getElementById("' . $this->name . 'contentcontent"))
              {
                document.getElementById("' . $this->name . 'contentcontent").focus();
                try
                {
                  document.selection.createRange().scrollIntoView(true);
                }catch(e) { }
              }
            }
          }

        function edithtml'.$this->name.'()
        {
            document.getElementById("div_'.$this->name.'_cont").value = document.getElementById("' . $this->name . 'contentcontent").innerHTML;

            args = {};
            args["plainhtml"] = document.getElementById("div_'.$this->name.'_cont").value;

            arr = showModalDialog( "?module=wysiwyg&action=edithtml&site_id='.$this->site_id.'&", args, "font-family:Verdana; font-size:12; dialogWidth:650px; dialogHeight:500px; help:0; status:0");
            if (arr != null)
            {
                document.getElementById("div_'.$this->name.'_cont").value = arr["plainhtml"];
                document.getElementById("' . $this->name . 'contentcontent").innerHTML = arr["plainhtml"];
            }
        }

        //modified to use the custom control to browse server side images - vsb
        function setimage'.$this->name.'()
        {
          if(isCursorInEditObject'.$this->name.'())
          {
            var arr, oSel, oParent, sType, s;
            var args = {};
            if(cnst.isFF)
            {
              var oRange = window.getSelection().getRangeAt(0);
              oRange.pasteHTML = function(text)
              {
                document.execCommand("inserthtml", 0, text);
              }
              var elem = oRange.cloneContents();
              if(elem.firstChild == elem.lastChild)
              {
                var el = elem.firstChild;
                if(el && el.tagName && el.tagName.toLowerCase() == "img")
                {
                  args["src"] = el.src;
                  args["vspace"] = (el.vspace > 0 ? el.vspace : null);
                  args["hspace"] = (el.hspace > 0 ? el.hspace : null);
                }
              }
            }else
            {
              oSel = document.selection;
              sType=oSel.type;
              if(sType=="Control")
              {
                //image selected
                var cList = oSel.createRange();

                var oRange = document.body.createTextRange( );

                var cObj = cList.item(0);
                oRange.moveToElementText(cObj);
                //might be an image
                var aSRC = oRange.htmlText.match(/<img.*src=[\'"]*([^"\' ]+)[\'"]*/i);
                var aVSPACE = oRange.htmlText.match(/<img.*vspace=[\'"]*([0-9]+)[\'"]*/i);
                var aHSPACE = oRange.htmlText.match(/<img.*hspace=[\'"]*([0-9]+)[\'"]*/i);
                args["src"] = (aSRC != null?aSRC[1]:"");
                args["vspace"] = (aVSPACE != null?aVSPACE[1]:null);
                args["hspace"] = (aHSPACE != null?aHSPACE[1]:null);
              }else
              {
                var oRange = oSel.createRange();
              }
            }
            arr = showModalDialog( "?module=wysiwyg&action=image&site_id='.$this->site_id.'&view=thumbs&", args, "font-family:Verdana; font-size:12; dialogWidth:530px; dialogHeight:450px; help:0; status:0");
            if (arr != null)
            {
              s = "<img src=\"" + arr["src"] + "\"";
              if (arr["vspace"] != "") s = s + " vspace=\"" + arr["vspace"] + "\"";
              if (arr["hspace"] != "") s = s + " hspace=\"" + arr["hspace"] + "\"";
              if (arr["align"] != "") s = s + " align=\"" + arr["align"] + "\"";
              s = s + " border=\"0\" alt=\"\" />";
              oRange.pasteHTML(s);
              MakeAbsolutePathsRelative'.$this->name.'()
            }
            //tbContentElement.focus();
          }
        }

        function setlightningimage'.$this->name.'()
        {
          if(isCursorInEditObject'.$this->name.'())
          {
            var arr, oSel, oParent, sType, s;
            args = new Array();
            oSel = document.selection;
            sType=oSel.type;

            //only create new images
            var oRange = oSel.createRange();

            if(sType=="Control")
            {
            }else
            {
              arr = showModalDialog( "?module=wysiwyg&action=lightningimage&site_id='.$this->site_id.'&view=thumbs&", args, "font-family:Verdana; font-size:12; dialogWidth:530px; dialogHeight:450px; help:0; status:0");
              if (arr != null)
              {
                s = "<a hr"+"ef=\"" + arr["href"] + "\" rel=lightbox><img src=\"" + arr["src"] + "\"";
                if (arr["vspace"] != "") s = s + " vspace=\"" + arr["vspace"] + "\"";
                if (arr["hspace"] != "") s = s + " hspace=\"" + arr["hspace"] + "\"";
                if (arr["align"] != "") s = s + " align=\"" + arr["align"] + "\"";
                s = s + " border=\"0\" alt=\"\" /></a>";
                oRange.pasteHTML(s);
                MakeAbsolutePathsRelative'.$this->name.'()
              }
              //tbContentElement.focus();
            }
          }
        }

        function replaceAll(stringValue, replaceValue, newValue)
        {
          var functionReturn = new String(stringValue);
          while ( true )
          {
            var currentValue = functionReturn;
            functionReturn = functionReturn.replace(replaceValue, newValue);
            if ( functionReturn == currentValue )
              break;
          }
          return functionReturn;
        }

        function instext'.$this->name.'(){
          if(isCursorInEditObject'.$this->name.'())
          {
            arr = null;
            arr = showModalDialog( "?module=wysiwyg&action=instext",
                                    1,
                                   "font-family:Verdana; font-size:12; dialogWidth:570px; dialogHeight:430px; status: 0; help: 0; scroll: 0");
            if (arr != null) {
              arr=replaceAll(arr,"<",\'&lt;\');
              arr=replaceAll(arr,">",\'&gt;\');
              arr=replaceAll(arr,"\n",\'<br/>\');
              if(cnst.isFF)
              {
                document.execCommand("inserthtml", 0, arr);
              }else
              {
                oSel = document.selection;
                var oRange = oSel.createRange();
                oRange.pasteHTML(arr);
              }
            }
            document.getElementById("' . $this->name . 'contentcontent").focus();
            try
            {
              document.selection.createRange().scrollIntoView(true);
            }catch(e) { }
          }
        }



        function InsertTable'.$this->name.'() {

        if(isCursorInEditObject'.$this->name.'())
        {
          var args = {};
          if(cnst.isFF)
          {
            var oRange = window.getSelection().getRangeAt(0);
            oRange.pasteHTML = function(text)
            {
              document.execCommand("inserthtml", 0, text);
            }
          }else
          {
            oSel = document.selection;
            var oRange = oSel.createRange();
          }
          //var arr = null;
          // Display table information dialog
          args["NumRows"] = 3;
          args["NumCols"] = 3;
          args["TableAttrs"] = "";
          //---------------------
          args["Border"] = "1";
          args["CellPadding"] = "1";
          args["CellSpacing"] = "1";
          args["Width"] = "75%";
          //---------------------
          args["CellAttrs"] = "";
          args["Caption"] = "";
          arr = null;
          arr = showModalDialog( "?module=wysiwyg&action=table",
                                  args,
                                 "font-family:Verdana; font-size:12; dialogWidth:400px; dialogHeight:320px; status: 0; help: 0; scroll: 0");
          if (arr != null) {
            s = "<table " + arr["TableAttrs"] + " border=" + arr["Border"] + " cellpadding=" + arr["CellPadding"] + " cellspacing=" + arr["CellSpacing"] + " width=" + arr["Width"] + ">";
            if(arr["Caption"]!="") s=s+"<caption>"+arr["Caption"]+"</caption>";
            for (i=1; i<=arr["NumRows"]; i++)
            {
                s = s + "<tr>";
                for (j=1; j<=arr["NumCols"]; j++)
                {
                    s = s + "<td " + arr["CellAttrs"] + "></td>";
                }
                s = s + "</tr>";
            }
            s = s + "</table>";
            oRange.pasteHTML(s);
          }
          document.getElementById("' . $this->name . 'contentcontent").focus();
          try
          {
            document.selection.createRange().scrollIntoView(true);
          }catch(e) { }
        }
      }


    function setforecolor'.$this->name.'(obj) {
      var arr = showModalDialog( "?module=wysiwyg&action=setcolor",
                                 "",
                                 "font-family:Verdana; font-size:12; dialogWidth:320px; dialogHeight:305px; help:0; status:0; scroll: 0" );
      if (arr != null) {
            if(document.getElementById("' . $this->name . 'contentcontent").contentEditable)
            {
              document.execCommand(obj, 0, arr);
            }
      }
      document.getElementById("' . $this->name . 'contentcontent").focus();
      try
      {
        document.selection.createRange().scrollIntoView(true);
      }catch(e) { }
    }

    function setbackcolor'.$this->name.'(obj) {
      var arr = showModalDialog( "?module=wysiwyg&action=setcolor",
                                 "",
                                 "font-family:Verdana; font-size:12; dialogWidth:320px; dialogHeight:305px; help:0; status:0; scroll: 0" );
      if (arr != null) {
            if(document.getElementById("' . $this->name . 'contentcontent").contentEditable)
            {
              document.execCommand(obj, 0, arr);
            }
      }
      document.getElementById("' . $this->name . 'contentcontent").focus();
      try
      {
        document.selection.createRange().scrollIntoView(true);
      }catch(e) { }
    }

    function isCursorInEditObject'.$this->name.'()
    {
      try {
        if(cnst.isFF)
        {
          var elem = window.getSelection().getRangeAt(0).commonAncestorContainer;
          if(elem.nodeName == "#text")
            elem = elem.parentNode;
        }else
        {
          var elem = document.selection.createRange().parentNode()
        }
        var found = false;
        while (elem != null)
        {
          if (elem != null)
          {
            if (elem.id == "'.$this->name.'contentcontent")
            {
              elem = null;
              found = true;
            }
            if (elem == null)
              break
            if(cnst.isFF)
            {
              elem = elem.parentNode
            }else
            {
              elem = elem.parentElement
            }
          }

  	  }
      return found

    } catch (e) {return true}
	} // End function

	function isTableSelected() {
		if(cnst.isFF)
    {
      return false;
    }else
    {
  		if (document.selection && document.selection.type == "Control") {
  			var oControlRange = document.selection.createRange();
  			if (oControlRange(0).tagName.toUpperCase() == "TABLE") {
  				window.selectedTable = document.selection.createRange()(0);
  				return true;
  			}
  		}
    }
	}

function getAdjacentElementA' . $this->name . '(e)
{
  var p = e.parentElement;
  var n;
  hasfound = false;
  if(p != "undefined")
  {
    for(n=0; n<p.childNodes.length; n++)
    {
      if(hasfound == true)
      {
        return p.childNodes[n];
      }
      if(p.childNodes[n] == e)
      {
        hasfound = true;
      }
    }
  }
  return null;
}


	function isCursorInTableCell'.$this->name.'() {
    if(cnst.isFF)
    {
      var oRange = window.getSelection().getRangeAt(0);
      var elem = oRange.startContainer;
      while(elem && !elem.tagName)
        elem = elem.parentNode;

      while (elem && elem.tagName && elem.tagName.toUpperCase() != "TD" && elem.tagName.toUpperCase() != "TH")
      {
          if (elem != null)
          {
            elem = elem.parentNode
            if (elem != null)
              {
                if (elem.id == "'.$this->name.'contentcontent")
                    elem = null
                if (elem == null)
                  break
              }
            else break
          }
          else break
      }
    }else
    {
		  if (document.selection && document.selection.type != "Control") {
        var elem = document.selection.createRange().parentElement()
        while (elem.tagName.toUpperCase() != "TD" && elem.tagName.toUpperCase() != "TH")
        {
            if (elem != null)
            {
              elem = elem.parentElement
              if (elem != null)
                {
                  if (elem.id == "'.$this->name.'contentcontent")
                      elem = null
                  if (elem == null)
                    break
                }
              else break
            }
            else break
        }
		  }
    }
    if (elem && elem.tagName) {
    	window.selectedTD = elem;
    	window.selectedTR = window.selectedTD.parentNode;
    	window.selectedTBODY =  window.selectedTR.parentNode;
    	window.selectedTable = window.selectedTBODY.parentNode;
    	return true;
    }
	} // End function

	function InsertRowBelow'.$this->name.'() {

		if (isCursorInTableCell'.$this->name.'()){

			var numCols = 0;

			allCells = window.selectedTR.cells;
			for (var i=0;i<allCells.length;i++) {
        var a = allCells[i].getAttribute("colSpan");
        if(a == null)
          a = 1;
			 	numCols = numCols + a;
			}

			var newTR = window.selectedTable.insertRow(window.selectedTR.rowIndex+1)
      var newStyleBorderStyle = window.selectedTD.style.borderRightStyle;
      var newStyleBorderWidth = window.selectedTD.style.borderRightWidth;
      var newStyleBorderColor = window.selectedTD.style.borderRightColor;

			for (i = 0; i != numCols; i++) {
			 	newTD = newTR.insertCell(-1)
				newTD.innerHTML = "&nbsp;"
        newTD.style.borderStyle = newStyleBorderStyle
        newTD.style.borderWidth = newStyleBorderWidth
        newTD.style.borderColor = newStyleBorderColor

				//if (borderShown == "yes") {
				//	newTD.runtimeStyle.border = "1px solid #BFBFBF"
				//}
			}
		}
        document.getElementById("' . $this->name . 'contentcontent").focus();
        try
        {
          document.selection.createRange().scrollIntoView(true);
        }catch(e) { }
	}

	function DeleteRow'.$this->name.'() {
		if (isCursorInTableCell'.$this->name.'()) {
			window.selectedTable.deleteRow(window.selectedTR.rowIndex)
		}
        document.getElementById("' . $this->name . 'contentcontent").focus();
        try
        {
          document.selection.createRange().scrollIntoView(true);
        }catch(e) { }
	}

	function InsertColBefore'.$this->name.'() {
        	if (isCursorInTableCell'.$this->name.'()) {
			moveFromEnd = (window.selectedTR.cells.length-1) - (window.selectedTD.cellIndex)
			allRows = window.selectedTable.rows
			for (i=0;i<allRows.length;i++) {
				rowCount = allRows[i].cells.length - 1
				position = rowCount - moveFromEnd
				//if (position < 0) {
				//	position = 0
				//}
     		newCell = allRows[i].insertCell(position)
				newCell.innerHTML = "&nbsp;"

				//if (borderShown == "yes") {
        if(typeof(newCell.runtimeStyle) != "undefined") {
					newCell.runtimeStyle.border = "1px solid #BFBFBF"
        }
				//}
			}
        	}
            document.getElementById("' . $this->name . 'contentcontent").focus();
            try
            {
              document.selection.createRange().scrollIntoView(true);
            }catch(e) { }
	}

	function DeleteCol'.$this->name.'() {
        	if (isCursorInTableCell'.$this->name.'()) {
			moveFromEnd = (window.selectedTR.cells.length-1) - (window.selectedTD.cellIndex)
			allRows = window.selectedTable.rows
			for (var i=0;i<allRows.length;i++) {
				endOfRow = allRows[i].cells.length - 1
				position = endOfRow - moveFromEnd
				if (position < 0) {
					position = 0
				} // End If


				allCellsInRow = allRows[i].cells

				if (allCellsInRow[position].colSpan > 1) {
					allCellsInRow[position].colSpan = allCellsInRow[position].colSpan - 1
				} else {
					allRows[i].deleteCell(position)
				}

			} // End For

        	} // End If
            document.getElementById("' . $this->name . 'contentcontent").focus();
            try
            {
              document.selection.createRange().scrollIntoView(true);
            }catch(e) { }
    }

    function InsertCell'.$this->name.'()
    {
        if (isCursorInTableCell'.$this->name.'()) {
  				var newCell = window.selectedTR.insertCell(window.selectedTD.cellIndex)
  				newCell.innerHTML = "&nbsp;"
          if(typeof(newCell.runtimeStyle) != "undefined")
  			    newCell.runtimeStyle.border = "1px solid #BFBFBF"
        }
        document.getElementById("' . $this->name . 'contentcontent").focus();
        try
        {
          document.selection.createRange().scrollIntoView(true);
        }catch(e) { }
    }

    function DeleteCell'.$this->name.'()
    {
        if (isCursorInTableCell'.$this->name.'()) {
            c = window.selectedTD.cellIndex;
            window.selectedTR.deleteCell(c);
        }
        document.getElementById("' . $this->name . 'contentcontent").focus();
        try
        {
          document.selection.createRange().scrollIntoView(true);
        }catch(e) { }
    }

	function IncreaseColspan'.$this->name.'() {
		if (isCursorInTableCell'.$this->name.'())
    {
			var colSpanTD = window.selectedTD.getAttribute("colSpan")
      if(colSpanTD == null)
        colSpanTD = 1;
			allCells = window.selectedTR.cells

			if (window.selectedTD.cellIndex + 1 != window.selectedTR.cells.length)
      { // If not last cell in row
				var addColspan = allCells[window.selectedTD.cellIndex+1].getAttribute("colSpan")
        if(addColspan == null)
          addColspan = 1;
				window.selectedTD.colSpan = parseInt(colSpanTD, 10) + parseInt(addColspan, 10)
				window.selectedTR.deleteCell(window.selectedTD.cellIndex+1)
			}
		}

    document.getElementById("' . $this->name . 'contentcontent").focus();
    try
    {
      document.selection.createRange().scrollIntoView(true);
    }catch(e) { }
  }

	function DecreaseColspan'.$this->name.'() {

		if (isCursorInTableCell'.$this->name.'()) {
			if (window.selectedTD.colSpan != 1) {
				var newCell = window.selectedTR.insertCell(window.selectedTD.cellIndex+1)
				window.selectedTD.colSpan = window.selectedTD.colSpan - 1
        newCell.innerHTML = "&nbsp;"
			}
		}
        document.getElementById("' . $this->name . 'contentcontent").focus();
        try
        {
          document.selection.createRange().scrollIntoView(true);
        }catch(e) { }

	} // End function

  function table_prop'.$this->name.'() {
        if (isCursorInTableCell'.$this->name.'() || isTableSelected()) {

            currTable = window.selectedTable;
          //currTable = getCurrTable();
          if (currTable!=null) {
            var arr = {};
            arr["TableWidth"] = currTable.getAttribute("width");
            arr["TableHeight"] = currTable.getAttribute("height");
            arr["CellPadding"] = currTable.getAttribute("cellPadding");
            arr["CellSpacing"] = currTable.getAttribute("cellSpacing");
            arr["BgColor"] = currTable.getAttribute("bgColor");
            arr["BorderWidth"] = currTable.getAttribute("border");
            arr["BorderColor"] = currTable.getAttribute("borderColor");
            arr["OBorderWidth"] = currTable.style.borderWidth;
            arr["OBorderColor"] = currTable.style.borderColor;
            arr["OBorderStyle"] = currTable.style.borderStyle;
            arr = showModalDialog( "?module=wysiwyg&action=tprop", arr, "font-family:Verdana; font-size:12; dialogWidth:350px; dialogHeight:480px; help:0; status:0; scroll: 0" );
            if (arr != null) {
              var oldBorderWidth = currTable.getAttribute("border");

              currTable.setAttribute("width", arr["TableWidth"]);
              currTable.setAttribute("height", arr["TableHeight"]);
              currTable.setAttribute("cellPadding", arr["CellPadding"]);
              currTable.setAttribute("cellSpacing", arr["CellSpacing"]);
              currTable.setAttribute("bgColor", arr["BgColor"]);
              currTable.setAttribute("border", arr["BorderWidth"]);
              currTable.setAttribute("borderColor", arr["BorderColor"]);
              currTable.style.borderWidth = arr["OBorderWidth"];
              currTable.style.borderColor = arr["OBorderColor"];
              currTable.style.borderStyle = arr["OBorderStyle"];
              //specially for ltb and maybe other projects as well
              //cycle through table cells and assingn border=1 to all
              if(oldBorderWidth != arr["BorderWidth"])
              {
                for(f1=0;f1<currTable.rows.length;f1++)
                {
                  for(f2=0;f2<currTable.rows(f1).cells.length;f2++)
                  {
                    currTable.rows(f1).cells(f2).style.borderWidth = arr["BorderWidth"];
                    //currTable.rows(f1).cells(f2).style.borderColor = arr["BorderColor"];
                    if(arr["BorderWidth"] > 0)
                      currTable.rows(f1).cells(f2).style.borderStyle = "solid";

                  }
                }
              }
            }
          }
        }
    }

    function cell_prop'.$this->name.'() {
        if (isCursorInTableCell'.$this->name.'()) {

            currTable = window.selectedTD;
          //currTable = getCurrTD();

          if (currTable!=null) {
            var arr = {};
            arr["TableWidth"] = currTable.getAttribute("width");
            arr["TableHeight"] = currTable.getAttribute("height");
            arr["BgColor"] = currTable.getAttribute("bgColor");
            arr["OBorderWidth"] = currTable.style.borderLeftWidth;
            arr["OBorderColor"] = currTable.style.borderLeftColor;
            arr["OBorderStyle"] = currTable.style.borderStyle;
            arr = showModalDialog( "?module=wysiwyg&action=cellprop", arr, "font-family:Verdana; font-size:12; dialogWidth:350px; dialogHeight:390px; help:0; status:0; scroll: 0" );
            if (arr != null) {
              currTable.setAttribute("width", arr["TableWidth"]);
              currTable.setAttribute("height", arr["TableHeight"]);
              currTable.setAttribute("bgColor", arr["BgColor"]);
              currTable.style.borderWidth = arr["OBorderWidth"];
              currTable.style.borderColor = arr["OBorderColor"];
              currTable.style.borderStyle = arr["OBorderStyle"];
            }
          }
        }
    }

    function row_prop'.$this->name.'() {
       if (isCursorInTableCell'.$this->name.'()) {
          currTable = window.selectedTR;
          //currTable = getCurrTR();
          if (currTable!=null) {
            var arr = {};
            arr["TableHeight"] = currTable.getAttribute("height");
            arr["BgColor"] = currTable.getAttribute("bgColor");
            arr = showModalDialog( "?module=wysiwyg&action=rowprop", arr, "font-family:Verdana; font-size:12; dialogWidth:350px; dialogHeight:235px; help:0; status:0; scroll: 0" );
            if (arr != null) {
              currTable.setAttribute("height", arr["TableHeight"]);
              currTable.setAttribute("bgColor", arr["BgColor"]);
            }
          }
       }
    }

    function sup' . $this->name . '() {
        if(document.getElementById("' . $this->name . 'contentcontent").contentEditable)
            {
              var arr, oSel;
              oSel = document.selection;
              var oRange = oSel.createRange();
              oRange.pasteHTML("<sup>" + oRange.text + "</sup>");
              document.getElementById("' . $this->name . 'contentcontent").focus();
              try
              {
                document.selection.createRange().scrollIntoView(true);
              }catch(e) { }
            }
    }

    function sub' . $this->name . '() {
        if(document.getElementById("' . $this->name . 'contentcontent").contentEditable)
            {
              var arr, oSel;
              oSel = document.selection;
              var oRange = oSel.createRange();
              oRange.pasteHTML("<sub>" + oRange.text + "</sub>");
              document.getElementById("' . $this->name . 'contentcontent").focus();
              try
              {
                document.selection.createRange().scrollIntoView(true);
              }catch(e) { }
            }
    }

// modified to browse server side files for making the link - vsb
function link'.$this->name.'() {
  if(isCursorInEditObject'.$this->name.'())
  {
    var r = selectforlinking'.$this->name.'();
    var oRange = r.oRange;
    var hrefelem = r.hrefelem;
    var hrefinnerhtml = r.hrefinnerhtml;
    var control = r.control;

    var arr=null;
    var args = {};
    // set a default value for your link button
    args["LinkUrl"] = "";
    args["LinkTarget"] = "_self";

    if(!cnst.isFF)
      var cmdEnable = oRange.queryCommandEnabled("Unlink");
    else
      var cmdEnable = (hrefelem && hrefelem.tagName && hrefelem.tagName.toLowerCase() == "a");
    if(cmdEnable){
      // do this only if a link is there
      var aLINK = oRange.htmlText.match(/<a.*href=[\'"]*([^"\' ]+)[\'"]*/i);
      var aTARGET = oRange.htmlText.match(/<a.*target=[\'"]*([a-z_]+)[\'"]*/i);
      args["LinkUrl"] = (aLINK != null?aLINK[1]:"");
      args["LinkTarget"] = (aTARGET != null?aTARGET[1]:null);
    }

      arr = showModalDialog( "?module=wysiwyg&action=hyperlink&site_id='.$this->site_id.'", args, "font-family:Verdana; font-size:12; dialogWidth:430px; dialogHeight:400px; help:0; status:0");
      if (arr != null){
        if(arr["LinkUrl"] != "")
        {
          if(control || cnst.isFF)
          {
            if(cnst.isFF)
              var text = hrefinnerhtml;
            else
              var text = oRange.htmlText;
            oRange.pasteHTML("<a hr"+"ef=\"" + arr["LinkUrl"] + "\"" + (arr["LinkTarget"] != "" ?  "target=\"" + arr["LinkTarget"] + "\"" : "") + ">" + text + "</a>");
          }else
          {
            oRange.pasteHTML("<a hr"+"ef=\"" + arr["LinkUrl"] + "\"" + (arr["LinkTarget"] != "" ?  "target=\"" + arr["LinkTarget"] + "\"" : "") + ">" + oRange.text + "</a>");

          }
        } else {
           // clear the selection of A tags.
           if(!cnst.isFF)
             oRange.pasteHTML( oRange.text );
           else
             oRange.pasteHTML( hrefinnerhtml );
        }
      }

    document.getElementById("' . $this->name . 'contentcontent").focus();
    try
    {
      document.selection.createRange().scrollIntoView(true);
    }catch(e) { }
    //tbContentElement.focus();
  }
}


function selectNodeContents(node, pos) {
	var range;
	var collapsed = (typeof pos != "undefined");

		range = document.body.createTextRange();
		range.moveToElementText(node);
		(collapsed) && range.collapse(pos);
		range.select();

}


function filelink'.$this->name.'() {
//  tbContentElement.ExecCommand(DECMD_IMAGE,OLECMDEXECOPT_PROMPTUSER);
//  tbContentElement.focus();
  if(isCursorInEditObject'.$this->name.'())
  {
    var r = selectforlinking'.$this->name.'();
    var oRange = r.oRange;
    var hrefelem = r.hrefelem;
    var hrefinnerhtml = r.hrefinnerhtml;
    var control = r.control;

    if(typeof(hrefelem) != "undefined")
      var urlarg = hrefelem;
    else
      var urlarg = "";

    var url = showModalDialog( "?module=dialogs&action=filex&site_id='.$this->site_id.'", urlarg, "font-family:Verdana; font-size:12; dialogWidth:530px; dialogHeight:370px; help:0; status:0");
    if (url != null){
      if(url != ""){
        if(control || cnst.isFF)
        {
          if(cnst.isFF)
            var text = hrefinnerhtml;
          else
            var text = oRange.htmlText;
          oRange.pasteHTML("<a hr"+"ef=\"" + url + "\"" + ">" + text + "</a>");
        }else
        {
          oRange.pasteHTML("<a hr"+"ef=\"" + url + "\"" + ">" + oRange.text + "</a>");

        }
      } else {
         // clear the selection of A tags.
         if(!cnst.isFF)
           oRange.pasteHTML( oRange.text );
         else
           oRange.pasteHTML( hrefinnerhtml );
      }
    }
    document.getElementById("' . $this->name . 'contentcontent").focus();
    try
    {
      document.selection.createRange().scrollIntoView(true);
    }catch(e) { }
    //tbContentElement.focus();
  }
}

function selectforlinking'.$this->name.'() {
  result = {};

  if(cnst.isFF)
  {
    var oRange = window.getSelection().getRangeAt(0);
    var elem = oRange.cloneContents();
    if(elem.firstChild != elem.lastChild)
    {
      elem = oRange.commonAncestorContainer;
      oRange.selectNode(elem);
    }else if(elem.firstChild.nodeName == "#text" && oRange.collapsed)
    {
      elem = oRange.commonAncestorContainer; //select the whole text
      oRange.selectNode(elem);
    }

    //is the element already linked
    var a = oRange.commonAncestorContainer;
    var hrefelem = null;
    var hrefinnerhtml = null;
    elem = null;
    if(a != null)
    {
      if (/^a$/i.test(a.tagName))
      {
        hrefelem = a;
        hrefinnerhtml = a.innerHTML;
        oRange.selectNode(hrefelem);
      }
    }

    elem = oRange.cloneContents();

    var el = document.createElement("div");
    el.appendChild(elem);
    elem.outerHTML = el.innerHTML;
    oRange.htmlText = elem.outerHTML;
    if(hrefinnerhtml == null)
      hrefinnerhtml = oRange.htmlText;
    oRange.pasteHTML = function(text)
    {
      document.execCommand("inserthtml", 0, text);
    }
  }else
  {
    var arr, args, oSel, oParent, sType;
    oSel = document.selection;
    sType=oSel.type;
    if(sType=="Control")
    {
      //image selected
      var cList = oSel.createRange();

      var oRange = document.body.createTextRange( );

      var cObj = cList.item(0);
      oRange.moveToElementText(cObj);
      var control = true;
    }else
    {
      var oRange = oSel.createRange();
      var control = false;
    }

    a = oRange.parentElement();

    if (a != null)
    {
        if (!/^a$/i.test(a.tagName))
            a = a.nextSibling;

        if (a != null)
        {
            if (/^a$/i.test(a.tagName))
            {
                    //a.target = param.f_target.trim();
                    //a.title = param.f_title.trim();
                    selectNodeContents(a);
                    sType=oSel.type;
                    oRange = oSel.createRange();
            }else
            {
              a = null;
            }
        }
    }

    if (a==null)
    {
        oRange.expand("word");
        oRange.select();
    }

  }

  result.oRange = oRange;
  result.hrefelem = hrefelem;
  result.hrefinnerhtml = hrefinnerhtml;
  result.control = control;
  return result;
}

function glink'.$this->name.'() {
  if(isCursorInEditObject'.$this->name.'())
  {
    var r = selectforlinking'.$this->name.'();
    var oRange = r.oRange;
    var hrefelem = r.hrefelem;
    var hrefinnerhtml = r.hrefinnerhtml;
    var control = r.control;

    arr=null;
    args= {};
    // set a default value for your link button
    args["LinkUrl"] = "";
    args["LinkTarget"] = "_self";
    args["LinkProtocol"] = "http://";

    if(!cnst.isFF)
      var cmdEnable = oRange.queryCommandEnabled("Unlink");
    else
      var cmdEnable = (hrefelem && hrefelem.tagName && hrefelem.tagName.toLowerCase() == "a");
    if(cmdEnable){
      // do this only if a link is there
      var aLINK = oRange.htmlText.match(/<a.*href=[\'"]*([^"\' ]+)[\'"]*/i);
      var aTARGET = oRange.htmlText.match(/<a.*target=[\'"]*([a-z_]+)[\'"]*/i);
      var aPROTOCOL = oRange.htmlText.match(/<a.*href=[\'"]*([a-z]+:\/\/)[\'"]*/i);
      args["LinkUrl"] = (aLINK != null?aLINK[1]:"");
      args["LinkTarget"] = (aTARGET != null?aTARGET[1]:null);
      args["LinkProtocol"] = (aPROTOCOL != null?aPROTOCOL[1]:null);
    }

    arr = showModalDialog( "?module=wysiwyg&action=ghyperlink&site_id='.$this->site_id.'", args, "font-family:Verdana; font-size:12; dialogWidth:430px; dialogHeight:140px; help:0; status:0");
    if (arr != null){
      if(arr["LinkUrl"] != ""){
        if(control || cnst.isFF)
        {
          if(cnst.isFF)
            var text = hrefinnerhtml;
          else
            var text = oRange.htmlText;
          oRange.pasteHTML("<a hr"+"ef=\"" + arr["LinkUrl"] + "\"" + (arr["LinkTarget"] != "" ?  "target=\"" + arr["LinkTarget"] + "\"" : "") + ">" + text + "</a>");
        }else
        {
          oRange.pasteHTML("<a hr"+"ef=\"" + arr["LinkUrl"] + "\"" + (arr["LinkTarget"] != "" ?  "target=\"" + arr["LinkTarget"] + "\"" : "") + ">" + oRange.text + "</a>");

        }
      } else {
         // clear the selection of A tags.
         if(!cnst.isFF)
           oRange.pasteHTML( oRange.text );
         else
           oRange.pasteHTML( hrefinnerhtml );
      }
    }

    document.getElementById("' . $this->name . 'contentcontent").focus();
    try
    {
      document.selection.createRange().scrollIntoView(true);
    }catch(e) { }
    //tbContentElement.focus();
  }
}

/*
function glink'.$this->name.'() {
  document.execCommand("CreateLink", 1, "");
  document.getElementById("' . $this->name . 'contentcontent").focus();
  try
  {
    document.selection.createRange().scrollIntoView(true);
  }catch(e) { }
}
*/

function addbookmark'.$this->name.'() {
  if(!cnst.isFF)
  {
    document.execCommand("CreateBookmark", "false", prompt("Enter bookmark name:", ""));
  }else
  {
    //doesn\'t work on FF
  }
  document.getElementById("' . $this->name . 'contentcontent").focus();
  try
  {
    document.selection.createRange().scrollIntoView(true);
  }catch(e) { }
}

function FindListStartEnd(text, level)
{
  var e = -1;
  var s = text.indexOf("{%%LI_CONSTRUCTOR "+level+"%%}");
  if(s != -1)
  {
    //list ends, when after closing tag does not follow opening tag
    var searchstring = text.substr(s, text.length);
    var reg = new RegExp("\{%%\/LI%%\}(?!\{%%LI_CONSTRUCTOR )");
    var i = searchstring.search(reg);
    e = s + i + 9; //9 is length of closing tag
    return new Array(s, e);
  }else
  {
    return 0;
  }
}

function RenameList(alltext, level)
{
  var tagstr = "{%%LI_CONSTRUCTOR "+level+"%%}";
  var ctagstr = "{%%/LI%%}";

  var a = FindListStartEnd(alltext, level);
  while(a)
  {
    var s = a[0];
    var e = a[1];
    var text = alltext.substring(s, e);

    //replace all opening and corresponding closing tags with <LI></LI>
    var i = text.indexOf(tagstr);
    while(i != -1)
    {
      text = text.substr(0, i) + "<li>" + text.substr(i + tagstr.length, text.length);
      var text1 = text.substr(i, text.length);
      var i1 = text1.indexOf(ctagstr);
      text = text.substr(0, i + i1) + "" + text.substr(i + i1 + ctagstr.length, text.length);
      text1 = text.substr(i, text.length);
      i1 = text1.indexOf(tagstr);
      if(i1 != -1)
      {
        text = text.substr(0, i + i1) + "</li>" + text.substr(i + i1, text.length);
      }else
      {
        text = text + "</li>";
      }
      i = text.indexOf(tagstr);
    }
    text = "<ul>" + text + "</ul>";

    //recursively replace other levels
    text = RenameList(text, level + 1);

    //replace text in alltext
    alltext = alltext.substr(0, s) + text + alltext.substr(e, alltext.length);

    a = FindListStartEnd(alltext, level);
  }

  return alltext;

}


var MainObj = null;


function ClearWordNodeAttributes(el)
{
  if(cnst.isFF)
   {
     for(var x = el.attributes.length - 1; x >= 0 ; x--)
     {
       if(el.attributes[x].nodeName.toLowerCase() != "id")
         el.removeAttribute(el.attributes[x].nodeName);
     }
   }else
   {
     el.clearAttributes();
   }
}


function removeAllComments(parentNode)
{
  var node = parentNode.firstChild;
  while(node)
  {
    var n = node;
    node = node.nextSibling;
    removeAllComments(n);
    if(n.nodeType == 8)
    {
      parentNode.removeChild(n);
    }
  }
}

function DeWord' . $this->name . '()
{





    //this is a bit tricky,
    //we got to get rid of fake objects first
    var origBody;
    var origMainOut;
    if(MainObj != null)
    {
      origMainOut = MainObj.outerHTML;
      origBody = tbContentElement.DOM.body.innerHTML;
      document.body.innerHTML = MainObj.innerHTML;
    }

        //remove non standard HTML tags
        if(cnst.isFF)
          var elemlist = document.getElementById("' . $this->name . 'contentcontent").getElementsByTagName("*");
        else
          var elemlist = document.body.all;
        for(i=elemlist.length-1; i>=0; i--)
        {
          if(cnst.isFF)
          {
            var match = /^(a|abbr|acronym|address|area|b|base|bdo|big|blockquote|body|br|button|caption|cite|code|col|colgroup|dd|del|dfn|div|dl|dt|em|fieldset|form|h1|h2|h3|h4|h5|h6|html|hr|i|img|input|ins|kbd|label|legend|li|map|noscript|object|ol|optgroup|option|p|param|pre|q|samp|script|select|small|span|strong|sub|sup|table|tbody|td|textarea|tfoot|th|thead|title|tr|tt|ul|var)$/i.test(elemlist[i].tagName);
            if(!match)
              elemlist[i].removeNode(true);
          }else
          {
            if(elemlist[i].scopeName!="HTML")
              document.body.all[i].removeNode();
          }
        }

        b=document.getElementById("' . $this->name . 'contentcontent");

        if(cnst.isFF)
          removeAllComments(b);

        f=b.getElementsByTagName("COLGROUP");
        if(f.length+""!="undefined")
          for(i=f.length-1;i>=0;i--)
            f[i].removeNode();

        f=b.getElementsByTagName("TH");
        if(f.length+""!="undefined")
          for(i=f.length-1;i>=0;i--)
            f[i].removeNode();

        f=b.getElementsByTagName("COL");
        if(f.length+""!="undefined")
          for(i=f.length-1;i>=0;i--)
            f[i].removeNode();

        f=b.getElementsByTagName("SPAN");
        if(f.length+""!="undefined")
          for(i=f.length-1;i>=0;i--)
          {
     //       f[i].removeAttribute("class");
            f[i].removeNode();
          }


        f=b.getElementsByTagName("FONT");
        if(f.length+""!="undefined")
          for(i=f.length-1;i>=0;i--)
            f[i].removeNode();

        f=b.getElementsByTagName("H1");
        if(f.length+""!="undefined")
          for(i=f.length-1;i>=0;i--)
   {
     var sPreserve = f[i].innerHTML;
     var oNewNode = document.createElement("B");
     f[i].replaceNode(oNewNode);
     oNewNode.innerHTML = sPreserve;
   }
        f=b.getElementsByTagName("H2");
        if(f.length+""!="undefined")
          for(i=f.length-1;i>=0;i--)
   {
     var sPreserve = f[i].innerHTML;
     var oNewNode = document.createElement("B");
     f[i].replaceNode(oNewNode);
     oNewNode.innerHTML = sPreserve;
   }
        f=b.getElementsByTagName("H3");
        if(f.length+""!="undefined")
          for(i=f.length-1;i>=0;i--)
   {
     var sPreserve = f[i].innerHTML;
     var oNewNode = document.createElement("B");
     f[i].replaceNode(oNewNode);
     oNewNode.innerHTML = sPreserve;
   }
        f=b.getElementsByTagName("H4");
        if(f.length+""!="undefined")
          for(i=f.length-1;i>=0;i--)
   {
     var sPreserve = f[i].innerHTML;
     var oNewNode = document.createElement("B");
     f[i].replaceNode(oNewNode);
     oNewNode.innerHTML = sPreserve;
   }
        f=b.getElementsByTagName("H5");
        if(f.length+""!="undefined")
          for(i=f.length-1;i>=0;i--)
   {
     var sPreserve = f[i].innerHTML;
     var oNewNode = document.createElement("B");
     f[i].replaceNode(oNewNode);
     oNewNode.innerHTML = sPreserve;
   }

        f=b.getElementsByTagName("H6");
        if(f.length+""!="undefined")
                 for(i=f.length-1;i>=0;i--)
   {
     var sPreserve = f[i].innerHTML;
     var oNewNode = document.createElement("B");
     f[i].replaceNode(oNewNode);
     oNewNode.innerHTML = sPreserve;
   }

        f=b.getElementsByTagName("PRE");
        if(f.length+""!="undefined")
          for(i=f.length-1;i>=0;i--)
   {
     var sPreserve = f[i].innerHTML;
     var oNewNode = document.createElement("PRE");
     f[i].replaceNode(oNewNode);
     oNewNode.innerHTML = sPreserve;
   }

        f=b.getElementsByTagName("DIV");
        if(f.length+""!="undefined")
                for(i=f.length-1;i>=0;i--)
                        f[i].removeNode();


        f=b.getElementsByTagName("A");
        if(f.length+""!="undefined")
        for(i=f.length-1;i>=0;i--)
        {
                        f[i].removeAttribute("onmouseover");
                        f[i].removeAttribute("onmouseout");
        }


        level = 0;
        f=b.getElementsByTagName("P");
        nexttaginsert = "";
        if(f.length+""!="undefined")
         for(i=f.length-1;i>=0;i--)
         {
           //we go through tags from end to beginning

           j = f[i].style.cssText.search(/; mso-list: l[0-9] level/i);
           if(j != -1)
           {
             l = f[i].style.cssText.substr(j+20, 1) * 1;
             bt = f[i].innerHTML;
             if(bt.substr(1, 37) == "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ")
               bt = bt.substr(38, bt.length);
             if(bt.substr(1, 43) == "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ")
               bt = bt.substr(44, bt.length);
             if(bt.substr(1, 49) == "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ")
               bt = bt.substr(50, bt.length);
             if(l == level)
             {

               //f[i].outerHTML = "<li>" + bt + "</li>";
               f[i].outerHTML = "{%%LI_CONSTRUCTOR "+l+"%%}" + bt + "{%%/LI%%}";
             }
             if(l > level)
             {
               //f[i].outerHTML = "<li>" + bt + "</li>" + "{%%% Constructor TAGChange: _/ul_ %%}";
               f[i].outerHTML = "{%%LI_CONSTRUCTOR "+l+"%%}" + bt + "{%%/LI%%}";
             }
             if(l < level)
             {
             /*  var oNewItem = document.createElement("LI");
               f[i].insertAdjacentElement("afterEnd", oNewItem);
               oNewItem.outerHTML = "{%%% Constructor TAGChange: _ul_ %%}";
               //find ending tag position, insert the whole bunch in <ul></ul>
               j = b.innerHTML.indexOf("{%%% Constructor TAGChange: _/ul_ %%}");
               if(j != -1)
               {
                 j1 = b.innerHTML.indexOf("{%%% Constructor TAGChange: _ul_ %%}");
                 if(j1 != -1)
                 {
                   intags = b.innerHTML.substring(j1 + 37, j);
                   nexttaginsert = "<ul>" + intags + "</ul>";
                   s = b.innerHTML.substring(0, j1) + b.innerHTML.substring(j + 37, b.innerHTML.length);
                   b.innerHTML = s;
                 }
               }else
               {
                 j1 = b.innerHTML.indexOf("{%%% Constructor TAGChange: _ul_ %%}");
                 if(j1 != -1)
                 {
                   //cut out tagchange thingy
                   s = b.innerHTML.substring(0, j1) + b.innerHTML.substring(j1 + 36, b.innerHTML.length);
                   b.innerHTML = s;
                 }
               } */
               //f[i].outerHTML = "<li>" + bt + nexttaginsert + "</li>";
               f[i].outerHTML = "{%%LI_CONSTRUCTOR "+l+"%%}" + bt + nexttaginsert + "{%%/LI%%}";
               nexttaginsert = "";
             }
             level = l;
           }else
           {
             var el = f[i];
             ClearWordNodeAttributes(el);
           }
         }

       b.innerHTML = RenameList(b.innerHTML, 1);

   //fix LI elements that dont have UL
   /*    f=b.getElementsByTagName("LI");
       if(f.length+""!="undefined")
         for(i=f.length-1;i>=0;i--)
         {
           if(f[i].parentElement.tagName != "UL")
           {
             createnew = true;
             e = getAdjacentElementA' . $this->name . '(f[i]);
//             e = null;
             if(e != null)
             {
               if(e.tagName == "UL")
               {
                 e.insertAdjacentElement("afterBegin", f[i]);
                 createnew = false;
               }
             }

             if(createnew)
             {
               //insert new list
               var oParentNode = document.createElement("UL");
               var oNode = document.createElement("B");
               f[i].insertAdjacentElement("beforeBegin", oParentNode);
               oParentNode.appendChild(f[i]);
             }
           }
         }
       */

        f=b.getElementsByTagName("LI");
        if(f.length+""!="undefined")
          for(i=f.length-1;i>=0;i--)
   {
     var sPreserve = f[i].innerHTML;
     var oNewNode = document.createElement("LI");
     f[i].replaceNode(oNewNode);
     oNewNode.innerHTML = sPreserve;
   }

f=b.getElementsByTagName("UL");
        if(f.length+""!="undefined")
          for(i=f.length-1;i>=0;i--)
   {
     var sPreserve = f[i].innerHTML;
     var oNewNode = document.createElement("UL");
     f[i].replaceNode(oNewNode);
     oNewNode.innerHTML = sPreserve;
   }

 //Tables
        f=b.getElementsByTagName("TR");
        if(f.length+""!="undefined")
   for(i=f.length-1;i>=0;i--)
   {
     ClearWordNodeAttributes(f[i]);
   }


         f=b.getElementsByTagName("TD");
         if(f.length+""!="undefined")
                for(i=f.length-1;i>=0;i--)
  {
    var aParams = f[i].style.cssText.split(";");
    oldColor="";
    for (var z=0; z < aParams.length; z++)
    {
      aType = aParams[z].split(": ");
      if (aType[0]==" BACKGROUND-COLOR")
        if (aType[1]!="transparent")
          oldColor = aType[1];
      if (aType[0]==" BACKGROUND")
        if (aType[1]!="transparent")
          oldColor = aType[1];

    }
    ClearWordNodeAttributes(f[i]);
    if (oldColor)
    {
      f[i].style.backgroundColor = oldColor;
    }
    f[i].className ="wordTD";
                }


  f=b.getElementsByTagName("TABLE");
  if(f.length+""!="undefined")
    for(i=f.length-1;i>=0;i--)
    {
      if (f[i].className != "wordTable" && f[i].className != "wordTableOuter")
      {
        ClearWordNodeAttributes(f[i]);
        f[i].className ="wordTable";
        f[i].setAttribute("width","100%");
        f[i].setAttribute("cellSpacing","1");
        f[i].setAttribute("cellPadding","2");

        var oNewTable = document.createElement("TABLE");
        var oNewTr    = document.createElement("TR");
        var oNewTd    = document.createElement("TD");

        code = f[i].outerHTML;
        oNewTr.appendChild(oNewTd);
        oNewTable.appendChild(oNewTr);
        oNewTd.innerHTML= code;
        f[i].replaceNode(oNewTable);
        f[i].outerHTML = oNewTable.outerHTML;

        oNewTable.setAttribute("cellPadding", "0");
        oNewTable.setAttribute("cellSpacing", "0");
        oNewTable.setAttribute("border", "0");
        oNewTable.setAttribute("width", "100%");
        oNewTable.className ="wordTableOuter";
        oNewTd.innerHTML= code;

      }
    }


    //restore the fake objects
    if(MainObj != null)
    {
      RestoreMainObj(origBody);
    }


}

          ';

    $onclick = 'clickedit'.$this->name.'();';
    $toolbars[] = $tlb_3->outputHTML();
    $toolbars[] = $tlb->outputHTML();
    //$toolbars[] = $tlb_2->outputHTML();
    $btnids = array_merge($tlb_3->getButtonIdList(),$tlb->getButtonIdList(), $tlb_2->getButtonIdList());

    return True;
  }

  function toolbar(&$script, &$toolbars, &$btnids, &$onclick){
    return $this->toolbarCKeditor($script, $toolbars, $btnids, $onclick);

  }

  function CanLoadDesignDynamically()
  {
    return true;
  }

  function CanBeCompiled()
  {
    return True;
  }

  function SetProperties()
  {
    $this->properties = Array(

      "name"        => Array(
        "label"     => "Name:",
        "type"      => "string"
      ),

      "text"        => Array(
        "label"     => "{%langstr:html_text%}:",
        "type"      => "customtext",
        "rows"      => "6",
        "cols"      => "30",
        "dialog"    => "?module=wysiwyg&canpreview='+((window.parent.dialogArguments)?1:0)+'&site_id=".$GLOBALS['site_id']."&page_id=".$this->page_id,
        "dialogw"   => "720",
        "dialogh"   => "450",
        "dlg_res"   => true,
        "search"    => true
      ),

      "enterhandling"    => Array(
        "label"     => "{%langstr:newline_handling%}:",
        "type"      => "list",
        "lookup"    => Array("0:New paragraph", "1:Single line break")
      ),

      "editorcssmode"    => Array(
        "label"     => "FCK editor styles:",
        "type"      => "list",
        "lookup"    => Array("0:Use CSS & custom styles", "1:Copy by JavaScript")
      ),

      "customfckcssfile"    => Array(
        "label"     => "FCK editor css file:",
        "type"      => "str",
        "value"      => "/scr/style.css"
      ),

      "customfckstyles"    => Array(
        "label"     => "Custom FCK editor styles:",
        "type"      => "customtext",
        "rows"      => "8",
        "cols"      => "80",
      ),

      "savemode"    => Array(
        "label"     => "Save text:",
        "type"      => "list",
        "lookup"    => Array("0:To edited page", "1:As language default values", "2:As default values")
      ),

      "customformat"    => Array(
        "label"     => "Custom format:",
        "type"      => "customtext",
        "rows"      => "8",
        "cols"      => "80",
      ),

      "customdesignformat"    => Array(
        "label"     => "Custom design format:",
        "type"      => "customtext",
        "rows"      => "8",
        "cols"      => "80",
      ),

    );
  }

}
