<?
// atbalsta arii veikala kategoriju izvadu

require_once("class.component.php");

class sitemapnew extends component
{
  function sitemapnew($name)
  {
    parent::__construct($name);
    $this->SetProperties();
  }

  function getLanguageStrings()
  {
    return array(
      'katalogs' => 'Katalogs'
    );
  }

  function output()
  {
    $this->shopman = $this->getServiceByName('shopmanager', false);
//    $this->shop_base_url = $this->shopman ? $this->shopman->getShopBaseURL($this->lang) : '';

    $this->outputBranch(intval($this->getProperty('parent')));
  }

  function outputBranch($parent_id, $level=0)
  {
    $data = sqlQueryData("SELECT * FROM " . $this->site_id . "_pages WHERE parent='".$parent_id."' AND enabled=1 AND visible=1 ORDER BY ind");
    if (!$data) return;

    echo $level == 0 ? '<ul class="sitemap">' : '<ul>';

    foreach ($data as $row)
    {
      $url = '/'.PagePathById($row['page_id'], $this->site_id);

      echo '<li><a href="'.$url.'">'.$row['title'].'</a>';

      $this->outputBranch($row['page_id'], $level+1);

      echo '</li>';
    }

    if ($level == 0 && $this->shopman)
    {
      list($tree, $ndata) = $this->shopman->getCatTree();

      echo '<li><a href="'.$this->shopman->getShopBaseURL($this->lang).'">'.$this->LS('katalogs').'</a>';

      $this->outputShopCategories($tree);

      echo '</li>';

    }

    echo '</ul>';
  }

  function outputShopCategories(&$tree, $active, $level=0)
  {
    $level++;
    echo '<ul>';

    foreach ($tree as $row)
    {
      if (!strlen($row['title_' . $this->lang])) continue;

      echo '<li'.($row['is_open']?' class="active"':'').'>';

      echo '<a href="'.$this->shopman->getCategoryURL($row['item_id']).'">' . $row['title_' . $this->lang] . '</a>';

      if ($row['children'])
      {
        $this->outputShopCategories($row['children'], $active, $level);
      }

      echo '</li>';

    }
    echo '</ul>';
  }

  function SetProperties()
  {
    if ($GLOBALS['from_backend'])
    {
      $pages = sqlQueryData("SELECT page_id,name,parent,title FROM " . $this->site_id . "_pages ORDER BY ind");
      ListNodes($pages, $combopages, 0, 0);
    }
    else
      $combopages = '';


    $this->properties = Array(

      "name"        => Array(
        "type"      => "string",
        "label"     => "Name:"
      ),

      "parent"      => Array(
        "type"      => "list",
        "label"     => "Parent:",
        "lookup"    => $combopages
      ),


    );
  }

}

?>