<?
// Serveris.LV Constructor component
// Created by Aivars Irmejs, 2006, aivars@serveris.lv
// Modified for project E-Veikals by Kristaps Taube, 2014, ktaube@datateks.lv

include_once("class.component.php");
require_once($GLOBALS['cfgDirRoot']."library/mPDF/mpdf.php");

use Constructor\WebApp;

class shoporderform extends component{

  var $parent;
  var $normalclass;
  var $normalstyle;
  var $normalbullet;
  var $selectedclass;
  var $selectedstyle;
  var $selectedbullet;
  var $orderreciever;
  var $catwebpath;

  //Class initialization
  function shoporderform($name){

    parent::__construct($name);

    $this->SetProperties();
    $this->lvsay = array(
     'nulle','viens','divi','trīs','četri','pieci','seši','septiņi','astoņi','deviņi','desmit',
     'vienpadsmit','divpadsmit','trīspadsmit','četrpadsmit','piecpadsmit','sešpadsmit',
      'septiņpadsmit','astoņpadsmit','deviņpadsmit',
     'divdesmit',30=>'trīsdesmit',40=>'četrdesmit',50=>'piecdesmit',60=>'sešdesmit',
       70=>'septiņdesmit',80=>'astoņdesmit',90=>'deviņdesmit',
     '100'=>'simts','100x'=>'simti','1000'=>'tūkstotis','1000x'=>'tūkstoši',
     '1000000'=>'miljons','1000000x'=>'miljoni','1000000000'=>'miljards','1000000000x'=>'miljardi',
     'ls'=>'lats','lsx'=>'lati','snt'=>'santīms','sntx'=>'santīmi','snt0'=>'santīmu'
    );

    $this->initOptions();

  }

  function visibleService(){
    return true;
  }

  static function processOptionBeforeDisplay($path, $opt, &$data){

    switch ($path)
    {
      case 'Shop\\OrderForm\\delivery_pvn_method':
        $data['lookupassoc'] = Array('included' => 'Already included', 'add' => 'Add');
        break;

      case 'Shop\\OrderForm\\orderlogo':
        $data['dialog'] = '?module=dialogs&action=filex&site_id='.$GLOBALS['site_id'].'&view=thumbs';
        break;

      case 'Shop\\OrderForm\\order_lang':
        $langs = Array();
        $list = sqlQueryData("SELECT shortname, fullname FROM `".$GLOBALS['site_id']."_languages`");
        foreach ($list as $row)
        {
          $langs[$row['shortname']] = $row['fullname'];
        }
        $data['lookupassoc'] = $langs;
        break;

    }
  }

  function initOptions(){

    if ($this->opts_initialized) return;

    option('Shop\\OrderForm', '', 'Order form');
    $optcat = getOptionByPath('Shop\\OrderForm');
    $params = Array('parent_id' => $optcat['id']);

    $this->delivery_pvn_method = option('delivery_pvn_method', null, 'Delivery PVN calculation method', array_merge($params, Array('type' => 'list', 'componenttype' => get_class($this) )), 'add');

    $this->orderpath = option('orderpath', null, 'Order path', array_merge($params, Array('is_advanced' => 1)) );
    $this->rekini_save_path = option('rekini_save_path', 'files/rekini/', 'Orders save path', array_merge($params, Array('is_advanced' => 1)), 'order');

    $this->orderreciever = $this->orderemail = option('orderemail', null, 'Send order to e-mail(seperated by ,)', $params, 'constructor@datateks.lv');
    $this->emailfrom_email = option('emailfrom_email', null, 'Send order from e-mail', $params, 'constructor@datateks.lv');
    $this->emailfrom_name = option('emailfrom_name', null, 'Send order from name', $params, 'Constructor');
    $this->order_prefix = option('order_prefix', null, 'Order prefix', $params, 'I-NET');
    $this->mail_subject = option('mail_subject', null, 'Mail subject', $params, 'Rekins');
    $this->orderlogo = option('orderlogo', null, 'Order logo link', array_merge($params, Array('type' => 'dialog', 'componenttype' => get_class($this) )) );
    $this->order_lang = option('order_lang', null, 'Order language for owner', array_merge($params, Array('type' => 'list', 'componenttype' => get_class($this) )), 'lv');
    $this->send_at_checkout = option('send_at_checkout', null, 'Send order at checkout', array_merge($params, Array('type' => 'check')), 'on');

    $this->pvn = option('Shop\\pvn', null, 'PVN (%)', null, 22) / 100;
    $this->catwebpath = option('Shop\\catwebpath', null, 'Catalog path', Array('is_advanced' => 1), 'katalogs');

    $this->use_soc_discount = option('Shop\\use_soc_discount', null, 'Use Soc. discount', Array("is_advanced" => true,'type' => 'check'));
    $this->soc_discount = option("Shop\\soc_discount", NULL, "Soc. discount", array("is_advanced" => true), 10, $this->site_id);

    $this->use_promo_codes = option('Shop\\use_promo_codes', null, 'Use Promo codes', Array("is_advanced" => true, 'type' => 'check'));

    $this->use_vat_validator = option('use_vat_validator', null, 'Use EU VAT validator', array_merge($params, Array('type' => 'check','is_advanced'=>true)), null);

    $this->use_automatic_prod_stock = option('Shop\\use_automatic_prod_stock', null, 'Use automatic product stock', Array("is_advanced" => true,'type' => 'check'));

    foreach(getLanguages() as $short => $lang_data){
        option('confirmation_mail_subject_'.$short, null, 'Apmaksas apstiprinājuma vēstules tēma('.$short.')', $params, 'Rēķina apmaksa saņemta');
        option(
            "confirmation_mail_text_".$short,
            null,
            "Apmaksas apstiprinājuma vēstule(".$short.")",
            array_merge($params,array(
                "type" => "wysiwyg",
                "value_on_create" => "Labdien,<br />Esam saņēmuši apmaksu par rēķinu ar numuru [ORDER_ID]. Rēķins pievienots vēstulei.<br /><br />Ar cieņu,<br />Administrācija",
            ))
        );

        option(
            "Shop\\service_address_".$short,
            null,
            "Pakalpojuma sniegšanas adrese(".$short.")",
            array(
                "type" => "wysiwyg",
                "value_on_create" => "Pakalpojumu sniegšanas adrese ir Cēsu 3, Rīga, LV1012",
            )
        );
    }

    $this->service_address = readOption("Shop\\service_address_".$this->lang);

    $this->opts_initialized = true;
  }

  //========================================================================//
  // Language strings
  //========================================================================//
  function InitLanguageStrings()
  {

    $this->lvsay['ls'] = $this->CurrencyMenu->default['ls'];
    $this->lvsay['lsx'] = $this->CurrencyMenu->default['lsx'];
    $this->lvsay['snt'] = $this->CurrencyMenu->default['snt'];
    $this->lvsay['sntx'] = $this->CurrencyMenu->default['sntx'];
    $this->lvsay['snt0'] = $this->CurrencyMenu->default['snt0'];

  }

  function getLanguageStrings()
  {
    $ls = Array(
      'pasutijumsnosutits' => 'Pasūtījums nosūtīts',
      'regoties' => 'Reģistrēties',
      'piegadesdati' => 'Piegādes dati',
      'piegade' => 'Piegāde',
      'kopa' => 'Kopā',
      'atlaide' => 'Lietotāja atlaide',
      'lietotajs' => 'Reģistrēts lietotājs',
      'valoda' => 'Valoda',
      'vardsuzvards' => 'Vārds, Uzvārds',
      'telefons' => 'Telefons',
      'epasts' => 'E-pasts',
      'piegadesadrese' => 'Piegādes adrese',
      'komentari' => 'Komentāri',
      'pirkt' => 'Pirkt',
      'grozaprecunav' => 'Grozā preču nav',
      'ludzunoradietvardu' => 'Lūdzu norādiet savu vārdu un uzvārdu!',
      'ludzunoradiettelefonu' => 'Lūdzu norādiet savu telefona numuru!',
      'ludzunoradietepastu' => 'Lūdzu norādiet savu e-pasta adresi!',
      'ludzunoradietadresi' => 'Lūdzu norādiet piegādes adresi!',
      '_turpinat' => 'Turpināt',
      '_lietotajvards' => 'Lietotājvārds',
      '_parole' => 'Parole',
      'maksatajs' => 'Maksātājs',
      'personaskods' => 'Personas kods',
      'piegadesadrese' => 'Piegādes adrese',
      'pilseta' => 'Pilsēta',
      'rajons' => 'Rajons',
      'pastaindekss' => 'Pasta indekss',
      'fakss' => 'Fakss',
      'nosaukums' => 'Nosaukums',
      'regnr' => 'Reģ nr.',
      'pvnregnr' => 'PVN reģ nr.',
      'pvnregnr_full' => 'PVN maksātāja reģistrācijas numurs',
      'juridadrese' => 'Jurid. adrese',
      'kontaktvardsuzvards' => 'Kontaktpersonas<br />vārds, uzvārds',
      'kontakttelefons' => 'Kontaktpersonas<br />telefons',
      'kontaktepasts' => 'Kontaktpersonas<br />e-pasts',
      'aizpildietlauku' => 'Lūdzu aizpildiet lauku',
      'paldiesparpirkumu' => 'Paldies par pirkumu!',
      'spiedietlaireg' => 'Lai Jums nebūtu jāievada pircēja dati katru reizi no jauna,',
      'spiedietseitlai' => 'spiediet šeit, lai reģistrētos!',
      'neveiksmigspieslegsanas' => 'Neveiksmīgs pieslēgšanās mēģinājums!',
      'meginietvelreiz' => 'Mēģiniet vēlreiz',
      'produktanosaukums' => 'Produkta nosaukums',
      '_cena' => 'Cena',
      '_skaits' => 'Skaits',
      'pirkt' => 'Pirkt',
      'juridiskapersona' => 'Juridiska persona',
      'fiziskapersona' => 'Fiziska persona',
      '_lietotajvards' => 'Lietotājvārds',
      '_parole' => 'Parole',
      'esosajiemklientiem' => 'Reģistrētiem lietotājiem',
      'piesledzietiesseit' => 'Ja esat reģistrējies kā mūsu klients, lūdzu, pieslēdzieties šeit',
      'janiemklientiem' => 'Nereģistrētiem lietotājiem',
      'piesledzieska' => 'Pieslēdzies kā',
      'piegadesveids' => 'Piegādes veids',
      'patsbraucpakal' => 'Pircējs ierodas uz firmas ofisu Ģertrūdes ielā 34, Rīga',
      'tabula_pats_atbrauks' => 'Pircējs ierodas uz firmas ofisu Ģertrūdes ielā 34, Rīga',
      'kurjerspiegada' => 'Preci piegādā kurjers',
      'tabula_kurjers' => 'Preci piegādā kurjers',
      'value_precuiesniedzejs' => '"Firma SIA"',
      'value_registracijasnumurs' => '400000000123',
      'value_juridiskaadrese' => 'Kr. Barona iela 129, Rīga, LV-1012',
      'value_bankasnosaukums' => 'Hansabanka',
      'value_bankaskods' => 'HABALV22',
      'value_talrunis' => '6777 0077',
      'value_fakss' => '6000 7700',
      'precuiesniedzejs' => 'Preču izsniedzējs',
      'registracijasnumurs' => 'Reģistrācijas numurs',
      'juridiskaadrese' => 'Juridiskā adrese',
      'bankasnosaukums' => 'Bankas nosaukums',
      'bankaskods' => 'Bankas kods',
      'precuizsniegsanasadrese' => 'Preču izsniegšanas adrese',
      'kontanr' => 'Konta Nr',
      'talrunis' => 'Tālrunis',
      'fakss' => 'Fakss',
      'precu_sanemejs' => 'Preču saņēmējs',
      'interneta_veikala_lietotajvards' => 'Interneta veikala lietotājvārds',
      'klienta_talrunis' => 'Klienta tālrunis',
      'klienta_nr' => 'Klienta nr.',
      'klienta_fakss' => 'Klienta fakss',
      'klienta_juridiska_adrese' => 'Juridiskā adrese',
      'klienta_bankas_nosaukums' => 'Bankas nosaukums',
      'klienta_bankas_kods' => 'Bankas kods',
      'klienta_sanemshanas_adrese' => 'Preču saņemšanas adrese',
      'klienta_konta_nr' => 'Konta Nr.',
      'klienta_papildus_info' => 'Papildus info',
      'apmaksas_veids' => 'Apmaksas veids',
      'tabula_nr' => 'Nr.',
      'tabula_nosaukums' => 'Preces nosaukums',
      'tabula_daudzums' => 'Daudzums (gab.)',
      'tabula_summalvl' => 'Summa (ar PVN)',
      'tabula_pvn' => 'PVN likme',
      'tabula_pvn_summa' => 'PVN summa',
      'tabula_atlaide' => 'Atlaide (%)',
      'tabula_cena' => 'Cena ar PVN',
      'tabula_fakss' => 'Fakss',
      'tabula_rajons' => 'Rajons',
      'tabula_piegadesveids' => 'Piegādes veids',
      'tabula_patsbraucpakal' => 'Pircējs ierodas uz firmas ofisu Ģertrūdes ielā 34, Rīga',
      'tabula_komentari' => 'Komentāri',
      'kopa_atlaides_kopa' => 'Atlaižu summa',
      'kopa_atlaides_summa' => 'Summa',
      'kopa_pvn' => 'Pievienotās vērtības nodoklis',
      'kopa_summa_samaksai' => 'Summa apmaksai',
      'kopa_summa_vardiem' => 'Summa vārdiem',
      'ludzam_steidzami_apmaksat' => 'Lūdzam rēķinu apmaksāt 3 darba dienu laikā!',
      'ar_cienu' => 'Ar cieņu, Jūsu <u><a href="wwww.firma.lv" target="_blank" title="www.firma.lv">www.firma.lv</a></u>',
      'epasutijumsnr' => 'E-PASŪTĪJUMS Nr',
      'bankaskonts' => 'Bankas konts',
      'klienta_reg_numurs' => 'Reģistrācijas numurs',
      'klienta_piegades_adrese' => 'Piegādes adrese',
      'klienta_email' => 'Klienta e-pasta adrese',
      'value_bankaskonts' => 'LV52HABA0551005111951',
      'value_precuizsniegsanasadrese' => 'A. Deglava iela 12, Rīga, LV-1032',
      'summa_bez_atlaidem' => 'Summa bez atlaidēm',
      'atlaizhu_summa' => 'Atlaides summa',
      'tabula_pvnlikme' => 'PVN likme',
      'tabula_pvnsumma' => 'PVN summa',
      'klienta_vards_uzvards' => 'Vārds, Uzvārds',
      'klienta_personas_kods' => 'Personas kods',
      'menesu_nosaukumi' => 'janvāris,februāris,marts,aprīlis,maijs,jūnijs,jūlijs,augusts,septembris,oktobris,novembris,decembris',
      'kurjeraizmaksas' => 'Kurjera izmaksas',
      'apm_cash' => 'Skaidrā naudā veikalā',
      'apm_transfer' => 'Ar pārskaitījumu',
      'apm_airpay' => 'AirPay - ērti un droši maksājumi internetā',
      'varat_ar_karti' => 'Atveriet šo saiti, lai apmaksātu rēķinu ar karti',
      'varat_ar_els' => 'Atveriet šo saiti, lai apmaksātu rēķinu ar E-LS sistēmu vai norēķinu karti',
      'iesk_pvn' => 'iesk. PVN'
    );
    return $ls;
  }

  function GetFields(){

    $f_rajons = null;
    $j_rajons = null;

    $rajoni = Array(0 => $this->l("Izvēlieties piegādes rajonu..."));
    foreach($this->regions->getDBData() as $rajons){
      $vat_text = ($this->delivery_pvn_method == 'add')? "+ ".round($this->pvn*100)."% ".$this->l("PVN") : $this->l("Ieskaitot PVN");
      $rajoni[$rajons['item_id']] = $rajons['name_' . $this->lang]." (".$_SESSION['currency']['label']." ".number_format($rajons['price'] / $_SESSION['currency']['rate'], 2)." ".$vat_text.")";
    }

    $f_rajons = Array('title' => $this->l('Rajons'), 'required' => true, 'type' => 'select', 'person' => 'fiziska', 'values' => $rajoni);
    $j_rajons = Array('title' => $this->l('Rajons'), 'required' => true, 'type' => 'select', 'person' => 'juridiska', 'values' => $rajoni);

		$this->paymethods = $this->paymethods_data = array();

		foreach($this->payment_methods->getVisible() as $paymethod){

			$this->paymethods_data[$paymethod['key']] = $paymethod;

			if($paymethod['output_type'] == 'text_only'){
				$output = $paymethod['name_'.$this->lang];
			}elseif($paymethod['output_type'] == 'icon_only'){
				$output = "<img class='payment_method_icon' src='".$paymethod['icon']."' />";
			}else{
				$output = "<img class='payment_method_icon' src='".$paymethod['icon']."' /> ".$paymethod['name_'.$this->lang];
			}

			$this->paymethods[$paymethod['key']] = $output;
		}
		$this->use_paymethods = (count($this->paymethods) > 1);

    $a = Array();
    if ($this->use_paymethods){
      $a['apmaksas_veids'] = Array('title' => $this->l('Apmaksas veids'), 'required' => true, 'type' => 'radio', 'values' => $this->paymethods, 'value' => $apm_val);
    }

    $delivery_methods = array();
    $shops = $this->shops->getDBData(array("where" => "can_pickup = 1", "order" => "ind"));
    foreach($shops as $shop){
			if($shop['coords']){ // has coords
     		$delivery_methods[$shop['item_id']] = str_replace("[address]", "<a class='map_fancy' href='?action=popup_map&amp;shop=".$shop['item_id']."'>".$shop['title_'.$this->lang]."</a>", $this->l("Izņem pasūtījumu [address]"));
      }else{ // hasn't coords
      	$delivery_methods[$shop['item_id']] = str_replace("[address]", $shop['title_'.$this->lang], $this->l("Izņem pasūtījumu [address]"));
      }
		}

    $delivery_methods["kurjers"] = $this->LS('kurjerspiegada');
    
		if($this->use_vat_validator){
			$a['j_pvnregnr'] = Array('title' => $this->LS('pvnregnr'), 'required' => false, 'type' => 'str', 'person' => 'juridiska');
			 $countries = Array(0 => $this->l("Izvēlieties valsti..."));
				foreach($this->countries->GetTable(array('order'=>'name_'.$this->lang)) as $country){
					
					$valstis[$country['code']] = $country['name_' . $this->lang];
				}

				$a['j_pvncountry'] = Array('title' => $this->l('Valsts'), 'required' => true, 'type' => 'select', 'person' => 'juridiska', 'values' => $valstis);
				$a['f_pvncountry'] = Array('title' => $this->l('Valsts'), 'required' => true, 'type' => 'select', 'person' => 'fiziska', 'values' => $valstis);					
		}

    $this->allfields = array_merge($a, Array(
      'maksatajs' => Array('title' => $this->LS('maksatajs'), 'required' => true, 'type' => '', 'person' => ''),
      'f_vards' => Array('title' => $this->LS('vardsuzvards'), 'required' => true, 'type' => 'str', 'person' => 'fiziska'),
      'f_adrese' => Array('title' => $this->LS('piegadesadrese'), 'required' => true, 'type' => 'area', 'person' => 'fiziska'),
      'f_piegadesveids' => Array('title' => $this->LS('piegadesveids'), 'required' => true, 'type' => 'radio', 'person' => 'fiziska', 'values' => $delivery_methods),
      'f_rajons' => $f_rajons,
      'f_pilseta' => Array('title' => $this->LS('pilseta'), 'required' => true, 'type' => 'str', 'person' => 'fiziska'),
      'f_indekss' => Array('title' => $this->LS('pastaindekss'), 'required' => true, 'type' => 'str', 'person' => 'fiziska'),
      'f_telefons' => Array('title' => $this->LS('telefons'), 'required' => true, 'type' => 'str', 'person' => 'fiziska'),
      'f_epasts' => Array('title' => $this->LS('epasts'), 'required' => true, 'type' => 'str', 'person' => 'fiziska'),
      'f_komentari' => Array('title' => $this->LS('komentari'), 'required' => false, 'type' => 'area', 'person' => 'fiziska'),
      'j_nosaukums' => Array('title' => $this->LS('nosaukums'), 'required' => true, 'type' => 'str', 'person' => 'juridiska'),
      'j_regnr' => Array('title' => $this->LS('regnr'), 'required' => true, 'type' => 'str', 'person' => 'juridiska'),
      'j_juradrese' => Array('title' => $this->LS('juridadrese'), 'required' => true, 'type' => 'area', 'person' => 'juridiska'),
      'j_adrese' => Array('title' => $this->LS('piegadesadrese'), 'required' => true, 'type' => 'area', 'person' => 'juridiska'),
      'j_piegadesveids' => Array('title' => $this->LS('piegadesveids'), 'required' => true, 'type' => 'radio', 'person' => 'juridiska', 'value' => 'pats_atbrauks', 'values' => $delivery_methods),
      'j_rajons' => $j_rajons,
      'j_pilseta' => Array('title' => $this->LS('pilseta'), 'required' => true, 'type' => 'str', 'person' => 'juridiska'),
      'j_indekss' => Array('title' => $this->LS('pastaindekss'), 'required' => true, 'type' => 'str', 'person' => 'juridiska'),
      'j_telefons' => Array('title' => $this->LS('telefons'), 'required' => true, 'type' => 'str', 'person' => 'juridiska'),
      'j_epasts' => Array('title' => $this->LS('epasts'), 'required' => true, 'type' => 'str', 'person' => 'juridiska'),
      'j_komentari' => Array('title' => $this->LS('komentari'), 'required' => false, 'type' => 'area', 'person' => 'juridiska'),
    ));

    //only functional for radiobuttons
    $this->fielddependencies = Array();

    foreach($shops as $shop){
      $this->fielddependencies['f_piegadesveids'][$shop['item_id']] = Array(
          'f_rajons'        => 'unrequire',
          'f_adrese'        => 'unrequire',
          'f_pilseta'       => 'unrequire',
          'f_indekss'       => 'unrequire'
        );
      $this->fielddependencies['j_piegadesveids'][$shop['item_id']] = Array(
          'j_rajons'        => 'unrequire',
          'j_adrese'        => 'unrequire',
          'j_pilseta'       => 'unrequire',
          'j_indekss'       => 'unrequire'
        );

    }

  }

  function OutputField($name){

    $field = $this->allfields[$name];

    ?>
    <tr id='field_<?=$name?>'>
      <td class='label <?=($field['err']? "error" : "")?>'><label for='field_input_<?=$name?>'> <?=$field['title']?> <?=$field['required']? "<span>*</span>" : "" ?>:</label></td>
      <td>
      <? if($field["type"] == 'str'){ ?>
        <input name="<?=$name?>" value="<?=htmlspecialchars($field['value'], ENT_QUOTES)?>" type="text" id='field_input_<?=$name?>' />
      <? }elseif($field['type'] == 'check'){ ?>
        <input name="<?=$name?>" <?=($field['value'] ? 'checked="checked"' : '')?>  type="checkbox" id='field_input_<?=$name?>'  />
      <? }elseif($field['type'] == 'area'){ ?>
        <textarea name="<?=$name?>" id='field_input_<?=$name?>'><?=htmlspecialchars($field['value'], ENT_QUOTES)?></textarea>
      <? }elseif($field['type'] == 'radio'){ ?>

        <? foreach($field['values'] as $value => $text){ ?>
          <input type="radio" name="<?=$name?>" id="<?=$name?>_<?=$value?>" value="<?=$value?>" <?=($field['value'] == $value ? ' checked="checked"' : '')?> onclick="processFieldDependencies()" />
          <label for="<?=$name?>_<?=$value?>"><?=$text?></label><br />
        <? } ?>

      <? }elseif($field['type'] == 'select'){ ?>

          <select name='<?=$name?>' class='styled' id='field_input_<?=$name?>'>
          <? foreach($field['values'] as $id => $value){ ?>
            <option value='<?=$id?>' <?=($id == $field['value'])? "selected='selected'" : "" ?>><?=$value?></option>
          <? } ?>
          </select>
          <div class='cb'></div>

      <? } ?>
      </td>
    </tr>
    <?

  }

  function outputDone(){

    echo "<div class='padding'>";
    echo $this->LS('paldiesparpirkumu');
    if(!$_SESSION['logged_in']) {
    ?>
      <br /><br /><?=$this->LS('spiedietlaireg') ?> <a href="<?php echo Constructor\SystemUrl::get('registration')?>"><?=$this->LS('spiedietseitlai') ?></a>
    <?
    }
    echo "</div>";

  }

  function outputOrderForm($p){

    extract($p);
    $sum = 0;

    if($_SESSION['giftcard_code']){
      $giftcard = $this->GiftCards->ValidateGiftCard($_SESSION['giftcard_code']);
    }

    if($this->Promo->active_promocode){
      $promocode = $this->Promo->ValidatePromoCode($this->Promo->active_promocode);
    }
    
?>
  <div id="orderform">
    <h2 class='styled padding'><?=$this->l("Jūsu pasūtījums")?></h2>
    <div id='BigCart'>
      <table>
        <tr>
          <th><?=$this->l("Prece")?></th>
          <th>&nbsp;</th>
          <th><?=$this->l("Cena")?></th>
          <th><?=$this->l("Daudzums")?></th>
          <th><?=$this->l("Kopā")?></th>
        </tr>
        <? foreach($goods as $ind => $good){ ?>
        <?

				 $image = $this->shopman->getProductPicture($good['product']);
         $image = $image? $image : $this->shop->default_product_image;
         $prod_sum = $good["count"] * $good['price'];
         $sum += $prod_sum;
         $goods_js[$good['key']] = array("price" => $good['price']);
        ?>
        <tr class='product <?=($good['too_much_in_cart'] ? 'too_much' : '')?>' id='product-<?=$good['key']?>'>
          <td class='picture'><img src='<?=getThumbUrl($image, 55, 50, 6)?>' alt='' /></td>
          <td>
            <? if($good['giftcard']){ ?>
              <span><?=$good['name']?></span>
            <? }else{ ?>
              <a href='<?=$this->shopman->getProductURL($good['product'])?>'><?=$good['name']?></a>
            <? } ?>
          </td>
          <td class='price'>
            <?=number_format($good['price'] / $_SESSION['currency']['rate'], 2)?> <?=$_SESSION['currency']['label']?>
            <? if($this->CurrencyMenu->secondary && $this->CurrencyMenu->secondary['title'] != $_SESSION['currency']['title']){ ?>
            <div class='secondary'><?=number_format($good['price'] / $this->CurrencyMenu->secondary['rate'], 2)?> <?=$this->CurrencyMenu->secondary['label']?></div>
            <? } ?>
          </td>
          <td class='counts'><?=$good['count']?> <?=$this->l("gab.")?></td>
          <td class='sum' style='padding-right: 20px;'>
            <div class='primary'><span><?=number_format($prod_sum / $_SESSION['currency']['rate'], 2)?></span> <?=$_SESSION['currency']['label']?></div>
            <? if($this->CurrencyMenu->secondary && $this->CurrencyMenu->secondary['title'] != $_SESSION['currency']['title']){ ?>
            <div class='secondary'><span><?=number_format($good["count"] * $good['price'] / $this->CurrencyMenu->secondary['rate'], 2)?></span> <?=$this->CurrencyMenu->secondary['label']?></div>
            <? } ?>
          </td>
        </tr>
        <? } ?>
      </table>
      <div class='total_box'>
        <? if($giftcard['status'] == 1){ ?>
        <? $giftcard['available'] = ($giftcard['available'] > $sum)? $sum : $giftcard['available'] ?>
        <? $sum -= $giftcard['available']; ?>
          <div class='total_row'>
            <div class='label'><?=$this->l("Dāvanu karte")?>:</div>
            <div class='total'><span><?=number_format($giftcard['available'] / $_SESSION['currency']['rate'], 2)?></span> <?=$_SESSION['currency']['label']?></div>
          </div>
        <? } ?>

        <? if($promocode && $promocode['discount']){ ?>
        <? $promocode['discount'] = ($promocode['discount'] > $sum)? $sum : $promocode['discount'] ?>
        <? $sum -= $promocode['discount']; ?>
          <div class='total_row'>
            <div class='label'><?=$this->l("Promo koda atlaide")?>:</div>
            <div class='total'><span><?=number_format($promocode['discount'] / $_SESSION['currency']['rate'], 2)?></span> <?=$_SESSION['currency']['label']?></div>
          </div>
        <? } ?>
        
        <div class='total_row'>
          <div class='label'><?=$this->l("Kopā")?>:</div>
          <div class='total'><span><?=number_format($sum / $_SESSION['currency']['rate'], 2)?></span> <?=$_SESSION['currency']['label']?></div>
          <? if($this->CurrencyMenu->secondary && $this->CurrencyMenu->secondary['title'] != $_SESSION['currency']['title']){ ?>
            <div class='secondary'><span><?=number_format($sum / $this->CurrencyMenu->secondary['rate'], 2)?></span> <?=$this->CurrencyMenu->secondary['label']?></div>
          <? } ?>
        </div>
      </div>
      <div class='cb'></div>
    </div>
    <div class='sep'></div>

    <? if(!$_SESSION['logged_in']) { //atteelo ielogoshanaas formu ?>
    <h2 class='styled padding'><?=$this->LS('esosajiemklientiem') ?></h2>
    <form action="<?php echo Constructor\SystemUrl::get('login')?>" method="post" name="" class="loginform">
      <table>
        <tr>
          <td class='label'><label for='field_login_email'><?=$this->l('E-pasts') ?>:</label></td>
          <td><input type="text" name="email" class="form_box" value="" id='field_login_email' /></td>
        </tr>
        <tr>
          <td class='label'><label for='field_password'><?=$this->l('Parole') ?>:</label></td>
          <td>
            <input type="password" name="password" class="form_box" value="" id='field_password' />
          </td>
        </tr>
        <tr>
          <td></td>
          <td><input type="submit" name="login" value="<?=$this->l('Ieiet') ?>"/> <a href='<?=$this->login->getProperty("recover_url")?>' class='recover'><?=$this->l("Aizmirsi paroli?")?></a> </td>
        </tr>
      </table>
      <? if($this->login->use_soc_logins){ ?>
      <div class='login_sep'></div>
      <table class='soc_login'>
        <tr>
          <td class='label'><?=$this->l("Ielogoties ar")?>:</td>
          <td>
            <? if($this->login->use_facebook_login){ ?>
            <a class='fb_login' href='<?=str_replace("&", "&amp;", $this->login->facebook->getLoginUrl(array("scope" => "email")))?>'><img src='/images/html/login_facebook.png' alt='' /></a>
            <? } ?>
            <? if($this->login->use_google_login){ ?>
            <a class='google_login' href='<?=str_replace("&", "&amp;", $this->login->google->createAuthUrl())?>'><img src='/images/html/login_google.png' alt='' /></a>
            <? } ?>
            <? if($this->login->use_draugiem_login){ ?>
            <a class='draugiem_login' href='<?=str_replace("&", "&amp;", $this->login->draugiem->getLoginURL($_SERVER['REQUEST_URI']))?>'><img src='/images/html/login_dr.png' alt='' /></a>
            <? } ?>
          </td>
        </tr>
      </table>
    <? } ?>
    </form>
    <div class='sep'></div>
  <? } ?>
  <h2 class='styled padding'><?=$this->l('Pasūtījuma informācija') ?></h2>
  <? if ($err) { ?>
    <div class="error">
    <ul>
			<li><?=implode("</li><li>", $err)?></li>
		</ul>
		</div>
  <? } ?>
  <form action="<?=$_SERVER["REQUEST_URI"]?>" method="POST" enctype="multipart/form-data" class="form_1" id="order_form">
    <? $this->outputCustomerForm() ?>
  </form>
  </div>
<?
  }

  function outputCustomerForm($show_buy_btn = true){

  ?>
  <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
  <div id="shoporderform_wrap">
    <table>
    	<tbody id='PaymentMethodSelectWrap'>
      <? if ($this->use_paymethods) { ?>
        <tr>
          <td class='label'><?=$this->LS('apmaksas_veids') ?> <span>*</span>:</td>
          <td>
            <? foreach ($this->paymethods as $key => $value) { ?>
              <input type="radio" name="apmaksas_veids" value="<?=$key ?>"<?=($this->allfields['apmaksas_veids']['value'] == $key ? ' checked="checked"' : '') ?> id="apmaksas_veids_<?=$key ?>"/>
              <label for="apmaksas_veids_<?=$key ?>"><?=$value ?></label><br/>
            <? } ?>
         </td>
        </tr>
      <? } ?>
			</tbody>
      <tr>
        <td class='label'><?=$this->LS('maksatajs') ?> <span>*</span>:</td>
        <td>
          <input type="radio" name="maksatajs" value="juridiska"<?=($this->allfields['maksatajs']['value'] != 'fiziska'?' checked="checked"':'') ?> id="maksatajs_jur" />
          <label for="maksatajs_jur"><?=$this->LS('juridiskapersona') ?></label><br />
          <input type="radio" name="maksatajs" value="fiziska"<?=($this->allfields['maksatajs']['value'] == 'fiziska'?' checked="checked"':'') ?> id="maksatajs_fiz"/>
          <label for="maksatajs_fiz"><?=$this->LS('fiziskapersona') ?></label>
        </td>
      </tr>
      <tbody style="display: <?=($this->allfields['maksatajs']['value'] == 'fiziska'?'table-row-group':'none') ?>;" class="fiz_table">
        <?=$this->OutputField('f_vards') ?>
        <?=($this->use_vat_validator ? $this->OutputField('f_pvncountry') : '')?>
        <?=$this->OutputField('f_piegadesveids') ?>
      </tbody>
      <tbody id="f_adr" class="fiz_table" style="display:<?=((!is_numeric($this->allfields['f_piegadesveids']['value']) && $this->allfields['f_piegadesveids']['value'] != 'pats_atbrauks' && $this->allfields['maksatajs']['value'] == 'fiziska') ? 'table-row-group' : 'none') ?>">
        <tr><td colspan="2">&nbsp;</td></tr>
        <?=$this->OutputField('f_rajons') ?>
        <?=$this->OutputField('f_adrese') ?>
        <?=$this->OutputField('f_pilseta') ?>
        <?=$this->OutputField('f_indekss') ?>
      </tbody>
      <tbody style="display: <?=($this->allfields['maksatajs']['value'] == 'fiziska'?'table-row-group':'none') ?>;" class="fiz_table">
        <?=$this->OutputField('f_telefons') ?>
        <?=$this->OutputField('f_epasts') ?>
        <?=$this->OutputField('f_komentari') ?>
        <? if ($show_buy_btn) { ?>
        <tr>
          <td></td>
          <td><input type="submit" name="f_submit" value="<?=$this->l('Apstiprināt maksājumu') ?>"/></td>
        </tr>
        <? } ?>
      </tbody>

      <tbody style="display: <?=($this->allfields['maksatajs']['value'] != 'fiziska'? 'table-row-group':'none') ?>;" class="jur_table">
        <?=$this->OutputField('j_nosaukums') ?>
        <?=$this->OutputField('j_regnr') ?>
        <?=($this->use_vat_validator ? $this->OutputField('j_pvnregnr') : '')?>
        <?=($this->use_vat_validator ? $this->OutputField('j_pvncountry') : '')?>
        <?=$this->OutputField('j_juradrese') ?>
        <?=$this->OutputField('j_piegadesveids') ?>
      </tbody>

      <tbody id="j_adr" class="jur_table" style="display:<?=((!is_numeric($this->allfields['j_piegadesveids']['value']) && $this->allfields['j_piegadesveids']['value'] != 'pats_atbrauks' && $this->allfields['maksatajs']['value'] == 'juridiska') ? 'table-row-group' : 'none') ?>">
        <tr><td colspan="2">&nbsp;</td></tr>
        <?=$this->OutputField('j_rajons') ?>
        <?=$this->OutputField('j_adrese') ?>
        <?=$this->OutputField('j_pilseta') ?>
        <?=$this->OutputField('j_indekss') ?>
      </tbody>

      <tbody style="display: <?=($this->allfields['maksatajs']['value'] != 'fiziska'? 'table-row-group' : 'none') ?>;" class="jur_table">
        <?=$this->OutputField('j_telefons') ?>
        <?=$this->OutputField('j_epasts') ?>
        <?=$this->OutputField('j_komentari')  ?>
        <? if ($show_buy_btn) { ?>
        <tr>
          <td></td>
          <td><input type="submit" name="j_submit" value="<?=$this->l('Apstiprināt maksājumu') ?>"/></td>
        </tr>
        <? } ?>
      </tbody>
    </table>
  </div>
	<?php if($this->Swedbank && $this->Swedbank->useSwedbank()){ ?>
	<div id='ServiceAddress' style='display:<?=$this->allfields['apmaksas_veids']['value'] == 'card' ? 'block' : 'none'?>'>
		<?=$pers.' '.$this->service_address?>
	</div>
	<?php } ?>

  <script>

    $(document).ready(function(){

			$('input[name="apmaksas_veids"]').change(function(){

				if($('input[name="apmaksas_veids"]:checked').val() == 'card'){
					$("#ServiceAddress").show();
				}else{
					$("#ServiceAddress").hide();
				}

			});

      $("input[name='maksatajs']").change(function(){

        if($(this).val() == 'juridiska'){

          $(".jur_table").show();
          $(".fiz_table").hide();

        }else{

          $(".jur_table").hide();
          $(".fiz_table").show();

        }

      });

    });

  </script>

<?

    echo '
    <script type="text/javascript">
    function processFieldDependencies()
    {

    ';
    //process dependencies
    foreach($this->fielddependencies as $masterfield => $values){
      foreach($values as $value => $fieldmodifiers){
        echo '
        if(document.getElementById("' . $masterfield . '_' . $value . '").checked){
        ';
          foreach($fieldmodifiers as $slavefield => $modifier){
            switch ($modifier){
              case 'require':
                echo '$("field_' . $slavefield . ' .label span").html("*");';
                break;
              case 'unrequire':
                echo '$("field_' . $slavefield . ' .label span").html("");';
                break;
            }
          }
        echo '
        }else{
        ';
          foreach($fieldmodifiers as $slavefield => $modifier)
          {
            switch ($this->allfields[$slavefield]['required'])
            {
              case true:
                echo '$("field_' . $slavefield . ' .label span").html("*");';
                break;
              case false:
                echo '$("field_' . $slavefield . ' .label span").html("");';
                break;
            }
          }
        echo '
        }
        ';
      }
    }
    echo '
    }
    </script>
    ';

?>
  <script type="text/javascript">

    $(function(){
      $('#shoporderform_wrap input[name=j_piegadesveids]').change(function(e){
        $('#j_adr').toggle(this.value == 'kurjers');
      });

      $('#shoporderform_wrap input[name=f_piegadesveids]').change(function(e){
        $('#f_adr').toggle(this.value == 'kurjers');
      });
    });

  </script>
<?

  }

  function initCustomerForm($fields = false){

    $this->GetFields();

    if($fields){
      foreach($this->allfields as $key => $val){
        $this->allfields[$key]['value'] = $fields[$key];
      }
    }

  }

  function processCustomerForm(){
    if (!$_POST) return false;

    $err = array();

    foreach($this->allfields as $key => $val){
      $this->allfields[$key]['value'] = stripslashes($_POST[$key]);
    }

    //process dependencies
    foreach($this->fielddependencies as $masterfield => $values)
    {
      foreach($values as $value => $fieldmodifiers)
      {
        if($this->allfields[$masterfield]['value'] == $value)
        {
          foreach($fieldmodifiers as $slavefield => $modifier)
          {
            switch ($modifier)
            {
              case 'require':
                $this->allfields[$slavefield]['required'] = true;
                break;
              case 'unrequire':
                $this->allfields[$slavefield]['required'] = false;
                break;
            }
          }
        }
      }
    }

    foreach($this->allfields as $key => $val){

      if($_POST['maksatajs'] != $this->allfields[$key]['person'] && $this->allfields[$key]['person']){
        $this->allfields[$key]['required'] = false;
        $this->allfields[$key]['ignoreinorder'] = true;
      }

      if($this->allfields[$key]['required'] && !$this->allfields[$key]['value']){
        $err[] = $this->LS('aizpildietlauku') . ' "' . $this->allfields[$key]['title'] . '"';
        $this->allfields[$key]['err'] = true;
      }

    }

    return $err;

  }

    function execute()
    {

    }

    function getServices()
    {

        $this->login = EVeikalsLogin::getInstance('Login');
        $this->shop = EVeikalsFrontendShop::getInstance();
        $this->CurrencyMenu = EVeikalsCurrencyMenu::getInstance();
        $this->shopman = shopmanager::getInstance();

        $this->Promo = EVeikalsPromo::getInstance();
        if($this->Promo) $this->Promo->init();

        $this->GiftCards = EVeikalsGiftCard::getInstance();
        if($this->GiftCards) $this->GiftCards->init();

    }

  function Output()
  {

    $this->getServices();
    $this->initOptions();
    $this->InitLanguageStrings();
    $this->initCustomerForm($_SESSION['buyers_information']);

    if ($_GET['done'] == 1){
      $this->outputDone();
      return;
    }

    $this->shopman->getSumAndGoodsList($goods, $sum, $hasgoods);

    if (count($goods) && ($_POST['j_submit'] || $_POST['f_submit'])){

      $err = $this->processCustomerForm();

      if(!$err){

        $a = Array();
        foreach($this->allfields as $key => $val){
          $a[$key] = $this->allfields[$key]['value'];
        }

        $_SESSION['buyers_information'] = $a;

        if($_SESSION['logged_in']){

          //update buyers information for user
          $fields = serialize($_SESSION['buyers_information']);
          $this->usercollection->Update(Array('savedfields' => $fields), array("item_id" => $_SESSION['logged_in']['item_id']));

        }else{
          $user = false;
        }

        $this->checkoutProduct($goods, $sum, $this->allfields, $this->lang, $user);

        header('Location: ?done=1');
        exit();

      }

    }

    $tpl = Array('goods' => $goods);
    $tpl['orderpath'] = '/'.$this->lang.'/'.$this->orderpath.'/';

    //atteelo lietotaaja datu (vaards, adrese u.t.t.) ievades formu
    $tpl['err'] = $err;

    $this->outputOrderForm($tpl);

  }

	function sendPaymentConfirmationMail($order){

		$order = is_array($order) ? $order : $this->ordercollection->GetById($order);

	 	$order_text = $this->getOrderById($order['item_id']);

    $savedir = DIR_ROOT."/".$this->rekini_save_path;
		$filename = "order_".$order['item_id']."_confirmation";

    $exported_doc_rekins = $savedir.$filename.".pdf";
    $mpdf = new mPDF('');
    $mpdf->WriteHTML($order_text);
    $mpdf->Output($exported_doc_rekins, "F");

		$letter = option();

		$order['valoda'] = $order['valoda'] ? $order['valoda'] : 'lv';

		//$order['epasts'] = 'kristaps.taube@gmail.com';

		$subject = readOption('Shop\\OrderForm\\confirmation_mail_subject_'.$order['valoda']);
		$text = readOption('Shop\\OrderForm\\confirmation_mail_subject_'.$order['valoda']);
		$text = str_replace("[ORDER_ID]", $this->order_prefix.' '.$order['item_id'], $text);

		$sent = phpMailerSend($text, $subject, $this->emailfrom_name, $this->emailfrom_email, $order['epasts'], true, false, Array($exported_doc_rekins));

	}

  function getRegion($allfields, $sum){

    $pers = ($allfields['maksatajs']['value'] == 'juridiska') ? 'j' : 'f';

    $rajons_id = intval($allfields[$pers.'_rajons']['value']);
    $pveids = $allfields[$pers.'_piegadesveids']['value'];

    if(is_numeric($pveids)){
      $rajons['name'] = '';
      $rajons['price'] = 0;
    }else{
      $rajons = $this->regions->getItemByIDAssoc($rajons_id);
      $rajons['name'] = $rajons['name_' . $this->lang];
    }
		
	 	if($this->use_vat_validator)
			$this->checkValidVAT($allfields);				
		
    switch ($this->delivery_pvn_method){
      case 'add':
        $rajons['price_pvn'] = $rajons['price'] * $this->pvn;
        $rajons['price_total'] = $rajons['price'] + $rajons['price'] * $this->pvn;
        break;

      case 'included':
      default:
        $rajons['price_pvn'] = $rajons['price'] - $rajons['price'] / (1 + $this->pvn);
        $rajons['price_total'] = $rajons['price'];
        break;

    }

    return $rajons;

  }

  function Design(){
    echo $this->designAddButtons();
    $this->Output();
  }

  function say($num, $lvsay){
    if (($tmp=floor($num/1000000000))>=1)
      return $this->say($tmp, $lvsay).' '.$lvsay['1000000000'.($tmp>1?'x':'')].' '.$this->say($num-$tmp*1000000000, $lvsay);
    if (($tmp=floor($num/1000000))>=1)
      return $this->say($tmp, $lvsay).' '.$lvsay['1000000'.($tmp>1?'x':'')].' '.$this->say($num-$tmp*1000000, $lvsay);
    if (($tmp=floor($num/1000))>=1)
      return $this->say($tmp, $lvsay).' '.$lvsay['1000'.($tmp>1?'x':'')].' '.$this->say($num-$tmp*1000, $lvsay);
    if (($tmp=floor($num/100))>=1)
      return $this->say($tmp, $lvsay).' '.$lvsay['100'.($tmp>1?'x':'')].' '.$this->say($num-$tmp*100, $lvsay);
    if (($tmp=floor($num/10))>=2)
      if (isset($lvsay[$num]))
        return $lvsay[$num];
      else
        return $lvsay[$tmp*10].' '.$this->say($num-$tmp*10, $lvsay);
    if ($num>=0)
      return $lvsay[$num];
  }

  function saynum($num, $lvsay){
    $num = round($num, 2);
    return $this->say($tmp=floor($num), $lvsay). //[(n)]n
      ' '.$lvsay['ls'.(floor($num-floor($num/10)*10)>1?'x':(($num%10==0)?'x':''))].', './/ls
    str_pad($tmp=round($num-$tmp,2)*100,2,0,STR_PAD_LEFT).' './/nn
    $lvsay['snt'.(($tmp=($tmp%10))>1?'x':($tmp==0?'0':''))];//snt
  }

  function checkoutProduct($productlist, $sum, $allfields, $lang, $user){

    $language = sqlQueryValue("SELECT language FROM " . $this->site_id . "_pages WHERE page_id=". $this->page_id);
    if($language){
      $encoding = sqlQueryValue("SELECT encoding FROM " . $this->site_id . "_languages WHERE language_id=" . $language);
    }

    $orderlang = $this->lang;

    if($_SESSION['giftcard_code']){

      $giftcard = $this->GiftCards->ValidateGiftCard($_SESSION['giftcard_code']);

      if($giftcard['status'] == 1){

        $this->used_giftcard_sum = ($sum < $giftcard['available'])? $sum : $giftcard['available'];
        $this->used_giftcard_code = $_SESSION['giftcard_code'];
        $giftcard['spent'] += $this->used_giftcard_sum;
        $this->GiftCards->cards->ChangeItemByIDAssoc($giftcard['id'], array("spent" => $giftcard['spent']));

      }

    }else{
      $this->used_giftcard_sum = 0;
    }

    $nextid = $this->ordercollection->getNextId();
    $ordertext = $this->getOrder($productlist, $sum, $allfields, $orderlang, $user, $nextid);
		// header('Content-Type: text/html; charset=utf-8');
		// echo $ordertext;
		// die;
    $allfields['komentari']['value'] = base64_encode($allfields['komentari']['value']);

    $order_data = Array(
      'products' => $productlist,
      'sum' => $this->total_sum,
      'allfields' => $allfields,
      'orderlang' => $orderlang,
      'user' => $user
    );

    $pers = $allfields['maksatajs']['value'][0];
    $buyer = $allfields[$pers == 'f' ? 'f_vards' : 'j_nosaukums']['value'];

    $user_id = intval($_SESSION['logged_in']['item_id']);

    $new_status = $this->statuses->getNewStatusRow();

    $order_id = $this->ordercollection->Insert(Array(
      "user_id" => $user_id,
      "datums" => date('Y-m-d H:i:s'),
      "rekins" => $ordertext,
      "summa" => $this->total_sum,
      "valoda" => $this->lang,
      "pasutitajs" => $buyer,
      "piegadats" => 0,
      "apmaksats" => 0,
      "apmaksas_veids" => $allfields['apmaksas_veids']['value'],
      'kopeja_summa' => $this->total_sum,
      'order_data' => serialize($order_data),
      'telefons' => $allfields[$pers.'_telefons']['value'],
      'epasts' => $allfields[$pers.'_epasts']['value'],
      'adrese' => $allfields[$pers.'_adrese']['value'].', '.$allfields[$pers.'_pilseta']['value'],
      'sent' => $this->send_at_checkout ? 1 : 0,
      "status_id" => $new_status['item_id']
    ));

    // adding history entry
    $this->order_history->AddItemAssoc(array(
      "order_id" => $order_id,
      "status_to_id" => $new_status['item_id'],
      "comments" => "Rēķins izveidots",
      "date" => date('Y-m-d H:i:s')
    ));

    $p_ids = Array();
    foreach($productlist as $key => $prod){

      $this->orderprodcollection->AddItemAssoc(Array(
        'price' => $prod['price'],
        'count' => $prod['count'],
        'prod_id' => $prod['id'],
        'order_id' => $order_id,
        'size_id' => $prod['size']['item_id'] ? $prod['size']['item_id'] : 0,
        'color_id' => $prod['color']['item_id'] ? $prod['color']['item_id'] : 0,
        'cart_key' => $key
      ));

      if($prod['giftcard']){

        for($i = $prod['count']; $i > 0; $i--){
          $this->GiftCards->create($prod['sum'], $order_id);
        }

        continue;
      }

      $id = $prod['id'];
      if (!isset($p_ids[$id])) $p_ids[$id] = 0;
      $p_ids[$id] += $prod['count'];
    }
    $this->productcollection->updateProductsTimesPurchased($p_ids,$this->use_automatic_prod_stock);
                            
    $this->sendOrders($productlist, $sum, $allfields, $lang, $user, $order_id, $this->send_at_checkout);

    //order sent, empty the basket
    $_SESSION['cartitems'] = $_SESSION['giftcards'] = Array();
    unset($_SESSION['cartitems']);
    unset($_SESSION['giftcards']);

    if($this->use_promo_codes && $_SESSION['promo_code']){
      $this->Promo->registerUsage($_SESSION['promo_code']);
    }

		if(in_array($allfields['apmaksas_veids']['value'], array('card', 'ibank_swed')) && $this->Swedbank->useSwedbank()){
			$result = $this->Swedbank->makePayment($order_id);
			if(!$result){ // error! If no errror, user is redirected to Swedbank
				$error = $this->Swedbank->getError();
				header("location: ".$this->Swedbank->return_page."?view=create_payment_error&code=".$error['code']);
				die();
			}
		}

    return $order_id;
		
  }

  function srvSendOrders($productlist, $sum, $allfields, $lang, $user, $order_id, $sendtouser=true)
  {
    $this->initCollections();
    $this->initOptions();

    $this->shopman = $this->getServiceByName('shopmanager', true);
    if (!$this->shopman)
      $this->shopman = $this->getBackendServiceByName('shopmanager');
    
    return $this->sendOrders($productlist, $sum, $allfields, $lang, $user, $order_id, $sendtouser);
  }

  function sendOrders($productlist, $sum, $allfields, $lang, $user, $order_id, $sendtouser=true){

    $orderlang = $this->order_lang;
    if (!$orderlang) $orderlang = 'lv';

    $ordertext = $this->getOrder($productlist, $sum, $allfields, $orderlang, $user, $order_id);

    $msg = '';
    if ($allfields['apmaksas_veids']['value'] == 'firstdata'){
      $firstdata_url = $this->getFirstDataURL($order_id);
      $firstdata_url = 'http://'.$_SERVER['SERVER_NAME'].$firstdata_url;
      $msg .= $this->LS('varat_ar_karti').':<br/><a target="_blank" href="'.$firstdata_url.'">'.htmlspecialchars($firstdata_url).'</a><br />';
    }else if ($this->use_els && $allfields['apmaksas_veids']['value'] == 'els'){
      $els = $this->getServiceByName('e-ls');
      if (!$els) die('e-ls service not found!');

      $els_url = htmlspecialchars('http://'.$_SERVER['SERVER_NAME'].$els->getOrderPaymentURL($order_id));
      $msg .= $this->LS('varat_ar_els').':<br/><a target="_blank" href="'.$els_url.'">'.$els_url.'</a><br />';
    }

    $datums = date('H_i_s__d_m_Y');

    $ordername = "order_".$order_id.'_'.$orderlang;
		foreach(explode(",", $this->orderreciever) as $reciever){
     	$this->sendSingleOrder(trim($reciever), $ordername, $ordertext, $orderlang, true, $msg);
		}


    if (!$sendtouser) return;

    $user_email = (substr($allfields['maksatajs']['value'], 0, 1) == 'f') ? $allfields['f_epasts']['value'] : $allfields['j_epasts']['value'];
    if (!$user_email) return;

    $ordername = "order_".$order_id.'_'.$lang;

    $saveorder = true;
    if ($lang != $orderlang)
    {
      $ordertext = $this->getOrder($productlist, $sum, $allfields, $lang, $user, $order_id);
      $ordername = "order__".$order_id.'_'.$lang;
      $saveorder = true;
      $orderlang = $lang;
    }

    $this->sendSingleOrder($user_email, $ordername, $ordertext, $orderlang, $saveorder, $msg);
  }

  function sendSingleOrder($email, $filename, $content, $lang, $saveorder=true, $pre_msg='')
  {
    $encoding = sqlQueryValue("SELECT encoding FROM " . $this->site_id . "_languages WHERE shortname='$lang'");
    if ($saveorder and false)
    {
      //save order to MS Word file   /files/rekini/
      $savedir = sqlQueryValue("select dirroot from sites where site_id=".$this->site_id);
      $savedir .= "/".$this->rekini_save_path;
      /*
      require_once($GLOBALS['cfgDirRoot']."library/"."func.files.php");
      forceDirectories($this->site_id, $this->rekini_save_path);

      $exported_doc_rekins = $savedir.$filename.".doc";
      if($handle = fopen($exported_doc_rekins, "w+")){
        fwrite($handle, chr(0xFF).chr(0xFE));
        fwrite($handle, mb_convert_encoding($content, 'UTF-16LE', 'UTF-8'));
        fclose($handle);
      } */

      $exported_doc_rekins = $savedir.$filename.".pdf";
      $mpdf = new mPDF('');
      $mpdf->WriteHTML($content);
      $mpdf->Output($exported_doc_rekins, "F");

    }

    require_once($GLOBALS['cfgDirRoot']."library/"."class.phpmailer.php");
    $mail = new PHPMailer();
    $mail->IsMail();
    $mail->From     = $this->emailfrom_email;
    $mail->FromName = $this->emailfrom_name;
    $mail->AddAddress($email);
    $mail->Sender = $this->emailfrom_email;//new fix
    $mail->AddReplyTo($this->emailfrom_email);//new fix
    $mail->AddAttachment($exported_doc_rekins);
    $mail->WordWrap = 70;
    $mail->CharSet = $encoding;
    $mail->IsHTML(true);
    $mail->Subject  =  $this->mail_subject;

    if ($pre_msg != '') $pre_msg .= '<br/>';
    $mailtext = $pre_msg . $content;
    $mail->Body     =  $mailtext;

    $mailtext = str_replace("<p>","<p>\n",$mailtext);
    $mailtext = str_replace("<P>","<P>\n",$mailtext);
    $mailtext = str_replace("<br>","\n",$mailtext);
    $mailtext = str_replace("<br />","\n",$mailtext);
    $mailtext = str_replace("<BR>","\n",$mailtext);
    $mailtext = str_replace("<BR />","\n",$mailtext);
    $mailtext = str_replace("&nbsp;"," ",$mailtext);
    $mailtext = str_replace("</tr>","\n",$mailtext);
    $mail->AltBody  =  strip_tags($mailtext);

    if (!$mail->Send())
    {
      $msg = 'Order send to '.$email.' failed: '.$mail->ErrorInfo;
      add_log($msg, $this->site_id);
    }
  }

  function srvGetOrder($productlist, $sum, $allfields, $lang, $user, $rekina_id){
    $this->initCollections();
    $this->initOptions();
    if (!$lang) $lang = $this->order_lang;
    return $this->getOrder($productlist, $sum, $allfields, $lang, $user, $rekina_id);
  }

	public function getOrderById($id, $lang = false){

		$lang = $lang ? $lang : $this->order_lang;
		$order = is_array($id) ? $id : $this->ordercollection->GetById($id);

		if(!$order || !is_array($order)) return false;

		$order_data = unserialize($order['order_data']);

		$allfields = $order_data['allfields'];
		$products = $order_data['products'];

		return $this->getOrder($products,0,$allfields,$lang,$order['user_id'],$order);

	}

  function getOrder($productlist, $sum, $allfields, $lang, $user, $order){

		$order = is_array($order) ? $order : $this->ordercollection->GetById($order);


		$months = explode(',',$this->LS('menesu_nosaukumi', false, $lang));
		if($order){
			$order_status_row = $this->statuses->GetById($order['status_id']);
			$time = strtotime($order['datums']);
			$datums = date('Y', $time).'. '.$this->l("gada", $lang).' '.date('d', $time).'. '.$months[date('m', $time)-1];
		}else{
			$order_status_row = $this->statuses->getNewStatusRow();
			$datums = date('Y').'. '.$this->l("gada", $lang).' '.date('d').'. '.$months[date('m')-1];
		}

    if($allfields['maksatajs']['value'] == 'juridiska')
      $pers = 'j';
    else
      $pers = 'f';

    $rajons = $this->getRegion($allfields, $sum);
    $rajons_id = $rajons['item_id'];

		$payment_method = $this->payment_methods->getByKey($allfields['apmaksas_veids']['value']);
    $logo = $this->orderlogo ? 'http://'.$GLOBALS['cfgDomain'].$this->orderlogo : '';

		ob_start();
		?>
      <style type="text/css">
				#ProdTable{
					width: 100%;
					border: 1px solid black;
					border-collapse: collapse;
				}

				#ProdTable td{
					border: 1px solid black;
				}

      </style>
      <table id="ordertable<?=$order['item_id']?>" style="page-break-inside:avoid" width="100%"  border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td colspan="1" rowspan="2"><?=($logo ? '<img src="'.$logo.'" alt="" style="max-height: 100px">' : '')?></td>
				<td colspan="4" align="right"><b><?=$this->LS('epasutijumsnr', false, $lang).'.: '.$this->order_prefix.' '.$order['item_id']?></b></td>
      </tr>
      <tr>
				<td colspan="4" align="right"><b><?=$datums?></b></td>
      </tr>
      <tr>
        <td colspan="5"><hr /></td>
      </tr>
      <tr>
        <td><strong><span class="mazsTd"><?=$this->LS('precuiesniedzejs', false, $lang)?>:</span></strong></td>
        <td><strong><span class="mazsTd"><?=$this->LS('value_precuiesniedzejs', false, $lang)?></span></strong></td>
      </tr>
      <tr>
        <td><span class="mazsTd"><?=$this->LS('registracijasnumurs', false, $lang)?>:</span></td>
        <td><span class="mazsTd"><?=$this->LS('value_registracijasnumurs', false, $lang)?></span></td>
      </tr>
      <tr>
        <td><span class="mazsTd"><?=$this->LS('juridiskaadrese', false, $lang)?>:</span></td>
        <td><span class="mazsTd"><?=$this->LS('value_juridiskaadrese', false, $lang)?></span></td>
      </tr>
			<tr>
      	<td><span class="mazsTd"><?=$this->LS('precuizsniegsanasadrese', false, $lang)?>:</span></td>
        <td><span class="mazsTd"><?=$this->LS('value_precuizsniegsanasadrese', false, $lang)?></span></td>
			</tr>
			<tr>
      	<td></td>
			</tr>
      <tr>
        <td><span class="mazsTd"><?=$this->LS('bankasnosaukums', false, $lang)?>:</span></td>
        <td><span class="mazsTd"><?=$this->LS('value_bankasnosaukums', false, $lang)?></span></td>
      </tr>
      <tr>
        <td><span class="mazsTd"><?=$this->LS('bankaskods', false, $lang)?>:</span></td>
        <td><span class="mazsTd"><?=$this->LS('value_bankaskods', false, $lang)?></span></td>
      </tr>
      <tr>
        <td><span class="mazsTd"><?=$this->LS('bankaskonts', false, $lang)?>:</span></td>
        <td><span class="mazsTd"><?=$this->LS('value_bankaskonts', false, $lang)?></span></td>
      </tr>
			<tr>
      	<td></td>
			</tr>
			<tr>
      	<td><span class="mazsTd"><?=$this->LS('talrunis', false, $lang)?>:</span></td>
        <td><span class="mazsTd"><?=$this->LS('value_talrunis', false, $lang)?></span></td>
			</tr>
			<tr>
      	<td><span class="mazsTd"><?=$this->LS('tabula_fakss', false, $lang)?>:</span></td>
        <td><span class="mazsTd"><?=$this->LS('value_fakss', false, $lang)?></span></td>
			</tr>
      <tr>
        <td colspan="5"><hr /></td>
      </tr>
      <tr>
        <td><strong><span class="mazsTd"><?=($pers == 'j' ? $this->LS('precu_sanemejs', false, $lang) : $this->LS('klienta_vards_uzvards', false, $lang))?>:</span></strong></td>
        <td><strong><span class="mazsTd"><?=($pers == 'j' ? $allfields[$pers.'_nosaukums']['value'] : $allfields[$pers.'_vards']['value'])?></span></strong></td>
			</tr>
			<? if($user['username']){ ?>
       <tr>
				<td><span class="mazsTd"><?=$this->LS('interneta_veikala_lietotajvards', false, $lang)?>:</span></td>
        <td><span class="mazsTd"><?=$user['username']?></span></td>
			</tr>
			<? } ?>

      <? if($pers == 'j' && $this->LS('klienta_reg_numurs', false, $lang)){ ?>
      <tr>
        <td><span class="mazsTd"><?=$this->LS('klienta_reg_numurs', false, $lang)?></span></td>
        <td><span class="mazsTd"><?=$allfields[$pers.'_regnr']['value']?></span></td>
      </tr>
      <? } ?>

      <? if ($pers == 'j'){ ?>
      <tr>
        <td><span class="mazsTd"><?=$this->LS('pvnregnr_full', false, $lang)?>:</span></td>
        <td><span class="mazsTd"><?=$allfields['j_pvnregnr']['value']?></span></td>
      </tr>			
      <tr>
      	<td><span class="mazsTd"><?=$this->LS('klienta_juridiska_adrese', false, $lang)?></span></td>
      	<td><span class="mazsTd"><?=$allfields[$pers.'_juradrese']['value']?></span></td>
			</tr>
			<? } ?>
			<tr>
        <td><span class="mazsTd"><?=$this->LS('klienta_talrunis', false, $lang)?>:</span></td>
        <td><span class="mazsTd"><?=$allfields[$pers.'_telefons']['value']?></span></td>
      </tr>
      <? if($allfields[$pers.'_fakss']['value']){ ?>
        <tr>
        	<td><span class="mazsTd"><?=$this->LS('klienta_fakss', false, $lang)?>:</span></td>
          <td><span class="mazsTd"><?=$allfields[$pers.'_fakss']['value']?></span></td>
        </tr>
      <? } ?>

      <? if ($allfields[$pers.'_piegadesveids']['value'] == 'kurjers'){ ?>
				<tr>
	        <td><span class="mazsTd"><?=$this->LS('klienta_piegades_adrese', false, $lang)?>:</span></td>
	        <td><span class="mazsTd"><?=$allfields[$pers.'_adrese']['value'].',&nbsp;'.$allfields[$pers.'_pilseta']['value'].',&nbsp;'.$allfields[$pers.'_indekss']['value'].($rajons['name'] ? ',&nbsp;'.$rajons['name'] : '')?></span></td>
        </tr>
      <? } ?>

      <?
				if(is_numeric($allfields[$pers.'_piegadesveids']['value'])){
	        $shop = $this->shops->getItemByIDAssoc($allfields[$pers.'_piegadesveids']['value']);
	        $delivery_method = $shop['title_'.$this->lang];
	      }else{
	        $delivery_method = $this->LS('tabula_' . $allfields[$pers.'_piegadesveids']['value'], false, $lang);
	      }
      ?>
      <tr>
        <td><span class="mazsTd"><?=$this->LS('klienta_email', false, $lang)?>:</span></td>
        <td><span class="mazsTd"><?=$allfields[$pers.'_epasts']['value']?></span></td>
      </tr>
			<tr>
				<td><span class="mazsTd"><?=$this->l('Rēķina status', $lang)?>:</span></td>
				<td style="font-weight:bold;color: #FFA500"><?=($order_status_row ? $order_status_row['status_'.$lang] : "")?></td>
			</tr>
			<?php if($order['card_data'] && $payment_method['key'] == 'card' && $order_status_row['paid']){ ?>
			<?php $card_data = json_decode($order['card_data'], true) ?>
			<tr>
				<td><b><?=$this->l("Kartes dati", $lang)?>:</b></td>
				<td></td>
			</tr>
			<tr>
				<td><?=$this->l("Izsniedzējs", $lang)?>:</td>
				<td><?=$card_data['issuer']?></td>
			</tr>
			<tr>
				<td><?=$this->l("Tips", $lang)?>:</td>
				<td><?=$card_data['scheme']?></td>
			</tr>
			<tr>
				<td><?=$this->l("Numurs", $lang)?>:</td>
				<td><?=$card_data['pan']?></td>
			</tr>
			<tr>
				<td><?=$this->l("Derīguma termiņš", $lang)?>:</td>
				<td><?=$card_data['expirydate']?></td>
			</tr>
			<?php } ?>
      <tr>
        <td colspan="5"><hr /></td>
      </tr>
      <tr>
        <td><?=$this->LS('piegadesveids', false, $lang)?>:</td>
        <td><?=$delivery_method?></td>
      </tr>
      <tr>
        <td><?=$this->LS('apmaksas_veids', false, $lang)?>:</td>
        <td>
					<?=$payment_method['name_'.$lang]?>
          <? if($this->used_giftcard_code){ ?>
        		(<?=$this->l("Dāvanu karte", $lang)?> <?=$this->used_giftcard_code?> - <?=$this->used_giftcard_sum?> <?=$this->CurrencyMenu->default['label']?>)
      		<? } ?>
      	</td>
      </tr>
      <? if($this->use_promo_codes && $this->Promo->active_promocode){ ?>
      <tr>
        <td><?=$this->l('Promo kods', $lang)?>:</td>
        <td><?=$this->Promo->active_promocode['code']?></td>
      </tr>
      <? } ?>

      <? if(trim($allfields[$pers.'_komentari']['value'])){ ?>
   	  <tr>
        <td><?=$this->LS('tabula_komentari', false, $lang)?>:</td>
        <td><?=nl2br($allfields[$pers.'_komentari']['value'])?></td>
      </tr>
			<? } ?>
      <tr>
        <td colspan="5"><hr /></td>
      </tr>
      <tr>
        <td colspan="5">
				<table id="ProdTable">
          <tr>
            <td><?=$this->LS('tabula_nr', false, $lang)?></td>
            <td><?=$this->LS('tabula_nosaukums', false, $lang)?></td>
            <td><?=$this->LS('tabula_daudzums', false, $lang)?></td>
            <td><?=$this->LS('tabula_cena', false, $lang)?></td>
            <td><?=$this->LS('tabula_atlaide', false, $lang)?></td>
            <td><?=$this->LS('atlaizhu_summa', false, $lang)?></td>
            <td><?=$this->LS('tabula_pvnlikme', false, $lang)?></td>
            <td><?=$this->LS('tabula_pvnsumma', false, $lang)?></td>
            <td><?=$this->LS('tabula_summalvl', false, $lang)?></td>
          </tr>
		<?

    $summa_price_sum = 0;
    $summa_atlaides_sum = 0;
    $summa_pvn_sum = 0;
    $summa_price_sum_ar_pvn = 0;

    $i = 1;

    foreach ($productlist as $prod_id => $prod){

      $discount_comment = '';

      $name = $prod['name'];
      $prod['undiscounted_price'] = $prod['undiscounted_price']? $prod['undiscounted_price'] : $prod['price'];
      $price_bez_atl = $prod['undiscounted_price'];
      $count = $prod['count'];
      $price_sum = $price_bez_atl * $count;

      // all discount magic comes down to here

      if($this->use_promo_codes && $this->Promo->active_promocode){
        $promo_discount = $this->Promo->getExactProductDiscountForCode($prod, $this->Promo->active_promocode['code']);
				$discount_comment = "(".$this->l("Promo kods").")";
      }else{
        $promo_discount = 0;
      }

      if($price_bez_atl && ($prod['price'] != $prod['undiscounted_price'])){

        $atlaide_percent = (1 - ($prod['price'] - $promo_discount) / $price_bez_atl);
        $atlaide = round($atlaide_percent * 100);

      }elseif($this->use_soc_discount && $this->soc_discount && $_SESSION['soc_discount'] === true){

        $atlaide = $this->soc_discount;
        $atlaide_percent = $atlaide / 100;
        $discount_comment = "(".$this->l("Social like", $lang).")";

      }elseif($this->use_promo_codes && $this->Promo->active_promocode && $promo_discount){

        $atlaide_percent = $promo_discount / $price_bez_atl;
        $atlaide = round($atlaide_percent * 100, 2);
        $discount_comment = "(".$this->l("Promo kods", $lang).")";

      }else{

        $atlaide_percent = 0;
        $atlaide = 0;

      }

      $atlaides_sum = $price_sum * $atlaide_percent;
      $pvn = (float)$prod['pvn'] ? (float)$prod['pvn'] : ($this->pvn * 100);
      if($this->pvn == 0)
				$pvn = 0;
			$price_sum_ar_pvn = $price_sum - $atlaides_sum;
      $pvn_sum = $price_sum_ar_pvn - $price_sum_ar_pvn / (1 + ($pvn / 100));

      ?>
      <tr>
        <td><?=$i?>.</td>
        <td><?=$name?></td>
        <td><?=$count?></td>
        <td><?=(number_format($price_bez_atl, 2,',',' '))?></td>
        <td><?=$atlaide.'% '.$discount_comment?></td>
        <td><?=(number_format($atlaides_sum, 2,',',' '))?></td>
        <td><?=$pvn?>%</td>
        <td><?=(number_format($pvn_sum, 2, ',' ,' '))?></td>
        <td><?=(number_format($price_sum_ar_pvn, 2, ',' ,' '))?></td>
       </tr>
			<?

      $summa_price_sum += $price_sum;
      $summa_atlaides_sum += $atlaides_sum;
      $summa_pvn_sum += $pvn_sum;
      $summa_price_sum_ar_pvn += $price_sum_ar_pvn;

      $i++;
    }

    if(($rajons['name'] || $rajons['price']) && $this->use_promo_codes && $allfields['promo_code']['value']){

      $del_discount = $this->Promo->getDeliveryDiscountForCode($rajons['price_total'], $allfields['promo_code']['value']);
      if($del_discount){
        $delivery_text = "(".str_replace("[discount]", number_format($del_discount, 2, ',', ' ')." ".$this->CurrencyMenu->default['label'], $this->l("-[discount] promo koda atlaide", $lang)).")";
      }

    }else{
      $delivery_text = "";
    }
    ?>

      <tr>
        <td colspan="5" align="right"><?=$this->LS('summa_bez_atlaidem', false, $lang)?>:</td>
        <td colspan="14" align="left"><?=number_format($summa_price_sum, 2, ',', ' ')." ".$this->CurrencyMenu->default['label']?></td>
      </tr>
		<?

    $summa_price_sum_ar_pvn += ($rajons['price_total'] - $del_discount);

    if($this->use_promo_codes && $this->Promo->active_promocode){
      $sum_discount = $this->Promo->getSumDiscountForCode($summa_price_sum_ar_pvn, $this->Promo->active_promocode['code']);

      if($sum_discount){

        $sum_discount_text = "(".str_replace("[discount]", number_format($sum_discount, 2, ',', ' ')." ".$this->CurrencyMenu->default['label'], $this->l("+[discount] promo koda atlaide", $lang)).")";
        $summa_price_sum_ar_pvn -= $sum_discount;

      }else{
        $sum_discount_text = '';
      }

    }

    ?>
      <tr>
        <td colspan="5" align="right"><?=$this->LS('atlaizhu_summa', false, $lang)?>:</td>
        <td colspan="14" align="left"><?=number_format($summa_atlaides_sum, 2, ',', ' ')." ".$this->CurrencyMenu->default['label'].$sum_discount_text?></td>
      </tr>
    <?

    if($rajons['name'] || $rajons['price']){ ?>
      <tr>
        <td colspan="5" align="right"><?=$this->LS('kurjeraizmaksas', false, $lang)?>:</td>
        <td colspan="14" align="left"><?=number_format($rajons['price_total'], 2, ',', ' ')." ".$this->CurrencyMenu->default['label']." ".$delivery_text?></td>
      </tr>
    <? }

    $summa_pvn_sum += $rajons['price_pvn'];


    if($this->used_giftcard_sum){ ?>
	    <tr>
	      <td colspan='5' align='right'><?=$this->l("Dāvanu karte", $lang)?>:</td>
	      <td colspan='14' align='left'><?=number_format($this->used_giftcard_sum, 2)." ".$this->CurrencyMenu->default['label']?></td>
	    </tr>
		<?

      $summa_price_sum_ar_pvn -= $this->used_giftcard_sum;

      $summa_pvn_sum = $summa_price_sum_ar_pvn * $this->pvn;

    }
    ?>

      <tr>
        <td colspan="5" align="right"></td>
        <td colspan="14" align="left"></td>
      </tr>
      <tr>
        <td colspan="5" align="right"><?=$this->LS('kopa_pvn', false, $lang)?>:</td>
        <td colspan="14" align="left"><?=number_format($summa_pvn_sum, 2, ',', ' ')." ".$this->CurrencyMenu->default['label']?></td>
      </tr>
      <tr>
        <td colspan="5" align="right"></td>
        <td colspan="14" align="left"></td>
      </tr>
      <tr>
        <td colspan="5" align="right"><strong><?=$this->LS('kopa_summa_samaksai', false, $lang)?>:</strong></td>
        <td colspan="14" align="left"><strong><?=number_format($summa_price_sum_ar_pvn, 2, ',', ' ')." ".$this->CurrencyMenu->default['label']?></strong></td>
      </tr>
      <tr>
        <td colspan="5" align="right"><?=$this->LS('kopa_summa_vardiem', false, $lang)?>:</td>
        <td colspan="14" align="left"><?=ucfirst($this->sayCurrency($summa_price_sum_ar_pvn, $lang, $this->CurrencyMenu->default['label']))?></td>
      </tr>
      </table></td>
      </tr>
      <tr>
        <td colspan="5" align="right"></td>
        <td colspan="14" align="left"></td>
      </tr>
      <tr>
        <td colspan="5" align="center">
        <?php if($order_status_row['paid']){ ?>
					<?php echo $this->l("Rēķina apmaksa saņemta") ?>
				<?php }else{ ?>
					<?=$this->LS('ludzam_steidzami_apmaksat', false, $lang)?><br />
	        <?=$this->LS('ar_cienu', false, $lang)?>
				<?php } ?>
        </td>
      </tr>
      </table>
			<?

      $this->total_sum = $summa_price_sum_ar_pvn;

      $t =  ob_get_clean();

      return $t;

  }

  function designAddButtons()
  {
    $data = '';
      $data .=  '<input type="button" value="Change recipient\'s e-mail ('.$this->orderemail.')"
      OnClick="javascript:
                   try {
                     PWin.close();
                   } catch(e) { }
                     var args = new Array(window, div_'.$this->name.');
                     openModalWin(\'?module=dialogs&action=componentframe&site_id='.$this->site_id.'&page_id='.$this->page_id.'&component_name='.$this->name.'&component_type='.get_class($this).'&method_name=changeEmail&refresh=0&okaction=submit&session_id=' . session_id() . '&session_name=' . session_name() . '\', args, 400, 500, 0, 0, 0);"
      >&nbsp;&nbsp;';
      $data .=  '<input type="button" value="Change orders logo '.($this->orderlogo ? "( ".$this->orderlogo." )" : "").'"
      OnClick="javascript:
                   try {
                     PWin.close();
                   } catch(e) { }
                     var args = new Array(window, div_'.$this->name.');
                     openModalWin(\'?module=dialogs&action=componentframe&site_id='.$this->site_id.'&page_id='.$this->page_id.'&component_name='.$this->name.'&component_type='.get_class($this).'&method_name=changeLogo&refresh=0&okaction=submit&session_id=' . session_id() . '&session_name=' . session_name() . '\', args, 400, 500, 0, 0, 0);"
      >&nbsp;&nbsp;';
      $data .=  '<input type="button" value="Change Email subject '.($this->mail_subject ? "( ".$this->mail_subject." )" : "").'"
      OnClick="javascript:
                   try {
                     PWin.close();
                   } catch(e) { }
                     var args = new Array(window, div_'.$this->name.');
                     openModalWin(\'?module=dialogs&action=componentframe&site_id='.$this->site_id.'&page_id='.$this->page_id.'&component_name='.$this->name.'&component_type='.get_class($this).'&method_name=changeMailSubject&refresh=0&okaction=submit&session_id=' . session_id() . '&session_name=' . session_name() . '\', args, 400, 500, 0, 0, 0);"
      >&nbsp;&nbsp;';
      $data .=  '<input type="button" value="Change Email From '.($this->emailfrom_name ? "( ".$this->emailfrom_name." )" : "").'"
      OnClick="javascript:
                   try {
                     PWin.close();
                   } catch(e) { }
                     var args = new Array(window, div_'.$this->name.');
                     openModalWin(\'?module=dialogs&action=componentframe&site_id='.$this->site_id.'&page_id='.$this->page_id.'&component_name='.$this->name.'&component_type='.get_class($this).'&method_name=changeMailFrom&refresh=0&okaction=submit&session_id=' . session_id() . '&session_name=' . session_name() . '\', args, 400, 500, 0, 0, 0);"
      >&nbsp;&nbsp;';
      $data .=  '<input type="button" value="Change Email From (E-mail) '.($this->emailfrom_email ? "( ".$this->emailfrom_email." )" : "").'"
      OnClick="javascript:
                   try {
                     PWin.close();
                   } catch(e) { }
                     var args = new Array(window, div_'.$this->name.');
                     openModalWin(\'?module=dialogs&action=componentframe&site_id='.$this->site_id.'&page_id='.$this->page_id.'&component_name='.$this->name.'&component_type='.get_class($this).'&method_name=changeMailFromEmail&refresh=0&okaction=submit&session_id=' . session_id() . '&session_name=' . session_name() . '\', args, 400, 500, 0, 0, 0);"
      >&nbsp;&nbsp;';

    return $data;
  }


  function changeEmail()
  {
    $form_data = Array(
      'email' => Array(
        'label' => 'E-mail:',
        'type'  => 'str',
        'value' => $this->orderemail,
      ),
    );

    require_once($GLOBALS['cfgDirRoot']."library/"."class.formgen.php");
    $FormGen = new FormGen();
    $FormGen->action = '?module=dialogs&action=component&site_id='.
		$this->site_id.'&page_id='.$this->page_id.'&component_name='.
		$this->name.'&component_type='.
		get_class($this).'&method_name=saveEmail&afterrefreshmethod=&refresh=1&p_pagedev_id='.$this->pagedev_id;
    $FormGen->title = 'Change order e-mail';
    $FormGen->properties = $form_data;
    $FormGen->buttons = false;
    echo $FormGen->output();
  }

  function saveEmail()
  {
    $this->setProperty('orderemail', $_POST['email']);
    if (CheckPermission(31, $this->site_id, $this->page_id) || CheckPermission(32, $this->site_id))
    {
        pageautopublish($this->site_id, $this->pagedev_id, true);
    }
  }

  function changeMailFromEmail()
  {
    $form_data = Array(
      'from' => Array(
        'label' => 'From (e-mail):',
        'type'  => 'str',
        'value' => $this->emailfrom_email,
      ),
    );

    require_once($GLOBALS['cfgDirRoot']."library/"."class.formgen.php");
    $FormGen = new FormGen();
    $FormGen->action = '?module=dialogs&action=component&site_id='.
		$this->site_id.'&page_id='.$this->page_id.'&component_name='.
		$this->name.'&component_type='.
		get_class($this).'&method_name=saveMailFromEmail&afterrefreshmethod=&refresh=1&p_pagedev_id='.$this->pagedev_id;
    $FormGen->title = 'Change mailer\'s email (ex. info@company.lv)';
    $FormGen->properties = $form_data;
    $FormGen->buttons = false;
    echo $FormGen->output();
  }

  function changeMailFrom()
  {
    $form_data = Array(
      'from' => Array(
        'label' => 'From:',
        'type'  => 'str',
        'value' => $this->emailfrom_name,
      ),
    );

    require_once($GLOBALS['cfgDirRoot']."library/"."class.formgen.php");
    $FormGen = new FormGen();
    $FormGen->action = '?module=dialogs&action=component&site_id='.
		$this->site_id.'&page_id='.$this->page_id.'&component_name='.
		$this->name.'&component_type='.
		get_class($this).'&method_name=saveMailFrom&afterrefreshmethod=&refresh=1&p_pagedev_id='.$this->pagedev_id;
    $FormGen->title = 'Change mailer\'s name (ex. MyCompany)';
    $FormGen->properties = $form_data;
    $FormGen->buttons = false;
    echo $FormGen->output();
  }

  function saveMailFrom()
  {
    $this->setProperty('emailfrom_name', $_POST['from']);
    if (CheckPermission(31, $this->site_id, $this->page_id) || CheckPermission(32, $this->site_id))
    {
        pageautopublish($this->site_id, $this->pagedev_id, true);
    }
  }

  function saveMailFromEmail()
  {
    $this->setProperty('emailfrom_email', $_POST['from']);
    if (CheckPermission(31, $this->site_id, $this->page_id) || CheckPermission(32, $this->site_id))
    {
        pageautopublish($this->site_id, $this->pagedev_id, true);
    }
  }

  function changeMailSubject()
  {
    $form_data = Array(
      'subject' => Array(
        'label' => 'Mailing subject:',
        'type'  => 'str',
        'value' => $this->mail_subject,
      ),
    );

    require_once($GLOBALS['cfgDirRoot']."library/"."class.formgen.php");
    $FormGen = new FormGen();
    $FormGen->action = '?module=dialogs&action=component&site_id='.
		$this->site_id.'&page_id='.$this->page_id.'&component_name='.
		$this->name.'&component_type='.
		get_class($this).'&method_name=saveMailSubject&afterrefreshmethod=&refresh=1&p_pagedev_id='.$this->pagedev_id;
    $FormGen->title = 'Change mailing subject';
    $FormGen->properties = $form_data;
    $FormGen->buttons = false;
    echo $FormGen->output();
  }

  function saveMailSubject()
  {
    $this->setProperty('mail_subject', $_POST['subject']);
    if (CheckPermission(31, $this->site_id, $this->page_id) || CheckPermission(32, $this->site_id))
    {
        pageautopublish($this->site_id, $this->pagedev_id, true);
    }
  }

  function changeLogo()
  {
    $form_data = Array(
     'logo' => Array(
        'label' => 'Logo:',
        'type'  => 'dialog',
        'dialog'  => '?module=dialogs&action=filex&site_id='.$GLOBALS['site_id'].'&view=thumbs',
        'dialogw' => '600'
      ),
    );

    require_once($GLOBALS['cfgDirRoot']."library/"."class.formgen.php");
    $FormGen = new FormGen();
    $FormGen->action = '?module=dialogs&action=component&site_id='.
		$this->site_id.'&page_id='.$this->page_id.'&component_name='.
		$this->name.'&component_type='.
		get_class($this).'&method_name=saveLogo&afterrefreshmethod=&refresh=1&p_pagedev_id='.$this->pagedev_id;
    $FormGen->title = 'Change order logo';
    $FormGen->properties = $form_data;
    $FormGen->buttons = false;
    echo $FormGen->output();
  }

  function saveLogo()
  {
    $this->setProperty('orderlogo', $_POST['logo']);
  }


	/** http://stackoverflow.com/questions/9158119/vies-vat-number-validation
	 * VIES VAT number validation
	 *
	 * @author Eugen Mihailescu
	 *
	 * @param string $countryCode
	 * @param string $vatNumber
	 * @param int $timeout
	 */
	function viesCheckVAT($countryCode, $vatNumber, $timeout = 30) {
			$url = 'http://ec.europa.eu/taxation_customs/vies/services/checkVatService';
			$response = array ();
			$pattern = '/<(%s).*?>([\s\S]*)<\/\1/';
			$keys = array (
							'countryCode',
							'vatNumber',
							'requestDate',
							'valid',
							'name',
							'address'
			);

			$content = "<s11:Envelope xmlns:s11='http://schemas.xmlsoap.org/soap/envelope/'>
		<s11:Body>
			<tns1:checkVat xmlns:tns1='urn:ec.europa.eu:taxud:vies:services:checkVat:types'>
				<tns1:countryCode>%s</tns1:countryCode>
				<tns1:vatNumber>%s</tns1:vatNumber>
			</tns1:checkVat>
		</s11:Body>
	</s11:Envelope>";

			$opts = array (
							'http' => array (
											'method' => 'POST',
											'header' => "Content-Type: text/xml; charset=utf-8; SOAPAction: checkVatService",
											'content' => sprintf ( $content, $countryCode, $vatNumber ),
											'timeout' => $timeout
							)
			);

			$ctx = stream_context_create ( $opts );
			$result = file_get_contents ( $url, false, $ctx );

			if (preg_match ( sprintf ( $pattern, 'checkVatResponse' ), $result, $matches )) {
					foreach ( $keys as $key )
							preg_match ( sprintf ( $pattern, $key ), $matches [2], $value ) && $response [$key] = $value [2];
			}
			return $response;
	}
	
	function checkValidVAT($allfields){
		$pers = ($allfields['maksatajs']['value'] == 'juridiska') ? 'j' : 'f';
		
			/*
			1.      Juridiska persona, PVN maksātājs un ES valsts -> Rēķinā 0% PVN precēm

			2.      Juridiska persona, nav PVN maksātājs un ES Valsts -> Rēķinā 21% PVN prcēm

			3.      Juridiska persona un nav ES valsts -> Rēķinā 0% PVN

			4.      Fiziska persona un ES valsts -> Rēķinā 21% PVN

			5.      Fiziska persona un nav ES valsts -> Rēķinā 0% PVN
			
			6.      Valsts LV pvn ANYWAY
		*/
		$check_country = $this->countries->GetValue(array('what'=>'in_eu','where'=>'code="'.$allfields[$pers.'_pvncountry']['value'].'"'));
			//1.
		if(!empty($allfields[$pers.'_pvnregnr']['value']) AND !empty($allfields[$pers.'_pvncountry']['value']) AND !empty($check_country)){
			$country = strtoupper($allfields[$pers.'_pvncountry']['value']);			
			$vat_nr = trim($allfields['j_pvnregnr']['value']);
			$code_check = substr($vat_nr, 0, 2);
			if($code_check == $country)
				$vat_nr = substr($vat_nr, 2);
			$check_pvn = $this->viesCheckVAT($country, $vat_nr);
			if($check_pvn['valid'])
				$pvn = 0;
			else
				$pvn = $this->pvn;
		}		
		//2.
		elseif(($pers == 'j') AND !empty($check_country) AND empty($allfields[$pers.'_pvnregnr']['value'])){
			$pvn = $this->pvn;
		}
		//3.
		elseif(($pers == 'j') AND empty($check_country)){
			$pvn = 0;//0%
		}
		//4.
		elseif(($pers == 'f') AND !empty($check_country)){
			$pvn = $this->pvn;
		}
		//5.
		elseif(($pers == 'f') AND empty($check_country)){
			$pvn = 0;//0%
		}
		//6.
		else
			$pvn = $this->pvn;
		if($allfields['j_pvncountry']['value'] == 'lv')
			$pvn = $this->pvn;
			
		$this->pvn = $pvn;
			
			
	}
	
	
  //=========================================================================//
  // Collection initialization routines
  //=========================================================================//

  function sayCurrency($sum, $lang, $currency){
    $sum = round($sum, 2);

    switch ($lang){
      case 'lv': return $this->saynum($sum, $this->lvsay);
      case 'en':
        require_once($GLOBALS['cfgDirRoot'].'/library/Numbers_Words/Words.php');
        return Numbers_Words::toCurrency($sum, 'en_US', $currency);

      case 'ru':
        require_once($GLOBALS['cfgDirRoot'].'/library/Numbers_Words/Words.php');
        return iconv('windows-1251', 'utf-8', Numbers_Words::toCurrency($sum, 'ru', $currency));
    }
    return '';
  }

  //=========================================================================//
  // Default properties
  //=========================================================================//

  function CanBeCompiled(){
    return False;
  }

  function CanLoadDesignDynamically(){
    return true;
  }

  function toolbar(&$script, &$toolbars, &$btnids, &$onclick){
    return true;
  }

  function SetProperties(){

    $this->properties = Array(

      "name"        => Array(
        "type"      => "string",
        "label"     => "Name:"
      ),

      "productcollection" => Array(
        "label"       => "Product collection:",
        "type"        => "collection",
        "collectiontype" => "shopprodcollection",
        "lookup"      => GetCollections($this->site_id, "shopprodcollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "usercollection" => Array(
        "label"       => "User collection:",
        "type"        => "collection",
        "collectiontype" => "shopusercollection",
        "lookup"      => GetCollections($this->site_id, "shopusercollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "ordercollection" => Array(
        "label"       => "Order collection:",
        "type"        => "collection",
        "collectiontype" => "EVeikalsOrderCollection",
        "lookup"      => GetCollections($this->site_id, "EVeikalsOrderCollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "orderprodcollection" => Array(
        "label"       => "Order collection:",
        "type"        => "collection",
        "collectiontype" => "shoporderprodcollection",
        "lookup"      => GetCollections($this->site_id, "shoporderprodcollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "regions" => Array(
        "label"       => "Region collection:",
        "type"        => "collection",
        "collectiontype" => "shopregionscollection",
        "lookup"      => GetCollections($this->site_id, "shopregionscollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "shops" => Array(
        "label"       => "Veikali:",
        "type"        => "collection",
        "collectiontype" => "EVeikalsShopCollection",
        "lookup"      => GetCollections($this->site_id, "EVeikalsShopCollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "statuses" => Array(
        "label"       => "Statuses collection:",
        "type"        => "collection",
        "collectiontype" => "EVeikalsOrderStatuses",
        "lookup"      => GetCollections($this->site_id, "EVeikalsOrderStatuses"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "order_history" => Array(
        "label"       => "Order history collection:",
        "type"        => "collection",
        "collectiontype" => "EVeikalsOrderHistory",
        "lookup"      => GetCollections($this->site_id, "EVeikalsOrderHistory"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),
			
			"countries" => Array(
        "label"       => "Countries collection:",
        "type"        => "collection",
        "collectiontype" => "EVeikalsCountriesCollection",
        "lookup"      => GetCollections($this->site_id, "EVeikalsCountriesCollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

			"payment_methods" => Array(
        "label"       => "Statuses collection:",
        "type"        => "collection",
        "collectiontype" => "EVeikalsPaymentMethods",
        "lookup"      => GetCollections($this->site_id, "EVeikalsPaymentMethods"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

    );

  }

}
