<?php

use LEDAkcijas\ProductAmountPriceCollection;

class shopmanager extends component{

    public $mobile_session = NULL;
    public $open_cats = array();

    function __construct($name)
    {

        parent::__construct($name);
        $this->SetProperties();
        $this->registerAsService('shopmanager');
        $this->registerAsBackendService('shopmanager');

        $this->catwebpath = option('Shop\\catwebpath', null, 'Catalog path', null, 'katalogs');
        $this->pvn = option('Shop\\pvn', null, 'PVN (%)', null, 22);

        $this->mobile_session = $_SESSION['mobile_session'];

        $this->use_automatic_prod_stock = option('Shop\\use_automatic_prod_stock', null, 'Use automatic product stock', Array("is_advanced" => true,'type' => 'check'));

        $this->initCollections();
        $this->getServices();

    }

  function visibleService(){
    return true;
  }

  function getLanguageStrings(){
    return Array(
      '_cena_gab' => 'gabalā',
      '_cena_iepak' => 'iepakojumā',
      '_apm_cash_or_transfer' => 'Skaidrā naudā vai ar pārskaitījumu',
      '_apm_firstdata' => 'Norēķinu karte',
      '_apm_els' => 'E-LS / norēķinu karte',
    );
  }

  function canAddProductFromList($prod){

    $prod = (is_array($prod))? $prod : $this->prodscol->getItemByIDAssoc($prod);

    if($prod['has_prices']) return false;
    if($prod['has_colors']) return false;
    if($prod['has_sizes']) return false;

    return true;

  }

  function getServices()
  {
    $this->bigcart = EVeikalsBigCart::getInstance();
    $this->CurrencyMenu = EVeikalsCurrencyMenu::getInstance();
  }

  function execute(){

    if (!$this->initCollections()) return;

    $id = $this->orderscol->getNextId();

    $this->processShopPath();

    if($_GET['action'] == 'set_mobile_session'){
      $this->mobile_session = $_SESSION['mobile_session'] = $_GET['on']? true : false;
      die(json_encode($_SESSION['mobile_session']));
    }

    $this->user = $_SESSION['logged_in'] ? $this->userscol->GetItemByIdAssoc(intval($_SESSION['logged_in']['item_id'])) : false;

  }

  function addGiftCard($data){
    $this->bigcart->addGiftCard($data);
  }

  function buildCatTree(&$data, $parent)
  {
    $list = Array();

    foreach (array_keys($data) as $key)
    {
      $row = &$data[$key];
      if ($row['parent'] != $parent) continue;

      $row['children'] = $this->buildCatTree($data, $row['item_id']);
      if ($row['parent'])
        $row['p'] = &$data[$row['parent']];

      $list[$key] = &$row;
    }
    return $list;
  }

  function getCatTree()
  {
    $data = $this->catscol->getAllCategories($this->prodscol);

    $ndata = Array();
    foreach ($data as $row)
    {
      $ndata[$row['item_id']] = $row;
    }

    return array(
      $this->buildCatTree($ndata, 0),
      $ndata
    );
  }

  function getSumAndGoodsList(&$goods, &$sum, &$hasgoods){

    $goods = Array();
    $sum = 0;
    $hasgoods = false;
    if($_SESSION['cartitems'] && count($_SESSION['cartitems'])){
      foreach($_SESSION['cartitems'] as $key => $val){
        $item_id = $val['id'];
        $item = $this->prodscol->getItemByIDAssoc($item_id);
        if($item['item_id'] == $item_id && $val && !$item['disabled']){

          $citem = $this->srvGetCartItem($val, $item, $this->user, $this->lang, $key);

            $goods[] = $citem;

          $hasgoods = true;
          $sum += $val['count'] * $citem['price'];
        }
      }
    }

    // giftcards
    if(count($_SESSION['giftcards'])){

      foreach($_SESSION['giftcards'] as $card){

        $hasgoods = true;
        $sum += $card['sum'];
        $card['giftcard'] = true;
        $card['count'] = $card['count']? $card['count'] : 1;
        $card['name'] = replace_all($this->l("Dāvanu karte([sum] [currency])"), array("[sum]" => $card['sum'], "[currency]" => $this->CurrencyMenu->default['label']));
        $card['price'] = $card['sum'];
        $card['key'] = $card['key'];
        $goods[] = $card;

      }

    }


  }

  function srvGetCartItem($val, $item, $user, $lang, $key=''){
    if (!$this->initCollections()) return null;

    $item = $this->getProductInfo($item, self::INFO_PRICES_ALL);

    $price = $item['price'];
    $undiscounted_price = $item['undiscounted_price'];
    $old_price = $item['old_price'];

    $price_id = (int)$val['price_id'];
    #dump($price_id);
    $price_row = null;

    if ($price_id){
      $prow = $this->pricescol->getItemByIdAssoc($price_id);
      if ($prow['product_id'] == $item['item_id']){
        $price_row = $prow;
        list($price, $old_price, $undiscounted_price) = $this->getProductPrice($item, $user, $prow['price'], $prow['saleout_price']);
      }
    }

    #dump($price);

    $additional_params = array();

    if($val['color']){
     $color = $this->colors->getItemByIDAssoc($val['color']);
     if($color){
       $additional_params[] = $color['title_'.$lang];
     }
    }

    if($val['size']){
     $size = $this->sizes->getItemByIDAssoc($val['size']);
     if($size){
       $additional_params[] = $size['title_'.$lang];
     }
    }

    $pname = '';
    if ($price_row){
      $additional_params[] = $price_row['name'];
    }

    return Array(
     'id' => $item['item_id'],
     'key' => $key,
     'name' => $item['name_' . $lang].(!empty($additional_params) ? ' ('.implode(", ",$additional_params).')' : ''),
     'title' => $item['title_' . $lang],
     'count' => $this->use_automatic_prod_stock ? (($val['count'] > $item['prod_count']) ? $item['prod_count'] : $val['count']) : $val['count'],
		 'too_much_in_cart' => $this->use_automatic_prod_stock ? (($val['count'] > $item['prod_count']) ? true : false) : false,

     'price' => $price,
     'undiscounted_price' => $undiscounted_price,
     'old_price' => $old_price,

     'price_id' => $price_id,
     'price_row' => $price_row,

     'cat' => $item['category_id'],
     'pvn' => $item['pvn'],
     'product' => $item,
     'unit' => $this->unitsText($item, false, false),
     'color' => $color,
     'size' => $size,
    );

  }

  // getProductInfo() $flags:
  const INFO_PRICE = 1;   // additional elements: price, old_price, undiscounted_price
  const INFO_PICS = 2;    // pictures
  const INFO_PARAMS = 4;  // parameters
  const INFO_COLORS = 8;  // colorsdata
  const INFO_URLS = 16;   // addbasket_url
//  const INFO_RELATEDPRODS = 32;   // relatedprods
  const INFO_PRICES_ALL = 64;   // prices

  const INFO_ALL = 127;

  // $prod: (array) product row or (int) product item_id
  function getProductInfo($prod, $flags=0)
  {

    static $cache = Array();

    $prod_id = is_array($prod) ? $prod['item_id'] : $prod;
    $ckey = $this->site_id.'_'.$this->collection_id.'_'.$prod_id.'_'.$flags;

    if (isset($cache[$ckey]))
      return $cache[$ckey];

    $prod = $this->prodscol->getProductInfo($prod);

    // INFO_PRICE
    if ($flags & (self::INFO_PRICE | self::INFO_PRICES_ALL) )
    {
      if ($prod['pack_price'])
      {
        list($price, $old_price, $undiscounted_price) = $this->getProductPrice($prod, $this->user, $prod['pack_price'], 0);
        $prod['pack_price'] = $price;
        $prod['pack_old_price'] = $old_price;
        $prod['pack_undiscounted_price'] = $undiscounted_price;
      }

      list($price, $old_price, $undiscounted_price) = $this->getProductPrice($prod, $this->user, $prod['price'], $prod['sale_price']);
      $prod['price'] = $price;
      $prod['old_price'] = $old_price;
      $prod['undiscounted_price'] = $undiscounted_price;
    }

    // INFO_PRICES_ALL
    if ($flags & self::INFO_PRICES_ALL)
    {
      if ($this->pricescol)
      {
        $prod['prices'] = $this->pricescol->getPricesByProduct($prod['item_id']);
        foreach ($prod['prices'] as $key => $pr)
        {
          list($price, $old_price, $undiscounted_price) = $this->getProductPrice($prod, $this->user, $pr['price'], $pr['saleout_price']);

          $prod['prices'][$key]['price'] = $price;
          $prod['prices'][$key]['old_price'] = $old_price;
          $prod['prices'][$key]['undiscounted_price'] = $undiscounted_price;
        }
      }

    }

    // INFO_PICS
    if ($flags & self::INFO_PICS && $this->picscol)
    {
      $data = $this->picscol->getImagesForProduct($prod_id);
      foreach ($data as $key => $row)
      {
        $row['title'] = $row['code_'.$this->lang];
        $data[$key] = $row;
      }
      $prod['pictures'] = $data;
    }

    // INFO_PARAMS
    if ($flags & self::INFO_PARAMS && $this->paramscol)
    {
      //visi lietotaaja nodefineetie parametri
      $allparams = $this->paramscol->getGroupedData($this->lang);
      //masiivs, atsleegas = parametra id, veertiibas = 1, ja parametrs izmantots
      //shajaa kategorijaa
      $catparams = $this->paramscol->getCategoryParameters($prod['category_id']);
      //masiivs, atsleegas = parametra id, veertiibas = parametru veertiibas produktam
      $paramvals = $this->paramscol->getProductParameterValues($prod['item_id']);
      $paramtable = Array();
      foreach($allparams as $param)
      {
        if($param['visible'])
        {
          //if($catparams[$param['item_id']])
          //{
            if($param['type'] == 0)
            {
              if($paramvals[$param['item_id']]['value'] == 'on')
                $value = '+';
              else if($paramvals[$param['item_id']]['value'] == 'off')
                $value = '-';
              else
                $value = '';
              $p = Array('t' => $param['title_' . $this->lang], 'v' => $value, 'type' => $param['type'], 'g' => $param['group_' . $this->lang]);
            }else{
              $p = Array('t' => $param['title_' . $this->lang], 'v' => $paramvals[$param['item_id']]['value'], 'type' => $param['type'], 'g' => $param['group_' . $this->lang]);
            }

            if (!empty($p['t']) || !empty($p['v']))
              $paramtable[] = $p;

          //}
        }
      }

      $prod['parameters'] = $paramtable;
    }

    // INFO_URLS
    if ($flags && self::INFO_URLS)
    {
      $prod['addbasket_url'] = '?addtobasket='.$prod_id.'&page='.(int)$_GET['page'];
      $prod['url'] = $this->getProductURL($prod);
    }

    $cache[$ckey] = $prod;
    return $prod;
  }

  function getProductPicture($prod){

    $this->initCollections();
    $prod = is_array($prod)? $prod : $this->prodscol->getItemByIDAssoc($prod);

		if($prod['shortcut']){
			return $this->getProductPicture($prod['shortcut']);
		}

    return $prod['picture'];

  }

  function getProductPictures($prod){

    $this->initCollections();
    $pics = array();

    $data = $this->picscol->getImagesForProduct($prod['shortcut'] ? $prod['shortcut'] : $prod['item_id']);

    foreach ($data as $key => $row){
      $pics[] = array(
        'id' => $row['item_id'],
        'title' => $row['code_'.$this->lang],
        'picture' => $row['image']
      );
    }

    return $pics;
  }

  function getShopBaseURL($lang='')
  {
    if (!$lang) $lang = $this->lang;
    return '/'.$lang.'/'.$this->catwebpath.'/';
  }

	function getCatalogURLPrefix($lang = false){

  	if (!$lang) $lang = $this->lang;
		return '/'.$lang.'/';

  }

  function getCategoryURL($cat_id, $lang='', $onlycatpath = false)
  {
    static $urls = Array();

    if (!$cat_id) return '';
    if (!$lang) $lang = $this->lang;

    $prefix = $this->getCatalogURLPrefix($lang);
    $url_id = $cat_id.':'.$lang;

    if (isset($urls[$url_id])){
      if($onlycatpath){
        return $urls[$url_id];
      }else{
        return $prefix . $urls[$url_id];
      }
    }

    if (!$this->catscol)
    {
      if (!($this->catscol = $this->initPropertyCollection('cat_collection') ))
        return '';

    }


    $path = $this->catscol->getCategoryPath($cat_id); 
    if (!$path) return '';

    $url = '';
    foreach ($path as $cat)
    {
      $url .= ($cat['url_'.$lang] != '') ? $cat['url_'.$lang].'/' : 'c'.$cat['item_id'].'/';
    }

    $urls[$url_id] = $url;
    if($onlycatpath){
      return $urls[$url_id];
    }else{
      return $prefix . $urls[$url_id];
    };
  }

    function createProductUrl($prod, $lang = false, $update = true){

        $prod = is_array($prod) ? $prod : $this->prodscol->getItemByIdAssoc($prod, true);
        $lang = $lang ? $lang : $this->lang;

        $prod['url_'.$lang] = $prod['name_'.$lang] ? $this->transliteratePageName($prod['name_'.$lang]) : "p".$prod['item_id'];

        $dup_prod = $this->prodscol->getCategoryProductByURL($prod['category_id'], $lang, $prod['url_'.$lang], $prod['item_id']);

        if($dup_prod){ // we have item with this url in this cat
        $i = 1;
        do{

            $url = $prod['url_'.$lang]."-".($i++);
            $dup_prod = $this->prodscol->getCategoryProductByURL($prod['category_id'], $lang, $url, $prod['item_id']);

        }while($dup_prod);
            $prod['url_'.$lang] = $url;
        }

        if($update){
            $this->prodscol->Update(array("url_".$lang => $prod['url_'.$lang]), array("item_id" => $prod['item_id']));
        }

        return $prod['url_'.$lang];

    }

  // $prod: (Array product_row) or (int product_id)
  function getProductURL($prod, $lang='', $onlycatpath = false){

    static $urls = Array();

    if (!$prod) return '';
    $prod_id = is_array($prod) ? $prod['item_id'] : $prod;
		$prod = is_array($prod) ? $prod : $this->prodscol->getItemByIdAssoc($prod, true);
    if (!$lang) $lang = $this->lang;

    $url_id = $prod_id.':'.$lang;

    if (isset($urls[$url_id])) return $urls[$url_id];

    $url = $this->getCategoryURL($prod['category_id'], $lang, $onlycatpath);

    $prod['url_'.$lang] = $prod['url_'.$lang] ? $prod['url_'.$lang] : $this->createProductUrl($prod, $lang);

    $url .= $prod['url_'.$lang];

    $urls[$url_id] = $url;
    return $url;

  }

  static function transliteratePageName($name){
    return transliterateURL($name, '-_0123456789abcdefghijklmnopqrstuvwxyz');
  }

  function createSemanticURLsForAllCatalog()
  {
    $langs = Array();
    $data = sqlQueryData("SELECT shortname FROM `".$this->site_id."_languages");
    foreach ($data as $row)
    {
      $langs[] = $row['shortname'];
    }

    $r = sqlQuery("SELECT * FROM `".$this->catscol->table."`");
    while ($row = db_fetch_array_assoc($r))
    {
      $item = Array();
      foreach ($langs as $lang)
      {
        $item['url_'.$lang] = shopmanager::transliteratePageName($row['title_'.$lang]);
      }
      $this->catscol->changeItemByIdAssoc($row['item_id'], $item);
    }

    $r = sqlQuery("SELECT * FROM `".$this->prodscol->table."`");
    while ($row = db_fetch_array_assoc($r))
    {
      $item = Array();
      foreach ($langs as $lang)
      {
        $item['url_'.$lang] = shopmanager::transliteratePageName($row['name_'.$lang]);
      }

      $this->prodscol->changeItemByIdAssoc($row['item_id'], $item);
    }
  }

  function repairEqualURLs()
  {
    // produktiem ar vienadiem urljiem piekabina "-"+$id

    $r = mysql_query("SELECT * FROM ".$this->prodscol->table);

    while ($row = mysql_fetch_assoc($r))
    {
      $urls = Array();
      foreach ($row as $key => $val)
      {
        if (substr($key, 0, 4) != 'url_') continue;
        if (empty($val)) continue;

        $urls[] = $key.'="'.addslashes($val).'"';
      }

      $r2 = mysql_query("SELECT * FROM ".$this->prodscol->table." WHERE category_id=".$row['category_id']." AND ".implode(' OR ', $urls)." AND item_id!=".$row['item_id']);
      $dups = Array();

      while ($row2 = mysql_fetch_assoc($r2))
      {
        if ($row2['item_id'] == $row['item_id']) continue;
        $dups[] = $row2;
      }

      if ($dups)
      {
        echo 'Prod '.$row['item_id'].' &nbsp; '.$row['name_lv'].':<br/>';
        foreach ($dups as $d)
        {
          echo '&nbsp;&nbsp;&nbsp;'.$d['item_id'].' &nbsp; '.$d['name_lv'].'<br/>';
    //      sqlQuery('UPDATE 4_coltable_shop_products SET url_lv=CONCAT(url_lv, "-", item_id), url_ru=CONCAT(url_ru, "-", item_id), url_en=CONCAT(url_en, "-", item_id) WHERE item_id='.$d['item_id']);
        }
      }


    }

  }

  function isProductTop($prod){

    $prod = (is_array($prod))? $prod : $this->prodscol->getItemByIDAssoc($prod);

    return $prod['is_top']? true : false;

  }

  function isProductNew($prod){

    $prod = (is_array($prod))? $prod : $this->prodscol->getItemByIDAssoc($prod);

    if(!$prod['is_new']) return false;

    if($prod['new_until']){ // end date specified

      if(strtotime($prod['new_until']) > time()){ // end date in future
        return true;
      }else{ // end date in past
        return false;
      }

    }else{ // no end date specified
      return true;
    }

  }

  function isProductFeatured($prod){

    $prod = (is_array($prod))? $prod : $this->prodscol->getItemByIDAssoc($prod);

    if ($prod['featured']){
      $t = time();
      $featured_from = $prod['featured_from'] == '0000-00-00' ? 0 : strtotime($prod['featured_from']);
      $featured_to = $prod['featured_to'] == '0000-00-00' ? 0 : strtotime($prod['featured_to']);
      $from_valid = !$featured_from || $t >= $featured_from;
      $to_valid = !$featured_to || $t < ($featured_to + 86400);

      if ($from_valid && $to_valid && $prod['sale_price'] > 0){
        return true;
      }
    }

    return false;

  }

    //returns ['price' => $price, 'base_price' => $base_price]
    public function getVolumePrice($volume, $user = false, $count = 1)
    {

        $base_price = $price = $item['price'];
        $product = $this->prodscol->GetById($volume['product_id']);

        if($this->isProductFeatured($product) && $volume['saleout_price'] > 0){ // on sale ?
            $price = $volume['saleout_price'];
        }elseif($user && $user['discount']){ // user discount ?
            $price = ((100 - $user['discount']) / 100) * $price;
        }else{ // some category discounts

            $cat_discount = $user ? $this->usercatdiscountcol->getDiscountForCat($item['category_id'], $user['item_id']) : 0;

            if($cat_discount){
                $price = ((100 - $cat_discount) / 100) * $price;
            }else{

                $category = $this->catscol->GetById($item['category_id']);
                if($category && $category['cat_atlaide']){ // cat_atlaide, where are you?
                    $price = ((100 - $category['cat_atlaide']) / 100) * $price;
                }

            }

        }

        return ['price' => $price, 'base_price' => $base_price];

    }

    //returns ['price' => $price, 'base_price' => $base_price]
    public function getProductPrice($item, $user = false, $count = 1)
    {

        static $cache;
        $cache_key = $item['item_id'].'_'.($user ? $user['item_id'] : 0).'_'.$count;

        if(isset($cache[$cache_key])) return $cache[$cache_key];

        $base_price = $item['price'];

        if($this->isProductFeatured($item) && $item['sale_price'] > 0){ // on sale ?
            $price = $item['sale_price'];
        }

        if(!isset($price) && $user && $user['discount']){ // no price yet and user discount ?
            $price = ((100 - $user['discount']) / 100) * $price;
        }

        if(!isset($price)){ // no price yet, some category discounts

            $cat_discount = $user ? $this->usercatdiscountcol->getDiscountForCat($item['category_id'], $user['item_id']) : 0;

            if($cat_discount){
                $price = ((100 - $cat_discount) / 100) * $price;
            }else{

                $category = $this->catscol->GetById($item['category_id']);
                if($category && $category['cat_atlaide']){ // cat_atlaide, where are you?
                    $price = ((100 - $category['cat_atlaide']) / 100) * $price;
                }

            }

        }

        if(!isset($price) && $count > 1){ // amount discount

            $product_steps = ProductAmountPriceCollection::getInstance()->getByProduct($item['item_id']);
            foreach($product_steps as $step){
                if($step['price'] > 0 && $step['count_from'] <= $count && (!$step['count_to'] || $step['count_to'] >= $count)){
                    $price = $step['price'];
                    break;
                }
            }

        }

        $price = isset($price) ? $price : $base_price; // if no price yet, use base price

        $cache[$cache_key] = ['price' => $price, 'base_price' => $base_price];

        return $cache[$cache_key];

    }

  //returns array (price, old_price, undiscounted_price)
  function getProductPrice_old($item, $user = false, $price = false, $sale_price = false){

    if($user == false && $_SESSION['logged_in']['item_id']){
      $user = $this->userscol->GetItemByIdAssoc(intval($_SESSION['logged_in']['item_id']));
    }

    $price = $price ? $price : $item['price'];
    $sale_price = $sale_price ? $sale_price : $price;

    $sale_price = (float)$sale_price;
    $price = (float)$price;
    $old_price = false;

    $undiscounted_price = $price;

    if ($sale_price && $this->isProductFeatured($item)){
      $old_price = $price;
      $price = $sale_price;
    }

    $userprice = $price;
    $usercatprice = $price;
    $catprice = $price;

    $fullprice = $price;

    if (!$this->isProductFeatured($item) && $user && $user['discount']){

       $userprice = (100 - $user['discount']) * $fullprice / 100;

      if($this->usercatdiscountcol){
        $cat_discount = $this->usercatdiscountcol->getDiscountForCat($item['category_id'], $user['item_id']);
        if($cat_discount){
          $usercatprice = (100 - $cat_discount) * $fullprice / 100;
          $old_price = $price;
        }
      }

        // $category, where are you?
      if($category && $category['cat_atlaide']){
        $catprice = (100 - $category['cat_atlaide']) * $fullprice / 100;
        $old_price = $price;
      }

    }

    $price = min($price, $userprice, $catprice, $usercatprice);
    return array($price, $old_price, $undiscounted_price);
  }

  function srvGetProductPrice($item, $user)
  {
    $this->initCollections();
    return $this->getProductPrice($item, $user, $item['price'], $item['sale_price']);
  }

  function unitsText($prod, $longname=false)
  {
    static $cache = Array();

    $count = empty($prod['package_items']) ? 1 : $prod['package_items'];
    if (!$prod['units'] || !$this->unitscol)
      return $count;

    $ckey = $this->site_id.'_'.$this->unitscol->collection_id.'_'.$prod['units'];
    if (!isset($cache[$ckey]))
      $cache[$ckey] = $this->unitscol->getItemByIdAssoc($prod['units']);

    if ($longname)
      return $cache[$ckey]['unit_'.$this->lang];

    return $count.' '.$cache[$ckey]['shortname_'.$this->lang];
  }

  function formatPrice($price)
  {
    $s = number_format($price, 2, '.', ' ');

    return $s;
  }

  function trimZeros($n)
  {
    $s = (string)$n;
    $a = explode('.', $s);
    $a[1] = rtrim($a[1], '0');

    if (strlen($a[1]))
      return $a[0].'.'.$a[1];
    else
      return $a[0];

  }
  
  function initCollections()
  {
    if ($this->prodscol) return true;
    $this->prodscol = $this->initPropertyCollection('prod_collection');
    $this->catscol = $this->initPropertyCollection('cat_collection');
    $this->userscol = $this->initPropertyCollection('user_collection');

    if (!$this->prodscol || !$this->catscol || !$this->userscol)
      return false;

    $this->usercatdiscountcol = $this->initPropertyCollection('user_cat_discount_collection');
    $this->picscol = $this->initPropertyCollection('customimages_collection');
    $this->paramscol = $this->initPropertyCollection('parameters_collection');
    $this->relatedprodscol = $this->initPropertyCollection('relatedprodscol');
    $this->pricescol = $this->initPropertyCollection('pricescol');
    $this->unitscol = $this->initPropertyCollection('units_collection');
    $this->orderscol = $this->initPropertyCollection('orderscol');
    $this->orderprodscol = $this->initPropertyCollection('orderprodscol');
    $this->brandscol = $this->initPropertyCollection('brandscol');
    $this->colors = $this->initPropertyCollection('colors');
    $this->sizes = $this->initPropertyCollection('sizes');

    $this->product_sizes = $this->initPropertyCollection('product_sizes');
    $this->product_colors = $this->initPropertyCollection('product_colors');

    $this->user = $_SESSION['logged_in']? $_SESSION['logged_in'] : NULL;
    $this->mobile_session = $_SESSION['mobile_session'];

    # $this->processShopPath();

    return true;
  }

  public function output()
  {
      
  }

  function SetProperties()
  {
    $this->properties = Array(
      "name"        => Array(
        "type"      => "string",
        "label"     => "Name:"
      ),

      "prod_collection" => Array(
        "label"       => "Product collection:",
        "type"        => "collection",
        "collectiontype" => "shopprodcollection",
        "lookup"      => GetCollections($this->site_id, "shopprodcollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "user_collection" => Array(
        "label"       => "User collection:",
        "type"        => "collection",
        "collectiontype" => "shopusercollection",
        "lookup"      => GetCollections($this->site_id, "shopusercollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "cat_collection" => Array(
        "label"       => "Category collection:",
        "type"        => "collection",
        "collectiontype" => "shopcatcollection",
        "lookup"      => GetCollections($this->site_id, "shopcatcollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "user_cat_discount_collection" => Array(
        "label"       => "User category discount collection:",
        "type"        => "collection",
        "collectiontype" => "shopusercatatlaidescollection",
        "lookup"      => GetCollections($this->site_id, "shopusercatatlaidescollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "customimages_collection" => Array(
        "label"       => "Product images collection:",
        "type"        => "collection",
        "collectiontype" => "shopcustomimagescollection",
        "lookup"      => GetCollections($this->site_id, "shopcustomimagescollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "parameters_collection" => Array(
        "label"       => "Parameters collection:",
        "type"        => "collection",
        "collectiontype" => "shopparameterscollection",
        "lookup"      => GetCollections($this->site_id, "shopparameterscollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "relatedprodscol" => Array(
        "label"       => "Related products collection:",
        "type"        => "collection",
        "collectiontype" => "shoprelatedprodscollection",
        "lookup"      => GetCollections($this->site_id, "shoprelatedprodscollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "pricescol" => Array(
        "label"       => "Prices collection:",
        "type"        => "collection",
        "collectiontype" => "shoppricescollection",
        "lookup"      => GetCollections($this->site_id, "shoppricescollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "units_collection" => Array(
        "label"       => "Units collection:",
        "type"        => "collection",
        "collectiontype" => "shopunitscollection",
        "lookup"      => GetCollections($this->site_id, "shopunitscollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "orderscol" => Array(
        "label"       => "Order collection:",
        "type"        => "collection",
        "collectiontype" => "EVeikalsOrderCollection",
        "lookup"      => GetCollections($this->site_id, "EVeikalsOrderCollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "orderprodscol" => Array(
        "label"       => "Order products collection:",
        "type"        => "collection",
        "collectiontype" => "shoporderprodcollection",
        "lookup"      => GetCollections($this->site_id, "shoporderprodcollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "brandscol" => Array(
        "label"       => "Brendi:",
        "type"        => "collection",
        "collectiontype" => "shopbrandscollection",
        "lookup"      => GetCollections($this->site_id, "shopbrandscollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "colors" => Array(
        "label"       => "Color collection:",
        "type"        => "collection",
        "collectiontype" => "EVeikalsColorCollection",
        "lookup"      => GetCollections($this->site_id, "EVeikalsColorCollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "sizes" => Array(
        "label"       => "Size collection:",
        "type"        => "collection",
        "collectiontype" => "EVeikalsSizeCollection",
        "lookup"      => GetCollections($this->site_id, "EVeikalsSizeCollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "product_sizes" => Array(
        "label"       => "Product size collection:",
        "type"        => "collection",
        "collectiontype" => "EVeikalsProductSizeCollection",
        "lookup"      => GetCollections($this->site_id, "EVeikalsProductSizeCollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "product_colors" => Array(
        "label"       => "Product color collection:",
        "type"        => "collection",
        "collectiontype" => "EVeikalsProductColorCollection",
        "lookup"      => GetCollections($this->site_id, "EVeikalsProductColorCollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

    );
  }
}




?>