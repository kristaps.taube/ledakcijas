<?
/**
*
*  Title: Datateks.lv Constructor component
*  Author: Kristaps Taube <ktaube@datateks.lv>
*  Year: 2011
*  Projet: Solrex Energy
*
*/

include_once("class.component.php");

class GoogleMap extends component
{
  function GoogleMap($name)
  {
    parent::__construct($name);
    $this->SetProperties();
  }

  function getLanguageStrings()
  {
    $ls = Array(

    );
    $this->parseTemplatePropertiesForLanguageStrings($ls);
    return $ls;
  }

  function output()
  {
    $coords = explode(",",$this->getProperty("coords"));
    $map_markers = Array();
    $map_markers[] = array(
        'name' => $this->getProperty("title"),
        'lat' => $coords[0],
        'lng' => $coords[1],
        'icon' => "/images/html/logo_small.png"
        # 'icon' => $this->getProperty("icon")  // this should be fixed
      );

   ?>
   <div class="map">
      <div id="map_canvas" style='width: <?=$this->getProperty("width")?>;height: <?=$this->getProperty("height")?>;'></div>
    </div>

   <script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
   <script type="text/javascript">
      var gm = google.maps;
      var map_markers = <?=json_encode($map_markers)?>;
      var latlng = new gm.LatLng(<?=$coords[0]?>, <?=$coords[1]?>);
      init_map(latlng);

      function init_map(latlng){

        map = new gm.Map(document.getElementById("map_canvas"), {
          zoom: 10,
          center: latlng,
          mapTypeId: gm.MapTypeId.ROADMAP
        });

        map.scrollwheel = false;

        for (var i=0; i<map_markers.length; i++)
        {
          var b = map_markers[i];
          var icon = new gm.MarkerImage(b.icon, new gm.Size(25, 25) );
          var m = new gm.Marker({
            position: new gm.LatLng(b.lat, b.lng),
            icon: icon,
            map: map,
            title: b.name
          });

        }

      }
    </script>
   <?

  }

  function SetProperties()
  {
    $this->properties = Array(
      "name"        => Array(
        "label"     => "Name:",
        "type"      => "str"
      ),

      "coords"        => Array(
        "label"     => "Coords:",
        "type"      => "str"
      ),

      "title"        => Array(
        "label"     => "Title:",
        "type"      => "str"
      ),

      "width"        => Array(
        "label"     => "Width:",
        "type"      => "str",
        "value" => "100%"
      ),

      "height"        => Array(
        "label"     => "Height:",
        "type"      => "str",
        "value" => "300px"
      ),


    );
  }



}



?>