<?php
/**
*
*  Title: Design Colors
*  Author: Kristaps Taube <ktaube@datateks.lv>
*  Date: 04.02.2014
*  Project: E-Veikals
*
*/

include_once("class.component.php");

class EVeikalsDesignColors extends component{

    function __construct($name){
        parent::__construct($name);
        $this->SetProperties();
        $this->registerAsBackendService("DesignColors");
    }

  function init(){

    $this->colors = array(
      "Green" => array("name" => "Green", "hex" => "#8ac53f"),
      "Red" => array("name" => "Red", "hex" => "#ee4735"),
      "Black" => array("name" => "Black", "hex" => "#000000"),
      "Blue" => array("name" => "Blue", "hex" => "#32afdc"),
      "Yellow" => array("name" => "Yellow", "hex" => "#f7ba15")
    );

    $this->color = $this->colors[$this->getProperty('color')];

  }

  function execute(){

    $this->init();
    $GLOBALS['designcolor'] = strtolower($this->color['name']);

  }

  function design(){

    $this->init();

    if(isset($_POST['save'])){
      $this->setDefaultProperty('color', $_POST['color']);
      $this->color = $this->colors[$_POST['color']];
      echo "Saved<br />";
    }

    ?>
      <form method='post' action=''>
        Color:
        <select name='color'>
          <? foreach($this->colors as $key => $color){ ?>
          <option value='<?=$key?>' <?=($this->color['name'] == $key)? "selected='selected'" : "" ?>><?=$color['name']?></option>
          <? } ?>
        </select>
        <input type='submit' value='Save' name='save' />
      </form>
			<script type='text/javascript'>
        var design_color_hex = <?=json_encode($this->color['hex'])?>;
      </script>
    <?


  }

  function output(){

    $this->init();

    ?>
      <link rel="stylesheet" type="text/css" href="/scr/css/colors/<?=strtolower($this->color['name'])?>.css">
      <script type='text/javascript'>
        var design_color_hex = <?=json_encode($this->color['hex'])?>;
      </script>
    <?

  }

  function SetProperties(){
    $this->properties = Array(

      "name"        => Array(
        "label"     => "Name:",
        "type"      => "str"
      ),

      "color"        => Array(
        "label"     => "Name:",
        "type"      => "str"
      ),

    );
  }



}
