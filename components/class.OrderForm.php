<?php
/**
*
*  Title: OrderForm
*  Author: Kristaps Taube <ktaube@datateks.lv>
*  Date: 23.02.2018
*  Project: LEDAkcijas.lv
*
*/

use Constructor\WebApp;

class OrderForm extends component
{

    private $_fields;

    public function __construct($name = false)
    {

        parent::__construct($name);

        $this->init();

    }

    public function output($backend = false)
    {

        $fields = $this->getFields();
        $items = WebApp::$app->cart->getItems(true);
        $cart_sum = WebApp::$app->cart->getSum(true);

        $delivery_method = $this->getDeliveryMethod($fields, $items, $lang);
        $sum_discount_data = $this->processSumDiscount($cart_sum, $items);

        return $this->render('index', [
            'fields' => $fields,
            'errors' => $this->getErrors(),
            'cart' => WebApp::$app->cart,
            'omniva_data' => $this->getOmnivaData(),
            'pastastacija_data' => $this->getPastastacijaData(),
            'dpd_data' => $this->getDPDData(),
            'delivery_prices' => $this->getDeliveryPrices(),
            'delivery_discounts' => $this->getDeliveryDiscounts(),
            'sum_discounts' => $this->sum_discounts->getSumDiscounts(),
            'delivery_price' => $delivery_method['price'],
            'sum_discount_data' => $sum_discount_data,
            'post' => $_POST,
            'items' => $items,
            'map_markers' => $this->getMapMarkers(),
            'backend' => $backend
        ]);

    }

    public function loadFieldData($data = [])
    {

        foreach($this->_fields as $key => $field){

            if(isset($data[$key])){
                $this->_fields[$key]['value'] = $data[$key];
            }

        }

    }

    public function getFields()
    {
        return $this->_fields;
    }

    public function getFieldValues()
    {

        $fields = [];
        foreach($this->getFields() as $key => $field){
            $fields[$key] = $field['value'];
        }

        return $fields;

    }

    private function setFields()
    {

        $fields = [
            'payer' => [
                'label' => WebApp::l("Maksātājs"),
                'type' => 'radio',
                'required' => true,
                'value' => 'legal',
                'values' => [
                    'legal' => WebApp::l("Juridiska persona"),
                    'private' => WebApp::l("Fiziska persona"),
                ],
            ],
            'name' => [
                'label' => WebApp::l("Vārds, uzvārds"),
                'type' => 'text',
                'required' => function($fields){ return $fields['payer']['value'] == 'private'; }
            ],
            'company_name' => [
                'label' => WebApp::l("Uzņēmuma nosaukums"),
                'type' => 'text',
                'required' => function($fields){ return $fields['payer']['value'] == 'legal'; }
            ],
            'registration_nr' => [
                'label' => WebApp::l("Reģistrācijas numurs"),
                'type' => 'text',
                'required' => function($fields){ return $fields['payer']['value'] == 'legal'; }
            ],
            'vat_number' => [
                'label' => WebApp::l("PVN reģistrācijas numurs"),
                'type' => 'text',
            ],
            'phone' => [
                'label' => WebApp::l("Telefona nr."),
                'type' => 'text',
            ],
            'email' => [
                'label' => WebApp::l("E-pasta adrese"),
                'type' => 'text',
                'required' => true,
                'validate' => function($value, $fields){

                    if(!filter_var($value, FILTER_VALIDATE_EMAIL)){
                        $this->addError('email', WebApp::l("Nekorekta e-pasta adrese"));
                    }

                }
            ]
        ];

        # delivery methods
        $delivery_methods = [];
        foreach($this->shops->getDBData(["where" => "can_pickup = 1"]) as $shop){
            $delivery_methods[$shop['item_id']] = WebApp::l("Bez maksas saņemt veikalā ([address])", ['replace' => ['[address]' => $shop['title_'.$this->lang]]]);
        }

        $delivery_methods['courier'] = WebApp::l("Saņemt izvēlētajā adresē ar kurjeru")." <strong>(".WebApp::l("bezmaksas piegāde sākot no € [sum]", ['replace' => ['[sum]' => number_format($this->courier_free_delivery_from, 2)]]).")</strong>";
        $delivery_methods['pakomats'] = WebApp::l("Saņemt pakomātā")." <strong>(".WebApp::l("bezmaksas piegāde sākot no € [sum]", ['replace' => ['[sum]' => number_format($this->packs_free_delivery_from, 2)]]).")</strong>";

        $fields['delivery_method'] = [
            'label' => WebApp::l("Piegādes metodes"),
            'type' => 'radio',
            'value' => array_keys($delivery_methods)[0], // first key ?
            'values' => $delivery_methods
        ];

        $pakomats_types = [];
        $pakomats_types['omniva'] = WebApp::l("Saņemt OMNIVA pakomātā");
        $pakomats_types['pastastacija'] = WebApp::l("Saņemt Pastastacijas pakomātā");
        $pakomats_types['dpd'] = WebApp::l("Saņemt DPD pakomātā");
        $pakomats_types['circlek'] = WebApp::l("Saņemt CircleK");
        $pakomats_types['pasts'] = WebApp::l("Saņemt Pasta nodaļā");

        $fields['pakomats_type'] = [
            'label' => WebApp::l("Pakomāts"),
            'value' => '',
            'values' => $pakomats_types,
            'required' => function($fields){ return $fields['delivery_method']['value'] == 'pakomats'; }
        ];

        // courier fields
        $fields['city'] = [
            'label' => WebApp::l('Pilsēta'),
            'type' => 'text',
            'required' => function($fields){ return $fields['delivery_method']['value'] == 'courier'; }
        ];

        $fields['street'] = [
            'label' => WebApp::l('Iela'),
            'type' => 'text',
            'value' => '',
            'required' => function($fields){ return $fields['delivery_method']['value'] == 'courier'; }
        ];

        $fields['house_nr'] = [
            'label' => WebApp::l('Mājas nr.'),
            'type' => 'text',
            'value' => '',
            'required' => function($fields){ return $fields['delivery_method']['value'] == 'courier'; }
        ];

        $fields['flat_nr'] = [
            'label' => WebApp::l('Dzīvokļa nr.'),
            'type' => 'text',
            'value' => '',
            'required' => function($fields){ return $fields['delivery_method']['value'] == 'courier'; }
        ];

        $fields['zip'] = [
            'label' => WebApp::l('Pasta indekss'),
            'type' => 'text',
            'value' => '',
            'required' => function($fields){ return $fields['delivery_method']['value'] == 'courier'; }
        ];

        // pickup places
        $fields['omniva_city'] = [
            'label' => WebApp::l('Pilsēta'),
            'type' => 'list',
            'value' => 0,
            'values' => [],
            'required' => function($fields){ return $fields['pakomats_type']['value'] == 'omniva_packs'; }
        ];

        $fields['omniva_address'] = [
            'label' => WebApp::l('Adrese'),
            'type' => 'list',
            'value' => 0,
            'values' => [],
            'required' => function($fields){ return $fields['pakomats_type']['value'] == 'omniva_packs'; }
        ];

        $fields['pastastacija_city'] = [
            'label' => WebApp::l('Pilsēta'),
            'type' => 'list',
            'value' => 0,
            'values' => [],
            'required' => function($fields){ return $fields['pakomats_type']['value'] == 'pastastacija_packs'; }
        ];

        $fields['pastastacija_address'] = [
            'label' => WebApp::l('Adrese'),
            'type' => 'list',
            'value' => 0,
            'values' => [],
            'required' => function($fields){ return $fields['pakomats_type']['value'] == 'pastastacija_packs'; }
        ];

        $fields['dpd_city'] = [
            'label' => WebApp::l('Pilsēta'),
            'type' => 'list',
            'value' => 0,
            'values' => [],
            'required' => function($fields){ return $fields['pakomats_type']['value'] == 'dpd_packs'; }
        ];

        $fields['dpd_address'] = [
            'label' => WebApp::l('Adrese'),
            'type' => 'list',
            'value' => 0,
            'values' => [],
            'required' => function($fields){ return $fields['pakomats_type']['value'] == 'dpd_packs'; }
        ];

        $circlek_addresses = [];
        foreach($this->circlek->getDBData(['order' => 'name_'.$this->lang]) as $address){
            $circlek_addresses[$address['item_id']] = $address['name_'.$this->lang].'('.$address['address_'.$this->lang].')';
        }

        $fields['circlek_address'] = [
            'label' => WebApp::l('Adrese'),
            'type' => 'list',
            'value' => 0,
            'values' => $circlek_addresses,
            'required' => function($fields){ return $fields['pakomats_type']['value'] == 'circlek'; }
        ];

        $addresses = [];
        foreach($this->pasts->getDBData(['order' => 'name_'.$this->lang]) as $address){
            $addresses[$address['item_id']] = $address['name_'.$this->lang].'('.$address['address_'.$this->lang].')';
        }

        $fields['pasts_address'] = [
            'label' => WebApp::l('Adrese'),
            'type' => 'list',
            'value' => 0,
            'values' => $addresses,
            'required' => function($fields){ return $fields['delivery_method']['value'] == 'pasts'; }
        ];

        #payment methods
        $payment_methods = $this->getPaymentMethods(WebApp::$app->getLanguage());

        $fields['payment_method'] = [
            'label' => WebApp::l("Apmaksas metodes"),
            'type' => 'radio',
            'value' => array_keys($payment_methods)[0], // first key ?
            'values' => $payment_methods,
            'required' => true,
            'validate' => function($value, $fields){

                $result = true;

                if($value == 'in_shop'){

                    if(in_array($fields['delivery_method']['value'], ['courier', 'pakomats'])){
                        $result = false;
                    }

                }elseif($value == 'order'){

                }elseif($value == 'online'){

                }

                if(!$result){
                    $this->addError('payment_method', WebApp::l("Nekorekta apmaksas metode"));
                }


            }
        ];

        $this->_fields = $fields;

        return $fields;

    }

    private function getPaymentMethods($lang = false)
    {

        $lang = $lang ? $lang : App::$app->getLanguage();

        $payment_methods = [];

        $payment_methods['in_shop'] = WebApp::l("Apmaksa preces saņemšanas brīdī veikalā", ['lang' => $lang]);
        $payment_methods['order'] = WebApp::l("Priekšapmaksa pēc rēķina saņemšanas e-pastā", ['lang' => $lang]);
        #$payment_methods['card'] = WebApp::l("Apmaksa ar kredītkarti", ['lang' => $lang]);
        $payment_methods['online'] = WebApp::l("Apmaksa tiešsaistē", ['lang' => $lang]);

        return $payment_methods;

    }

    public function processCheckout($data)
    {

        $this->loadFieldData($data);

        if($this->validateFields()){

            $user = WebApp::$app->user->get();
            $lang = WebApp::$app->getLanguage();

            WebApp::$app->user->updateInfo($this->getFieldValues());

            $nextid = $this->orders->getNextId();
            $fields = $this->getFields();

            $products = WebApp::$app->cart->getItems(true);
            $product_sum = WebApp::$app->cart->getSum(true);

            $delivery_method = $this->getDeliveryMethod($fields, $products, $lang);

            $delivery_price = $delivery_method && isset($delivery_method['price']) ? $delivery_method['price'] : 0;

            $sum_discount_data = $this->processSumDiscount($product_sum, $products);

            $total_sum = $product_sum + $delivery_price - $sum_discount_data['sum'];

            $field_values = $this->getFieldValues();

            $order_data = [
                'products' => $products,
                'sum' => $total_sum,
                'allfields' => $field_values,
                'orderlang' => $lang,
                'user' => $user ? $user : false
            ];

            $new_status = $this->statuses->getNewStatusRow();

            $buyer = $fields['payer']['value'] == 'private' ? $fields['name']['value'] : $fields['company_name']['value'];

            $order = [
                "user_id" => $user ? $user['item_id'] : null,
                "datums" => date('Y-m-d H:i:s'),
                "summa" => $total_sum,
                "valoda" => $lang,
                "pasutitajs" => $buyer,
                "piegadats" => 0,
                "apmaksats" => 0,
                "apmaksas_veids" => $fields['payment_method']['value'],
                'kopeja_summa' => $total_sum,
                'order_data' => serialize($order_data),
                'telefons' => $fields['phone']['value'],
                'epasts' => $fields['email']['value'],
                'sent' => $this->send_at_checkout ? 1 : 0,
                "status_id" => $new_status ? $new_status['item_id'] : 0
            ];

            $result = $order_id = $this->orders->Insert($order);

            $num = $this->getOrderNumber($order_id);
            $order_html = $this->getHtml($field_values, $products, $user, $num, $lang, [], $sum_discount_data);

            $this->orders->Update(['rekins' => $order_html, 'num' => $num], $order_id);

            foreach($products as $item){

                $this->order_products->Insert([
                    'price' => $item['price'],
                    'base_price' => $item['base_price'],
                    'count' => $item['count'],
                    'prod_id' => $item['id'],
                    'order_id' => $order_id,
                    'size_id' => isset($prod['size']['item_id']) ? $prod['size']['item_id'] : 0,
                    'color_id' => isset($prod['color']['item_id']) ? $prod['color']['item_id'] : 0,
                    'cart_key' => $item['key']
                ]);

                 $product = $item['product'];
                 $update = [];
                 $update['prod_count'] = $product['prod_count'] - $item['count'];
                 $update['prod_count'] = $update['prod_count'] > 0 ? $update['prod_count'] : 0;
                 $update['times_purchased'] = $update['times_purchased'] + 1;

                 if($product['featured_count'] && !$update['prod_count']){
                    $update['featured'] = 0;
                 }

                 $this->products->Update($update, $product['item_id']);

            }

            $related_col = \shoprelatedprodscollection::getInstance();
            // proces related products
            foreach($products as $item){
                foreach($products as $item2){

                    if($item['id'] == $item2['id']){ // do not link with myself
                        continue;
                    }
                    $related_col->addRelation($item['id'], $item2['id']);

                }
            }

            $order = $this->orders->getById($order_id);

            // send orders
            $sender = \OrderSender::getInstance();
            $sender->sendToAdmin($order);

            // make xml
            $xml_gen = LEDOrderXMLGenerator::getInstance();
            $xml = $xml_gen->generate($order_id);
            $xml_gen->save('pasutijums-'.$order_id.'.xml', $xml);

            LEDAkcijasOrderStatusChangeNotifications::getInstance()->process($order_id, null, $new_status['item_id']);

            /*
            if($this->send_at_checkout){
                $sender->sendToClient($order);
            }
            */

            WebApp::$app->cart->clear();

            if(isset($data['subscribe'])){ // add to mailing list

                $mailinglist = mailinglistex::getInstance();

                if(!$mailinglist->serviceIsEmailSubscribed($fields['email']['value'])){
                    $cat = $mailinglist->catcollection->getAddByName(strtoupper($this->lang));
                    $mailinglist->AddEmail($buyer, $fields['email']['value'], $cat['item_id'], 1, $lang);
                }

            }

        }else{ // has some errors
            $result = false;
        }

        return $result;

    }

    private function getMapMarkers()
    {

        $lang = WebApp::$app->getLanguage();

        $conf = [
            'omniva' => [
                'defaultIcon' => '/images/html/pin_omniva.png',
                'selectedIcon' => '/images/html/pin_omniva_active.png'
            ],
            'pastastacija' => [
                'defaultIcon' => '/images/html/pin_pasta_stacija.png',
                'selectedIcon' => '/images/html/pin_pasta_stacija_active.png'
            ],
            'dpd' => [
                'defaultIcon' => '/images/html/pin_dpd.png',
                'selectedIcon' => '/images/html/pin_dpd_active.png'
            ],
            'circlek' => [
                'defaultIcon' => '/images/html/pin_circlek.png',
                'selectedIcon' => '/images/html/pin_circlek_active.png'
            ],
            'pasts' => [
                'defaultIcon' => '/images/html/pin_latvijas_pasts.png',
                'selectedIcon' => '/images/html/pin_latvijas_pasts_active.png'
            ],
        ];

        $markers = [];

        foreach($conf as $pickup => $p_conf){

            foreach($this->$pickup->getDBData(['assoc' => true]) as $item){

                if(!$item['coords']) continue;

                $coords = explode(",", $item['coords']);
                if(count($coords) != 2) continue;

                $marker = [];
                $marker['coords'] = ['lat' => (float)$coords[0], 'lng' => (float)$coords[1]];
                $marker['customProperties'] = [
                    'providerID' => $pickup,
                    'ppointID' => $item['item_id']
                ];

                if(isset($item['city_'.$lang])){
                    $marker['customProperties']['city'] = $item['city_'.$lang];
                }

                $marker['defaultIcon'] = $p_conf['defaultIcon'];
                $marker['selectedIcon'] = $p_conf['selectedIcon'];

                $markers[] = $marker;

            }

        }

        return $markers;

    }

    public function processSumDiscount($sum, $products)
    {

        $discount_percentage = $this->sum_discounts->getSumDiscount($sum);

        $discount_sum = 0;

        if($discount_percentage > 0){

            foreach($products as $product){

                if($product['base_price'] == $product['price']){
                    $discount_sum += ($product['price'] * $product['count'] * $discount_percentage);
                }

            }

        }

        return ['percentage' => $discount_percentage, 'sum' => $discount_sum];

    }

    public function getInvoiceNumber($order_id)
    {
        return $this->invoice_prefix.' '.$order_id;
    }

    public function getOrderNumber($order_id)
    {
        return $this->order_prefix.' '.$order_id;
    }

    public function getHtml($fields, $items, $user, $num, $lang, $order = [], $sum_discount_data = [], $invoice = false)
    {

        return $this->render('_order', [
            'lang' => $lang,
            'fields' => $fields,
            'items' => $items,
            'num' => $num,
            'user' => $user,
            'order' => $order,
            'delivery_method' => $this->getDeliveryMethod($fields, $items, $lang),
            'payment_method' => $this->getPaymentMethod($fields, $lang),
            'sum_discount_data' => $sum_discount_data,
            'invoice' => $invoice
        ]);

    }

    private function getPaymentMethod($fields, $lang)
    {

        $pm = $this->getPaymentMethods($lang);
        return isset($pm[$fields['payment_method']['value']]) ? $pm[$fields['payment_method']['value']] : '';
    }

    public function getDeliveryMethod($fields, $items, $lang)
    {

        $cart_sum = 0;
        foreach($items as $item){
            $cart_sum += ($item['price'] * $item['count']);
        }

        $key = $fields['delivery_method']['value'];

        $data = ['name' => '', 'sub' => [], 'price' => 0];

        if(is_numeric($key)){ // shop

            $shop = $this->shops->getByID($key);
            $data['name'] = WebApp::l("Bez maksas saņemt veikalā ([address])", ['replace' => ['[address]' => $shop['title_'.$lang]]]);;
            $data['address'] = $shop['title_'.$lang];
            $data['price'] = 0;

        }elseif($key == 'pakomats'){

            $pack_type = $fields['pakomats_type']['value'];

            if($pack_type == 'omniva'){

                $data['name'] = WebApp::l("Saņemt OMNIVA pakomātā", ['lang' => $lang]);
                $data['type'] = 'Omniva';

                $address = $this->omniva->getById($fields['omniva_address']['value']);

                if($address){
                    $data['sub']['label'] = WebApp::l("Pakomāta adrese", ['lang' => $lang]);
                    $data['sub']['value'] = $address['address_'.$lang];
                }

            }elseif($pack_type == 'pastastacija'){

                $data['name'] = WebApp::l("Saņemt Pastastacijas pakomātā", ['lang' => $lang]);
                $data['type'] = 'Pastastacija';

                $address = $this->pastastacija->getById($fields['pastastacija_address']['value']);

                if($address){
                    $data['sub']['label'] = WebApp::l("Pakomāta adrese", ['lang' => $lang]);
                    $data['sub']['value'] = $address['address_'.$lang];
                }

            }elseif($pack_type == 'dpd'){

                $data['name'] = WebApp::l("Saņemt DPD pakomātā", ['lang' => $lang]);
                $data['type'] = 'DPD';

                $address = $this->dpd->getById($fields['dpd_address']['value']);

                if($address){
                    $data['sub']['label'] = WebApp::l("Pakomāta adrese", ['lang' => $lang]);
                    $data['sub']['value'] = $address['address_'.$lang];
                }

            }elseif($pack_type == 'circlek'){

                $data['name'] = WebApp::l("Saņemt CircleK", ['lang' => $lang]);
                $data['type'] = 'CircleK';

                $address = $this->circlek->getById($fields['circlek_address']['value']);

                if($address){
                    $data['sub']['label'] = WebApp::l("Adrese", ['lang' => $lang]);
                    $data['sub']['value'] = $address['name_'.$lang].'('.$address['address_'.$lang].')';
                }

            }elseif($pack_type == 'pasts'){

                $data['name'] = WebApp::l("Saņemt Pasta nodaļā", ['lang' => $lang]);
                $data['type'] = WebApp::l("Latvijas pasts", ['lang' => $lang]);;

                $address = $this->pasts->getById($fields['pasts_address']['value']);

                if($address){
                    $data['sub']['label'] = WebApp::l("Pasta nodaļa", ['lang' => $lang]);
                    $data['sub']['value'] = $address['name_'.$lang].'('.$address['address_'.$lang].')';
                }

            }

            $data['price'] = ($cart_sum < $this->packs_free_delivery_from) ? $this->packs_price : 0;

        }elseif($key == 'courier'){

            $data['name'] = WebApp::l("Saņemt izvēlētajā adresē ar kurjeru", ['lang' => $lang]);
            $data['sub']['label'] = WebApp::l("Piegādes adrese", ['lang' => $lang]);
            $data['sub']['value'] = $fields['city']['value'].', '.$fields['street']['value'].' '.$fields['house_nr']['value'].'/'.$fields['flat_nr']['value'].', '.$fields['zip']['value'];

            $data['price'] = ($cart_sum < $this->courier_free_delivery_from) ? $this->courier_price : 0;

        }

        return $data;

    }

    public function validateFields()
    {

        $errors = [];

        foreach($this->_fields as $key => $field){

            $is_required = false;

            if(isset($field['required'])){ // this field has required rule

                #$is_required = (is_bool($this->allfields[$key]['required']) && $this->allfields[$key]['required']) || is_callable($this->allfields[$key]['required']) && $this->allfields[$key]['required']($this->allfields);

                if(is_bool($field['required'])){ // bool

                    $is_required = $field['required'];

                }elseif(is_callable($field['required'])){ // a function

                    $is_required = $field['required']($this->_fields);

                }

            }

            if($is_required && !$field['value']){ // field is required but we dont have a value
                #var_dump($key);
                #var_dump($field);

                $this->addError($key, WebApp::l("Lauks [field] ir obligāts", ['replace' => ["[field]" => '"'.$field['label'].'"']]));
            }

            if(isset($field['validate']) && is_callable($field['validate'])){
                $field['validate']($field['value'], $this->_fields); // lets validate
            }

        }

        return !$this->hasErrors();

    }

    private function addError($key, $error)
    {

        $this->_fields[$key]['error'] = true;
        if(!isset($this->_fields[$key]['errors'])) $this->_fields[$key]['errors'] = [];
        $this->_fields[$key]['errors'][] = $error;

    }

    private function hasErrors()
    {

        return count($this->getErrors()) ? true : false;

    }

    public function getErrors($key = false)
    {

        $fields = $key ? [$key => $this->_fields[$key]]: $this->_fields; // one or all fields

        $errors = [];
        foreach($fields as $key => $field_data){

            if(isset($field_data['error']) && $field_data['error']){
                $errors[$key] = isset($field_data['errors']) ? $field_data['errors'] : [];
            }

        }

        return $errors;

    }

    public function getOmnivaData($lang = false)
    {

        $lang = $lang ? $lang : WebApp::$app->getLanguage();

        $omniva_data = [];
        foreach($this->omniva->getDBData() as $entry){

            if(!isset($omniva_data[$entry['city_'.$lang]])){
                $omniva_data[$entry['city_'.$lang]] = [];
            }

            $omniva_data[$entry['city_'.$lang]][$entry['item_id']] = $entry['address_'.$lang];

        }

        return $omniva_data;

    }

    public function getPastastacijaData($lang = false)
    {

        $lang = $lang ? $lang : WebApp::$app->getLanguage();

        $data = [];
        foreach($this->pastastacija->getDBData() as $entry){

            if(!isset($data[$entry['city_'.$lang]])){
                $omniva_data[$entry['city_'.$lang]] = [];
            }

            $data[$entry['city_'.$lang]][$entry['item_id']] = $entry['address_'.$lang];

        }

        return $data;

    }

    public function getDPDData($lang = false)
    {

        $lang = $lang ? $lang : WebApp::$app->getLanguage();

        $data = [];
        foreach($this->dpd->getDBData() as $entry){

            if(!isset($data[$entry['city_'.$lang]])){
                $omniva_data[$entry['city_'.$lang]] = [];
            }

            $data[$entry['city_'.$lang]][$entry['item_id']] = $entry['address_'.$lang];

        }

        return $data;

    }

    private function init()
    {

        $this->packs_price = option('Shop\\packs_price', null, 'Omniva piegāde (EUR)', null, 10);
        #this->omniva_price = option('Shop\\omniva_price', null, 'Omniva piegāde (EUR)', null, 10);
        #$this->circlek_price = option('Shop\\circlek_price', null, 'CircleK piegāde (EUR)', null, 10);
        #$this->pastastacija_price = option('Shop\\pastastacija_price', null, 'Pastastacija piegāde (EUR)', null, 10);
        #$this->dpd_price = option('Shop\\dpd_price', null, 'DPD piegāde (EUR)', null, 10);
        #$this->pasts_price = option('Shop\\pasts_price', null, 'Pasts piegāde (EUR)', null, 10);
        $this->courier_price = option('Shop\\courier_price', null, 'Kurjera piegāde (EUR)', null, 10);

        $this->packs_free_delivery_from = option('Shop\\packs_free_delivery_from', null, 'Pakomāta bezmaksas piegāde no (EUR)', null, 40);
        #$this->pack_free_delivery_from = option('Shop\\pack_free_delivery_from', null, 'Pakomāta bezmaksas piegāde no (EUR)', null, 40);
        #$this->omniva_free_delivery_from = option('Shop\\omniva_free_delivery_from', null, 'Omniva bezmaksas piegāde no (EUR)', null, 40);
        #$this->circlek_free_delivery_from = option('Shop\\circlek_free_delivery_from', null, 'CircleK bezmaksas piegāde no (EUR)', null, 40);
        #$this->pasts_free_delivery_from = option('Shop\\pasts_free_delivery_from', null, 'Pasts bezmaksas piegāde no (EUR)', null, 40);
        #$this->dpd_free_delivery_from = option('Shop\\dpd_free_delivery_from', null, 'DPD bezmaksas piegāde no (EUR)', null, 40);
        #$this->pastastacija_free_delivery_from = option('Shop\\pastastacija_free_delivery_from', null, 'Pastastacija bezmaksas piegāde no (EUR)', null, 40);
        $this->courier_free_delivery_from = option('Shop\\courier_free_delivery_from', null, 'Kurjera bezmaksas piegāde no (EUR)', null, 50);


        option('Shop\\OrderForm', '', 'Order form');
        $optcat = getOptionByPath('Shop\\OrderForm');
        $params = Array('parent_id' => $optcat['id']);

        $this->send_at_checkout = option('send_at_checkout', null, 'Send order at checkout', array_merge($params, Array('type' => 'check')), 'on');
        $this->order_prefix = option('order_prefix', null, 'Order prefix', $params, 'I-NET');
        $this->invoice_prefix = option('invoice_prefix', null, 'Invoice prefix', $params, 'I-INVOICE');
        $this->vat = option('Shop\\pvn', null, 'PVN (%)', null, 22) / 100;

        $this->setFields();

    }

    private function getDeliveryPrices()
    {

        return [
            'pakomats' => $this->packs_price,
            'courier' => $this->courier_price,
        ];

    }

    private function getDeliveryDiscounts()
    {

        return [
            'pakomats' => $this->packs_free_delivery_from,
            'courier' => $this->courier_free_delivery_from,
        ];

    }

    public function addProperties()
    {

        return [
            'shops' => [
                'label'       => 'Shops:',
                'type'        => 'collection',
                'collectiontype' => 'EVeikalsShopCollection',
            ],
            'omniva' => [
                'label'       => 'Omniva:',
                'type'        => 'collection',
                'collectiontype' => 'OmnivaAddressCollection',
            ],
            'pastastacija' => [
                'label'       => 'Pastastacija:',
                'type'        => 'collection',
                'collectiontype' => 'PastastacijaCollection',
            ],
            'dpd' => [
                'label'       => 'DPD:',
                'type'        => 'collection',
                'collectiontype' => 'DPDPickupCollection',
            ],
            'circlek' => [
                'label'       => 'CircleK:',
                'type'        => 'collection',
                'collectiontype' => 'CircleKCollection',
            ],
            'pasts' => [
                'label'       => 'Pasts:',
                'type'        => 'collection',
                'collectiontype' => 'PastsCollection',
            ],
            "orders" => [
                "label"       => "Order collection:",
                "type"        => "collection",
                "collectiontype" => "EVeikalsOrderCollection",
            ],
            "order_products" => [
                "label"       => "Order product collection:",
                "type"        => "collection",
                "collectiontype" => "shoporderprodcollection",
            ],
            "products" => [
                "label"       => "Product collection:",
                "type"        => "collection",
                "collectiontype" => "shopprodcollection",
            ],
            "statuses" => [
                "label"       => "Order status collection:",
                "type"        => "collection",
                "collectiontype" => "EVeikalsOrderStatuses",
            ],
            "sum_discounts" => [
                "label"       => "Sum discount collection:",
                "type"        => "collection",
                "collectiontype" => "LedAkcijasSumDiscountCollection",
            ]
        ];

    }


}