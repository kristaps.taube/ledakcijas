<?
//this class extends menuonelevelsimple.
//methods Output and Design are not redeclared, thus methods from menuonelevel will be used

include_once("class.menutwoleveltemplate.php");

class menuthreeleveltemplate extends menutwoleveltemplate
{

  //Class initialization
  function menuthreeleveltemplate($name)
  {
    $this->menutwoleveltemplate($name);
    $this->SetProperties();
  }


  function DisplayMenuItem3($name, $title, $path, $active, $row)
  {
    $this->outvars['name'] = $name;
    $this->outvars['title'] = $title;
    $this->outvars['path'] = $path;
    $this->outvars['active'] = $active;
    $this->outvars['row'] = $row;

    return $this->ReturnTemplate("l3template");
  }

    function DisplayMenuItem2($name, $title, $path, $active, $row,$menu3,$openbranch2)
  {
    $this->outvars['name'] = $name;
    $this->outvars['title'] = $title;
    $this->outvars['path'] = $path;
    $this->outvars['active'] = $active;
    $this->outvars['row'] = $row;
    $this->outvars['submenu2'] = $menu3;
    $this->outvars['openbranch2'] = $openbranch2;

    return $this->ReturnTemplate("l2template");
  }


  //override OutputSubmenu, which is empty function in menuonelevel class
  function OutputSubmenu($parent, $lev)
  {

      $data = sqlQueryData("SELECT * FROM " . $this->site_id . "_pages WHERE parent=$parent AND enabled=1 AND visible=1 AND in_trash=0 ORDER BY ind");
      if (count($data[0])==0) return;
          $first = 1;
          $menu3 = '';
          $s = '';
          foreach($data as $row)
          {
              $data2 = array();
              $data2 = sqlQueryData("SELECT * FROM " . $this->site_id . "_pages WHERE parent=" . $row['page_id']." AND enabled=1 AND visible=1 AND in_trash=0 ORDER BY ind");
              $this->outvars['submenu2'] = '';
              $menu3 = '';
              if (count($data2)>0)
              {
                $first2 = 1;
                $i = 0;
                $parent_page = -1;
                foreach ($data2 as $row2)
                {
                  if ($first2)
                  {
                    $pageparent2 = sqlQueryValue("SELECT parent FROM  " . $this->site_id . "_pages WHERE page_id=" . $row2['page_id']);
                    if (!$pageparent2) $parentpath2 = "/";
                    else $parentpath2 = PagePathByID($pageparent2, $this->site_id);
                    $first2 = 0;
                  }

                  $path2 = "";
                  $name2 = $row2['name'];
                  $title2 = $row2['title'];
                  if($name2 == 'index')
                      $path2 = $parentpath2;
                  else
                      $path2 = $parentpath2 . $name2;
                  if ($des2)
                  {
                      $path2 = 'edit.php?module=pages&site_id='.$this->site_id.'&action=inline_frame_page&page_id='.$row2["page_id"];
                  }
                  $active2 = ($row2['page_id']==$this->page_id);
                  $s2 = $this->DisplayMenuItem3($name2, $title2, $path2, $active2, $row2);

                  $menu3 .= '<ul>'.$s2.'</ul>';
                  if($row2['page_id'] == $this->page_id)$parent_page = $i;
                  $i++;

                }

              }


              if ($first)
              {
                $pageparent = sqlQueryValue("SELECT parent FROM  " . $this->site_id . "_pages WHERE page_id=" . $row['page_id']);
                if (!$pageparent) $parentpath = "/";
                else $parentpath = PagePathByID($pageparent, $this->site_id);
                $first = 0;
              }

              $path = "";
              $name = $row['name'];
              $title = $row['title'];
              if($name == 'index')
                  $path = $parentpath;
              else
                  $path = $parentpath . $name;
              if ($des)
              {
                  $path = 'edit.php?module=pages&site_id='.$this->site_id.'&action=inline_frame_page&page_id='.$row["page_id"];
              }
              $active = ($row['page_id']==$this->page_id);//print_r($data2);
              if((count($data2)>0) && (($this->page_id == $row['page_id']) || ($data2[$parent_page]['parent'] == $row['page_id'])))
                $openbranch2 = true;
              else
                $openbranch2 = false;

              $s .= $this->DisplayMenuItem2($name, $title, $path, $active, $row,$menu3,$openbranch2);
              $data2 = array();
              $data2[$parent_page]['parent']='';
              $parent_page = -1;

              $this->outvars['submenuinside'] = $s;
            }
            $this->outvars['submenu'] = $this->ReturnTemplate('submenutemplate');
            return $this->outvars['submenu'];
  }

  function CanBeCompiled()
  {
    if($GLOBALS['developmode'])
      return False;
    else
      return True;
  }



  function CanLoadDesignDynamically()
  {
    return true;
  }

  function SetProperties()
  {
    $pages = sqlQueryData("SELECT page_id,name,parent,title FROM " . $this->site_id . "_pages ORDER BY ind");
    ListNodes($pages, $combopages, 0, 0);
    $combopages[count($combopages)] = "-1:(Active page parent)";
    $combopages[count($combopages)] = "-2:(Active page)";
    $combopages[count($combopages)] = "-3:(Specify Level)";

    $this->properties = Array(

      "name"        => Array(
        "type"      => "string",
        "label"     => "Name:"
      ),

      "parent"      => Array(
        "type"      => "list",
        "label"     => "Root:",
        "lookup"    => $combopages
      ),

      "level"     => Array(
          "label"     => "Menu level:",
          "type"      => "string"
        ),

      "menutemplate"    => Array(
        "label"   => "Menu Template:",
        "type"    => "template",
        "rows"      => "8",
        "cols"      => "30",
        "tags"    => "{!menuinside}",
        "samples" => Array(
                       "UL List" => "<ul>{!menuinside}</ul>"),
        "value"   => "<ul>{!menuinside}</ul>"
      ),

      "submenutemplate"    => Array(
        "label"   => "Submenu Template:",
        "type"    => "template",
        "rows"      => "8",
        "cols"      => "30",
        "tags"    => "{!submenuinside}",
        "samples" => Array(
                       "UL List" => "<ul>{!submenuinside}</ul>"),
        "value"   => "<ul>{!submenuinside}</ul>"
      ),

      "l1template"    => Array(
        "label"   => "L1 item template:",
        "type"    => "template",
        "rows"      => "8",
        "cols"      => "30",
        "tags"    => "{!path};{!name};{!title};{active};{!openbranch};{!first};{!row};{!submenu}",
        "samples" => Array(
                       "UL List" => "<li><a href=\"/{!path}\"[if !openbranch] class=\"active\"[/if]>{!title}</a>[if !openbranch]{!submenu}[/if]</li>"),
        "value"   => "<li><a href=\"/{!path}\"[if !openbranch] class=\"active\"[/if]>{!title}</a>[if !openbranch]{!submenu}[/if]</li>"
      ),

      "l2template"    => Array(
        "label"   => "L2 item template:",
        "type"    => "template",
        "rows"      => "8",
        "cols"      => "30",
        "tags"    => "{!path};{!name};{!title};{active};{!row};{!openbranch2};{!submenu2}",
        "samples" => Array(
                       "UL List" => "<li><a href=\"/{!path}\"[if !openbranch2] class=\"active\"[/if]>{!title}</a>[if !openbranch2]{!submenu2}[/if]</li>"),
        "value"   => "<li><a href=\"/{!path}\"[if !openbranch2] class=\"active\"[/if]>{!title}</a>[if !openbranch2]{!submenu2}[/if]</li>"
      ),

      "l3template"    => Array(
        "label"   => "L3 item template:",
        "type"    => "template",
        "rows"      => "8",
        "cols"      => "30",
        "tags"    => "{!path};{!name};{!title};{active};{!row}",
        "samples" => Array(
                       "UL List" => "<li><a href=\"/{!path}\"[if !active] class=\"active\"[/if]>{!title}</a></li>"),
        "value"   => "<li><a href=\"/{!path}\"[if !active] class=\"active\"[/if]>{!title}</a></li>"
      ),

    );

  }

}

?>
