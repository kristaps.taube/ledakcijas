<?
//Serveris.LV Constructor component
//Created by Aivars Irmejs, 2004, aivars@serveris.lv

include_once("class.component.php");

class menuoneleveltemplate extends component
{

  var $parent;
  var $normalclass;
  var $normalstyle;
  var $normalbullet;
  var $selectedclass;
  var $selectedstyle;
  var $selectedbullet;

  //Class initialization
  function menuoneleveltemplate($name)
  {
    parent::__construct($name);
    $this->SetProperties();
    $this->getLanguageStrings();
  }


  function getLanguageStrings()
  {
    $ls = Array(
    );
    $this->parseTemplatePropertiesForLanguageStrings($ls);
    return $ls;
  }

  function OutputSubmenu($parent, $lev, $name, $title, $path, $active, $row = Array(), $openbranch = false, $first=false,$last=false)
  {
  }

  function OutputMenuDelimiter()
  {
    return "";
  }

  function OutputMenuBegin()
  {
    return "";
  }

  function OutputMenuEnd()
  {
    return "";
  }


  function OutputSubMenuBegin($empty)
  {
    return "";
  }

  function OutputSubMenuEnd($empty)
  {
    return "";
  }

	function getVisibleByParent($parent_id){

  	if(!$GLOBALS['pages_cache']){
      LoadPageCache();
		}

		$pages = array();
  	foreach($GLOBALS['pages_cache'] as $page){

    	if($page['enabled'] && $page['visible'] && !$page['in_trash'] && $page['parent'] == $parent_id){
      	$pages[] = $page;
    	}

  	}

    return $pages;

	}

	function getParent($id){

  	$page = $this->getByPageId($id);

		return $page ? $page['parent'] : false;

  }

  function getByPageId($id){

		if(!$GLOBALS['pages_cache']){
    	LoadPageCache();
		}

		foreach($GLOBALS['pages_cache'] as $page){
    	if($page['page_id'] == $id) return $page;
    }

    return false;

  }

  function Output($des="")
  {
      $this->parent = $this->getProperty("parent");
      $show = true;

      $parent = intval($this->parent);
      if(!$parent) $parent = 0;

      $outedData = false;

      //Obtain parent of currently active page
      $selparent = intval($this->getParent($this->page_id));
      if($selparent) $selparentparent = intval($this->getParent($selparent));
      if($selparentparent) $selparentparentparent = intval($this->getParent($selparentparent));
      if($selparentparentparent) $selparentparentparentparent = intval($this->getParent($selparentparentparent));

      $this->selparent = $selparent;
      $this->selparentparent = $selparentparent;
      $this->selparentparentparent = $selparentparentparent;
      $this->selparentparentparentparent = $selparentparentparentparent;

      if($parent == -1)
        $parent = $selparent;
      if($parent == -2)
        $parent = $this->page_id;
      if($parent == -3)
      {
        $level = $this->getProperty("level");
        $i = 0;
        $newp = $this->page_id;
        $parent = 0;
        $allparent = array();
        while($newp != 0)
        {
          $i++;
          $parent = $newp;
          $newp = sqlQueryValue("SELECT parent FROM  " . $this->site_id . "_pages WHERE page_id=" . $parent);
          $allparent[] = $newp;
        }
        //echo $i;
        if ($i == $level)
            $parent = sqlQueryValue("SELECT parent FROM  " . $this->site_id . "_pages WHERE page_id=" . $this->page_id);
        else if ( ($level-$i)==1 )
            $parent = $this->page_id;
        else if ($level<$i)
          {
            for ($j=1; $j<=$level; $j++)
              {
                $parent = array_pop($allparent);
              }
          }
        else $show = false;
      }

      if ($show)
      {

          echo $this->OutputMenuBegin();

          //Obtain list of all visible pages with parent==$parent
          $data = sqlQueryData("SELECT * FROM " . $this->site_id . "_pages WHERE parent=$parent AND enabled=1 AND visible=1 AND in_trash=0 ORDER BY ind");
          if (!$data) return;
          
          echo $this->OutputMenuBegin();


          $first = 1;
          $last = 0;

          $this->outvars['menuinside'] = '';
          $this->outvars['submenu'] = '';
          $i = 1;
          foreach($data as $row)
          {
            $this->outvars['n'] = $i;
              if ($first)
              {
                $pageparent = sqlQueryValue("SELECT parent FROM  " . $this->site_id . "_pages WHERE page_id=" . $row['page_id']);
                if (!$pageparent) $parentpath = "/";
                else $parentpath = PagePathByID($pageparent, $this->site_id);
              }

              if (count($data)==$i)
              {
                $last = 1;
              }
              $i++;

              $path = "";
              $name = $row['name'];
              $title = $row['title'];
              
              if ($parentpath == '') $parentpath = 'index/';

              if($name == 'index')
                $path = $parentpath;
              else
                  $path = $parentpath . $name;
              if ($this->design)
              {
                  $path = 'edit.php?module=pages&site_id='.$this->site_id.'&action=inline_frame_page&page_id='.$row["page_id"];
              }
              $active = ($row['page_id']==$this->page_id);
              if(($row['page_id']==$this->page_id)or($row['page_id']==$selparent)or($row['page_id']==$selparentparent)or($row['page_id']==$selparentparentparent)or($row['page_id']==$selparentparentparentparent))
                $openbranch = true;
              else
                $openbranch = false;

              if($openbranch)
              {
                //$this->outvars['submenu'] = $this->OutputSubmenu($row['page_id'],2);
              }
              $this->outvars['submenu'] = $this->OutputSubmenu($row['page_id'],2,$name, $title, $path, $active, $row, $openbranch, $first, $last);

              $this->outvars['menuinside'] .= $this->DisplayMenuItem($name, $title, $path, $active, $row, $openbranch, $first, $last);

              $first = 0;

              $outedData = true;

            }
            $this->ProcessTemplate('menutemplate');
      }
      return $outedData;
  }

  function DisplayMenuItem($name, $title, $path, $active, $row = Array(), $openbranch = false, $first=false,$last=false)
  {
    $this->outvars['name'] = $name;
    $this->outvars['title'] = $title;
    $this->outvars['path'] = $path;
    $this->outvars['active'] = $active;
    $this->outvars['row'] = $row;
    $this->outvars['openbranch'] = $openbranch;
    $this->outvars['last'] = $last;
    $this->outvars['first'] = $first;


    return $this->ReturnTemplate("l1template");
  }

  function Design()
  {
    $this->design = true;
    if(!$this->Output(1))
    {
      echo "[EMPTY]";
    }
  }

  function CanBeCompiled()
  {
    if($GLOBALS['developmode'])
      return False;
    else
      return True;
  }


  function CanLoadDesignDynamically()
  {
    return true;
  }

  function toolbar(&$script, &$toolbars, &$btnids, &$onclick)
  {
    return true;
  }

  function InitFormData()
  {
    $pages = sqlQueryData("SELECT page_id,name,parent,title FROM " . $this->site_id . "_pages ORDER BY ind");
    ListNodes($pages, $combopages, 0, 0);
    $combopages[count($combopages)] = "-1:(Active page parent)";
    $combopages[count($combopages)] = "-2:(Active page)";
    $combopages[count($combopages)] = "-3:(Specify Level)";
    $this->properties['parent']['lookup'] = $combopages;
  }

  function SetProperties()
  {

    $this->properties = Array(

      "name"        => Array(
        "type"      => "string",
        "label"     => "Name:"
      ),

      "parent"      => Array(
        "type"      => "list",
        "label"     => "Root:",
        "lookup"    => $combopages
      ),

      "level"     => Array(
          "label"     => "Menu level:",
          "type"      => "string"
        ),

      "menutemplate"    => Array(
        "label"   => "Menu Template:",
        "type"    => "template",
        "rows"      => "8",
        "cols"      => "30",
        "tags"    => "{!menuinside}",
        "samples" => Array(
                       "UL List" => "<ul>{!menuinside}</ul>"),
        "value"   => "<ul>{!menuinside}</ul>"
      ),

      "l1template"    => Array(
        "label"   => "L1 item template:",
        "type"    => "template",
        "rows"      => "8",
        "cols"      => "30",
        "tags"    => "{!path};{!name};{!title};{active};{!openbranch};{!first};{!row};{!submenu};{!last};{!n}",
        "samples" => Array(
                       "UL List" => "<li><a href=\"/{!path}\"[if !active] class=\"active\"[/if]>{!title}</a>{!submenu}</li>"),
        "value"   => "<li><a href=\"/{!path}\"[if !active] class=\"active\"[/if]>{!title}</a>{!submenu}</li>"
      ),

    );
  }

}

?>
