<?php

class shopsmallcart extends component
{

    function __construct($name)
    {
        parent::__construct($name);
        $this->carts = EVeikalsSavedCarts::getInstance();
    }

    function addGiftCard($data)
    {

        $data['key'] = "gc".rand(1,10000);
        while(isset($_SESSION['giftcards'][$data['key']])){ // make me unique
            $data['key'] = "gc".rand(1,10000);
        }

        $_SESSION['giftcards'][$data['key']] = $data;

    }

    function addCartItem($additem, $count)
    {

        $found = false;
        foreach($_SESSION['cartitems'] as $key => $cartitem){
            unset($cartitem['count']);
            if($cartitem == $additem){

                $found = true;
                $_SESSION['cartitems'][$key]['count'] += $count;
                $additem['count'] = $_SESSION['cartitems'][$key]['count'];
                break;

            }
        }

        if(!$found){
            $additem['count'] = $count;
            $_SESSION['cartitems'][] = $additem;
        }

        $this->saveCart();

        $_SESSION['shop_last_cart_item'] = $additem;

    }

    function deleteSavedCart($user)
    {
        $this->carts->deleteSavedCart($user);
    }

    function loadCart($cart)
    {

        if($cart && empty($_SESSION['cartitems'])){
            foreach($cart as $item){
                $_SESSION['cartitems'][] = $item;
            }
        }

    }

    function saveCart()
    {

        if($_SESSION['logged_in']){
            $_SESSION['logged_in']['saved_cart_id'] = $this->carts->updateData($_SESSION['logged_in']['item_id'], $_SESSION['cartitems'], $_SESSION['logged_in']['saved_cart_id']);
            return $_SESSION['logged_in']['saved_cart_id'];
        }
        return null;

    }

    function Output()
    {

        $CurrencyMenu = EVeikalsCurrencyMenu::getInstance();
        $CurrencyMenu->init();
        $shopman = shopmanager::getInstance();

        $goods = Array();
        $sum = 0;
        $shopman->getSumAndGoodsList($goods, $sum, $hasgoods);

        $count = 0;
        foreach($goods as $good){
            $count += $good['count'];
        }

        $currency_rate = $_SESSION['currency']['rate'] ? $_SESSION['currency']['rate'] : 1;

        ?>
        <div class="basket_wrap">
            <a href="<?php echo Constructor\SystemUrl::get('cart') ?>" class="basket">
                <span class="count"><?=$count?></span>
                <span <?=($count >= 100)? "style='display: none'" : ""?> class='text'><?=$this->l("preces")?></span>
            </a>
        </div>
        <span class='sum'>
            <span class='primary'><span><?=number_format($sum / $_SESSION['currency']['rate'], 2)?></span> <?=$_SESSION['currency']['label']?></span>
            <? if($CurrencyMenu->secondary && $CurrencyMenu->secondary['title'] != $_SESSION['currency']['title']){ ?>
            <span <?=($sum / $CurrencyMenu->secondary['rate'] >= 100000)? "style='display: none'" : ""?> class='secondary'>/ <span><?=number_format($sum / $CurrencyMenu->secondary['rate'], 2)?></span> <?=$CurrencyMenu->secondary['label']?></span>
            <? } ?>
        </span>
        <script>
            var rate = <?=json_encode((float)$_SESSION['currency']['rate'])?>;
            var show_secondary_currency = <?=json_encode(($CurrencyMenu->secondary && $CurrencyMenu->secondary['title'] != $_SESSION['currency']['title'])? true : false)?>;
            <? if($CurrencyMenu->secondary){ ?>
                var secondary_rate = <?=json_encode((float)$CurrencyMenu->secondary['rate'])?>;
            <? } ?>
        </script>

        <?

    }

}


