<?php
/**
*
*  Title: Gift cards
*  Author: Kristaps Taube <ktaube@datateks.lv>
*  Date: 07.03.2014
*  Project: EVeikals
*
*/

include_once("class.component.php");
require_once($GLOBALS['cfgDirRoot']."library/mPDF/mpdf.php");

class EVeikalsGiftCard extends component{

  private $giftcard_path = "/files/giftcards/";

  function __construct($name){
    parent::__construct($name);
    $this->SetProperties();
    $this->registerAsService("GiftCards");
    $this->registerAsBackendService("GiftCards");
  }

  function getServices()
  {
    $this->CurrencyMenu = EVeikalsCurrencyMenu::getInstance();
    $this->Manager = shopmanager::getInstance();
    if($this->Manager) $this->Manager->getServices();
  }

  function execute(){

    if(!$this->initCollections()) return;
     $this->init();

    if($_POST['action'] == 'ValidateGiftCard'){
      $_SESSION['giftcard_code'] = $_POST['code'];
      echo json_encode($this->ValidateGiftCard($_POST['code']));
      die();
    }

    if($_GET['action'] == 'view' && !$this->visible){
      $this->title = $this->getServiceByName("title");
      $this->title->srvSetTitle($this->l("Dāvanu kartes apskate"));
    }

    // only for me
    if($_SERVER["REMOTE_ADDR"] == '213.21.219.21' || $_SERVER['REMOTE_ADDR'] == '127.0.0.1'){

      if($_GET['action'] == 'ProcessPaid' && $_GET['id']){

        $res = $this->ProcessPaid($_GET['id']);

        die("Done");
      }

      if($_GET['action'] == 'ProcessPaidOrder' && $_GET['id']){

        $cards = $this->cards->getByOrderId($_GET['id']);
        foreach($cards as $card){
          $this->ProcessPaid($card);
        }

        $this->sendCardsToEmail((int)$_GET['id']);

        die("done");
      }

      if($_GET['action'] == 'GenerateGiftCardPDF' && $_GET['id']){

        $output = $_GET['output']? true : false;
        $path = $this->GeneratePDF($_GET['id'], $output);
        if($path){
          $this->cards->changeItemByIDAssoc($_GET['id'], array("pdf" => $path['web']));
        }

        die("PDF generated!");

      }

    }

    if(isset($_POST['OrderGiftCard'])){
      $this->ProcessForm();
    }

  }


  function output(){

    if(!$this->initCollections()){
      echo $this->name.": Can't init collections.<br />";
      return;
    }

    if($_GET['action'] == 'view'){
      $this->displayCard($_GET['hash']);
      return;
    }

    $this->outputForm();

  }

  function displayCard($hash){

    $card = $this->cards->getByViewHash($hash);

    if($card){

      $valid = explode(" ", $card['valid_until']);
      $valid = explode("-", $valid[0]);
      $valid = $valid[2].".".$valid[1].".".$valid[0];

      ?>
      <div id='GiftCardView' class='padding'>
        <div class='number'><?=$this->l("ID numurs")?> <b><?=$card['id']?></b></div>
        <div class='sum'><?=$this->l("Atlikums")?>: <?=$_SESSION['currency']['label']?> <?=number_format(($card['sum'] - $card['spent']) / $_SESSION['currency']['rate'], 2)?></div>
        <div class='valid_until'><?=$this->l("Derīga līdz")?>: <?=$valid?></div>
      </div>
      <style type="text/css">
        .number{
          font: 22px Verdana;
          color: #316385;
        }

        .sum{
          font: bold 24px Verdana;
          color: #f1930b;
        }

        .valid_until{
          font: 22px Verdana;
          color: #316385;
        }
      </style>
      <?

    }else{
      echo "<div class='error'>".$this->l("Karte netika atrasta")."</div>";
    }

  }

  function initOptions(){

    $this->min_gift_card_sum = option('GiftCard\\min_gift_card_sum', null, 'Min. gift card sum', array("is_advanced" => true), 50);
    $this->giftcard_expiration = option('GiftCard\\expiration', null, 'Derīguma termiņš(mēnešos)', array("is_advanced" => true), 6);
    $this->giftcard_logo = option('GiftCard\\logo', null, 'Logo link', Array("is_advanced" => true, 'type' => 'dialog', 'componenttype' => get_class($this) ) );
    $this->giftcard_products = option('GiftCard\\products', null, 'Product image link', Array("is_advanced" => true, 'type' => 'dialog', 'componenttype' => get_class($this) ) );

    $this->admin_email = readOption("admin_emailaddress", null, $this->site_id);

  }

  function init(){

    $this->initOptions();
    $this->getServices();
    $this->initFields();

  }

  // TODO add valida date
  function ValidateGiftCard($code){

    $code = $this->cards->getByCode($code);

    $response = array("status" => 0, "msg" => "");

    if(!$code){
      $response['msg'] = $this->l("Dāvanu karte nav atrasta");
      return $response;
    }

    if(!$code['paid']){
      $response['msg'] = $this->l("Dāvanu karte nav apmaksāta");
      return $response;
    }

    if($code['sum'] == $code['spent']){
      $response['msg'] = $this->l("Dāvanu karte ir iztērēta");
      return $response;
    }

		if(strtotime($code['valid_until']) < time()){
     	$response['msg'] = $this->l("Dāvanu kartei ir beidzies derīguma termiņš");
      return $response;
		}

    $available = (float)round(($code['sum'] - $code['spent']) / $_SESSION['currency']['rate'], 2) ;

    // $response['available'] is in default currency
    // while $response['msg'] is already converted to active currency !!!

    $response['id'] = $code['item_id'];
    $response['status'] = 1;
    $response['sum'] = $code['sum'];
    $response['spent'] = $code['spent'];
    $response['available'] = $code['sum'] - $code['spent'];
    $response['msg'] = replace_all($this->l("Dāvanu kartes atlikums [sum] [currency]"), array(
      "[sum]" => "<span class='highlight'>".$available."</span>",
      "[currency]" => $_SESSION['currency']['label']
    ));

    return $response;

  }

  static function processOptionBeforeDisplay($path, $opt, &$data){

    if($path == 'GiftCard\\logo' || $path == 'GiftCard\\products'){
      $data['dialog'] = '?module=dialogs&action=filex&site_id='.$GLOBALS['site_id'].'&view=thumbs';
    }

  }

  function GeneratePDF($card, $output = false){

    $card = is_numeric($card)? $this->cards->getItemByIDAssoc($card) : $card;

    if(!$card){
      echo "Gift card not found";
      return;
    }

    $valid = explode(" ", $card['valid_until']);
    $valid = explode("-", $valid[0]);
    $valid = $valid[2].".".$valid[1].".".$valid[0];

    ob_start()
    ?>
      <style type="text/css">
        .number{
          font: 22px Verdana;
          color: #316385;
        }
        .sum{
          font: bold 24px Verdana;
          color: #f1930b;
          margin-bottom: 20px;
        }
        .valid_until{
          font: 22px Verdana;
          color: #316385;
        }
        .wrap{
          padding: 50px 75px 0px;
          position: relative;
        }

        .column2{ font: 14px verdana; }
        .title{
          color: #f1930b;
          margin-bottom: 20px;
          font-weight: bold;
        }

        table{ width: 100%; }
        table td{ vertical-align: top; }

        .fc{
          border-right: 1px solid #CCCCCC;
          width: 70%;
          padding-right: 10px;
        }

        .ft{
          padding: 15px 10px;
        }

        .footer{
          border-top: 3px solid #CCCCCC;
          padding-top: 10px;
          margin-top: 10px;
        }

        .logo{
          width: 200px;
          margin: 20px 75px 0px;
        }

        .logosep{
          width: 100%;
          border-bottom: 3px solid #7cbd42;
        }
        
        .productback_wrap{
          position: absolute;
          right: 0px;
          top: 0px;
        }

        .products_wrap{
          position: absolute;
          right: 40px;
          top: 20px;
        }

        .t{
          font-weight: bold;
          font-style: italic;
          color: #7cbd42;
          margin: 0px 80px -3px;
          font: italic bold 23px Verdana
        }

      </style>
      <img src='http://<?=$_SERVER['SERVER_NAME']?><?=$this->giftcard_logo?>' alt='' class='logo' />
      <div class='t'><?=$this->l("Dāvanu karte")?></div>
      <div class='productback_wrap'>
        <img src='http://<?=$_SERVER['SERVER_NAME']."/images/html/giftcardproductback.png"?>' alt='' class='productback' />
      </div>
      <div class='products_wrap'>
        <img src='http://<?=$_SERVER['SERVER_NAME']?><?=$this->giftcard_products?>' alt='' class='logo' />
      </div>
      <div class='logosep'></div>
      <div class='wrap'>
        <table>
          <tr>
            <td class='fc'>
              <div class='column1'>
                <div class='number'><?=$this->l("ID numurs")?> <b><?=$card['id']?></b></div>
                <div class='sum'><?=$this->CurrencyMenu->default['label']?> <?=number_format($card['sum'], 2)?></div> <br />
                <div class='valid_until'><?=$this->l("Derīga līdz")?>: <?=$valid?></div>
              </div>
            </td>
            <td class='ft'>
              <div class='column2'>
                <div class='title'><?=$this->l("Kontakti")?></div>
                <?=$this->l('SIA "E-veikals"<br />Cēsu iela 31, II korpuss<br />Rīga, Latvija LV-1012<br />E-pasts: info@datateks.lv<br />Tālrunis: +371 67299532<br />Fakss: +371 67299537<br />')?>
              </div>
            </td>
          </tr>
        </table>
        <div class='footer'>
          <?=$this->l('Šīs dāvanu kartes īpašnieks to var izmantot produktu iegādei tiešsaistē e-veikals.lv mājas lapā, kā arī citu mūsu pakalpojumu iegādei klātienē mūsu birojā  Cēsu ielā 31, II korpuss, Rīgā.')?>
        </div>
       </div>

    <?
    $html = ob_get_contents();
    ob_end_clean();

    $dir = sqlQueryValue("select dirroot from sites where site_id=".$this->site_id);
    $filename = sha1("card".$card['item_id'].rand(1,10000)).".pdf"; // so noone can read
    $web_path = "files".DIRECTORY_SEPARATOR ."giftcards".DIRECTORY_SEPARATOR . $filename;
    $path = $dir.$web_path;

    $mpdf = new mPDF();
    $mpdf->AddPage('P', '', '', '', '', 0,0,0,0);
    $mpdf->WriteHTML($html);
    $mpdf->Output($path); // save pdf

    if($output){
      $mpdf->Output(); // output pdf
    }

    return array("web" => $web_path, "full" => $path);

  }

  function ValidateForm(){

    $ok = true;

    foreach($this->fields as &$field){

      if($field['required'] && !$field['value']){

        $field['error'] = true;
        $this->errors[] = $this->l("Aizpildiet lauku").": ".$field['label'];

        $ok = false;
      }

      if($field['name'] == 'sum' && $field['value'] < $this->min_gift_card_sum){
        $this->errors[] = replace_all($this->l("Minimālā dāvanas kartes summa ir [sum] [currency]"), array("[sum]" => $this->min_gift_card_sum, "[currency]" => $this->CurrencyMenu->default['label']));
        $field['error'] = true;
        $ok = false;
      }

    }

    if(!$_SESSION['captcha'] || $_SESSION['captcha'] != $_POST['captcha']){
      $this->errors[] = $this->l("Nepareizs drošības kods");
      $ok = false;
    }

    $this->hasErrors = !$ok;

    return $ok;

  }

  function sendCardsToEmail($order){

    $order_id = is_array($order)? $order['item_id'] : $order;

    $cards = $this->cards->getByOrderId($order_id);

    if(!count($cards)) return true;

    $attach = array();
    $links = array();

    foreach($cards as $card){
      $attach[] = $card['full'];
      $url = "http://".$GLOBALS['cfgDomain']."?action=view&hash=".$card['view_hash'];
      $links = "<a href='".$url."'>".$url."</a>";
    }

    $link = implode(", ", $links);

    // send pdf to email
    $title = $this->l('EVeikals dāvanu karte');
    $text = $this->l('Labdien, <br /> Paldies par dāvanu karšu iegādi. Dāvanu kartes pievienotas vēstulei. Lai apmskatītu dāvanu kartes atlikumu, sekojiet norādītajai saitei [link].  <br /><br />Ar cieņu,<br /> EVeikals');

    $text = str_replace("[link]", $link, $text);

    $sent = phpMailerSend($text, $title, $this->l("EVeikals"), $this->admin_email, $card['email'], true, false, $attach);

    return $sent? true : false;

  }

  function ProcessPaid($card){

    $card = is_numeric($card)? $this->cards->getItemByIDAssoc($card) : $card;
    if(!$card) return false;

    $card['valid_until'] = date("Y-m-d", strtotime("+".$this->giftcard_expiration." month", time()));
    $pdf = $this->GeneratePDF($card);
    $card['paid'] = 1;
    $card['pdf'] = $pdf['web'];

    $card['view_hash'] = $this->generateCode(32);
    $this->cards->changeItemByIDAssoc($card['item_id'], $card);

  }

  function generateCode($length = 8, $possible = false){

    $code = "";
    $possible = $possible ? $possible : "012346789abcdfghjkmnpqrtvwxyzABCDFGHJKLMNPQRTVWXYZ";
    $maxlength = strlen($possible);

    if ($length > $maxlength) {
      $length = $maxlength;
    }

    $i = 0;

    while ($i < $length) {

      $char = substr($possible, mt_rand(0, $maxlength-1), 1);

      if (!strstr($code, $char)) {
        $code .= $char;
        $i++;
      }

    }

    return $code;

  }

  function create($sum, $order_id){

    $card = $this->cards->createNew();
    $card["paid"] = 0;
    $card["sum"] = $sum;
    $card["order_id"] = $order_id;
    $this->cards->changeItemByIDAssoc($card['item_id'], $card);
    return $card['item_id'];

  }

  function ProcessForm(){

    if($this->ValidateForm()){

      $card['sum'] = $this->fields['sum']['value'];
      $this->Manager->addGiftCard($card);
      header("location:?");
      die();

    }

  }

  function initFields(){

    $this->fields = Array(
      "sum" => Array(
        "name" => "sum",
        "label" => str_replace("[currency]", $this->CurrencyMenu->default['label'], $this->l("Summa ([currency])")),
        "type" => "text",
        "required" => true,
      ),

    );

    foreach($this->fields as &$field){
      $field['value'] = $_POST[$field['name']];
    }

  }

  function getFields(){

    return $this->fields;

  }

  function outputForm(){

    ?>

      <form method='post' action='' id='GiftCardBuyForm' class='padding'>
      <? if($this->hasErrors){ ?>
        <div class='errors'>
          <ul>
            <li><?=implode("</li><li>", $this->errors)?></li>
          </ul>
        </div>
      <? } ?>
      <table>
        <? foreach($this->getFields() as $field){ ?>
        <tr>
          <td class='<?=$field["error"]? "error" : "" ?>'>
            <?=$field['label']?><?=$field['required']? "*" : ""?>:
          </td>
          <td><input type='text' name='<?=$field['name']?>' value='<?=htmlspecialchars($field['value'])?>' /></td>
        </tr>
        <? } ?>
        <tr>
          <td>
            <?=$this->l("Drošības kods")?>:
          </td>
          <td>
            <input type="text" name="captcha" id="captcha-form"  /><br />
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>
            <a href="#" onclick="
              document.getElementById('captcha').src='http://<?=$_SERVER["HTTP_HOST"]?>/cms/library/captcha/captcha.php?'+Math.random();
              document.getElementById('captcha-form').focus(); return false;"
              id="change-image">
              <img src="http://<?=$_SERVER["HTTP_HOST"]?>/cms/library/captcha/captcha.php" id="captcha" />
            </a>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td><input type='submit' name='OrderGiftCard' value='<?=$this->l("Iegādāties")?>' /></td>
        </tr>
      </table>
    </form>
    <?

  }

  function SetProperties(){
    $this->properties = Array(

      "name"        => Array(
        "label"     => "Name:",
        "type"      => "str"
      ),

      "cards" => Array(
        "label"       => "Gift cards nosaukums:",
        "type"        => "collection",
        "collectiontype" => "EVeikalsGiftCards",
        "lookup"      => GetCollections($this->site_id, "EVeikalsGiftCards"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "orders" => Array(
        "label"       => "Order collection:",
        "type"        => "collection",
        "collectiontype" => "EVeikalsOrderCollection",
        "lookup"      => GetCollections($this->site_id, "EVeikalsOrderCollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

    );

  }

}

?>