<?php
/**
*
*  Title: OrderStatusChangeNotifications
*  Author: Kristaps Taube <ktaube@datateks.lv>
*  Date: 08.03.2018
*  Project: LEDAkcijas.lv
*  Description: Sends email ans SMS notifications
*/

use Constructor\WebApp;
use Constructor\Url;

class LEDAkcijasOrderStatusChangeNotifications extends component
{

    public function __construct($name = false)
    {

        parent::__construct($name);
        $this->init();

    }

    public function process($order_id, $status_from_id, $status_to_id)
    {

        $order = $this->orders->getById($order_id);
        $status_from = $this->statuses->getById($status_from_id);
        $status_to = $this->statuses->getById($status_to_id);

        $orderdata = unserialize($order['order_data']);

        $delivery_method = $orderdata['allfields']['delivery_method'];
        $payment_method = $orderdata['allfields']['payment_method'];

        if(is_numeric($delivery_method)){
            $delivery_method = 'shop';
        }

        $key = 'email_notification_'.$delivery_method.'_'.$payment_method.'_'.$order['valoda'];

        if(isset($status_to[$key]) && $status_to[$key]){

            $email_notification_subject = $status_to['email_notification_subject_'.$order['valoda']];

            $email_notification = $status_to[$key];
            $email_notification = $this->replaceAllTags($email_notification, $order, $status_to);

            if($status_to['new']){
                $path = $this->createPDF($order); // do we really have to create PDF twice?
                $attachs = [$path];
            }else{
                $attachs = [];
            }

            $hash = sha1(time().'|'.$order_id);
            $view_url = Url::get('/emails', ['h' => $hash], true);

            // wrap in template
            $mail_html = $this->render("/Mailing/notifications", [
                'content' => $email_notification,
                'lang' => $order['valoda'],
                'view_link' => $view_url
            ]);

            $this->notifications->Insert([
                'date' => date('Y-m-d H:i:s'),
                'email' => $order['epasts'],
                'hash' => $hash,
                'subject' => $email_notification_subject,
                'content' => $mail_html
            ]);

            $t = phpMailerSend($mail_html, $email_notification_subject, $this->sender_name, $this->sender_email, $order['epasts'], true, false, $attachs);

        }

        $sms_key = 'sms_notification_'.$delivery_method.'_'.$payment_method.'_'.$order['valoda'];

        if(isset($status_to[$sms_key]) && $status_to[$sms_key] && $order['telefons']){

            $sms_notification = $status_to[$sms_key];

            $sms_notification = $this->replaceAllTags($sms_notification, $order, $status_to);
            $sms_notification = strip_tags($sms_notification);
            $sms_notification = str_replace('&scaron;', 'š', $sms_notification);

            $response = $this->sales_traffic->sendOne([
                'Sender' => $this->sms_sender,
                'Number' => $order['telefons'],
                'Content' => $sms_notification
            ]);

        }

        #var_dump($this->sender_email);
        #var_dump($order['epasts']);
        #die();

    }

    private function replaceAllTags($message, $order, $status)
    {

        static $orderform;

        $orderdata = unserialize($order['order_data']);
        $fields = $orderdata['allfields'];
        $items = $orderdata['products'];

        $orderform = $orderform ? $orderform : OrderForm::getInstance();
        $orderform->loadFieldData($fields);

        $replace = [
            "[ORDER_NUM]" => $order['num'],
            "[STATUS]" => $status['status_'.$order['valoda']]
        ];

        $delivery_method = $orderform->getDeliveryMethod($orderform->getFields(), $items, $order['valoda']);

        if($fields['delivery_method'] == 'pakomats'){
            $replace['[PICKUP_POINT]'] = $delivery_method['sub']['value']."(".$data['type'].")";
        }elseif($fields['delivery_method'] == 'courier'){
            $replace['[DELIVERY_ADDRESS]'] = $delivery_method['sub']['value'];
        }else{ // pickup at one of shops
            $replace['[PICKUP_POINT]'] = $delivery_method['address'];
        }

        foreach($replace as $what => $for){
            $message = str_replace($what, $for, $message);
        }

        # [PICKUP_POINT]

        return $message;

    }

    private function init()
    {

        $this->sender_email = option('EmailNotifications\sender_email', null, 'Send from e-mail', [], 'constructor@datateks.lv');
        $this->sender_name = option('EmailNotifications\\sender_name', null, 'Send from name', [], 'Constructor');

        $this->sms_api_key = option('SMS\\api_key', null, 'Api key', null, 'd2d4fe98db58b6c221bc60a46a8483bdae180ea9');
        $this->sms_sender = option('SMS\\sender', null, 'Sender name', null, 'LEDAkcijas.lv');

        $this->sales_traffic = new SalesTraffic($this->sms_api_key);

        $this->order_form = OrderForm::getInstance();

        $this->order_path = WebApp::$app->getSite()['dirroot'].'files/rekini/';

    }

    private function createPDF($order)
    {

        require_once(WebApp::$app->getConfigValue('dir_root')."/library/mPDF/mpdf.php");
        $mpdf = new \mPDF();
        $mpdf->WriteHTML($order['rekins']);

        $path = $this->order_path.transliterateURL($order['num']).'.pdf';

        $mpdf->Output($path, "F");

        return $path;

    }

    public function addProperties()
    {

        return [
            "orders" => [
                "label"       => "Order collection:",
                "type"        => "collection",
                "collectiontype" => "EVeikalsOrderCollection",
            ],
            "order_products" => [
                "label"       => "Order product collection:",
                "type"        => "collection",
                "collectiontype" => "shoporderprodcollection",
            ],
            "statuses" => [
                "label"       => "Order status collection:",
                "type"        => "collection",
                "collectiontype" => "EVeikalsOrderStatuses",
            ],
            "notifications" => [
                "label"       => "Notification collection:",
                "type"        => "collection",
                "collectiontype" => "EMailNotificationCollection",
            ]
        ];

    }

}