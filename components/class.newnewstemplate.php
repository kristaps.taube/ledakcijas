<?

include_once('class.component.php');

class newnewstemplate extends component{

  function newnewstemplate($name){
    parent::__construct($name);
    $this->SetProperties();
    $this->parseTemplatePropertiesForLanguageStrings($ls);
    $this->page_url = '/'.PagePathById($this->page_id, $this->site_id);
    $this->registerAsService("news");
  }

  function getLanguageStrings()
  {
    $ls = Array(
      'readmore' => 'Lasīt vairāk...',
      'back'     => 'Atpakaļ',
      'author'   => 'Autors',
      'date'     => 'Datums',
      'nonews'   => 'Ziņa nav atrasta!',
      'really_del' => 'Vai tiešām vēlies dzēst šo ziņu?',
      'addnews' => 'Pievienot jaunu ziņu',
      'editnews' => 'Rediģēt ziņu',
      'news_archive' => 'Vecākas ziņas',
      'zinai'   => '%s ziņai <a href="%s">"%s"</a>',
      'rss_title' => 'My site'
    );

    if ($this->getProperty('commentscol'))
    {
      $ls = $ls + array(
        'editcomment' => 'Rediģēt komentāru',
        'nocomments'  => 'Nav komentāru',
        'comment'  => 'komentārs',
        'comments' => 'komentāri',
        'comment_field' => 'Komentārs',
        'comments_title' => 'Komentāri',
        'send_comment' => 'Pievienot',
        'input'       => 'Ievadi',
        'no_fields'   => 'Nav aizpildīti visi lauki!',
        'no_security' => 'Nepareizs drošības kods!',
        'nick'     => 'Vārds',
      );
    }

    return $ls;
  }

  function initVars()
  {
    $this->show_type = $this->getProperty('show_type');
    $this->singlenews_url = $this->getProperty('singlenews_url');
    $this->comments_url = $this->getProperty('commentslist_url');
    $this->newslist_url = $this->getProperty('newslist_url');
  }


  function execute(){
    if (!$this->visible()) return;
    if (!$this->initCollections()) return;

    $this->title_srv = $this->getServiceByName('title');

    switch ($this->getProperty('show_type')){
      case 2:
        $this->initSingleNews();
        break;
      default:
        $id = intval($_GET['nid']);
        if ($id > 0) $this->initSingleNews();
        break;
    }
  }

  function initSingleNews()
  {
    $row = $this->item = $this->newscol->GetItemByIdAssoc(intval($_GET['nid']));

    if ($this->getProperty('use_title_srv') && $this->title_srv)
    {
      $this->title_srv->srvSetTitle($row['title']);
    }
  }

  function output($isdesign = false)
  {
    if ($isdesign) $this->execute();
    
    if (!$this->newscol) return;

    if ($isdesign)
    {
      $this->setCollectionProperties();
    }

    $this->initVars();

    switch ($this->getProperty('show_type'))
    {
      case 1: return $this->outputNewsList($isdesign);
      case 2: return $this->outputSingleNews(intval($_GET['nid']), $isdesign);
      case 3: return $this->outputComments(intval($_GET['nid']));
      default:
        $id = intval($_GET['nid']);
        if ($id > 0) return $this->outputSingleNews($id, $isdesign);
        else return $this->outputNewsList($isdesign);
    }
  }

  function design()
  {
    $this->output(true);
  }

  function setCollectionProperties()
  {
    $props = Array(
      'image_width', 'image_height', 'image_resizemode',
      'thumb_width', 'thumb_height', 'thumb_resizemode',
      'bigthumb_width', 'bigthumb_height', 'bigthumb_resizemode',
      'show_full_text', 'show_image', 'show_news_date', 'show_start_date',
      'show_end_date', 'show_archived', 'show_author'
    );

    foreach ($props as $p)
    {
      $this->newscol->setProperty($p, $this->getProperty($p));
    }
  }

  function getNews($limit=null)
  {
    $pagelimit = ($limit !== null) ? $limit : intval($this->getProperty('pagelimit'));

    $where = Array();
    switch ($this->getProperty('list_type'))
    {
      case 0:
        $where[] = 'disabled=0';
        $where[] = 'archived=0';
        break;
      case 1: $where[] = 'archived!=0'; break;
      case 2: break;
    }
    $where_q = $where ? implode(' AND ', $where) : '';
    $orderby = ($this->getProperty('sort_by') == 1) ? 'news_date DESC' : 'ind';

    $a = $this->newscol->getNews($where_q, intval($_GET['pg']) * $pagelimit, $pagelimit, $orderby);

    $_SESSION['newslist_'.$this->name] = array(
      'ids' => explode(',', $this->newscol->getNewsIds($where_q, $orderby)),
    );

    return $a;
  }

  function getNewsListInfo()
  {
    if (empty($_SESSION['newslist_'.$this->name]))
    {
      $this->getNews();
    }
    return $_SESSION['newslist_'.$this->name];
  }

  function xmlEntities($string)
  {
     return str_replace ( array ( '&', '"', "'", '<', '>' ), array ( '&amp;' , '&quot;', '&apos;' , '&lt;' , '&gt;' ), $string );
  }

  function outputRSS()
  {
    $a = $this->getNews();
    $data = $a['data'];
    $count = $a['count'];


//    header('Content-type: text/xml; charset=utf-8');
?>
<? echo '<'.'?xml version="1.0" encoding="utf-8"?'.'>'."\n" ?>
  <rss version="2.0">
    <channel>
      <title><?=$this->LS('rss_title') ?></title>
      <link>http://<?=$_SERVER['SERVER_NAME'].$this->getURL('newslist') ?></link>
      <description></description>

    <? foreach ($data as $row) { ?>
      <item>
         <title><?=$this->xmlEntities(strip_tags($row['title'])) ?></title>
         <link>http://<?=$_SERVER['SERVER_NAME'].$this->getURL('news', $row['item_id']) ?></link>
         <description><?=$this->xmlEntities(strip_tags($row['short_text'])) ?></description>
      </item>
    <? } ?>
    </channel>
  </rss>
<?
    die;
  }

  function outputNewsList($isdesign = false)
  {
    if ($_GET['rss'] == 1)
    {
      $this->outputRSS();
      return;
    }

    $this->newscol->updateStartEndNews();

    if ($this->getProperty('archive_mode') == 1)
    {
      $this->newscol->updateAutomaticArchive($this->getProperty('archiving_age') * 86400);
    }

    $a = $this->getNews();
    $data = $a['data'];
    $count = $a['count'];

    $newslist = '';
    $date_format = $this->getProperty('date_format');
    $show_type = $this->getProperty('show_type');
    $singlenews_url = $this->getProperty('singlenews_url');
    $i = 0;
    $time = time();

    $thumb_w = $this->getProperty('thumb_width');
    $thumb_h = $this->getProperty('thumb_height');
    $thumb_m = $this->getProperty('thumb_resizemode');
    $bigthumb_w = $this->getProperty('bigthumb_width');
    $bigthumb_h = $this->getProperty('bigthumb_height');
    $bigthumb_m = $this->getProperty('vthumb_resizemode');

    foreach ($data as $row)
    {
      if (!empty($row['image']))
      {
        $row['thumb'] = getThumbURL($row['image'], $thumb_w, $thumb_h, $thumb_m);
        $row['bigthumb'] = getThumbURL($row['image'], $bigthumb_w, $bigthumb_h, $bigthumb_m);
      }

      $this->outvars = $row;
      $this->outvars['text'] = ($row['full_text'] != '') ? $row['full_text'] : $row['short_text'];
      $this->outvars['news_date'] = $row['news_date'] ? date($date_format, $row['news_date']) : '';
      $this->outvars['start_date'] = $row['start_date'] ? date($date_format, $row['start_date']) : '';
      $this->outvars['end_date'] = $row['end_date'] ? date($date_format, $row['end_date']) : '';

      $this->outvars['url'] = $this->getURL('news', $row['item_id']);
      $this->outvars['comment_count'] = $this->commentscol ? $this->commentscol->getNewsCommentCount($row['item_id']) : 0;
      $this->outvars['comments_label'] = ($this->outvars['comment_count'] == 1) ? '1 '.$this->LS('comment') : $this->outvars['comment_count'].' '.$this->LS('comments');

//      if ($show_type != 0) $this->outvars['url'] = $this->getURL($singlenews_url . $this->outvars['url'];

      $this->outvars['comments_url'] = $this->getURL('comments', $row['item_id']);
      $this->outvars['even'] = !($i & 1);
      $this->outvars['odd'] = ($i & 1);
      $this->outvars['first'] = ($i == 0);
      $this->outvars['last'] = ($i == count($data)-1);

      if ($isdesign)
      {
        $edit_url = $this->getComponentDialogLink('newsEdit', Array('news_id' => $row['item_id']), $this->LS('editnews'));

        $params = Array('news_id' => $row['item_id']);
        $del_url = $this->getComponentModifierLink('newsDelete', $params);
        $down_url = $this->getComponentModifierLink('newsDown', $params);
        $up_url = $this->getComponentModifierLink('newsUp', $params);

        $this->outvars['editbuttons'] =
           $this->getDesignIconOutput('down', $down_url, 150, 100) .
           $this->getDesignIconOutput('up', $up_url, 150, 100) .
           $this->getDesignIconOutput('edit', $edit_url, 650, 700) .
           $this->getDesignIconOutput('del', $del_url, 150, 100, $this->LS('really_del') );
      }

      $newslist .= $this->ReturnTemplate('news_template');
      $i++;
    }
    $this->outvars['newslist'] = $newslist;
    $this->outvars['archive_url'] = $this->getProperty('archive_url');

    $purl = '?';
    if ($isdesign)
    {
      $g = $_GET;
      unset($g['pg']);
      $purl .= http_build_query($g);
    }

    $pagelimit = intval($this->getProperty('pagelimit'));

    $this->outvars['pages'] = $this->getPagingTemplate('paging_template', ceil($count / $pagelimit), $purl, intval($_GET['pg']) );


    if ($isdesign)
    {
      $addurl = $this->getComponentDialogLink('newsEdit', Array(), $this->LS('addnews'));
      $addjs = $this->getDesignIconScript($addurl, 650, 700);
      $this->outvars['addbutton'] = '<input type="button" onclick="'.$addjs.'" value="Add"/>';
    }

    echo $this->ProcessTemplate('newslist_template');
  }

  function newsEdit()
  {
    if (!$this->initCollections()) return;
    $id = intval($_GET['news_id']);

    $form_data = $this->newscol->getDBProperties();

    if ($id)
    {
      $item = $this->newscol->getItemByIdAssoc($id);
      if (!$item) return;

      foreach ($form_data as $key => $field)
      {
        $form_data[$key]['value'] = $item[$key];
      }

    }

    $this->newscol->processFormBeforeDisplay($form_data, !$id);
    echo $this->getDialogFormOutput($form_data, 'newsEditSave', $_GET['title'], Array('news_id' => $id));
  }

  function newsEditSave()
  {
    if (!$this->initCollections()) return;
    $id = intval($_GET['news_id']);

    $item = $this->newscol->getItemAssocFromPost();
    $this->newscol->processItemBeforeSave($id, $item, !$id, $msg);
    if ($id)
    {
      $this->newscol->changeItemByIdAssoc($id, $item);
    }
    else
    {
      $ind = $this->newscol->addItemAssoc($item, $this->getProperty('sort_by') != 0);
    }

  }

  function newsDelete()
  {
    if (!$this->initCollections()) return;
    $id = intval($_GET['news_id']);

    if ($this->commentscol)
      $this->commentscol->deleteNewsComments($id);

    $this->newscol->delDBItem($id);
  }

  function newsDown()
  {
    if (!$this->initCollections()) return;
    $id = intval($_GET['news_id']);
    $this->newscol->moveItemDown($this->newscol->getItemInd($id));
  }

  function newsUp()
  {
    if (!$this->initCollections()) return;
    $id = intval($_GET['news_id']);
    $this->newscol->moveItemUp($this->newscol->getItemInd($id));
  }

  function outputSingleNews($news_id, $isdesign = false)
  {
    $this->setComponentFlag('news_view', true);

    $row = $this->item;
    if (!$row)
    {
      echo $this->LS('nonews');
      return;
    }

    if (!empty($row['image']))
    {
      $thumb_w = $this->getProperty('thumb_width');
      $thumb_h = $this->getProperty('thumb_height');
      $thumb_m = $this->getProperty('thumb_resizemode');
      $bigthumb_w = $this->getProperty('bigthumb_width');
      $bigthumb_h = $this->getProperty('bigthumb_height');
      $bigthumb_m = $this->getProperty('bigthumb_resizemode');

      $row['thumb'] = getThumbURL($row['image'], $thumb_w, $thumb_h, $thumb_m);
      $row['bigthumb'] = getThumbURL($row['image'], $bigthumb_w, $bigthumb_h, $bigthumb_m);
    }

    $date_format = $this->getProperty('date_format');

    $commentstpl = $this->getCommentsTemplate($news_id, $row);

    $this->outvars = $row;
    $this->outvars['text'] = ($row['full_text'] != '') ? $row['full_text'] : $row['short_text'];
      $this->outvars['news_date'] = $row['news_date'] ? date($date_format, $row['news_date']) : '';
      $this->outvars['start_date'] = $row['start_date'] ? date($date_format, $row['start_date']) : '';
      $this->outvars['end_date'] = $row['end_date'] ? date($date_format, $row['end_date']) : '';

    $this->outvars['image_js'] = 'javascript:window.open(\''.$row['image'].'\',\'galwin\',\'width=490,height=480,scrollbars=0,toolbar=0,resizable=1\'); return false;';

    $this->outvars['url'] = $this->getURL('news', $news_id);
    $this->outvars['comments'] = $commentstpl;
    $this->outvars['comment_count'] = $this->commentscol ? $this->commentscol->getNewsCommentCount($row['item_id']) : 0;
    $this->outvars['comments_label'] = ($this->outvars['comment_count'] == 1) ? '1 '.$this->LS('comment') : $this->outvars['comment_count'].' '.$this->LS('comments');

    $this->outvars['backurl'] = $this->getURL('newslist');
    ob_start();
    ?>
    <div class='soc_item twitter'><a href="https://twitter.com/share" class="twitter-share-button">Tweet</a>
      <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
    </div>
    <div class="soc_item google">
      <!-- Place this tag where you want the +1 button to render. -->
      <div class="g-plusone" data-size="standart" data-annotation="bubble" data-width="120"></div>

      <!-- Place this tag after the last +1 button tag. -->
      <script type="text/javascript">
        (function() {
          var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
          po.src = 'https://apis.google.com/js/platform.js';
          var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
        })();
      </script>
    </div>
    <div class="soc_item facebook">
      <div class="fb-like" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>
    </div>
    <div class="cb"></div>

    <?
    $soc = ob_get_contents();
    ob_end_clean();
    $this->outvars['soc'] = $soc;

    $info = $this->getNewsListInfo();
    $i = array_search($news_id, $info['ids']);
    if ($i !== false)
    {
      $this->outvars['prevurl'] = ($i > 0) ? $this->getURL('news', $info['ids'][$i-1]) : '';
      $this->outvars['nexturl'] = ($i < count($info['ids'])-1) ? $this->getURL('news', $info['ids'][$i+1]) : '';
    }

    return $this->ProcessTemplate('singlenews_template');

  }

  function outputComments($news_id)
  {
    $row = $this->newscol->GetItemByIdAssoc($news_id);
    if (!$row)
    {
      echo $this->LS('nonews');
      return;
    }

    echo $this->getCommentsTemplate($news_id, $row);
  }

  function getCommentsTemplate($news_id, $news_row)
  {
    if (!$this->commentscol) return '';

    if (isset($_POST['niks']))
    {
      $errmsg = '';
      if (!$_POST['niks'] || !$_POST['komentars'] || !$_POST['drosibaskods'])
      {
        $errmsg = $this->LS('no_fields');
      }
      else if ($_POST['drosibaskods'] != $_SESSION['news_comment_security_code'])
      {
        $errmsg = $this->LS('no_security');
      }
      else
      {
        $_SESSION['news_comment_nick'] = $_POST['niks'];
        $ind = $this->commentscol->AddItemAssoc(Array(
          'news_id'       => $news_id,
          'nick'          => $_POST['niks'],
          'comment_text'  => $_POST['komentars'],
          'log_time'      => time(),
          'log_ip'        => ip2long($_SERVER['REMOTE_ADDR'])
        ));
        $id = $this->commentscol->GetItemID($ind);
        header('Location: '.$this->getURL('news', $news_id).'#comment'.$id);
        exit();
      }
    }
    $pagelimit = intval($this->getProperty('comments_pagelimit'));

    $data = $this->commentscol->getNewsComments($news_id, intval($_GET['pg']) * $pagelimit, $pagelimit);
    $count = $this->commentscol ? $this->commentscol->getNewsCommentCount($news_id) : 0;
    $date_format = $this->getProperty('date_format');
    $commentslist = '';
    $n = 1 + intval($_GET['pg']) * $pagelimit;
    $i = 0;
    foreach ($data as $row)
    {
      $this->outvars = $row; //array_merge($this->outvars, $row);
      $this->outvars['log_time'] = date($date_format, $this->outvars['log_time']);
      $this->outvars['log_ip'] = long2ip($this->outvars['log_ip']);
      $this->outvars['comment_text'] = nl2br(htmlspecialchars($row['comment_text']));
      $this->outvars['comment_n'] = $n;
      $this->outvars['even'] = !($i & 1);
      $this->outvars['odd'] = ($i & 1);
      $this->outvars['first'] = ($i == 0);
      $this->outvars['last'] = ($i == count($data)-1);
/*
      if ($this->visualedit)
      {
        $edit_url = $this->getComponentDialogLink('commentEdit', Array('comment_id' => $row['item_id']), $this->LS('editcomment'));

        $params = Array('comment_id' => $row['item_id']);
        $del_url = $this->getComponentModifierLink('commentDelete', $params);

        $this->outvars['editbuttons'] =
           $this->getDesignIconOutput('edit', $edit_url, 450, 700) .
           $this->getDesignIconOutput('del', $del_url, 150, 100);
      }
*/
      $n++;

      $commentslist .= $this->returnTemplate('comment_template');
    }
    $this->outvars['errmsg'] = $errmsg;
    $this->outvars['commentslist'] = $commentslist;

    $nick = isset($_POST['niks']) ? $_POST['niks'] : $_SESSION['news_comment_nick'];
    $this->outvars['nick'] = htmlspecialchars($nick);

    $this->outvars['comment'] = htmlspecialchars($_POST['komentars']);
    $this->outvars['comment_count'] = $count;

    $comments_lbl = ($this->outvars['comment_count'] == 1) ? '1 '.$this->LS('comment') : $this->outvars['comment_count'].' '.$this->LS('comments');
    $this->outvars['comments_label'] = sprintf($this->LS('zinai'), $comments_lbl, $this->getURL('news', $news_id), $news_row['title']);

    $code = generate_random_string(2, 'abcdefghijkmnopqrstuvwxyz023456789');
    $_SESSION['news_comment_security_code'] = $code;
    $this->outvars['security_code'] = '&#'.ord($code[0]).';' . '&#'.ord($code[1]).';';

    $this->outvars['pages'] = $this->getPagingTemplate('commentspaging_template', ceil($count / $pagelimit), '?', intval($_GET['pg']) );

    $news_messages = Array(
      'no_fields'   => $this->LS('no_fields')
    );
    $this->outvars['script'] = '
      <script type="text/javascript">
        var news_messages = '.json_encode($news_messages).';

        function newsAddComment()
        {
          var form = document.getElementById("commentform");

          if (!form.niks.value || !form.komentars.value)
          {
            var e = document.getElementById("news_comment_err");
            if (e) e.innerHTML = news_messages.no_fields;
            return false;
          }

          form.drosibaskods.value = String.fromCharCode('.ord($code[0]).') + String.fromCharCode('.ord($code[1]).');
          return true;
        }

      </script>
    ';

    $tpl = $this->returnTemplate('commentslist_template');

    $tpl .= '
      <script type="text/javascript">
        var e = document.getElementById("ievadi_drosibaskodu");
        if (e) e.style.display = "none";
      </script>
    ';
    return $tpl;
  }

  function commentEdit()
  {
    if (!$this->initCollections()) return;
    $id = intval($_GET['comment_id']);

    $form_data = $this->commentscol->getDBProperties();
    $form_data['news_id']['type'] = 'hidden';
    $form_data['log_ip']['type'] = 'hidden';

    if ($id)
    {
      $item = $this->commentscol->getItemByIdAssoc($id);
      if (!$item) return;

      foreach ($form_data as $key => $field)
      {
        $form_data[$key]['value'] = $item[$key];
      }

    }

    $this->commentscol->processFormBeforeDisplay($form_data, !$id);
    echo $this->getDialogFormOutput($form_data, 'commentEditSave', $_GET['title'], Array('comment_id' => $id));
  }

  function commentEditSave()
  {
    if (!$this->initCollections()) return;
    $id = intval($_GET['comment_id']);

    $item = $this->commentscol->getItemAssocFromPost();
    $this->commentscol->processItemBeforeSave($id, $item, !$id, $msg);
    if ($id)
    {
      $this->commentscol->changeItemByIdAssoc($id, $item);
    }
    else
    {
      $ind = $this->commentscol->addItemAssoc($item);
    }

  }

  function commentDelete()
  {
    if (!$this->initCollections()) return;
    $id = intval($_GET['comment_id']);

    $this->commentscol->delDBItem($id);
  }

  function getPagingTemplate($tplname, $pages, $url, $active)
  {
    if ($pages <= 1) return '';

    if ($this->pagedev_id)
    {
      $g = $_GET;
      unset($g['pg']);
      $url = '?'.http_build_query($g);
    }
    else
      $url .= '?';

    $s = '';
    for ($i=0; $i<$pages; $i++)
    {
      $this->outvars['n'] = $i + 1;
      $this->outvars['url'] = $url.'&pg='.$i;
      $this->outvars['active'] = ($active == $i);
      $s .= $this->returnTemplate($tplname);
    }
    return $s;
  }

  function getURL($type, $id=0)
  {
    $url = '';
    $path = '';
    $hash = '';
    $query = Array();
    switch ($type)
    {
      case 'news':
        $url = $this->show_type ? $this->singlenews_url : $this->page_url;
        $path = 'get/nid/'.$id;
        $query['nid'] = $id;
        break;

      case 'comments':
        $url = $this->show_type ? $this->comments_url : $this->page_url;
        $path = 'get/nid/'.$id;
        $query['nid'] = $id;
        $hash = '#comments';
        break;

      case 'newslist':
        $url = $this->show_type ? $this->newslist_url : $this->page_url;
        $query['nid'] = 0;
        break;
    }

    if (!empty($url))
    {
      if ($_GET['action'] == 'inline_frame_page') //($this->visualedit)
      {
        $page_id = PageIdByPath($url, $this->site_id);
        if (!$page_id) return '';

        $g = $_GET;
        $g['page_id'] = $page_id;
        $g['pagedev_id'] = sqlQueryValue("SELECT pagedev_id FROM ".$this->site_id."_pagesdev WHERE page_id=".$page_id);

        $g = array_merge($g, $query);
        return '?'.http_build_query($g).$hash;
      }

      return $url.$path.$hash;
    }

    return '';
  }

  function indexData($lastindextime)
  {
    if (!$this->initCollections()) return null;

    $this->initVars();
//        'lookup'    => Array('0:Automātisks', '1:Saraksts', '2:Vienums', '3:Komentāri'),
    $data = Array();
    $type = $this->getProperty('show_type');
    if (!$type || $type == 2)
    {
      $news = $this->newscol->getItemsForIndex($lastindextime);
      $data[$this->newscol->collection_id] = Array();
      foreach ($news as $row)
      {
        $data[$this->newscol->collection_id][$row['item_id']] = Array(
          'query' => 'get/nid/'.$row['item_id'],
          'title' => $row['title'],
          'data' => $row['title'].' '.strip_tags($row['short_text']).' '.strip_tags($row['full_text']).' '.$row['author']
        );
      }
    }

    if ((!$type || $type == 3) && $this->commentscol)
    {
      $comments = $this->commentscol->getItemsForIndex($lastindextime);
      $data[$this->commentscol->collection_id] = Array();
      foreach ($comments as $com)
      {
        $data[$this->commentscol->collection_id][$com['item_id']] = Array(
          'query' => $type ? 'get/nid/'.$com['news_id'] : 'get/nid/'.$com['news_id'].'#comments',
          'title' => $com['nick'],
          'data' => $com['comment_text'].' '.$com['nick']
        );
      }
    }

    return $data;
  }

  function initCollections()
  {
    if (!($this->newscol = $this->initPropertyCollection('newscol')) ) return false;
    $this->newscol->indexAtPage($this->page_id);

    $this->commentscol = $this->initPropertyCollection('commentscol');
    if ($this->commentscol)
      $this->commentscol->indexAtPage($this->page_id);

    return true;
  }

  function toolbar(&$script, &$toolbars, &$btnids, &$onclick)
  {
    return true;
  }

  function SetProperties()
  {
    $this->properties = Array(
      'name' => Array(
        'label'     => 'Nosaukums:',
        'type'      => 'str'
      ),

      'newscol' => Array(
        'label'     => 'Ziņu kolekcija:',
        'type'      => 'collection',
        'collectiontype' => 'newnewscollection',
        'lookup'    => GetCollections($this->site_id, 'newnewscollection'),
        'dialog'    => '?module=dialogs&action=collection&site_id='.$this->site_id.'&page_id='.$this->page_id.'&category_id='
      ),

      'commentscol' => Array(
        'label'     => 'Komentāru kolekcija:',
        'type'      => 'collection',
        'collectiontype' => 'newnewscommentscollection',
        'lookup'    => GetCollections($this->site_id, 'newnewscommentscollection'),
        'dialog'    => '?module=dialogs&action=collection&site_id='.$this->site_id.'&page_id='.$this->page_id.'&category_id='
      ),

      'image_width' => Array(
        'label'     => 'Attēla platums:',
        'type'      => 'str'
      ),

      'image_height' => Array(
        'label'     => 'Attēla augstums:',
        'type'      => 'str'
      ),

      'image_resizemode' => Array(
        'label'     => 'Attēla resize mode:',
        'type'      => 'list',
        'lookup'    => Array("1:By width", "2:By height", "3:By width if too big", "4:By height if too big", "5:By width and height", "6:By width and height if too big", "7:Crop by width and height"),
        'value'     => 6
      ),

      'thumb_width' => Array(
        'label'     => 'Ziņu listes thumb platums:',
        'type'      => 'str',
        'value'     => 100
      ),

      'thumb_height' => Array(
        'label'     => 'Ziņu listes thumb augstums:',
        'type'      => 'str',
        'value'     => 75
      ),

      'thumb_resizemode' => Array(
        'label'     => 'Ziņu listes thumb resize mode:',
        'type'      => 'list',
        'lookup'    => Array("1:By width", "2:By height", "3:By width if too big", "4:By height if too big", "5:By width and height", "6:By width and height if too big", "7:Crop by width and height"),
        'value'     => 6
      ),

      'bigthumb_width' => Array(
        'label'     => 'Ziņas bigthumb platums:',
        'type'      => 'str',
        'value'     => 200
      ),

      'bigthumb_height' => Array(
        'label'     => 'Ziņas bigthumb augstums:',
        'type'      => 'str',
        'value'     => 150
      ),

      'bigthumb_resizemode' => Array(
        'label'     => 'Ziņas bigthumb resize mode:',
        'type'      => 'list',
        'lookup'    => Array("1:By width", "2:By height", "3:By width if too big", "4:By height if too big", "5:By width and height", "6:By width and height if too big", "7:Crop by width and height"),
        'value'     => 6
      ),

      'archive_mode' => Array(
        'label'     => 'Arhivēšanas režīms:',
        'type'      => 'list',
        'lookup'    => Array('0:Manuāls', '1:Automātisks'),
      ),

      'archiving_age' => Array(
        'label'     => 'Pēc cik dienām ielikt arhīvā,<br/>ja arhivēšanas režīms = auto:',
        'type'      => 'str',
        'value'     => 7
      ),

      'list_type' => Array(
        'label'     => 'Saraksta tips:',
        'type'      => 'list',
        'lookup'    => Array('0:Nearhivētās', '1:Arhivētās', '2:Visas'),
      ),

      'show_type' => Array(
        'label'     => 'Veidnes tips:',
        'type'      => 'list',
        'lookup'    => Array('0:Automātisks', '1:Saraksts', '2:Vienums', '3:Komentāri'),
      ),

      'singlenews_url' => Array(
        'label'     => 'Links uz ziņas vienumu:',
        'type'      => 'dialog',
        'dialog'    => '?module=dialogs&action=links&site_id='.$this->site_id,
      ),

      'newslist_url' => Array(
        'label'     => 'Links uz ziņu sarakstu:',
        'type'      => 'dialog',
        'dialog'    => '?module=dialogs&action=links&site_id='.$this->site_id,
      ),

      'commentslist_url' => Array(
        'label'     => 'Links uz komentāriem:',
        'type'      => 'dialog',
        'dialog'    => '?module=dialogs&action=links&site_id='.$this->site_id,
      ),

      'archive_url' => Array(
        'label'     => 'Links uz ziņu arhīvu:',
        'type'      => 'dialog',
        'dialog'    => '?module=dialogs&action=links&site_id='.$this->site_id,
      ),

      'sort_by' => Array(
        'label'     => 'Kārtot pēc:',
        'type'      => 'list',
        'lookup'    => Array('0:Nekārtot (pēc lietotāja izvēles)', '1:Pēc datuma'),
      ),

      'pagelimit' => Array(
        'label'     => 'Cik ziņas vienā lapā:',
        'type'      => 'str',
        'value'     => 30
      ),

      'comments_pagelimit' => Array(
        'label'     => 'Cik komentāri vienā lapā:',
        'type'      => 'str',
        'value'     => 30
      ),

      'date_format' => Array(
        'label'     => 'Datuma formāts:',
        'type'      => 'str',
        'value'     => 'd.m.Y, H:i'
      ),

      'comment_mode' => Array(
        'label'     => 'Atļaut komentēt:',
        'type'      => 'list',
        'lookup'    => Array('0:Visiem lietotājiem', '1:Tikai reģistrētiem')
      ),

      'newslist_template' => Array(
        'label'     => 'Ziņu saraksta veidne:',
        'type'      => 'template',
        'rows'      => 8,
        'cols'      => 30,
        'tags'      => '{!newslist};{!archive_url};{!pages};{!addbutton};',
        'samples'   => Array(
          'Default'     => '{!newslist}
[if !pages]<div>&raquo; {!pages}</div>[/if]
<br/><div>
&raquo; <a href="{!archive_url}">{^news_archive^}</a>
[if !addbutton]<br/>{!addbutton}[/if]
</div>'
        )
      ),

      'news_template' => Array(
        'label'     => 'Ziņas veidne:',
        'type'      => 'template',
        'rows'      => 8,
        'cols'      => 30,
        'tags'      => '{!item_id};{!title};{!short_text};{!full_text};{!text};{!image};{!thumb};{!bigthumb};{!news_date};{!start_date};{!end_date};{!author};{!archived};{!allow_comments};{!url};{!comment_count};{!comments};{!comments_label};{!comments_url};{!odd};{!even};{!editbuttons};',
        'samples'   => Array(
          'Default'     => '          <div class="content_new[if !odd] new_2[/if]">
            [if !thumb]<a href="{!url}"><img src="{!thumb}" align="left" alt="{!title}"/></a>[/if]
            <h1><a href="{!url}">{!title}</a></h1>
            <div class="date">{!news_date}</div>
	   {!short_text}
            <div class="content_new_bottom">
              <div class="comment"><a href="{!comments_url}">{^comments_title^} ({!comment_count})</a></div>
              <div class="more"><a href="{!url}">{^readmore^}</a></div>
            </div>
            {!editbuttons}
          </div>
'
        )
      ),

      'singlenews_template' => Array(
        'label'     => 'Ziņas vienuma veidne:',
        'type'      => 'template',
        'rows'      => 8,
        'cols'      => 30,
        'tags'      => '{!item_id};{!title};{!short_text};{!full_text};{!text};{!image};{!thumb};{!bigthumb};{!news_date};{!start_date};{!end_date};{!author};{!archived};{!allow_comments};{!url};{!comment_count};{!comments_label};{!comments};{!image_js};{!prevurl};{!nexturl};{^readmore^};{^author^};{^date^};',
        'samples'   => Array(
          'Default'     => '<div class="news">[if !bigthumb]<a href="{!image}" onclick="{!image_js}"><img src="{!bigthumb}" alt="" align="left" /></a>[/if]
{!text}</div><br/>
<a href="{!backurl}">&laquo; {^back^}</a>
<div style="clear:both"></div>
{!comments}'
        )
      ),

      'commentslist_template' => Array(
        'label'     => 'Komentāru saraksta veidne:',
        'type'      => 'template',
        'rows'      => 8,
        'cols'      => 30,
        'tags'      => '{!script};{!nick};{!comment};{!comment_count};{!commentslist};{!errmsg};{!pages};',
        'samples'   => Array(
          'Default'     => '{!script}
<div id="comments">
<h3>{!comments_label}</h3>
{!commentslist}
[if !pages]<div>&raquo; {!pages}</div>[/if]
<br/>
<div style="color: red">&nbsp;<strong><span id="news_comment_err">{!errmsg}</span></strong></div>
<form id="commentform" method="post" action="#commentform" onsubmit="return newsAddComment()">
<table cellspacing="0" cellpadding="0">
  <tr><td style="vertical-align: top">{^nick^}:</td><td><input type="text" name="niks" value="{!nick}"/></td></tr>
  <tr><td style="vertical-align: top">{^comment_field^}:</td><td><textarea name="komentars" cols="40" rows="6">{!comment}</textarea></td></tr>
  <tr id="ievadi_drosibaskodu"><td style="vertical-align: top">{^input^}: <strong>{!security_code}</strong></td><td><input type="text" name="drosibaskods" value=""/></td></tr>
  <tr><td></td><td><input type="submit" value="{^send_comment^}"/></td></tr>
</table>
</form>
</div>'
        )
      ),

      'comment_template' => Array(
        'label'     => 'Komentāra veidne:',
        'type'      => 'template',
        'rows'      => 8,
        'cols'      => 30,
        'tags'      => '{!item_ind};{!nick};{!comment_text};{!log_time};{!log_ip};{!comment_n};{!odd};{!even};',
        'samples'   => Array(
          'Default'     => '<div id="comment{!item_id}"><hr/>
{!comment_n}. <strong>{!nick}</strong> @ {!log_time}:<br/>
{!comment_text}
</div>
'
        )
      ),

      'paging_template' => Array(
        'label'     => 'Ziņu saraksta lapošanas veidne:',
        'type'      => 'template',
        'rows'      => 8,
        'cols'      => 30,
        'tags'      => '{!n};{!url};{!active};',
        'samples'   => Array(
          'Default'     => '<a href="{!url}"[if !active] style="font-weight:bold"[/if]>{!n}</a> '
        )
      ),

      'commentspaging_template' => Array(
        'label'     => 'Komentāru saraksta lapošanas veidne:',
        'type'      => 'template',
        'rows'      => 8,
        'cols'      => 30,
        'tags'      => '{!n};{!url};{!active};',
        'samples'   => Array(
          'Default'     => '<a href="{!url}"[if !active] style="font-weight:bold"[/if]>{!n}</a> '
        )
      ),

      'show_full_text' => Array(
        'label'     => 'Rādīt pilno tekstu:',
        'type'      => 'check',
        'value'     => 'on'
      ),

      'show_image' => Array(
        'label'     => 'Rādīt attēlu:',
        'type'      => 'check',
        'value'     => 'on'
      ),

      'show_news_date' => Array(
        'label'     => 'Rādīt ziņas datumu:',
        'type'      => 'check',
        'value'     => 'on'
      ),

      'show_start_date' => Array(
        'label'     => 'Rādīt ziņas sākuma datumu:',
        'type'      => 'check',
        'value'     => 'on'
      ),

      'show_end_date' => Array(
        'label'     => 'Rādīt ziņas beigu datumu:',
        'type'      => 'check',
        'value'     => 'on'
      ),

      'show_archived' => Array(
        'label'     => 'Lietot iespēju "arhivēta":',
        'type'      => 'check',
        'value'     => 'on'
      ),

      'show_author' => Array(
        'label'     => 'Rādīt autoru:',
        'type'      => 'check',
        'value'     => 'on'
      ),

      'use_title_srv' => Array(
        'label'     => 'Lietot title servisu:',
        'type'      => 'check',
        'value'     => 'on'
      ),

    );

    foreach ($this->properties as $key => $prop)
    {
      if (isset($prop['samples']['Default']) && !isset($prop['value']) )
        $this->properties[$key]['value'] = $prop['samples']['Default'];
    }
  }
}


?>