<?php

use Constructor\WebApp;

class LEDAkcijasSmallCart extends component
{

    public function output()
    {

        $items = WebApp::$app->cart->getItems();
        $open = WebApp::$app->session->getFlash('cart_item_added', false);

        echo $this->render('index', ['items' => $items, 'open' => $open]);

    }

}