<?php

use Constructor\WebApp;

class LEDAkcijasLogin extends component
{

    public function output()
    {

        $user = WebApp::$app->user->get();

        if($user){
            echo $this->render('loggedin', ['user' => $user]);
        }else{
            echo $this->render('index');
        }

    }

}