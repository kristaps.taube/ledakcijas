<?php
/**
*
*  Title: Upgrade
*  Author: Kristaps Taube <ktaube@datateks.lv>
*  Date: 25.04.2014
*  Project: E-Veikals
*
*/

require_once("class.component.php");
require_once($GLOBALS['cfgDirRoot']."library/CrossDB.php");

class EVeikalsUpgrade extends component{

  // Datasource
  private $datasource = "db1";

  function __construct($name){
    parent::__construct($name);
    $this->SetProperties();
    $this->CrossDB = new CrossDB($this->datasource);

    // Collection property => import table
    $this->table_relation = array(
      "prods" => "4_coltable_shop_products",
      "prod_images" => "4_coltable_shop_customimages",
      "cats" => "4_coltable_shop_categories",
      "brands" => "4_coltable_shop_brands",
      "units" => "4_coltable_shop_units",
      "regions" => "4_coltable_shop_regions",
      "orders" => "4_coltable_shop_orders",
      "order_products" => "4_coltable_shop_orders_products",
      "users" => "4_coltable_shop_users",
      "user_cat_discount_collection" => "4_coltable_shop_usercatdiscounts",
    );

  }

  function output(){

    if(!$this->initCollections()){
      echo $this->name.": Can't init collections.<br />";
      return;
    }

    if(!$this->CrossDB){
      echo "Can't connect to datasource";
      return;
    }

    if(isset($_POST['DoUpgrade'])){
      $this->clear(); // clearing db from data
      $this->DoUpgrade();
    }

    ?>
      <form method='post' action=''>
        <input type='submit' name='DoUpgrade' value='Do upgrade' />
      </form>
    <?

  }

  function clear(){

    foreach($this->properties as $key => $prop){
      if($prop['type'] == "collection"){
        $this->$key->truncate();
      }
    }

  }

  function DoUpgrade(){

    set_time_limit(300); // 5 min
    ini_set("memory_limit", "1024M");

    foreach($this->table_relations as $prop => $table){

      foreach($this->CrossDB->get_table("SELECT * FROM `".$table."`") as $item){

        foreach($item as $key => $value){
          $item[$key] = mysql_real_escape_string($value);
        }

        $this->$prop->AddItemAssoc($item);

      }

    }

  }

  function SetProperties(){
    $this->properties = Array(

      "name"        => Array(
        "label"     => "Name:",
        "type"      => "str"
      ),

      "cats" => Array(
        "label"       => "Category collection:",
        "type"        => "collection",
        "collectiontype" => "shopcatcollection",
        "lookup"      => GetCollections($this->site_id, "shopcatcollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "prods" => Array(
        "label"       => "Product collection:",
        "type"        => "collection",
        "collectiontype" => "shopprodcollection",
        "lookup"      => GetCollections($this->site_id, "shopprodcollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "users" => Array(
        "label"       => "User collection:",
        "type"        => "collection",
        "collectiontype" => "shopusercollection",
        "lookup"      => GetCollections($this->site_id, "shopusercollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "user_cat_discount_collection" => Array(
        "label"       => "User category discount collection:",
        "type"        => "collection",
        "collectiontype" => "shopusercatatlaidescollection",
        "lookup"      => GetCollections($this->site_id, "shopusercatatlaidescollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "units" => Array(
        "label"       => "Units collection:",
        "type"        => "collection",
        "collectiontype" => "shopunitscollection",
        "lookup"      => GetCollections($this->site_id, "shopunitscollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "prod_images" => Array(
        "label"       => "Product images collection:",
        "type"        => "collection",
        "collectiontype" => "shopcustomimagescollection",
        "lookup"      => GetCollections($this->site_id, "shopcustomimagescollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "related_prods" => Array(
        "label"       => "Related products collection:",
        "type"        => "collection",
        "collectiontype" => "shoprelatedprodscollection",
        "lookup"      => GetCollections($this->site_id, "shoprelatedprodscollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "brands" => Array(
        "label"       => "Brands collection:",
        "type"        => "collection",
        "collectiontype" => "shopbrandscollection",
        "lookup"      => GetCollections($this->site_id, "shopbrandscollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "prices" => Array(
        "label"       => "Prices collection:",
        "type"        => "collection",
        "collectiontype" => "shoppricescollection",
        "lookup"      => GetCollections($this->site_id, "shoppricescollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "regions" => Array(
        "label"       => "Region collection:",
        "type"        => "collection",
        "collectiontype" => "shopregionscollection",
        "lookup"      => GetCollections($this->site_id, "shopregionscollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "orders" => Array(
        "label"       => "Order collection:",
        "type"        => "collection",
        "collectiontype" => "EVeikalsOrderCollection",
        "lookup"      => GetCollections($this->site_id, "EVeikalsOrderCollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

      "order_products" => Array(
        "label"       => "Order collection:",
        "type"        => "collection",
        "collectiontype" => "shoporderprodcollection",
        "lookup"      => GetCollections($this->site_id, "shoporderprodcollection"),
        "dialog"      => "?module=dialogs&action=collection&site_id=".$this->site_id."&page_id=".$this->page_id."&category_id=",
        "dialogw"     => "600"
      ),

    );
  }



}
