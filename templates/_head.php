<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="msapplication-tap-highlight" content="no">
<meta name="theme-color" content="#000">
<title>{%title%}</title>

<link rel="stylesheet" href="/styles/assets.min.css">
<link rel="stylesheet" href="/styles/style.min.css">
<link rel="stylesheet" href="/styles/custom.css">

{%header%}