<?php

use Constructor\WebApp;

?>
<!doctype html>
<html lang="lv">
<head>
{%template:_head%}
</head>
<body>
    <div id="mobile-menu">
        <?php echo LEDAkcijasMobileMenu::run()?>
    </div>
    <div id="main-wrapper">
        <header id="header">
            {%template:_header%}
        </header>

        <div id="main-top">
            <div class="container">
                <div class="row no-gutters">
                    <?php echo LEDAkcijasCatalogMenu::run(['inner' => true]) ?>
                    <div class="col">
                        <div id="breadcrumbs">
                            <a href="/">Sākums</a>
                            <a href="#cat">LED spuldzes</a>
                            <!-- <a href="#">T8 LED dienasgaismas spuldze ''NANO Plastic'', rotējami kontakti</a> izvēle ir tavā ziņā-->
                            <span>T8 LED dienasgaismas spuldze ''NANO Plastic'', rotējami kontakti</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <section id="main">
            <div class="container">
                <?php echo $content ?>
            </div>
        </section>

        <footer>
            {%template:_footer%}
        </footer>
    </div>

    <script src="/scripts/dependencies.min.js"></script>
    <script src="/scripts/scripts.min.js"></script>
    <script src="/scripts/custom.js"></script>

</body>
</html>