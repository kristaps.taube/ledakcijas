<?php

use Constructor\WebApp;

?>
<div class="container">
    <div class="row">
        <div class="col-12 col-sm-6 col-lg-3">
            <div class="header"><?php echo WebApp::l("Par LEDakcijas.lv")?></div>
            <div class="content nav">
                <?php echo LEDAkcijasBottomMenu::run() ?>
            </div>
        </div>
        <div class="col-12 col-sm-6 col-lg-3">
            <div class="header">
                <a href="#"><?php echo WebApp::l('Kontakti')?></a>
            </div>
            <div class="content contacts">
                <ul>
                    <li class="phone">
                        <a href="tel:<?php echo WebApp::l("+371 200 22 881")?>"><?php echo WebApp::l("+371 200 22 881")?></a>
                    </li>
                    <li class="email">
                        <a href="mailto:<?php echo WebApp::l("info@ledakcijas.lv")?>"><?php echo WebApp::l("info@ledakcijas.lv")?></a>
                    </li>
                    <li class="pin">
                        <?php echo WebApp::l("Katlakalna iela 10, Rīga, LV-1073")?>
                    </li>
                </ul>

                <div class="soc-wrap">
                    <a href="https://www.facebook.com/ledakcijas/" target="_blank" class="soc-item facebook" style="background-image:url('/images/html/facebook-logo.svg')"></a>
                    <a href="https://twitter.com/ledakcijas" class="soc-item twitter" target="_blank" style="background-image:url('/images/html/twitter-logo.svg')"></a>
                    <a href="https://www.youtube.com/channel/UCbpaCBf56Vo3EEXdIXyYyCw" target="_blank" class="soc-item youtube" style="background-image:url('/images/html/youtube-logo.svg')"></a>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-6 col-lg-3">
            <div class="header"><?php echo WebApp::l("Darba laiks")?></div>
            <div class="content">
                <ul>
                    <li><?php echo WebApp::l('P.,O.,T.,Pk.: 9:00-18:00')?></li>
                    <li><?php echo WebApp::l('Cet: 9:00 - 19:00')?></li>
                    <li><?php echo WebApp::l('Se.: 9:00 - 15:00')?></li>
                    <li><?php echo WebApp::l('Sv.: Nestrādājam')?></li>
                    <li>
                        <img src="/images/html/cards.svg" alt="" style="height:30px">
                        <img src="/images/html/swedbank.png" alt="">
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-12 col-sm-6 col-lg-3">
            <div class="header"><?php echo WebApp::l("Pieteikties jaunumiem")?></div>
            <div id='subscribe-wrap'>
            {%component:mailinglistex:mailinglis%}
            </div>
            <div class="links d-flex align-items-center justify-content-between">
                <a href="#salidzini">
                    <img src="/images/html/salidzini.png" alt="">
                </a>
                <a href="#salidzini">
                    <img src="/images/html/kurpirkt.png" alt="">
                </a>
                <a href="#salidzini">
                    <img src="/images/html/gudriem.png" alt="">
                </a>
            </div>
        </div>
    </div>
    <div class="row no-gutters copy">
        <div class="col-12">
            <div class="formatedtext">
                <p><?php echo WebApp::l("Copyright Ledakcijas.lv, 2018")?></p>
            </div>
        </div>
    </div>
</div>