<?php

use Constructor\Url;
use Constructor\WebApp;
use LEDAkcijas\CategoryAmountStepCollection;
use LEDAkcijas\ProductAmountPriceCollection;

$manager = \shopmanager::getInstance();
$lang = WebApp::$app->getLanguage();

$prices = $manager->getProductPrice($product, WebApp::$app->user->get());

$price = $prices['price'];
$base_price = $prices['base_price'];

$product_steps = ProductAmountPriceCollection::getInstance()->getByProduct($product['item_id']);
$js_steps = [];
foreach($product_steps as $key => $value){
    if(!($value['price'] > 0)){
        unset($product_steps[$key]);
        continue;
    }
    $js_steps[] = ['from' => $value['count_from'], 'to' => $value['count_to'], 'price' => $value['price']];
}

$unit = WebApp::l("gab.");

$pictures = [];
$pictures[] = $product['picture'] ? $product['picture'] : "/images/html/blank_product.png";

$featured = ($price != $base_price);

?>
<div id="product" data-id="<?php echo $product['item_id']?>" data-featured="<?php echo ($featured) ? 1 : 0 ?>">
    <div class="row top">
        <div class="col-12 col-md-6">
            <div class="gallery">
                <div class="tags">
                    <?php if($featured){ ?>
                    <div class="tag discount"></div>
                    <?php } ?>
                </div>
                <div class="main-image">
                    <div class="carousel slide" data-interval="false">
                        <div class="carousel-inner">
                            <?php foreach($pictures as $i => $picture){ ?>
                            <div class="carousel-item <?php echo !$i ? "active" : ""?>">
                                <img class="d-block w-100" src="<?php echo getThumbUrl($picture, 540, 470, 6)?>" alt="Slide">
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>

                <div class="thumbnails owl-carousel">
                    <?php foreach($pictures as $i => $picture){ ?>
                    <div class="thumbnail <?php echo !$i ? "current" : ""?>" data-slide="<?php echo $i ?>">
                        <img src="<?php echo getThumbUrl($picture, 165, 140, 7)?>" alt="">
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-6">
            <h2><?php echo $product['name_'.$lang]?></h2>
            <div class="code"><?php echo WebApp::l("Preces kods")?>: <?php echo $product['code']?></div>
            <div class="availability">
                <div class="row">
                    <div class="col-12 col-md-4">
                        <span><?php echo WebApp::l("Pieejamība")?>:</span>
                    </div>
                    <div class="col-12 col-md-8">
                        <div class="row">
                            <?php if($product['available_sample']){ ?>
                            <span class="item availability" style="background-image: url('/images/html/availability.svg')"><?php echo WebApp::l("Paraugs pieejams veikalā, piegāde 3 darba dienu laikā no pasūtījuma izveidošanas.")?></span>
                            <?php } ?>

                            <?php if($product['not_available']){ ?>
                            <span class="item availability"><?php echo WebApp::l("Prece patreiz nav pieejama ,uzdot jautājumu par paredzamo piegādes laiku. ")?></span>
                            <?php } ?>

                            <?php if($product['available_in_shop']){ ?>
                            <span class="item truck availability" style="background-image: url('/images/html/truck.svg')"><?php echo WebApp::l("Pieejams veikalā")?></span>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="add-to-cart-form">
                <form action="<?php echo Url::get("/product/add-to-cart")?>" method="POST">
                    <div class="row">
                        <div class="col">
                            <label><?php echo WebApp::l("Cena")?></label>
                            <div class="price-wrap" data-unit="<?php echo $unit;?>">
                                <?php if($price != $base_price){ ?>
                                <span class="new-price">
                                    &euro;
                                    <span class="value" data-default="<?php echo $price?>"><?php echo number_format($price, 2)?></span>/<?php echo $unit;?>
                                </span>
                                <?php } ?>
                                <span class="price">
                                    &euro;
                                    <span class="value" data-default='<?php echo $base_price?>'><?php echo number_format($base_price, 2)?></span>/<?php echo $unit;?>
                                </span>
                            </div>
                            <?php if(!$featured && $product_steps){ ?>
                            <div class="price-steps">
                                <?php foreach($product_steps as $step){ ?>
                                <p>
                                    <?php
                                        echo WebApp::l("Pērc [counts] - maksā [price]", [
                                            'replace' => [
                                                '[counts]' => '<b>'.$step['count_from'].$unit.($step['count_to'] ? '-'.$step['count_to'].$unit : ' '.WebApp::l("un vairāk")).'</b>',
                                                '[price]' => '<b>&euro;'.number_format($step['price'], 2).'/'.$unit.'</b>'
                                            ]
                                        ]);
                                    ?>
                                </p>
                                <?php } ?>
                            </div>
                            <?php } ?>
                            <div class="individual-price">
                                <p>
                                    <span class="large"><?php echo WebApp::l("Vēlies individuālu cenas piedāvājumu?")?></span>
                                    <br>
                                    <b>
                                        <a href="#q">&raquo;<?php echo WebApp::l("Pieprasīt individuālu cenas piedāvājumu")?>&laquo;</a>
                                    </b>
                                </p>
                            </div>
                            <div class="share medium-large">
                                <?php echo WebApp::l("Dalies un saņem atlaidi! ->")?>
                                <div class="soc-items">
                                    <a href="" class="soc-item share facebook">
                                        <img src="/images/html/twitter-share.svg" alt="">
                                    </a>
                                    <a href="" class="soc-item share twitter">
                                        <img src="/images/html/facebook-share.svg" alt="">
                                    </a>
                                </div>
                            </div>

                        </div>
                        <div class="col right">
                            <label><?php echo WebApp::l("Daudzums")?></label>
                            <div class="numwrap">
                                <input type="number" name="count" id="prod1-count" min="1" value="1">
                                <a href="#" class="num-control minus">
                                    <span>&ndash;</span>
                                </a>
                                <a href="#" class="num-control plus">
                                    <span>+</span>
                                </a>
                            </div>
                            <input type="submit" value="Likt grozā" class="btn orange w-100">
                            <div class="actions">
                                <div class="action-wrap">
                                    <a href="<?php echo Url::get('/wishlist/toggle', ['id' => $product['item_id'], 'lang' => WebApp::$app->getLanguage()])?>" class="action-toggle wish-toggle"><?php echo $in_wishlist ? WebApp::l("Noņemt no vēlmēm") : WebApp::l("Pievienot vēlmēm")?></a>
                                </div>
                                <div class="action-wrap">
                                    <a href="<?php echo Url::get('/compare/add', ['id' => $product['item_id'], 'lang' => WebApp::$app->getLanguage()])?>" class="action-toggle compare-toggle"><?php echo WebApp::l("Salīdzināt")?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type='hidden' name='p' value='<?php echo $product['item_id']?>' />
                    <input type='hidden' name='addTocart' value='1' />
                    <input type='hidden' name='r2d2' value='' />
                </form>
            </div>
            <div class="shortdesc">
                <div class="formatedtext">
                    <p style="text-align: justify"><?php echo $product['small_text_'.$lang]?></p>
                </div>
            </div>
        </div>
    </div>
    <div class="row bottom">
        <hr>
        <div class="col-12 col-md-6">
            <div class="tabbed-content">
                <ul class="nav nav-tabs d-flex" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="description-tab" data-toggle="tab" href="#description" role="tab" aria-selected="true"><?php echo WebApp::l("Apraksts")?></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="specification-tab" data-toggle="tab" href="#specification" role="tab" aria-selected="false"><?php echo WebApp::l("Specifikācija")?></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="certificate-tab" data-toggle="tab" href="#certificate" role="tab" aria-selected="false"><?php echo WebApp::l("Sertifikāti")?></a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="description" role="tabpanel" aria-labelledby="description-tab">
                        <div class="formatedtext">
                            <?php echo $product['description_'.$lang]?>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="specification" role="tabpanel" aria-labelledby="specification-tab">
                        <div class="formatedtext">
                            <?php echo $this->_renderView("_specification", ['product' => $product])?>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="certificate" role="tabpanel" aria-labelledby="certificate-tab">
                        <div class="formatedtext">
                            <?php echo $product['certificates_'.$lang]?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-6">
            <div class="contact-form">
                <form action="<?php echo Url::get('product', ['product' => $product])?>#q" method="POST" id='q'>
                    <?php if($question_flash_message){ ?>
                    <div class='success'><?php echo $question_flash_message?></div>
                    <?php } ?>
                    <div class="header">
                        <h4><?php echo WebApp::l("Jautājumi par šo preci?")?></h4>
                        <a href="<?php echo Url::get("buj")?>" class="more"><?php echo WebApp::l("Skatīt BUJ")?> &raquo;</a>
                    </div>
                    <?php if($question_errors){ ?>
                    <div class="row">
                        <div class="col-12">
                            <ul class='errors'>
                                <li><?php echo implode("</li><li>", $question_errors)?></li>
                            </ul>
                        </div>
                    </div>
                    <?php } ?>
                    <div class="row">
                        <div class="col-12">
                            <input type="text" name="name" id="cf-name" placeholder="<?php echo WebApp::l("Vārds, uzvārds..")?>" value="<?php echo htmlspecialchars(isset($post['name'])? $post['name'] : '')?>" />
                        </div>
                        <div class="col-12">
                            <select name="subject" id="cf-subject" class="styled">
                                <option value="0" data-default="true"><?php echo WebApp::l("Tēma")?></option>
                                <?php foreach($topics as $i=> $topic){ ?>
                                <option value="<?php echo $i ?>" <?php echo (isset($post['subject']) && $post['subject'] == $i ? 'selected="selected"' : '')?>><?php echo $topic?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-12">
                            <input type="email" name="email" id="cf-email" placeholder="<?php echo WebApp::l("E-pasts..")?>" value="<?php echo htmlspecialchars(isset($post['email'])? $post['email'] : '')?>">
                        </div>
                        <div class="col-12">
                            <textarea name="comment" id="cf-comment" placeholder="<?php echo WebApp::l("Jautājums vai komentārs")?>"><?php echo htmlspecialchars(isset($post['comment'])? $post['comment'] : '')?></textarea>
                        </div>
                        <div class="col-12 clearfix">
                            <input type="submit" class="btn blue outline float-right" value="<?php echo WebApp::l("Nosūtīt ziņu")?>">
                        </div>
                    </div>
                    <input type='hidden' name='question' value='1' />
                    <input type='hidden' name='r2d2' value='' />
                </form>
            </div>
        </div>
    </div>
</div>
<?php if($related_products){ echo $this->_renderView("_related", ['related_products' => $related_products]); } ?>

<?php if($js_steps){ ?>
<script>
    var product_prices = <?php echo json_encode($js_steps)?>;
</script>
<?php } ?>