<?php

use Constructor\WebApp;

?>

<div class="product-block">
    <div class="header">
        <h4><?php echo WebApp::l("Saistītās preces")?></h4>
    </div>
    <div class="product-grid d-flex">
        <?php foreach($related_products as $related_product){ ?>
            <?php echo $this->renderComponentView('/LEDAkcijasProductList/_item', ['product' => $related_product])?>
        <?php } ?>
        <div class="product placeholder"></div>
        <div class="product placeholder"></div>
        <div class="product placeholder"></div>
        <div class="product placeholder"></div>
    </div>
</div>