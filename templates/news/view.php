<div id="NewsItem">
    <div class="padding">
        <h1><?php echo $entry['title']?></h1>
        <div class="date"><?php echo date("d.m.Y", strtotime($entry['date'])) ?></div>
        <div class="short">
            <?php echo $entry['short_text']?>
        </div>
        <div class="full">
            <?php echo $entry['short_text']?>
        </div>
    </div>
    <div class="soc">
        <div class='soc_item twitter'>
            <a href="https://twitter.com/share" class="twitter-share-button">Tweet</a>
            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
        </div>
        <div class="soc_item google">
            <!-- Place this tag where you want the +1 button to render. -->
            <div class="g-plusone" data-size="standart" data-annotation="bubble" data-width="120"></div>

            <!-- Place this tag after the last +1 button tag. -->
            <script type="text/javascript">
            (function() {
            var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
            po.src = 'https://apis.google.com/js/platform.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
            })();
            </script>
        </div>
        <div class="soc_item facebook">
            <div class="fb-like" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>
        </div>
        <div class="cb"></div>

    </div>
</div>

