<?php

namespace Constructor;

use \Pager;

$this->title = App::l("Jaunumi");
\title::getInstance()->srvSetTitle($this->title);

?>
<div id="NewsList">
    <?php foreach($news as $entry){ ?>
    <div class="item">
        <h2><a href="<?php echo Url::get('news/view', ['entry' => $entry])?>"><?php echo $entry['title'] ?></a></h2>
            <div class="date"><?php echo date("d.m.y", strtotime($entry['date']))?></div>
            <div class="short"><?php echo $entry['short_text']?></div>
        <div class="more"><a href="<?php echo Url::get('news/view', ['entry' => $entry])?>"><?php echo App::l('Lasīt vairāk...')?></a></div>
    </div>
    <?php } ?>
    <div class="pages">
    <?php Pager::output([
        'count' => $count,
        'page' => $page,
        'page_size' => $page_size
    ]) ?>
    </div>
</div>