<div class='width_wrapper'>
    <div class='first column'>
        <a href="#mobile-menu" id="mobile-menu-trigger"></a>
        <a href='/' class='logo_link'>{%component:EVeikalsLogo:logo%}</a>
        <?php LEDAkcijasLanguageMenu::run()?>
    </div>
    <div class='second column'>
    {%component:shopsmallcart:smallcart:novisualedit "1"%}
    </div>
    <div class='third column'>
        {%component:EVeikalsLogin:Login:novisualedit "1"%}
        <div class='cb'></div>
    </div>
    <div class='cb'></div>
</div>