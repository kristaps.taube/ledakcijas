<?php

namespace Controllers;
use Constructor\App;
use Constructor\Url;

$this->title = App::l('Rēķina apskate');
\title::getInstance()->srvSetTitle($this->title);

$CurrencyMenu = \EVeikalsCurrencyMenu::getInstance();
$CurrencyMenu->init();

$eveikals = \EVeikalsFrontendShop::getInstance();

echo "<div id='OrderViewer_view'>";
echo "<div class='padding'>";

if(!$order){
    echo "<div class='error'>".App::l("Rēķins netika atrasts")."</div>";
    }else{

    $prods = \shoporderprodcollection::getInstance()->getProductsByOrder($order['item_id']);
    $order_data = unserialize($order['order_data']);
    $prods = $order_data['products'];

    $sum = 0;
    ?>
    <? if($_GET['added']){ ?>
    <div class='success'><?=App::l("Produkti pievienoti grozam")?></div>
    <? } ?>

    <h2><?=App::l("Order")?>: <?=App::l("I-NET.").$order['item_id']?></h2>

    <table>
        <tr>
            <th>#</th>
            <th><?=App::l("Product")?></th>
            <th><?=App::l("Count")?></th>
            <th><?=App::l("Price")?></th>
            <th><?=App::l("Sum")?></th>
        </tr>
        <? foreach($prods as $prod){ ?>
        <? $img = ($prod['product']['picture']) ? $prod['product']['picture'] : $eveikals->default_product_image; ?>
        <? $sum += ($prod['price'] * $prod['count']) ?>
        <tr>
            <td><?=(++$i)?>.</td>
            <td>
            <? if(!$prod['giftcard']){ ?>
            <img src='<?=getThumbUrl($img, 55, 50, 6)?>' alt='' />
            <a href='<?php echo Url::get('product', ['product' => $prod['product']])?>'><?=$prod['name']?></a>
            <? }else{ ?>
                <?=$prod['name']?>
            <? } ?>
            </td>
            <td><?=$prod['count']?></td>
            <td class='price'><?=$prod['price']?> <?=$CurrencyMenu->default['label']?></td>
            <td class='sum'><?=number_format($prod['price'] * $prod['count'], 2)?> <?=$CurrencyMenu->default['label']?></td>
        </tr>
        <? } ?>
        <tr>
            <td class='total' colspan='5'>
                <?=App::l("Kopā")?>: <?=number_format($sum, 2)?> <?=$CurrencyMenu->default['label']?>
            </td>
        </tr>
    </table>
    <?
}

echo "<a href='?' class='button'>".App::l("Atpakaļ")."</a> <a class='button' href='?action=add&id=".$order['item_id']."'>".App::l("Pievienot grozam")."</a> ";
echo "</div>";
echo "</div>";