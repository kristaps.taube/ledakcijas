<?php

namespace Controllers;
use Constructor\WebApp;
use Constructor\Url;

$lang = WebApp::$app->getLanguage();

$order_prod_col = \shoporderprodcollection::getInstance();

$this->title = WebApp::l('Rēķinu vēsture');
\title::getInstance()->srvSetTitle($this->title);

$this->includeJSFile('/scripts/order-history.js');

$date_from = date("Y-m-d", strtotime($date_from));
$date_to =  date("Y-m-d", strtotime($date_to));

?>
<div class="row no-gutters">
    <div class="col-12">
        <section class="col order-form-section">
            <div id="history">
                <div class="row heading">
                    <h2><?php echo WebApp::l('Pasūtījumu saraksts')?></h2>
                    <div class="history-filters">
                        <div class="filter-wrap status">
                            <label for="filter-status"><?php echo WebApp::l('Statuss')?>:</label>
                            <select name="status" id="filter-status" class="styled" data-url="<?php echo Url::get('/user/change-status-filter')?>">
                                <option value="0"><?php echo WebApp::l('Jebkāds')?></option>
                                <?php foreach($statuses as $status){?>
                                <option value="<?php echo $status['item_id']?>" <?php echo ($order_status_filter == $status['item_id'] ? 'selected' : '')?>><?php echo $status['status_'.$lang]?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="filter-wrap">
                            <script>
                                translations = {
                                    "selected": <?php echo json_encode(WebApp::l("No:"))?>,
                                    "day": <?php echo json_encode(WebApp::l("Diena"))?>,
                                    "days": <?php echo json_encode(WebApp::l("Dienas"))?>,
                                    "apply": <?php echo json_encode(WebApp::l("Aizvērt"))?>,
                                    "week-1": <?php echo json_encode(WebApp::l("P"))?>,
                                    "week-2": <?php echo json_encode(WebApp::l("O"))?>,
                                    "week-3": <?php echo json_encode(WebApp::l("T"))?>,
                                    "week-4": <?php echo json_encode(WebApp::l("C"))?>,
                                    "week-5": <?php echo json_encode(WebApp::l("P"))?>,
                                    "week-6": <?php echo json_encode(WebApp::l("S"))?>,
                                    "week-7": <?php echo json_encode(WebApp::l("Sv"))?>,
                                    "week-number": <?php echo json_encode(WebApp::l("Ned."))?>,
                                    "month-name": [
                                        <?php echo json_encode(WebApp::l("janvāris"))?>,
                                        <?php echo json_encode(WebApp::l("februāris"))?>,
                                        <?php echo json_encode(WebApp::l("marts"))?>,
                                        <?php echo json_encode(WebApp::l("aprīlis"))?>,
                                        <?php echo json_encode(WebApp::l("maijs"))?>,
                                        <?php echo json_encode(WebApp::l("jūnijs"))?>,
                                        <?php echo json_encode(WebApp::l("jūlijs"))?>,
                                        <?php echo json_encode(WebApp::l("augusts"))?>,
                                        <?php echo json_encode(WebApp::l("septembris"))?>,
                                        <?php echo json_encode(WebApp::l("oktobris"))?>,
                                        <?php echo json_encode(WebApp::l("novembris"))?>,
                                        <?php echo json_encode(WebApp::l("decembris"))?>
                                    ],
                                    "shortcuts": <?php echo json_encode(WebApp::l("Īsceļi"))?>,
                                    "custom-values": <?php echo json_encode(WebApp::l("Pielāgotas vērtības"))?>,
                                    "past": <?php echo json_encode(WebApp::l("Pagātne"))?>,
                                    "following": <?php echo json_encode(WebApp::l("Nāk."))?>,
                                    "previous": <?php echo json_encode(WebApp::l("Iepr."))?>,
                                    "prev-week": <?php echo json_encode(WebApp::l("Nedēļa"))?>,
                                    "prev-month": <?php echo json_encode(WebApp::l("Mēnesis"))?>,
                                    "prev-year": <?php echo json_encode(WebApp::l("Gads"))?>,
                                    "next": <?php echo json_encode(WebApp::l("Nākošais"))?>,
                                    "next-week": <?php echo json_encode(WebApp::l("Nedēļa"))?>,
                                    "next-month": <?php echo json_encode(WebApp::l("Mēnesis"))?>,
                                    "next-year": <?php echo json_encode(WebApp::l("Gads"))?>,
                                    "less-than": <?php echo json_encode(WebApp::l("Intervālam jābūt mazākam par %d dienām"))?>,
                                    "more-than": <?php echo json_encode(WebApp::l("Intervālam jābūt lielākam par %d dienām"))?>,
                                    "default-more": <?php echo json_encode(WebApp::l("Izvēlieties intervālu lielāku par %d dienām"))?>,
                                    "default-single": <?php echo json_encode(WebApp::l("Izvēlieties datumu"))?>,
                                    "default-less": <?php echo json_encode(WebApp::l("Izvēlieties intervālu mazāku par %d dienām"))?>,
                                    "default-range": <?php echo json_encode(WebApp::l("Izvēlieties intervālu no %d līdz %d dienām"))?>,
                                    "default-default": <?php echo json_encode(WebApp::l("Izvēlieties datumu intervālu"))?>,
                                    "time": <?php echo json_encode(WebApp::l("Laiks"))?>,
                                    "hour": <?php echo json_encode(WebApp::l("Stundas"))?>,
                                    "minute": <?php echo json_encode(WebApp::l("Minūtes"))?>,
                                    "to": <?php echo json_encode(WebApp::l("Līdz:"))?>
                                }
                            </script>
                            <label for="filter-status"><?php echo WebApp::l("Periods")?>:</label>
                            <div class="action-wrap date-range-wrap">
                                <a href="#" class="action-toggle date-range-toggle">
                                    <span class="from"><?php echo date('d.m.Y', strtotime($date_from))?></span> -
                                    <span class="to"><?php echo date('d.m.Y', strtotime($date_to))?></span>
                                </a>
                                <div class="action-menu date-range-menu">
                                    <form action="" method="POST" id="date-filter-form">
                                        <div id="calendar"></div>
                                        <input type="hidden" name="date-from" id="filter-date-from" value="<?php echo date('d.m.Y', strtotime($date_from))?>">
                                        <input type="hidden" name="date-to" id="filter-date-to" value="<?php echo date('d.m.Y', strtotime($date_to))?>">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php foreach($orders as $order){ ?>
                <?php
                    $products = $order_prod_col->getOrderProducts($order['item_id']);
                    $status = isset($statuses[$order['status_id']]) ? $statuses[$order['status_id']] : false
                ?>
                <div class="history-item row">
                    <div class="container-fluid">
                        <div class="history-item-head row">
                            <div class='toggle'>
                                <a href="#" class="toggle">
                                    <span class="toggler">&raquo;</span>
                                    <?php echo WebApp::l("Pasūtījuma ID")?>:
                                    <strong><?php echo $order['num']?></strong>
                                </a>
                            </div>
                            <span class="date">
                                <?php echo WebApp::l("Datums")?>:
                                <strong><?php echo date('d.m.Y', strtotime($order['datums']))?></strong>
                            </span>
                            <span class="status">
                                <?php echo WebApp::l("Statuss")?>:
                                <?php if($status){ ?>
                                <strong style="color:<?php echo $status['color']?>"><?php echo $status['status_'.$lang]?></strong>
                                <?php } ?>
                            </span>
                            <span class="sum">
                                <?php echo WebApp::l("Summa")?>:
                                <strong>&euro;
                                    <span class="value"><?php echo number_format($order['summa'], 2)?></span>
                                </strong>
                            </span>
                            <span class="download">
                                <a href="<?php echo Url::get('/user/download-order', ['o' => $order['item_id']])?>" class="download" target="_blank">
                                    <img src="/images/html/pdf.svg" alt="">
                                    <span><?php echo WebApp::l('Lejuplādēt rēķinu')?></span>
                                </a>
                            </span>
                        </div>
                        <div class="history-item-content row">
                            <div class="container-fluid">
                                <div class="row header">
                                    <div class="col img"></div>
                                    <div class="col name"><?php echo WebApp::l('Prece')?></div>
                                    <div class="col count"><?php echo WebApp::l('Daudzums')?></div>
                                    <div class="col price"><?php echo WebApp::l('Cena gabalā')?></div>
                                    <div class="col sum"><?php echo WebApp::l('Summa')?></div>
                                    <div class="col buy-more"></div>
                                </div>
                                <?php foreach($products as $product){ ?>
                                <?php
                                $picture = $product['picture'] ? $product['picture'] : "/images/html/blank_product.png";
                                ?>
                                <div class="row prod">
                                    <div class="col img">
                                        <a href="<?php echo Url::get('product', ['product' => $product])?>">
                                            <img src="<?php echo getThumbUrl($picture, 70, 65, 7)?>" alt="">
                                        </a>
                                    </div>
                                    <div class="col name">
                                        <a href="<?php echo Url::get('product', ['product' => $product])?>">
                                            <span class="name"><?php echo $product['name_'.$lang]?></span>
                                            <span class="code"><?php echo WebApp::l("Preces kods")?>: <?php echo $product['code']?></span>
                                        </a>
                                    </div>
                                    <div class="col count">
                                        <label class="d-block d-lg-none"><?php echo WebApp::l('Daudzums')?></label>
                                        <div class="numwrap">
                                            <span class="value"><?php echo number_format($product['count'])?></span>
                                        </div>
                                    </div>
                                    <div class="col price">
                                        <label class="d-block d-lg-none"><?php echo WebApp::l('Cena gabalā')?></label>
                                        <div class="price-wrap">
                                            <?php if($product['base_price'] && $product['price'] != $product['base_price']){ ?>
                                            <span class="new-price">
                                                &euro;
                                                <span class="value">
                                                    <?php echo number_format($product['price'], 2)?>
                                                </span>
                                            </span>
                                            <?php } ?>
                                            <span class="price">
                                                &euro;
                                                <span class="value">
                                                    <?php echo number_format($product['base_price'], 2)?>
                                                </span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col sum">
                                        <label class="d-block d-lg-none"><?php echo WebApp::l('Summa')?></label>
                                        <div class="price-wrap">
                                            <?php if($product['base_price'] && $product['price'] != $product['base_price']){ ?>
                                            <span class="new-price">
                                                &euro;
                                                <span class="value">
                                                    <?php echo number_format($product['price'] * $product['count'], 2)?>
                                                </span>
                                            </span>
                                            <?php } ?>
                                            <span class="price">
                                                &euro;
                                                <span class="value">
                                                    <?php echo number_format($product['base_price'] * $product['count'], 2)?>
                                                </span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col buy-more">
                                        <a href="<?php echo Url::get('/user/add-to-cart', ['id' => $product['order_prod_id']])?>" class="buy-more">
                                            <img src="./images/html/grozs-more.svg" alt="">
                                            <span><?php echo WebApp::l('Pirkt vēl')?></span>
                                        </a>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
        </section>
    </div>
</div>
