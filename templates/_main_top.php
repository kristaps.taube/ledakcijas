<div class="container">
    <div class="row no-gutters">
        <?php echo LEDAkcijasCatalogMenu::run(['inner' => true]) ?>
        <div class="col">
            <?php echo LEDAkcijasBreadcrumbs::run(['items' => isset($breadcrumbs) ? $breadcrumbs : []]) ?>
        </div>
    </div>
</div>