<?php

use Constructor\WebApp;

$lang = WebApp::$app->getLanguage();

?>
<div class="row">
    <div class="col text-content">
        <h2><?php echo WebApp::l('Biežāk uzdotie jautājumi')?></h2>

        <div class="faq">
            <?php foreach($categories as $i => $category){ ?>
            <div id="category-<?php echo $category['item_id']?>" class="cat">
                <h3><?php echo ($i+1)?>. <?php echo $category['category_title_'.$lang]?></h3>
                <?php if(isset($questions[$category['item_id']])){ ?>
                    <?php foreach($questions[$category['item_id']] as $question){ ?>
                    <div class="row no-gutters question-wrap">
                        <a class="col-12 question" data-toggle="collapse" href="#faq-<?php echo $question['item_id'] ?>" role="button" aria-expanded="false" aria-controls="faq-<?php echo $question['item_id'] ?>">
                            <span><?php echo $question['question']?></span>
                            <span class="toggle"></span>
                        </a>
                        <div class="col-12 collapse answer" id="faq-<?php echo $question['item_id'] ?>">
                            <?php echo $question['answere']?>
                        </div>
                    </div>
                    <?php } ?>
                <?php } ?>
            </div>
            <?php } ?>
        </div>
    </div>
    <aside class="col-12 col-md-4 align-self-start sticky-top">
        <div class="side-links">
            <h5><?php echo WebApp::l("Tēmas")?></h5>
            <div class="nav">
                <?php foreach($categories as $category){ ?>
                <div class="nav-item">
                    <a href="#category-<?php echo $category['item_id']?>" class="nav-link"><?php echo $category['category_title_'.$lang]?></a>
                </div>
                <?php } ?>
            </div>
        </div>
        <div class="contact-form">
            <form action="" method="POST" id='q'>
                <div class="header">
                    <h4><?php echo WebApp::l("Tev ir savs jautājums?")?></h4>
                </div>
                <?php if($errors){ ?>
                <ul class='errors'>
                    <li><?php echo implode('</li><li>', $errors)?></li>
                </ul>
                <?php } ?>
                <?php if($buj_success){?>
                    <?php echo $buj_success?>
                <?php } ?>
                <div class="row">
                    <div class="col-12">
                        <input type="text" name="name" id="cf-name" placeholder="<?php echo WebApp::l("Vārds, uzvārds..")?>" value='<?php echo isset($post['name']) ? htmlspecialchars($post['name']) : '' ?>'>
                    </div>
                    <div class="col-12">
                        <input type="email" name="email" id="cf-email" placeholder="<?php echo WebApp::l("E-pasts..")?>" value='<?php echo isset($post['email']) ? htmlspecialchars($post['email']) : '' ?>'>
                    </div>
                    <div class="col-12">
                        <textarea name="comment" id="cf-comment" placeholder="<?php echo WebApp::l("Jautājums vai komentārs")?>"><?php echo isset($post['comment']) ? htmlspecialchars($post['comment']) : '' ?></textarea>
                    </div>
                    <div class="col-12 clearfix">
                        <input type="submit" class="btn blue outline float-right" value="<?php echo WebApp::l("Nosūtīt ziņu")?>" name='ask'>
                    </div>
                </div>
                <input type="hidden" name="r2d2" value="" />
            </form>
        </div>
    </aside>
</div>