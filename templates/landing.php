<?php

use Constructor\WebApp;
use Constructor\Template;

?>
<!doctype html>
<html lang="lv">
<head>
    {%template:_head%}
</head>

<body>
    <?php Template::getInstance()->startBody() ?>
    <div id="mobile-menu">
        <?php echo LEDAkcijasMobileMenu::run()?>
    </div>
    <div id="main-wrapper">
        <header id="header">
            {%template:_header%}
        </header>

        <section id="bn">
            <div class="container">
                <div class="row no-gutters">
                    <div class="col-12 col-md-4 col-lg-3 left catalog-menu">
                        <?php echo LEDAkcijasCatalogMenu::run()?>
                    </div>
                    <div class="col">
                        <div id="main-bns" class="carousel slide" data-ride="carousel">
                            <?php echo LEDAkcijasSlideshow::run() ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="main">
            <div class="container">
                <?php echo LEDAkcijasProductList::run(['type' => 'new'])?>
                <?php echo LEDAkcijasProductList::run(['type' => 'bought'])?>
                <?php echo LEDAkcijasFeaturedCategories::run() ?>
            </div>
        </section>

        <?php echo LEDAkcijasPros::run()?>
        <div id="to-top-wrap">
            <div class="container">
                <div class="row justify-content-end">
                    <div>
                        <a href="#" id="to-top" class="visible"></a>
                    </div>
                </div>
            </div>
        </div>
        <footer>
            {%template:_footer%}
        </footer>
    </div>

    <script src="/scripts/dependencies.min.js"></script>
    <script src="/scripts/scripts.min.js"></script>
    <script src="/scripts/custom.js"></script>
    <?php Template::getInstance()->endBody() ?>

</body>
</html>