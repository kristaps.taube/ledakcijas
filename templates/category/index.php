<?php

use Constructor\WebApp;
use Constructor\Url;

$lang = WebApp::$app->getLanguage();

?>
<div class="header">
    <h2><?php echo $title?></h2>
    <div class="pager-wrap">
        <div class="row no-gutters justify-content-between">
            <div class="page-size d-flex align-items-center mr-auto">
                <label for="page-size"><?php echo WebApp::l("Rādīt lapā")?>:</label>
                <select name="page-size" id="page-size" class="styled" data-url="<?php echo Url::get("/category/change-page-size")?>">
                    <?php foreach($page_sizes as $ps){ ?>
                    <option value="<?php echo $ps?>" <?php echo $page_size == $ps ? 'selected = "selected"' : '' ?>><?php echo $ps; ?></option>
                    <?php } ?>
                </select>
                <span>
                    <?php echo WebApp::l('Produkti')?>
                <b><?php echo number_format(($page - 1) * $page_size + 1)?>-<?php echo number_format(($page * $page_size > $product_count ? $product_count : $page * $page_size))?></b> <?php echo WebApp::l('no')?> <b><?php echo number_format($product_count)?></b>
                </span>
            </div>
            <div class="sort-wrap d-flex align-items-center">
                <label for="sort-by"><?php echo WebApp::l("Kārtot")?>:</label>
                <select name="sort-by" id="sort-by" class="styled" data-url="<?php echo Url::get("/category/change-order")?>">
                    <?php foreach($orders as $key => $value){ ?>
                    <option value="<?php echo $key; ?>" <?php echo $order == $key ? 'selected = "selected"' : '' ?>><?php echo $value?></option>
                    <?php }?>
                </select>
            </div>
            <?php if(isset($has_filters) && $has_filters){ ?>
            <button class="filter-toggler d-flex d-md-none" type="button" aria-label="Toggle filters">
                <div class="spans">
                    <span class="navbar-toggler-icon"></span>
                    <span class="navbar-toggler-icon"></span>
                    <span class="navbar-toggler-icon"></span>
                </div>
                <span><?php echo WebApp::l("Filtrēt")?></span>
            </button>
            <?php } ?>
        </div>
    </div>
</div>
<div class="product-grid d-flex">
    <?php foreach($products as $product){ ?>
        <?php echo $this->renderComponentView('/LEDAkcijasProductList/_item', ['product' => $product])?>
    <?php } ?>
    <div class="product placeholder"></div>
    <div class="product placeholder"></div>
    <div class="product placeholder"></div>
    <div class="product placeholder"></div>
</div>
<?php if(isset($link) && $link && $link['text_'.$lang] && $page == 1){ ?>
<div class='category_text'>
    <?php echo $link['text_'.$lang]; ?>
</div>
<?php } ?>
<div class="pager-wrap">
    <div class="row no-gutters justify-content-between">
        <div class="page-size d-flex align-items-center">
            <label for="page-size-bottom"><?php echo WebApp::l("Rādīt lapā")?>:</label>
            <select name="page-size" id="page-size-bottom" class="styled" data-url="<?php echo Url::get("/category/change-page-size")?>">
                    <?php foreach($page_sizes as $ps){ ?>
                    <option value="<?php echo $ps?>" <?php echo $page_size == $ps ? 'selected = "selected"' : '' ?>><?php echo $ps; ?></option>
                    <?php } ?>
            </select>
        </div>
        <div class="pager">
            <?php echo Pager::output(['count' => $product_count, 'page' => $page, 'page_size' => $page_size])?>
        </div>
        <div class="d-flex align-items-center">
            <span>
                <?php echo WebApp::l('Produkti')?>
                <b><?php echo number_format(($page - 1) * $page_size + 1)?>-<?php echo number_format(($page * $page_size > $product_count ? $product_count : $page * $page_size))?></b> <?php echo WebApp::l('no')?> <b><?php echo number_format($product_count)?></b>
            </span>
        </div>
    </div>
</div>
