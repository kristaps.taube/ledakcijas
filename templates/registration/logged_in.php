<?php

use Constructor\WebApp;

?>
<div class='padding'>
    <?php echo WebApp::l("Jūs esat pieslēdzies kā [name]", ['replace' => ['[name]' => $user['name']." ".$user['surname']]]); ?>
</div>