<?php

use Constructor\WebApp;
use Constructor\Template;

?>
<!doctype html>
<html lang="lv">
<head>
{%template:_head%}
</head>
<body>
    <?php Template::getInstance()->startBody() ?>
    <div id="mobile-menu">
        <?php echo LEDAkcijasMobileMenu::run()?>
    </div>
    <div id="main-wrapper">
        <header id="header">
            {%template:_header%}
        </header>

        <div id="main-top">
            {%template:_main_top%}
        </div>

        <section id="main">
            <div class="container">
                <div class='row product-block'>
                    <aside class="col-12 col-md-4 col-lg-3">
                        <?php echo LEDAkcijasFilter::run([
                            'category' => $category,
                            'filter_link' => $filter_link
                        ]) ?>
                    </aside>
                    <div class="col product-block">
                        <?php echo $content ?>
                    </div>
                </div>
            </div>
        </section>
        <div id="to-top-wrap">
            <div class="container">
                <div class="row justify-content-end">
                    <div>
                        <a href="#" id="to-top" class="visible"></a>
                    </div>
                </div>
            </div>
        </div>
        <footer>
            {%template:_footer%}
        </footer>
    </div>
    <script src="/scripts/dependencies.min.js"></script>
    <script src="/scripts/scripts.min.js"></script>
    <script src="/scripts/custom.js"></script>

</body>
</html>