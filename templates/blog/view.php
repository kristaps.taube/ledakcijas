<?php

use Constructor\WebApp;
use Constructor\Url;

$lang = WebApp::$app->getLanguage();

?>
<div class="row">
    <div class="col text-content">
        <h2><?php echo $article['title']?></h2>
        <article>
            <span class="date">
                <?php echo date("d.m.Y", strtotime($article['date']))?>
            </span>
            <div class="content">
                <?php if($article['image']){ ?>
                <img src="<?php echo getThumbUrl($article['image'], 770, 360, 6)?>" alt="" />
                <?php } ?>
                <?php echo $article['text']?>
            </div>
            <a href="<?php echo Url::get('blog') ?>" class="back">&laquo; <?php echo WebApp::l("Uz visiem rakstiem")?></a>
            <div class="soc-share float-right">
                <div class="soc-items">
                    <a href="" class="soc-item share facebook">
                        <img src="./images/html/twitter-share.svg" alt="">
                    </a>
                    <a href="" class="soc-item share twitter">
                        <img src="./images/html/facebook-share.svg" alt="">
                    </a>
                </div>
            </div>
        </article>
    </div>
    <?php echo $this->_renderView('_categories', ['categories' => $categories])?>
</div>
<?php if($products){ ?>
<hr>
<div class="product-block">
    <div class="header">
        <h4><?php echo WebApp::l('Saistītās preces')?></h4>
    </div>
    <div class="product-grid d-flex">
    <?php foreach($products as $product){ ?>
        <?php echo $this->renderComponentView('/LEDAkcijasProductList/_item', ['product' => $product])?>
    <?php } ?>
    </div>
</div>
<?php } ?>