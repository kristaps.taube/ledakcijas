<?php

use Constructor\WebApp;
use Constructor\Url;

$lang = WebApp::$app->getLanguage();

?>
<aside class="col-12 col-md-4 align-self-start sticky-top">
    <div class="side-links">
        <h5><?php echo WebApp::l("Rakstu kategorijas")?></h5>
        <div class="nav">
            <?php foreach($categories as $category){ ?>
            <div class="nav-item">
                <a href="<?php echo Url::get('blog_category', ['category' => $category])?>" class="nav-link"><?php echo $category['category_title_'.$lang]?></a>
            </div>
            <?php } ?>
        </div>
    </div>
</aside>