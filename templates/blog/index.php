<?php

use Constructor\WebApp;
use Constructor\Url;

$lang = WebApp::$app->getLanguage();


?>
<div class="row">
    <div class="col text-content">
        <h2><?php echo WebApp::l('Blogs')?></h2>

        <div class="article-list">
            <?php foreach($articles as $article){ ?>
            <article>
                <a href="<?php echo Url::get('blog_article', ['article' => $article])?>">
                    <?php if($article['image']){ ?>
                    <img src="<?php echo getThumbUrl($article['image'], 770, 190, 6)?>" alt="" />
                    <?php } ?>
                    <h4><?php echo $article['title']?></h4>
                    <div class="date"><?php echo date('d.m.Y', strtotime($article['date']))?></div>
                </a>
            </article>
            <?php } ?>
            <?php if($article_count){ ?>
            <div class="pager-wrap">
                <div class="row no-gutters justify-content-between">
                    <div class="pager">
                        <?php echo Pager::output(['count' => $article_count, 'page' => $page, 'page_size' => $page_size])?>
                    </div>
                    <div class="d-flex align-items-center">
                        <span>
                            <?php echo WebApp::l('Raksti')?>
                            <b><?php echo number_format(($page - 1) * $page_size + 1)?>-<?php echo number_format(($page * $page_size > $article_count ? $article_count : $page * $page_size))?></b> <?php echo WebApp::l('no')?> <b><?php echo number_format($article_count)?></b>
                        </span>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
    <?php echo $this->_renderView('_categories', ['categories' => $categories])?>
</div>