<?php

use Constructor\WebApp;
use Constructor\Url;
use LEDAkcijas\ProductAmountPriceCollection;

$default_image = "/images/html/blank_product.png";
$lang = WebApp::$app->getLanguage();
$manager = \shopmanager::getInstance();

$this->includeJSFile('/scripts/checkout.js');

$js_prices = [];
$sum = 0;

?>
<div class="row no-gutters">
    <div class="col-12">
        <h2><?php echo WebApp::l("Pasūtījuma grozs")?></h2>
        <?php if($items){ ?>
        <section class="col order-form-section">
            <div id="cart" data-change-count-url="<?php echo Url::get("/cart/change-count")?>">
                <div class="row">
                    <h3><?php echo WebApp::l("1. Pasūtāmās preces")?></h3>
                </div>
                <div class="row header">
                    <div class="col img"></div>
                    <div class="col name"><?php echo WebApp::l("Prece")?></div>
                    <div class="col count"><?php echo WebApp::l('Daudzums')?></div>
                    <div class="col price"><?php echo WebApp::l('Cena gabalā')?></div>
                    <div class="col sum"><?php echo WebApp::l('Summa')?></div>
                    <div class="col delete"></div>
                </div>
                <?php foreach($items as $key => $item){ ?>
                <?php
                    $product = $item['product'];
                    $prices = $manager->getProductPrice($product, WebApp::$app->user->get(), $item['count']);
                    $price = $prices['price'];
                    $base_price = $prices['base_price'];
                    $featured = $manager->isProductFeatured($product);
                    $picture = $product['picture'] ? $product['picture'] : $default_image;

                    if(!$featured){ // if its featured, no need to get step data

                        $product_steps = ProductAmountPriceCollection::getInstance()->getByProduct($product['item_id']); // will this be a bottle neck?

                        $js_steps = [];
                        foreach($product_steps as $key => $value){
                            if(!($value['price'] > 0)){
                                unset($product_steps[$key]);
                                continue;
                            }
                            $js_steps[] = ['from' => $value['count_from'], 'to' => $value['count_to'], 'price' => $value['price']];
                        }
                        $js_prices[$item['key']] = $js_steps;

                    }

                    $sum += ($price * $item['count']);
                    ?>
                <div class="row prod" data-key="<?php echo $item['key']?>" data-featured="<?php echo $featured ? 1 : 0?>">
                    <div class="col img">
                        <a href="<?php echo Url::get("product", ['product' => $product])?>">
                            <img src="<?php echo getThumbUrl($picture, 70, 65, 6)?>" alt="">
                        </a>
                    </div>
                    <div class="col name">
                        <a href="<?php echo Url::get("product", ['product' => $product])?>">
                            <span class="name"><?php echo $product['name_'.$lang]?></span>
                            <span class="code"><?php echo WebApp::l('Preces kods')?>: <?php echo $product['code']?></span>
                        </a>
                    </div>
                    <div class="col count">
                        <div class="numwrap">
                            <input type="number" name="count" min="1" value="<?php echo $item['count']?>">
                            <a href="#" class="num-control minus">
                                <span>&ndash;</span>
                            </a>
                            <a href="#" class="num-control plus">
                                <span>+</span>
                            </a>
                        </div>
                    </div>
                    <div class="col price">
                        <label class="d-block d-lg-none"><?php echo WebApp::l("Cena gabalā")?></label>
                        <div class="price-wrap">
                            <?php if($price != $base_price){ ?>
                            <span class="new-price">
                                &euro;
                                <span class="value" data-default="<?php echo $price ?>">
                                    <?php echo number_format($price, 2)?>
                                </span>
                            </span>
                            <?php } ?>
                            <span class="price">
                                &euro;
                                <span class="value" data-default="<?php echo $base_price ?>">
                                    <?php echo number_format($base_price, 2)?>
                                </span>
                            </span>
                        </div>
                    </div>
                    <div class="col sum">
                        <label class="d-block d-lg-none"><?php echo WebApp::l("Summa")?></label>
                        <div class="price-wrap">
                            <?php if($price != $base_price){ ?>
                            <span class="new-price">
                                &euro;
                                <span class="value">
                                    <?php echo number_format($price * $item['count'], 2)?>
                                </span>
                            </span>
                            <?php } ?>
                            <span class="price">
                                &euro;
                                <span class="value">
                                    <?php echo number_format($base_price * $item['count'], 2)?>
                                </span>
                            </span>
                        </div>
                    </div>
                    <div class="col delete">
                        <a href="<?php echo Url::get("/cart/remove", ['key' => $item['key']])?>" class="delete"></a>
                    </div>
                </div>
                <?php } ?>
                <div class="row summary ">
                    <div class="col justify-content-between">
                        <div class="code-wrap">
                            <?php /* <form action="<?php echo Url::get("/cart/add-discount-code")?>" method="POST" class="d-flex align-items-center">
                                <label for="discount-code"><?php echo WebApp::l("Atlaižu kods")?></label>
                                <div class="input-wrap">
                                    <input type="text" name="discount-code" id="discount-code">
                                    <button type="submit"><?php echo WebApp::l("Pievienot")?></button>
                                </div>
                            </form> */ ?>
                        </div>

                        <div class="sum">
                            <span class="label"><?php echo WebApp::l("Preču kopsumma")?>:</span>
                            <div class="price-wrap">
                                <span class="price">
                                    &euro;
                                    <span class="value"><?php echo number_format($sum, 2)?></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php } ?>
        <section class="order-form-section">
            <?php echo $orderform->output() ?>
        </section>
    </div>
</div>
<script>
	var js_prices = <?php echo json_encode($js_prices);?>
</script>