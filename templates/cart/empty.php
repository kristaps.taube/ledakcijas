<?php

use Constructor\WebApp;
use Constructor\Url;

?>
<div class="col-12">
    <h2><?php echo WebApp::l("Pirkumu grozs")?></h2>
    <section class="col order-form-section">
        <div id="empty-cart">
            <div class="image">
                <img src="/images/html/cart-gray.svg" alt="">
            </div>
            <h4><?php echo WebApp::l("Grozs ir tukšs!")?></h4>
            <a href="<?php echo Url::get("catalog")?>" class="btn blue outline"><?php echo WebApp::l("Ejam iepirkties")?></a>
        </div>
    </section>
</div>