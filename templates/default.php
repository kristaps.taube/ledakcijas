<?php

use Constructor\WebApp;
use Constructor\Template;

?>
<!doctype html>
<html lang="lv">
<head>
    {%template:_head%}
</head>
<body>

    <?php Template::getInstance()->startBody() ?>
    <div id="mobile-menu">
        <?php echo LEDAkcijasMobileMenu::run()?>
    </div>
    <div id="main-wrapper">
        <header id="header">
            {%template:_header%}
        </header>

        <div id="main-top">
            {%template:_main_top%}
        </div>

        <section id="main">
            <div class="container">
                <?php echo $content ?>
                <?php echo formatedtext0::run(['page_id' => isset($page_id) ? $page_id : false, 'name' => 'content'])?>
            </div>
        </section>
        <div id="to-top-wrap">
            <div class="container">
                <div class="row justify-content-end">
                    <div>
                        <a href="#" id="to-top" class="visible"></a>
                    </div>
                </div>
            </div>
        </div>
        <footer>
            {%template:_footer%}
        </footer>
    </div>

    <script src="/scripts/dependencies.min.js"></script>
    <script src="/scripts/scripts.min.js"></script>
    <script src="/scripts/custom.js"></script>

    <?php Template::getInstance()->endBody() ?>

</body>
</html>