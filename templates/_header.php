<?php

use Constructor\WebApp;

?>
<div class="container">
    <div class="row">
        <div class="col left">
            <button class="navbar-toggler d-block d-lg-none" type="button" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
                <span class="navbar-toggler-icon"></span>
                <span class="navbar-toggler-icon"></span>
            </button>
            <a class="navbar-brand" href="/">
                <img src="/images/html/logo.png" alt="">
            </a>
        </div>

        <div class="col">
            <div class="row">
                <div class="col middle">
                    <div class="row">
                        <?php echo LEDAkcijasSearch::run()?>
                    </div>
                </div>
                <div class="col right d-flex flex-column justify-content-between">
                    <div class="row">
                        <button class="navbar-toggler d-block d-lg-none" type="button" data-toggle="collapse" data-target="#mainMenu" aria-controls="mainMenu" aria-expanded="false"
                            aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                            <span class="navbar-toggler-icon"></span>
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="contacts">
                            <a href="tel:<?php echo WebApp::$app->l('+371 200 22 881')?>"><?php echo WebApp::$app->l('+371 200 22 881')?></a>
                            <a href="mailto:<?php echo WebApp::$app->l('info@ledakcijas.lv')?>"><?php echo WebApp::$app->l('info@ledakcijas.lv')?></a>
                        </div>
                        <?php echo LEDAkcijasLanguageMenu::run()?>
                    </div>
                </div>
            </div>
            <div class="row align-items-end">
                <nav class="navbar navbar-expand-lg">

                    <div class="row no-padding">
                        <div class="col-12">
                            <div class="collapse navbar-collapse" id="mainMenu">
                                <?php echo LEDAkcijasTopMenu::run()?>
                            </div>
                        </div>
                    </div>
                </nav>
                <div class="col">
                    <div class="row align-items-end flex-nowrap justify-content-end">
                        <div class="action-wrap login-wrap">
                            <?php echo LEDAkcijasLogin::run() ?>
                        </div>
                        <div class="action-wrap wish-wrap">
                            <?php echo LEDAkcijasWishlistLink::run()?>
                        </div>
                        <div class="action-wrap cart-wrap">
                            <?php echo LEDAkcijasSmallCart::run()?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>