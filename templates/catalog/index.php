<?php

use Constructor\WebApp;
use Constructor\Url;

$lang = WebApp::$app->getLanguage();
$links_col = \LedAkcijasLinkCollection::getInstance();
$catcol = \shopcatcollection::getInstance();

?>

<h2><?php echo WebApp::l('Visas kategorijas')?></h2>
<div id="all-cats" class="card-columns catalog-menu">
    <?php foreach($categories as $category){ ?>
    <div class="card submenu">
        <div class="menu">
            <div class="nav-item heading">
                <a href="<?php echo Url::get('category', ['category' => $category])?>" class="nav-link"><?php echo $category['title_'.$lang]?></a>
            </div>
            <?php if($links = $links_col->getForAllCategories($category['item_id'])){ ?>

                <?php foreach($links as $link){ ?>
                <?php
                    $link_cat = $link['category_id'] ? $catcol->getFromCache($link['category_id']) : $category;
                ?>
                <div class="nav-item">
                    <a href="<?php echo Url::get('filter_link', ['category' => $link_cat, 'link' => $link])?>" class="nav-link"><?php echo $link['name_'.$lang]?></a>
                </div>
                <?php } ?>

            <?php } ?>

        </div>
    </div>
    <?php } ?>
</div>