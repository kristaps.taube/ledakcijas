<?php

use Constructor\Url;

?>

<?php foreach($products as $prod){ ?>
<?php
    $image = $prod['picture'] ? $prod['picture'] : "/images/html/blank_product.png";
?>
 <a href="<?php echo Url::get('product', ['product' => $prod, 'lang' => $lang])?>" class="suggestion">
    <img src="<?php echo getThumbUrl($image, 40, 40, 7)?>" alt="">
    <div class="name">
        <?php echo $prod['name_'.$lang]?>
    </div>
</a>
<?php } ?>