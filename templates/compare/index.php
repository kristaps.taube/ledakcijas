<?php

use Constructor\WebApp;
use Constructor\Url;

$lang = WebApp::$app->getLanguage();

?>
<h2><?php echo WebApp::l("Preču salīdzināšana")?></h2>
<?php if($error){ ?>
<div class='error'><?php echo $error?></div>
<?php } ?>
<div class="product-block compare">
    <div class="product-grid d-flex justify-content-center">
        <?php foreach($products as $product){ ?>
            <?php
                $tags = ['<div class="tag delete"><a href="'.Url::get('/compare/remove', ['id' => $product['item_id']]).'" class="delete"></a></div>'];
                echo $this->renderComponentView('/LEDAkcijasProductList/_item', ['product' => $product, 'tags' => $tags]);
            ?>
        <?php } ?>
    </div>
    <div class="heading text-center">
        <h4><?php echo WebApp::l("Parametru salīdzināšana")?></h4>
    </div>
    <div class="row no-gutters spec-row">
        <?php foreach($products as $product){ ?>
        <div class="col spec-col">
        <?php
            $data = isset($compare_data[$product['item_id']]) && $compare_data[$product['item_id']] ? $compare_data[$product['item_id']] : [];
            foreach($data as $entry){
                echo '<p>'.$entry['title_'.$lang].': '.$entry['param_title_'.$lang].'</p>';
            }
        ?>
        </div>
        <?php } ?>

    </div>

</div>