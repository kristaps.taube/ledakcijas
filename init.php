<?php

if($inited !== true){

  if (isset($_GET['session_id'])) session_id($_GET['session_id']);
  session_start();

  if (substr($cfgDirRoot, strlen($cfgDirRoot)-1)!='/') $cfgDirRoot = $cfgDirRoot.'/';

  $GLOBALS['cfgWebRoot'] = $cfgWebRoot;
  $GLOBALS['cfgDirRoot'] = $cfgDirRoot;

  require($cfgDirRoot."library/class.sessionmanager.php");
  require($cfgDirRoot."library/class.db.php");
  require($cfgDirRoot."library/models/class.langpages.php");
  require($cfgDirRoot."library/models/class.scriptsmanager.php");
  require($cfgDirRoot."library/"."func.components.php");
  require($cfgDirRoot."library/"."func.database.php");
  require($cfgDirRoot."library/"."func.misc.php");
  require($cfgDirRoot."library/"."func.interface.php");
  require($cfgDirRoot."library/"."func.login.php");
  require($cfgDirRoot."library/"."func.permissions.php");
  require($cfgDirRoot."library/"."func.log.php");
  require($cfgDirRoot."library/"."func.search.php");
  require($cfgDirRoot."library/"."func.files.php");
  require($cfgDirRoot."library/"."func.options.php");
  require($cfgDirRoot."library/"."func.xmlrpc.php");
  require($cfgDirRoot."components/class.cmsconfig.php");

  ini_set('memory_limit', MEMORY_LIMIT);
	if(defined("DEFAULT_TIMEZONE") && DEFAULT_TIMEZONE){
  	date_default_timezone_set (DEFAULT_TIMEZONE);
	}

  error_reporting(ERROR_REPORTING_LEVEL);
  ini_set('error_log', ERROR_LOG);

	if (DISPLAY_ERRORS) {
	  ini_set('display_errors', 1);
	}else{
		ini_set('display_errors', 0);
	}

	if(LOG_ERRORS){
	  ini_set('log_errors', 1);
	}else{
	  ini_set('log_errors', 0);
	}


	$GLOBALS['cur_url'] = "http://".$GLOBALS['cfgDomain'].$_SERVER["SCRIPT_URL"];

	if($GLOBALS['developmode'] && !$win){
	  // update crontab
	  $conf = file_get_contents(CRONTAB_CONF);
    $conf = "# E-Veikals Crontab conf. start \n".(trim($conf))."\n# E-Veikals Crontab conf. end";

		$conf = str_replace("[DirRoot]", $GLOBALS['cfgDirRoot'], $conf);
		$conf = str_replace("[CronFolder]", CRON_FOLDER, $conf);
		$conf = trim($conf);
		$tmp_file = CRON_FOLDER."tmp";

		$crontab = shell_exec('crontab -l');
	  if(!is_null($crontab)){ // update existing crontab
	  	preg_match('/# E-Veikals Crontab conf. start(.+)# E-Veikals Crontab conf. end/s', $crontab, $matches);
			if(count($matches)){
				$conf = str_replace($matches[0], $conf, $crontab);
			}else{
	    	$conf = $crontab."\n\n".$conf;
			}

	  }

		file_put_contents($tmp_file, $conf);
		shell_exec('crontab '.$tmp_file);
		unlink($tmp_file); // delete tmp file
	}

	// strip slahses if magic quotes is on
  if (get_magic_quotes_gpc()) {
    array_walk_recursive($_GET, 'stripslashes_array');
    array_walk_recursive($_POST, 'stripslashes_array');
    array_walk_recursive($_COOKIE, 'stripslashes_array');
    array_walk_recursive($_REQUEST, 'stripslashes_array');
	}

  // process templates
  if($GLOBALS['developmode'] && defined("TEMPLATE_FOLDER")){

  	$items = scandir(TEMPLATE_FOLDER);

  	$sites = DB::GetTable("SELECT * FROM sites");
		foreach($sites as $site){

			$templates = DB::GetTable("SELECT * FROM `".$site['site_id']."_templates`");
			$max_ind = DB::GetValue("SELECT max(ind) FROM `".$site['site_id']."_templates`");
			$tmp = array();
			foreach($templates as $t){
				$tmp[$t['name']] = $t;
			}
      $templates = $tmp;

      foreach($items as $item){

      	if(getFileExtension($item) == 'php'){

					$path_to_file = TEMPLATE_FOLDER.$item;

        	$parts = explode(".", $item);
          $tpl_name = $parts[0];

          if(isset($templates[$tpl_name])){ // we have this tpl, check upd time

          	$tpl_row = $templates[$tpl_name];
						$file_mod_time = filemtime($path_to_file);

						if(!$tpl_row['updated'] || strtotime($tpl_row['updated']) < $file_mod_time){

							DB::Update($site['site_id']."_templates", array("template_id" => $tpl_row['template_id']), array(
              	"body" => file_get_contents($path_to_file),
								"updated" => date("Y-m-d H:i:s", $file_mod_time),
							));

						}

          }else{ // we dont have this tpl, lets add

          	$template_id = DB::Insert($site['site_id']."_templates", array(
							"name" => $tpl_name,
							"body" => file_get_contents($path_to_file),
							"updated" => date("Y-m-d H:i:s", $file_mod_time),
							"description" => "Auto created",
							"ind" => ++$max_ind
						));

						//Add access to the newly created template for Everyone
				    AddGroupPermission(1, 12, $site['site_id'], $template_id);
				    AddGroupPermission(1, 15, $site['site_id'], $template_id);
				    AddGroupPermission(1, 15, $site['site_id'], $template_id);
				    AddGroupPermission(1, 15, $site['site_id'], $template_id);
    				Add_Log("Auto created template '".$tpl_name."' (".$site['site_id'].")", $site['site_id']);

          }

      	}

      }

			// creating missing template files
			foreach($templates as $template){

      	$tpl = $template['name'].".php";
      	if(!in_array($tpl, $items)){
        	file_put_contents(TEMPLATE_FOLDER.$tpl, $template['body']);
      	}

			}

		}

  }


  $inited = true;

}