<?php
        
	$start = microtime(true);

	if (!isset($_SERVER['SCRIPT_URL'])){
 		$_SERVER['SCRIPT_URL'] = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
 	}

  $GLOBALS['meta_keywords'] = '';
  $GLOBALS['meta_description'] = '';
  $GLOBALS['additional_header_tags'] = '';
  $GLOBALS['from_backend'] = false;

  if(!isset($inited) || $inited == false){
    require("config.php");
    require_once($cfgDirRoot."/init.php");
    require_once($GLOBALS['cfgDirRoot'] . "backend/const.php");
  }

  if ($GLOBALS['cfgWWWredirect'] && substr($_SERVER['HTTP_HOST'], 0, 4) != 'www.'){
    header("Location: http://www." . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], true, 301);
    die();
  }

  if($GLOBALS['disabled_frontend']){

    $url =(isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"]=="on") ? "https://" : "http://";
    $url .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
    if(!$_SESSION['passprotect_allowed'] && empty($_POST)){

    ?>
      <form method='post' action=''>
       Username: <input name='user' />
       Password: <input name='password' type='password' />
       <input type='submit' value='login' />
      </form>
    <?
      die();
    }else{

      if(!$_SESSION['passprotect_allowed']){
        $user = $_POST['user'];
        $password = $_POST['password'];
        if($GLOBALS['frontend_user'] == $user && $GLOBALS['frontend_password'] == $password){
          $_SESSION['passprotect_allowed'] = true;
        }else{
          $_SESSION['passprotect_allowed'] = false;
          header("location: ".$url);
          die();
        }
      }
    }

  }

  $outputpage = true;
  $ajaxrequest = false;

  $xmlrequest = false;
  $post_xml = null;
  $out_xml = null;
  $content_type = isset($_SERVER['CONTENT_TYPE']) ? $_SERVER['CONTENT_TYPE'] : $_SERVER['HTTP_CONTENT_TYPE'];

  if($_GET['_component'] && $_GET['_method']){
    $outputpage = false;
    $ajaxrequest = true;
    $ajax_component_name = $_GET['_component'];
    $ajax_method = $_GET['_method'];
  }else if ($content_type){
    $a = explode(';', $content_type);
    if (trim($a[0]) == 'text/xml'){
      require_once($GLOBALS['cfgDirRoot']."library/class.xmlnode.php");

      $post_xml = new XMLNode();
      $post_xml->loadFromString($HTTP_RAW_POST_DATA);

      if ($post_xml->name == 'request')
      {
        header('Content-Type: text/xml; charset=utf-8');
        $xmlrequest = true;
        $outputpage = false;
        $out_xml = new XMLNode('response');
      }
    }
  }
  $GLOBALS['outputpage'] = $outputpage;
  $GLOBALS['ajaxrequest'] = $ajaxrequest;
  $GLOBALS['ajax_component_name'] = $ajax_component_name;
  $GLOBALS['ajax_method'] = $ajax_method;
  $GLOBALS['xmlrequest'] = $xmlrequest;
  $GLOBALS['out_xml'] = $out_xml;
  $GLOBALS['post_xml'] = $post_xml;

  $force_template = $_GET['glo_template'];
  //Obtain the site_id and page_id by parsing the uri
  $url = parse_url($_SERVER['REQUEST_URI']);
  if((!$url['host'])or($_SERVER['REQUEST_URI']{0}=='/')){
    $url = 'http://' . strtolower($_SERVER['HTTP_HOST']) . $_SERVER['REQUEST_URI'];
    $url = parse_url($url);
  }

  if ($out_xml)
    $out_xml->attribs['url'] = $url['path'];

  if(($GLOBALS['forced_site_id'])and($GLOBALS['forced_page_id'])){
    $site_id = $GLOBALS['forced_site_id'];
    $page_id = $GLOBALS['forced_page_id'];
  }else{

    //Obtain site_id
    $row = DB::GetRow("SELECT site_id,dirroot,domain FROM sites WHERE domain='" . $url['host'] . "'");

    if(!$row){
      //check if site is in aliases
      $sites = DB::GetTable("SELECT site_id, alias, dirroot, domain FROM sites WHERE LOCATE('" . $url['host'] . "',alias)");
      foreach($sites as $site){
        $aliases = explode(";", $site['alias']);
        if(in_array($url['host'],$aliases) || in_array('*', $aliases)){
          $row = $site;
					break;
        }
      }
    }

    if($row){
			define("DIR_ROOT", $row['dirroot']);
			define("DOMAIN", $row['domain']);
      $site_id = $row['site_id'];			
			//remove some junk data if needed
			$test_domain = option('site_domain', null, 'Site domain', array('is_advanced' => 1));
			$test_domain = str_replace('www.','',$test_domain);
			$site_domain = DOMAIN;
			$site_domain = str_replace('www.','',$site_domain);
			if($test_domain != $site_domain){
				option('site_domain', $site_domain, 'Site domain', array('is_advanced' => 1));
				//delete upload dir contents
				unlinkr(DIR_ROOT . '/upload');
				//delete orders
				unlinkr(DIR_ROOT . '/files/rekini');
				//delete promo
				unlinkr(DIR_ROOT . '/files/promocodes');
				//delete giftcards
				unlinkr(DIR_ROOT . '/files/giftcards');	
				sqlQuery("TRUNCATE TABLE `".$site_id."_stats_main`");
				sqlQuery("TRUNCATE TABLE `".$site_id."_stats_pages`");
			}
			
		}
		
    if(!$site_id){
      Die ("Error 404 - page not found");
    }

    $jslibsfile = getFilePathFromLink($site_id, '/scr/components/jslibs.php');

    if (file_exists($jslibsfile))
      $libs = include($jslibsfile);
    else
      $libs = array();

    $GLOBALS['scripts_manager'] = new ScriptsManager($libs);


    //show history html page
    if (isset($_GET['pagehistory_id']))
    {
        $pagehistory_id = $_GET['pagehistory_id'];

        $row = sqlQueryRow("SELECT * FROM ".$site_id."_pageshistory WHERE pagehistory_id=$pagehistory_id");
        echo $row['html'];
        die;
    }

    //we have site_id
    $tryindex = false;
    if(substr($url['path'],strlen($url['path'])-1,1)=='/')
    {
      $tryindex = true;
    }

    //Obtain page_id
    $url['path'] = trim($url['path'], '/');
    $path = explode("/", $url['path']);
    if(!$path[0]){
      $path = Array();
      $tryindex = true;
    }

    $page_id = '0';
    $last_page_id = null;
    $last_parent = null;
    for($f=0; (($f<count($path))and($page_id!=null)); $f++){
      $last_page_id = $page_id;
      $last_parent = $parent;
      $d = sqlQueryRow("SELECT parent, page_id FROM " . $site_id . "_pages WHERE parent='" . $page_id . "' AND name='" . $path[$f] . "'");
      list($parent, $page_id) = $d;
		}

    if($page_id == null)
    {
      if($path[$f - 1] == 'get')
      {
        for($i = $f; $i < count($path); $i += 2)
        {
          if($path[$i] && !$_GET[$path[$i]]) //params after ? are stronger (/get/a/1/?a=2 would make a=2)
          {
            $_GET[$path[$i]] = $path[$i + 1];
            $GLOBALS[$path[$i]] = $path[$i + 1];
          }
        }
        //remove all GET parameters from path array
        $path = array_slice($path, 0, $f - 1);
        $page_id = $last_page_id;
        $parent = $last_parent;
      }
      else if($path[$f - 1] == 'shop')
      {
        $GLOBALS['_shop_path'] = array_slice($path, $f);
        $page_id = $last_page_id;
        $parent = $last_parent;
      }
    }

    $langs = getLanguages($site_id);
    $needle = $path[count($path)-1];

    /* Looking into catalog categories */
    if($page_id == null){
      $catalog_table = readOption("Shop\\category_table", null, $site_id);

      if($catalog_table){
        $where = Array();
        foreach($langs as $short => $data){
          $where[] = "url_".$short." = '".$needle."'";
        }

        if(!empty($where)){
          $category = sqlQueryRow("SELECT * FROM `".$catalog_table."` WHERE ".implode(" OR ", $where));
        }else{
          $category = false;
        }

        if($category){ // we found category
          foreach($langs as $short => $data){
            if($category['url_'.$short] == $needle){ // we found language
              $catalog_page = readOption("Shop\\catalog_page_id_".$short, null, $site_id);
              if($catalog_page){ // we found page
                $page_id = $catalog_page;
              }
              break;
            }
          }
        }

      }
    }
    /* End of looking into catalog categories */

    /* Looking into catalog products */
    if($page_id == null){

      $products_table = readOption("Shop\\product_table", null, $site_id);
      if($products_table){
        $where = Array();
        foreach($langs as $short => $data){
          $where[] = "url_".$short." = '".$needle."'";
        }

        if(!empty($where)){
          $product = sqlQueryRow("SELECT * FROM `".$products_table."` WHERE ".implode(" OR ", $where));
        }else{
          $product = false;
        }

        if($product){ // we found product
				
          if($product['url_'.$lang] == $needle){
          	$catalog_page = readOption("Shop\\catalog_page_id_".$lang, null, $site_id);
             if($catalog_page){ // we found page
               $page_id = $catalog_page;
             }

        	}else{

	          foreach($langs as $short => $data){
	            if($product['url_'.$short] == $needle){ // we found language
	              $catalog_page = readOption("Shop\\catalog_page_id_".$short, null, $site_id);
	              if($catalog_page){ // we found page
	                $page_id = $catalog_page;
	              }
	              break;
	            }
	          }

					}

        }

      }

    }
    /* End of looking into catalog products */

    if(($tryindex)and($page_id!=null)){

      $result = DB::GetRow("SELECT parent, page_id FROM " . $site_id . "_pages WHERE parent='" . $page_id . "' AND name='index'");
			$iparent = $result['parent'];
			$ipage_id = $result['page_id'];

      if($ipage_id){
        $parent = $iparent;
        $page_id = $ipage_id;
      }
    }

  }

  $GLOBALS['urlpath'] = implode('/', $path);

  function outputComponent(&$component, $type, $name, &$canreplace)	{
      global $xmlrequest, $post_xml;

      $start = microtime(true);

      if ($GLOBALS['ajaxrequest'] && $GLOBALS['ajax_component_name'] != $name) return '';

      $xml = null;

      //$m = microtime_float();
      $canreplace = true;
      if($component !== false)
      {
        $GLOBALS['component_name'] = $name;
        ob_start();
        $GLOBALS['profiler_component'] = $name;

        $obj = &$component;
        if($obj->getProperty('visible') == 0)
        {
          if(!$obj->CanBeCompiled())
            $canreplace = false;
          if((!$_GET['formatpageforsearch'])or(!$obj->getProperty('nosearch')))
          {
            if ($GLOBALS['ajaxrequest'])
            {
              $method = 'ajax_'.$GLOBALS['ajax_method'];
              if (method_exists($obj, $method))
                $obj->$method();

              $html = ob_get_contents();
            }
            else if ($xmlrequest)
            {
              $in_xml = null;
              foreach ($post_xml->children as $ch)
              {
                if ($ch->name == 'component' && $ch->attribs['type'] == $type && $ch->attribs['name'] == $name)
                {
                  $in_xml = $ch;
                  break;
                }
              }
              $xml = null;
              if ($in_xml)
              {
                $xml = new XMLNode('component');
                $xml->attribs['type'] = $type;
                $xml->attribs['name'] = $name;
                $obj->xmlRequest($in_xml, $xml);
              }
            }
            else
            {
              $obj->Output();
              $html = ob_get_contents();
            }
          }
        }else{
          $html = '';
        }
        $GLOBALS['profiler_component'] = '';
        ob_end_clean();

        $end = microtime(true);

        $output_time = $end - $start;

				if(LOG_COMPONENT_OUTPUT_TIME){
	   	    file_put_contents(COMPONENT_OUTPUT_TIME_LOG, $name."(".$type.") ".number_format($output_time,5)."s " . PHP_EOL, FILE_APPEND);
				}

        return $xmlrequest ? $xml : $html;;
      }else{
        return '';
      }
    }

    function DisplayPage($page_id, $site_id, &$encoding, $allowdisabled=false, $forcetemplate='', $authorize=false){

      if (option('frontend_disabled', null, 'Disable webpage', array('fieldparams' => array('type' => 'check') ) )){
        header('Content-Type: text/html; charset=utf-8');
        return 'Page is under construction';
      }

      $url = $url ? $url : $GLOBALS['url'];

        unset($pageData);
        $ppath = array();
        $redirs = array();
        do{
          $ppath[] = $pageData['page_id'];

          if($pageData['redirect']){

		    if ($pageData['redirect'] == -1){
              $pageData['redirect'] = sqlQueryValue('SELECT page_id FROM '.$site_id.'_pages WHERE parent='.$pageData['page_id'].' ORDER BY ind LIMIT 1');
              if (!$pageData['redirect']) $pageData['redirect'] = 0;
            }

            if ($pageData['redirect']){
              $page_id = $pageData['redirect'];
              $GLOBALS['page_id'] = $page_id;
              $redirs[] = $page_id;
            }

          }

          if(!$page_id || $pageData['in_trash'])
          {
            //Check if we have a page for 404
            if($url['path']!='404')
            {
              $page_id = sqlQueryValue("SELECT page_id FROM " . $site_id . "_pages WHERE name='404' and parent=0");
              $GLOBALS['page_id'] = $page_id;
							header("HTTP/1.0 404 Not Found");

            }

            if(!$page_id)
              return ("Error 404 - page not found");
          }

          $pageData = sqlQueryRow("SELECT * FROM " . $site_id . "_pages LEFT JOIN " . $site_id . "_languages ON " . $site_id . "_languages.language_id=" . $site_id . "_pages.language WHERE " . $site_id . "_pages.page_id='$page_id'");

					$langs = getLanguages();
          $page_lang = $langs[$pageData['shortname']];

          if($pageData['in_trash'] || $page_lang['disabled']){
            $page_id = 0;

            //Check if we have a page for 404
            if($url['path']!='404')
            {
              $page_id = sqlQueryValue("SELECT page_id FROM " . $site_id . "_pages WHERE name='404' and parent=0");
              $GLOBALS['page_id'] = $page_id;
            }

            if(!$page_id)
              return ("Error 404 - page not found");

            $pageData = sqlQueryRow("SELECT * FROM " . $site_id . "_pages LEFT JOIN " . $site_id . "_languages ON " . $site_id . "_languages.language_id=" . $site_id . "_pages.language WHERE " . $site_id . "_pages.page_id='$page_id'");

          }

           if (in_array($pageData['page_id'], $ppath)) break;
        } while(($pageData['redirect'])and($pageData['redirect']!=$page_id));



        if ($redirs && $_GET['action'] != 'reindex')
        {
          $url = '/'.PagePathById($redirs[count($redirs)-1], $site_id);
          header("Location: ".$url, true, 301);
          die;
        }

        if (!empty($pageData['redirect_url']))
          redirect($pageData['redirect_url']);

        //dev page preview
        if (isset($_GET['showdevpage'])){
            $pagedev_id = $_GET['showdevpage'];
            $GLOBALS['pagedev_id'] = $pagedev_id;
            $pageData = sqlQueryRow("SELECT * FROM " . $site_id . "_pagesdev LEFT JOIN " . $site_id . "_languages ON " . $site_id . "_languages.language_id=" . $site_id . "_pagesdev.language WHERE " . $site_id . "_pagesdev.pagedev_id='$pagedev_id'");
        }

        $GLOBALS['pageData'] = $pageData;
        if((!$pageData['enabled'])and(!$allowdisabled)){
          return("The page has been disabled.");
        }

        $GLOBALS['hasbeenauthorized'] = false;
          //ok we have got to the page now
        if($pageData['passprotect'] && $authorize)
        {
          $err = '';
          $cms_username = '';
          $cms_password = '';
          $set_password = false;

          if ($_SESSION['cms_unprotect'])
          {
            $GLOBALS['hasbeenauthorized'] = true;
          }
          else if (!empty($_POST['username']) && !empty($_POST['password']))
          {
            $cms_username = $_POST['username'];
            $cms_password = $_POST['password'];
            $set_password = true;
          }
          else if (!empty($_COOKIE['cms_username']) && !empty($_COOKIE['cms_password']))
          {
            $cms_username = $_COOKIE['cms_username'];
            $cms_password = base64_decode($_COOKIE['cms_password']);
          }

          if (!empty($cms_username) && !empty($cms_password))
          {
            if ((valid_login($cms_username, $cms_password, $_SERVER['REMOTE_ADDR'], true))and((!$GLOBALS['currentUserSiteID'])or($GLOBALS['currentUserSiteID'] == $site_id)) )
            {
              $_SESSION['cms_unprotect'] = true;

              if ($set_password)
              {
                setcookie('cms_username', $_POST['username'], time()+30.5*86400, '/');
                setcookie('cms_password', base64_encode($_POST['password']), time()+30.5*86400, '/');
              }

              header('Location: '.$_SERVER['REQUEST_URI']);
              die;
            }
            else
              $err = 'Wrong username or password!';

          }

          if (!$_SESSION['cms_unprotect']) {
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <title>Login</title>
</head>
<body>
  <form id="login" action="" method="post">
  <? if ($err) { ?><div style="color:red"><?=$err ?></div><? } ?>
  <table>
    <tr>
      <td>Username:</td>
      <td><input type="text" name="username" value="<?=htmlspecialchars(stripslashes($_POST['username']))?>"/></td>
    </tr>
    <tr>
      <td>Password:</td>
      <td><input type="password" name="password" value=""/></td>
    </tr>
    <tr>
      <td></td>
      <td><input type="submit" value="Login"/></td>
    </tr>
  </table>
  </form>
  <script type="text/javascript"> document.getElementById('login').<?=$err ? 'password' : 'username' ?>.focus() </script>
</body>
</html>
<?
            die;
          }

        }else if($pageData['passprotect']){
          //return 'This page has been password protected';
        }

        if($forcetemplate){
          $template_id = sqlQueryValue("SELECT template_id FROM " . $site_id . "_templates WHERE name='" . $forcetemplate . "'");
          if($template_id){
            $pageData['template'] = $template_id;
          }
        }

        if(!($pageData['template'])){
          return ("The page currently has no template selected");
        }

        $noctemplate = false;

        if($templateData == null){
          $templateData = sqlQueryRow("SELECT body, copybody FROM " . $site_id . "_templates WHERE template_id=" . $pageData['template']);
          $noctemplate = true;
        }

        $GLOBALS['template_id'] = $pageData['template'];
        $GLOBALS['language_id'] = $pageData['language'];
        $GLOBALS['language'] = $pageData['shortname'];
        $GLOBALS['copypage'] = $pageData['copypage'];

        if (isset($_GET['glo_view']))
        {
            $GLOBALS['copypage'] = sqlQueryValue("SELECT view_id FROM ".$site_id."_views WHERE name='".$_GET['glo_view']."' AND template_id=".$template_id);
        }

        $body = $templateData['body'];

        $copybody = $templateData['copybody'];
        while($copybody)
          list($body, $copybody) = sqlQueryRow("SELECT body, copybody FROM " . $site_id . "_templates WHERE template_id=", $copybody);


        $body = attachSubTemplates($body, $site_id);


        //process template language
        //remove comments
       $body = preg_replace('/{%\/\/(.*?)\/\/%}/s', '', $body);

       //process [php] *** [/php] tags
        preg_match_all("/\[php\](.*?)\[\/php\]/ism", $body, $matches);

        foreach($matches[1] as $ind => $match){

          ob_start();
          eval($match);
          $content = ob_get_contents();
          ob_end_clean();

          $body = str_replace($matches[0][$ind], $content, $body);

        }

        //process ifvisible parts
        while(preg_match_all('/{%ifvisible([0-9]*?):([a-zA-Z0-9|]*?)%}(.*?){%fivisible\\1%}/s', $body, $matches, PREG_SET_ORDER)){

          foreach($matches as $key => $row){
            $s = explode('{%elsevisible' . $row[1] . '%}', $row[3]);
            $components = explode('|', $row[2]);
            $hidden = true;
            foreach($components as $component){
              $hid = getProperty('visible', $component, '', $page_id, $site_id, $pageData['copypage']);
              if(!$hid) $hidden = false;
            }

            if(!$hidden)
              $body = str_replace($row[0], $s[0], $body);
            else
              $body = str_replace($row[0], $s[1], $body);
          }

        }

        //process ifdesign parts
        preg_match_all ( '/{%ifdesign([0-9]*?)%}(.*?){%fidesign\\1%}/s', $body, $matches, PREG_SET_ORDER);
        foreach($matches as $key => $row)
        {
          $s = explode('{%elsedesign' . $row[1] . '%}', $row[2]);
          $body = str_replace($row[0], $s[1], $body);
        }
        //process iflanguage parts
        preg_match_all ( '/{%iflanguage([0-9]*?):([a-zA-Z0-9|]*?)%}(.*?){%filanguage\\1%}/s', $body, $matches, PREG_SET_ORDER);
        foreach($matches as $key => $row)
        {
          $s = explode('{%elselanguage' . $row[1] . '%}', $row[3]);
          $languages = explode('|', $row[2]);
          $shortname = sqlQueryValue("SELECT shortname FROM " . $site_id . "_languages WHERE language_id=" . $pageData['language']);
          $hidden = true;
          foreach($languages as $l)
          {
            if($l == $shortname)
              $hidden = false;
          }
          if(!$hidden)
            $body = str_replace($row[0], $s[0], $body);
          else
            $body = str_replace($row[0], $s[1], $body);
        }

        $GLOBALS['must_index_components'] = array();
				if(PROCESS_SEARCH_PARTS){
	        //process onsearch/nosearch parts
	        preg_match_all( '/{%nosearch%}(.*?){%onsearch%}/s', $body, $matches, PREG_SET_ORDER);

	        $temp_body = $body;
	        foreach($matches as $key => $row){
	          $temp_body = str_replace($row[0], "", $temp_body);
	        }

	        $componentcount = preg_match_all("/{%component:([a-zA-Z0-9_]+):([a-zA-Z0-9]+)(:[a-zA-Z0-9% \\.\\,\\\"\\\'\\#\\!-]+)?%}/s", $temp_body, $matches, PREG_SET_ORDER);

	        if($componentcount){

	          foreach($matches as $match){

	            $component = Array(
	              'fulltag' => $match[0],
	              'type' => $match[1],
	              'name' => $match[2],
	              'params' => $match[3],
	            );

	            $component['obj'] = initializeComponent($component['type'], $component['name']);

	            if($component['obj'] && $component['obj']->getProperty("visible") == 0){
	              $GLOBALS['must_index_components'][] = $match[2];
	            }

	          }

	        }

				}

        //dev page strings
        if (isset($_GET['showdevpage']))
        {
            $pagedev_id = $_GET['showdevpage'];
            //strings
            while (preg_match("/{%string:([a-zA-Z0-9_]+)%}/s", $body, $regs))
            {
              $body = str_replace($regs[0], GetPageDevString($site_id, $pagedev_id, $regs[1]), $body);
            }
        }
        else
        {
            //strings
            while (preg_match("/{%string:([a-zA-Z0-9_]+)%}/s", $body, $regs))
            {
              $body = str_replace($regs[0], GetPageString($site_id, $page_id, $regs[1]), $body);
            }
        }

        //languagestrings
        while (preg_match("/{%ls:([a-zA-Z0-9_]+)(:(([^%])*))?%}/s", $body, $regs)){
          $ls = GetLanguageString($site_id, $regs[1], '', $pageData['language']);

          if(!$ls){
            $ls = $regs[3];
						$string = $ls;
            $key = transliterateText($ls);
 
						$cmp = "_template";

            if (file_exists(TRANSLATION_FOLDER.$pageData['language'].'.'.$cmp.'.lang.php')){
							require_once(TRANSLATION_FOLDER.$pageData['language'].'.'.$cmp.'.lang.php');
						}

            if (!isset($languagestrings[$pageData['language']][$cmp][$key])){
				    	$test=DB::GetValue("select ls_id from ".$site_id."_languagestrings where language=:lang and name=:key and component=:cmp",array(':lang'=>$pageData['language'],':key'=>$key,':cmp'=>$cmp));
							if (!$test){
				      	DB::Query("INSERT INTO `".$site_id."_languagestrings`(language, name, value, `default`, component) VALUES(:lang,:key,:val,:val,:cmp)",array(':lang'=>$pageData['language'],':key'=>$key,':val'=>$string,':cmp'=>$cmp));
								DB::Query("INSERT INTO ".$site_id."_languagestrings_info(name, component, is_used) VALUES(:key,:cmp,1)",array(':key'=>$key,':cmp'=>$cmp));
							}
				      $res=DB::GetTable("select name,value,`default` from ".$GLOBALS['site_id']."_languagestrings where language=:lang and component=:cmp order by name",array(':lang'=>$pageData['language'],':cmp'=>$cmp));
							if (!file_exists($GLOBALS['cfgDirRoot'].'lang')){
								mkdir($GLOBALS['cfgDirRoot'].'lang');
							}
							$f=fopen(TRANSLATION_FOLDER.$pageData['language'].'.'.$cmp.'.lang.php','w');
							fwrite($f,'<?php'."\n");
							foreach ($res as $r){
								$value = $r['value']?$r['value']:$r['default'];
								fwrite($f,'$languagestrings['.$pageData['language'].'][\''.$cmp.'\'][\''.$r['name'].'\']=\''.str_replace("'","\'",str_replace('\\','\\\\',$value)).'\';'."\n");
							}
							fwrite($f,'?>');
							fclose($f);
							unset($languagestrings[$shortname][$cmp]);
							clearstatcache();
							include(TRANSLATION_FOLDER.$pageData['language'].'.'.$cmp.'.lang.php');
				    }

          }else{
          	$key = transliterateText($regs[3]);
          }

          $body = str_replace($regs[0], $languagestrings[$pageData['language']][$cmp][$key], $body);
        }

        //variables
        $body = str_replace("{%var:name%}", $pageData['name'], $body);
        $body = str_replace("{%var:site_id%}", $site_id, $body);
        $body = str_replace("{%var:page_id%}", $page_id, $body);
        $body = str_replace("{%var:template_id%}", $template_id, $body);

        //language, encodings
        $pos = strpos($body, '{%language:');


        if($pageData['language'])
        {
          list($shortname, $fullname, $encoding) = sqlQueryRow("SELECT shortname, fullname, encoding FROM " . $site_id . "_languages WHERE language_id=" . $pageData['language']);
        }else
        {
          $shortname = '';
          $fullname = '';
          $encoding = 'UTF-8';
        }

        if($pos)
        {
          $body = str_replace("{%language:shortname%}", $shortname, $body);
          $body = str_replace("{%language:fullname%}", $fullname, $body);
          $body = str_replace("{%language:encoding%}", $encoding, $body);
        }

        if($noctemplate)
        {
          $cbody = $body;
        }

        //dynamic stuff (that don't get compiled into ctemplate) goes here
        //gets and posts
        while (preg_match("/{%get:([a-zA-Z0-9]+)%}/s", $body, $regs))
        {
          $body = str_replace($regs[0], $_GET[$regs[1]], $body);
        }
        while (preg_match("/{%getx:([a-zA-Z0-9]+)%}/s", $body, $regs))
        {
          $getval = $_GET[$regs[1]];
          $getval = strip_tags($getval);
          $body = str_replace($regs[0], $getval, $body);
        }
        while (preg_match("/{%post:([a-zA-Z0-9]+)%}/s", $body, $regs))
        {
          $body = str_replace($regs[0], $_POST[$regs[1]], $body);
        }

        //variables that change dynamically
        $body = str_replace("{%url:host%}", $url['host'], $body);
        $body = str_replace("{%url:path%}", $url['path'], $body);
        $body = str_replace("{%url:query%}", $url['query'], $body);

        if(defined("DOMAIN")){
        	$body = str_replace("{%var:sitedomain%}", DOMAIN, $body);
        }

        $componentcount = preg_match_all("/{%component:([a-zA-Z0-9_]+):([a-zA-Z0-9]+)(:[a-zA-Z0-9% \\.\\,\\\"\\\'\\#\\!-]+)?%}/s", $body, $matches, PREG_SET_ORDER);

        //initialize all page component objects
        $GLOBALS['page_components'] = Array();
        if($componentcount){
          foreach($matches as $match){
            $component = Array(
              'fulltag' => $match[0],
              'type' => $match[1],
              'name' => $match[2],
              'params' => $match[3],
            );

            $component['obj'] = initializeComponent($component['type'], $component['name']);

            if($component['obj']->service_name && !isset($GLOBALS['page_components'][$component['obj']->service_name])){
              $component['service'] = $component['obj']->service_name;
              $GLOBALS['page_components'][$component['service']] = $component;
            }else{
              $GLOBALS['page_components'][$component['name']] = $component;
            }
          }

        }

        foreach ($GLOBALS['page_components'] as $key => $component){
          if($GLOBALS['page_components'][$key]['obj'] !== false){
            executeComponent($GLOBALS['page_components'][$key]['obj'], $component['type'], $component['name']);
          }
        }

				$title_tag_suffix = option('title_tag_suffix', NULL, 'Title tag suffix', array('value_on_create' => ' - E-Veikals'));

        $title = $pageData['title'].$title_tag_suffix;

				$optparams = array('use_pagecat' => 1, 'page_id' => $page_id);
        $title_tag = readOption('title_tag', $optparams, $site_id);

				if($title_tag){
					$title = $title_tag;
				}

        if($GLOBALS['force_title']){
          $title = $GLOBALS['force_title'];
        }

        $body = str_replace("{%title%}", $title, $body);

        //display all page components
        foreach ($GLOBALS['page_components'] as $component){
          $comp = outputComponent($component['obj'], $component['type'], $component['name'], $canreplace);

          if ($GLOBALS['ajaxrequest']){
            if ($GLOBALS['ajax_component_name'] == $component['name'])
              return $comp;

          }else if ($GLOBALS['xmlrequest']){
            if ($comp)
              $GLOBALS['out_xml']->addChild($comp);

          }else if ($GLOBALS['outputpage']){

            $body = str_replace_once($component['fulltag'], $comp, $body);

						if($noctemplate){
              $cc = $comp;
              if($canreplace)
              {
                $cbody = str_replace($regs[0], $cc, $cbody);
              }
            }

          }
        }

        //process ifset parts
        while(preg_match_all ( '/{%ifset([0-9]*?):([a-zA-Z0-9_-]*?)(?::([a-zA-Z0-9_-]*?))?%}(.*?){%fiset\\1%}/s', $body, $matches, PREG_SET_ORDER))
        {
          foreach($matches as $key => $row)
          {
            $s = explode('{%elseset' . $row[1] . '%}', $row[4]);
            $flagname = $row[2];
            $componentname = $row[3];

            $hasflag = false;
            if($componentname)
            {
              if(isset($GLOBALS['component_flags'][$flagname][$componentname]))
                $hasflag = $GLOBALS['component_flags'][$flagname][$componentname];
            }
            else if($GLOBALS['component_flags'][$flagname])
            {
              foreach($GLOBALS['component_flags'][$flagname] as $flag)
                $hasflag = ($hasflag || $flag);
            }

            if($hasflag)
              $body = str_replace($row[0], $s[0], $body);
            else
              $body = str_replace($row[0], $s[1], $body);
          }
        }

        if ($GLOBALS['outputpage'])
        {
          // construct html header
          $header = '';
          $optparams = array('use_pagecat' => 1, 'page_id' => $page_id);

          /* $meta_keywords = readOption('meta_keywords', $optparams, $site_id);
          $meta_keywords .= $GLOBALS['meta_keywords'];
          if (!empty($meta_keywords))
            $header .= '  <meta name="keywords" content="'.$meta_keywords.'"/>'."\n"; */

          $meta_description = readOption('meta_description', $optparams, $site_id);
          $meta_description .= $GLOBALS['meta_description'];
          if (!empty($meta_description))
            $header .= '  <meta name="description" content="'.$meta_description.'"/>'."\n";			
			
          $header .= "  ".$GLOBALS['additional_header_tags']."\n";


          $header .= $GLOBALS['scripts_manager']->generateHeaderHTML();

          //global variables (after processing components)
          while (preg_match("/{%var:glo:([a-zA-Z0-9_]+)%}/s", $body, $regs))
          {
            $body = str_replace($regs[0], $GLOBALS[$regs[1]], $body);
          }

          //document links
          while (preg_match("/{%documentfile:([a-zA-Z0-9]+)%}/s", $body, $regs))
          {
            $body = str_replace($regs[0], 'constructor.getfile/?file='.$regs[1], $body);
          }

          // noindex and nofollow in DevMode
					if($GLOBALS['developmode']){
						$header .= "<meta name='robots' content='noindex, nofollow' />\n";
          }

          $body = str_replace("{%null%}", '', $body);
          $body = str_replace("{%header%}", $header, $body);
          $body = str_replace("{%visualeditjs%}", $header, $body);
        }

        if ($GLOBALS['outputpage'])
        {
          //index page for search if neccessary
          if($pageData['mustsearchindex'])
          {
            IndexPage($site_id, $page_id, $body);
          }

          //remove search indexing controller tags after indexing
          if(!$GLOBALS['search_leavesearchtags'])
          {
            $body = str_replace("{%nosearch%}", '', $body);
            $body = str_replace("{%onsearch%}", '', $body);
          }
        }

        if ($GLOBALS['xmlrequest'])
          $body = $GLOBALS['out_xml']->output();

        return $body;
    }


  $GLOBALS['runningfromfrontend'] = true;

  $GLOBALS['url'] = $url;
  $body = DisplayPage($page_id, $site_id, $encoding, false, $force_template, !$GLOBALS['dontneedauth']);

  $GLOBALS['nocache'] = false;
  $GLOBALS['maincharset'] = $encoding;
  require($GLOBALS['cfgDirRoot'] . "library/inc.http_headers.php");

  echo $body;

 if ($_SERVER["HTTP_X_FORWARDED_FOR"]) {
	if ($_SERVER["HTTP_CLIENT_IP"]) {
		$proxy = $_SERVER["HTTP_CLIENT_IP"];
	} else {
		$proxy = $_SERVER["REMOTE_ADDR"];
  }
 }else{
 	unset($proxy);
 }

 $sm_id      = sqlQueryValue("SELECT sm_id FROM ".$site_id."_stats_main WHERE session_id='" . session_id() . "'");
 $ip         = $_SERVER["REMOTE_ADDR"];
 $page_path  = $_SERVER['SCRIPT_URL'] . ( (substr($_SERVER["SCRIPT_URL"], -1) != "/") ? '/' : '' );
 $is_proxy   = intval(isset($proxy));
 $user_agent = addslashes(getenv("HTTP_USER_AGENT"));
 $referrer   = addslashes(getenv("HTTP_REFERER"));
 $lang       = BrowserLanguage();
 //if not new user store page data only
 if ($sm_id){
    $last_visited = sqlQueryValue("SELECT page_path FROM ".$site_id."_stats_pages WHERE sm_id=$sm_id ORDER BY time DESC LIMIT 1");

    //check if last visited page of user is not same as current page
    if ($page_path != $last_visited){
        DB::Query("INSERT INTO ".$site_id."_stats_pages (sm_id, time, page_path) VALUES(:id, :time, :path)", array(":id" => $sm_id, ":time" => time(), ":path" => $page_path));
    }

 } else{ //store user data and current page data

    DB::Query("INSERT INTO ".$site_id."_stats_main (session_id, ip, referrer, user_agent, lang, is_proxy, time) VALUES ('".session_id()."', '$ip', '$referrer', '$user_agent', '$lang', $is_proxy, " . time() . ")");

    $sm_id = DB::GetLastInsertId();
    DB::Query("INSERT INTO ".$site_id."_stats_pages (sm_id, time, page_path) VALUES(:id, :time, :path)", array(":id" => $sm_id, ":time" => time(), ":path" => $page_path));

 }

 //delete old stats from DB
 if ($GLOBALS['cfgStatiscticsPeriod']==2) {
    $yr = mktime(date("H"), date("i"), date("s"), date("m")  , date("d"), date("Y")-1);
    sqlQuery("DELETE FROM `".$site_id."_stats_main` WHERE time<'".$yr."'");
    sqlQuery("DELETE FROM `".$site_id."_stats_pages` WHERE time<'".$yr."'");
 }
 else if ($GLOBALS['cfgStatiscticsPeriod']==1) {
    $m = mktime(date("H"), date("i"), date("s"), date("m")-1  , date("d"), date("Y"));
    sqlQuery("DELETE FROM `".$site_id."_stats_main` WHERE time<'".$m."'");
    sqlQuery("DELETE FROM `".$site_id."_stats_pages` WHERE time<'".$m."'");
 }
 else {
    $w = mktime(date("H"), date("i"), date("s"), date("m")  , date("d")-7, date("Y"));
    sqlQuery("DELETE FROM `".$site_id."_stats_main` WHERE time<'".$w."'");
    sqlQuery("DELETE FROM `".$site_id."_stats_pages` WHERE time<'".$w."'");
 }

  $ref = $_SERVER['HTTP_REFERER'];
  if (!empty($ref) && ($ref != $_SESSION['HTTP_LAST_REFERER']))
  {
    $_SESSION['HTTP_LAST_REFERER'] = $ref;
    addReferer($ref);
  }

  $end = microtime(true);
  $dif = $end - $start;

  if(LOG_EXECUTION_TIME){
    file_put_contents(EXECUTION_TIME_FILE, $_SERVER['REQUEST_URI']." ".$dif . " Queries: ". $GLOBALS['total_queries_executed'] . " (Query time: ".$GLOBALS['total_query_time'].")" . PHP_EOL, FILE_APPEND);
  }
	
	function unlinkr($dir, $pattern = "*") {
    // find all files and folders matching pattern
    $files = glob($dir . "/$pattern"); 

    //interate thorugh the files and folders
    foreach($files as $file){ 
    //if it is a directory then re-call unlinkr function to delete files inside this directory     
        if (is_dir($file) and !in_array($file, array('..', '.','.htaccess','.gitignore')))  {
            echo "<p>opening directory $file </p>";
            unlinkr($file, $pattern);
            //remove the directory itself
            echo "<p> deleting directory $file </p>";
            rmdir($file);
        } else if(is_file($file) and ($file != __FILE__)) {
          if(($file != '.htaccess') OR ($file != '.gitignore')){
						// make sure you don't delete the current script
            echo "<p>deleting file $file </p>";
            unlink($file); 
					}
        }
    }
	}	