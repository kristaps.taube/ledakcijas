ALTER TABLE `4_languages` ADD COLUMN `disabled` INT(1) NOT NULL DEFAULT 0 AFTER `is_default`;
UPDATE phrases set lang = 'en' where lang = 'English';
UPDATE phrases set lang = 'lv' where lang = 'Latvian';
UPDATE phrases set lang = 'ru' where lang = 'Russian';
