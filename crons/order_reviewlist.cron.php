<?php

if(php_sapi_name() != 'cli'){
	die("failed");
	chdir("..");
}

require("config.php");
require("init.php");

// here we assume we got only one site!
$GLOBALS['site_id'] = (int)DB::GetValue('select site_id from sites');

$order_reviewlist = createComponentInstance("no_name", "EVeikalsOrderReviewSender");

$order_reviewlist->send();
