<?php

if(php_sapi_name() != 'cli'){
	die("failed");
	chdir("..");
}

require("config.php");
require("init.php");

// here we assume we got only one site!
$site = DB::GetRow('select site_id,dirroot from sites LIMIT 1');

$GLOBALS['site_id'] = (int)$site['site_id'];
define("DIR_ROOT", $site['dirroot']);

$swedbank = createComponentInstance("Swedbank", "EVeikalsSwedbank");
$swedbank->orderform = createComponentInstance("orderform", "shoporderform");

$swedbank->processCron();
