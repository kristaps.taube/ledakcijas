<?php

if(php_sapi_name() != 'cli'){
	die("failed");
}

require("config.php");

$command = "mysqldump -h ".DB_HOST." ".(DB_PORT ? "-P ".DB_PORT : "")." -u ".DB_USERNAME." -p".DB_PASSWORD." ".DB_DATABASE." | gzip > ".DB_BACKUP_FILE;

exec($command);