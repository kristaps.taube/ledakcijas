<?php

if(php_sapi_name() != 'cli'){
	die("failed");
	#chdir("..");
}

require("config.php");
require("init.php");

// here we assume we got only one site!
$site_id = (int)DB::GetValue('select site_id from sites');


// send pdo errors
if($GLOBALS['cfgAdminMail']){

	$date = strtotime('-1 day'); // here we asume that daily cron runs after midnight

	$file = PDO_ERROR_LOG_FOLDER.date("Y", $date)."/".date("m", $date)."/".date("d", $date).".txt";
	if(file_exists($file)){

  	$sent = phpMailerSend("Error report is added to this letter. Have fun..", "PDO error report from ".$GLOBALS['cfgDomain'], "Reporter", "errors@".$GLOBALS['cfgDomain'], $GLOBALS['cfgAdminMail'], true, false, Array($file));

	}

}

