<!DOCTYPE HTML>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />


  <link rel="stylesheet" href="/scr/js/fancybox/fancybox.css" type="text/css" />
  <link rel="stylesheet" href="/scr/css/stylesheet.css" type="text/css" />
  <link rel="stylesheet" href="/scr/css/adaptive.css" type="text/css" />
  {%component:EVeikalsDesignColors:DesignColors%}

  <script src="/scr/js/jquery.js" type="text/javascript"></script>
  <script src="/scr/js/jquery-ui.js" type="text/javascript"></script>
  <script src="/scr/js/jquery.autocomplete.js" type="text/javascript"></script>
  <script src="/scr/js/checkboxes.js" type="text/javascript"></script>
  <script src="/scr/js/fancybox/fancybox.js" type="text/javascript"></script>
  <script src="/scr/js/scripts.js" type="text/javascript"></script>

  <title>{%title%} - {%ls:title:E-veikals%}</title>
  {%header%}
  {%var:glo:head_user_scripts%}
</head>
<body>
  <div id="Hat">
    <div class='width_wrapper'>
      <div class='first column'>
        <a href="#mobile-menu" id="mobile-menu-trigger"></a>
        <a href='/' class='logo_link'><img src='/images/html/{%var:glo:designcolor%}/logo.png' alt='' /></a>
        <ul class='lang menu'>
          <li class='{%iflanguage0:lv%}active{%filanguage0%}'><a href="{%component:shoplangswitcher:shopswitchlv:novisualedit "1"%}">LV</a></li>
          <li class='{%iflanguage1:en%}active{%filanguage1%}'><a href="{%component:shoplangswitcher:shopswitchen:novisualedit "1"%}">EN</a></li>
          <li class='{%iflanguage2:ru%}active{%filanguage2%}'><a href="{%component:shoplangswitcher:shopswitchru:novisualedit "1"%}">RU</a></li>
        </ul>
      </div>
      <div class='second column'>
      {%component:shopsmallcart:smallcart%}
      </div>
      <div class='third column'>
        {%component:EVeikalsLogin:Login%}
        <div class='cb'></div>
      </div>
      <div class='cb'></div>
    </div>
  </div>
  <div id='Head' class='{%var:glo:slideshow%} {%ifvisible3:Slideshow%}{%elsevisible3%}no_slideshow{%fivisible3%}'>
    <div class='width_wrapper'>
      <div id='SocNetworks'>
        {%component:EVeikalsSocIcons:EVeikalsSocIconsTop%}
      </div>
      <div id="CatalogSearch">
      	{%component:EVeikalsCatalogSearch:CatalogSearch%}
      </div>
      <div class='cb'></div>
    </div>
  </div>
  {%ifvisible1:Slideshow%}
  {%component:bannerlisttemplate:Slideshow:novisualedit "1"%}
  {%elsevisible1%}
  <div id='HeaderBigSep'></div>
  {%fivisible1%}
  <div id='TopMenuWrap' class='{%var:glo:slideshow%} {%ifvisible3:Slideshow%}{%elsevisible3%}no_slideshow{%fivisible3%}'>
    <div id='TopMenu' class='width_wrapper'>
      {%component:menutwoleveltemplate:TopMenu%}
      <div class='cb'></div>
    </div>
  </div>
  <div id='Body'>
    <div class='width_wrapper'>
      {%component:EVeikalsBreadcrumbs:Breadcrumbs%}
      <div id='LeftColumn'>
        {%component:EVeikalsShopMenu:ShopMenu%}
        {%component:EVeikalsShopFilter:ShopFilter%}
        {%component:mailinglistex:mailinglist%}
        {%component:bannerlisttemplate:LeftBanners%}
      </div>
      <div id='RightColumn'>

        {%ifvisible5:shop%}
          {%component:FrontendShop:shop%}
        {%elsevisible5%}
          <div id="PageContent">
            <h1 class='styled'>{%component:title:title%}</h1>
            {%component:formatedtext0:contenttext%}

            {%component:EVeikalsRegistrationForm:regform%}
            {%component:EVeikalsPasswordRecovery:paswrecover%}
            {%component:EVeikalsBigCart:BigCart%}
            {%component:shoporderform:orderform%}
            {%component:EVeikalsGiftCard:GiftCard%}
            {%component:EVeikalsLogin:LoginForm%}
            {%component:EVeikalsShopMap:ShopMap%}

          </div>
        {%fivisible5%}
        {%component:EVeikalsTabbedProducts:TabbedProducts%}

      </div>
      <div class='cb'></div>
    </div>
  </div>
  <div id='Footer'>
    <div class='menu width_wrapper'>
      {%component:menutwoleveltemplate:FooterMenu%}
      <div class='cb'></div>
    </div>
    <div id='MobileMailinglist'>
    	{%component:mailinglistex:mailinglistmobile%}
    </div>
    <div class='columns'>
      <div class='width_wrapper'>
        <div class='first column'>
          <div class='title'>{%ls:contacts:Kontakti%}:</div>
          <img src='/images/html/icon_phone.png' alt='' /> <span>{%ls:contacts_phone:(00371) 29893933%}</span><br />
          <img src='/images/html/icon_email.png' alt='' /> <span>{%ls:contacts_email:info@eveikals.lv%}</span>
        </div>
        <div class='second column'>
          <div class='title'>Apmaksas veidi:</div>
          <img src='/images/html/mastercard.png' alt='' />
          <img src='/images/html/visa.png' alt='' />
          <img src='/images/html/paypal.png' alt='' />
        </div>
        <div class='third column'>
          {%component:EVeikalsSocIcons:EVeikalsSocIconsBottom%}
        </div>
        <div class='fourth column'>
          <div class='title'>Mājas lapas izstrāde:</div>
          <a href='http://www.datateks.lv' title="Datateks" target='_blank'><img src='/images/html/datateks.png' alt='' /></a>
        </div>
        <div class='cb'></div>
      </div>
    </div>
    <div class='copyrights'>{%ls:copyrights:&copy; Copyright 2013 SIA e-veikals All rights reserved.%}</div>
  </div>

  <!-- contact form -->
  {%component:EVeikalsContactForm:ContactForm:novisualedit "1"%}
  <!-- contact form end -->
  {%var:glo:body_user_scripts%}
  <script type="text/javascript">
    (function() {
     var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
     po.src = 'https://apis.google.com/js/client:plusone.js';
     var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
   })();
  </script>
</body>
</html>
{%component:EVeikalsMobileMenu:MobileMenu:novisualedit "1"%}
{%component:shop:backendshop:novisualedit "1"%}
{%component:shopmanager:eshopmanager:novisualedit "1"%}
{%component:shopordereditor:rekinuredaktors:novisualedit "1"%}
{%component:userscripts:uscripts:novisualedit "1"%}
{%component:EVeikalsCurrencyMenu:CurrencyMenu:novisualedit "1"%}
{%component:FrontendShop:shopservice:novisualedit "1"%}
{%component:EVeikalsAirPay:EVeikalsAirPay:novisualedit "1"%}
{%component:EVeikalsPromo:EVeikalsPromo:novisualedit "1"%}
{%component:EVeikalsMaintenance:Maintenance:novisualedit "1"%}