<?php

require __DIR__ . DIRECTORY_SEPARATOR . 'config.php';  // old config

return [
    'version' => CMS_VERSION,
    'developmode' => $GLOBALS['developmode'],
    'default_route' => 'index/index', // <controller>/<action>
    'default_template' => 'default',
    'domain' => $cfgDomain,
    'url_prefix' => '', # piem "/veikals"
    'https' => false,
    'language' => 'lv',
    'admin_email' => $cfgAdminMail,
    'licence_type' => $cfgLicType,
    'timezone' => 'Europe/Riga',
    'database' => [ // fill these values in local config
        'host' => '',
        'port' => '',
        'name' => '',
        'username' => '',
        'password' => '',
    ],
    'web_root' => $GLOBALS['cfgWebRoot'],
    'dir_root' => dirname(__FILE__),
    'memory_limit' => '256M',
    'translation_folder' => dirname(__FILE__).'/translations/',
    'translations_store' => dirname(__FILE__).'/translations/store.txt',
    'template_folder' => dirname(__FILE__).'/templates/',
    'temp_folder' => dirname(__FILE__).'/temp/',
    'view_folder' => dirname(__FILE__).'/views/',
    'errors' => [
        'reporting_level' => E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT,
        'display' => false,
        'log' => true,
        'file' => dirname(__FILE__).'/logs/php_errors.log'
    ],
    'log' => [
        'directory' => dirname(__FILE__).'/logs/',
        'execution_time' => [
            'log' => false,
            'file' => 'execution_time.log'
        ],
        'component_output_time' => [
            'log' => false,
            'file' => 'component_output_time.log'
        ],
        'slow_queries' => [
            'log' => false,
            'time_limit' => 0.3,
            'file' => 'slow_queries.log'
        ],
        'db_queries' => [
            'log' => false,
            'file' => "queries/queries_".uniqid().".log"
        ]
    ],
    'extensions' => [
        'logger' => [
            'class' => 'Extensions\LogExtension',
            'output_checkpoints' => false,
            'log_to_file' => true,
            'clear_log' => true,
            'file' => dirname(__FILE__).'/logs/logger.log'
        ],
        'manager' => ['class' => 'Extensions\ShopManager'],
        'cart' => ['class' => 'Extensions\CartExtension'],
        'user' => ['class' => 'Extensions\UserExtension'],
        'session' => [
            'class' => 'Extensions\SessionExtension',
            'session_key' => 'Constructor-frontend'
        ]
    ],
    'url_rules' => [
        'UrlRules\FirstUrlRule',
        'UrlRules\CatalogUrlRule',
        'UrlRules\PageUrlRule',
        'UrlRules\SystemUrlRule',
        'UrlRules\BlogUrlRule',
        'UrlRules\FilterLinkUrlRule',
        #'UrlRules\DefaultUrlRule',
        'UrlRules\LastUrlRule',
    ],
    'commands' => [
        'test' => 'commands\TestCommand'
    ],
    'crons' => [
        'config' => dirname(__FILE__).'/crons/crontab.conf',
        'directory' => dirname(__FILE__).'/crons/',
    ],
    'cache' => [
        'use' => false,
        'config' => dirname(__FILE__).'/cache/cache_config.php',
        'directory' => dirname(__FILE__).'/cache/',
        'default_cache_time' => 300
    ],
    'backup' =>[
        'directory' => dirname(__FILE__).'/backup/',
        'db_file' => 'db_backup.gz'
    ],
    'autoload' => [
        dirname(__FILE__).'/library/func.misc.php',
        dirname(__FILE__).'/library/func.files.php',
        dirname(__FILE__).'/library/func.components.php',
        dirname(__FILE__).'/library/func.database.php',
        dirname(__FILE__).'/library/func.interface.php',
        dirname(__FILE__).'/library/func.login.php',
        dirname(__FILE__).'/library/func.permissions.php',
        dirname(__FILE__).'/library/func.log.php',
        dirname(__FILE__).'/library/func.search.php',
        dirname(__FILE__).'/library/func.options.php',
        dirname(__FILE__).'/library/func.xmlrpc.php',
        dirname(__FILE__).'/library/class.db.php',
    ]
];
